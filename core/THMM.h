/*
 * THMM.h
 *
 *  Created on: Jan 19, 2021
 *      Author: phaentu
 */

#ifndef MCMCFRAMEWORK_CORE_THMM_H_
#define MCMCFRAMEWORK_CORE_THMM_H_

#include "TEM.h"

//-------------------------------------
// THMM
//-------------------------------------
template <typename PrecisionType, typename NumStatesType, typename LengthType> class THMM : public TEM_base<PrecisionType, NumStatesType, LengthType> {
private:
    THMMForwardBackward<PrecisionType, NumStatesType, LengthType> _forwardBackward;
    TTransitionMatrix_base<PrecisionType, NumStatesType, LengthType>& _EMPrior;
    bool _curForwardIsAtBeginningOfAJunk; //variable used to handle very first of a junk differently inside handles.

    using TEM_base<PrecisionType,NumStatesType,LengthType>::_latentVariable;
    using TEM_base<PrecisionType,NumStatesType,LengthType>::_writeLog;
    using TEM_base<PrecisionType,NumStatesType,LengthType>::_logfile;

protected:
    //standard HMM functions
    PrecisionType _runForward(const LengthType & First, const LengthType & Last, THMMForwardAlpha<PrecisionType, NumStatesType, LengthType> & Alpha){
        if(_writeLog){
            _logfile->listFlushTime("Running forward-algorithm ...");
        }

        //ensure capacity
        int dist = Last - First;
        if(dist < 1){ throw std::runtime_error("PrecisionType _runForward(const LengthType & First, const LengthType & Last, THMMForwardAlpha<PrecisionType, NumStatesType, LengthType> & Alpha): distance between First and Last is < 1!"); }

        //calculate first alpha
        THMMEmission<PrecisionType, NumStatesType> emission(_EMPrior.numStates());
        _latentVariable.calculateEmissionProbabilities(First, emission);
        Alpha.startCalculationsForwards(_EMPrior, emission);

        //loop across others
        for(LengthType i = First + 1; i < Last; ++i){
            _latentVariable.calculateEmissionProbabilities(i, emission);
            Alpha.moveCalculationsForward(i, _EMPrior, emission);
        }

        //done
        if(_writeLog){
            _logfile->doneTime();
        }

        //return LL
        return Alpha.LL();
    };

    void _runBackward(const LengthType & First, const LengthType & Last, THMMBackwardBeta<PrecisionType, NumStatesType, LengthType> & Beta){
        if(_writeLog){
            _logfile->listFlushTime("Running backward-algorithm ...");
        }

        //calculate last beta
        THMMEmission<PrecisionType, NumStatesType> emission(_EMPrior.numStates());
        Beta.startCalculationsBackwards(_EMPrior);

        //loop across others
        //index runs from Last to 1 to be able to use unsigned
        for(LengthType i = Last-1; i > First; --i){
            //calculate emission at next (index i points to next data)
            _latentVariable.calculateEmissionProbabilities(i, emission);

            //iterate backward
            Beta.moveCalculationsBackward(i, _EMPrior, emission);
        }

        //done
        if(_writeLog){
            _logfile->doneTime();
        }
    };

    void _runForward(const LengthType & First, void (THMM::*handle)(const LengthType &)){
        if(_writeLog){
            _logfile->listFlushTime("Running forward-algorithm (posterior decoding) ...");
        }

        //calculate first alpha
        THMMEmission<PrecisionType, NumStatesType> emission(_EMPrior.numStates());
        _latentVariable.calculateEmissionProbabilities(First, emission);
        _forwardBackward.startCalculationsForwards(_EMPrior, emission);
        _curForwardIsAtBeginningOfAJunk = true;
        (this->*handle)(First);

        //loop across others
        _curForwardIsAtBeginningOfAJunk = false;
        LengthType Last = First + _forwardBackward.length();
        for(LengthType i = First + 1; i < Last; ++i){
            //move
            _latentVariable.calculateEmissionProbabilities(i, emission);
            _forwardBackward.moveCalculationsForward(i, _EMPrior, emission);
            (this->*handle)(i);
        }

        if(_writeLog){
            _logfile->doneTime();
        }
    };

    // function to run EM
    PrecisionType _runEMOneIteration_OneJunk(const LengthType & First, const LengthType & Last, void (TEM_base<PrecisionType,NumStatesType,LengthType>::*handle)(const LengthType &)) override{
        //run backward algorithm and store beta
        LengthType length = Last - First;
        _forwardBackward.beta().resize(_EMPrior.numStates(), length);
        _runBackward(First, Last, _forwardBackward.beta());

        //run forward algorithm with handle
        _runForward(First, handle);
        return _forwardBackward.alpha().LL();
    }

    // function to calculate likelihood
    PrecisionType _calculateLL_OneJunk(const LengthType & First, const LengthType & Last){
        return _runForward(First, Last, _forwardBackward.alpha());
    };

    // handle functions
    void _handleEMParameterEstimationOneIteration(const LengthType & Index) override{
        if(this->_EMEstimatePrior){
        	if(_curForwardIsAtBeginningOfAJunk && _forwardBackward.gammaCalculated()){
        		_EMPrior.handleEMParameterEstimationOneIterationInitialDistribution(Index, _forwardBackward.gamma());
        	} else if(_forwardBackward.xiCalculated()){
        		_EMPrior.handleEMParameterEstimationOneIterationTransitionProbabilities(Index, _forwardBackward.xi());
        	}
        }
        _latentVariable.handleEMParameterEstimationOneIteration(Index, _forwardBackward.gamma());
    };

    void _handleStatePosteriorEstimation(const LengthType & Index) override {
        _latentVariable.handleStatePosteriorEstimation(Index, _forwardBackward.gamma());
    };

    void _handleStatePosteriorEstimationAndWriting(const LengthType & Index) override {
        _latentVariable.handleStatePosteriorEstimationAndWriting(Index, _forwardBackward.gamma(), this->_out);
    };

public:
    THMM(TTransitionMatrix_base<PrecisionType, NumStatesType, LengthType> & EMPrior,
         TLatentVariable<PrecisionType, NumStatesType, LengthType> & LatentVariable,
         TParameters & Parameters,
         TLog* Logfile)
            : TEM_base<PrecisionType,NumStatesType,LengthType>(LatentVariable, Parameters, Logfile),
              _EMPrior(EMPrior){
    	_curForwardIsAtBeginningOfAJunk = false;
    };

    THMM(TTransitionMatrix_base<PrecisionType, NumStatesType, LengthType> & EMPrior,
         TLatentVariable<PrecisionType, NumStatesType, LengthType> & LatentVariable,
         TParameters & Parameters)
            : TEM_base<PrecisionType,NumStatesType,LengthType>(LatentVariable, Parameters),
                    _EMPrior(EMPrior){
    	_curForwardIsAtBeginningOfAJunk = false;
    };

    THMM(TTransitionMatrix_base<PrecisionType, NumStatesType, LengthType> & EMPrior,
         TLatentVariable<PrecisionType, NumStatesType, LengthType> & LatentVariable,
         const uint16_t & BaumWelchMaxNumIterations,
         const double & BaumWelchMinDeltaLL,
         const bool & BaumWelchEstimateTransitionMatrix)
            : TEM_base<PrecisionType,NumStatesType,LengthType>(LatentVariable, BaumWelchMaxNumIterations, BaumWelchMinDeltaLL, BaumWelchEstimateTransitionMatrix),
              _EMPrior(EMPrior){
    	_curForwardIsAtBeginningOfAJunk = false;
    };

    THMM(TTransitionMatrix_base<PrecisionType, NumStatesType, LengthType> & EMPrior,
         TLatentVariable<PrecisionType, NumStatesType, LengthType> & LatentVariable)
            : TEM_base<PrecisionType,NumStatesType,LengthType>(LatentVariable),
              _EMPrior(EMPrior){
    	_curForwardIsAtBeginningOfAJunk = false;
    };

    virtual ~THMM() = default;

    PrecisionType runEM(const std::vector<LengthType> & JunkEnds) {
        _forwardBackward.enableGammaCalculation();
        if (this->_EMEstimatePrior){
            _forwardBackward.enableXiCalculation();
        }
        return this->_runEM(JunkEnds, _EMPrior);
    }

    PrecisionType calculateLL(const std::vector<LengthType> & JunkEnds) {
        // we don't gamma and xi for this
        return this->_calculateLL(JunkEnds);
    }

    PrecisionType estimateStatePosteriors(const std::vector<LengthType> & JunkEnds, const bool & CalculateXi){
        _forwardBackward.enableGammaCalculation();
        if (CalculateXi){
            _forwardBackward.enableXiCalculation();
        }
        return this->_estimateStatePosteriors(JunkEnds, _EMPrior);
    }

    PrecisionType estimateStatePosteriors(const std::vector<LengthType> & JunkEnds, const std::string & Filename){
        _forwardBackward.enableGammaCalculation();
        return this->_estimateStatePosteriors(JunkEnds, Filename, _EMPrior);
    }
};




#endif /* MCMCFRAMEWORK_CORE_THMM_H_ */
