//
// Created by caduffm on 9/24/20.
//

//-------------------------------------------
// TMultiDimensionalStorage
//-------------------------------------------

template <typename T> TMultiDimensionalStorage<T>::TMultiDimensionalStorage() = default;

template <typename T> TMultiDimensionalStorage<T>::TMultiDimensionalStorage(const std::vector<size_t> & dimensions, bool trackExponential, bool trackLog, bool isUpdated) {
    init(dimensions, trackExponential, trackLog, isUpdated);
}

template <typename T> void TMultiDimensionalStorage<T>::_resizeDimensionNames() {
    _dimensionNames.resize(_dimension.numDim());
    for (size_t dim = 0; dim < _dimension.numDim(); dim++){
        if (!_dimensionNames[dim]){ // no valid ptr yet -> create one
            std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>(_dimension.dimensions()[dim]);
            _dimensionNames[dim] = dimNames;
        } else { // resize
            _dimensionNames[dim]->resize(_dimension.dimensions()[dim]);
        }
    }
}

template <typename T> void TMultiDimensionalStorage<T>::init(const std::vector<size_t> & dimensions, bool trackExponential, bool trackLog, bool isUpdated) {
    _dimension.init(dimensions);

    // initialize dimension names: each dimension gets basic name class that returns indices when called
    _resizeDimensionNames();

    // initialize values
    if (!isUpdated){
        if (trackExponential && trackLog) {
            _values = std::make_unique<TValueFixedWithLogAndExponentialVector<T>>(totalSize());
        } else if (trackExponential){
            _values = std::make_unique<TValueFixedWithExponentialVector<T>>(totalSize());
        } else if (trackLog){
            _values = std::make_unique<TValueFixedWithLogVector<T>>(totalSize());
        } else {
            _values = std::make_unique<TValueFixedVector<T>>(totalSize());
        }
    } else {
        if (trackExponential && trackLog) {
            _values = std::make_unique<TValueUpdatedWithLogAndExponentialVector<T>>(totalSize());
        } else if (trackExponential) {
            _values = std::make_unique<TValueUpdatedWithExponentialVector<T>>(totalSize());
        } else if (trackLog) {
            _values = std::make_unique<TValueUpdatedWithLogVector<T>>(totalSize());
        } else {
            _values = std::make_unique<TValueUpdatedVector<T>>(totalSize());
        }
    }
}

template <typename T> void TMultiDimensionalStorage<T>::resize(const std::vector<size_t> &dimensions) {
    // ATTENTION: if values have been filled already, this is a very dangerous function!
    // you should only change the entry of the vector of dimensions -> this does not change the coordinates of the values
    // that have already been filled
    // if a more inner dimension is changed, a already filled value will have different coordinates before and after this function was called!

    // reset dimensions
    _dimension.init(dimensions);

    // resize values
    _values->resize(totalSize());

    // resize names
    _resizeDimensionNames();
}

template <typename T> void TMultiDimensionalStorage<T>::reserve(const std::vector<size_t> &dimensions) {
    // ATTENTION: if values have been filled already, this is a very dangerous function!
    // you should only change the entry of the vector of dimensions -> this does not change the coordinates of the values
    // that have already been filled
    // if a more inner dimension is changed, a already filled value will have different coordinates before and after this function was called!

    // reset dimensions
    _dimension.init(dimensions);

    // reserve values
    _values->reserve(totalSize());

    // resize names
    _resizeDimensionNames();
}

template <typename T> void TMultiDimensionalStorage<T>::setDimensionName(const std::shared_ptr<TNamesEmpty> &Name, const size_t &Dim) {
    assert(Dim < _dimensionNames.size());
    _dimensionNames[Dim] = Name;
}

template <typename T> void TMultiDimensionalStorage<T>::setDimensionNames(const std::vector<std::shared_ptr<TNamesEmpty>> & Names) {
    assert(Names.size() == _dimension.numDim());
    _dimensionNames = Names;
}

template <typename T> const std::shared_ptr<TNamesEmpty> & TMultiDimensionalStorage<T>::getDimensionName(const size_t &Dim) const {
    assert(Dim < _dimensionNames.size());
    return _dimensionNames[Dim];
}

template <typename T> void TMultiDimensionalStorage<T>::fillFullDimensionName(const size_t &LinearIndex, std::vector<std::string> & Name) const {
    // get coordinate of linear index for each dimension
    std::vector<size_t> coords = _dimension.getSubscripts(LinearIndex);
    fillFullDimensionName(coords, Name);
}

template <typename T> void TMultiDimensionalStorage<T>::fillFullDimensionName(const std::vector<size_t>& Coord, std::vector<std::string> & Name) const {
    assert(_dimension.numDim() > 0);
    assert(_dimensionNames.size() == _dimension.numDim());
    assert(Coord.size() == _dimension.numDim());

    Name.clear();
    Name.resize(_dimension.numDim());

    for (size_t dim = 0; dim < _dimension.numDim(); dim++){
        Name[dim] = (*_dimensionNames[dim])[Coord[dim]];
    }
}

template <typename T> std::string TMultiDimensionalStorage<T>::getFullDimensionNameAsString(const size_t &LinearIndex, const std::string &Delimiter) const {
    // get coordinate of linear index for each dimension
    std::vector<size_t> coords = _dimension.getSubscripts(LinearIndex);
    return getFullDimensionNameAsString(coords, Delimiter);
}

template <typename T> std::string  TMultiDimensionalStorage<T>::getFullDimensionNameAsString(const std::vector<size_t> &Coord, const std::string &Delimiter) const {
    assert(_dimension.numDim() > 0);
    assert(_dimensionNames.size() == _dimension.numDim());

    std::string name = (*_dimensionNames[0])[Coord[0]];
    for (size_t dim = 1; dim < _dimension.numDim(); dim++){
        name += Delimiter + (*_dimensionNames[dim])[Coord[dim]];
    }
    return name;
}

template <typename T> void  TMultiDimensionalStorage<T>::appendToVectorOfAllFullDimensionNames(std::vector<std::string> & FullNames, const std::string &Delimiter) const {
    // fill vector fullNames with full names of all elements of storage
    assert(_dimension.numDim() > 0);
    assert(_dimensionNames.size() == _dimension.numDim());

    TRange full = getFull();
    for (size_t i = full.first; i < full.last; i += full.increment){
        FullNames.push_back(getFullDimensionNameAsString(i, Delimiter));
    }
}

template <typename T> void  TMultiDimensionalStorage<T>::appendToVectorOfAllFullDimensionNamesWithPrefix(std::vector<std::string> & FullNames, const std::string & Prefix, const std::string &Delimiter) const {
    // fill vector fullNames with full names of all elements of storage
    // add prefix + Delimiter to each full Name
    // do NOT clear vector FullNames! There might be values of other parameters already (used to write header)
    assert(_dimension.numDim() > 0);
    assert(_dimensionNames.size() == _dimension.numDim());

    TRange full = getFull();
    for (size_t i = full.first; i < full.last; i += full.increment){
        std::string nameOneElement = getFullDimensionNameAsString(i, Delimiter);
        if (nameOneElement.empty()){ // if dimension names are empty -> don't add Delimiter to Prefix
            FullNames.push_back(Prefix);
        } else {
            FullNames.push_back(Prefix + Delimiter + nameOneElement);
        }
    }
}

template <typename T> size_t TMultiDimensionalStorage<T>::totalSize() const{
    return _dimension.totalSize();
}

template <typename T> size_t TMultiDimensionalStorage<T>::numDim() const {
    return _dimension.numDim();
}

template <typename T> std::vector<size_t> TMultiDimensionalStorage<T>::dimensions() const{
    return _dimension.dimensions();
}

template <typename T> size_t TMultiDimensionalStorage<T>::getIndex(const std::vector<size_t>& coord) const{
    return _dimension.getIndex(coord);
}

template <typename T> TRange TMultiDimensionalStorage<T>::getRange(const std::vector<size_t> &startCoord, const std::vector<size_t> &endCoord) const{
    return _dimension.getRange(startCoord, endCoord);
}

template <typename T> TRange TMultiDimensionalStorage<T>::get1DSlice(size_t dim, const std::vector<size_t> &startCoord) const{
    return _dimension.get1DSlice(dim, startCoord);
}

template <typename T> TRange TMultiDimensionalStorage<T>::getFull() const{
    return _dimension.getFull();
}

template <typename T> TRange TMultiDimensionalStorage<T>::getDiagonal() const {
    return _dimension.getDiagonal();
}

template <typename T> std::vector<size_t> TMultiDimensionalStorage<T>::getSubscripts(size_t linearIndex) const {
    return _dimension.getSubscripts(linearIndex);
}

template <typename T> void TMultiDimensionalStorage<T>::initBoth(T val){
    (*_values)[0].initBoth(val);
}

template <typename T> void TMultiDimensionalStorage<T>::initBoth(const size_t &i, T val){
    (*_values)[i].initBoth(val);
}

template <typename T> void TMultiDimensionalStorage<T>::setVal(T val){
    (*_values)[0].setVal(val);
}

template <typename T> void TMultiDimensionalStorage<T>::setVal(const size_t &i, T val){
    (*_values)[i].setVal(val);
}

template <typename T> void TMultiDimensionalStorage<T>::set(T val){
    (*_values)[0].set(val);
}

template <typename T> void TMultiDimensionalStorage<T>::set(const size_t &i, T val){
    (*_values)[i].set(val);
}

template <typename T> void TMultiDimensionalStorage<T>::emplace_back(const T &value) {
    _values->emplace_back(value);
}

template <typename T> void TMultiDimensionalStorage<T>::reset() {
    (*_values)[0].reset();
}

template <typename T> void TMultiDimensionalStorage<T>::reset(const size_t &i) {
    (*_values)[i].reset();
}

template <typename T> inline T TMultiDimensionalStorage<T>::value() const {
    return (*_values)[0].value();
}

template <typename T> inline T TMultiDimensionalStorage<T>::value(const size_t &i) const {
    return (*_values)[i].value();
}

template <typename T> inline T TMultiDimensionalStorage<T>::oldValue() const {
    return (*_values)[0].oldValue();
}

template <typename T> inline T TMultiDimensionalStorage<T>::oldValue(const size_t &i) const {
    return (*_values)[i].oldValue();
}

template <typename T> inline double TMultiDimensionalStorage<T>::expValue() const {
    return (*_values)[0].expValue();
}

template <typename T> inline double TMultiDimensionalStorage<T>::expValue(const size_t &i) const {
    return (*_values)[i].expValue();
}

template <typename T> inline double TMultiDimensionalStorage<T>::oldExpValue() const {
    return (*_values)[0].oldExpValue();
}

template <typename T> inline double TMultiDimensionalStorage<T>::oldExpValue(const size_t &i) const {
    return (*_values)[i].oldExpValue();
}

template <typename T> inline double TMultiDimensionalStorage<T>::logValue() const {
    return (*_values)[0].logValue();
}

template <typename T> inline double TMultiDimensionalStorage<T>::logValue(const size_t &i) const {
    return (*_values)[i].logValue();
}

template <typename T> inline double TMultiDimensionalStorage<T>::oldLogValue() const {
    return (*_values)[0].oldLogValue();
}

template <typename T> inline double TMultiDimensionalStorage<T>::oldLogValue(const size_t &i) const {
    return (*_values)[i].oldLogValue();
}

//-------------------------------------------
// TSingleStorage
//-------------------------------------------

template <typename T> TSingleStorage<T>::TSingleStorage() = default;

template <typename T> TSingleStorage<T>::TSingleStorage(bool trackExponential, bool trackLog, bool isUpdated) {
    init({1}, trackExponential, trackLog, isUpdated);
}

template <typename T> void TSingleStorage<T>::init(const std::vector<size_t> & dimensions, bool trackExponential, bool trackLog, bool isUpdated) {
    this->_dimension.init({1});

    // init names: just with empty string
    std::vector<std::string> dimNamesVec = {""};
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesStrings>(dimNamesVec);
    this->_dimensionNames.push_back(dimNames);

    // initialize values
    if (!isUpdated){
        if (trackExponential && trackLog) {
            _value = std::make_unique<TValueFixedWithLogAndExponential<T>>();
        } else if (trackExponential){
            _value = std::make_unique<TValueFixedWithExponential<T>>();
        } else if (trackLog){
            _value = std::make_unique<TValueFixedWithLog<T>>();
        } else {
            _value = std::make_unique<TValueFixed<T>>();
        }
    } else {
        if (trackExponential && trackLog) {
            _value = std::make_unique<TValueUpdatedWithLogAndExponential<T>>();
        } else if (trackExponential) {
            _value = std::make_unique<TValueUpdatedWithExponential<T>>();
        } else if (trackLog) {
            _value = std::make_unique<TValueUpdatedWithLog<T>>();
        } else {
            _value = std::make_unique<TValueUpdated<T>>();
        }
    }
}

template <typename T> void TSingleStorage<T>::resize(const std::vector<size_t> &dimensions) {
    throw std::runtime_error("In function 'template <typename T> void TSingleStorage<T>::resize(const std::vector<size_t> &dimensions)': Can not resize TSingleStorage!");
}

template <typename T> void TSingleStorage<T>::reserve(const std::vector<size_t> &dimensions) {
    throw std::runtime_error("In function 'template <typename T> void TSingleStorage<T>::reserve(const std::vector<size_t> &dimensions)': Can not reserve TSingleStorage!");
}

template <typename T> void TSingleStorage<T>::initBoth(T val){
    _value->initBoth(val);
}

template <typename T> void TSingleStorage<T>::initBoth(const size_t &i, T val){
    _value->initBoth(val);
}

template <typename T> void TSingleStorage<T>::setVal(T val){
    _value->setVal(val);
}

template <typename T> void TSingleStorage<T>::setVal(const size_t &i, T val){
    _value->setVal(val);
}

template <typename T> void TSingleStorage<T>::set(T val){
    _value->set(val);
}

template <typename T> void TSingleStorage<T>::set(const size_t &i, T val){
    _value->set(val);
}

template <typename T> void TSingleStorage<T>::emplace_back(const T &value) {
    throw std::runtime_error("In function 'template <typename T> void TSingleStorage<T>::emplace_back(const T &value)': Can not emplace back to TSingleStorage!");
}

template <typename T> void TSingleStorage<T>::reset() {
    _value->reset();
}

template <typename T> void TSingleStorage<T>::reset(const size_t &i){
    _value->reset();
}

template <typename T> inline T TSingleStorage<T>::value() const {
    return _value->value();
}

template <typename T> inline T TSingleStorage<T>::oldValue() const {
    return _value->oldValue();
}

template <typename T> inline T TSingleStorage<T>::value(const size_t &i) const {
    return _value->value();
}

template <typename T> inline T TSingleStorage<T>::oldValue(const size_t &i) const {
    return _value->oldValue();
}

template <typename T> inline double TSingleStorage<T>::expValue() const {
    return _value->expValue();
}

template <typename T> inline double TSingleStorage<T>::expValue(const size_t &i) const {
    return _value->expValue();
}

template <typename T> inline double TSingleStorage<T>::oldExpValue() const {
    return _value->oldExpValue();
}

template <typename T> inline double TSingleStorage<T>::oldExpValue(const size_t &i) const {
    return _value->oldExpValue();
}

template <typename T> inline double TSingleStorage<T>::logValue() const {
    return _value->logValue();
}

template <typename T> inline double TSingleStorage<T>::logValue(const size_t &i) const {
    return _value->logValue();
}

template <typename T> inline double TSingleStorage<T>::oldLogValue() const {
    return _value->oldLogValue();
}

template <typename T> inline double TSingleStorage<T>::oldLogValue(const size_t &i) const {
    return _value->oldLogValue();
}
