//
// Created by madleina on 24.03.21.
//

#ifndef EMPTY_TNEWTONRAPHSON_H
#define EMPTY_TNEWTONRAPHSON_H

#include <cstdint>
#include <cmath>
#include <cassert>
#include "stringFunctions.h"
#include "TLog.h"

#ifdef WITH_ARMADILLO
#include <armadillo>
#endif

//-------------------------------
// One-Dimensional Newton-Raphson
//-------------------------------

class TNewtonRaphson {
    // this class implements two Newton-Raphson algorithms
    // the first one (runNewtonRaphson_withBisection) prevents overshooting by bisection
    // the second one (runNewtonRaphson_withBacktracking) prevents overshooting by backtracking
private:
    TLog * _logfile;
    bool _report;

    // when to stop?
    double _epsilon;
    double _maxIterations;
    size_t _counterIterations;

    // dynamically adjust tolerance to terminate?
    bool _dynamicallyAdjustTolerance;
    double _abs_F_ref;
    double _factorDynamicTolerance;

    void _checkMinMax(const double & f_min, const double & f_max, double & Min, double & Max){
        if (std::isnan(f_min) || !std::isfinite(f_min)){
            throw std::runtime_error("Newton-Raphson: Min (" + toString(Min) + ") provided as lower boundary results in invalid function values (f(Min) = " + toString(f_min) + ").");
        }
        if (std::isnan(f_max) || !std::isfinite(f_max)){
            throw std::runtime_error("Newton-Raphson: Max (" + toString(Max) + ") provided as upper boundary results in invalid function values (f(Max) = " + toString(f_max) + ").");
        }
        if ((f_min > 0. && f_max > 0.) || (f_min < 0. && f_max < 0.)){
            throw std::runtime_error("Newton-Raphson: Min (" + toString(Min) + ") and Max (" + toString(Max) + ") provided do not bracket zero (interval is " + toString(f_min) + "," + toString(f_max) + ").");
        }

        if (f_min > 0.) {
            // Turn min and max around such that d(Min) < 0
            double tmp = Min;
            Min = Max;
            Max = tmp;
        }
    }

    bool _terminate(const double & Cur_Fx) {
        double abs_Cur_Fx = std::fabs(Cur_Fx);
        // check for convergence
        bool stop = false;
        if (_dynamicallyAdjustTolerance){
            if (abs_Cur_Fx < _abs_F_ref/_factorDynamicTolerance) {
                stop = true;
            }
        } else {
            if (abs_Cur_Fx < _epsilon) {
                stop = true;
            }
        }
        if (stop){
            if (_report) {
                _logfile->list("Newton-Raphson: Found root within " + toString(_counterIterations) + " iterations. Absolute function value at end: " + toString(abs_Cur_Fx) + ".");
            }
            return true;
        }
        return false;
    }

public:
    TNewtonRaphson(){
        _logfile = nullptr;
        _report = false;

        _epsilon = 10e-10;
        _maxIterations = 1000;
        _counterIterations = 0;

        _dynamicallyAdjustTolerance = false;
        _factorDynamicTolerance = 0.;
        _abs_F_ref = 0.;
    }

    TNewtonRaphson(TLog * Logfile) : TNewtonRaphson(){
        report(Logfile);
    }

    void report(TLog * Logfile){
        _logfile = Logfile;
        _report = true;
    }

    void setMaxIterations(const size_t & MaxIterations){
        _maxIterations = MaxIterations;
    }

    void setEpsilon(const double & Epsilon){
        _epsilon = Epsilon;
    }

    void dynamicallyAdjustTolerance(const double & FactorPrecision = 1000.){
        // functions that enables Newton-Raphson to dynamically adjust the tolerance for termination
        // idea: e.g. Newton-Raphson is used within another algorithm, e.g. EM -> we want to dynamically scale epsilon
        // depending on how "far" we are in the EM, i.e. don't be precise inside NR in the beginning and be precise in the end
        // as next iteration of EM will anyways change values

        // Newton-Raphson will compare the current function value f_x with a reference function value f_ref
        // where f_ref is e.g. the function value of the previous EM iteration
        // if those are very close -> i.e. we are already close to the previous estimate of the root,
        //   we want to have a low tolerance, i.e. we want to be very precise
        // if these are different -> i.e. the current estimate of the root is very different than the previous one
        //   we don't want to be too precise, and have a relatively high tolerance
        _dynamicallyAdjustTolerance = true;
        _factorDynamicTolerance = FactorPrecision;
    }

    const size_t & getCounterIterations() const {
        return _counterIterations;
    }

    template<class Object, class Function, class Derivative>
    double runNewtonRaphson_withBisection(Object & Obj, Function & F, Derivative & D, double Min, double Max) {
        // No starting value given -> make initial guess
        double start = 0.5 * (Min + Max);

        return runNewtonRaphson_withBisection(Obj, F, D, start, Min, Max);
    }

    template<class Object, class Function, class Derivative>
    double runNewtonRaphson_withBisection(Object & Obj, Function & F, Derivative & D, double Start, double Min, double Max) {
        // Returns the root of a function F using a combination of Newton-Raphson and bisection
        // Min and Max = initial boundaries of the function (for bisection)
        // root will be refined until the accuracy is known within +- Epsilon
        // Parameter F is the function where we want to find the root of (for MLE problems: this would be the first derivative)
        // Parameter D is the first derivative of F (for MLE problems: this would be the second derivative)
        // Parameter Obj is the class instance that contains functions F and D

        // if dynamically adjust tolerance: calculate function at _x_ref
        if (_dynamicallyAdjustTolerance){
            _abs_F_ref = std::fabs((Obj.*F)(Start));
        }

        // check min and max
        double f_min = (Obj.*F)(Min);
        double f_max = (Obj.*F)(Max);
        _checkMinMax(f_min, f_max, Min, Max);
        if (f_min == 0.){
            if (_report){
                _logfile->list("Newton-Raphson with bisection: Found root within 0 iterations, root is exactly Min.");
            }
            return Min;
        }
        if (f_max == 0.){
            if (_report){
                _logfile->list("Newton-Raphson with bisection: Found root within 0 iterations, root is exactly Min.");
            }
            return Max;
        }

        double x = Start;

        // Initial step size
        double step = std::fabs(Max - Min);
        double previousStep = step;

        double f_x = (Obj.*F)(x);
        double d_x = (Obj.*D)(x);

        for (_counterIterations = 0; _counterIterations < _maxIterations; _counterIterations++) { // loop
            if ((((x - Max) * d_x - f_x) * ((x - Min) * d_x - f_x) > 0.) || // out of range
                    (std::fabs(2. * f_x) > std::fabs(previousStep * d_x))) { // not decreasing fast enough
                previousStep = step;
                step = 0.5 * (Max - Min);
                x = Min + step;
            } else { // step is accepted
                previousStep = step;
                step = f_x / d_x;
                x -= step;
            }
            if (_terminate(f_x)){
                return x;
            }

            // calculate function at new x
            f_x = (Obj.*F)(x);
            d_x = (Obj.*D)(x);

            if (f_x < 0.) { // maintain bracket around zero
                Min = x;
            } else {
                Max = x;
            }
        }
        if (_report) {
            _logfile->warning("Newton-Raphson with bisection: Failed to find root, reached maximum number of iterations (" + toString(_maxIterations) + "). Absolute function value end: " + toString(std::fabs(f_x)) + ".");
        }
        return x;
    }

    template<class Object, class Function, class Derivative>
    double runNewtonRaphson_withBacktracking(Object & Obj, Function & F, Derivative & D, const double & Start) {
        // Returns the root of a function F using a combination of Newton-Raphson and backtracking
        // root will be refined until the accuracy is known within +- Epsilon
        // Parameter F is the function where we want to find the root of (for MLE problems: this would be the first derivative)
        // Parameter D is the first derivative of F (for MLE problems: this would be the second derivative)
        // Parameter Obj is the class instance that contains functions F and D

        // if dynamically adjust tolerance: calculate function at _x_ref
        if (_dynamicallyAdjustTolerance){
            _abs_F_ref = std::fabs((Obj.*F)(Start));
        }

        double x = Start;
        double oldX = x;
        double f_x = 0.;

        for (_counterIterations = 0; _counterIterations < _maxIterations; _counterIterations++) {
            f_x = (Obj.*F)(x);
            double d_x = (Obj.*D)(x);
            double step = f_x / d_x;
            oldX = x;
            x -= step;

            double factor = 0.5;
            while(std::fabs((Obj.*F)(x)) > std::fabs(f_x)){
                // backtracking
                x = oldX - step * factor;
                factor /= 2.;
            }

            if (_terminate(f_x)){
                return x;
            }
        }
        if (_report) {
            _logfile->warning("Newton-Raphson with backtracking: Failed to find root, reached maximum number of iterations (" + toString(_maxIterations) + "). Absolute function value at end: " + toString(std::fabs(f_x)) + ".");
        }
        return x;
    }
};


//---------------------------------
// Multi-Dimensional Newton-Raphson
//---------------------------------

#ifdef WITH_ARMADILLO
class TMultiDimensionalNewtonRaphson {
    // this class implements a multidimensional globally-convergent Newton-Raphson algorithms
    // by preventing overshooting with backtracking
    // code adapted from Numerical Receipes, chapter 9.6 (page 479)
private:
    // report to logfile
    TLog * _logfile;
    bool _report;

    // when to stop?
    double _toleranceF;
    double _toleranceFMin;
    double _maxStepSize;
    double _convergenceThresholdDeltaX;
    size_t _maxIterations;
    size_t _counterIterations;

    // dynamically adjust tolerance to terminate?
    bool _dynamicallyAdjustTolerance;
    double _max_absF_ref;
    double _factorDynamicTolerance;

    void _restrictInitialStepSize(const size_t & N, arma::vec & Direction, const double & MaxStepSize){
        // calculate vector norm of Direction -> sum corresponds to step size in NR
        assert(Direction.size() == N);
        double sum = 0.;
        for (size_t i = 0; i < N; i++){
            sum += Direction[i] * Direction[i];
        }
        sum = sqrt(sum);

        // check if step is too big
        if (sum > MaxStepSize) {
            for (size_t i = 0; i < N; i++) {
                Direction[i] *= MaxStepSize / sum; // scale if attempted step is too big
            }
        }
    }

    double _calculateSlope(const size_t & N, const std::vector<double> & D_Old, const arma::vec & Direction){
        double slope = 0.;
        for (size_t i = 0; i < N; i++) {
            slope += D_Old[i] * Direction[i];
        }
        if (slope >= 0.){
            throw std::runtime_error("In function 'TMultiDimensionalNewtonRaphson::_calculateSlope': Roundoff problem while backtracking, slope (" + toString(slope) + ") is >= 0.");
        }
        return slope;
    }

    double _calculateMinLambda(const size_t & N, const arma::vec & Direction, const std::vector<double> & X_Old){
        // compute lambda_min
        double test = 0.;
        for (size_t i = 0; i < N; i++) {
            double temp = std::fabs(Direction[i]) / std::max(std::fabs(X_Old[i]), 1.);
            if (temp > test){
                test = temp;
            }
        }

        return std::numeric_limits<double>::epsilon() / test;
    }

    template<class Object, class Function>
    double _calculate_f(Object & Obj, Function & F, const std::vector<double> & X, std::vector<double> & ValuesF) {
        // Calculates the function value at X as f = 1/2 FF
        // and stores F(x) in ValuesF
        double sum = 0.;
        ValuesF = (Obj.*F)(X);
        for (size_t i = 0; i < X.size(); i++) {
            sum += ValuesF[i]*ValuesF[i];
        }
        return 0.5 * sum;
    }

    void _makeStep(const size_t & N, std::vector<double> & X_New, const std::vector<double> & X_Old, const double & Lambda, const arma::vec & Direction) {
        for (size_t i = 0; i < N; i++) {
            X_New[i] = X_Old[i] + Lambda * Direction[i];
        }
    }

    bool _backTracking_convergedOnDeltaX(const size_t & N, const double & Lambda, const double & MinLambda, std::vector<double> & X_New, const std::vector<double> & X_Old, bool & ConvergedOnDeltaX){
        if (Lambda < MinLambda) {
            // convergence on delta x. For zero finding, the calling program should verify the convergence
            for (size_t i = 0; i < N; i++) {
                X_New[i] = X_Old[i];
            }
            ConvergedOnDeltaX = true;
            return true;
        }
        return false;
    }

    bool _backTracking_convergedOnFunctionValue(const double & F_New, const double & F_Old, const double & Lambda, const double & Slope, const double & DecreaseFactorF){
        return F_New <= F_Old + DecreaseFactorF * Lambda * Slope;
    }

    double _backTrack_oneStep(const double & F_New, const double & F_Old, const double & Lambda, const double & Slope, const double & PreviousF, const double & PreviousLambda){
        double tempLambda;
        if (Lambda == 1.) {
            tempLambda = -Slope / (2. * (F_New - F_Old - Slope)); // first time
        }
        else { // subsequent backtracks
            double rhs1 = F_New - F_Old - Lambda * Slope;
            double rhs2 = PreviousF - F_Old - PreviousLambda * Slope;
            double a = (rhs1/(Lambda * Lambda) - rhs2/(PreviousLambda * PreviousLambda)) / (Lambda - PreviousLambda);
            double b = (-PreviousLambda * rhs1 / (Lambda * Lambda) + Lambda * rhs2 / (PreviousLambda * PreviousLambda)) / (Lambda - PreviousLambda);
            if (a == 0.){
                tempLambda = -Slope / (2. * b);
            }
            else {
                double disc = b * b-3. * a * Slope;
                if (disc < 0.){
                    tempLambda = 0.5 * Lambda;
                }
                else if (b <= 0.){
                    tempLambda = (-b + sqrt(disc)) / (3. * a);
                }
                else {
                    tempLambda = -Slope / (b + sqrt(disc));
                }
            }
            if (tempLambda > 0.5 * Lambda) {
                tempLambda = 0.5 * Lambda; // lambda <= 0.5*lambda_1
            }
        }
        return tempLambda;
    }

    template<class Object, class Function>
    void _backTrack(Object & Obj, Function & F,
                    const std::vector<double> & X_Old, const double & F_Old, const std::vector<double> & Gradient,
                    arma::vec & Direction, std::vector<double> & ValuesF,
                    std::vector<double> & X_New, double &F_New, const double & MaxStepSize, bool &ConvergedOnDeltaX) {
        /*
         * Perform backtracking in the multidimensional Newton-Raphson algorithm
         *
         * @param Obj and F: Newton-Raphson object and function
         * @param X_Old: A n-dimensional point
         * @param F_Old: Value of function F at point X_Old
         * @param Gradient: Gradient at point X_Old
         * @param Direction: Newton direction
         *
         * @param X_New: A new n-dimensional point along the direction p from X_Old where the function F has decreased sufficiently
         * @param F_New: Value of function F at point X_New
         *
         * @param MaxStepSize: Limit the length of the steps so that you do not try to evaluate the function in regions where it is undefined or subject to overflow
         * @param ConvergedOnDeltaX: False on a normal exit. True when X_New is too close to X_Old.
         *                   In a minimization algorithm, this usually signals convergence and can be ignored.
         *                   However, in a zero-finding algorithm the calling program should check whether
         *                   the convergence is spurious.
         */

        // settings
        const double decreaseFactorF = 1.e-4; // ensures sufficient decrease in function value
        size_t n = X_Old.size();
        ConvergedOnDeltaX = false;

        // restrict initial step size
        _restrictInitialStepSize(n, Direction, MaxStepSize);

        // calculate slope
        double slope = _calculateSlope(n, Gradient, Direction);

        // calculate minimal lambda
        double minLambda = _calculateMinLambda(n, Direction, X_Old);

        double lambda = 1.; // Always try full Newton step first
        double tempLambda = 0., previousLambda = 0., previousF = 0.;
        for (;;) { // start of loop
            // make step and get X_New
            _makeStep(n, X_New, X_Old, lambda, Direction);
            F_New = _calculate_f(Obj, F, X_New, ValuesF);

            if (_backTracking_convergedOnDeltaX(n, lambda, minLambda, X_New, X_Old, ConvergedOnDeltaX)){
                return;
            } else if (_backTracking_convergedOnFunctionValue(F_New, F_Old, lambda, slope, decreaseFactorF)) {
                return; // sufficient function decrease
            } else { // backtrack
                tempLambda = _backTrack_oneStep(F_New, F_Old, lambda, slope, previousF, previousLambda);
            }

            previousLambda = lambda;
            previousF = F_New;
            lambda = std::max(tempLambda, 0.1 * lambda); // lambda >= 0.1*lambda_1
        } // try again
    }

    double _adjustMaxStepSize(const size_t & N, const std::vector<double> & X, const double & MaxStepSize){
        double sum = 0.;
        for (size_t i = 0; i < N; i++){  // Calculate maxStepSize for line search
            sum += X[i]*X[i];
        }
        return MaxStepSize * std::max(sqrt(sum), static_cast<double>(N));
    }

    void _fillGradient(const size_t & N, const arma::mat & Jacobian, const std::vector<double> & ValuesF, std::vector<double> & Gradient){
        for (size_t i = 0; i < N; i++) {
            // Compute gradient of f for line search
            double sum = 0.;
            for (size_t j = 0; j < N; j++){
                sum += Jacobian(j, i) * ValuesF[j];
            }
            Gradient[i] = sum;
        }
    }

    void _fillDirection(const size_t & N, const std::vector<double> & ValuesF, arma::vec & Direction){
        for (size_t i = 0; i < N; i++){
            Direction[i] = -ValuesF[i]; // Right-hand side for linear equations
        }
    }

    double _getMaxAbsF(const size_t & N, const std::vector<double> & ValuesF){
        double maxAbsF = 0.;
        for (size_t i = 0; i < N; i++) {
            if (std::fabs(ValuesF[i]) > maxAbsF){
                maxAbsF = std::fabs(ValuesF[i]);
            }
        }
        return maxAbsF;
    }

    bool _convergedOnFunctionValues(const size_t & N, const std::vector<double> & ValuesF, const double & Tolerance, bool & ConvergedOnDeltaX){
        // Test for initial guess being a root. Use more stringent test than simply toleranceF
        double maxAbsF = _getMaxAbsF(N, ValuesF);

        bool stop = false;
        if (_dynamicallyAdjustTolerance && maxAbsF < _max_absF_ref/_factorDynamicTolerance){
            stop = true;
        } else if (!_dynamicallyAdjustTolerance && maxAbsF < Tolerance) {
            stop = true;
        }

        if (stop){
            ConvergedOnDeltaX = false;
            if (_report) {
                _logfile->list("Multidimensional Newton-Raphson: Found root within " + toString(_counterIterations) + " iterations, terminated based on convergence of function values. Maximum absolute function value at end: " + toString(maxAbsF) + ".");
            }
            return true;
        }
        return false;
    }

    void _checkForSpuriousConvergence(const size_t & N, const double & f, const std::vector<double> & Gradient, const std::vector<double> & X, const double & ToleranceFMin, bool & ConvergedOnDeltaX, const std::vector<double> & ValuesF){
        // Check for gradient of f zero, i.e., spurious convergence.
        double test = 0.;
        double den = std::max(f, 0.5*static_cast<double>(N));
        for (size_t i = 0; i < N; i++) {
            double temp = std::fabs(Gradient[i]) * std::max(std::fabs(X[i]), 1.) / den;
            if (temp > test) {
                test = temp;
            }
        }
        ConvergedOnDeltaX = (test < ToleranceFMin);

        if (_report) {
            double maxAbsF = _getMaxAbsF(N, ValuesF);
            _logfile->list("Multidimensional Newton-Raphson: Found root within " + toString(_counterIterations) + " iterations, terminated based on spurious convergence (gradient of f is ~zero). Absolute function value at end: " + toString(maxAbsF) + ".");
        }
    }

    bool _convergedOnDeltaX(const size_t & N, const std::vector<double> & X, const std::vector<double> & X_Old, const double & ConvergenceThresholdDeltaX, const std::vector<double> & ValuesF){
        double test = 0.; // test for convergence on deltaX
        for (size_t i = 0; i < N; i++) {
            double temp = (std::fabs(X[i] - X_Old[i])) / std::max(std::fabs(X[i]), 1.);
            if (temp > test){
                test = temp;
            }
        }
        if (test < ConvergenceThresholdDeltaX){
            if (_report) {
                double maxAbsF = _getMaxAbsF(N, ValuesF);
                _logfile->list("Multidimensional Newton-Raphson: Found root within " + toString(_counterIterations) + " iterations, terminated based on convergence of deltaX (step size). Absolute function value at end: " + toString(maxAbsF) + ".");
            }
            return true;
        }
        return false;
    }

public:
    TMultiDimensionalNewtonRaphson(){
        _logfile = nullptr;
        _report = false;

        _toleranceF = 1.e-8; // convergence criterion on function values
        _toleranceFMin = 1.e-12; // criterion for deciding whether spurious convergence to a minimum of fmin has occurred
        _maxStepSize = 100.; // scaled maximum step length allowed in line searches
        _convergenceThresholdDeltaX = std::numeric_limits<double>::epsilon(); // convergence criterion on delta x

        _maxIterations = 200;
        _counterIterations = 0;

        _dynamicallyAdjustTolerance = false;
        _factorDynamicTolerance = 0.;
        _max_absF_ref = 0.;
    }

    TMultiDimensionalNewtonRaphson(TLog * Logfile) : TMultiDimensionalNewtonRaphson(){
        report(Logfile);
    }

    void report(TLog * Logfile){
        _logfile = Logfile;
        _report = true;
    }

    void setMaxIterations(const size_t & MaxIterations){
        _maxIterations = MaxIterations;
    }

    void setToleranceF(const double & ToleranceF){
        _toleranceF = ToleranceF;
    }

    void setToleranceFMin(const double & ToleranceFMin){
        _toleranceFMin = ToleranceFMin;
    }

    void dynamicallyAdjustTolerance(const double & FactorPrecision = 1000.){
        // functions that enables Newton-Raphson to dynamically adjust the tolerance for termination
        // idea: e.g. Newton-Raphson is used within another algorithm, e.g. EM -> we want to dynamically scale epsilon
        // depending on how "far" we are in the EM, i.e. don't be precise inside NR in the beginning and be precise in the end
        // as next iteration of EM will anyways change values

        // Newton-Raphson will compare the current function value f_x with a reference function value f_ref
        // where f_ref is e.g. the function value of the previous EM iteration
        // if those are very close -> i.e. we are already close to the previous estimate of the root,
        //   we want to have a low tolerance, i.e. we want to be very precise
        // if these are different -> i.e. the current estimate of the root is very different than the previous one
        //   we don't want to be too precise, and have a relatively high tolerance
        _dynamicallyAdjustTolerance = true;
        _factorDynamicTolerance = FactorPrecision;
    }

    const size_t & getCounterIterations() const {
        return _counterIterations;
    }

    template<class Object_Function, class Function, class JacobianObject, class JacobianFunction>
    void runNewtonRaphson_withBacktracking(Object_Function & Obj, Function & F, JacobianObject & JacobianObj, JacobianFunction & JacobianF, std::vector<double> & X, bool & ConvergedOnDeltaX) {
        /*
         * Run a multidimensional Newton-Raphson algorithm (globally convergent)
         *
         * @tparam Obj is the class instance that contains function F
         * @tparam F is the function where we want to find the root of (for MLE problems: this would be the vector of first derivatives)
         *           -> takes as argument std::vector<double> and returns std::vector<double>
         * @tparam JacobianObject is the class instance that contains function JacobianFunction (not necessarily the same as Obj -> if it is the same, use overloaded function below)
         * @tparam JacobianFunction is the function that calculates the partial derivatives of F (for MLE problems: this would return the matrix of second derivatives)
         *           -> takes as argument two std::vector<double> (X and ValuesF) and returns arma::mat
         *
         * @param X: A n-dimensional point, representing an initial guess for a root
         * @param ConvergedOnDeltaX: False on a normal exit. True if the routine has converged to a local minimum
         * of the function. In this case try restarting from a different initial guess.
         */

        // prepare storage
        size_t n = X.size();
        arma::mat jacobian(n, n);
        arma::vec direction(n);
        std::vector<double> gradient(n);
        std::vector<double> xOld(n);
        std::vector<double> valuesF;

        // Calculate initial f
        double f = _calculate_f(Obj, F, X, valuesF);
        if (_dynamicallyAdjustTolerance){
            _max_absF_ref = _getMaxAbsF(n, valuesF);
        }

        // Test for initial guess being a root; use more stringent test than simply toleranceF
        if (_convergedOnFunctionValues(n, valuesF, 0.01 * _toleranceF, ConvergedOnDeltaX)){
            return;
        }

        // adjust maxStepSize for line search
        _maxStepSize = _adjustMaxStepSize(n, X, _maxStepSize);

        for (_counterIterations = 0; _counterIterations < _maxIterations; _counterIterations++) { // Start of iteration loop
            // fill Jacobian, gradient and direction
            jacobian = (JacobianObj.*JacobianF)(X, valuesF);
            _fillGradient(n, jacobian, valuesF, gradient);
            _fillDirection(n, valuesF, direction);

            // store x and f
            for (size_t i = 0; i < n; i++) {
                xOld[i] = X[i];
            }
            double fOld = f;

            // solve linear equations
            direction = arma::solve(jacobian, direction);

            // do line search (backtracking) to get new x and f
            _backTrack(Obj, F, xOld, fOld, gradient, direction, valuesF, X, f, _maxStepSize, ConvergedOnDeltaX);

            // Check for convergence
            if (_convergedOnFunctionValues(n, valuesF, _toleranceF, ConvergedOnDeltaX)){
                // 1) test for convergence on function values
                return;
            } else if (ConvergedOnDeltaX) {
                // 2) line search reported spurious convergence -> check again and return
                _checkForSpuriousConvergence(n, f, gradient, X, _toleranceFMin, ConvergedOnDeltaX, valuesF);
                return;
            } else if (_convergedOnDeltaX(n, X, xOld, _convergenceThresholdDeltaX, valuesF)){
                // 3) test for convergence on deltaX
                return;
            }
        }
        if (_report) {
            _logfile->warning("Multidimensional Newton-Raphson with backtracking: Failed to find root, reached maximum number of iterations (" + toString(_maxIterations) + "). Absolute function value at end: " + toString(std::fabs(f)) + ".");
        }
    }

    template<class Object, class Function, class JacobianFunction>
    void runNewtonRaphson_withBacktracking(Object & Obj, Function & F, JacobianFunction & JacobianF, std::vector<double> & X, bool & ConvergedOnDeltaX) {
        // overloaded function where Obj defines both F and JacobianF
        return runNewtonRaphson_withBacktracking(Obj, F, Obj, JacobianF, X, ConvergedOnDeltaX);
    }
};

template<class Object, class Function>
class TApproxJacobian {
    /*
     * If Jacobian matrix can not be calculated analytically,
     * this class routine will attempt to compute the necessary partial derivatives of Function F
     * numerically by finite differences.
     *
     * Algorithm adapted Numerical Recipes, Chapter 9.7.2 (Globally Convergent Newton Method) and Chapter 5.7 (Numerical derivatives)
     *
     * @tparam Obj is the class instance that contains functions F
     * @tparam F are the functions where we want to find the root of (for MLE problems: this would be the first derivative)
     */
private:
    const double _epsilon = 1.e-8; // Set to approximate square root of the machine precision
    Object & _obj;
    Function & _function;

public:
    TApproxJacobian(Object & Obj, Function & F) : _obj(Obj), _function(F) {
        // Initialize with user-supplied function that returns the vector of functions to be zeroed.
    }

    arma::mat approximateJacobian(const std::vector<double> & X, const std::vector<double> & ValuesF) {
        /* Calculates the Jacobian array by finite differences
         *
         * @param X: A n-dimensional point at which the Jacobian is to be evaluated
         * @param ValuesF: vector of function values at X
         */
        size_t n = X.size();
        arma::mat jacobian(n, n);
        std::vector<double> xh = X;
        for (size_t j = 0; j < n; j++) {
            double temp = xh[j];
            double h = _epsilon * std::fabs(temp);
            if (h == 0.){
                h = _epsilon;
            }
            xh[j] = temp + h; // trick to reduce finite-precision error
            h = xh[j] - temp;
            std::vector<double> f = (_obj.*_function)(xh);
            xh[j] = temp;
            for (size_t i = 0; i < n; i++) { // forward difference formula
                jacobian(i, j) = (f[i] - ValuesF[i]) / h;
            }
        }
        return jacobian;
    }
};

#else
// armadillo is not defined -> will throw an error, because mutlidimensional Newton-Raphson requires armadillo
class TMultiDimensionalNewtonRaphson {
public:
    TMultiDimensionalNewtonRaphson() {
        throw "The library 'armadillo' has not been found but is required to use the mutlidimensional Newton-Raphson algorithm!";
    };

    TMultiDimensionalNewtonRaphson(TLog * Logfile) {
        throw "The library 'armadillo' has not been found but is required to use the mutlidimensional Newton-Raphson algorithm!";
    }
};

#endif // WITH_ARMADILLO

#endif //EMPTY_TNEWTONRAPHSON_H
