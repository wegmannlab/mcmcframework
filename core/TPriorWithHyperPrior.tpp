//-------------------------------------------
// TPriorWithHyperPrior
//-------------------------------------------
template <class T> void TPriorWithHyperPrior<T>::_checkSizeParams(int actualSize, int expectedSize){
    if (actualSize != expectedSize)
        throw std::runtime_error("Size of TMCMCParameter vector (" + toString(actualSize) + ") does not match the number of parameters of a " + this->_name + " distribution (" + toString(expectedSize) + ")!");
}

template <class T> void TPriorWithHyperPrior<T>::_checkSizeObservations(int actualSize, int expectedSize){
    if (actualSize != expectedSize)
        throw std::runtime_error("Size of observation vector (" + toString(actualSize) + ") does not match the expected number of observations for a " + this->_name + " distribution (" + toString(expectedSize) + ")!");
}

template <class T> void TPriorWithHyperPrior<T>::_checkTypesOfPriorParameters(const std::shared_ptr<TMCMCObservationsBase> & priorParam, bool throwIfNot, bool mustBeFloatingPoint, bool mustBeIntegral, bool mustBeUnsigned) const {
    // go over all parameters on which the prior is defined
    // and check if they are of a compatible type (e.g. normal prior only supports floating point numbers)
    if (throwIfNot) { // possibility ignore the check
        std::string errorString = "In function 'void TPriorWithHyperPrior<T>::_checkTypesOfPriorParameters(const std::shared_ptr<TMCMCParameterBase> & priorParam, bool throwIfNot, bool mustBeFloatingPoint, bool mustBeIntegral, bool mustBeUnsigned) const': prior parameter " + priorParam->name() + " of a " + this->_name + " prior distribution has a non-compatible type. ";
        if (mustBeFloatingPoint && !priorParam->isFloatingPoint()) {
            throw std::runtime_error(errorString + "Type must be floating point.");
        } if (mustBeIntegral && !priorParam->isIntegral()) {
            throw std::runtime_error(errorString + "Type must be integral.");
        } if (mustBeUnsigned && !priorParam->isUnsigned()) {
            throw std::runtime_error(errorString + "Type must be unsigned.");
        }
    }
}

//-------------------------------------------
// TPriorNormalWithHyperPrior
//-------------------------------------------

template <typename T> TPriorNormalWithHyperPrior<T>::TPriorNormalWithHyperPrior() : TPriorWithHyperPrior<T>(), TPriorNormal<T>() {
    this->_twoVar = _oneDivTwo_log_var = 0.;
}

template <typename T> void TPriorNormalWithHyperPrior<T>::initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, std::vector<std::shared_ptr<TObservationsBase> > & observations){
    this->_checkSizeParams(params.size(), 2);
    this->_checkSizeObservations(observations.size(), 0);
    _mean = params.at(0);
    _var = params.at(1);

    _checkMinMaxOfHyperPrior();
}

template <typename T> void TPriorNormalWithHyperPrior<T>::_updateTempVals(){
    // need to be updated whenever var has changed
    this->_twoVar = 2. * _var->value<double>();
    _oneDivTwo_log_var = 0.5 * log(_var->value<double>());
}

template <typename T> void TPriorNormalWithHyperPrior<T>::_checkMinMaxOfHyperPrior(){
    // idea: parameter _var should always be > 0 -> re-set boundaries of _var
    _var->reSetBoundaries(false, true, "0.", toString(std::numeric_limits<double>::max()), false, true);
}

template <class T> void TPriorNormalWithHyperPrior<T>::constructDAG(TDAG & DAG, TDAG & temporaryDAG){
    this->_constructDAG(DAG, temporaryDAG, {_mean, _var});
}

template <class T> void TPriorNormalWithHyperPrior<T>::typesAreCompatible(bool throwIfNot) {
    // parameters on which normal prior is defined must be floating point type
    this->_checkTypes(throwIfNot, true, false, false);

    // in addition: check if prior parameters have expected type
    // mean and var must be floating point
    this->_checkTypesOfPriorParameters(_mean, throwIfNot, true, false, false);
    this->_checkTypesOfPriorParameters(_var, throwIfNot, true, false, false);

    // check if the prior distribution on the prior parameters is ok
    _mean->typesAreCompatible(throwIfNot);
    _var->typesAreCompatible(throwIfNot);
}

template <class T> void TPriorNormalWithHyperPrior<T>::initializeStorageOfPriorParameters() {
    _mean->initializeStorageSingleElementBasedOnPrior();
    _var->initializeStorageSingleElementBasedOnPrior();

    _updateTempVals();
}

template <typename T> double TPriorNormalWithHyperPrior<T>::_getLogPriorDensity_oneVal(T x){
    // note: expensive to calculate, call getLogPriorRatio() if ever possible
    return TNormalDistr::density(x, _mean->value<double>(), sqrt(_var->value<double>()), true);
}

template <typename T> double TPriorNormalWithHyperPrior<T>::_getLogPriorRatio_oneVal(T x, T x_old){
    double tmp = (x - _mean->value<double>());
    double tmp_old = (x_old - _mean->value<double>());
    return (tmp_old * tmp_old - tmp * tmp) / this->_twoVar;
}

template <typename T> void TPriorNormalWithHyperPrior<T>::updateParams(){
    if (_mean->update())
        _updateMean();
    if (_var->update()) {
        _updateVar();
        _updateTempVals();
    }
}

template <typename T> double TPriorNormalWithHyperPrior<T>::mean() const {
    return _mean->value<double>();
}

template <typename T> double TPriorNormalWithHyperPrior<T>::var() const {
    return _var->value<double>();
}

template <typename T> double TPriorNormalWithHyperPrior<T>::_calcLLUpdateMean(){
    // compute log likelihood
    double LL = 0.;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange range = paramToUse->getFull();
        double tmp = 0.;
        for (size_t i = range.first; i < range.last; i += range.increment) {
            double tmpOld = paramToUse->value<T>(i) - _mean->oldValue<double>();
            double tmpNew = paramToUse->value<T>(i) - _mean->value<double>();
            tmp += tmpOld * tmpOld - tmpNew * tmpNew;
        }
        LL += tmp / this->_twoVar;
    }
    return LL;
}

template <typename T> double TPriorNormalWithHyperPrior<T>::_calcLogHUpdateMean(){
    // compute log likelihood
    double LL = _calcLLUpdateMean();
    // compute prior
    double logPrior = _mean->getLogPriorRatio();
    return LL + logPrior;
}

template <typename T> void TPriorNormalWithHyperPrior<T>::_updateMean(){
    // compute log(Hastings ratio)
    double logH = _calcLogHUpdateMean();
    // accept or reject
    _mean->acceptOrReject(logH);
}

template <typename T> double TPriorNormalWithHyperPrior<T>::_calcLLUpdateVar() {
    // compute log likelihood
    double LL = 0.;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange range = paramToUse->getFull();
        double sum = 0.;
        for (size_t i = range.first; i < range.last; i += range.increment) {
            double tmp = paramToUse->value<T>(i) - _mean->value<double>();
            sum += tmp * tmp;
        }
        LL += 0.5* paramToUse->totalSize() * log(_var->oldValue<double>() / _var->value<double>()) + 0.5 * (1. / _var->oldValue<double>() - 1. / _var->value<double>()) * sum;
    }
    return LL;
}

template <typename T> double TPriorNormalWithHyperPrior<T>::_calcLogHUpdateVar() {
    // compute LL
    double LL = _calcLLUpdateVar();
    // compute prior
    double logPrior = _var->getLogPriorRatio();
    // compute log(Hastings ratio)
    return LL + logPrior;
}

template <typename T> void TPriorNormalWithHyperPrior<T>::_updateVar(){
    // compute log(Hastings ratio)
    double logH = _calcLogHUpdateVar();
    // accept or reject
    _var->acceptOrReject(logH);
}

template <class T> double TPriorNormalWithHyperPrior<T>::getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data) {
    // returns sum of log-density of array + log density of mean + log density of var
    return this->getSumLogPriorDensity(data) + _mean->getLogPriorDensity() + _var->getLogPriorDensity();
}

template <class T> void TPriorNormalWithHyperPrior<T>::estimateInitialPriorParameters(TLog * Logfile) {
    if (!_mean->hasFixedInitialValue()) {
        _setMeanToMLE();
    }
    if (!_var->hasFixedInitialValue()) {
        _setVarToMLE();
    }
}

template <class T> void TPriorNormalWithHyperPrior<T>::_setMeanToMLE() {
    // calculate MLE of mean
    double innerSum = 0.;
    double totalNumElements = 0;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange range = paramToUse->getFull();
        for (size_t i = range.first; i < range.last; i += range.increment) {
            innerSum += paramToUse->value<T>(i);
        }
        totalNumElements += paramToUse->totalSize();
    }
    double mean = innerSum/totalNumElements;

    // now set mean to MLE
    _mean->set(mean);
}

template <class T> void TPriorNormalWithHyperPrior<T>::_setVarToMLE() {
    // calculate MLE of var
    double innerSum = 0.;
    double totalNumElements = 0;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange range = paramToUse->getFull();
        for (size_t i = range.first; i < range.last; i += range.increment) {
            double tmp = paramToUse->value<T>(i) - _mean->value<double>();
            innerSum += tmp * tmp;
        }
        totalNumElements += paramToUse->totalSize();
    }

    // now set var to MLE
    _var->set(innerSum / totalNumElements);
    _updateTempVals();
}

template <class T> void TPriorNormalWithHyperPrior<T>::simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) {
    // first update temporary values: mean and var might have changed in meantime
    _updateTempVals();

    // sample random normal values
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        if (!paramToUse->hasFixedInitialValue()) { // only simulate if parameter has not a fixed value
            TRange range = paramToUse->getFull();
            for (size_t i = range.first; i < range.last; i += range.increment) {
                double value = RandomGenerator->getNormalRandom(_mean->value<double>(), sqrt(_var->value<double>()));
                while (!paramToUse->valueIsInsideBoundary(value)) { // propose again if value is too large, to prevent overshooting while mirroring
                    value = RandomGenerator->getNormalRandom(_mean->value<double>(), sqrt(_var->value<double>()));
                }
                // set value
                paramToUse->set(i, value);
            }
        }
    }
}

//-------------------------------------------
// TPriorExponentialWithHyperPrior
//-------------------------------------------

template <typename T> TPriorExponentialWithHyperPrior<T>::TPriorExponentialWithHyperPrior() : TPriorWithHyperPrior<T>(), TPriorExponential<T>() {}

template <typename T> void TPriorExponentialWithHyperPrior<T>::initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, std::vector<std::shared_ptr<TObservationsBase> > & observations){
    this->_checkSizeParams(params.size(), 1);
    this->_checkSizeObservations(observations.size(), 0);
    _lambda = params.at(0);

    _checkMinMaxOfHyperPrior();
}

template <typename T> void TPriorExponentialWithHyperPrior<T>::_checkMinMaxOfHyperPrior(){
    // idea: parameter _lambda should always be > 0 -> re-set boundaries of _lambda
    _lambda->reSetBoundaries(false, true, "0.", toString(std::numeric_limits<double>::max()), false, true);
}

template <class T> void TPriorExponentialWithHyperPrior<T>::constructDAG(TDAG & DAG, TDAG & temporaryDAG){
    this->_constructDAG(DAG, temporaryDAG, {_lambda});
}

template <class T> void TPriorExponentialWithHyperPrior<T>::typesAreCompatible(bool throwIfNot) {
    // parameters on which exponential prior is defined must be floating point type
    this->_checkTypes(throwIfNot, true, false, false);

    // in addition: check if prior parameters have expected type
    // _lambda must be floating point
    this->_checkTypesOfPriorParameters(_lambda, throwIfNot, true, false, false);

    // check if the prior distribution on the prior parameters is ok
    _lambda->typesAreCompatible(throwIfNot);
}

template <class T> void TPriorExponentialWithHyperPrior<T>::initializeStorageOfPriorParameters() {
    _lambda->initializeStorageSingleElementBasedOnPrior();

    _updateTempVals();
}

template <typename T> void TPriorExponentialWithHyperPrior<T>::_updateTempVals(){
    // need to be updated whenever lambda has changed
    this->_logLambda = log(_lambda->value<double>());
}

template <typename T> double TPriorExponentialWithHyperPrior<T>::_getLogPriorDensity_oneVal(T x){
    // note: expensive to calculate, call getLogPriorRatio() if ever possible
    return TExponentialDistr::density(x, _lambda->value<double>(), true);
}

template <typename T> double TPriorExponentialWithHyperPrior<T>::_getLogPriorRatio_oneVal(T x, T x_old){
    return _lambda->value<double>() * (x_old - x);
}

template <typename T> void TPriorExponentialWithHyperPrior<T>::updateParams(){
    if (_lambda->update()) {
        _updateLambda();
        _updateTempVals();
    }
}

template <typename T> double TPriorExponentialWithHyperPrior<T>::_calcLLUpdateLambda() {
    // compute log likelihood
    double LL = 0.;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange range = paramToUse->getFull();
        double sum = 0.;
        for (size_t i = range.first; i < range.last; i += range.increment) {
            sum += paramToUse->value<T>(i);
        }
        LL += log(_lambda->value<double>() / _lambda->oldValue<double>()) * paramToUse->totalSize() + (_lambda->oldValue<double>() - _lambda->value<double>()) * sum;
    }
    return LL;
}

template <typename T> double TPriorExponentialWithHyperPrior<T>::_calcLogHUpdateLambda() {
    // compute log likelihood
    double LL = _calcLLUpdateLambda();
    // compute _prior
    double logPrior = _lambda->getLogPriorRatio();
    // compute log(Hastings ratio)
    return LL + logPrior;
}

template <typename T> void TPriorExponentialWithHyperPrior<T>::_updateLambda(){
    // compute log(Hastings ratio)
    double logH = _calcLogHUpdateLambda();
    // accept or reject
    _lambda->acceptOrReject(logH);
}

template <typename T> double TPriorExponentialWithHyperPrior<T>::lambda() const {
    return _lambda->value<double>();
}

template <class T> double TPriorExponentialWithHyperPrior<T>::getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data) {
    // returns sum of log-density of array + log density of lambda
    return this->getSumLogPriorDensity(data) + _lambda->getLogPriorDensity();
}

template <typename T> void TPriorExponentialWithHyperPrior<T>::_setLambdaToMLE() {
    // compute log likelihood
    double sum = 0.;
    double totalNumElements = 0.;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange range = paramToUse->getFull();
        for (size_t i = range.first; i < range.last; i += range.increment) {
            sum += paramToUse->value<T>(i);
        }
        totalNumElements += paramToUse->totalSize();
    }
    if (sum == 0.){ // avoid division by zero
        sum += 0.01;
    }

    double lambda = totalNumElements / sum;

    _lambda->set(lambda);
    _updateTempVals();
}

template <typename T> void TPriorExponentialWithHyperPrior<T>::estimateInitialPriorParameters(TLog * Logfile) {
    if (!_lambda->hasFixedInitialValue()) {
        _setLambdaToMLE();
    }
}

template <class T> void TPriorExponentialWithHyperPrior<T>::simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) {
    // first update temporary values: lambda might have changed in the meantime
    _updateTempVals();

    // sample random exponential values
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        if (!paramToUse->hasFixedInitialValue()) { // only simulate if parameter has not a fixed value
            TRange range = paramToUse->getFull();
            for (size_t i = range.first; i < range.last; i += range.increment) {
                double value = RandomGenerator->getExponentialRandom(_lambda->value<double>());
                while (!paramToUse->valueIsInsideBoundary(value)) { // propose again if value is too large, to prevent overshooting while mirroring
                    value = RandomGenerator->getExponentialRandom(_lambda->value<double>());
                }
                // set value
                paramToUse->set(i, value);
            }
        }
    }
}

//-------------------------------------------
// TPriorBetaWithHyperPrior
//-------------------------------------------

template <typename T> TPriorBetaWithHyperPrior<T>::TPriorBetaWithHyperPrior() : TPriorWithHyperPrior<T>(), TPriorBeta<T>() {}

template <typename T> void TPriorBetaWithHyperPrior<T>::initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, std::vector<std::shared_ptr<TObservationsBase> > & observations){
    this->_checkSizeParams(params.size(), 2);
    this->_checkSizeObservations(observations.size(), 0);
    _alpha = params.at(0);
    _beta = params.at(1);

    _checkMinMaxOfHyperPrior();
}

template <typename T> void TPriorBetaWithHyperPrior<T>::_checkMinMaxOfHyperPrior(){
    // idea: parameters _alpha and _beta should always be > 0 (for log) -> re-set boundaries.
    _alpha->reSetBoundaries(false, true, "0.", toString(std::numeric_limits<double>::max()), false, true);
    _beta->reSetBoundaries(false, true, "0.", toString(std::numeric_limits<double>::max()), false, true);
}

template <class T> void TPriorBetaWithHyperPrior<T>::constructDAG(TDAG & DAG, TDAG & temporaryDAG){
    this->_constructDAG(DAG, temporaryDAG, {_alpha, _beta});
}

template <class T> void TPriorBetaWithHyperPrior<T>::typesAreCompatible(bool throwIfNot) {
    // parameters on which beta prior is defined must be floating point type
    this->_checkTypes(throwIfNot, true, false, false);

    // in addition: check if prior parameters have expected type
    // _alpha and _beta must be floating point
    this->_checkTypesOfPriorParameters(_alpha, throwIfNot, true, false, false);
    this->_checkTypesOfPriorParameters(_beta, throwIfNot, true, false, false);

    // check if the prior distribution on the prior parameters is ok
    _alpha->typesAreCompatible(throwIfNot);
    _beta->typesAreCompatible(throwIfNot);
}

template <class T> void TPriorBetaWithHyperPrior<T>::initializeStorageOfPriorParameters() {
    _alpha->initializeStorageSingleElementBasedOnPrior();
    _beta->initializeStorageSingleElementBasedOnPrior();

    _updateTempVals();
}

template <typename T> void TPriorBetaWithHyperPrior<T>::_updateTempVals(){
    // need to be updated whenever alpha or beta have changed
    this->_alphaMin1 = _alpha->value<double>() - 1.;
    this->_betaMin1 = _beta->value<double>() - 1.;
}

template <typename T> double TPriorBetaWithHyperPrior<T>::_getLogPriorDensity_oneVal(T x){
    // note: I also calculate the normalization term 1/Beta(alpha, beta), but it is expensive to calculate.
    // If ever possible, directly call getLogPriorRatio()
    return TBetaDistr::density(x, _alpha->value<double>(), _beta->value<double>(), true);
}

template <typename T> double TPriorBetaWithHyperPrior<T>::_getLogPriorRatio_oneVal(T x, T x_old){
    return this->_alphaMin1 * (log(x) - log(x_old)) + this->_betaMin1 * (log(1.-x) - log(1.-x_old));
}

template <typename T> void TPriorBetaWithHyperPrior<T>::updateParams(){
    if (_alpha->update()) {
        _updateAlpha();
        _updateTempVals();
    }
    if (_beta->update()) {
        _updateBeta();
        _updateTempVals();
    }
}

template <typename T> double TPriorBetaWithHyperPrior<T>::_calcLLUpdateAlpha() {
    // compute log likelihood
    double LL = 0.;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange range = paramToUse->getFull();
        double sum = 0.;
        for (size_t i = range.first; i < range.last; i += range.increment) {
            sum += log(paramToUse->value<T>(i));
        }
        LL += paramToUse->totalSize() * (gammaLog(_alpha->value<double>() + _beta->value<double>()) + gammaLog(_alpha->oldValue<double>()) -
               gammaLog(_alpha->oldValue<double>() + _beta->value<double>()) - gammaLog(_alpha->value<double>())) +
                       (_alpha->value<double>() - _alpha->oldValue<double>()) * sum;
    }
    return LL;
}

template <typename T> double TPriorBetaWithHyperPrior<T>::_calcLogHUpdateAlpha() {
    // compute log likelihood
    double LL = _calcLLUpdateAlpha();
    // compute prior
    double logPrior = _alpha->getLogPriorRatio();
    // compute log(Hastings ratio)
    return LL + logPrior;
}

template <typename T> void TPriorBetaWithHyperPrior<T>::_updateAlpha(){
    // compute log(Hastings ratio)
    double logH = _calcLogHUpdateAlpha();
    // accept or reject
    _alpha->acceptOrReject(logH);
}

template <typename T> double TPriorBetaWithHyperPrior<T>::_calcLLUpdateBeta() {
    // compute log likelihood
    double LL = 0.;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange range = paramToUse->getFull();
        double sum = 0.;
        for (size_t i = range.first; i < range.last; i += range.increment) {
            sum += log(1. - paramToUse->value<T>(i));
        }
        LL += paramToUse->totalSize() * (gammaLog(_alpha->value<double>() + _beta->value<double>()) + gammaLog(_beta->oldValue<double>()) -
                                            gammaLog(_alpha->value<double>() + _beta->oldValue<double>()) - gammaLog(_beta->value<double>())) +
                                                    (_beta->value<double>() - _beta->oldValue<double>()) * sum;
    }
    return LL;
}

template <typename T> double TPriorBetaWithHyperPrior<T>::_calcLogHUpdateBeta() {
    // compute log likelihood
    double LL = _calcLLUpdateBeta();
    // compute prior
    double logPrior = _beta->getLogPriorRatio();
    // compute log(Hastings ratio)
    return LL + logPrior;
}

template <typename T> void TPriorBetaWithHyperPrior<T>::_updateBeta(){
    // compute log(Hastings ratio)
    double logH = _calcLogHUpdateBeta();
    // accept or reject
    _beta->acceptOrReject(logH);
}

template <typename T> double TPriorBetaWithHyperPrior<T>::alpha() const {
    return _alpha->value<double>();
}

template <typename T> double TPriorBetaWithHyperPrior<T>::beta() const {
    return _beta->value<double>();
}

template <class T> double TPriorBetaWithHyperPrior<T>::getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data) {
    // returns sum of log-density of array + log density of alpha + log density of beta
    return this->getSumLogPriorDensity(data) + _alpha->getLogPriorDensity() + _beta->getLogPriorDensity();
}

template <typename T> void TPriorBetaWithHyperPrior<T>::_calculateMeanAndVar(double &mean, double &var) {
    // calculate mean and variance of all parameters
    TMeanVar<T> meanVar;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange range = paramToUse->getFull();
        for (size_t i = range.first; i < range.last; i += range.increment) {
            meanVar.add(paramToUse->value<T>(i));
        }
    }

    // fill
    mean = meanVar.mean();
    var = meanVar.variance();
}

template <typename T> void TPriorBetaWithHyperPrior<T>::_setAlphaToMOM(const double &mean, const double &nu) {
    _alpha->set(mean * nu);
}

template <typename T> void TPriorBetaWithHyperPrior<T>::_setBetaToMOM(const double &mean, const double &nu) {
    _beta->set((1. - mean) * nu);
}

template <typename T> void TPriorBetaWithHyperPrior<T>::estimateInitialPriorParameters(TLog * Logfile){
    double mean;
    double var;
    _calculateMeanAndVar(mean, var);

    // calculate nu
    double nu = (mean * (1.-mean)) / var - 1.;
    if (nu <= 0.){
        // invalid nu -> re-set such that alpha = beta = 0.5 result
        nu = 1.;
        mean = 0.5;
    }

    if (!_alpha->hasFixedInitialValue()) {
        _setAlphaToMOM(mean, nu);
    }
    if (!_beta->hasFixedInitialValue()) {
        _setBetaToMOM(mean, nu);
    }
    _updateTempVals();
}

template <class T> void TPriorBetaWithHyperPrior<T>::simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) {
    // update temporary values: alpha or beta might have changed in the meantime
    _updateTempVals();

    // sample random beta values
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        if (!paramToUse->hasFixedInitialValue()) { // only simulate if parameter has not a fixed value
            TRange range = paramToUse->getFull();
            for (size_t i = range.first; i < range.last; i += range.increment) {
                double value = RandomGenerator->getBetaRandom(_alpha->value<double>(), _beta->value<double>());
                while (!paramToUse->valueIsInsideBoundary(value)) { // propose again if value is too large, to prevent overshooting while mirroring
                    value = RandomGenerator->getBetaRandom(_alpha->value<double>(), _beta->value<double>());
                }
                // set value
                paramToUse->set(i, value);
            }
        }
    }
}

//-------------------------------------------
// TPriorBinomialWithHyperPrior
//-------------------------------------------

template <typename T> TPriorBinomialWithHyperPrior<T>::TPriorBinomialWithHyperPrior() : TPriorWithHyperPrior<T>(), TPriorBinomial<T>() {}

template <typename T> void TPriorBinomialWithHyperPrior<T>::initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, std::vector<std::shared_ptr<TObservationsBase> > & observations){
    // expects 1 parameter p
    this->_checkSizeParams(params.size(), 1);
    this->_checkSizeObservations(observations.size(), 0);
    _paramP = params.at(0);

    _checkMinMaxOfHyperPrior();
}

template <typename T> void TPriorBinomialWithHyperPrior<T>::_checkMinMaxOfHyperPrior(){
    // idea: parameter _p should always be > 0 and < 1 -> re-set boundaries of _paramP
    _paramP->reSetBoundaries(false, false, "0.", "1.", false, false);
}

template <class T> bool TPriorBinomialWithHyperPrior<T>::checkMinMax(std::string &min, std::string &max, bool &minIsIncluded, bool &maxIsIncluded, bool & hasDefaultMin, bool & hasDefaultMax) {
    // k of binomial distribution must be k = {0, 1, ..., n}
    // n is not known at this point -> just check if > 0
    bool didNotChange = true;
    if (convertString<T>(min) < 0.) {
        hasDefaultMin = false;
        min = "0";
        minIsIncluded = true;
        didNotChange = false;
    }
    return didNotChange;
}

template <class T> void TPriorBinomialWithHyperPrior<T>::constructDAG(TDAG & DAG, TDAG & temporaryDAG){
    this->_constructDAG(DAG, temporaryDAG, {_paramP});
}

template <class T> void TPriorBinomialWithHyperPrior<T>::typesAreCompatible(bool throwIfNot) {
    // parameters on which binomial prior is defined must be unsigned integer
    this->_checkTypes(throwIfNot, false, true, true);

    // in addition: check if prior parameters have expected type
    // _p must be floating point
    this->_checkTypesOfPriorParameters(_paramP, throwIfNot, true, false, false);
    // check if the prior distribution on the prior parameters is ok
    _paramP->typesAreCompatible(throwIfNot);
}

template <class T> void TPriorBinomialWithHyperPrior<T>::initializeStorageOfPriorParameters() {
    _paramP->initializeStorageSingleElementBasedOnPrior();
    _updateTempVals();

    // check if dimensions of parameter below are as expected
    TPriorBinomial<T>::initializeStorageOfPriorParameters();
}

template <typename T> void TPriorBinomialWithHyperPrior<T>::_updateTempVals(){
    // need to be updated whenever p has changed
    this->_log_p = log(_paramP->value<double>());
    this->_log_q = log(1.-_paramP->value<double>());
    this->_p = _paramP->value<double>();
}

template <typename T> void TPriorBinomialWithHyperPrior<T>::updateParams(){
    if (_paramP->update()) {
        _updateP();
        _updateTempVals();
    }
}

template <typename T> double TPriorBinomialWithHyperPrior<T>::_calcLLUpdateP() {
    // compute log likelihood
    double LL = 0.;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange range = paramToUse->get1DSlice(0, {0,0});
        double sumK = 0.;
        double sumNMinusK = 0.;
        for (size_t i = range.first; i < range.last; i += range.increment) {
            T n = paramToUse->value<T>(i);
            T k = paramToUse->value<T>(i+1);
            sumK += k;
            sumNMinusK += n-k;
        }
        LL += log(_paramP->value<double>() / _paramP->oldValue<double>()) * sumK +
              log((1.-_paramP->value<double>())/(1.-_paramP->oldValue<double>())) * sumNMinusK;
    }
    return LL;
}

template <typename T> double TPriorBinomialWithHyperPrior<T>::_calcLogHUpdateP() {
    // compute log likelihood
    double LL = _calcLLUpdateP();
    // compute _prior
    double logPrior = _paramP->getLogPriorRatio();
    // compute log(Hastings ratio)
    return LL + logPrior;
}

template <typename T> void TPriorBinomialWithHyperPrior<T>::_updateP() {
    // compute log(Hastings ratio)
    double logH = _calcLogHUpdateP();
    // accept or reject
    _paramP->acceptOrReject(logH);
}

template <typename T> double TPriorBinomialWithHyperPrior<T>::p() const {
    return _paramP->value<double>();
}

template <class T> double TPriorBinomialWithHyperPrior<T>::getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data) {
    // returns sum of log-density of array + log density of p
    return this->getSumLogPriorDensity(data) + _paramP->getLogPriorDensity();
}

template <class T> void TPriorBinomialWithHyperPrior<T>::_setPToMLE() {
    double sumK = 0.;
    double sumN = 0.;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange range = paramToUse->get1DSlice(0, {0,0});
        for (size_t i = range.first; i < range.last; i += range.increment) {
            sumN += paramToUse->value<T>(i);
            sumK += paramToUse->value<T>(i+1);
        }
    }

    // avoid p=0 and p=1 (to respect boundaries of p)
    if (sumK == 0.){
        sumK++;
    } else if (sumK == sumN){
        sumK--;
    }
    _paramP->set(sumK / sumN);
    _updateTempVals();
}

template <class T> void TPriorBinomialWithHyperPrior<T>::estimateInitialPriorParameters(TLog * Logfile) {
    if (!_paramP->hasFixedInitialValue()) {
        _setPToMLE();
    }
}

template <class T> void TPriorBinomialWithHyperPrior<T>::simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) {
    // first update temporary values: p might have changed in the meantime
    _updateTempVals();
    TPriorBinomial<T>::simulateUnderPrior(RandomGenerator, Logfile, Parameters);
}

//-------------------------------------------
// TPriorBernouilliWithHyperPrior
//-------------------------------------------

template <typename T> TPriorBernouilliWithHyperPrior<T>::TPriorBernouilliWithHyperPrior() : TPriorWithHyperPrior<T>(), TPriorBernouilli<T>(), TEMPriorIID_base<double, size_t, size_t>(2) {
    _EMSum = 0.;
    _EMTotal = 0;
    _ranEM = false;
}

template <typename T> void TPriorBernouilliWithHyperPrior<T>::initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, std::vector<std::shared_ptr<TObservationsBase> > & observations){
    this->_checkSizeParams(params.size(), 1);
    this->_checkSizeObservations(observations.size(), 0);
    _pi = params.at(0);

    _checkMinMaxOfHyperPrior();
}

template <typename T> void TPriorBernouilliWithHyperPrior<T>::_checkMinMaxOfHyperPrior(){
    // idea: parameter _pi should always be > 0 and < 1 -> re-set boundaries of _pi
    _pi->reSetBoundaries(false, false, "0.", "1.", false, false);
}

template <class T> void TPriorBernouilliWithHyperPrior<T>::constructDAG(TDAG & DAG, TDAG & temporaryDAG){
    this->_constructDAG(DAG, temporaryDAG, {_pi});
}

template <class T> void TPriorBernouilliWithHyperPrior<T>::typesAreCompatible(bool throwIfNot) {
    // parameters on which bernouilli prior is defined must be unsigned integer (actually 0 or 1, but this is ensured by checkMinMax())
    this->_checkTypes(throwIfNot, false, true, true);

    // in addition: check if prior parameters have expected type
    // _pi must be floating point
    this->_checkTypesOfPriorParameters(_pi, throwIfNot, true, false, false);

    // check if the prior distribution on the prior parameters is ok
    _pi->typesAreCompatible(throwIfNot);
}

template <class T> void TPriorBernouilliWithHyperPrior<T>::initializeStorageOfPriorParameters() {
    _pi->initializeStorageSingleElementBasedOnPrior();

    _updateTempVals();
}

template <typename T> void TPriorBernouilliWithHyperPrior<T>::_updateTempVals(){
    // need to be updated whenever lambda has changed
    this->_logPi = log(_pi->value<double>());
    this->_log1MinPi = log(1.-_pi->value<double>());
}

template <typename T> double TPriorBernouilliWithHyperPrior<T>::_getLogPriorDensity_oneVal(T x){
    // note: expensive to calculate, call getLogPriorRatio() if ever possible
    return TBernouilliDistr::density(x, _pi->value<double>(), true);
}

template <typename T> double TPriorBernouilliWithHyperPrior<T>::_getLogPriorRatio_oneVal(T x, T x_old){
    return (x-x_old)*this->_logPi + (x_old-x)*this->_log1MinPi;
}

template <typename T> void TPriorBernouilliWithHyperPrior<T>::updateParams(){
    if (_pi->update()) {
        _updatePi();
        _updateTempVals();
    }
}

template <typename T> double TPriorBernouilliWithHyperPrior<T>::_calcLLUpdatePi() {
    // compute log likelihood
    double LL = 0.;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange range = paramToUse->getFull();
        size_t counter0 = 0;
        size_t counter1 = 0;
        for (size_t i = range.first; i < range.last; i += range.increment) {
            if (paramToUse->value<size_t>(i) == 1){
                counter1++;
            } else {
                counter0++;
            }
        }
        LL += log(_pi->value<double>() / _pi->oldValue<double>()) * counter1 +
             log((1.-_pi->value<double>())/(1.-_pi->oldValue<double>())) * counter0;
    }
    return LL;
}

template <typename T> double TPriorBernouilliWithHyperPrior<T>::_calcLogHUpdatePi() {
    // compute log likelihood
    double LL = _calcLLUpdatePi();
    // compute _prior
    double logPrior = _pi->getLogPriorRatio();
    // compute log(Hastings ratio)
    return LL + logPrior;
}

template <typename T> void TPriorBernouilliWithHyperPrior<T>::_updatePi() {
    // compute log(Hastings ratio)
    double logH = _calcLogHUpdatePi();
    // accept or reject
    _pi->acceptOrReject(logH);
}

template <typename T> double TPriorBernouilliWithHyperPrior<T>::pi() const {
    return _pi->value<double>();
}

template <class T> double TPriorBernouilliWithHyperPrior<T>::getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data) {
    // returns sum of log-density of array + log density of pi
    return this->getSumLogPriorDensity(data) + _pi->getLogPriorDensity();
}

template <class T> void TPriorBernouilliWithHyperPrior<T>::_setPiToMLE() {
    double counter1 = 0;
    double numElementsTotal = 0;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange range = paramToUse->getFull();
        for (size_t i = range.first; i < range.last; i += range.increment) {
            if (paramToUse->value<size_t>(i) == 1){
                counter1++;
            }
        }
        numElementsTotal += paramToUse->totalSize();
    }

    // set pi (but prevent pi == 0 and pi == 1)
    if (counter1 == 0){
        _pi->set(1. / numElementsTotal);
    } else if (counter1 == numElementsTotal){
        _pi->set((numElementsTotal-1.) / numElementsTotal);
    } else {
        _pi->set(counter1 / numElementsTotal);
    }
    _updateTempVals();
}

template <class T> void TPriorBernouilliWithHyperPrior<T>::estimateInitialPriorParameters(TLog * Logfile) {
    if (!_pi->hasFixedInitialValue() && !_ranEM) { // only set pi to MLE if it was not set with EM previously
        _setPiToMLE();
    }
}

template <class T> void TPriorBernouilliWithHyperPrior<T>::runEMEstimation(TLatentVariable<double, size_t, size_t> & latentVariable, TLog * Logfile){
    // only allow for 1 parameter (no shared priors)
    // reason: EM. z below passes pointer to itself to this prior, which then runs EM. If there are multiple z,
    // we would need to find a way to have pointers to both and run the EM on them simultaneously -> not so easy
    if (this->_parameters.size() > 1){
        throw std::runtime_error("In function 'template <class T> void TPriorBernouilliWithHyperPrior<T>::runEMEstimation(TLatentVariable<double, size_t, size_t> & latentVariable, TLog * Logfile)': Can not run EM estimation of prior " + this->_name + " for " + toString(this->_parameters.size()) + " parameters. Currently only implemented for 1 parameter.");
    }

    // update transition matrices in EM?
    TEM<double, size_t, size_t > em(*this, latentVariable, 100, 0.0001, true);
    em.report(Logfile);

    // get total length
    std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(this->_parameters[0]);
    size_t length = paramToUse->totalSize();
    std::vector<size_t> vecLength = {length};

    em.runEM(vecLength);

    // to initialize z
    em.estimateStatePosteriors(vecLength);

    _ranEM = true;
}

template <class T> const double TPriorBernouilliWithHyperPrior<T>::operator[](const size_t & State) const{
    return TBernouilliDistr::density(State, _pi->value<double>());
};

template<typename T> void TPriorBernouilliWithHyperPrior<T>::handleEMParameterInitialization(const size_t & Index, const size_t & State){
    if (State == 1){
        _EMSum++;
    }
    _EMTotal++;
}

template<typename T> void TPriorBernouilliWithHyperPrior<T>::finalizeEMParameterInitialization(){
    if (!_pi->hasFixedInitialValue()){
        // set pi (but prevent pi == 0 and pi == 1)
        if (_EMSum == 0.){
            _pi->set(1. / _EMTotal);
        } else if (_EMSum == _EMTotal){
            _pi->set((_EMTotal-1.) / _EMTotal);
        } else {
            _pi->set(_EMSum / _EMTotal);
        }
    }
}

template<typename T> void TPriorBernouilliWithHyperPrior<T>::prepareEMParameterEstimationInitial(){
    prepareEMParameterEstimationOneIteration();
}

template<typename T> void TPriorBernouilliWithHyperPrior<T>::prepareEMParameterEstimationOneIteration(){
    _EMSum = 0.;
    _EMTotal= 0;
}

template<typename T> void TPriorBernouilliWithHyperPrior<T>::handleEMParameterEstimationOneIteration(const size_t & Index, const TDataVector<double, size_t> & weights){
    _EMSum += weights[1];
    _EMTotal++;
}

template<typename T> void TPriorBernouilliWithHyperPrior<T>::finalizeEMParameterEstimationOneIteration(){
    finalizeEMParameterInitialization();
}

template<typename T> void TPriorBernouilliWithHyperPrior<T>::finalizeEMParameterEstimationFinal(){
    // no need to clean
}

template<typename T> void TPriorBernouilliWithHyperPrior<T>::switchPriorClassificationAfterEM(TLog * Logfile) {
    // classification of components by EM is random, but sometimes we have certain restrictions (e.g. normal mixed model with 2 components, shared mean but different variances)
    // we then might have to switch the EM labels and all associated prior parameters

    // set to opposite
    if (!_pi->hasFixedInitialValue()) {
        _pi->set(1. - _pi->value<double>());
        _updateTempVals();
    }
}

template <class T> void TPriorBernouilliWithHyperPrior<T>::simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) {
    // first update temporary values: pi might have changed in the meantime
    _updateTempVals();

    // sample random bernouilli values
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        if (!paramToUse->hasFixedInitialValue()) { // only simulate if parameter has not a fixed value
            TRange range = paramToUse->getFull();
            for (size_t i = range.first; i < range.last; i += range.increment) {
                double value = RandomGenerator->getBinomialRand(_pi->value<double>(), 1);
                while (!paramToUse->valueIsInsideBoundary(value)) { // propose again if value is too large, to prevent overshooting while mirroring
                    value = RandomGenerator->getBinomialRand(_pi->value<double>(), 1);
                }
                // set value
                paramToUse->set(i, value);
            }
        }
    }
}

//-------------------------------------------
// TPriorCategoricalWithHyperPrior
//-------------------------------------------

template <typename T> TPriorCategoricalWithHyperPrior<T>::TPriorCategoricalWithHyperPrior() : TPriorWithHyperPrior<T>(), TEMPriorIID_base<double, size_t, size_t>() {
    this->_name = MCMCPrior::categorical;
    _K = 0;
    _EMTotal = 0;
    _ranEM = false;
}

template <typename T> void TPriorCategoricalWithHyperPrior<T>::initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, std::vector<std::shared_ptr<TObservationsBase> > & observations){
    this->_checkSizeParams(params.size(), 1);
    this->_checkSizeObservations(observations.size(), 0);
    _pis = params.at(0);

    _checkMinMaxOfHyperPrior();
}

template <class T> void TPriorCategoricalWithHyperPrior<T>::constructDAG(TDAG & DAG, TDAG & temporaryDAG){
    this->_constructDAG(DAG, temporaryDAG, {_pis});
}

template <class T> void TPriorCategoricalWithHyperPrior<T>::initializeStorageOfPriorParameters() {
    // pis must have size K -> K corresponds to maximum value plus one of x
    // because x = {0, K-1}
    std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(this->_parameters[0]);
    double max = paramToUse->max();
    for (auto & param : this->_parameters) {
        paramToUse = this->_getSharedPtrParam(param);
        if (paramToUse->max() != max){
            throw std::runtime_error("In function 'template <class T> void TPriorCategoricalWithHyperPrior<T>::initializeStorageOfPriorParameters()': Parameter " + paramToUse->name() + " has a different maximum value than other shared parameters, which implies a different prior!");
        }
    }

    // pi will have indices as dimension names (results in pi_1, pi_2, pi_3, ...)
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    _pis->initializeStorageBasedOnPrior({static_cast<size_t>(max + 1)}, {dimNames});

    // now set variables
    _K = _pis->totalSize();
    this->_numStates = _K;

    // make sure pi is not exactly 0 or 1 (not allowed for log)
    for (size_t j = 0; j < _K; j++) {
        if (_pis->value<double>(j) == 0.){
            _pis->set(j, 0.0000001);
        }
    }

    _normalizePis(false);
}

template <typename T> void TPriorCategoricalWithHyperPrior<T>::_normalizePis(bool onlyReSetNewValue, const size_t & k){
    // calculate sum and check if all pi sum to 1
    // bool onlyReSetNewValue: if we update one pi_k, we need to normalize all pi in order to sum to one. This is achieved by
    // the function set(normalizedValue). However, the pi_k which we just updated must be re-set using the function setVal(normalizedValue),
    // because this function only re-sets the value (and leaves the old value as it is)
    // If we didn't do this distinction, oldValue would be unnormalized and value would be normalized version of pi_k -> not what we want!
    double sum = 0.;
    for (size_t j = 0; j < _K; j++) {
        sum += _pis->value<double>(j);
    }
    if (sum != 1.){ // normalize
        for (size_t j = 0; j < _K; j++) {
            double newVal = _pis->value<double>(j) / sum;
            if (onlyReSetNewValue && j == k){
                _pis->setVal(j, newVal); // set only value, don't modify old_value, because we don't want to compare unnormalized to normalized in hastings ratio
            } else {
                _pis->set(j, newVal);
            }
        }
    }
}

template <typename T> void TPriorCategoricalWithHyperPrior<T>::_checkMinMaxOfHyperPrior(){
    // idea: parameter _pi should always be > 0 -> re-set boundaries of _pi
    _pis->reSetBoundaries(false, false, "0.", "1.", false, true);
}

template <class T> bool TPriorCategoricalWithHyperPrior<T>::checkMinMax(std::string &min, std::string &max, bool & minIsIncluded, bool & maxIsIncluded, bool & hasDefaultMin, bool & hasDefaultMax) {
    // parameter on which categorical distribution is defined must be x = {0, K-1}
    // developer must specify the maximum (K-1) of the parameter, in order for us to infer the expected size of pis
    if (hasDefaultMax){
        throw std::runtime_error("In function 'template <class T> bool TPriorCategoricalWithHyperPrior<T>::checkMinMax(std::string &min, std::string &max, bool & minIsIncluded, bool & maxIsIncluded, bool & hasDefaultMin, bool & hasDefaultMax)': need to set max for parameter with categorical distribution! This is required in order to infer K, the dimensionality of the prior.");
    }
    bool didNotChange = true;
    if (convertString<T>(min) < 0) {
        hasDefaultMin = false;
        min = "0";
        minIsIncluded = true;
        didNotChange = false;
    }
    return didNotChange;
}

template <class T> void TPriorCategoricalWithHyperPrior<T>::typesAreCompatible(bool throwIfNot) {
    // parameters on which categorical prior is defined must be unsigned integer
    this->_checkTypes(throwIfNot, false, true, true);

    // in addition: check if prior parameters have expected type
    // _pi must be floating point
    this->_checkTypesOfPriorParameters(_pis, throwIfNot, true, false, false);

    // check if the prior distribution on the prior parameters is ok
    _pis->typesAreCompatible(throwIfNot);
}

template <typename T> double TPriorCategoricalWithHyperPrior<T>::_getLogPriorDensity_oneVal(T x){
    // assert value of x is valid (between 0 and K-1)
    assert(x >= 0 && x < _K);

    // P(x=k | pis) = pis[k]
    return log(_pis->value<double>(x));
}

template <typename T> double TPriorCategoricalWithHyperPrior<T>::_getLogPriorRatio_oneVal(T x, T x_old){
    // assert value of x is valid (between 0 and K-1)
    assert(x >= 0 && x < _K);
    assert(x_old >= 0 && x_old < _K);

    // P(x=k | pis) = pis[k]
    return log(_pis->value<double>(x)/_pis->value<double>(x_old));
}

template <typename T> void TPriorCategoricalWithHyperPrior<T>::updateParams(){
    for (size_t k = 0; k < _K; k++) {
        if (_pis->update(k)) {
            // we update one pi[k] after another, but sum of all pi must be one -> always co-update other pi's
            _updatePi(k);
        }
    }
}

template <typename T> double TPriorCategoricalWithHyperPrior<T>::_calcLLUpdatePi() {
    // compute log likelihood
    double LL = 0.;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange range = paramToUse->getFull();

        std::vector<size_t > counters(_K, 0);
        for (size_t i = range.first; i < range.last; i += range.increment) {
            size_t k = paramToUse->value<size_t>(i);
            counters[k]++;
        }

        double tmp = 0.;
        for (size_t k = 0; k < _K; k++){
            tmp += counters[k] * log(_pis->value<double>(k) / _pis->oldValue<double>(k));
        }
        LL += tmp;
    }
    return LL;
}

template <typename T> double TPriorCategoricalWithHyperPrior<T>::_calcLogHUpdatePi() {
    // compute log likelihood
    double LL = _calcLLUpdatePi();
    // compute _prior (of only one pis, as this will take into account all other pi's as well)
    double logPrior = _pis->getLogPriorRatio(0); // TODO: ? WHAT IF WE DON'T USE A DIRICHLET PRIOR?
    // compute log(Hastings ratio)
    return LL + logPrior;
}

template <typename T> void TPriorCategoricalWithHyperPrior<T>::_updatePi(const size_t & k) {
    // first normalize all pi such that pis to sum to 1
    _normalizePis(true, k);

    // compute log(Hastings ratio)
    double logH = _calcLogHUpdatePi();
    // accept or reject
    if (!_pis->acceptOrReject(k, logH)){ // rejected -> reset all other pi's as well
        for (size_t j = 0; j < _K; j++) {
            if (j == k){ // skip the pi that is updated
                continue;
            }
            _pis->reset(j);
        }
    }
}

template <typename T> double TPriorCategoricalWithHyperPrior<T>::pi(const size_t & k) const {
    return _pis->value<double>(k);
}

template <class T> double TPriorCategoricalWithHyperPrior<T>::getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data) {
    // returns sum of log-density of array + log density of pi
    return this->getSumLogPriorDensity(data) + _pis->getLogPriorDensityFull();
}

template <class T> void TPriorCategoricalWithHyperPrior<T>::_setPisToMLE() {
    std::vector<double> counter(_K, 0);
    double numElementsTotal = 0;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange range = paramToUse->getFull();
        for (size_t i = range.first; i < range.last; i += range.increment) {
            size_t k = paramToUse->value<size_t>(i);
            counter[k]++;
        }
        numElementsTotal += paramToUse->totalSize();
    }

    for (size_t k = 0; k < _K; k++) {
        double val = counter[k] / numElementsTotal;
        if (val == 0.){ // not allowed because of log
            val = 0.00001;
        }
        _pis->set(k, val);
    }
    // normalize again
    _normalizePis(false);
}

template <class T> void TPriorCategoricalWithHyperPrior<T>::estimateInitialPriorParameters(TLog * Logfile) {
    if (!_pis->hasFixedInitialValue() && !_ranEM) { // only initialize pis to MLE if no EM was run before
        _setPisToMLE();
    }
}

template <class T> void TPriorCategoricalWithHyperPrior<T>::runEMEstimation(TLatentVariable<double, size_t, size_t> & latentVariable, TLog * Logfile){
    // only allow for 1 parameter (no shared priors)
    // reason: EM. z below passes pointer to itself to this prior, which then runs EM. If there are multiple z,
    // we would need to find a way to have pointers to both and run the EM on them simultaneously -> not so easy
    if (this->_parameters.size() > 1){
        throw std::runtime_error("In function 'template <class T> void TPriorCategoricalWithHyperPrior<T>::runEMEstimation(TLatentVariable<double, size_t, size_t> & latentVariable, TLog * Logfile)': Can not run EM estimation of prior " + this->_name + " for " + toString(this->_parameters.size()) + " parameters. Currently only implemented for 1 parameter.");
    }

    // update transition matrices in EM?
    TEM<double, size_t, size_t > em(*this, latentVariable, 100, 0.0001, true);
    em.report(Logfile);

    // get total length
    std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(this->_parameters[0]);
    size_t length = paramToUse->totalSize();
    std::vector<size_t> vecLength = {length};

    em.runEM(vecLength);

    // to initialize z
    em.estimateStatePosteriors(vecLength);

    _ranEM = true;
}

template <class T> const double TPriorCategoricalWithHyperPrior<T>::operator[](const size_t & State) const{
    return _pis->value<double>(State);
};

template<typename T> void TPriorCategoricalWithHyperPrior<T>::handleEMParameterInitialization(const size_t & Index, const size_t & State){
    _EMSums[State]++;
    _EMTotal++;
}

template<typename T> void TPriorCategoricalWithHyperPrior<T>::finalizeEMParameterInitialization(){
    finalizeEMParameterEstimationOneIteration();
}

template<typename T> void TPriorCategoricalWithHyperPrior<T>::prepareEMParameterEstimationInitial(){
    _EMSums.resize(this->_numStates, 0.);
    _EMTotal = 0;
}

template<typename T> void TPriorCategoricalWithHyperPrior<T>::prepareEMParameterEstimationOneIteration(){
    std::fill(_EMSums.begin(), _EMSums.end(), 0.);
    _EMTotal= 0;
}

template<typename T> void TPriorCategoricalWithHyperPrior<T>::handleEMParameterEstimationOneIteration(const size_t & Index, const TDataVector<double, size_t> & weights){
    for (size_t k = 0; k < _K; k++) {
        _EMSums[k] += weights[k];
    }
    _EMTotal++;
}

template<typename T> void TPriorCategoricalWithHyperPrior<T>::finalizeEMParameterEstimationOneIteration(){
    if (!_pis->hasFixedInitialValue()){
        for (size_t k = 0; k < _K; k++) {
            _pis->set(k, _EMSums[k] / _EMTotal);
        }
    }
}

template<typename T> void TPriorCategoricalWithHyperPrior<T>::finalizeEMParameterEstimationFinal(){
    _EMSums.clear();
}

template <class T> void TPriorCategoricalWithHyperPrior<T>::simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) {
    // first normalize pis: they might have changed in the meantime
    _normalizePis(false, 0);

    // calculate cumulative pi's
    std::vector<double> cumPis(_K);
    cumPis[0] = _pis->value<double>(0); // first
    for (size_t k = 1; k < _K; k++) { // all others
        cumPis[k] = cumPis[k-1] + _pis->value<double>(k);
    }

    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        if (!paramToUse->hasFixedInitialValue()) { // only simulate if parameter has not a fixed value
            TRange range = paramToUse->getFull();
            for (size_t i = range.first; i < range.last; i += range.increment) {
                uint32_t value = RandomGenerator->pickOne(cumPis);
                while (!paramToUse->valueIsInsideBoundary(value)) { // propose again if value is too large, to prevent overshooting while mirroring
                    value = RandomGenerator->pickOne(cumPis);
                }
                // set value
                paramToUse->set(i, value);
            }
        }
    }
}
//-------------------------------------------
// TTransitionMatrixExponentialMCMCPrior
//-------------------------------------------

template <typename T> TTransitionMatrixExponentialMCMCPrior<T>::TTransitionMatrixExponentialMCMCPrior() : TPriorWithHyperPrior<T>(), TTransitionMatrixExponentialBase<double, size_t, size_t>() {
    _ranEM = false;
    _paramsAreInitialized = false;
}

template <typename T> void TTransitionMatrixExponentialMCMCPrior<T>::initGeneratingMatrix(const size_t & NumStates, std::unique_ptr<TGeneratingMatrixBase<double, size_t>> &GeneratingMatrix) {
    this->_numStates = NumStates;
    addGeneratingMatrix(GeneratingMatrix);
}

template <typename T> void TTransitionMatrixExponentialMCMCPrior<T>::initDistances(const std::shared_ptr<TDistancesBinnedBase> & distances){
    // initialize transition matrices
    initializeWithDistances(this->_numStates, distances);

    if (_paramsAreInitialized){
        _updateTransitionMatrices();
    }
}

template <class T> bool TTransitionMatrixExponentialMCMCPrior<T>::checkMinMax(std::string &min, std::string &max, bool & minIsIncluded, bool & maxIsIncluded, bool & hasDefaultMin, bool & hasDefaultMax) {
    // parameter on which transition matrix is defined must be x = {0, 1, ..., numStates-1)
    bool didNotChange = true;
    if (convertString<double>(min) < 0) {
        hasDefaultMin = false;
        min = "0";
        minIsIncluded = true;
        didNotChange = false;
    }
    // can not check max < numStates-1, since numStates is not initialized at this point (only after data has been filled)
    return didNotChange;
}

template <typename T> void TTransitionMatrixExponentialMCMCPrior<T>::_updateTransitionMatrices(){
    _resetTransitionMatrices(); // old = current

    // fill values of MCMC parameters into vector
    std::vector<double> Values;
    _fillCurrentGeneratingMatrixParameters(Values);
    // generate new transition matrices from updated parameters
    this->_fillGeneratingMatrix(Values);
    this->_fillTransitionProbabilities();
    this->_fillStationaryDistribution();
}

template <typename T> void TTransitionMatrixExponentialMCMCPrior<T>::_resetTransitionMatrices(){
    this->_transitionMatrices.swap(this->_transitionMatrices_old);
}

template <typename T> double TTransitionMatrixExponentialMCMCPrior<T>::_getLogPriorDensity_oneVal(T x){
    throw std::runtime_error("Elements in array with HMM prior are non-iid -> do not call function _getLogPriorDensity_oneVal(T x) with single elements!");
}

template <typename T> double TTransitionMatrixExponentialMCMCPrior<T>::_getLogPriorRatio_oneVal(T x, T x_old){
    throw std::runtime_error("Elements in array with HMM prior are non-iid -> do not call function _getLogPriorRatio_oneVal(T x, T x_old) with single elements!");
}

template <typename T> double TTransitionMatrixExponentialMCMCPrior<T>::_calcLogPOfValueGivenPrevious(const std::shared_ptr<TMCMCObservationsBase> & data, const size_t & i){
    if (i == 0) {
        return log((*this->_transitionMatrices)[(*this->_distances)[0]](0, data->value<T>(0))); // stationary -> values[i-1] does not work
    }
    // calculates log(P(val_i | val_{i-1}))
    return log((*this->_transitionMatrices)[(*this->_distances)[i]](data->value<T>(i-1), data->value<T>(i)));
}

template <typename T> double TTransitionMatrixExponentialMCMCPrior<T>::_calcLogPOfNextGivenValue(const std::shared_ptr<TMCMCObservationsBase> & data, const size_t & i){
    if (i == data->totalSize() - 1) { // last -> there is no next
        return 0.;
    }

    // calculates log(P(val_{i+1} | val_i, Lambda, dist_{i+1})
    return log((*this->_transitionMatrices)[(*this->_distances)[i+1]](data->value<T>(i), data->value<T>(i+1)));
}

template <typename T> double TTransitionMatrixExponentialMCMCPrior<T>::_calcLogPOfOldValueGivenPrevious(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & i){
    if (i == 0) {
        return log((*this->_transitionMatrices)[(*this->_distances)[0]](0, data->oldValue<T>(0))); // stationary -> values[i-1] does not work
    }
    // calculates log(P(oldVal_i | val_{i-1}, Lambda, dist_i))
    return log((*this->_transitionMatrices)[(*this->_distances)[i]](data->value<T>(i-1), data->oldValue<T>(i)));
}

template <typename T> double TTransitionMatrixExponentialMCMCPrior<T>::_calcLogPOfNextGivenOldValue(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & i){
    if (i == data->totalSize() - 1) { // last -> there is no next
        return 0.;
    }
    // calculates log(P(val_{i+1} | oldVal_i, Lambda, dist_{i+1})
    return log((*this->_transitionMatrices)[(*this->_distances)[i+1]](data->oldValue<T>(i), data->value<T>(i+1)));
}

template <typename T> double TTransitionMatrixExponentialMCMCPrior<T>::_getLogPriorDensity_vec(const std::shared_ptr<TMCMCObservationsBase> & data, const size_t & i){
    return _calcLogPOfValueGivenPrevious(data, i) + _calcLogPOfNextGivenValue(data, i);
}

template <typename T> double TTransitionMatrixExponentialMCMCPrior<T>::_getLogPriorDensityOld_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & i){
    return _calcLogPOfOldValueGivenPrevious(data, i) + _calcLogPOfNextGivenOldValue(data, i);
}

template <typename T> double TTransitionMatrixExponentialMCMCPrior<T>::_getLogPriorRatio_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & i){
    return _calcLogPOfValueGivenPrevious(data, i) +_calcLogPOfNextGivenValue(data, i) - _calcLogPOfOldValueGivenPrevious(data, i) - _calcLogPOfNextGivenOldValue(data, i);
}

template <typename T> double TTransitionMatrixExponentialMCMCPrior<T>::_calcLLUpdateGeneratingMatrixParameter() {
    // compute log likelihood
    double LL = 0.;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        // first
        LL += log((*this->_transitionMatrices)[(*this->_distances)[0]](0, paramToUse->value<T>(0)))
              - log((*this->_transitionMatrices_old)[(*this->_distances)[0]](0, paramToUse->value<T>(0)));
        // all other
        TRange range = paramToUse->getFull();
        for (size_t i = range.first + range.increment; i < range.last; i += range.increment) {
            LL += log((*this->_transitionMatrices)[(*this->_distances)[i]](paramToUse->value<T>(i - 1), paramToUse->value<T>(i)))
                  - log((*this->_transitionMatrices_old)[(*this->_distances)[i]](paramToUse->value<T>(i - 1), paramToUse->value<T>(i)));
        }
    }
    return LL;
}

template <typename T> double TTransitionMatrixExponentialMCMCPrior<T>::_calcLogHUpdateGeneratingMatrixParameter(std::shared_ptr<TMCMCParameterBase> &PriorParam) {
    // compute log likelihood
    double LL = _calcLLUpdateGeneratingMatrixParameter();
    // compute prior
    double logPrior = PriorParam->getLogPriorRatio();
    // compute log(Hastings ratio)
    return LL + logPrior;
}

template <typename T> void TTransitionMatrixExponentialMCMCPrior<T>::_updateGeneratingMatrixParameter(std::shared_ptr<TMCMCParameterBase> &PriorParam) {
    // update transition matrices
    _updateTransitionMatrices();
    // compute log(Hastings ratio)
    double logH = _calcLogHUpdateGeneratingMatrixParameter(PriorParam);
    // accept or reject
    if (!PriorParam->acceptOrReject(logH)) { // not accepted -> resetTransitionMatrices
        _resetTransitionMatrices();
    }
}

template <class T> double TTransitionMatrixExponentialMCMCPrior<T>::getSumLogPriorDensity(const std::shared_ptr<TMCMCObservationsBase> & data) {
    double sum = 0.;
    for (size_t i = 0; i < data->totalSize(); i++){
        sum += _calcLogPOfValueGivenPrevious(data, i);
    }
    return sum;
}

template <typename T> void TTransitionMatrixExponentialMCMCPrior<T>::_runEMEstimation(TLatentVariable<double, size_t, size_t> &latentVariable, TLog * Logfile, const bool & FixTransitionMatricesDuringEM) {
    // only allow for 1 parameter (no shared priors)
    // reason: EM. z below passes pointer to itself to this prior, which then runs EM. If there are multiple z,
    // we would need to find a way to have pointers to both and run the EM on them simultaneously -> not so easy
    if (this->_parameters.size() > 1){
        throw std::runtime_error("In function 'template <typename T> void TTransitionMatrixExponentialMCMCPrior<T>::runEMEstimation(TLatentVariable<double, size_t, size_t> &latentVariable, TLog * Logfile)': Can not run EM estimation of prior " + this->_name + " for " + toString(this->_parameters.size()) + " parameters. Currently only implemented for 1 parameter.");
    }

    // fix transition matrix with EM only if kappa shouldn't be initialized
    TTransitionMatrixExponentialBase::runEMEstimation(latentVariable, FixTransitionMatricesDuringEM, Logfile);
    _ranEM = true;
}

// if z are known: no need to do EM
template <typename T> void TTransitionMatrixExponentialMCMCPrior<T>::_estimateInitialPriorParameters(TLog * Logfile, const bool & FixTransitionMatricesDuringEM) {
    // gather all parameters
    std::vector<std::shared_ptr<TMCMCObservationsBase>> params;
    for (auto param : this->_parameters){
        params.push_back(this->_getSharedPtrParam(param));
    }

    if (!_ranEM) { // only initialize prior parameters if we didn't run EM before
        if (!FixTransitionMatricesDuringEM) {
            estimateInitialPriorParameters_HiddenStateKnown(params, Logfile);
        } else {
            // don't initialize prior parameters, just leave it as is is
        }
    }
}

template <typename T> void TTransitionMatrixExponentialMCMCPrior<T>::estimateInitialPriorParameters_HiddenStateKnown(const std::vector<std::shared_ptr<TMCMCObservationsBase>> & Parameters, TLog * Logfile){
    // estimate the transition matrix if hidden state was known: no EM required
    this->_logfile = Logfile;

    // get junk ends from distances
    std::vector<size_t> junkEnds = this->_distances->template getJunkEnds<size_t>();

    // prepare storage
    this->prepareEMParameterEstimationInitial();
    this->prepareEMParameterEstimationOneIterationTransitionProbabilities();

    size_t first = 0;
    size_t previousState;

    // go over all parameters
    for (auto & paramToUse : Parameters) {
        // go over all junks
        for (size_t last : junkEnds) {
            // go over all elements in one junk
            previousState = paramToUse->template value<size_t>(0);
            for (size_t i = first + 1; i < last; ++i) {
                auto state = paramToUse->template value<size_t>(i);
                handleEMParameterInitializationTransitionProbabilities(i, previousState, state);
                previousState = state;
            }
            //update first of next
            first = last;
        }
    }
    this->finalizeEMParameterInitializationTransitionProbabilities();
    this->finalizeEMParameterEstimationFinal();
}

template <typename T> void TTransitionMatrixExponentialMCMCPrior<T>::simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) {
    // set number of states
    TTransitionMatrixExponentialBase::initialize(this->_numStates);

    // simulate distances (same for all _parameters)
    std::shared_ptr<TMCMCObservationsBase> tmpParam = this->_getSharedPtrParam(this->_parameters[0]);
    this->simulateDistances(tmpParam->totalSize(), RandomGenerator);

    // update transition matrices
    _updateTransitionMatrices();

    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        if (!paramToUse->hasFixedInitialValue()) { // only simulate if parameter has not a fixed value
            TRange range = paramToUse->getFull();
            // sample first state
            size_t previousState = 0;
            for (size_t i = range.first; i < range.last; i += range.increment) {
                size_t currentState = sampleNextState(i, previousState, *RandomGenerator);

                // set value
                paramToUse->set(i, currentState);

                // update state
                previousState = currentState;
            }
        }
    }
}

//-------------------------------------------
// TPriorHMMBoolWithHyperPrior
//-------------------------------------------

template <typename T> TPriorHMMBoolWithHyperPrior<T>::TPriorHMMBoolWithHyperPrior() : TTransitionMatrixExponentialMCMCPrior<T>(), TTransitionMatrixExponentialNelderMead<double, size_t, size_t>() {
    this->_name = MCMCPrior::hmm_bool;

    // add generating matrix
    std::unique_ptr<TGeneratingMatrixBase<double, size_t>> generatingMatrix = std::make_unique<TGeneratingMatrixBool<double, size_t>>();
    this->initGeneratingMatrix(2, generatingMatrix);
}

template <typename T> void TPriorHMMBoolWithHyperPrior<T>::initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, std::vector<std::shared_ptr<TObservationsBase> > & observations){
    this->_checkSizeParams(params.size(), 2);
    this->_checkSizeObservations(observations.size(), 0);
    _lambda1 = params.at(0);
    _lambda2 = params.at(1);

    _checkMinMaxOfHyperPrior();
}

template <class T> void TPriorHMMBoolWithHyperPrior<T>::constructDAG(TDAG & DAG, TDAG & temporaryDAG){
    this->_constructDAG(DAG, temporaryDAG, {_lambda1, _lambda2});
}

template <typename T> void TPriorHMMBoolWithHyperPrior<T>::_checkMinMaxOfHyperPrior(){
    // idea: parameters _lambda1 and _lambda2 should always be > 0 -> re-set boundaries (0 is not included because of log)
    _lambda1->reSetBoundaries(false, true, "0.", toString(std::numeric_limits<double>::max()), false, true);
    _lambda2->reSetBoundaries(false, true, "0.", toString(std::numeric_limits<double>::max()), false, true);
}

template <class T> void TPriorHMMBoolWithHyperPrior<T>::initializeStorageOfPriorParameters() {
    _lambda1->initializeStorageSingleElementBasedOnPrior();
    _lambda2->initializeStorageSingleElementBasedOnPrior();
    this->_paramsAreInitialized = true;

    if (this->_distances){
        this->_updateTransitionMatrices();
    }
}

template <class T> void TPriorHMMBoolWithHyperPrior<T>::typesAreCompatible(bool throwIfNot) {
    // parameters on which HMM bool prior is defined must be unsigned integer (actually 0 or 1, but this is ensured by checkMinMax())
    this->_checkTypes(throwIfNot, false, true, true);

    // in addition: check if prior parameters have expected type
    // _lambda1 and _lambda2 must be floating point
    this->_checkTypesOfPriorParameters(_lambda1, throwIfNot, true, false, false);
    this->_checkTypesOfPriorParameters(_lambda2, throwIfNot, true, false, false);

    // check if the prior distribution on the prior parameters is ok
    _lambda1->typesAreCompatible(throwIfNot);
    _lambda2->typesAreCompatible(throwIfNot);
}

template <typename T> void TPriorHMMBoolWithHyperPrior<T>::_fillCurrentGeneratingMatrixParameters(std::vector<double> & Values) {
    Values = {_lambda1->value<double>(), _lambda2->value<double>()};
}

template <typename T> void TPriorHMMBoolWithHyperPrior<T>::_fillParametersBasedOnGeneratingMatrix(const std::vector<double> & Values) {
    // fill MCMC parameters lambda based on values of generator matrix
    assert(Values.size() == 2);
    _lambda1->set(Values[0]);
    _lambda2->set(Values[1]);
}

template <typename T> void TPriorHMMBoolWithHyperPrior<T>::updateParams(){
    if (_lambda1->update()) {
        this->_updateGeneratingMatrixParameter(_lambda1);
    }
    if (_lambda2->update()) {
        this->_updateGeneratingMatrixParameter(_lambda2);
    }
}

template <typename T> double TPriorHMMBoolWithHyperPrior<T>::lambda1() const {
    return _lambda1->value<double>();
}

template <typename T> double TPriorHMMBoolWithHyperPrior<T>::lambda2() const {
    return _lambda2->value<double>();
}

template <class T> double TPriorHMMBoolWithHyperPrior<T>::getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data) {
    // returns sum of log-density of array + log density of alpha + log density of beta
    return this->getSumLogPriorDensity(data) + _lambda1->getLogPriorDensity() + _lambda2->getLogPriorDensity();
}

template <typename T> bool TPriorHMMBoolWithHyperPrior<T>::_fixTransitionMatricesDuringEM() {
    bool fixTransitionMatricesDuringEM = _lambda1->hasFixedInitialValue() && _lambda2->hasFixedInitialValue();
    if ((_lambda1->hasFixedInitialValue() && !_lambda2->hasFixedInitialValue()) || (!_lambda1->hasFixedInitialValue() && _lambda2->hasFixedInitialValue())){
        throw "Error in initialization of " + this->_name + " prior: either specify initial values for both lambda1 and lambda2, or for none of them. Don't know how to handle only one!";
    }
    return fixTransitionMatricesDuringEM;
}

template <typename T> void TPriorHMMBoolWithHyperPrior<T>::runEMEstimation(TLatentVariable<double, size_t, size_t> &latentVariable, TLog * Logfile) {
    this->setInitialValuesNelderMead({_lambda1->value<double>(), _lambda2->value<double>()}, 0.5);
    this->_runEMEstimation(latentVariable, Logfile, _fixTransitionMatricesDuringEM());
}

// if z are known: no need to do EM
template <typename T> void TPriorHMMBoolWithHyperPrior<T>::estimateInitialPriorParameters(TLog * Logfile) {
    this->setInitialValuesNelderMead({_lambda1->value<double>(), _lambda2->value<double>()}, 0.5);
    this->_estimateInitialPriorParameters(Logfile, _fixTransitionMatricesDuringEM());
}

template <typename T> void TPriorHMMBoolWithHyperPrior<T>::switchPriorClassificationAfterEM(TLog * Logfile){
    // classification of components by EM is random, but sometimes we have certain restrictions (e.g. normal mixed model with 2 components, shared mean but different variances)
    // we then might have to switch the EM labels and all associated prior parameters

    // we switch z but are not allowed to switch lambda1 and lambda2 -> will not match at all!
    if (_fixTransitionMatricesDuringEM()){
        Logfile->warning("Switched z after EM, but can not switch Lambda, because initial values are fix.");
    }
    double tmpLambda1 = _lambda1->value<double>();
    if (!_lambda1->hasFixedInitialValue()) {
        _lambda1->set(_lambda2->value<double>());
    }
    if (!_lambda2->hasFixedInitialValue()) {
        _lambda2->set(tmpLambda1);
    }
    this->_updateTransitionMatrices();
}

//-------------------------------------------
// TPriorHMMLadderWithHyperPrior
//-------------------------------------------

template <typename T> TPriorHMMLadderWithHyperPrior<T>::TPriorHMMLadderWithHyperPrior() : TTransitionMatrixExponentialMCMCPrior<T>(), TTransitionMatrixExponentialNewtonRaphson<double, size_t, size_t>() {
    this->_name = MCMCPrior::hmm_ladder;
}

template <typename T> void TPriorHMMLadderWithHyperPrior<T>::setNumStates(const size_t &NumStates) {
    // add generating matrix
    std::unique_ptr<TGeneratingMatrixBase<double, size_t>> generatingMatrix = std::make_unique<TGeneratingMatrixLadder<double, size_t>>(NumStates);
    this->initGeneratingMatrix(NumStates, generatingMatrix);
}

template <typename T> void TPriorHMMLadderWithHyperPrior<T>::initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, std::vector<std::shared_ptr<TObservationsBase> > & observations){
    // expect 2 parameters: kappa and numStates
    this->_checkSizeParams(params.size(), 2);
    this->_checkSizeObservations(observations.size(), 0);
    _kappa = params.at(0);
    _numStatesParam = params.at(1);

    _checkMinMaxOfHyperPrior();
}

template <class T> void TPriorHMMLadderWithHyperPrior<T>::constructDAG(TDAG & DAG, TDAG & temporaryDAG){
    this->_constructDAG(DAG, temporaryDAG, {_kappa, _numStatesParam});
}

template <typename T> void TPriorHMMLadderWithHyperPrior<T>::_checkMinMaxOfHyperPrior(){
    // idea: parameters _kappa should always be > 0 -> re-set boundaries (0 is not included because of log)
    _kappa->reSetBoundaries(false, true, "0.", toString(std::numeric_limits<double>::max()), false, true);
    // numStates: require at least two states
    _numStatesParam->reSetBoundaries(false, true, "2", toString(std::numeric_limits<double>::max()), true, true);
}

template <class T> void TPriorHMMLadderWithHyperPrior<T>::initializeStorageOfPriorParameters() {
    _kappa->initializeStorageSingleElementBasedOnPrior();
    _numStatesParam->initializeStorageSingleElementBasedOnPrior();
    if (_numStatesParam->isUpdated()){
        throw std::runtime_error("In function 'template <class T> void TPriorHMMLadderWithHyperPrior<T>::initializeStorageOfPriorParameters()': Currently do not support updates of the numStates parameter of a ladder-type transition matrix!");
    }
    setNumStates(_numStatesParam->value<size_t>());

    this->_paramsAreInitialized = true;

    if (this->_distances){
        this->_updateTransitionMatrices();
    }
}

template <class T> void TPriorHMMLadderWithHyperPrior<T>::typesAreCompatible(bool throwIfNot) {
    // parameters on which HMM ladder prior is defined must be unsigned integer
    this->_checkTypes(throwIfNot, false, true, true);

    // in addition: check if prior parameters have expected type
    // _kappa must be floating point
    this->_checkTypesOfPriorParameters(_kappa, throwIfNot, true, false, false);
    // numStates must be unsigned integer
    this->_checkTypesOfPriorParameters(_numStatesParam, throwIfNot, false, true, true);

    // check if the prior distribution on the prior parameters is ok
    _kappa->typesAreCompatible(throwIfNot);
    _numStatesParam->typesAreCompatible(throwIfNot);
}

template <typename T> void TPriorHMMLadderWithHyperPrior<T>::_fillCurrentGeneratingMatrixParameters(std::vector<double> & Values) {
    Values = {_kappa->value<double>()};
}

template <typename T> void TPriorHMMLadderWithHyperPrior<T>::_fillParametersBasedOnGeneratingMatrix(const std::vector<double> & Values) {
    // fill MCMC parameter kappa based on values of generator matrix
    assert(Values.size() == 1);
    _kappa->set(Values[0]);
}

template <typename T> void TPriorHMMLadderWithHyperPrior<T>::updateParams(){
    if (_kappa->update()) {
        this->_updateGeneratingMatrixParameter(_kappa);
    }
}

template <typename T> double TPriorHMMLadderWithHyperPrior<T>::kappa() const{
    return _kappa->value<double>();
}

template <class T> double TPriorHMMLadderWithHyperPrior<T>::getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data) {
    // returns sum of log-density of array + log density of kappa
    return this->getSumLogPriorDensity(data) + _kappa->getLogPriorDensity();
}

template <typename T> void TPriorHMMLadderWithHyperPrior<T>::runEMEstimation(TLatentVariable<double, size_t, size_t> &latentVariable, TLog * Logfile) {
    this->setInitialValuesNewtonRaphson(_kappa->value<double>(), _kappa->min(), _kappa->max());

    // fix transition matrix with EM only if lambda1 and lambda2 shouldn't be initialized
    this->_runEMEstimation(latentVariable, Logfile, _kappa->hasFixedInitialValue());
}

// if z are known: no need to do EM
template <typename T> void TPriorHMMLadderWithHyperPrior<T>::estimateInitialPriorParameters(TLog * Logfile) {
    this->setInitialValuesNewtonRaphson(_kappa->value<double>(), _kappa->min(), _kappa->max());

    this->_estimateInitialPriorParameters(Logfile, _kappa->hasFixedInitialValue());
}

template <typename T> void TPriorHMMLadderWithHyperPrior<T>::switchPriorClassificationAfterEM(TLog * Logfile){
    // classification of components by EM is random, but sometimes we have certain restrictions (e.g. normal mixed model with 2 components, shared mean but different variances)
    // we then might have to switch the EM labels and all associated prior parameters

    // stays empty -> parameter of transition matrix (kappa) is the same for all states
}

//-------------------------------------------
// TMultivariateNormal
//-------------------------------------------

#ifdef WITH_ARMADILLO
template <typename T> TMultivariateNormal<T>::TMultivariateNormal() : TPriorWithHyperPrior<T>() {
    _D = 0;
    _minusDdiv2Log2Pi = 0.;
}

template <typename T> double TMultivariateNormal<T>::_numberOfElementsInTriangularMatrix_Diagonal0(const size_t & dimensions){
    // this is for a triangular matrix where the diagonal is ALSO zero
    return (dimensions - 1.) * dimensions / 2.;
}

template <typename T> double TMultivariateNormal<T>::_calc_Dimensions_from_numberOfElementsInTriangularMatrix_Diagonal0(const size_t & totalSize){
    // this is for a triangular matrix where the diagonal is ALSO zero
    return (1. + sqrt(1.+8.*totalSize)) / 2.;
}

template <typename T> void TMultivariateNormal<T>::_calculateAndSetD() {
    // derive D from size of parameter (start with first and then check if all others match dimensions of first)
    std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(this->_parameters[0]);
    if (paramToUse->numDim() != 2) {
        throw std::runtime_error("In function 'template <typename T> void TMultivariateNormal<T>::_calculateD()': a multivariate normal prior requires a two-dimensional parameter!");
    }
    int D = paramToUse->dimensions()[1];

    for (auto & param : this->_parameters) {
        paramToUse = this->_getSharedPtrParam(param);
        // check if param is 2-dimensional
        if (paramToUse->numDim() != 2) {
            throw std::runtime_error("In function 'template <typename T> void TMultivariateNormal<T>::_calculateD()': a multivariate normal prior requires a two-dimensional parameter!");
        }
        if (paramToUse->dimensions()[1] != D){
            throw std::runtime_error("In function 'template <typename T> void TMultivariateNormal<T>::_calculateD()': Number of dimensions of multivariate normal prior (=" + toString(D) + ") and of parameter " + paramToUse->name() + " (=" + toString(paramToUse->dimensions()[1]) + ") do not match!");
        }
    }

    _D = D;
    _minusDdiv2Log2Pi = - (static_cast<double>(_D))/2. * log(2*3.14159265359);
}

template <typename T> const std::shared_ptr<TNamesEmpty> & TMultivariateNormal<T>::_getDimensionName() {
    // relevant for _D is the second dimension
    // start with first and then check if all others match dimensions of first
    std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(this->_parameters[0]);
    const std::shared_ptr<TNamesEmpty> dimensionNameFirst = paramToUse->getDimensionName(1);

    for (auto & param : this->_parameters) {
        paramToUse = this->_getSharedPtrParam(param);
        // check if pointer is the same
        if (dimensionNameFirst != paramToUse->getDimensionName(1)) {
            throw std::runtime_error("In function 'template <typename T> void TMultivariateNormal<T>::_getDimensionName()': pointer to name class of dimension 1 of parameter " + paramToUse->name() + " differs from the name class of other parameters that share this prior!");
        }
    }
    return paramToUse->getDimensionName(1);
}

template <typename T> void TMultivariateNormal<T>::_setDimensionsOfMu(const std::shared_ptr<TMCMCParameterBase> & mu, const std::shared_ptr<TNamesEmpty> & DimName) {
    mu->initializeStorageBasedOnPrior({static_cast<size_t>(_D)}, {DimName});
}

template <typename T> void TMultivariateNormal<T>::_setDimensionsOfM(const std::shared_ptr<TMCMCParameterBase> & m) {
    m->initializeStorageSingleElementBasedOnPrior();
}

template <typename T> void TMultivariateNormal<T>::_setDimensionsOfMrr(std::shared_ptr<TMCMCParameterBase> & mrr, const std::shared_ptr<TNamesEmpty> & DimName) {
    mrr->initializeStorageBasedOnPrior({static_cast<size_t>(_D)}, {DimName});

    // check: if D=1 (univariate normal distribution), we will only update m and mu, and fix mrr to 1.
    if (_D == 1) {
        mrr->set(0, 1.);
    }
}

template <typename T> void TMultivariateNormal<T>::_setDimensionsOfMrs(const std::shared_ptr<TMCMCParameterBase> & mrs, const std::shared_ptr<TNamesEmpty> & DimName) {
    if (_D > 1) { // only check if there are >1 dimension (mrs are not defined for D=1)
        std::shared_ptr<TNamesEmpty> dimNamesMrs = _generateDimNamesMrs(DimName);
        mrs->initializeStorageBasedOnPrior({static_cast<size_t>(_numberOfElementsInTriangularMatrix_Diagonal0(static_cast<size_t>(_D)))}, {dimNamesMrs});
    } else {
        std::shared_ptr<TNamesEmpty> dimNamesMrs = std::make_shared<TNamesStrings>(0);
        mrs->initializeStorageBasedOnPrior({0}, {dimNamesMrs});
    }
}

template <typename T> std::shared_ptr<TNamesEmpty> TMultivariateNormal<T>::_generateDimNamesMrs(const std::shared_ptr<TNamesEmpty> & DimName) {
    // DimName corresponds to the row-/colnames of the matrix -> now we want every combination of it (for lower triangular matrix)
    std::vector<std::string> namesMrs;
    for (size_t r = 1; r < _D; r++) {
        for (size_t s = 0; s < r; s++) {
            std::string first = (*DimName)[r];
            std::string second = (*DimName)[s];
            namesMrs.push_back(first + "_" + second);
        }
    }
    assert(namesMrs.size() == _numberOfElementsInTriangularMatrix_Diagonal0(static_cast<size_t>(_D)));

    // create ptr and return
    std::shared_ptr<TNamesEmpty> dimNamesMrs = std::make_shared<TNamesStrings>(namesMrs);
    return dimNamesMrs;
}

template <typename T> void TMultivariateNormal<T>::_reSetBoundaryTo0_M_Mrr(const std::shared_ptr<TMCMCParameterBase> & param){
    // parameter _Mrr and _m should always be > 0 -> re-set boundaries
    param->reSetBoundaries(false, true, "0.", toString(std::numeric_limits<double>::max()), false, true);
}

template <typename T> double TMultivariateNormal<T>::_calcSumLogMrr(const std::shared_ptr<TMCMCParameterBase> & Mrr){
    double sumlogMrr = 0;
    for (size_t d = 0; d < _D; d++) {
        sumlogMrr += log(Mrr->value<double>(d));
    }
    return sumlogMrr;
}

template <typename T>
double TMultivariateNormal<T>::_calcDoubleSum(const std::shared_ptr<TMCMCObservationsBase> & data, const TRange & row, const std::shared_ptr<TMCMCParameterBase> & mus, const std::shared_ptr<TMCMCParameterBase> & Mrr, const std::shared_ptr<TMCMCParameterBase> & Mrs){
    double outerSum = 0;
    for (size_t s = 0; s < _D; s++) {
        double innerSum = 0.;
        for (size_t r = s; r < _D; r++) {
            // take value for all data elements
            double value = data->value<T>(row.first + r*row.increment);
            if (s == r) {
                innerSum += Mrr->value<double>(r) * (value - mus->value<double>(r));
            } else {
                innerSum += Mrs->value<double>(_linearizeIndex_Mrs(r, s)) * (value - mus->value<double>(r));
            }
        }
        outerSum += innerSum * innerSum;
    }

    return outerSum;
}

template <typename T> double TMultivariateNormal<T>::_calcDoubleSum_OldParam(const std::shared_ptr<TMCMCParameterBase> & data, const TRange & row, const size_t & indexInRowThatChanged,
                                                                             const std::shared_ptr<TMCMCParameterBase> & mus, const std::shared_ptr<TMCMCParameterBase> & Mrr, const std::shared_ptr<TMCMCParameterBase> & Mrs){
    double outerSum = 0;
    for (size_t s = 0; s < _D; s++) {
        double innerSum = 0.;
        for (size_t r = s; r < _D; r++) {
            // get correct value for data (old or new)
            double value;
            if (r == indexInRowThatChanged) {
                value = data->oldValue<T>(row.first + r * row.increment);
            } else {
                value = data->value<T>(row.first + r * row.increment);
            }

            // add to sum
            if (s == r) {
                innerSum += Mrr->value<double>(s) * (value - mus->value<double>(r));
            } else {
                innerSum += Mrs->value<double>(_linearizeIndex_Mrs(r, s)) * (value - mus->value<double>(r));
            }
        }
        outerSum += innerSum * innerSum;
    }

    return outerSum;
}

template <typename T> double TMultivariateNormal<T>::_calcDoubleSum_updateMrr(const std::shared_ptr<TMCMCObservationsBase> & paramToUse, const TRange & row,
        const size_t & indexMrrThatChanged, double & sumParam_nd_minus_mu_d,
        const std::shared_ptr<TMCMCParameterBase> & mus, const std::shared_ptr<TMCMCParameterBase> & Mrs)
                {
    double sum = 0.;
    for (size_t r = (indexMrrThatChanged + 1); r < _D; r++){
        sum += Mrs->value<double>(_linearizeIndex_Mrs(r, indexMrrThatChanged)) * (paramToUse->value<T>(row.first + r*row.increment) - mus->value<double>(r));
    }

    double paramND_minus_muD =  paramToUse->value<T>(row.first + indexMrrThatChanged*row.increment) - mus->value<double>(indexMrrThatChanged);

    sumParam_nd_minus_mu_d += paramND_minus_muD*paramND_minus_muD;
    return paramND_minus_muD * sum;
}

template <typename T> double TMultivariateNormal<T>::_calcDoubleSum_updateMrs(const std::shared_ptr<TMCMCObservationsBase> & paramToUse, const TRange & row,
        const size_t & d, const size_t & e, double & sumParam_nd_minus_mu_d,
        const std::shared_ptr<TMCMCParameterBase> & mus, const std::shared_ptr<TMCMCParameterBase> & Mrr, const std::shared_ptr<TMCMCParameterBase> & Mrs){
    double sum = 0.;
    for (size_t r = e; r < _D; r++){
        if (r == d) continue;
        else if (r == e){
            sum += Mrr->value<double>(e) * (paramToUse->value<T>(row.first + e*row.increment) - mus->value<double>(e)); // take mrr
        } else {
            sum += Mrs->value<double>(_linearizeIndex_Mrs(r, e)) * (paramToUse->value<T>(row.first + r * row.increment) - mus->value<double>(r));
        }
    }

    double paramND_minus_muD = paramToUse->value<T>(row.first + d*row.increment) - mus->value<double>(d);

    sumParam_nd_minus_mu_d += paramND_minus_muD * paramND_minus_muD;
    return paramND_minus_muD * sum;
}

template <typename T>
double TMultivariateNormal<T>::_calcDoubleSum_updateMu(const std::shared_ptr<TMCMCObservationsBase> & paramToUse, const TRange & row,
                                                                const size_t & indexMuThatChanged, const size_t & min_r_DMin1,
                                                                const std::shared_ptr<TMCMCParameterBase> & mus, const std::shared_ptr<TMCMCParameterBase> & Mrr, const std::shared_ptr<TMCMCParameterBase> & Mrs){
    double sum = 0.;
    // go over all s = 1:min(r, D-1)
    for (size_t s = 0; s < min_r_DMin1; s++) {
        // go over all r = s:D, r != d
        double tmp = 0.;
        for (size_t r = s; r < _D; r++) {
            if (r == indexMuThatChanged) continue;
            else if (r == s){
                tmp += Mrr->value<double>(r) * (paramToUse->template value<T>(row.first + r * row.increment) - mus->value<double>(r));
            } else {
                tmp += Mrs->value<double>(_linearizeIndex_Mrs(r, s)) * (paramToUse->template value<T>(row.first + r * row.increment) - mus->value<double>(r));
            }
        }
        if (indexMuThatChanged == s){
            sum += tmp * Mrr->value<double>(s);
        } else {
            sum += tmp * Mrs->value<double>(_linearizeIndex_Mrs(indexMuThatChanged, s));
        }
    }

    return sum;
}

template <typename T>
double TMultivariateNormal<T>::_calcDoubleSum_updateParam(const std::shared_ptr<TMCMCObservationsBase> & data, const TRange & row,
                                                          const size_t & indexMuThatChanged, const size_t & min_r_DMin1,
                                                          const std::shared_ptr<TMCMCParameterBase> & mus, const std::shared_ptr<TMCMCParameterBase> & Mrr, const std::shared_ptr<TMCMCParameterBase> & Mrs){
    double sum = 0.;
    // go over all s = 1:min(r, D-1)
    for (size_t s = 0; s < min_r_DMin1; s++) {
        // go over all r = s:D, r != d
        double tmp = 0.;
        for (size_t r = s; r < _D; r++) {
            if (r == indexMuThatChanged) continue;
            else if (r == s){
                tmp += Mrr->value<double>(r) * (data->value<T>(row.first + r * row.increment) - mus->value<double>(r));
            } else {
                tmp += Mrs->value<double>(_linearizeIndex_Mrs(r, s)) * (data->value<T>(row.first + r * row.increment) - mus->value<double>(r));
            }
        }
        if (indexMuThatChanged == s){
            sum += tmp * Mrr->value<double>(s);
        } else {
            sum += tmp * Mrs->value<double>(_linearizeIndex_Mrs(indexMuThatChanged, s));
        }
    }

    return sum;
}

template <typename T> double TMultivariateNormal<T>::_getLogPriorDensity_oneVal(T x){
    throw std::runtime_error("Elements in multidimensional array are non-iid -> do not call function _getLogPriorDensity_oneVal(T x) with single elements!");
}

template <typename T> double TMultivariateNormal<T>::_getLogPriorRatio_oneVal(T x, T x_old){
    throw std::runtime_error("Elements in multidimensional array are non-iid -> do not call function _getLogPriorDensity_oneVal(T x) with single elements!");
}

template <typename T>
template <typename C>
double TMultivariateNormal<T>::_calcLogPriorDensity(const std::shared_ptr<C> & paramOrdata, const TRange &row,
        const std::shared_ptr<TMCMCParameterBase> & mus, const std::shared_ptr<TMCMCParameterBase> & m,
        const std::shared_ptr<TMCMCParameterBase> & Mrr, const std::shared_ptr<TMCMCParameterBase> & Mrs){
    double sumlogMrr = _calcSumLogMrr(Mrr);
    double doubleSum = _calcDoubleSum(paramOrdata, row, mus, Mrr, Mrs);
    return - _D * log(m->value<double>()) + sumlogMrr
                + _minusDdiv2Log2Pi
                - 1. / (2. * m->value<double>() * m->value<double>()) * doubleSum;
}

template <typename T> double TMultivariateNormal<T>::_calcLogPriorDensityOld(const std::shared_ptr<TMCMCParameterBase> & data, const TRange &row, const size_t & col,
                                                                             const std::shared_ptr<TMCMCParameterBase> & mus, const std::shared_ptr<TMCMCParameterBase> & m,
                                                                             const std::shared_ptr<TMCMCParameterBase> & Mrr, const std::shared_ptr<TMCMCParameterBase> & Mrs){
    double sumlogMrr = _calcSumLogMrr(Mrr);
    double doubleSumOld = _calcDoubleSum_OldParam(data, row, col, mus, Mrr, Mrs);

    return -_D * log(m->value<double>()) + sumlogMrr
                + _minusDdiv2Log2Pi
                - 1. / (2. * m->value<double>() * m->value<double>()) * doubleSumOld;
}

template <typename T> double TMultivariateNormal<T>::_calcLogPriorRatio(const std::shared_ptr<TMCMCParameterBase> & data, const TRange &row, const size_t & d,
                                                                        const std::shared_ptr<TMCMCParameterBase> & mus, const std::shared_ptr<TMCMCParameterBase> & m,
                                                                        const std::shared_ptr<TMCMCParameterBase> & Mrr, const std::shared_ptr<TMCMCParameterBase> & Mrs){
    // store values
    double oldParam = data->oldValue<T>(row.first + d*row.increment);
    double newParam = data->value<T>(row.first + d*row.increment);

    // calculate simplified hastings ratio
    double sumM = _calcSumM_dr_squared(Mrr->value<double>(d), d, Mrs);
    size_t min_r_DMin1 = std::min(d+1, static_cast<size_t>(_D-1));
    double sum = _calcDoubleSum_updateParam(data, row, d, min_r_DMin1, mus, Mrr, Mrs);
    double tmp = sumM *(oldParam*oldParam - newParam*newParam + 2.*mus->value<double>(d)*(newParam - oldParam)) + 2.*(oldParam - newParam) * sum;

    return 1. / (2. * m->value<double>() * m->value<double>()) * tmp;
}

template <typename T> double TMultivariateNormal<T>::_calcLLRatioUpdateMrr(const size_t & N, const double & oldMrr, const double & newMrr, const double & sum,
        const double & sumParam_nd_minus_mu_d, const std::shared_ptr<TMCMCParameterBase> & m){
    return N * log(newMrr / oldMrr) + 1. / (2. * m->value<double>() * m->value<double>())
                               * ((oldMrr*oldMrr - newMrr*newMrr) * sumParam_nd_minus_mu_d
                                  + 2.*(oldMrr-newMrr) * sum);
}

template <typename T> double TMultivariateNormal<T>::_calcLLRatioUpdateMrs(const double & oldMrs, const double & newMrs, const double & sum, const double & sumParam_nd_minus_mu_d, const std::shared_ptr<TMCMCParameterBase> & m){
    return 1. / (2. * m->value<double>() * m->value<double>()) * ((oldMrs*oldMrs - newMrs*newMrs) * sumParam_nd_minus_mu_d + 2. * (oldMrs - newMrs) * sum);
}

template<typename T> double TMultivariateNormal<T>::_calcSumM_dr_squared(const double & mrr, const size_t & r, const std::shared_ptr<TMCMCParameterBase> & Mrs){
    double sumM = mrr*mrr;
    for (size_t d = 0; d < r; d++){
        double tmpVal = Mrs->value<double>(_linearizeIndex_Mrs(r, d));
        sumM += tmpVal*tmpVal;
    }
    return sumM;
}


template <typename T> double TMultivariateNormal<T>::_calcLLRatioUpdateMu(const double & oldMu, const double & newMu, const double & sumM, size_t N, const double & sumParam, const double & sum, const std::shared_ptr<TMCMCParameterBase> & m){
    return 1. / (2. * m->value<double>() * m->value<double>()) * ( N * (oldMu*oldMu - newMu*newMu) * sumM + 2.*(newMu - oldMu) * (sumM*sumParam + sum) );;
}

template <typename T> double TMultivariateNormal<T>::_calcLLRatioUpdateM(const size_t & N, const double & oldM, const double & newM, const double & sum){
    return N * _D * log(oldM / newM) + 0.5 * (1./(oldM*oldM) - 1./(newM*newM)) * sum;
}

template <class T> double TMultivariateNormal<T>::getSumLogPriorDensity(const std::shared_ptr<TMCMCObservationsBase> &data) {
    throw std::runtime_error("Function 'template <class T> double TMultivariateNormal<T>::getSumLogPriorDensity(const std::shared_ptr<Tdata<T>> & data)' is not implemented for base class TMultivariateNormal!");
}

template <class T> double TMultivariateNormal<T>::getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data) {
    throw std::runtime_error("Function 'template <class T> double TMultivariateNormal<T>::getLogPriorDensityFull(const std::shared_ptr<Tdata<T>> & data)' is not implemented for base class TMultivariateNormal!");
}

template <class T> void TMultivariateNormal<T>::_addToSumMLEMu(std::vector<double> & sums, const TRange & row, const std::shared_ptr<TMCMCObservationsBase> & paramToUse){
    size_t d = 0;
    for (size_t col = row.first; col < row.last; col += row.increment, d++){ // go over all cols of one row (D)
        sums[d] += paramToUse->value<T>(col);
    }
}

template <class T> void TMultivariateNormal<T>::_normalizeSumMLEMu(std::vector<double> & sums, const double & totalNumElements){
    for (size_t d = 0; d < _D; d++){
        sums[d] /= totalNumElements;
    }
}

template <class T> std::vector<double> TMultivariateNormal<T>::_calculateMLEMu(){
    // calculate MLE of mus
    std::vector<double> sums(_D, 0.);
    double totalNumElements = 0.;
    for (auto &param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        for (size_t n = 0; n < paramToUse->dimensions()[0]; n++) { // go over all rows (N)
            TRange row = paramToUse->get1DSlice(1, {n, 0});
            _addToSumMLEMu(sums, row, paramToUse);
        }
        totalNumElements += paramToUse->dimensions()[0];
    }

    // divide
    _normalizeSumMLEMu(sums, totalNumElements);

    return sums;
}

template <class T> void TMultivariateNormal<T>::_addToSumMLESigma(arma::mat & sums, const TRange & row, const std::shared_ptr<TMCMCObservationsBase> & paramToUse, const std::shared_ptr<TMCMCParameterBase> & mus){
    size_t j = 0;
    for (size_t col1 = row.first; col1 < row.last; col1 += row.increment, j++){ // go over all cols of one row (D)
        size_t k = 0;
        for (size_t col2 = row.first; col2 < row.last; col2 += row.increment, k++) { // go over all cols of one row (D)
            sums(j, k) += (paramToUse->value<T>(col1) - mus->value<double>(j)) * (paramToUse->value<T>(col2) - mus->value<double>(k));
        }
    }
}

template <class T> void TMultivariateNormal<T>::_normalizeSumMLESigma(arma::mat & sums, const double & totalNumElements){
    for (size_t j = 0; j < _D; j++){
        for (size_t k = 0; k < _D; k++){
            sums(j, k) /= totalNumElements;
        }
    }
}

template <class T> arma::mat TMultivariateNormal<T>::_calculateMLESigma(const std::shared_ptr<TMCMCParameterBase> & mus){
    // initialize data for sums (matrix)
    arma::mat sums(_D, _D, arma::fill::zeros);

    double totalNumElements = 0.;
    for (auto &param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        for (size_t n = 0; n < paramToUse->dimensions()[0]; n++) { // go over all rows (N)
            TRange row = paramToUse->get1DSlice(1, {n, 0});
            _addToSumMLESigma(sums, row, paramToUse, mus);
        }
        totalNumElements += paramToUse->dimensions()[0];
    }

    // normalize to get MLE of Sigma
    _normalizeSumMLESigma(sums, totalNumElements);

    return sums;
}

template<typename T> arma::mat TMultivariateNormal<T>::_calculateMFromSigma(const arma::mat & Sigma){
    arma::mat invSigma;
    arma::mat SigmaNoisy = Sigma;
    SigmaNoisy.diag() += 0.00000001;
    if (!arma::inv_sympd(invSigma, SigmaNoisy)){ // if it still doesn't work -> throw. Could increase noise to diagonal
        throw std::runtime_error("In function 'template<typename T> arma::mat TMultivariateNormal<T>::_calculateMFromSigma(const arma::mat & Sigma):' could not take inverse of Sigma!");
    };
    arma::mat M;
    if (!arma::chol(M, invSigma, "lower")){
        // failed to do Cholesky decomposition, throw error
        std::cout << "Sigma: " << std::endl;
        std::cout << Sigma << std::endl;
        std::cout << "invSigma: " << std::endl;
        std::cout << invSigma << std::endl;
        throw std::runtime_error("In function 'template<typename T> arma::mat TMultivariateNormal<T>::_calculateMFromSigma(const arma::mat & Sigma):' could not do Cholesky decomposition of Sigma!");
    }
    return M;
}

template<typename T> void TMultivariateNormal<T>::_fillParametersMFromSigma(const arma::mat & Sigma,
                                                                       std::shared_ptr<TMCMCParameterBase> &Mrr,
                                                                       std::shared_ptr<TMCMCParameterBase> &Mrs,
                                                                       std::shared_ptr<TMCMCParameterBase> &m){
    // calculate M from Sigma
    arma::mat M = _calculateMFromSigma(Sigma);

    // set elements of M (m=1)
    if (_D == 1){
        m->set(1./M(0,0));
    } else {
        m->set(1.);
        for (size_t s = 0; s < _D; s++) {
            for (size_t r = s; r < _D; r++) {
                if (s == r) {
                    Mrr->set(r, M(r, s));
                } else {
                    Mrs->set(_linearizeIndex_Mrs(r, s), M(r, s));
                }
            }
        }
    }
}

template <class T> void TMultivariateNormal<T>::_fillArmadilloMFromParametersM(arma::mat &M,
                                                                 const std::shared_ptr<TMCMCParameterBase> &Mrr,
                                                                 const std::shared_ptr<TMCMCParameterBase> &Mrs,
                                                                 const std::shared_ptr<TMCMCParameterBase> &m) {
    // fills armadillo matrix from current values of MCMCParameters
    for (size_t s = 0; s < _D; s++) {
        for (size_t r = s; r < _D; r++){
            if (r == s){
                M(r, s) = Mrr->value<double>(r);
            } else {
                M(r, s) = Mrs->value<double>(_linearizeIndex_Mrs(r, s));
            }
        }
    }
    // divide all entries by m
    M /= m->value<double>();
}

template <class T> arma::mat TMultivariateNormal<T>::_getArmadilloLForSimulation(const std::shared_ptr<TMCMCParameterBase> &Mrr,
                                                                                  const std::shared_ptr<TMCMCParameterBase> &Mrs,
                                                                                  const std::shared_ptr<TMCMCParameterBase> &m) {
    // first get M and Sigma as armadillo matrices
    arma::mat M(this->_D, this->_D, arma::fill::zeros);
    _fillArmadilloMFromParametersM(M, Mrr, Mrs, m);
    arma::mat Sigma = arma::inv_sympd(M * M.t());

    // now calculate L from Sigma
    arma::mat L = arma::chol(Sigma, "lower");
    return L;
}

template <class T> void TMultivariateNormal<T>::_fillRandomMultivariateValues(std::vector<double> & x, const arma::mat &L, const std::shared_ptr<TMCMCParameterBase> &mus, TRandomGenerator * randomGenerator) {

    // draw random univariate normal values
    std::vector<double> y(_D);
    for (size_t d = 0; d < _D; d++) {
        y[d] = randomGenerator->getNormalRandom(0, 1);
    }

    // calculate x = Ly + mus -> get random multivariate normal values
    for (size_t r = 0; r < this->_D; r++) {
        // calculate Ly (matrix multiplication)
        double sum = 0.;
        for (size_t s = 0; s <= r; s++) {
            sum += L(r, s) * y[s];
        }

        // calculate x[d] = Ly + mus[d]
        x[r] = sum + mus->value<double>(r);
    }
}


template <class T> void TMultivariateNormal<T>::_simulateMultivariateNormal(const std::shared_ptr<TMCMCObservationsBase> &data,
                                                                            size_t curIndex, const arma::mat &L,
                                                                            const std::shared_ptr<TMCMCParameterBase> &mus,
                                                                            TRandomGenerator * randomGenerator) {
    /* according to Numerical Recipes 3rd edition, pp.378:
     * 1) Do Cholesky decomposition to factor Sigma into a left triangular matrix L times its transpose
     * ATTENTION: L != M
     * M is the result of chol(solve(Sigma)), and L is the result of chol(Sigma):
     * Sigma = LL^T
     * We can hence not directly use our M
     * 2) Get a a vector y of independent random univariate normal deviates with mean = 0 and var = 1.
     * 3) To get a multivariate normal deviate x, calculate x = Ly + mu
     */

    // fill a vector (of size D) with random multivariate values
    std::vector<double> x(this->_D);
    _fillRandomMultivariateValues(x, L, mus, randomGenerator);

    // make sure the values are all < boundary.range()
    size_t index = 0;
    while (index < x.size()){
        if(!data->valueIsInsideBoundary(x[index])){ // propose again if value is too large, to prevent overshooting while mirroring
            _fillRandomMultivariateValues(x, L, mus, randomGenerator);
            index = 0; // restart from the beginning, as all values have changed
        } else{
            index++;
        }
    }

    // now set values
    for (size_t d = 0; d < this->_D; d++, curIndex++) {
        data->set(curIndex, x[d]);
    }
}

#endif // WITH_ARMADILLO

//-------------------------------------------
// TMultivariateNormal
//-------------------------------------------

#ifdef WITH_ARMADILLO
template <typename T> TPriorMultivariateNormalWithHyperPrior<T>::TPriorMultivariateNormalWithHyperPrior() : TMultivariateNormal<T>() {
    this->_name = MCMCPrior::multivariateNormal;
}

template <typename T> void TPriorMultivariateNormalWithHyperPrior<T>::initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, std::vector<std::shared_ptr<TObservationsBase> > & observations){
    // we need 1 TMCMCParameter mu (1D), 1 TMCMCParameter m (single), 1 TMCMCParameter Mrr (1D) and 1 TMCMCParameter Mrs (1D)
    this->_checkSizeParams(params.size(), 4);
    this->_checkSizeObservations(observations.size(), 0);

    // initialize parameters: mus, m, M
    _mus = params[0];
    _m = params[1];
    _Mrr = params[2];
    _Mrs = params[3];

    // reSet boundaries of m and Mrr
    _checkMinMaxOfHyperPrior();
}

template <typename T> void TPriorMultivariateNormalWithHyperPrior<T>::_checkMinMaxOfHyperPrior(){
    // parameter _Mrr and _m should always be > 0 -> re-set boundaries of _Mrr and _m
    this->_reSetBoundaryTo0_M_Mrr(_m);
    this->_reSetBoundaryTo0_M_Mrr(_Mrr);
}

template <class T> void TPriorMultivariateNormalWithHyperPrior<T>::constructDAG(TDAG & DAG, TDAG & temporaryDAG){
    this->_constructDAG(DAG, temporaryDAG, {_mus, _m, _Mrr, _Mrs});
}

template <class T> void TPriorMultivariateNormalWithHyperPrior<T>::typesAreCompatible(bool throwIfNot) {
    // parameters on which multivariate normal prior is defined must be floating point type
    this->_checkTypes(throwIfNot, true, false, false);

    // in addition: check if prior parameters have expected type
    // _mus, _m, _Mrr and _Mrs must be floating point
    this->_checkTypesOfPriorParameters(_mus, throwIfNot, true, false, false);
    this->_checkTypesOfPriorParameters(_m, throwIfNot, true, false, false);
    this->_checkTypesOfPriorParameters(_Mrr, throwIfNot, true, false, false);
    this->_checkTypesOfPriorParameters(_Mrs, throwIfNot, true, false, false);

    // check if the prior distribution on the prior parameters is ok
    _mus->typesAreCompatible(throwIfNot);
    _m->typesAreCompatible(throwIfNot);
    _Mrr->typesAreCompatible(throwIfNot);
    _Mrs->typesAreCompatible(throwIfNot);
}

template <class T> void TPriorMultivariateNormalWithHyperPrior<T>::initializeStorageOfPriorParameters() {
    // derive D from dimensionality of parameters below
    this->_calculateAndSetD();

    const std::shared_ptr<TNamesEmpty> dimName = this->_getDimensionName();

    this->_setDimensionsOfMu(_mus, dimName);
    this->_setDimensionsOfM(_m);
    this->_setDimensionsOfMrr(_Mrr, dimName);
    this->_setDimensionsOfMrs(_Mrs, dimName);
}

template <typename T> double TPriorMultivariateNormalWithHyperPrior<T>::_getLogPriorDensity_vec(const std::shared_ptr<TMCMCObservationsBase> & data, const size_t & index){
    // index corresponds to index in linear array
    // -> calculate index in multi-array:
    std::vector<size_t> subscript_updatedParam = data->getSubscripts(index);
    TRange row = data->get1DSlice(1, {subscript_updatedParam[0], 0}); // one row of Tdata

    return this->_calcLogPriorDensity(data, row, _mus, _m, _Mrr, _Mrs);
}

template <typename T> double TPriorMultivariateNormalWithHyperPrior<T>::_getLogPriorDensityOld_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index){
    // index corresponds to index in linear array
    // -> calculate index in multi-array:
    std::vector<size_t> subscript_updatedParam = data->getSubscripts(index);
    TRange row = data->get1DSlice(1, {subscript_updatedParam[0], 0}); // one row of Tdata

    return this->_calcLogPriorDensityOld(data, row, subscript_updatedParam[1], _mus, _m, _Mrr, _Mrs);
}

template <typename T> double  TPriorMultivariateNormalWithHyperPrior<T>::_getLogPriorRatio_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index){
    // index corresponds to index in linear array
    // -> calculate index in multi-array:
    std::vector<size_t> subscript_updatedParam = data->getSubscripts(index);
    TRange row = data->get1DSlice(1, {subscript_updatedParam[0], 0}); // one row of Tdata
    size_t d = subscript_updatedParam[1];

    return this->_calcLogPriorRatio(data, row, d, _mus, _m, _Mrr, _Mrs);
}

template <typename T> void TPriorMultivariateNormalWithHyperPrior<T>::updateParams(){
    // update mus
    for (size_t d = 0; d < this->_D; d++){
        if (_mus->update(d))
            _updateMu(d);
    }
    // update m
    if (_m->update())
        _updateM();
    // update Mrr and Mrs (only if >1 dimensions to avoid overfitting)
    if (this->_D > 1) {
        for (size_t s = 0; s < this->_D; s++) {
            for (size_t r = s; r < this->_D; r++) {
                if (s == r) {
                    if (_Mrr->update(r))
                        _updateMrr(r);
                } else {
                    if (_Mrs->update(this->_linearizeIndex_Mrs(r, s)))
                        _updateMrs(r, s);
                }
            }
        }
    }
}

template <typename T> void TPriorMultivariateNormalWithHyperPrior<T>::_updateMrr(const size_t & r) {
    // compute log(Hastings ratio)
    double logH = _calcLogHUpdateMrr(r);
    // accept or reject
    _Mrr->acceptOrReject(r, logH);
}

template <typename T> double TPriorMultivariateNormalWithHyperPrior<T>::_calcLogHUpdateMrr(const size_t & r){
    // compute log likelihood
    double LL = _calcLLUpdateMrr(r);
    // compute prior
    double logPrior = _Mrr->getLogPriorRatio(r);
    return LL + logPrior;
}

template <typename T> double TPriorMultivariateNormalWithHyperPrior<T>::_calcLLUpdateMrr(const size_t & r){
    // compute log likelihood
    double LL = 0;

    double oldMrr = _Mrr->oldValue<double>(r);
    double newMrr = _Mrr->value<double>(r);
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        size_t N = paramToUse->dimensions()[0];

        double sumParam_nd_minus_mu_d = 0.;
        double sum = 0.;
        for (size_t n = 0; n < N; n++) {
            TRange row = paramToUse->get1DSlice(1, {n, 0});

            // calculate simplified ratio
            sum += this->_calcDoubleSum_updateMrr(paramToUse, row, r, sumParam_nd_minus_mu_d, _mus, _Mrs);
        }

        LL += this->_calcLLRatioUpdateMrr(N, oldMrr, newMrr, sum, sumParam_nd_minus_mu_d, _m);
    }
    return LL;
}

template <typename T> void TPriorMultivariateNormalWithHyperPrior<T>::_updateMrs(const size_t & r, const size_t & s) {
    // compute log(Hastings ratio)
    double logH = _calcLogHUpdateMrs(r, s);
    // accept or reject
    _Mrs->acceptOrReject(this->_linearizeIndex_Mrs(r, s), logH);
}

template <typename T> double TPriorMultivariateNormalWithHyperPrior<T>::_calcLogHUpdateMrs(const size_t & r, const size_t & s){
    // compute log likelihood
    double LL = _calcLLUpdateMrs(r, s);
    // compute prior
    double logPrior = _Mrs->getLogPriorRatio(this->_linearizeIndex_Mrs(r, s));
    return LL + logPrior;
}

template <typename T> double TPriorMultivariateNormalWithHyperPrior<T>::_calcLLUpdateMrs(const size_t & r, const size_t & s) {
    // compute log likelihood
    double LL = 0;

    size_t linearIndex = this->_linearizeIndex_Mrs(r, s);
    double oldMrs = _Mrs->oldValue<double>(linearIndex);
    double newMrs = _Mrs->value<double>(linearIndex);

    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        size_t N = paramToUse->dimensions()[0];

        double sum = 0.;
        double sumParam_nd_minus_mu_d = 0.;
        for (size_t n = 0; n < N; n++) {
            TRange row = paramToUse->get1DSlice(1, {n, 0});

            // calculate simplified ratio
            sum += this->_calcDoubleSum_updateMrs(paramToUse, row, r, s, sumParam_nd_minus_mu_d, _mus, _Mrr, _Mrs);
        }

        LL += this->_calcLLRatioUpdateMrs(oldMrs, newMrs, sum, sumParam_nd_minus_mu_d, _m);
    }
    return LL;
}

template <typename T> void TPriorMultivariateNormalWithHyperPrior<T>::_updateMu(const size_t & r){
    // compute log(Hastings ratio)
    double logH = _calcLogHUpdateMu(r);
    // accept or reject
    _mus->acceptOrReject(r, logH);
}

template <typename T> double TPriorMultivariateNormalWithHyperPrior<T>::_calcLogHUpdateMu(const size_t & r) {
    // compute log likelihood
    double LL = _calcLLUpdateMu(r);
    // compute prior
    double logPrior = _mus->getLogPriorRatio(r);
    return LL + logPrior;
}

template <typename T> double TPriorMultivariateNormalWithHyperPrior<T>::_calcLLUpdateMu(const size_t & r) {
    // compute log likelihood
    double LL = 0.;

    double oldMu = _mus->oldValue<double>(r);
    double newMu = _mus->value<double>(r);
    double mrr = _Mrr->value<double>(r);

    size_t min_r_DMin1 = std::min(r+1, static_cast<size_t>(this->_D-1));

    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        size_t N = paramToUse->dimensions()[0];

        double sum = 0.;
        double sumParam = 0.;
        for (size_t n = 0; n < N; n++) {
            TRange row = paramToUse->get1DSlice(1, {n, 0});

            // calculate simplified ratio
            sum += this->_calcDoubleSum_updateMu(paramToUse, row, r, min_r_DMin1, _mus, _Mrr, _Mrs);
            sumParam += paramToUse->value<T>(row.first + r * row.increment);
        }
        double sumM = this->_calcSumM_dr_squared(mrr, r, _Mrs);

        LL += this->_calcLLRatioUpdateMu(oldMu, newMu, sumM, N, sumParam, sum, _m);
    }
    return LL;
}

template <typename T> void TPriorMultivariateNormalWithHyperPrior<T>::_updateM(){
    // compute log(Hastings ratio)
    double logH = _calcLogHUpdateM();
    // accept or reject
    _m->acceptOrReject(logH);
}

template <typename T> double TPriorMultivariateNormalWithHyperPrior<T>::_calcLogHUpdateM() {
    // compute log likelihood
    double LL = _calcLLUpdateM();
    // compute prior
    double logPrior = _m->getLogPriorRatio();
    return LL + logPrior;
}

template <typename T> double TPriorMultivariateNormalWithHyperPrior<T>::_calcLLUpdateM() {
    // compute log likelihood
    double LL = 0;

    double oldM = _m->oldValue<double>();
    double newM = _m->value<double>();

    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        size_t N = paramToUse->dimensions()[0];

        double sum = 0.;
        for (size_t n = 0; n < paramToUse->dimensions()[0]; n++) {
            TRange row = paramToUse->get1DSlice(1, {n, 0});

            // calculate
            // sum_s (sum_r mrs(beta_nr - mu_r)^2)
            sum += this->_calcDoubleSum(paramToUse, row, _mus, _Mrr, _Mrs);
        }

        LL += this->_calcLLRatioUpdateM(N, oldM, newM, sum);
    }
    return LL;
}

template <class T> double TPriorMultivariateNormalWithHyperPrior<T>::getSumLogPriorDensity(const std::shared_ptr<TMCMCObservationsBase> & data) {
    double sum = 0.;
    // always take first element of each row
    for (size_t n = 0; n < data->dimensions()[0]; n++) {
        TRange row = data->get1DSlice(1, {n, 0});
        sum += _getLogPriorDensity_vec(data, row.first);
    }
    return sum;
}

template <class T> double TPriorMultivariateNormalWithHyperPrior<T>::getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data) {
    // returns sum of log-density of array + log density of mu + log density of m + log density of mrr + log density of mrs
    if (this->_D == 1)
        return this->getSumLogPriorDensity(data) + _mus->getLogPriorDensityFull() + _m->getLogPriorDensity(); // Mrr and Mrs are not learned for if one-dimensional
    else
        return this->getSumLogPriorDensity(data) + _mus->getLogPriorDensityFull() + _m->getLogPriorDensity() + _Mrr->getLogPriorDensityFull() + _Mrs->getLogPriorDensityFull();
}

template <class T> void TPriorMultivariateNormalWithHyperPrior<T>::_setMusToMLE(){
    // calculate MLE of mus
    std::vector<double> means = this->_calculateMLEMu();

    // set each mus to its MLE
    for (size_t d = 0; d < this->_D; d++){
        _mus->set(d, means[d]);
    }
}

template <class T> void TPriorMultivariateNormalWithHyperPrior<T>::_setMToMLE(){
    // calculate MLE of Sigma
    arma::mat Sigma = this->_calculateMLESigma(_mus);

    this->_fillParametersMFromSigma(Sigma, _Mrr, _Mrs, _m);
}

template <class T> void TPriorMultivariateNormalWithHyperPrior<T>::estimateInitialPriorParameters(TLog * Logfile) {
    if (!_mus->hasFixedInitialValue()) {
        _setMusToMLE();
    }

    if (!_Mrr->hasFixedInitialValue() || !_Mrs->hasFixedInitialValue() || !_m->hasFixedInitialValue()) { // at least one should be estimated
        if (!_Mrr->hasFixedInitialValue() && !_Mrs->hasFixedInitialValue() && !_m->hasFixedInitialValue()) {
            // all are estimated -> ok
            _setMToMLE();
        } else {
            throw "Error when initializing " + this->_name + " prior: parameters m, Mrr and Mrs must either all or none have an initial value. Can not initialize only partially!";
        }
    }
}


template <class T> void TPriorMultivariateNormalWithHyperPrior<T>::simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) {
    // calculate L for simulation
    arma::mat L = this->_getArmadilloLForSimulation(_Mrr, _Mrs, _m);

    // get multivariate normal random numbers
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        if (!paramToUse->hasFixedInitialValue()) { // only simulate if parameter has not a fixed value
            assert(paramToUse->dimensions()[1] == this->_D); // number of columns of data must match D

            for (size_t n = 0; n < paramToUse->dimensions()[0]; n++) { // go over all rows
                TRange row = paramToUse->get1DSlice(1, {n, 0});
                this->_simulateMultivariateNormal(paramToUse, row.first, L, _mus, RandomGenerator);
            }
        }
    }
}


#endif // WITH_ARMADILLO
