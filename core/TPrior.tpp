
//-------------------------------------------
// TPrior
//-------------------------------------------
template <class T> std::vector<double> TPrior<T>::_processPriorParameterString(const std::string& params, int expectedSize, bool check){
    // split string param1,param2 into two doubles
    std::vector<std::string> temp;
    fillContainerFromStringAny(params, temp, ",", true);
    if (check && temp.size() != expectedSize) {
        throw "Parameters of " + _name + " prior have wrong format. Provided number of parameters (" +
              toString(temp.size()) + ") deviates from expected number (" + toString(expectedSize) + ").";
    }
    std::vector<double> doubleVec;
    for (auto &it: temp){
        try {
            auto d = convertStringCheck<double>(it);
            doubleVec.push_back(d);
        } catch (...) {
            throw "Parameters of " + _name + " prior have invalid format (" + params + "). Provide parameters as single argument for single-parameter priors, or as comma-separated list for multi-parameter priors.";
        }
    }
    return doubleVec;
}

template <class T> bool TPrior<T>::_allParamChildrenExistInDAG(const TDAG &DAG){
    // go through all _parameters and check if they already exist in DAG
    for (auto & param : this->_parameters){
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        if (!DAG.nodeExists(paramToUse->name())){
            return false;
        }
    }
    return true;
}

template <class T> void TPrior<T>::_addFirstChildToDAG(TDAG &DAG){
    // add first parameter of _parameter vector to final DAG
    std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(this->_parameters[0]);
    DAG.add(paramToUse);
}

template <class T> void TPrior<T>::_constructDAG(TDAG &DAG, TDAG & temporaryDAG, const std::vector<std::shared_ptr<TMCMCObservationsBase>> & PriorParameters){
    if (_allParamChildrenExistInDAG(temporaryDAG)){
        _addFirstChildToDAG(DAG);

        // go over all prior parameters
        for (auto & priorParam : PriorParameters){
            priorParam->constructDAG(DAG, temporaryDAG);
        }
    }
}

template <class T> void TPrior<T>::constructDAG(TDAG &DAG, TDAG & temporaryDAG) {
    // TPrior classes that have no prior parameters that should be learned
    // just call _constructDAG but give empty prior parameter vector -> recursion will stop there
    _constructDAG(DAG, temporaryDAG, {});
}

template <class T> double TPrior<T>::_getLogPriorDensity_oneVal(T x){
    return 0.0;
};

template <class T> double TPrior<T>::_getLogPriorRatio_oneVal(T x, T x_old){
    return 0.0;
};

template <class T> double TPrior<T>::_getLogPriorRatio_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index){
    return getLogPriorRatio(data->value<T>(index), data->oldValue<T>(index));
};

template <class T> double TPrior<T>::_getLogPriorDensity_vec(const std::shared_ptr<TMCMCObservationsBase> & data, const size_t & index){
    return getLogPriorDensity(data->value<T>(index));
};

template <class T> double TPrior<T>::_getLogPriorDensityOld_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index){
    return getLogPriorDensity(data->oldValue<T>(index));
};

template <class T> void TPrior<T>::initialize(const std::string & params){};

template <class T> void TPrior<T>::initializeStorageOfPriorParameters() {
    // function can stay empty if there are not prior parameters
}

template <class T> void TPrior<T>::initParameter(const std::shared_ptr<TMCMCObservationsBase> & parameter){
    _parameters.push_back(parameter);
}

template <class T>  bool TPrior<T>::checkMinMax(std::string &min, std::string &max, bool & minIsIncluded, bool & maxIsIncluded, bool & hasDefaultMin, bool & hasDefaultMax){
    return true;
};

template <class T> void TPrior<T>::_checkTypes(bool throwIfNot, bool mustBeFloatingPoint, bool mustBeIntegral, bool mustBeUnsigned) {
    // go over all parameters on which the prior is defined
    // and check if they are of a compatible type (e.g. normal prior only supports floating point numbers)
    if (throwIfNot) { // possibility ignore the check
        for (auto & param : this->_parameters) {
            std::shared_ptr<TMCMCObservationsBase> paramToUse = _getSharedPtrParam(param);
            std::string errorString = "In function 'void TPrior<T>::typesAreCompatible(bool throwIfNot, bool mustBeFloatingPoint, bool mustBeIntegral, bool mustBeSigned)': prior " + _name + " on parameter " + paramToUse->name() + " is not compatible with type " + typeid(T).name() + ". ";
            if (mustBeFloatingPoint && !paramToUse->isFloatingPoint()) {
                throw std::runtime_error(errorString + "Type must be floating point.");
            } if (mustBeIntegral && !paramToUse->isIntegral()) {
                throw std::runtime_error(errorString + "Type must be integral.");
            } if (mustBeUnsigned && !paramToUse->isUnsigned()) {
                throw std::runtime_error(errorString + "Type must be unsigned.");
            }
        }
    }
}

template <typename T> std::shared_ptr<TMCMCObservationsBase> TPrior<T>::_getSharedPtrParam(std::weak_ptr<TMCMCObservationsBase> & param){
    std::shared_ptr<TMCMCObservationsBase> paramToUse = param.lock();
    if (paramToUse) {
        return paramToUse;
    } else throw std::runtime_error("In function 'std::shared_ptr<TMCMCObservationsBase> & _getSharedPtrParam(std::weak_ptr<TMCMCObservationsBase> & param)': can not return shared ptr paramToUse! Has it been deleted in the meantime?");
}

template <class T> void TPrior<T>::typesAreCompatible(bool throwIfNot) {
   throw std::runtime_error("Function 'template <class T> void TPrior<T>::typesAreCompatible(bool throwIfNot)' not implemented for base class TPrior!");
}

template <class T>
void TPrior<T>::simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) {
    throw std::runtime_error("Function 'template <class T> void TPrior<T>::simulateUnderPrior(TRandomGenerator *randomGenerator, TLog *logfile)' not implemented for base class TPrior!");
}

template<typename T> void TPrior<T>::switchPriorClassificationAfterEM(TLog * Logfile) {
    throw std::runtime_error("Function 'template<typename T> void TPrior<T>::switchPriorClassificationAfterEM() is not implemented for prior " + _name + "!");
}

template <class T> std::string TPrior<T>::name(){
    return _name;
};

template <class T> double TPrior<T>::getLogPriorDensity(T x){
    return _getLogPriorDensity_oneVal(x);
};

template <class T> double TPrior<T>::getLogPriorRatio(T x, T x_old){
    return _getLogPriorRatio_oneVal(x, x_old);
};

template <class T> double TPrior<T>::getLogPriorDensity(const std::shared_ptr<TMCMCObservationsBase> & data, const size_t & index){
    return _getLogPriorDensity_vec(data, index);
}

template <class T> double TPrior<T>::getLogPriorDensityOld(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index) {
    return _getLogPriorDensityOld_vec(data, index);
}

template <class T> double TPrior<T>::getLogPriorRatio(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index){
    return _getLogPriorRatio_vec(data, index);
};

template <class T> double TPrior<T>::getSumLogPriorDensity(const std::shared_ptr<TMCMCObservationsBase> & data) {
    double sum = 0.;
    TRange range = data->getFull();
    for (size_t i = range.first; i < range.last; i += range.increment) {
        sum += this->getLogPriorDensity(data->value<T>(i));
    }
    return sum;
}

template <class T> double TPrior<T>::getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data) {
    return getSumLogPriorDensity(data);
}

//----------------------------------------------
template <class T> TPriorUniform<T>::TPriorUniform(){
    this->_name = MCMCPrior::uniform;
    _min = _max = _logDensity = 0;
}

template <class T> void TPriorUniform<T>::initialize(const std::string & params){
    // parameter string should be empty; if uniform should have min/max, this should be specified via checkMinMax
    std::vector<double> tmp = this->_processPriorParameterString(params, 0);
}

template <class T> bool TPriorUniform<T>::checkMinMax(std::string &min, std::string &max, bool & minIsIncluded, bool & maxIsIncluded, bool & hasDefaultMin, bool & hasDefaultMax){
    _min = convertString<T>(min);
    _max = convertString<T>(max);
    _logDensity = log(1.0 / (_max - _min));
    if (!std::isfinite(_logDensity))
        _logDensity = 0.0; // prevent problems with if min,max are very large -> log(1./(inf-(-inf))) = -inf
    return true;
}

template <class T> void TPriorUniform<T>::typesAreCompatible(bool throwIfNot) {
    // all types are compatible with uniform prior!
}

template <class T> double TPriorUniform<T>::_getLogPriorDensity_oneVal(T x){
    return _logDensity;
}

template <class T> double TPriorUniform<T>::_getLogPriorRatio_oneVal(T x, T x_old){
    return 0.;
}

template <class T>
void TPriorUniform<T>::simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) {
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        if (!paramToUse->hasFixedInitialValue()) { // only simulate if parameter has not a fixed value
            TRange range = paramToUse->getFull();
            for (size_t i = range.first; i < range.last; i += range.increment) {
                double value = RandomGenerator->getRand();
                // stretch to be between min and max
                // if min and max are large (e.g. default values for double), this will result in crazy large values
                // that lead to downstream problems
                // therefore, we cut at [-1000:1000]
                double max = paramToUse->max();
                double min = paramToUse->min();
                if (max > cutoffSimulateUniformPrior) {
                    Logfile->warning("Max of uniform prior " + toString(max) + " is larger than " +
                                     toString(cutoffSimulateUniformPrior) + ". Will restrict it to " +
                                     toString(cutoffSimulateUniformPrior) +
                                     ". Increase this cutoff or choose better prior if needed!");
                    max = cutoffSimulateUniformPrior;
                }
                if (min < -cutoffSimulateUniformPrior) {
                    Logfile->warning("Min of uniform prior " + toString(min) + " is smaller than -" +
                                     toString(cutoffSimulateUniformPrior) + ". Will restrict it to " +
                                     toString(cutoffSimulateUniformPrior) +
                                     ". Increase this cutoff or choose better prior if needed!");
                    min = -cutoffSimulateUniformPrior;
                }
                value = (value * (max - min) + min);
                paramToUse->set(i, static_cast<T>(value));
            }
        }
    }
}

//----------------------------------------------
template <class T> TPriorNormal<T>::TPriorNormal(){
    this->_name = MCMCPrior::normal;
    _mean = _var = _twoVar = 0.;
    _sqrtTwoPi = 2.506628274631000241612; // = sqrt(2.*3.1415)
}

template <class T> void TPriorNormal<T>::initialize(const std::string & params){
    // normal(mean,var)
    std::vector<double> tmp = this->_processPriorParameterString(params, 2);
    _mean = tmp.at(0);
    _var = tmp.at(1);
    if (_var <= 0.)
        throw "Parameter var of normal distribution must be > 0!";

    _twoVar = 2. * _var;
}

template <class T> void TPriorNormal<T>::typesAreCompatible(bool throwIfNot) {
    // parameters on which normal prior is defined must be floating point type
    this->_checkTypes(throwIfNot, true, false, false);
}

template <class T> double TPriorNormal<T>::_getLogPriorDensity_oneVal(T x){
    // note: expensive to calculate, use getLogPriorRatio() whenever possible!
    return TNormalDistr::density(x, _mean, sqrt(_var), true);
}

template <class T> double TPriorNormal<T>::_getLogPriorRatio_oneVal(T x, T x_old){
    double tmp = (x-_mean);
    double tmp_old = (x_old-_mean);
    return (tmp_old * tmp_old - tmp * tmp) / _twoVar;
}

template <class T> double TPriorNormal<T>::mean() const{
    return _mean;
}

template <class T> double TPriorNormal<T>::var() const {
    return _var;
}

template <class T> void TPriorNormal<T>::simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) {
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        if (!paramToUse->hasFixedInitialValue()) { // only simulate if parameter has not a fixed value
            TRange range = paramToUse->getFull();
            for (size_t i = range.first; i < range.last; i += range.increment) {
                double value = RandomGenerator->getNormalRandom(_mean, sqrt(_var));
                while (!paramToUse->valueIsInsideBoundary(value)) { // propose again if value is too large, to prevent overshooting while mirroring
                    value = RandomGenerator->getNormalRandom(_mean, sqrt(_var));
                }
                // set value
                paramToUse->set(i, value);
            }
        }
    }
}

//----------------------------------------------
template <class T> TPriorExponential<T>::TPriorExponential(){
    this->_name = MCMCPrior::exponential;
    _lambda = _logLambda = 0.;
}

template <class T> void TPriorExponential<T>::initialize(const std::string & params){
    // exponential(lambda)
    std::vector<double> tmp = this->_processPriorParameterString(params, 1);
    _lambda = tmp.at(0);
    if (_lambda <= 0)
        throw "Parameter lambda of exponential distribution must be > 0!";
    _logLambda = log(_lambda);
}

template <class T> bool TPriorExponential<T>::checkMinMax(std::string &min, std::string &max, bool & minIsIncluded, bool & maxIsIncluded, bool & hasDefaultMin, bool & hasDefaultMax) {
    if (convertString<T>(min) < 0.) {
        hasDefaultMin = false;
        min = "0.";
        minIsIncluded = true;
        return false;
    }
    return true;
}

template <class T> void TPriorExponential<T>::typesAreCompatible(bool throwIfNot) {
    // parameters on which exponential prior is defined must be floating point type
    this->_checkTypes(throwIfNot, true, false, false);
}

template <class T> double TPriorExponential<T>::_getLogPriorDensity_oneVal(T x){
    // note: expensive to calculate, use getLogPriorRatio() whenever possible!
    return TExponentialDistr::density(x, _lambda, true);
}

template <class T> double TPriorExponential<T>::_getLogPriorRatio_oneVal(T x, T x_old){
    return _lambda * (x_old - x);
}

template <class T> double TPriorExponential<T>::lambda() const {
    return _lambda;
}

template <class T> void TPriorExponential<T>::simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) {
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        if (!paramToUse->hasFixedInitialValue()) { // only simulate if parameter has not a fixed value
            TRange range = paramToUse->getFull();
            for (size_t i = range.first; i < range.last; i += range.increment) {
                double value = RandomGenerator->getExponentialRandom(_lambda);
                while (!paramToUse->valueIsInsideBoundary(value)) { // propose again if value is too large, to prevent overshooting while mirroring
                    value = RandomGenerator->getExponentialRandom(_lambda);
                }
                // set value
                paramToUse->set(i, value);
            }
        }
    }
}

//----------------------------------------------
template <class T> TPriorChisq<T>::TPriorChisq(){
    this->_name = MCMCPrior::chisq;
    _k = 0;
    _kDiv2Min1 = 0.;
}

template <class T> void TPriorChisq<T>::initialize(const std::string & params){
    // chisq(k)
    std::vector<double> tmp = this->_processPriorParameterString(params, 1);
    if (tmp.at(0) <= 0)
        throw "Parameter k of Chisq distribution must be > 0!";
    _k = static_cast<unsigned int>(tmp.at(0));
    if (static_cast<double>(_k) != tmp.at(0))
        throw "Parameter k of Chisq distribution must integer!";
    _kDiv2Min1 = (_k/2. - 1.);
}

template <class T> bool TPriorChisq<T>::checkMinMax(std::string &min, std::string &max, bool &minIsIncluded, bool &maxIsIncluded, bool & hasDefaultMin, bool & hasDefaultMax) {
    // If k=1, chisq distribution must be > 0. If k > 1, it must be >= 0.
    if (convertString<T>(min) < 0.) {
        hasDefaultMin = false;
        min = "0.";
        minIsIncluded = (_k != 1);
        return false;
    }
    if (convertString<T>(min) == 0 && _k == 1 && minIsIncluded){
        hasDefaultMin = false;
        minIsIncluded = false;
        return false;
    }
    return true;
}

template <class T> void TPriorChisq<T>::typesAreCompatible(bool throwIfNot) {
    // parameters on which chisq prior is defined must be floating point type
    this->_checkTypes(throwIfNot, true, false, false);
}

template <class T> double TPriorChisq<T>::_getLogPriorDensity_oneVal(T x){
    // note: expensive to calculate, use getLogPriorRatio() whenever possible!
    return TChisqDistr::density(x, _k, true);
}

template <class T> double TPriorChisq<T>::_getLogPriorRatio_oneVal(T x, T x_old){
    return _kDiv2Min1 * (log(x) - log(x_old)) + (x_old - x)/2.;
}

template <class T> uint32_t TPriorChisq<T>::k() const{
    return _k;
}

template <class T> void TPriorChisq<T>::simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) {
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        if (!paramToUse->hasFixedInitialValue()) { // only simulate if parameter has not a fixed value
            TRange range = paramToUse->getFull();
            for (size_t i = range.first; i < range.last; i += range.increment) {
                double value = RandomGenerator->getChisqRand(_k);
                while (!paramToUse->valueIsInsideBoundary(value)) { // propose again if value is too large, to prevent overshooting while mirroring
                    value = RandomGenerator->getChisqRand(_k);
                }
                // set value
                paramToUse->set(i, value);
            }
        }
    }
}

//-------------------------------------------
// TPriorMultivariateChisq
//-------------------------------------------
template<typename T> TPriorMultivariateChisq<T>::TPriorMultivariateChisq() : TPrior<T>() {
    this->_name = MCMCPrior::multivariateChisq;
    _D = 0;
}

template <typename T> void TPriorMultivariateChisq<T>::initialize(const std::string & params){
    // chisq(k1, k2, k3, ..., kD)
    std::vector<double> tmp = this->_processPriorParameterString(params, 1, false);
    for (auto & k : tmp){
        _ks.push_back(static_cast<uint64_t>(k));

        // check if valid
        if (_ks.back() <= 0)
            throw "Parameter k of Chisq distribution must be > 0!";
        if (static_cast<double>(_ks.back()) != k)
            throw "Parameter k of Chisq distribution must integer!";
    }

    // store variables often used in calculations
    _D = _ks.size();
}

template <class T> bool TPriorMultivariateChisq<T>::checkMinMax(std::string &min, std::string &max, bool &minIsIncluded, bool &maxIsIncluded, bool & hasDefaultMin, bool & hasDefaultMax) {
    // If k=1, chisq distribution must be > 0. If k > 1, it must be >= 0.
    bool thereAreKsWithValue1 = false;
    for (size_t d = 0; d < _D; d++){
        if (_ks[d] == 1) {
            thereAreKsWithValue1 = true;
            break;
        }
    }

    if (convertString<T>(min) < 0.) {
        hasDefaultMin = false;
        min = "0.";
        if (thereAreKsWithValue1) minIsIncluded = false;
        else minIsIncluded = true;
        return false;
    }
    if (convertString<T>(min) == 0 && thereAreKsWithValue1 && minIsIncluded){
        hasDefaultMin = false;
        minIsIncluded = false;
        return false;
    }
    return true;
}

template <class T> void TPriorMultivariateChisq<T>::initializeStorageOfPriorParameters() {
    // check if size of parameter matches size of k
    for (auto &param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        if (paramToUse->totalSize() != _D) {
            // does not match -> set K from totalSize
            _fillK_fromDimensions(paramToUse->totalSize());
        }
    }
}

template <class T> void TPriorMultivariateChisq<T>::_fillK_fromDimensions(const size_t & D) {
    _D = D;
    _ks.clear();
    for (int r = D; r >= 1; r--){
        _ks.push_back(r);
    }
}

template <class T> void TPriorMultivariateChisq<T>::typesAreCompatible(bool throwIfNot) {
    // parameters on which multivariate chisq prior is defined must be floating point type
    this->_checkTypes(throwIfNot, true, false, false);
}

template <typename T> double TPriorMultivariateChisq<T>::_getLogPriorDensity_oneVal(T x){
    throw std::runtime_error("Elements in multidimensional chisq array are non-iid -> do not call function _getLogPriorDensity_oneVal(T x) with single elements!");
}

template <typename T> double TPriorMultivariateChisq<T>::_getLogPriorRatio_oneVal(T x, T x_old){
    throw std::runtime_error("Elements in multidimensional chisq array are non-iid -> do not call function _getLogPriorDensity_oneVal(T x) with single elements!");
}

template <typename T> double TPriorMultivariateChisq<T>::_getLogPriorDensity_vec(const std::shared_ptr<TMCMCObservationsBase> & data, const size_t & index){
    return TChisqDistr::density(data->value<T>(index), _ks[index], true);
}

template <typename T> double TPriorMultivariateChisq<T>::_getLogPriorDensityOld_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index){
    return TChisqDistr::density(data->oldValue<T>(index), _ks[index], true);
}

template <typename T> double  TPriorMultivariateChisq<T>::_getLogPriorRatio_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index){
    return (_ks[index]/2. - 1.) * (log(data->value<T>(index)) - log(data->oldValue<T>(index))) + (data->oldValue<T>(index) - data->value<T>(index))/2.;
}

template <class T> double TPriorMultivariateChisq<T>::getSumLogPriorDensity(const std::shared_ptr<TMCMCObservationsBase> & data) {
    double sum = 0.;
    // go over each element of data
    for (size_t d = 0; d < data->totalSize(); d++) {
        sum += _getLogPriorDensity_vec(data, d);
    }
    return sum;
}

template <class T> void TPriorMultivariateChisq<T>::simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) {
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        if (!paramToUse->hasFixedInitialValue()) { // only simulate if parameter has not a fixed value
            assert(paramToUse->totalSize() == _D);

            TRange range = paramToUse->getFull();
            for (size_t i = range.first; i < range.last; i += range.increment) {
                double value = RandomGenerator->getChisqRand(_ks[i]);
                while (!paramToUse->valueIsInsideBoundary(value)) { // propose again if value is too large, to prevent overshooting while mirroring
                    value = RandomGenerator->getChisqRand(_ks[i]);
                }
                // set value
                paramToUse->set(i, value);
            }
        }
    }
}


//-------------------------------------------
// TPriorMultivariateChi
//-------------------------------------------
template<typename T> TPriorMultivariateChi<T>::TPriorMultivariateChi() : TPriorMultivariateChisq<T>() {
    this->_name = MCMCPrior::multivariateChi;
}

template <typename T> double TPriorMultivariateChi<T>::_getLogPriorDensity_vec(const std::shared_ptr<TMCMCObservationsBase> & data, const size_t & index){
    return TChiDistr::density(data->value<T>(index), this->_ks[index], true);
}

template <typename T> double TPriorMultivariateChi<T>::_getLogPriorDensityOld_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index){
    return TChiDistr::density(data->oldValue<T>(index), this->_ks[index], true);
}

template <typename T> double  TPriorMultivariateChi<T>::_getLogPriorRatio_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index){
    return (this->_ks[index] - 1.) * (log(data->value<T>(index)) - log(data->oldValue<T>(index))) + (data->oldValue<T>(index)*data->oldValue<T>(index) - data->value<T>(index)*data->value<T>(index))/2.;
}

template <class T> void TPriorMultivariateChi<T>::simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) {
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        if (!paramToUse->hasFixedInitialValue()) { // only simulate if parameter has not a fixed value
            assert(paramToUse->totalSize() == this->_D);

            TRange range = paramToUse->getFull();
            for (size_t i = range.first; i < range.last; i += range.increment) {
                double value = sqrt(RandomGenerator->getChisqRand(this->_ks[i]));
                while (!paramToUse->valueIsInsideBoundary(value)) { // propose again if value is too large, to prevent overshooting while mirroring
                    value = sqrt(RandomGenerator->getChisqRand(this->_ks[i]));
                }
                // set value
                paramToUse->set(i, value);
            }
        }
    }
}

//----------------------------------------------
template <class T> TPriorBeta<T>::TPriorBeta(){
    this->_name = MCMCPrior::beta;
    _beta = _alpha = _betaMin1 = _alphaMin1 = 0.;
}

template <class T> void TPriorBeta<T>::initialize(const std::string & params){
    // beta(alpha,beta)
    std::vector<double> tmp = this->_processPriorParameterString(params, 2);
    _alpha = tmp.at(0);
    _beta = tmp.at(1);
    if (_alpha <= 0. || _beta <= 0.)
        throw "Parameters alpha and beta of Beta distribution must be > 0!";
    _alphaMin1 = _alpha - 1.;
    _betaMin1 = _beta - 1.;
}

template <class T> bool TPriorBeta<T>::checkMinMax(std::string &min, std::string &max, bool &minIsIncluded, bool &maxIsIncluded, bool & hasDefaultMin, bool & hasDefaultMax) {
    // beta distribution must be 0 < x < 1; because of log, 0 and 1 are excluded
    bool didNotChange = true;
    if (convertString<T>(min) < 0. || (convertString<T>(min) == 0 && minIsIncluded)) {
        hasDefaultMin = false;
        min = "0.";
        minIsIncluded = false;
        didNotChange = false;
    }
    if (convertString<T>(max) > 1. || (convertString<T>(max) == 1 && maxIsIncluded)) {
        hasDefaultMax = false;
        max = "1.";
        maxIsIncluded = false;
        didNotChange = false;
    }
    return didNotChange;
}

template <class T> void TPriorBeta<T>::typesAreCompatible(bool throwIfNot) {
    // parameters on which beta prior is defined must be floating point type
    this->_checkTypes(throwIfNot, true, false, false);
}

template <class T> double TPriorBeta<T>::_getLogPriorDensity_oneVal(T x){
    // note: expensive to calculate, use getLogPriorRatio() whenever possible!
    return TBetaDistr::density(x, _alpha, _beta, true);
}

template <class T> double TPriorBeta<T>::_getLogPriorRatio_oneVal(T x, T x_old){
    return _alphaMin1 * (log(x) - log(x_old)) + _betaMin1 * (log(1.-x) - log(1.-x_old));
}

template <class T> double TPriorBeta<T>::alpha() const {
    return _alpha;
}

template <class T> double TPriorBeta<T>::beta() const {
    return _beta;
}

template <class T> void TPriorBeta<T>::simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) {
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        if (!paramToUse->hasFixedInitialValue()) { // only simulate if parameter has not a fixed value
            TRange range = paramToUse->getFull();
            for (size_t i = range.first; i < range.last; i += range.increment) {
                double value = RandomGenerator->getBetaRandom(_alpha, _beta);
                while (!paramToUse->valueIsInsideBoundary(value)) { // propose again if value is too large, to prevent overshooting while mirroring
                    value = RandomGenerator->getBetaRandom(_alpha, _beta);
                }
                // set value
                paramToUse->set(i, value);
            }
        }
    }
}

//----------------------------------------------
template <class T> TPriorBinomial<T>::TPriorBinomial(){
    this->_name = MCMCPrior::binomial;
    _p = 0.;
    _log_p = 0.;
    _log_q = 0.;
}

template <class T> void TPriorBinomial<T>::initialize(const std::string & params){
    // binomial(p)
    std::vector<double> tmp = this->_processPriorParameterString(params, 1);
    // check if p is ok
    _p = tmp.at(0);
    if (_p <= 0. || _p >= 1.){ // shouldn't be exactly 0 or 1 because of log
        throw "Parameter p of a Beta distribution must be 0 < p < 1!";
    }

    // pre-calculate expensive stuff
    _log_p = log(_p);
    _log_q = log(1.-_p);
}

template <class T> bool TPriorBinomial<T>::checkMinMax(std::string &min, std::string &max, bool &minIsIncluded, bool &maxIsIncluded, bool & hasDefaultMin, bool & hasDefaultMax) {
    // n and k of binomial distribution must be >= 0
    if (convertString<double>(min) < 0.) {
        hasDefaultMin = false;
        min = "0";
        minIsIncluded = true;
        return false;
    }
    return true;
}

template <class T> void TPriorBinomial<T>::typesAreCompatible(bool throwIfNot) {
    // parameters on which binomial prior is defined must be unsigned integer
    this->_checkTypes(throwIfNot, false, true, true);
}

template <class T> void TPriorBinomial<T>::initializeStorageOfPriorParameters() {
    // check if size of parameter matches
    // eithter 1-dimensional with 2 elements, n and k
    // or 2-dimensional, where the number of columns is 2 (n and k)
    for (auto &param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        if (paramToUse->numDim() == 1 && paramToUse->totalSize() != 2) {
            throw std::runtime_error("In function 'template <class T> void TPriorBinomial<T>::initializeStorageOfPriorParameters()': Parameter " + paramToUse->name() + " of 1 dimension must have a total size of 2 (not" + toString(paramToUse->totalSize()) + "), where the first element is n and the second element is k!");
        } else if (paramToUse->numDim() == 2 && paramToUse->dimensions()[1] != 2){
            throw std::runtime_error("In function 'template <class T> void TPriorBinomial<T>::initializeStorageOfPriorParameters()': Parameter " + paramToUse->name() + " of 2 dimensions must have exactly 2 columns (not" + toString(paramToUse->dimensions()[1]) + "), where the first element is n and the second element is k!");
        } else if (paramToUse->numDim() > 2){
            throw std::runtime_error("In function 'template <class T> void TPriorBinomial<T>::initializeStorageOfPriorParameters()': Parameter " + paramToUse->name() + " can not have more than 2 dimensions (not " + toString(paramToUse->numDim()) + ").");
        }
    }
}

template <typename T> double TPriorBinomial<T>::_getLogPriorDensity_oneVal(T x){
    throw std::runtime_error("Elements in binomial array are require n and k -> do not call function _getLogPriorDensity_oneVal(T x) with single elements!");
}

template <typename T> double TPriorBinomial<T>::_getLogPriorRatio_oneVal(T x, T x_old){
    throw std::runtime_error("Elements in binomial array are require n and k -> do not call function _getLogPriorRatio_oneVal(T x) with single elements!");
}

template <typename T> double TPriorBinomial<T>::_getLogPriorDensity_vec(const std::shared_ptr<TMCMCObservationsBase> & data, const size_t & index){
    T n = data->value<T>(index);
    T k = data->value<T>(index+1);
    assert(n >= k);

    return TBinomialDistr::density(n, k, _p, true);
}

template <typename T> double TPriorBinomial<T>::_getLogPriorDensityOld_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index){
    // THIS ASSUMES ONLY K HAS CHANGED, N REMAINS THE SAME!
    T n = data->value<T>(index);
    T oldK = data->oldValue<T>(index+1);
    assert(n >= oldK);

    return TBinomialDistr::density(n, oldK, _p, true);
}

template <typename T> double  TPriorBinomial<T>::_getLogPriorRatio_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index){
    // THIS ASSUMES ONLY K HAS CHANGED, N REMAINS THE SAME!
    uint64_t n = data->value<uint64_t>(index);
    uint64_t newK = data->value<uint64_t>(index+1);
    uint64_t oldK = data->oldValue<uint64_t>(index+1);
    assert(n >= newK);
    assert(n >= oldK);

    return chooseLog(n, newK) - chooseLog(n,  oldK) + (static_cast<int>(newK)-static_cast<int>(oldK))*_log_p + (static_cast<int>(oldK)-static_cast<int>(newK))*_log_q;
}

template <class T> double TPriorBinomial<T>::getSumLogPriorDensity(const std::shared_ptr<TMCMCObservationsBase> & data) {
    double sum = 0.;
    // always take first element of each row
    TRange range = data->get1DSlice(0, {0,0}); // first column (= n)
    for (size_t i = range.first; i < range.last; i += range.increment) {
        sum += _getLogPriorDensity_vec(data, i);
    }
    return sum;
}

template <class T> double TPriorBinomial<T>::p() const {
    return _p;
}

template <class T> void TPriorBinomial<T>::simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) {
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        if (!paramToUse->hasFixedInitialValue()) { // only simulate if parameter has not a fixed value
            long min_n = 0;
            long max_n = paramToUse->max();

            TRange range = paramToUse->get1DSlice(0, {0,0}); // first column (= n)
            for (size_t i = range.first; i < range.last; i += range.increment) {
                // draw random n: uniform between 0 and max
                T n = static_cast<T>(RandomGenerator->getRand(min_n, max_n+1));

                double k = RandomGenerator->getBinomialRand(_p, n);
                while (!paramToUse->valueIsInsideBoundary(k)) { // propose again if value is too large, to prevent overshooting while mirroring
                    k = RandomGenerator->getBinomialRand(_p, n);
                }
                // set value
                paramToUse->set(i, n);
                paramToUse->set(i+1, k);
            }
        }
    }
}

//----------------------------------------------
template <class T> TPriorBernouilli<T>::TPriorBernouilli(){
    this->_name = MCMCPrior::bernouilli;
    _pi = 0.;
    _logPi = 0.;
    _log1MinPi = 0.;
}

template <class T> void TPriorBernouilli<T>::initialize(const std::string & params){
    // bernouilli(pi)
    std::vector<double> tmp = this->_processPriorParameterString(params, 1);
    _pi = tmp.at(0);
    if (_pi <= 0) // exactly 0 is not allowed because log(0) = -Inf
        throw "Parameter pi of bernouilli distribution must be > 0!";
    if (_pi >= 1) // exactly 1 is not allowed because log(1-1) = -Inf
        throw "Parameter pi of bernouilli distribution must be < 1!";
    _logPi = log(_pi);
    _log1MinPi = log(1.-_pi);
}

template <class T> bool TPriorBernouilli<T>::checkMinMax(std::string &min, std::string &max, bool & minIsIncluded, bool & maxIsIncluded, bool & hasDefaultMin, bool & hasDefaultMax) {
    // bernouilli distribution must be x == 0 or x == 1
    bool didNotChange = true;
    if (convertString<double>(min) < 0.) { // convert to double, because convertToBool does not work for doubles
        hasDefaultMin = false;
        min = "0";
        minIsIncluded = true;
        didNotChange = false;
    }
    if (convertString<double>(max) > 1.) {
        hasDefaultMax = false;
        max = "1";
        maxIsIncluded = true;
        didNotChange = false;
    }
    return didNotChange;
}

template <class T> void TPriorBernouilli<T>::typesAreCompatible(bool throwIfNot) {
    // parameters on which beta prior is defined must be unsigned integers (actually 0 or 1, but this is ensured by checkMinMax)
    this->_checkTypes(throwIfNot, false, true, true);
}

template <class T> double TPriorBernouilli<T>::_getLogPriorDensity_oneVal(T x){
    // note: expensive to calculate, use getLogPriorRatio() whenever possible!
    return TBernouilliDistr::density(x, _pi, true);
}

template <class T> double TPriorBernouilli<T>::_getLogPriorRatio_oneVal(T x, T x_old){
    return (x-x_old)*_logPi + (x_old-x)*_log1MinPi;
}

template <class T> double TPriorBernouilli<T>::pi() const {
    return _pi;
}

template <class T> void TPriorBernouilli<T>::simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) {
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        if (!paramToUse->hasFixedInitialValue()) { // only simulate if parameter has not a fixed value
            TRange range = paramToUse->getFull();
            for (size_t i = range.first; i < range.last; i += range.increment) {
                double value = RandomGenerator->getBinomialRand(_pi, 1);
                while (!paramToUse->valueIsInsideBoundary(value)) { // propose again if value is too large, to prevent overshooting while mirroring
                    value = RandomGenerator->getBinomialRand(_pi, 1);
                }
                // set value
                paramToUse->set(i, value);
            }
        }
    }
}

//----------------------------------------------
template <class T> TPriorDirichlet<T>::TPriorDirichlet(){
    this->_name = MCMCPrior::dirichlet;
}

template <class T> void TPriorDirichlet<T>::initialize(const std::string & params){
    // dirichlet(alpha1, alpha2, ..., alphaK)
    std::vector<double> alphas = this->_processPriorParameterString(params, 1, false);
    dirichletDistribution.resetParameters(alphas); // will do the check
}

template <class T> std::vector<double> TPriorDirichlet<T>::alpha() const{
    return dirichletDistribution.alpha();
}

template <class T> bool TPriorDirichlet<T>::checkMinMax(std::string &min, std::string &max, bool & minIsIncluded, bool & maxIsIncluded, bool & hasDefaultMin, bool & hasDefaultMax) {
    // dirichlet distribution must be x > 0 (because of log) and x <= 1
    bool didNotChange = true;
    if (convertString<double>(min) < 0.) {
        hasDefaultMin = false;
        min = "0";
        minIsIncluded = false;
        didNotChange = false;
    }
    if (convertString<double>(max) > 1.) {
        hasDefaultMax = false;
        max = "1";
        maxIsIncluded = true;
        didNotChange = false;
    }
    return didNotChange;
}

template <class T> void TPriorDirichlet<T>::initializeStorageOfPriorParameters() {
    // check if size of parameter matches size of alpha
    for (auto &param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        if (paramToUse->totalSize() != dirichletDistribution.alpha().size()) {
            throw std::runtime_error("In function 'template <class T> void TPriorDirichlet<T>::initializeStorageOfPriorParameters()': Parameter " + paramToUse->name() + " has a different size (" + toString(paramToUse->totalSize()) + ") than expected based on size of alpha ( " + toString(dirichletDistribution.alpha().size()) + ")!");
        }
    }
}

template <class T> void TPriorDirichlet<T>::typesAreCompatible(bool throwIfNot) {
    // parameters on which dirichlet prior is defined must be floating points
    this->_checkTypes(throwIfNot, true, false, false);
}

template <typename T> double TPriorDirichlet<T>::_getLogPriorDensity_oneVal(T x){
    throw std::runtime_error("Elements in Dirichlet array are non-iid -> do not call function _getLogPriorDensity_oneVal(T x) with single elements!");
}

template <typename T> double TPriorDirichlet<T>::_getLogPriorRatio_oneVal(T x, T x_old){
    throw std::runtime_error("Elements in Dirichlet array are non-iid -> do not call function _getLogPriorDensity_oneVal(T x) with single elements!");
}

template <typename T> double TPriorDirichlet<T>::_getLogPriorDensity_vec(const std::shared_ptr<TMCMCObservationsBase> & data, const size_t & index){
    double betaConstant = dirichletDistribution.getBetaConstant(true); // in log
    std::vector<double> alpha = dirichletDistribution.alpha();
    // calculate log(prod_k x_k^(alpha_k - 1)) = sum_k (alpha_k - 1) * log(x_k)
    double sum = 0.;
    for (size_t k = 0; k < data->totalSize(); k++) {
        sum += (alpha[k]-1.) * log(data->value<T>(k));
    }
    return -betaConstant + sum;
}

template <typename T> double TPriorDirichlet<T>::_getLogPriorDensityOld_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index){
    // THIS ASSUMES THAT ALL X HAVE CHANGED (TAKE OLDVALUE FOR EVERY X), NOT ONLY THE INDEX ONE!
    double betaConstant = dirichletDistribution.getBetaConstant(true); // in log
    std::vector<double> alpha = dirichletDistribution.alpha();
    // calculate log(prod_k x_k^(alpha_k - 1)) = sum_k (alpha_k - 1) * log(x_k)
    double sum = 0.;
    for (size_t k = 0; k < data->totalSize(); k++) {
            sum += (alpha[k]-1.) * log(data->oldValue<T>(k));
    }
    return -betaConstant + sum;
}

template <typename T> double  TPriorDirichlet<T>::_getLogPriorRatio_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index){
    // THIS ASSUMES THAT ALL X HAVE CHANGED, NOT ONLY THE INDEX ONE!
    std::vector<double> alpha = dirichletDistribution.alpha();
    double sum = 0.;
    for (size_t k = 0; k < data->totalSize(); k++) {
        sum += (alpha[k]-1.) * log(data->value<T>(k)/data->oldValue<T>(k));
    }
    return sum;
}

template <class T> double TPriorDirichlet<T>::getSumLogPriorDensity(const std::shared_ptr<TMCMCObservationsBase> & data) {
    // take first element of data only, because prior is defined on entire vector
    return _getLogPriorDensity_vec(data, 0);
}

template <class T> void TPriorDirichlet<T>::simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) {
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        if (!paramToUse->hasFixedInitialValue()) { // only simulate if parameter has not a fixed value
            assert(dirichletDistribution.alpha().size() == paramToUse->totalSize());

            // get random values from Dirichlet
            std::vector<double> values;
            RandomGenerator->fillDirichletRandom(dirichletDistribution.alpha(), values);

            // make sure the values are all < boundary.range()
            size_t index = 0;
            while (index < values.size()) {
                if (!paramToUse->valueIsInsideBoundary(values[index])) { // propose again if value is too large, to prevent overshooting while mirroring
                    RandomGenerator->fillDirichletRandom(dirichletDistribution.alpha(), values);
                    index = 0; // restart from the beginning, as all values have changed
                } else {
                    index++;
                }
            }

            // fill them into data
            TRange range = paramToUse->getFull();
            for (size_t i = range.first; i < range.last; i += range.increment) {
                paramToUse->set(i, values[i]);
            }
        }
    }
}