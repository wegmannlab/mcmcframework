//
// Created by madleina on 01.02.21.
//

#ifndef EMPTY_TDAGBUILDER_H
#define EMPTY_TDAGBUILDER_H

#include "TParameters.h"
#include "TMCMCParameter.h"
#include "TObservations.h"

//-------------------------------------------
// TMCMCUserInterface
//-------------------------------------------

struct LimitsHaveChanged {
    LimitsHaveChanged(){
        clear();
    };
    std::string newValue;
    bool valueHasChanged;
    bool newIncluded;
    bool includedHasChanged;

    void clear(){
        valueHasChanged = false;
        includedHasChanged = false;
        newIncluded = false;
        newValue = "0";
    }
};

class TMCMCUserInterface {
protected:
    TInputFile _configFile;
    TLog * _logfile;

    std::vector<std::string> _expectedColNames =  {"name", "observation", "prior", "parameters", "traceFile", "meanVarFile", "simulationFile", "update", "propKernel", "min", "minIncluded", "max", "maxIncluded", "sharedJumpSize"};

    // these are bools indicating that the user changed min(included) and max(included); so we need to check if the hard boundaries are not crossed
    LimitsHaveChanged _tempMin;
    LimitsHaveChanged _tempMax;

    // open and read files
    void _readParamConfigFile(std::vector<std::shared_ptr<TParameterDefinition>> & paramDefs, std::vector<std::shared_ptr<TObservationDefinition> > & observationDefs, TParameters & parameters);
    void _readInitValFile(std::vector<std::shared_ptr<TParameterDefinition>> & paramDefs, std::vector<std::shared_ptr<TObservationDefinition> > & observationDefs, TParameters & parameters);

    // parse one line
    void _checkHeaderConfigFile(const std::string & filename, const std::vector<std::string> & actualColNames);
    void _parseParamConfigurations(std::vector<std::shared_ptr<TParameterDefinition> > & paramDefs, std::vector<std::shared_ptr<TObservationDefinition> > & observationDefs, const std::vector<std::string> & line);
    void _parseInitVals(std::vector<std::shared_ptr<TParameterDefinition>> & paramDefs, std::vector<std::shared_ptr<TObservationDefinition> > & observationDefs, const std::vector<std::string> & line);

    // parse command line parameters
    void _parseCommandLineParamConfigs(std::vector<std::shared_ptr<TParameterDefinition>> & paramDefs, std::vector<std::shared_ptr<TObservationDefinition> > & observationDefs, TParameters & parameters);
    void _parseCommandLineParamInitVals(std::vector<std::shared_ptr<TParameterDefinition>> & paramDefs, std::vector<std::shared_ptr<TObservationDefinition> > & observationDefs, TParameters & parameters);

    // match single configuration
    bool _val1IsSmallerThanVal2(const std::string & newMin, const std::string & oldMin, const std::string & type);
    void _matchConfig(std::shared_ptr<TParameterDefinition> & def, const std::string & config, const std::string & val);
    void _matchConfig(std::shared_ptr<TObservationDefinition> & def, const std::string & config, const std::string & val);
    void _checkMin(const std::string& newMin, const std::string& oldMin, const std::string & type, const std::string& name);
    void _checkMax(const std::string& newMax, const std::string& oldMax, const std::string & type, const std::string& name);
    void _assertMaxIsValid(const std::shared_ptr<TDefinitionBase> & def) const;
    void _assertMinIsValid(const std::shared_ptr<TDefinitionBase> & def) const;
    void _assertMinAndMaxAreValid(const std::shared_ptr<TDefinitionBase> & def);

public:
    TMCMCUserInterface(TLog * logfile);
    virtual ~TMCMCUserInterface();
    void parseUserConfiguration(std::vector<std::shared_ptr<TParameterDefinition> > & paramDefs, std::vector<std::shared_ptr<TObservationDefinition> > & observationDefs, TParameters & parameters);
    void writeAllConfigurationsToFile(const std::string & prefix, std::vector<std::shared_ptr<TParameterDefinition>> & paramDefs, std::vector<std::shared_ptr<TObservationDefinition> > & observationDefs);
};

//-------------------------------------------
// TDAGBuilder
//-------------------------------------------

class TDAGBuilder {
protected:
    // declare friend classes
    // user that constructs a TDAGBuilder will only see relevant functions, but within mcmcframework, we can still access maps, dag etc.
    friend class TMCMC;
    friend class TSimulator;

    TLog * _logfile;
    TRandomGenerator * _randomGenerator;

    // parameter and observation definitions
    std::vector<std::shared_ptr<TParameterDefinition> > _paramDefs;
    std::vector<std::shared_ptr<TObservationDefinition> > _observationDefs;
    std::vector<std::shared_ptr<TDefinitionBase> > _allDefs;

    // initialized parameters and observations
    std::map<std::string, std::shared_ptr<TMCMCParameterBase> > _nameToParamMap;
    std::map<std::string, std::shared_ptr<TObservationsBase> > _nameToObservationsMap;
    TDAG _dag;

    // files
    std::vector<std::unique_ptr<TMCMCFile> > _traceFiles;
    std::vector<std::unique_ptr<TMCMCFile> > _meanVarFiles;
    std::vector<std::unique_ptr<TMCMCFile> > _simulationFiles;
    std::shared_ptr<TMCMCStateFile> _stateFile;
    bool _writeStateFile;

    // assemble parameter definitions
    template<template<typename> class TFunctor> void _buildDAG(bool throwIfTypesDontMatch, TParameters & Parameters);
    template<template<typename> class TFunctor> void _buildParameter(std::vector<std::shared_ptr<TDefinitionBase>> & definitionsWithSharedPrior, TParameters & Parameters);
    void _checkIfAllPriorParamameterDefinitionsExist();
    bool _allPriorParametersAreBuilt(const std::string & PriorParameters);
    bool _definitionsAreBuilt(const std::vector<std::shared_ptr<TDefinitionBase>> & definitionsWithSharedPrior);
    bool _definitionsSharePrior(std::shared_ptr<TDefinitionBase> & def, std::shared_ptr<TDefinitionBase> & otherDef);
    void _addToDefinitionsVector(std::shared_ptr<TDefinitionBase> & defToAdd, std::shared_ptr<TDefinitionBase> & existingDef, std::vector< std::vector<std::shared_ptr<TDefinitionBase>> > & definitionsWithSharedPriorsVector);
    bool _definitionExistsInVector(const std::string & name, std::vector< std::vector<std::shared_ptr<TDefinitionBase>> > & definitionsWithSharedPriorsVector);
    bool _matchSharedPriorWithHyperPriors(std::shared_ptr<TDefinitionBase> & def, std::shared_ptr<TDefinitionBase> & otherDef, std::vector< std::vector<std::shared_ptr<TDefinitionBase>> > & definitionsWithSharedPriorVector);
    void _findHyperParameters(std::shared_ptr<TDefinitionBase> & def, std::vector<std::shared_ptr<TMCMCParameterBase> > & initializedParams, std::vector<std::shared_ptr<TObservationsBase> > & initializedObservations);
    void _bundleParametersWithSharedPriors(std::vector<std::vector<std::shared_ptr<TDefinitionBase>>> & definitionsWithSharedPriorsVector);
    void _checkForUniqueNames(const std::string & name) const;
    void _checkForValidDAG() const;

    // build parameters
    template<template<typename> class TFunctor> void _buildTemplatedParameter(std::vector<std::shared_ptr<TDefinitionBase>> & defs, std::vector<std::shared_ptr<TMCMCParameterBase>> & initializedParams, std::vector<std::shared_ptr<TObservationsBase>> & initializedObservations, TParameters & Parameters);
    std::shared_ptr<TObservationDefinition> _getPointerToObservationDefinition(const std::string & Name);
    std::shared_ptr<TParameterDefinition> _getPointerToParameterDefinition(const std::string & Name);
    template<typename T> std::shared_ptr<TPrior<T>> _initPrior(const std::string & priorName, std::string priorParams, const std::string& paramName);
    template<template<typename> class TFunctor, class T> std::shared_ptr<TPrior<T>> _initPriorWithHyperPrior(const std::string & priorName, std::vector<std::shared_ptr<TMCMCParameterBase> > & initializedParams,std::vector<std::shared_ptr<TObservationsBase>> & initializedObservations, const std::string& paramName);
    template<typename T> void _checkMinMaxPrior(const std::shared_ptr<TPrior<T>> & prior, std::shared_ptr<TDefinitionBase> & def);
    template<template<typename> class TFunctor, class T> void _buildParams(std::vector<std::shared_ptr<TDefinitionBase>> & defs, std::vector<std::shared_ptr<TMCMCParameterBase>> & initializedParams, std::vector<std::shared_ptr<TObservationsBase>> & initializedObservations, TParameters & Parameters);
    template<template<typename> class TFunctor, class T> std::shared_ptr<TPrior<T>> _buildPrior(const std::shared_ptr<TDefinitionBase> & def, std::vector<std::shared_ptr<TMCMCParameterBase>> & initializedParams, std::vector<std::shared_ptr<TObservationsBase>> & initializedObservations);
    template<typename TranslatedType, typename StoredType> void _buildTranslatedObservation_templated(const std::shared_ptr<TObservationDefinition> & observationsDef, const std::shared_ptr<TPrior<TranslatedType>> & prior);
    template<typename T> void _buildTranslatedObservation(const std::shared_ptr<TObservationDefinition> & observationsDef, const std::shared_ptr<TPrior<T>> & prior);
    template<typename T> void _buildObservation(const std::string & Name, const std::shared_ptr<TPrior<T>> & prior);
    template<typename T> void _buildParameter(const std::string & Name, const std::shared_ptr<TPrior<T>> & prior);
    void _createOneDAG(bool throwIfTypesDontMatch);

    // files
    void _prepareFiles(MCMCFile::Filetypes mcmcFiletype, std::vector<std::unique_ptr<TMCMCFile> > & fileVec, const std::string & prefix);
    void _prepareStateFiles(const std::string & prefix);
    void _bundleParameterFiles(MCMCFile::Filetypes mcmcFiletype, const std::string & paramName, const std::string& filename, std::vector<std::unique_ptr<TMCMCFile> > & fileVec, const std::string & prefix);

    // functions for fried classes
    void _prepareAllMCMCFiles(const std::string & Prefix, bool WriteStateFile);
    void _prepareAllSimulationFiles(const std::string & Prefix);
    void _closeAllMCMCFiles();
    void _closeAllSimulationFiles();
    void _estimateInitialPriorParameters();
    void _updateParameters_MCMC();
    void _simulate(TParameters & Parameters);

public:
    TDAGBuilder(TRandomGenerator * RandomGenerator, TLog * Logfile);
    virtual ~TDAGBuilder();

    // 1) add parameter and observation definitions to DAG
    void addObservation(const TObservationDefinition& def);
    void addParameter(const TParameterDefinition & Param);

    // 2) build scaffold of DAG
    template<template<typename> class TFunctor> void buildScaffoldDAG(TParameters & parameters);

    // 3) get pointer to observation -> fill observations
    const std::shared_ptr<TObservationsBase> & getPointerToObservation(const std::string& name);

    // 4) initialize DAG (dimensions are known now)
    void initializeStorage();

    // additional get functions
    const std::shared_ptr<TMCMCParameterBase> & getPointerToParameter(const std::string& name);
    const std::shared_ptr<TDefinitionBase> & getPointerToDefinition(const std::string& name);
    const std::shared_ptr<TParameterDefinition> & getPointerToParameterDefinition(const std::string& name);
    const std::map<std::string, std::shared_ptr<TMCMCParameterBase> > & getAllParameters();
};

#include "TDAGBuilder.tpp"

#endif //EMPTY_TDAGBUILDER_H
