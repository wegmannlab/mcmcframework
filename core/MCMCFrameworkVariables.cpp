//
// Created by madleina on 03.02.21.
//

#include "MCMCFrameworkVariables.h"

//--------------------------------------------
// PROPOSAL KERNELS
//--------------------------------------------

namespace ProposalKernel {
    MCMCProposalKernel stringToProposalKernel(const std::string &String) {
        // loop over map
        for (auto &prop : proposalKernelToString) {
            if (prop.second == String) {
                return prop.first;
            }
        }
        throw "Proposal kernel '" + String + "' does not exist!";
    }
}

//--------------------------------------------
// MCMC FILES
//--------------------------------------------

namespace MCMCFile {
    Filetypes stringToFileType(const std::string &String) {
        // loop over map
        for (auto &file : fileTypeToString) {
            if (file.second == String) {
                return file.first;
            }
        }
        throw "MCMC file type '" + String + "' does not exist!";
    }
}

//--------------------------------------------
// TRANSLATORS (for observations)
//--------------------------------------------

namespace Translator {
    Translator stringToTranslator(const std::string &String) {
        // loop over map
        for (auto &trans : translatorToString) {
            if (trans.second == String) {
                return trans.first;
            }
        }
        throw "Translator '" + String + "' does not exist!";
    }
}