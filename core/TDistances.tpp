
//-------------------------------------------
// TDistancesBinned
//-------------------------------------------
template <class NumDistanceGroupsType>
TDistancesBinned<NumDistanceGroupsType>::TDistancesBinned() : TDistancesBinnedBase() {
    _numDistanceGroups = 0;
}

template <class NumDistanceGroupsType>
TDistancesBinned<NumDistanceGroupsType>::TDistancesBinned(size_t maxDist) : TDistancesBinned() {
    initialize(maxDist);
}

template <class NumDistanceGroupsType>
void TDistancesBinned<NumDistanceGroupsType>::initialize(size_t maxDist){
    // initialize _distanceGroups
    if (maxDist < 1)
        throw std::runtime_error("In function 'void TDistances::initialize(size_t maxDist):' maxDist (" + toString(maxDist) + ") must be > 0!");

    _numDistanceGroups = static_cast<NumDistanceGroupsType>(ceil(log2(maxDist)) + 1); //one extra for beginning of chromosome (and last in genome)
    _distanceGroups.assign(_numDistanceGroups, TDistanceGroup());

    //group 0: first site of chromosome
    _distanceGroups[0].min = 0;
    _distanceGroups[0].maxPlusOne = 0;
    _distanceGroups[0].distance = 0;
    _distanceGroups[0].hasSites = false;

    //all other groups
    if (_numDistanceGroups > 1) {
        _distanceGroups[1].min = 1;
        _distanceGroups[1].hasSites = false;
        int g;
        for (g = 2; g < _numDistanceGroups; g++) {
            _distanceGroups[g].min = _distanceGroups[g - 1].min * 2;
            _distanceGroups[g - 1].maxPlusOne = _distanceGroups[g].min;
            _distanceGroups[g - 1].distance = _distanceGroups[g - 1].maxPlusOne - _distanceGroups[g - 1].min;
            _distanceGroups[g].hasSites = false;
        }
        _distanceGroups[_numDistanceGroups - 1].maxPlusOne = _distanceGroups[_numDistanceGroups - 1].min * 2;
        _distanceGroups[_numDistanceGroups - 1].distance =
                _distanceGroups[_numDistanceGroups - 1].maxPlusOne - _distanceGroups[_numDistanceGroups - 1].min;
    }
}

template <class NumDistanceGroupsType>
void TDistancesBinned<NumDistanceGroupsType>::_store(const NumDistanceGroupsType& g){
    _groupMembership.push_back(g);
    _distanceGroups[g].hasSites = true;
}

template <class NumDistanceGroupsType>
void TDistancesBinned<NumDistanceGroupsType>::_addPositionOnNewJunk(const size_t &Position) {
    // store raw position (base class)
    TDistancesBinnedBase::_addPositionOnNewJunk(Position);

    // add distance class 0 (first on junk)
    _store(0);
}

template <class NumDistanceGroupsType>
void TDistancesBinned<NumDistanceGroupsType>::_addPositionOnExistingJunk(const size_t &Position) {
    size_t lastPosition = _positions.back();

    // store raw position (base class)
    TDistancesBinnedBase::_addPositionOnExistingJunk(Position);

    // calculate distance
    NumDistanceGroupsType g = 0;
    if (_numDistanceGroups > 1){ // if maxDist = 1 -> results in _numDistanceGroups=1 -> all loci have same distance group (= initial one)
        size_t dist = Position - lastPosition;
        for (g = 1; g < _numDistanceGroups; ++g) {
            if (g == _numDistanceGroups - 1 || dist < _distanceGroups[g].maxPlusOne)
                break;
        }
    }
    _store(g);
}

template <class NumDistanceGroupsType>
size_t TDistancesBinned<NumDistanceGroupsType>::numDistanceGroups() const {
    return _numDistanceGroups;
}

template <class NumDistanceGroupsType>
TDistanceGroup TDistancesBinned<NumDistanceGroupsType>::distanceGroup(const size_t & Group) const {
    if (Group >= _numDistanceGroups)
        throw std::runtime_error("In function 'TDistanceGroup TDistances::distanceGroup(const size_t & Group)': group (" + toString(Group) + ") is >= than _numDistanceGroups (" + toString(_numDistanceGroups) + ")!");
    return _distanceGroups[Group];
}

template <class NumDistanceGroupsType>
size_t TDistancesBinned<NumDistanceGroupsType>::size() const{
    return _groupMembership.size();
}

template <class NumDistanceGroupsType>
bool TDistancesBinned<NumDistanceGroupsType>::groupHasSites(const size_t & Group) {
    if (Group >= _numDistanceGroups)
        throw std::runtime_error("In function 'bool TDistances::groupHasSites(const size_t & Group)': group (" + toString(Group) + ") is >= than _numDistanceGroups (" + toString(_numDistanceGroups) + ")!");
    return _distanceGroups[Group].hasSites;
}

template <class NumDistanceGroupsType>
size_t TDistancesBinned<NumDistanceGroupsType>::operator[](const size_t &l) const {
    if (l >= _groupMembership.size())
        throw std::runtime_error("In function 'size_t TDistances::operator[](const size_t &l) const': l >= _groupMembership.size()!");
    return _groupMembership[l];
}

template <class NumDistanceGroupsType>
uint32_t TDistancesBinned<NumDistanceGroupsType>::_getRandomDistance(const double & lambda, TRandomGenerator * RandomGenerator){
    uint32_t distance = RandomGenerator->getPoissonRandom(lambda);
    while (distance == 0){
        // draw again
        distance = RandomGenerator->getPoissonRandom(lambda);
    }

    return distance;
}

template <class NumDistanceGroupsType>
void TDistancesBinned<NumDistanceGroupsType>::simulate(const std::vector<size_t> & JunkSizes, TRandomGenerator * RandomGenerator, double Lambda) {
    // JunkSizes: vector of length of junks -> e.g. {10, 50, 5} will simulate 3 junks of size 10, 50 and 5, respectively.
    // simulate distances with random values from a Poisson distribution

    size_t position;
    std::string prefix = "junk_";
    size_t counter = 1;
    for(size_t last : JunkSizes){
        position = 0;
        for(size_t i = 0; i < last; ++i) {
            // add other
            if (Lambda == 0.){ // adjacent loci
                position ++;
            } else { // simulate distance
                position += _getRandomDistance(Lambda, RandomGenerator);
            }
            add(position, prefix + toString(counter));
        }
        counter++;
    }

    finalizeFilling();
}
