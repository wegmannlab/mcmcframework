/*
 * TEMPriorIID.h
 *
 *  Created on: Jan 18, 2021
 *      Author: phaentu
 */

#ifndef MCMCFRAMEWORK_CORE_TEMPRIORIID_H_
#define MCMCFRAMEWORK_CORE_TEMPRIORIID_H_

#include "TEMPrior.h"
#include "TLatentVariable.h"

//--------------------------------------------
// TEMPriorIID_base
// A pure virtual class
// IID stands for independent and identically distributed: no Markov property (not an HMM)
//--------------------------------------------
template <typename PrecisionType, typename NumStatesType, typename LengthType> class TEMPriorIID_base : public TEMPrior_base<PrecisionType,NumStatesType,LengthType>{
public:
    TEMPriorIID_base() : TEMPrior_base<PrecisionType,NumStatesType,LengthType>(){};
    TEMPriorIID_base(const NumStatesType& NumStates) : TEMPrior_base<PrecisionType,NumStatesType,LengthType>(NumStates) {};

    virtual ~TEMPriorIID_base() = default;

	//getters
    virtual const PrecisionType operator[](const NumStatesType & State) const = 0;

    //EM initialization
    void initializeEMParameters(const TLatentVariable<PrecisionType, NumStatesType, LengthType> & LatentVariable, const std::vector<LengthType> & JunkEnds) override{
        // prepare storage
        this->prepareEMParameterEstimationOneIteration();

        LengthType first = 0;
        // go over all junks
        for(LengthType last : JunkEnds){
            // go over all elements in one junk
            for(LengthType i = first; i < last; ++i) {
                NumStatesType state = LatentVariable.getHiddenState(i);
                handleEMParameterInitialization(i, state);
            }
            //update first of next
            first = last;
        }
        this->finalizeEMParameterInitialization();
    };

    // EM initialization: only need current state
    virtual void handleEMParameterInitialization(const LengthType & Index, const NumStatesType & State){};

    //EM update: maximization function gets a TDataVector as argument (= EM weights)
	virtual void handleEMParameterEstimationOneIteration(const LengthType & Index, const TDataVector<PrecisionType, NumStatesType> & weights){};

	virtual void print(){
		std::cout << "Probabilities:" << std::endl;
		for(NumStatesType s = 0; s < this->_numStates; ++s){
		    if(s>0){ std::cout << ", "; }
		    std::cout << operator[](s);
		}
	};
};




#endif /* MCMCFRAMEWORK_CORE_TEMPRIORIID_H_ */
