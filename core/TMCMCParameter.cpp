//
// Created by madleina on 01.07.20.
//

#include "TMCMCParameter.h"

//-------------------------------------------
// TMCMCParameters
//-------------------------------------------

// template specializations

template<> void TMCMCParameter<bool>::_initJumpSizeVal_unique(const std::string &initJumpSize) {
    // calls _initJumpSizeVal_shared, as unique jump sizes for bools are simply a waste of memory
    _initJumpSizeVal_shared(initJumpSize);
}

template<> void TMCMCParameter<bool>::_initJumpSizeVal_shared(const std::string &initJumpSize) {
    _sharedJumpSize = true;
    _jumpSizeProposalVec.reserve(1); // size 1, as all have the same proposal width
    _setPropKernel(true);
    _acceptedUpdates.push_back(0);
}

template <> void TMCMCParameter<bool>::_readVals(const std::string& initVal, std::vector<bool> & storage){
    // initVal can either be..
    //          1) a vector of size _storage.totalSize().
    //          2) a single value
    //          3) a filename
    if (stringContains(initVal, ",")) // case 1): probably a comma-separated vector
        _readValsFromVec(initVal, storage);
    else {
        if (stringIsProbablyABool(initVal)) // case 2): probably only one bool -> initialize all elements in array to this value
            _readValsOnlyOneNumber(initVal, storage);
        else // case 3): probably a file -> try opening it!
            _readValsFromFile(initVal, storage);
    }
}

template <> void TMCMCParameter<bool>::_adjustJumpSize(size_t i){
    // don't adjust jump size if bool (can only go from 0->1 or 1->0)
}
