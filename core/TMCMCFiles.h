//
// Created by madleina on 11.01.21.
//

#ifndef EMPTY_TMCMCFILES_H
#define EMPTY_TMCMCFILES_H

#include "TFile.h"
#include <memory>
#include <vector>
#include "TMCMCParameterBase.h"

//-------------------------------------------
// TMCMCFile
//-------------------------------------------
class TMCMCFile {
protected:
    std::vector<std::shared_ptr<TMCMCParameterBase>> _paramsInFile;
    std::vector<std::shared_ptr<TObservationsBase>> _observationsInFile;

    // write
    TOutputFile _file;
    std::string _name;

    // constructors
    TMCMCFile(const std::string & Filename);
    void _initialize(const std::string & Filename);

public:
    virtual ~TMCMCFile();
    void addParam(const std::shared_ptr<TMCMCParameterBase>& paramInFile);
    void addObservation(const std::shared_ptr<TObservationsBase>& paramInFile);

    // write
    std::string name(){return _name;};
    virtual void writeHeader();
    virtual void write();

    // close
    void close();
};

class TMCMCTraceFile : public TMCMCFile {
public:
    TMCMCTraceFile(const std::string & Filename);
    ~TMCMCTraceFile() override;
    void write() override;
};

class TMCMCMeanVarFile : public TMCMCFile {
public:
    TMCMCMeanVarFile(const std::string & Filename);
    ~TMCMCMeanVarFile() override;
    void write() override;
};

class TMCMCSimulationFile : public TMCMCFile {
public:
    TMCMCSimulationFile(const std::string & Filename);
    ~TMCMCSimulationFile() override;
    void write() override;
};

class TMCMCStateFile : public TMCMCFile {
protected:
    // read
    void _writeHeader(int iteration);
public:
    TMCMCStateFile(const std::string & Filename);
    ~TMCMCStateFile() override;
    void write(int iteration);
};

//-------------------------------------------
// TMCMCFile_Reader
//-------------------------------------------

class TMCMCFile_Reader {
protected:
    // write
    TInputFile _file;
    std::string _name;
    std::vector<std::string> _line;

    // constructors
    TMCMCFile_Reader(const std::string & Filename);
    void _initialize(const std::string & Filename);

    // read
    std::pair<size_t, size_t> _findFirstAndLastCol(const std::string & paramName, const size_t & expectedSize) const;
    virtual void _readRelevantLine(std::vector<std::string> & vec) = 0 ;

public:
    virtual ~TMCMCFile_Reader();
    std::string name(){return _name;};
    std::vector<std::string> header(){return _file.header();};

    // read
    template<typename T> void readParameterValuesIntoVec(const std::string & paramName, const size_t & expectedSize, std::vector<T> & storage){
        // read relevant line (-> last one for trace and simulated files, first one for meanVar files)
        storage.clear();
        if (_line.empty()) { // only read line first time this function is called (otherwise we don't read anything)
            _readRelevantLine(_line);
        }
        // get columns we need to store
        std::pair<size_t, size_t> startAndEnd = _findFirstAndLastCol(paramName, expectedSize);

        // fill storage vector
        for (size_t col = 0; col < _file.numCols(); col++){
            if (col >= startAndEnd.first && col < startAndEnd.second){
                // convert to number
                T val;
                try {
                    val = convertString<T>(_line[col]);
                } catch (...) {
                    throw "Invalid initial value '" + _line[col] + "' in column " + toString(col) + " of file '" + _file.name() + "' for parameter " + paramName + "!";
                }
                storage.push_back(val);
            }
        }
    }

    // close
    void close();
};


class TMCMCTraceFile_Reader : public TMCMCFile_Reader {
protected:
    // read
    void _readRelevantLine(std::vector<std::string> & vec) override;
public:
    TMCMCTraceFile_Reader(const std::string & Filename);
    ~TMCMCTraceFile_Reader() override;
};

class TMCMCMeanVarFile_Reader : public TMCMCFile_Reader {
protected:
    // read
    void _readRelevantLine(std::vector<std::string> & vec) override;
public:
    TMCMCMeanVarFile_Reader(const std::string & Filename);
    ~TMCMCMeanVarFile_Reader() override;
};

class TMCMCSimulationFile_Reader : public TMCMCFile_Reader {
protected:
    // read
    void _readRelevantLine(std::vector<std::string> & vec) override;
public:
    TMCMCSimulationFile_Reader(const std::string & Filename);
    ~TMCMCSimulationFile_Reader() override;
};

class TMCMCStateFile_Reader : public TMCMCFile_Reader {
protected:
    // read
    void _readRelevantLine(std::vector<std::string> & vec) override;
public:
    TMCMCStateFile_Reader(const std::string & Filename);
    ~TMCMCStateFile_Reader() override;
};


#endif //EMPTY_TMCMCFILES_H
