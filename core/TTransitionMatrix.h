/*
 * TTransitionMatrix.h
 *
 *  Created on: Jan 18, 2021
 *      Author: phaentu
 */

#include "TEMPrior.h"
#include "TLatentVariable.h"
#include "THMMVars.h"
#include <cmath>
#include <vector>

#ifndef TTRANSITIONMATRIX_H_
#define TTRANSITIONMATRIX_H_

//forward declare THMMPosteriorXi
template <typename PrecisionType, typename NumStatesType, typename LengthType> class THMMPosteriorXi;

//--------------------------------------------
// TTransitionMatrix_base
// A pure virtual class
//--------------------------------------------
template <typename PrecisionType, typename NumStatesType, typename LengthType> class TTransitionMatrix_base : public TEMPrior_base<PrecisionType,NumStatesType,LengthType>{
protected:
	TDataVector<PrecisionType, NumStatesType> _initialProbabilities;
	TDataVector<PrecisionType, NumStatesType> _initialProbabilities_tmp; //used during EM estimation
	LengthType _numJunksUsedForUpdate;

	void _init(const NumStatesType& NumStates){
		_initialProbabilities.resize(this->_numStates);
		_initialProbabilities.set(1.0 / (PrecisionType) this->_numStates);
	};

public:
	TTransitionMatrix_base() : TEMPrior_base<PrecisionType,NumStatesType,LengthType>(){};
    TTransitionMatrix_base(const NumStatesType& NumStates): TEMPrior_base<PrecisionType,NumStatesType,LengthType>(NumStates){
    	_init(NumStates);
    };
	virtual ~TTransitionMatrix_base() = default;

	//getters
    virtual const PrecisionType& operator()(const LengthType & Index, const NumStatesType & From, const NumStatesType & To) const = 0;
    virtual const PrecisionType& stationary(const NumStatesType & State) const = 0;
    virtual const PrecisionType& initialProbability(const NumStatesType & State) const { return _initialProbabilities[State]; };
    virtual const TDataVector<PrecisionType, NumStatesType>& initialProbabilities() const { return _initialProbabilities; };

    //EM initialization (functions can stay empty if parameters should not be initialized prior to EM)
    void initializeEMParameters(const TLatentVariable<PrecisionType, NumStatesType, LengthType> & LatentVariable, const std::vector<LengthType> & JunkEnds) override{
        // prepare storage
        prepareEMParameterEstimationOneIteration();

        LengthType first = 0;
        NumStatesType previousState;

        // go over all junks
        for(LengthType last : JunkEnds){
            // first element in junk: initial distribution
            previousState = LatentVariable.getHiddenState(0);
            handleEMParameterInitializationInitialDistribution(previousState);

            // go over all other elements in one junk: transition probabilities
            for(LengthType i = first + 1; i < last; ++i) {
                NumStatesType state = LatentVariable.getHiddenState(i);
                handleEMParameterInitializationTransitionProbabilities(i, previousState, state);
                previousState = state;
            }
            //update first of next
            first = last;
        }
        finalizeEMParameterInitialization();
    };

    virtual void handleEMParameterInitializationInitialDistribution(const NumStatesType & State){
        _initialProbabilities_tmp[State]++;
        _numJunksUsedForUpdate++;
    };

    virtual void handleEMParameterInitializationTransitionProbabilities(const LengthType & Index, const NumStatesType & PreviousState, const NumStatesType & CurrentState){};

    void finalizeEMParameterInitialization() override{
        finalizeEMParameterInitializationTransitionProbabilities();
        finalizeEMParameterInitializationInitialDistribution();
    };

    virtual void finalizeEMParameterInitializationTransitionProbabilities(){};
    virtual void finalizeEMParameterInitializationInitialDistribution(){
        for(NumStatesType s = 0; s < this->_numStates; ++s){
            _initialProbabilities[s] = (_initialProbabilities_tmp[s] + 1 ) / (PrecisionType) (_numJunksUsedForUpdate + this->numStates());
        }
    };

    //prepare EM

    virtual void prepareEMParameterEstimationOneIteration(){
    	prepareEMParameterEstimationOneIterationInitialDistribution();
    	prepareEMParameterEstimationOneIterationTransitionProbabilities();
    };

    virtual void prepareEMParameterEstimationOneIterationInitialDistribution(){
		_initialProbabilities_tmp.resize(this->_numStates);
		_initialProbabilities_tmp.set(0.0);
		_numJunksUsedForUpdate = 0;
	};

    virtual void prepareEMParameterEstimationOneIterationTransitionProbabilities(){};

    //handle EM
    //for initial distribution: maximization function gets a TDataVector rather than THMMPosteriorGamma as argument to avoid circular header inclusion.
    virtual void handleEMParameterEstimationOneIterationInitialDistribution(const LengthType & Index, const TDataVector<PrecisionType, NumStatesType> & Gamma){
    	for(NumStatesType s = 0; s < this->_numStates; ++s){
    		_initialProbabilities_tmp[s] += Gamma[s];
    	}
    	++_numJunksUsedForUpdate;
    };

    //EM update: maximization function gets a THMMPosteriorXi as argument
    virtual void handleEMParameterEstimationOneIterationTransitionProbabilities(const LengthType & Index, const THMMPosteriorXi<PrecisionType, NumStatesType, LengthType> & xi){};

    //finalize EM

    virtual void finalizeEMParameterEstimationOneIteration(){
    	finalizeEMParameterEstimationOneIterationInitialDistribution();
    	finalizeEMParameterEstimationOneIterationTransitionProbabilities();
    };

    virtual void finalizeEMParameterEstimationOneIterationInitialDistribution(){
    	for(NumStatesType s = 0; s < this->_numStates; ++s){
    		_initialProbabilities[s] = _initialProbabilities_tmp[s] / (PrecisionType) _numJunksUsedForUpdate;
    	}
    };

    virtual void finalizeEMParameterEstimationOneIterationTransitionProbabilities(){};

	//Simulate
	virtual NumStatesType sampleFromStationary(TRandomGenerator & RandomGenerator) const{
	    double random = RandomGenerator.getRand();
	    NumStatesType s = 0;
	    double cumul = stationary(s);

	    while(cumul <= random){
	        ++s;
	        cumul += stationary(s);
	    }

        return s;
	};

	virtual NumStatesType sampleNextState(const LengthType & Index, const NumStatesType & State, TRandomGenerator & RandomGenerator) const{
        double random = RandomGenerator.getRand();
        NumStatesType s = 0;
        double cumul = operator()(Index, State, s);

        while(cumul <= random){
            ++s;
            cumul += operator()(Index, State, s);
        }

        return s;
	};

	//output
	void print(const LengthType & Index) {
		std::cout << "Transition matrix:" << std::endl;
		for(NumStatesType s = 0; s < this->_numStates; ++s){
			for(NumStatesType t = 0; t < this->_numStates; ++t){
				if(t>0){ std::cout << ", "; }
				std::cout << operator()(Index, s, t);
			}
			std::cout << std::endl;
		}
	};
};

#endif /* TTRANSITIONMATRIX_H_ */
