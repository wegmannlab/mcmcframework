/*
 * TLatentVariable.h
 *
 *  Created on: Dec 21, 2020
 *      Author: phaentu
 */

#ifndef MCMCFRAMEWORK_SRC_TLATENTVARIABLE_H_
#define MCMCFRAMEWORK_SRC_TLATENTVARIABLE_H_

#include "TDataVector.h"
#include "TFile.h"

//-------------------------------------
// TLatentVariable
// A pure virtual class
//-------------------------------------
template <typename PrecisionType, typename NumStatesType, typename LengthType> class TLatentVariable{
public:
	TLatentVariable(){};
	virtual ~TLatentVariable() = default;

	//function to calculate emission probabilities
	virtual void calculateEmissionProbabilities(const LengthType & Index, TDataVector<PrecisionType, NumStatesType> & Emission) = 0;

    //EM initialization (functions can stay empty if parameters should not be initialized prior to EM)
    virtual NumStatesType getHiddenState(const LengthType & Index) const{
        return 0;
    };
    virtual void initializeEMParameters(){};
    virtual void finalizeEMParameterInitialization(){};

	// EM estimation.
	// These functions are not required as THMM may be used to calculate posterior distributions only.
	// Note: to estimate the transition matrix only, these functions can stay "empty"
    virtual void prepareEMParameterEstimationInitial(){};
    virtual void prepareEMParameterEstimationOneIteration(){};
	virtual void handleEMParameterEstimationOneIteration(const LengthType & index, const TDataVector<PrecisionType, NumStatesType> & Weights){};
	virtual void finalizeEMParameterEstimationOneIteration(){};
    virtual void finalizeEMParameterEstimationFinal(){};
    virtual void reportEMParameters(){};

    // Calculate LL.
    virtual void prepareLLCalculation(const NumStatesType & NumStates){};
    virtual void finalizeLLCalculation(){};

    // Handling posterior estimates.
    virtual void prepareStatePosteriorEstimation(const NumStatesType & NumStates){};
    virtual void handleStatePosteriorEstimation(const LengthType & Index, const TDataVector<PrecisionType, NumStatesType> & StatePosteriorProbabilities){};
    virtual void finalizeStatePosteriorEstimation(){};

    // Write posterior estimates.
    virtual void prepareStatePosteriorEstimationAndWriting(const NumStatesType & NumStates, std::vector<std::string> & header){
        // fill header
        header.clear();
        header = {"Index"};
        for(NumStatesType s = 0; s < NumStates; ++s){
            header.push_back("P(S=" + toString(s) + ")");
        }
    };
	virtual void handleStatePosteriorEstimationAndWriting(const LengthType & Index, const TDataVector<PrecisionType, NumStatesType> & StatePosteriorProbabilities, TOutputFile & out){
		out << Index;
		for(NumStatesType s = 0; s < StatePosteriorProbabilities.size(); ++s){
			out << StatePosteriorProbabilities[s];
		}
		out << std::endl;
	};
    virtual void finalizeStatePosteriorEstimationAndWriting(){};
};



#endif /* MCMCFRAMEWORK_SRC_TLATENTVARIABLE_H_ */
