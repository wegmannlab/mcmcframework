//
// Created by madleina on 14.05.21.
//

//-------------------------------------------
// TUpdateWeights
//-------------------------------------------
template <class TYPE> TUpdateWeights<TYPE>::TUpdateWeights() {
    _randomGenerator = nullptr;
    _size = 0;
}

template <class TYPE> TUpdateWeights<TYPE>::TUpdateWeights(TRandomGenerator* RandomGenerator) {
    _randomGenerator = RandomGenerator;
    _size = 0;
}

template <class TYPE> void TUpdateWeights<TYPE>::addRandomGenerator(TRandomGenerator *RandomGenerator) {
    _randomGenerator = RandomGenerator;
}

template <class TYPE> void TUpdateWeights<TYPE>::initialize(const std::vector<TYPE> &Weights) {
    _size = Weights.size();

    double sum = _calculateAbsoluteSum(Weights);
    _normalize_AndFillCumulativeSum(sum,Weights, _cumWeights);
}

template <class TYPE> double TUpdateWeights<TYPE>::_calculateAbsoluteSum(const std::vector<TYPE> & Vec){
    // sum
    double sum = 0.;
    for (auto & v : Vec){
        sum += std::fabs(v);
    }
    return sum;
}

template <class TYPE> void TUpdateWeights<TYPE>::_normalize_AndFillCumulativeSum(const double & Sum, const std::vector<TYPE> & Vec, std::vector<TYPE> & CumSum){
    // sum
    CumSum.resize(Vec.size(), 0.);
    CumSum[0] = Vec[0] / Sum;
    for (size_t i = 1; i < Vec.size(); i++){
        CumSum[i] = CumSum[i-1] + Vec[i]/Sum;
    }
}

template <class TYPE> size_t TUpdateWeights<TYPE>::pick(){
    double randomNumber = _randomGenerator->getRand(0., 1.); // random number between 0 and 1

    // initialize
    size_t left = 0;
    size_t right = _size;

    // find locus containing the random number in its cumulative weight (approximate binary search)
    while (left < right){
        // divide
        size_t middle = static_cast<size_t>(std::floor(static_cast<double>(left + right) / 2.)); // round down
        // conquer
        if (_cumWeights[middle] < randomNumber) {
            left = middle + 1;
        } else {
            right = middle;
        }
    }
    return left;
}