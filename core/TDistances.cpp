//
// Created by madleina on 07.01.21.
//

#include "TDistances.h"

//-------------------------------------------
// TPositionsRaw
//-------------------------------------------

TPositionsRaw::TPositionsRaw(){
    _indexCurrentlyVisitedJunk = 0;
    _indexPreviouslyVisitedJunk = 0;
    _indexCurrentlyVisitedPosition = 0;
    _indexPreviouslyVisitedPosition = 0;
    _found = false;
    _maxSearchDistance = 100;
    _curJunkName = "";
}

void TPositionsRaw::_addToJunkEnds(){
    // take size of container (and not -1) because end is not included (loop until < end)
    _junkEnds.push_back(_positions.size());
}

void TPositionsRaw::_addJunk(const std::string &JunkName){
    _junkNames.push_back(JunkName);

    // add current index inside position to junk ends (only if it is not the first position)
    if (!_positions.empty()){
        _addToJunkEnds();
    }

    // update _curJunkName
    _curJunkName = JunkName;
}

void TPositionsRaw::_addPositionOnNewJunk(const size_t &Position){
    // base class: doesn't matter if position is on new junk or not
    // (but it does matter for derived classes)
    _positions.push_back(Position);
}

void TPositionsRaw::_addPositionOnExistingJunk(const size_t &Position){
    _positions.push_back(Position);
}

void TPositionsRaw::add(const size_t &Position, const std::string &JunkName) {
    if (_curJunkName != JunkName){
        _addJunk(JunkName);
        _addPositionOnNewJunk(Position);
    } else {
        _addPositionOnExistingJunk(Position);
    }
}

void TPositionsRaw::finalizeFilling(){
    // add index of last position to junk ends
    _addToJunkEnds();
}

size_t TPositionsRaw::size() const {
    return _positions.size();
}

const std::string & TPositionsRaw::getJunkName(const size_t & Index) const {
    assert(Index < _positions.size());
    // which interval of junkEnds contains this index?
    for (size_t j = 0; j < _junkEnds.size(); j++){
        if (Index < _junkEnds[j]){
            return _junkNames[j];
        }
    }
    // should never get here
    throw std::runtime_error("In function 'const std::string & TPositionsRaw::getJunkName(const MCMCIndexType & Index) const': Should never get here - did not find junk name for index " +toString(Index) + "!");
}

size_t TPositionsRaw::getPosition(const size_t &Index) const {
    assert(Index < _positions.size());
    return _positions[Index];
}

std::string TPositionsRaw::getPositionJunkAsString(const size_t & Index, const std::string & Delimiter) const{
    return toString(getPosition(Index)) + Delimiter + getJunkName(Index);
}

std::string TPositionsRaw::getJunkPositionAsString(const size_t & Index, const std::string & Delimiter) const{
    return getJunkName(Index) + Delimiter + toString(getPosition(Index));
}

void TPositionsRaw::setMaxSearchDistance(const size_t & MaxSearchDistance){
    _maxSearchDistance = MaxSearchDistance;
}

bool TPositionsRaw::_findJunk(const std::string & Junk){
    // same junk as last?
    if (Junk == _junkNames[_indexPreviouslyVisitedJunk]){
        _indexCurrentlyVisitedJunk = _indexPreviouslyVisitedJunk;
        return true;
    }
    // next?
    if (_indexPreviouslyVisitedJunk + 1 < _junkNames.size() && Junk == _junkNames[_indexPreviouslyVisitedJunk + 1]){
        _indexCurrentlyVisitedJunk = _indexPreviouslyVisitedJunk + 1;
        return true;
    }
    // previous?
    if (_indexPreviouslyVisitedJunk > 0 && Junk == _junkNames[_indexPreviouslyVisitedJunk - 1]){
        _indexCurrentlyVisitedJunk = _indexPreviouslyVisitedJunk - 1;
        return true;
    }
    // some other -> binary search
    try {
        _indexCurrentlyVisitedJunk = binarySearch_getIndex(_junkNames.begin(), _junkNames.end(), Junk);
        return true;
    } catch (const std::runtime_error & error){
        // not found
        return false;
    }
}

bool TPositionsRaw::_findPositionAfter(const uint32_t & Position, const size_t & Start){
    size_t until = Start+_maxSearchDistance;
    for (size_t i = Start; i < until; i++){
        // reached end of junk?
        if (i == _junkEnds[_indexCurrentlyVisitedJunk]){
            return false;
        }
        if (_positions[i] == Position){
            _indexCurrentlyVisitedPosition = i;
            return true;
        }
    }
    // not found within these first maxSearchDistance elements -> do binary search
    try {
        auto start = _positions.begin() + until; // skip elements we've already checked
        auto end = _positions.begin() + _junkEnds[_indexCurrentlyVisitedJunk]; // end of junk
        _indexCurrentlyVisitedPosition = binarySearch_getIndex(start, end, Position) + until;
        return true;
    } catch (const std::runtime_error & error){
        // not found
        return false;
    }
}

bool TPositionsRaw::_findPositionBefore(const uint32_t & Position, const size_t & Start){
    if (_indexPreviouslyVisitedPosition == 0){
        // there is no previous -> directly return (otherwise issues with negative size_t)
        return false;
    }

    // define beginning of current junk
    uint32_t beginJunk = 0;
    if (_indexCurrentlyVisitedJunk > 0){
        beginJunk = _junkEnds[_indexCurrentlyVisitedJunk-1];
    }

    // define until where we want to search (until is included)
    int until = 0;
    if (Start > _maxSearchDistance){
        until =  static_cast<int>(Start)-_maxSearchDistance+1;
    }

    bool stop = false;
    for (int i = static_cast<int>(Start); i >= until; i--){ // go backwards from Start
        if (stop){
            return false;
        }
        // reached beginning of junk? -> stop in next iteration
        if ((_indexCurrentlyVisitedJunk == 0 && i == 0) ||
            (_indexCurrentlyVisitedJunk >  0 && i == beginJunk)){
            stop = true;
        }
        if (_positions[i] == Position){
            _indexCurrentlyVisitedPosition = i;
            return true;
        }
    }
    // not found within these first maxSearchDistance elements -> do binary search
    try {
        auto start = _positions.begin() + beginJunk;
        auto end = _positions.begin() + until; // skip elements we've already checked
        _indexCurrentlyVisitedPosition = binarySearch_getIndex(start, end, Position) + beginJunk;
        return true;
    } catch (const std::runtime_error & error){
        // not found
        return false;
    }
}

bool TPositionsRaw::_findPosition(const uint32_t & Position){
    // same junk as last position?
    if (_indexCurrentlyVisitedJunk == _indexPreviouslyVisitedJunk){
        // position same as previous?
        if (Position == _positions[_indexPreviouslyVisitedPosition]){
            _indexCurrentlyVisitedPosition = _indexPreviouslyVisitedPosition;
            return true;
        } else if (Position > _positions[_indexPreviouslyVisitedPosition]){
            // position after previous?
            return _findPositionAfter(Position, _indexPreviouslyVisitedPosition+1);
        } else {
            // position before previous
            return _findPositionBefore(Position, _indexPreviouslyVisitedPosition-1);
        }
    }
    // previous junk?
    if (_indexCurrentlyVisitedJunk < _indexPreviouslyVisitedJunk){
        // start at end of that junk
        return _findPositionBefore(Position, _junkEnds[_indexCurrentlyVisitedJunk]-1);
    } else {
        // next junk
        // start at beginning of junk
        size_t startIndex = 0;
        if (_indexCurrentlyVisitedJunk > 0){
            startIndex += _junkEnds[_indexCurrentlyVisitedJunk-1]; // beginning of junk
        }
        return _findPositionAfter(Position, startIndex);
    }
}

bool TPositionsRaw::exists(const uint32_t & Pos, const std::string & Junk) {
    // Idea: we will usually parse positions in some more or less ordered fashion
    // -> usually we want just consecutive element of the one we've visited before
    // algorithm:
    // get new index -> check if it is on same junk as previous
    //    -> yes: if currentPos > lastPos -> search in consecutive elements of lastPos for currentPos
    //            -> if we can't find it (after searching for _maxSearchDistance positions) -> do binary search
    //            if currentPos < lastPos -> search in preceeding elements of lastPos for currentPos
    //            -> if we can't find it (after searching for _maxSearchDistance positions) -> do binary search
    //    -> no:  if newJunk > lastJunk -> start searching for element in first elements of newJunk
    //            -> if we can't find it (after searching for _maxSearchDistance positions) -> do binary search
    //            if newJunk < lastJunk -> start searching for element in last elements of newJunk
    //            -> if we can't find it (after searching for _maxSearchDistance positions) -> do binary search

    // first re-set all elements
    _found = false;
    _indexPreviouslyVisitedJunk = _indexCurrentlyVisitedJunk;
    _indexPreviouslyVisitedPosition = _indexCurrentlyVisitedPosition;

    // now find
    if (_findJunk(Junk)) {
        _found = _findPosition(Pos);
        if (!_found){
            // found junk, but not position -> reset junk variable
            _indexCurrentlyVisitedJunk = _indexPreviouslyVisitedJunk;
        }
    }
    return _found;
}

size_t TPositionsRaw::getIndex(const uint32_t & Pos, const std::string &Junk) const {
    // idea: always first check with exists() if element exists.
    // If yes, this search will already store the indices to the match, so we don't need to search twice
    if (_found){
        // security check: someone might forget to use exists() and getIndex() right after each other
        if (_positions[_indexCurrentlyVisitedPosition] != Pos || _junkNames[_indexCurrentlyVisitedJunk] != Junk){
            throw std::runtime_error("Position " + toString(Pos) + " on junk " + toString(Junk) + " is different than expected from lookup! Did you use getIndex() right after calling exists()?");
        }
        return _indexCurrentlyVisitedPosition;
    } else {
        throw std::runtime_error("Position " + toString(Pos) + " on junk " + toString(Junk) + " does not exist in TPositionsRaw! Always check first with exist() whether or not name class exists.");
    }
}

void TPositionsRaw::simulate(const std::vector<size_t> & JunkSizes, TRandomGenerator * RandomGenerator, double Lambda) {
    // JunkSizes: vector of length of junks -> e.g. {10, 50, 5} will simulate 3 junks of size 10, 50 and 5, respectively.
    // all distances = 1

    size_t position;
    std::string prefix = "junk_";
    size_t counter = 1;
    for(size_t last : JunkSizes){
        position = 0;
        for(size_t i = 0; i < last; ++i) {
            // add other
            position++;
            add(position, prefix + toString(counter));
        }
        counter++;
    }

    finalizeFilling();
}