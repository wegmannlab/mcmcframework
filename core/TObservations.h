//
// Created by madleina on 22.01.21.
//

#ifndef MCMCFRAMEWORK_TOBSERVATIONS_H
#define MCMCFRAMEWORK_TOBSERVATIONS_H

#include "TMCMCObservationsBase.h"
#include "TDataTranslator.h"
#include "TPrior.h"

//-------------------------
// TObservationsTranslated
//-------------------------
template <typename TranslatedType, typename StoredType> class TObservationsTranslated : public TObservationsBase, public std::enable_shared_from_this<TMCMCObservationsBase> {
    // class that stores data in a certain type (StoredType) and translates it for all interface-functions
protected:
    // values and dimensionality
    TMultiDimensionalStorage<StoredType> _storage;

    // prior
    std::shared_ptr<TPrior<TranslatedType>> _prior;

    // boundary (for simulating data) --> boundary is TranslatedType, because we need it for simulating values -> we check if
    // a randomly drawn value is within boundaries. Not used for mirroring (observations are never updated)!
    TBoundary<TranslatedType> _boundary;

    // translator
    std::unique_ptr<TDataTranslator<TranslatedType, StoredType>> _translator;

    // randomGenerator (for simulating data)
    TRandomGenerator * _randomGenerator;

    // temporary values for filling data
    size_t _counterSize;

    void _initialize(const std::shared_ptr<TPrior<TranslatedType>> Prior, const std::shared_ptr<TObservationDefinition> & def, TRandomGenerator* RandomGenerator);

    // get values (can not overload _getValue from TObservations because return type is different)
    virtual TranslatedType _getValue(const size_t & i) const;

    void _value(double & dest) const override;
    void _value(float & dest) const override;
    void _value(int & dest) const override;
    void _value(uint8_t & dest) const override;
    void _value(uint16_t & dest) const override;
    void _value(uint32_t & dest) const override;
    void _value(uint64_t & dest) const override;
    void _value(bool & dest) const override;

    void _value(const size_t & i, double & dest) const override;
    void _value(const size_t & i, float & dest) const override;
    void _value(const size_t & i, int & dest) const override;
    void _value(const size_t & i, uint8_t & dest) const override;
    void _value(const size_t & i, uint16_t & dest) const override;
    void _value(const size_t & i, uint32_t & dest) const override;
    void _value(const size_t & i, uint64_t & dest) const override;
    void _value(const size_t & i, bool & dest) const override;

    // set values
    template<class U> void _setAndTranslateBack(U val);
    template<class U> void _setAndTranslateBack(const size_t &i, U val);

    void _fillDataRaw(const StoredType & value);
    virtual void _fillDataAndTranslate(const TranslatedType & value);

    // protected constructor only accessible for dervied classes: allows for no translator
    TObservationsTranslated(const std::shared_ptr<TPrior<TranslatedType>> Prior, const std::shared_ptr<TObservationDefinition> & def, TRandomGenerator* RandomGenerator);

public:
    TObservationsTranslated();
    TObservationsTranslated(const std::shared_ptr<TPrior<TranslatedType>> Prior, const std::shared_ptr<TObservationDefinition> & def, TRandomGenerator* RandomGenerator, std::unique_ptr<TDataTranslator<TranslatedType, StoredType>> & Translator);
    virtual ~TObservationsTranslated() = default;

    // functions to create DAG
    void handPointerToPrior(const std::shared_ptr<TMCMCObservationsBase>& pointer) override;
    void constructDAG(TDAG &DAG, TDAG & temporaryDAG) override;
    void initializeStorage() override;
    void initializeStorageSingleElementBasedOnPrior() override;
    void initializeStorageBasedOnPrior(const std::vector<size_t> & ExpectedDimensions, const std::vector<std::shared_ptr<TNamesEmpty>> & DimensionNames) override;

    // set distances (for HMM prior)
    void setDistances(const std::shared_ptr<TDistancesBinnedBase> & distances) override;

    // fill data
    void prepareFillData(const size_t & guessLengthOfUnknownDimension, const std::vector<size_t> & allKnownDimensions) override;
    // fill data raw: directly fill storage
    void fillDataRaw(const double & value) override;
    void fillDataRaw(const float & value) override;
    void fillDataRaw(const int & value) override;
    void fillDataRaw(const uint8_t & value) override;
    void fillDataRaw(const uint16_t & value) override;
    void fillDataRaw(const uint32_t & value) override;
    void fillDataRaw(const uint64_t & value) override;
    void fillDataRaw(const bool & value) override;
    // fill data and translate: first translate to storedType
    void fillDataAndTranslate(const double & value) override;
    void fillDataAndTranslate(const float & value) override;
    void fillDataAndTranslate(const int & value) override;
    void fillDataAndTranslate(const uint8_t & value) override;
    void fillDataAndTranslate(const uint16_t & value) override;
    void fillDataAndTranslate(const uint32_t & value) override;
    void fillDataAndTranslate(const uint64_t & value) override;
    void fillDataAndTranslate(const bool & value) override;
    void finalizeFillData() override;

    // fill names
    void setDimensionName(const std::shared_ptr<TNamesEmpty> & Name, const size_t & Dim) override;
    const std::shared_ptr<TNamesEmpty> & getDimensionName(const size_t & Dim) override;

    //values (can not overload _getValue from TObservations because return type is different)
    double logValue() const override;
    double logValue(const size_t & i) const override;
    double expValue() const override;
    double expValue(const size_t & i) const override;
    size_t getValueUntranslated(const size_t & i) override;

    // boundaries
    double min() const override;
    double max() const override;

    // indexing and dimensions
    size_t getIndex(const std::vector<size_t>& coord) override;
    TRange getRange(const std::vector<size_t> & startCoord, const std::vector<size_t> & endCoord) override;
    TRange getFull() override;
    TRange getDiagonal() override;
    TRange get1DSlice(size_t dim, const std::vector<size_t> & startCoord) override;
    size_t numDim() const override;
    std::vector<size_t> dimensions() const override;
    size_t totalSize() const override;
    std::vector<size_t> getSubscripts(size_t linearIndex) const override;
    void resize(const std::vector<size_t> & Dimensions) override;

    // estimate initial prior parameters (e.g. with MLE)
    void estimateInitialPriorParameters(TLog * Logfile) override;

    // check for compatible template types (check for compatibility of translated type, not stored type)
    void reSetBoundaries(bool hasDefaultMin, bool hasDefaultMax, std::string min, std::string max, bool minIncluded, bool maxIncluded) override;
    bool valueIsInsideBoundary(const double & Value) const override;
    void typesAreCompatible(bool throwIfNot) const override;
    bool isFloatingPoint() const override;
    bool isIntegral() const override;
    bool isUnsigned() const override;

    // prior densities
    void updatePrior() override;
    double getLogPriorDensity() override;
    double getLogPriorDensity(const size_t & i) override;
    double getLogPriorDensityFull() override;

    // accessing
    void set(double val) override;
    void set(float val) override;
    void set(int val) override;
    void set(uint8_t val) override;
    void set(uint16_t val) override;
    void set(uint32_t val) override;
    void set(uint64_t val) override;
    void set(bool val) override;
    void set(const size_t & i, double val)  override;
    void set(const size_t & i, float val) override;
    void set(const size_t & i, int val) override;
    void set(const size_t & i, uint8_t val) override;
    void set(const size_t & i, uint16_t val) override;
    void set(const size_t & i, uint32_t val) override;
    void set(const size_t & i, uint64_t val) override;
    void set(const size_t & i, bool val) override;

    // write values
    void fillHeader(std::vector<std::string> &header) override;
    void writeVals(TOutputFile &file) override;

    // simulate values under prior distribution
    void simulateUnderPrior(TLog *logfile, TParameters & Parameters) override;
};

//-------------------------
// TObservationsRaw
//-------------------------

template <typename T> class TObservationsRaw : public TObservationsTranslated<T, T> {
    // class that stores untranslated observations
protected:

    // fill data
    void _fillDataAndTranslate(const T & value) override;

    // get values
    T _getValue(const size_t & i) const override;

    // set values
    template<class U> void _set(U val);
    template<class U> void _set(const size_t &i, U val);

public:
    TObservationsRaw();
    TObservationsRaw(const std::shared_ptr<TPrior<T>> Prior, const std::shared_ptr<TObservationDefinition> & def, TRandomGenerator* RandomGenerator);
    virtual ~TObservationsRaw() = default;

    //values
    double logValue() const override;
    double logValue(const size_t & i) const override;
    double expValue() const override;
    double expValue(const size_t & i) const override;

    // accessing
    void set(double val) override;
    void set(float val) override;
    void set(int val) override;
    void set(uint8_t val) override;
    void set(uint16_t val) override;
    void set(uint32_t val) override;
    void set(uint64_t val) override;
    void set(bool val) override;
    void set(const size_t & i, double val)  override;
    void set(const size_t & i, float val) override;
    void set(const size_t & i, int val) override;
    void set(const size_t & i, uint8_t val) override;
    void set(const size_t & i, uint16_t val) override;
    void set(const size_t & i, uint32_t val) override;
    void set(const size_t & i, uint64_t val) override;
    void set(const size_t & i, bool val) override;
};

#include "TObservations.tpp"

#endif // MCMCFRAMEWORK_TOBSERVATIONS_H