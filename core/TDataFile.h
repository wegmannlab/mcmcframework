//
// Created by madleina on 20.04.21.
//

#ifndef BANGOLIN_TDATAFILE_H
#define BANGOLIN_TDATAFILE_H

#include "TDataFileNames.h"

//--------------------
// TDataFileBase
//--------------------

class TDataFileBase {
protected:
    // filename
    std::string _filename;

    // information on dimensions
    size_t _numDim;

    // information on names
    std::unique_ptr<TRowNames> _rowNames;
    std::unique_ptr<TColNameBase> _colNames;

    std::vector<char> _delimiters;

    template<class T> std::vector<T> _getAllExceptFirst(const std::vector<T> &Vec);

    // check functions
    void _checkDelimiters();

public:
    TDataFileBase();
    virtual ~TDataFileBase() = default;

    void setDelimiterRowNames(const char & DelimiterRowNames);
    void setDelimiterConcatenationColNames(const char & DelimiterConcatenation);
};

//--------------------
// TDataWriterBase
//--------------------

template <typename TYPE>
class TDataWriterBase : public TDataFileBase {
    // class for writing a data file
protected:
    using TDataFileBase::_filename;
    using TDataFileBase::_rowNames;
    using TDataFileBase::_colNames;
    using TDataFileBase::_delimiters;

    // output file
    std::ostream* _filePointer;
    bool _isOpen;

    // observation
    std::shared_ptr<TMCMCObservationsBase> _observation;

    // open file
    void _openFile();
    void _closeFile();

    // extract names from observations
    std::vector<std::shared_ptr<TNamesEmpty>> _extractNamesFromObservations(const std::shared_ptr<TMCMCObservationsBase> & Observation, const size_t & NumDim);
    void _initialize(const std::vector<std::shared_ptr<TNamesEmpty>> & Names, const std::string & Filename, const std::shared_ptr<TMCMCObservationsBase> &Observation, const std::vector<char> & Delimiters, const std::vector<bool> & NameIsWritten, const colNameTypes & ColNameType);

    // write header
    void _writeHeader();

    // write
    void _write();
    void _write(const std::vector<size_t> & BlockDimensions, const std::vector<char> & BlockDelimiters, std::vector<char> &DelimitersLookupOneBlock, const std::vector<std::vector<size_t>> &CoordinatesOneBlock);
    void _ensureLastBlockEndsWithNewline(const size_t & CurBlock, const size_t & BlockSize, std::vector<char> &DelimitersLookupOneBlock);

    // check functions
    void _checkInput();

public:
    TDataWriterBase();
    ~TDataWriterBase() override;

    void write(const std::string & Filename, const std::shared_ptr<TMCMCObservationsBase> &Observation, const std::vector<char> & Delimiters, const std::vector<bool> & NameIsWritten, const colNameTypes & ColNameType);
    void write(const std::vector<std::shared_ptr<TNamesEmpty>> & Names, const std::string & Filename, const std::shared_ptr<TMCMCObservationsBase> &Observation, const std::vector<char> & Delimiters, const std::vector<bool> & NameIsWritten, const colNameTypes & ColNameType);
};

//--------------------
// TDataReader
//--------------------

template <typename TYPE>
class TDataReaderBase : public TDataFileBase {
    // pure virtual base class for reading a data file
    // derived classes differ in the way they read the header (override pure virtual function _readHeader())

protected:
    using TDataFileBase::_filename;
    using TDataFileBase::_rowNames;
    using TDataFileBase::_colNames;
    using TDataFileBase::_delimiters;

    // observation
    std::shared_ptr<TObservationsBase> _observation;

    // input file
    std::istream* _filePointer;
    bool _isOpen;
    bool _isZipped;
    std::string _delimiterComment; // which delimiter is used to comment lines

    // for each element in one block: store it, and if yes in which order?
    std::vector<bool> _storeCell;
    std::vector<size_t> _indexCell_inOneBlockObservation;
    std::vector<size_t> _dimensionsOfObservation;

    // settings
    bool _throwIfTitleDoesntMatch;

    // tweaks
    bool _addedArtificialFirstDimension; // for 1D-objects

    // open file
    void _openFile();
    void _closeFile();
    void _restartAtBeginning();

    // infer dimensions
    void _inferDimensions(std::vector<size_t> &Dimensions, std::vector<bool> &DimensionsFilled, size_t & GuessLengthFirstDimension);
    void _parseFirstLine(std::vector<size_t> & Dimensions, std::vector<bool> & DimensionsFilled);
    void _finalCheck_InferDimensions(const std::vector<size_t> & Dimensions, const std::vector<bool> & DimensionsFilled);
    void _countNumberDelims_oneLine(const std::string & Line, std::map<char, size_t> & DelimCounter, const bool & FirstLine);
    void _inferDimensions_oneLine(std::map<char, size_t> & DelimCounter, std::vector<size_t> & Dimensions, std::vector<bool> & DimensionsFilled);
    size_t _getProductOverAllDim_WithSameDelim(const size_t & Dim, const std::vector<size_t> & Dimensions, const std::vector<bool> & DimensionsFilled);
    size_t _removeAllUpperColumns_FromDelimCounter(const size_t & TotalCountsDelim, const size_t & Dim, const std::vector<size_t> & Dimensions, const std::vector<bool> & DimensionsFilled);
    void _translateCoordinates_toIndexInObservation(const size_t & BlockSize, const size_t & NumDimBlock, const std::vector<std::vector<bool>> & ShouldBeStored, const std::vector<std::vector<size_t>> & CoordinatesToStore);

    // read header
    void _readHeader(std::vector<size_t> & Dimensions, std::vector<bool> &DimensionsFilled);
    std::vector<std::string> _readHeaderIntoVector();

    // read rest
    void _read(std::vector<size_t> & Dimensions, const size_t & GuessLengthUnknownDimension, const bool & ReadDataRaw);
    void _read(const std::vector<size_t> & BlockDimensions, const std::vector<char> & BlockDelimiters, const std::vector<std::vector<size_t>> &CoordinatesOneBlock, const std::vector<char> &DelimitersLookupOneBlock, const size_t & GuessLengthUnknownDimension, const bool & ReadDataRaw);
    void _translateBlockCoordinates_toIndexInObservation(const std::vector<size_t> & BlockDimensions, const std::vector<std::vector<size_t>> &CoordinatesOneBlock);
    void _prepareStorageObservation(const size_t & GuessLengthUnknownDimension);
    void _storeDataInTempVec(const std::string & DataPoint, std::vector<TYPE> & OneBlock_forObservation, const bool & KeepLine, const size_t & ElementInBlock);
    void _storeInObservation(const std::vector<TYPE> & OneBlock_forObservation, const bool & KeepLine, const bool & ReadDataRaw);
    void _finalizeFillingObservation();

    // check functions
    void _checkInput(std::vector<size_t> &Dimensions, std::vector<bool> &DimensionsFilled);
    void _checkIfBlockSizeCanBeInferred(const std::vector<bool> &DimensionsFilled);
    void _checkDimensionsFromDeveloper(std::vector<size_t> &Dimensions, std::vector<bool> &DimensionsFilled);
    void _initialize(const std::string &Filename, const size_t &NumDim, const std::shared_ptr<TObservationsBase> &Observation, const std::vector<std::shared_ptr<TNamesEmpty>> &Names, const std::vector<char> &Delimiters, const std::vector<bool> &NameIsWritten, std::vector<size_t> &Dimensions, std::vector<bool> &DimensionsFilled, const size_t & GuessLengthUnknownDimension, const colNameTypes & ColNameType);


public:
    TDataReaderBase();
    ~TDataReaderBase() override;

    void read(const std::string &Filename, size_t NumDim, const std::shared_ptr<TObservationsBase> &Observation, std::vector<std::shared_ptr<TNamesEmpty>> Names, std::vector<char> Delimiters, std::vector<bool> NameIsWritten, std::vector<size_t> Dimensions, std::vector<bool> DimensionsFilled, size_t GuessLengthUnknownDimension, const colNameTypes & ColNameType, const bool & ReadDataRaw);

    // settings
    void throwIfTitleDoesntMatch(const bool & ThrowIfTitleDoesntMatch);
};

#include "TDataFile.tpp"

#endif //BANGOLIN_TDATAFILE_H
