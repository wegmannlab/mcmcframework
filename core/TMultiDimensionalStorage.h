//
// Created by caduffm on 9/24/20.
//

#ifndef MCMCFRAMEWORK_TSTORAGE_H
#define MCMCFRAMEWORK_TSTORAGE_H

#include <memory>
#include <numeric>
#include "TValue.h"
#include "stringFunctions.h"
#include "TNames.h"

//-------------------------------------------
// TRange
//-------------------------------------------

class TRange {
public:
    size_t first;
    size_t last;
    size_t increment;

    TRange(){
        first = 0;
        last = 0;
        increment = 0;
    };
    TRange(size_t f, size_t l, size_t inc){
        first = f;
        last = l;
        increment = inc;
    };
};

//-------------------------------------------
// TDimension
//-------------------------------------------

class TDimension {
    // nice explanation of concept: https://eli.thegreenplace.net/2015/memory-layout-of-multi-dimensional-arrays
protected:
    size_t _numDim;
    std::vector<size_t> _dimensions;
    size_t _totalSize;

    // checks
    bool _validateCoordinates(const std::vector<size_t> & coord) const;
    static inline size_t _linearizeIndex(const std::vector<size_t> & coord, const size_t & NumDim, const std::vector<size_t> & Dimensions) {
        // write out 1, 2 and 3 dimensions for speed
        if (NumDim == 1){
            return coord[0];
        } else if (NumDim == 2){
            return coord[0] * Dimensions[1] + coord[1];
        } else if (NumDim == 3){
            return (coord[0] * Dimensions[1] + coord[1]) * Dimensions[2] + coord[2];
        } else {
            size_t index = 0;
            for (size_t i = 0; i < NumDim; i++) {
                double prod = 1.;
                for (size_t j = i + 1; j < NumDim; j++) {
                    prod *= Dimensions[j];
                }
                index += prod * coord[i];
            }
            return index;
        }
    };
    inline size_t _linearizeIndex(const std::vector<size_t> & coord) const {
        return _linearizeIndex(coord, _numDim, _dimensions);
    };

public:
    TDimension();
    explicit TDimension(const std::vector<size_t> & dimensions);
    virtual void init(const std::vector<size_t> & dimensions);

    // linearize multi-dimensional array
    size_t getIndex(const std::vector<size_t>& coord) const;
    static size_t getIndex(const std::vector<size_t>& coord, const size_t & NumDim, const std::vector<size_t> &Dimensions);
    TRange getDiagonal() const;
    TRange getRange(const std::vector<size_t> & startCoord, const std::vector<size_t> & endCoord) const;
    TRange getFull() const;
    TRange get1DSlice(size_t dim, const std::vector<size_t> & startCoord) const;
    size_t totalSize() const;
    size_t numDim() const;
    std::vector<size_t> dimensions() const;

    // re-construct index in multi-dimensional array from linear array
    std::vector<size_t> getSubscripts(size_t linearIndex) const;
    static std::vector<size_t> getSubscripts(size_t linearIndex, const size_t & NumDim, const std::vector<size_t> &Dimensions);
};

//-------------------------------------------
// TMultiDimensionalStorage
//-------------------------------------------

template <class T> class TMultiDimensionalStorage {
    // stores values and dimensions -> manages dimensionality of values

protected:
    // pointer to a vector of TValueBase
    std::unique_ptr<TValueVectorBase<T>> _values;

    // dimensions
    TDimension _dimension;

    // names (one per dimension)
    std::vector<std::shared_ptr<TNamesEmpty>> _dimensionNames;
    void _resizeDimensionNames();

public:
    TMultiDimensionalStorage();
    TMultiDimensionalStorage(const std::vector<size_t> & dimensions, bool trackExponential, bool trackLog, bool isUpdated);
    virtual ~TMultiDimensionalStorage() = default;
    virtual void init(const std::vector<size_t> & dimensions, bool trackExponential, bool trackLog, bool isUpdated);

    // resize
    virtual void resize(const std::vector<size_t> & dimensions);
    virtual void reserve(const std::vector<size_t> & dimensions);

    // set names
    void setDimensionName(const std::shared_ptr<TNamesEmpty> & Name, const size_t & Dim);
    void setDimensionNames(const std::vector<std::shared_ptr<TNamesEmpty>> & Names);

    // get names
    const std::shared_ptr<TNamesEmpty> & getDimensionName(const size_t &Dim) const;
    void fillFullDimensionName(const std::vector<size_t>& Coord, std::vector<std::string> & Name) const;
    void fillFullDimensionName(const size_t & LinearIndex, std::vector<std::string> & Name) const;
    std::string getFullDimensionNameAsString(const size_t & LinearIndex, const std::string & Delimiter = "_") const;
    std::string getFullDimensionNameAsString(const std::vector<size_t>& Coord, const std::string & Delimiter = "_") const;
    void appendToVectorOfAllFullDimensionNamesWithPrefix(std::vector<std::string> & FullNames, const std::string & Prefix, const std::string &Delimiter = "_") const;
    void appendToVectorOfAllFullDimensionNames(std::vector<std::string> & FullNames, const std::string &Delimiter = "_") const;

    // wrappers of TDimension
    virtual size_t getIndex(const std::vector<size_t>& coord) const;
    virtual TRange getRange(const std::vector<size_t> & startCoord, const std::vector<size_t> & endCoord) const;
    virtual TRange getFull() const;
    virtual TRange getDiagonal() const;
    virtual TRange get1DSlice(size_t dim, const std::vector<size_t> & startCoord) const;
    virtual size_t totalSize() const;
    size_t numDim() const;
    std::vector<size_t> dimensions() const;
    std::vector<size_t> getSubscripts(size_t linearIndex) const;

    // wrappers for TValue
    virtual void initBoth(T val);
    virtual void initBoth(const size_t & i, T val);
    virtual void setVal(T val);
    virtual void setVal(const size_t & i, T val);
    virtual void set(T val);
    virtual void set(const size_t & i, T val);
    virtual void emplace_back(const T & value);
    virtual void reset();
    virtual void reset(const size_t & i);
    virtual T value() const;
    virtual T value(const size_t & i) const;
    virtual T oldValue() const;
    virtual T oldValue(const size_t & i) const;
    virtual double expValue() const;
    virtual double expValue(const size_t & i) const;
    virtual double oldExpValue() const;
    virtual double oldExpValue(const size_t & i) const;
    virtual double logValue() const;
    virtual double logValue(const size_t & i) const;
    virtual double oldLogValue() const;
    virtual double oldLogValue(const size_t & i) const;
};

template <class T> class TSingleStorage : public TMultiDimensionalStorage<T> {
    // for single parameters -> only store a single value
    // we want to write param.value() -> need to write wrappers for TValue functions!
private:
    std::unique_ptr<TValueBase<T>> _value;

public:
    TSingleStorage();
    explicit TSingleStorage(bool trackExponential, bool trackLog, bool isUpdated);
    virtual ~TSingleStorage() = default;
    void init(const std::vector<size_t> & dimensions, bool trackExponential, bool trackLog, bool isUpdated) override;

    // resize
    virtual void resize(const std::vector<size_t> & dimensions);
    virtual void reserve(const std::vector<size_t> & dimensions);

    // wrappers for TValue
    void initBoth(T val) override;
    void initBoth(const size_t & i, T val) override;
    void setVal(T val) override;
    void setVal(const size_t & i, T val) override;
    void set(T val) override;
    void set(const size_t & i, T val) override;
    void emplace_back(const T & value) override;
    void reset() override;
    void reset(const size_t & i) override;
    T value() const override;
    T value(const size_t & i) const override;
    T oldValue() const override;
    T oldValue(const size_t & i) const override;
    double expValue() const override;
    double expValue(const size_t & i) const override;
    double oldExpValue() const override;
    double oldExpValue(const size_t & i) const override;
    double logValue() const override;
    double logValue(const size_t & i) const override;
    double oldLogValue() const override;
    double oldLogValue(const size_t & i) const override;
};

#include "TMultiDimensionalStorage.tpp"

#endif //MCMCFRAMEWORK_TSTORAGE_H
