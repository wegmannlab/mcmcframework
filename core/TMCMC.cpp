//
// Created by Madleina Caduff on 03.04.20.
//

#ifdef WITH_PROFILING
#include <gperftools/profiler.h>
#endif // WITH_PROFILING

#include "TMCMC.h"

//-------------------------------------------
// TMCMCFramework
//-------------------------------------------

TMCMCFramework::TMCMCFramework(TLog* Logfile, TRandomGenerator* RandomGenerator, TParameters & Parameters){
    _logfile = Logfile;
    _randomGenerator = RandomGenerator;

}

void TMCMCFramework::_setDAGBuilder(const std::shared_ptr<TDAGBuilder> & DAGBuilder){
    _DAGBuilder = DAGBuilder;
}

//-------------------------------------------
// TMCMC
//-------------------------------------------

TMCMC::TMCMC(TLog* Logfile, TRandomGenerator* RandomGenerator, TParameters & Parameters) : TMCMCFramework(Logfile, RandomGenerator, Parameters){
    _writeStateFile = false;
    _iterations = 0;
    _thinning = 1;
    _burnin = 0;
    _numBurnin = 0;
    _writeBurnin = false;
    _thinningStateFile = 0;

    _init(Parameters);
}

TMCMC::~TMCMC()= default;

void TMCMC::_init(TParameters & params){
    //reading MCMC parameters
    //iterations
    _logfile->startIndent("Reading MCMC parameters:");
    _iterations = params.getParameterWithDefault<int>("iterations", 10000);
    _logfile->list("Will run an MCMC for " + toString(_iterations) + " iterations.");

    //_burnin
    _burnin  = params.getParameterWithDefault<int>("burnin", 1000);
    _numBurnin = params.getParameterWithDefault<int>("numBurnin", 10);
    if(_numBurnin > 0)
        _logfile->list("Will run " + toString(_numBurnin) + " burnins of " + toString(_burnin) + " iterations each.");

    //writing a state file? (= file with values and jumpSizes for all parameters, can be used to re-start MCMC)
    _thinningStateFile = params.getParameterWithDefault<int>("thinningStateFile", 0);
    if (_thinningStateFile < 0) throw "Parameter 'thinningStateFile' must be > 0!";
    else if (_thinningStateFile > 0){
        _writeStateFile = true;
        _logfile->list("Will write the state of the MCMC chain every " + toString(_thinningStateFile) + " iterations.");
    }

    //write trace files during burnin?
    if (params.parameterExists("writeBurnin")){
        _writeBurnin = true;
        _logfile->list("Will write the trace of the burnin to file, too.");
    }

    //thinning
    _thinning = params.getParameterWithDefault<int>("thinning", 10);
    if(_thinning < 1) throw "Thinning must be > 0!";
    if(_thinning == 1)
        _logfile->list("Will write full chain.");
    else if(_thinning == 2)
        _logfile->list("Will write every second iteration.");
    else if(_thinning == 3)
        _logfile->list("Will write every third iteration.");
    else
        _logfile->list("Will write every " + toString(_thinning) + "th iteration.");
    _logfile->endIndent();
}

void TMCMC::_prepareFiles(const std::string & Prefix) {
    _DAGBuilder->_prepareAllMCMCFiles(Prefix, _writeStateFile);
}

void TMCMC::_closeFiles() {
    _DAGBuilder->_closeAllMCMCFiles();
}

void TMCMC::runMCMC(const std::string & Prefix, const std::shared_ptr<TDAGBuilder> & DAGBuilder) {
    // set DAG builder
    _setDAGBuilder(DAGBuilder);

    // prepare files
    _prepareFiles(Prefix);

    // run initialization (MLE etc)
    _runPriorParameterInitialization();

    // run MCMC
    _runMCMC();
}

void TMCMC::_runPriorParameterInitialization() {
    _logfile->startIndent("Running prior parameter initialization:");

    // initialize prior parameters with MLE/MOM/EM etc.
    _DAGBuilder->_estimateInitialPriorParameters();

    _logfile->endIndent();
    _logfile->overList("done!");
}

void TMCMC::_runMCMC(){
    // run MCMC
    _logfile->startIndent("Running MCMC inference:");

    #ifdef WITH_PROFILING
    ProfilerStart("profile.prof");
    #endif // WITH_PROFILING

    //run _burnin
    if(_numBurnin > 0){
        if(_numBurnin > 1) {
            _logfile->startNumbering("Running " + toString(_numBurnin) + " burnins:");
        } else {
            _logfile->startNumbering("Running " + toString(_numBurnin) + " burnin:");
        }
        for(int b=0; b < _numBurnin; ++b){
            _logfile->number("Burnin number " + toString(b + 1) + ":");
            _logfile->addIndent(2);
            _runBurnin();
            _logfile->removeIndent(2);
        }
        _logfile->endNumbering();
    }

    //run MCMC chain
    _logfile->startIndent("Running MCMC chain:");
    _runMCMCChain();
    _closeFiles();
    _logfile->endIndent();
    _logfile->endIndent();

    #ifdef WITH_PROFILING
    ProfilerStop();
    #endif // WITH_PROFILING
}

void TMCMC::_runBurnin(){
    std::string report = "Running a burnin of " + toString(_burnin) + " iterations ... ";
    _logfile->listFlush(report + "(0%)");

    // write values of initialization to file, if needed
    if (_writeBurnin){
        _writeToTraceFiles();
    }

    int prog;
    int prog_old = 0;
    for(int i=0; i < _burnin; ++i){
        _runMCMCIterationFull();

        //print to file?
        if (_writeBurnin) {
            if (i % _thinning == 0)
                _writeToTraceFiles();
            if (_writeStateFile && i % _thinningStateFile == 0)
                _writeToStateFile(i);
        }

        //report progress
        prog = static_cast<int>(100. * (double) i / (double) _burnin);
        if(prog > prog_old){
            _logfile->overListFlush(report, "(", prog, "%)");
            prog_old = prog;
        }
    }
    _logfile->overList(report + "done! ");

    //report acceptance rates
    _reportAcceptanceRates();

    //adjust proposal ranges
    _logfile->listFlush("Adjusting proposal parameters ...");
    _adjustProposalRanges();
    _logfile->done();
}

void TMCMC::_runMCMCChain(){
    std::string report = "Running an MCMC chain of " + toString(_iterations) + " iterations ... ";
    _logfile->listFlush(report + "(0%)");

    int prog;
    int prog_old = 0;
    for(int i=0; i<_iterations; ++i){
        _runMCMCIterationFull();

        //print to file?
        if(i % _thinning == 0)
            _writeToTraceFiles();
        if (_writeStateFile && i % _thinningStateFile == 0)
            _writeToStateFile(i);

        //report progress
        prog = static_cast<int>(100. * (double) i / (double) _iterations);
        if(prog > prog_old){
            _logfile->overListFlush(report, "(", prog, "%)");
            prog_old = prog;
        }
    }
    // write final iteration to state file
    if (_writeStateFile)
        _writeToStateFile(_iterations - 1);
    _logfile->overList(report + "done! ");

    //report acceptance rates
    _reportAcceptanceRates();
    _writeToMeanVarFiles();
}

void TMCMC::_runMCMCIterationFull(){
    _DAGBuilder->_updateParameters_MCMC();
}

void TMCMC::_reportAcceptanceRates(){
    for (auto &it : _DAGBuilder->_nameToParamMap){
        it.second->printAccRateToLogfile(_logfile);
    }
}

void TMCMC::_adjustProposalRanges(){
    for (auto &it : _DAGBuilder->_nameToParamMap){
        it.second->adjustProposalAndResetCounters();
    }
}

void TMCMC::_writeToTraceFiles(){
    for (auto &it : _DAGBuilder->_traceFiles){
        it->write();
    }
}

void TMCMC::_writeToMeanVarFiles(){
    for (auto &it : _DAGBuilder->_meanVarFiles){
        it->write();
    }
}

void TMCMC::_writeToStateFile(int iterations){
    if (_writeStateFile)
        _DAGBuilder->_stateFile->write(iterations);
}

//-------------------------------------------
// TSimulator
//-------------------------------------------

TSimulator::TSimulator(TLog* Logfile, TRandomGenerator* RandomGenerator, TParameters & Parameters) : TMCMCFramework(Logfile, RandomGenerator, Parameters){}

TSimulator::~TSimulator()= default;

void TSimulator::_prepareFiles(const std::string & Prefix){
    _DAGBuilder->_prepareAllSimulationFiles(Prefix);
}

void TSimulator::_closeFiles() {
    _DAGBuilder->_closeAllSimulationFiles();
}

void TSimulator::simulate(const std::string & Prefix, const std::shared_ptr<TDAGBuilder> & DAGBuilder, TParameters & Parameters) {
    // set DAG builder
    _setDAGBuilder(DAGBuilder);

    // simulate
    _logfile->startIndent("Simulating under prior distribution:");

    _DAGBuilder->_simulate(Parameters);
    writeFiles(Prefix);

    _logfile->endIndent();
}

void TSimulator::writeFiles(const std::string & Prefix){
    // write files
    _prepareFiles(Prefix);
    for (auto &it : _DAGBuilder->_simulationFiles){
        it->write();
    }
    _closeFiles();
}
