//
// Created by madleina on 28.07.20.
//

#include "TMultiDimensionalStorage.h"

//-------------------------------------------
// TDimension
//-------------------------------------------

TDimension::TDimension() {
    _totalSize = 0;
    _numDim = 0;
}

TDimension::TDimension(const std::vector<size_t> & dimensions) {
    init(dimensions);
}

void TDimension::init(const std::vector<size_t> &dimensions) {
    _numDim = dimensions.size();
    _dimensions = dimensions;
    // multiplies all dimensions
    if (_dimensions.empty()){
        _totalSize = 0;
    } else {
        _totalSize = std::accumulate(std::begin(_dimensions), std::end(_dimensions), 1, std::multiplies<>());
    }
}

size_t TDimension::totalSize() const{
    return _totalSize;
}

size_t TDimension::numDim() const {
    return _numDim;
}

std::vector<size_t> TDimension::dimensions() const{
    return _dimensions;
}

bool TDimension::_validateCoordinates(const std::vector<size_t>& coord) const {
    // check if size of given coordinates is correct
    if (coord.size() != _numDim) {
        throw std::runtime_error("Number of dimensions of coordinate vector (=" + toString(coord.size()) +
                                 ") does not match number of dimensions of multi-array (=" + toString(_numDim) + ")!");
    }
    // check if given coordinates are within array
    for (auto i = 0; i < _numDim; i++){
        if (coord[i] >= _dimensions[i]) {
            throw std::runtime_error("Coordinate of dimension " + toString(i) + " (=" + toString(coord[i]) +
                                     ") is outside of multi-array (=" + toString(_dimensions[i]) + ")!");
        }
    }
    return true;
}

size_t TDimension::getIndex(const std::vector<size_t>& coord) const{
    assert(_validateCoordinates(coord));
    return _linearizeIndex(coord);
}

size_t TDimension::getIndex(const std::vector<size_t>& coord, const size_t & NumDim, const std::vector<size_t> &Dimensions) {
    return _linearizeIndex(coord, NumDim, Dimensions);
}

TRange TDimension::getDiagonal() const {
    if (_numDim != 2 || _dimensions[0] != _dimensions[1]) // must be 2D square matrix
        throw std::runtime_error("In function 'TRange TDimension::getDiagonal() const': diagonal is only implemented for 2D square matrices!");
    TRange range(0, totalSize(), _dimensions[0] + 1);
    return range;
}

TRange TDimension::getRange(const std::vector<size_t> &startCoord, const std::vector<size_t> &endCoord) const {
    assert(_validateCoordinates(startCoord));
    assert(_validateCoordinates(endCoord));

    size_t first = _linearizeIndex(startCoord);
    size_t last = _linearizeIndex(endCoord);

    if (startCoord > endCoord)
        throw std::runtime_error("Start coordinate (" + toString(first) + ") in function getRange() corresponds to a larger index in linear array than end coordinate (" + toString(last) + ")!");
    TRange range(first, last + 1, 1); // last + 1, as otherwise we could not access single elements by giving the same start and end coordinates
    return range;
}

TRange TDimension::get1DSlice(size_t dim, const std::vector<size_t> &startCoord) const {
    if (dim >= _numDim) {
        throw std::runtime_error("Dimension " + toString(dim) + " in function get1DSlice() does not exist! (Number of dimensions: " + toString(_numDim) + ").");
    }
    assert(_validateCoordinates(startCoord));

    size_t first = _linearizeIndex(startCoord);
    // where's the last?
    std::vector<size_t> endCoord = startCoord;
    endCoord[dim] = _dimensions[dim]; // go until the end of this dimension
    size_t last = _linearizeIndex(endCoord);
    // what is the increment?
    double inc = 1.;
    for (size_t j = dim + 1; j < _numDim; j++){
        inc *= _dimensions[j];
    }

    TRange range(first, last, inc);
    return range;
}

TRange TDimension::getFull() const {
    TRange range(0, totalSize(), 1); // no need to do last + 1
    return range;
}

std::vector<size_t> TDimension::getSubscripts(size_t linearIndex, const size_t & NumDim, const std::vector<size_t> &Dimensions) {
    // algorithm from https://stackoverflow.com/questions/46782444/how-to-convert-a-linear-index-to-subscripts-with-support-for-negative-strides
    // write out 1 and 2 dimensions for speed
    if (NumDim == 1){
        return {linearIndex};
    } else if (NumDim == 2){
        size_t coord2 = linearIndex % Dimensions[1];
        size_t coord1 = (linearIndex - coord2) / Dimensions[1];
        return {coord1, coord2};
    } else if (NumDim == 3){
        size_t coord3 = linearIndex % Dimensions[2];
        linearIndex -= coord3;
        linearIndex /= Dimensions[2];
        size_t coord2 = linearIndex % Dimensions[1];
        size_t coord1 = (linearIndex - coord2) / Dimensions[1];
        return {coord1, coord2, coord3};
    } else {
        std::vector<size_t> coords(NumDim);

        // go over all dimensions, start at most inner one
        for (int i = NumDim - 1; i >= 0; i--) {
            // get the index in the current dimension
            size_t indexInCurDimension = linearIndex % Dimensions[i];
            coords[i] = indexInCurDimension;
            // update linear index
            linearIndex -= indexInCurDimension;
            linearIndex /= Dimensions[i];
        }
        return coords;
    }
}

std::vector<size_t> TDimension::getSubscripts(size_t linearIndex) const {
    // validate index
    if (linearIndex >= totalSize())
        throw std::runtime_error("Linear index (" + toString(linearIndex) + ") in function getIndexInArray() is larger than total size of array (" + toString(totalSize()) + ")!");

    return getSubscripts(linearIndex, _numDim, _dimensions);
}