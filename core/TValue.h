//
// Created by madleina on 21.01.21.
//

#ifndef EMPTY_TVALUE_H
#define EMPTY_TVALUE_H

#include <cassert>
#include <type_traits>
#include <numeric>
#include <vector>
#include <cstdint>

//-------------------------------------------
// TValueBase
//-------------------------------------------
template <class T>
class TValueBase {
    // a pure virtual base class
protected:
    // overridden in derived classes
    virtual void _setValue(T val) = 0;
    virtual void _set(T val) = 0;
    // can stay empty, if there is no oldValue
    virtual void _setOldValue(T val){};
    virtual void _reset(){};

public:
    TValueBase() = default;
    virtual ~TValueBase() = default;

    void initBoth(T val) {
        _setValue(val);
        _setOldValue(val);
    };
    void setVal(T val) {
        _setValue(val);
    };
    void set(T val) {
        _set(val);
    };
    void reset() {
        _reset();
    };

    virtual T value() const = 0;
    virtual T oldValue() const = 0;
    virtual double expValue() const = 0;
    virtual double oldExpValue() const = 0;
    virtual double logValue() const = 0;
    virtual double oldLogValue() const = 0;
};

//-------------------------------------------
// TValueFixed
//-------------------------------------------
template <class T>
class TValueFixed : virtual public TValueBase<T> {
    // class that only stores _value, but not _value_old
protected:
    T _value;

    // overridden in derived classes
    void _setValue(T val) override{
        _value = val;
    };
    virtual void _set(T val) override{
        _setValue(val);
    };

public:
    TValueFixed() {
        _value = 0;
    }
    ~TValueFixed() override = default;

    virtual T value() const{
        return _value;
    }

    T oldValue() const override{
        return _value;
    }

    double expValue() const override{
        // should normally not be used (if you need to keep track of the exponential, use class TValueWithExpVal)
        return exp(_value);
    }

    double oldExpValue() const override{
        // should normally not be used (if you need to keep track of the exponential, use class TValueWithExpVal)
        return expValue();
    }

    double logValue() const override{
        // should normally not be used (if you need to keep track of the log, use class TValueWithLogVal)
        assert(_value > 0);
        return log(_value);
    }

    double oldLogValue() const override{
        // should normally not be used (if you need to keep track of the log, use class TValueWithLogVal)
        return logValue();
    }
};

//-------------------------------------------
template <class T>
class TValueFixedWithExponential : virtual public TValueFixed<T> {
    // keeps track of exp(x) -> whenever x is changed, we change exp(x) too
protected:
    double _exp_value;

    void _setValue(T val) override{
        TValueFixed<T>::_setValue(val);
        _exp_value = exp(this->_value);
    };

public:
    TValueFixedWithExponential() : TValueFixed<T>() {
        _exp_value = exp(this->_value);
    }
    ~TValueFixedWithExponential() override = default;

    double expValue() const override {
        return _exp_value;
    }

    double oldExpValue() const override {
        return expValue();
    }
};

//-------------------------------------------
template <class T>
class TValueFixedWithLog : virtual public TValueFixed<T> {
    // keeps track of log(x) -> whenever x is change, we change log(x) too
protected:
    double _log_value;

    void _setValue(T val) override{
        TValueFixed<T>::_setValue(val);
        assert(this->_value > 0);
        _log_value = log(this->_value);
    };

public:
    template <typename U = T,
            std::enable_if_t<std::is_same_v<U, T>, int> = 0,
            std::enable_if_t<std::is_floating_point<U>::value, int> = 0>
    TValueFixedWithLog() : TValueFixed<T>() {
        // constructor for floating points: take small floating point number
        // initialize to something small, because log(0)=-Inf
        _setValue(0.000001);
    }
    template <typename U = T,
            std::enable_if_t<std::is_same_v<U, T>, int> = 0,
            std::enable_if_t<std::is_integral<U>::value, int> = 0>
    TValueFixedWithLog() : TValueFixed<T>() {
        // constructor for integers/bools: take small integer number
        // initialize to something small, because log(0)=-Inf
        _setValue(1);
    }

    ~TValueFixedWithLog() override = default;

    double logValue() const override{
        return _log_value;
    }

    double oldLogValue() const override {
        return logValue();
    }
};

//-------------------------------------------
template <class T>
class TValueFixedWithLogAndExponential : virtual public TValueFixedWithLog<T>, virtual public TValueFixedWithExponential<T> {
    // keeps track of exp(x) -> whenever x is changed, we change exp(x) too
protected:

    void _setValue(T val) override{
        TValueFixedWithLog<T>::_setValue(val);
        this->_exp_value = exp(this->_value);
    };

public:
    TValueFixedWithLogAndExponential() : TValueFixedWithLog<T>(), TValueFixedWithExponential<T>() {
    }
    ~TValueFixedWithLogAndExponential() override = default;

    double expValue() const override {
        return TValueFixedWithExponential<T>::expValue();
    }
    double oldExpValue() const override {
        return TValueFixedWithExponential<T>::oldExpValue();
    }
    double logValue() const override{
        return TValueFixedWithLog<T>::logValue();
    }

    double oldLogValue() const override {
        return TValueFixedWithLog<T>::oldLogValue();
    }
};

//-------------------------------------------
// TValueUpdated
//-------------------------------------------
template <class T>
class TValueUpdated : virtual public TValueFixed<T> {
    // additionally keeps track of _value_old
protected:
    T _value_old;

    // overridden in derived classes
    void _setOldValue(T val) override{
        _value_old = val;
    };
    void _set(T val) override{
        _value_old = this->_value;
        this->_setValue(val);
    };
    void _reset() override{
        this->_value = _value_old;
    };

public:
    TValueUpdated() : TValueFixed<T>() {
        _value_old = 0;
    }

    ~TValueUpdated() override = default;

    T oldValue() const override{
        return _value_old;
    }

    double oldExpValue() const override{
        // should normally not be used (if you need to keep track of the exponential, use class TValueWithExpVal)
        // only purpose: have same behaviour if you for base and child class.
        return exp(_value_old);
    }

    double oldLogValue() const override{
        // should normally not be used (if you need to keep track of the log, use class TValueWithLogVal)
        // only purpose: have same behaviour if you for base and child class)
        assert(_value_old > 0);
        return log(_value_old);
    }
};

//-------------------------------------------
template <class T>
class TValueUpdatedWithExponential : virtual public TValueUpdated<T>, virtual public TValueFixedWithExponential<T> {
    // keeps track of exp(x) -> whenever x is change, we change exp(x) too
protected:
    double _exp_value_old;

    void _setValue(T val) override{
        TValueFixedWithExponential<T>::_setValue(val);
    };

    void _setOldValue(T val) override{
        TValueUpdated<T>::_setOldValue(val);
        _exp_value_old = exp(this->_value_old);
    };

    void _set(T val) override{
        _exp_value_old = this->_exp_value;
        TValueUpdated<T>::_set(val);
    };

    void _reset() override{
        TValueUpdated<T>::_reset();
        this->_exp_value = _exp_value_old;
    };


public:
    TValueUpdatedWithExponential() : TValueUpdated<T>(), TValueFixedWithExponential<T>() {
        _exp_value_old = exp(this->_value_old);
    }
    ~TValueUpdatedWithExponential() override = default;

    double expValue() const override {
        return this->_exp_value;
    }

    double oldExpValue() const override {
        return _exp_value_old;
    }
};

//-------------------------------------------
template <class T>
class TValueUpdatedWithLog : virtual public TValueUpdated<T>, virtual public TValueFixedWithLog<T> {
    // keeps track of log(x) -> whenever x is change, we change log(x) too
protected:
    double _log_value_old;

    void _setValue(T val) override{
        TValueFixedWithLog<T>::_setValue(val);
    };

    void _setOldValue(T val) override{
        TValueUpdated<T>::_setOldValue(val);
        assert(this->_value_old > 0);
        _log_value_old = log(this->_value_old);
    };

    void _set(T val) override{
        _log_value_old = this->_log_value;
        TValueUpdated<T>::_set(val);
    };

    void _reset() override{
        TValueUpdated<T>::_reset();
        this->_log_value = _log_value_old;
    };

public:
    template <typename U = T,
            std::enable_if_t<std::is_same_v<U, T>, int> = 0,
            std::enable_if_t<std::is_floating_point<U>::value, int> = 0>
    TValueUpdatedWithLog() : TValueUpdated<T>(), TValueFixedWithLog<T>() {
        // constructor for floating points: take small floating point number
        // initialize to something small, because log(0)=-Inf
        _setOldValue(0.000001);
    }
    template <typename U = T,
            std::enable_if_t<std::is_same_v<U, T>, int> = 0,
            std::enable_if_t<std::is_integral<U>::value, int> = 0>
    TValueUpdatedWithLog() : TValueUpdated<T>(), TValueFixedWithLog<T>() {
        // constructor for integers/bools: take small integer number
        // initialize to something small, because log(0)=-Inf
        _setOldValue(1);
    }

    ~TValueUpdatedWithLog() override = default;

    double logValue() const override{
        return this->_log_value;
    }

    double oldLogValue() const override {
        return _log_value_old;
    }
};

//-------------------------------------------
template <class T>
class TValueUpdatedWithLogAndExponential : public TValueUpdatedWithLog<T>, public TValueUpdatedWithExponential<T> {
    // keeps track of log(x) and exp(x) -> whenever x is changed, we change log(x) and exp(x) too
protected:
    void _setValue(T val) override{
        TValueUpdatedWithLog<T>::_setValue(val);
        this->_exp_value = exp(this->_value);
    };

    void _setOldValue(T val) override{
        TValueUpdatedWithLog<T>::_setOldValue(val);
        this->_exp_value_old = exp(this->_value_old);
    };

    void _set(T val) override{
        this->_exp_value_old = this->_exp_value;
        TValueUpdatedWithLog<T>::_set(val);
    };

    void _reset() override{
        TValueUpdatedWithLog<T>::_reset();
        this->_exp_value = this->_exp_value_old;
    };

public:
    TValueUpdatedWithLogAndExponential() : TValueUpdatedWithLog<T>(), TValueUpdatedWithExponential<T>() {}

    ~TValueUpdatedWithLogAndExponential() override = default;

    double expValue() const override {
        return this->_exp_value;
    }

    double oldExpValue() const override {
        return this->_exp_value_old;
    }

    double logValue() const override{
        return this->_log_value;
    }

    double oldLogValue() const override {
        return this->_log_value_old;
    }
};

//-------------------------------------------
// TValueVectors
// used such that TMCMCStorageMultiDimensional can have a pointer to TValueVectorBase
// which points to a specific derived class of  TValueVectorBase (depending if updated/fixed and track log/exp/both)
//-------------------------------------------

template<typename T>
class TValueVectorBase {
public:
    TValueVectorBase()= default;
    virtual ~TValueVectorBase() = default;
    virtual TValueBase<T> & operator[](const size_t & i) = 0;
    virtual const TValueBase<T> & operator[](const size_t & i) const = 0;
    virtual void emplace_back(const T & value) = 0;
    virtual size_t size() = 0;
    virtual void resize(const size_t & Size) = 0;
    virtual void reserve(const size_t & Size) = 0;
};

template<typename T>
class TValueFixedVector : public TValueVectorBase<T> {
private:
    std::vector<TValueFixed<T>> _values;
public:
    TValueFixedVector(const size_t & Size) : TValueVectorBase<T>(){
        _values.resize(Size);
    };
    ~TValueFixedVector() override = default;
    TValueBase<T> & operator[](const size_t & i) override {
        assert(i < _values.size());
        return _values[i];
    };
    const TValueBase<T> & operator[](const size_t & i) const override{
        assert(i < _values.size());
        return _values[i];
    }
    void emplace_back(const T & value) override{
        _values.emplace_back(); // add new TValue object to vector
        _values.back().set(value);
    }
    size_t size() override{
        return _values.size();
    }
    void resize(const size_t & Size) override {
        _values.resize(Size);
    }
    void reserve(const size_t & Size) override {
        _values.reserve(Size);
    }
};

template<typename T>
class TValueFixedWithExponentialVector : public TValueVectorBase<T> {
private:
    std::vector<TValueFixedWithExponential<T>> _values;
public:
    TValueFixedWithExponentialVector(const size_t & Size) : TValueVectorBase<T>(){
        _values.resize(Size);
    };
    ~TValueFixedWithExponentialVector() override = default;
    TValueBase<T> & operator[](const size_t & i)  override{
        assert(i < _values.size());
        return _values[i];
    };
    const TValueBase<T> & operator[](const size_t & i) const override{
        assert(i < _values.size());
        return _values[i];
    }
    void emplace_back(const T & value) override{
        _values.emplace_back(); // add new TValue object to vector
        _values.back().set(value);
    }
    size_t size() override{
        return _values.size();
    }
    void resize(const size_t & Size) override {
        _values.resize(Size);
    }
    void reserve(const size_t & Size) override {
        _values.reserve(Size);
    }
};

template<typename T>
class TValueFixedWithLogVector : public TValueVectorBase<T> {
private:
    std::vector<TValueFixedWithLog<T>> _values;
public:
    TValueFixedWithLogVector(const size_t & Size) : TValueVectorBase<T>(){
        _values.resize(Size);
    };
    ~TValueFixedWithLogVector() override = default;
    TValueBase<T> & operator[](const size_t & i) override {
        assert(i < _values.size());
        return _values[i];
    };
    const TValueBase<T> & operator[](const size_t & i) const override{
        assert(i < _values.size());
        return _values[i];
    }
    void emplace_back(const T & value) override{
        _values.emplace_back(); // add new TValue object to vector
        _values.back().set(value);
    }
    size_t size() override{
        return _values.size();
    }
    void resize(const size_t & Size) override {
        _values.resize(Size);
    }
    void reserve(const size_t & Size) override {
        _values.reserve(Size);
    }
};

template<typename T>
class TValueFixedWithLogAndExponentialVector : public TValueVectorBase<T> {
private:
    std::vector<TValueFixedWithLogAndExponential<T>> _values;
public:
    TValueFixedWithLogAndExponentialVector(const size_t & Size) : TValueVectorBase<T>(){
        _values.resize(Size);
    };
    ~TValueFixedWithLogAndExponentialVector() override = default;
    TValueBase<T> & operator[](const size_t & i) override {
        assert(i < _values.size());
        return _values[i];
    };
    const TValueBase<T> & operator[](const size_t & i) const override{
        assert(i < _values.size());
        return _values[i];
    }
    void emplace_back(const T & value) override{
        _values.emplace_back(); // add new TValue object to vector
        _values.back().set(value);
    }
    size_t size() override{
        return _values.size();
    }
    void resize(const size_t & Size) override {
        _values.resize(Size);
    }
    void reserve(const size_t & Size) override {
        _values.reserve(Size);
    }
};

template<typename T>
class TValueUpdatedVector : public TValueVectorBase<T> {
private:
    std::vector<TValueUpdated<T>> _values;
public:
    TValueUpdatedVector(const size_t & Size) : TValueVectorBase<T>(){
        _values.resize(Size);
    };
    ~TValueUpdatedVector() override = default;
    TValueBase<T> & operator[](const size_t & i) override {
        assert(i < _values.size());
        return _values[i];
    };
    const TValueBase<T> & operator[](const size_t & i) const override{
        assert(i < _values.size());
        return _values[i];
    }
    void emplace_back(const T & value) override{
        _values.emplace_back(); // add new TValue object to vector
        _values.back().set(value);
    }
    size_t size() override{
        return _values.size();
    }
    void resize(const size_t & Size) override {
        _values.resize(Size);
    }
    void reserve(const size_t & Size) override {
        _values.reserve(Size);
    }
};

template<typename T>
class TValueUpdatedWithExponentialVector : public TValueVectorBase<T> {
private:
    std::vector<TValueUpdatedWithExponential<T>> _values;
public:
    TValueUpdatedWithExponentialVector(const size_t & Size) : TValueVectorBase<T>(){
        _values.resize(Size);
    };
    ~TValueUpdatedWithExponentialVector() override = default;
    TValueBase<T> & operator[](const size_t & i) override {
        assert(i < _values.size());
        return _values[i];
    };
    const TValueBase<T> & operator[](const size_t & i) const override{
        assert(i < _values.size());
        return _values[i];
    }
    void emplace_back(const T & value) override{
        _values.emplace_back(); // add new TValue object to vector
        _values.back().set(value);
    }
    size_t size() override{
        return _values.size();
    }
    void resize(const size_t & Size) override {
        _values.resize(Size);
    }
    void reserve(const size_t & Size) override {
        _values.reserve(Size);
    }
};

template<typename T>
class TValueUpdatedWithLogVector : public TValueVectorBase<T> {
private:
    std::vector<TValueUpdatedWithLog<T>> _values;
public:
    TValueUpdatedWithLogVector(const size_t & Size) : TValueVectorBase<T>(){
        _values.resize(Size);
    };
    ~TValueUpdatedWithLogVector() override = default;
    TValueBase<T> & operator[](const size_t & i) override {
        assert(i < _values.size());
        return _values[i];
    };
    const TValueBase<T> & operator[](const size_t & i) const override{
        assert(i < _values.size());
        return _values[i];
    }
    void emplace_back(const T & value) override{
        _values.emplace_back(); // add new TValue object to vector
        _values.back().set(value);
    }
    size_t size() override{
        return _values.size();
    }
    void resize(const size_t & Size) override {
        _values.resize(Size);
    }
    void reserve(const size_t & Size) override {
        _values.reserve(Size);
    }
};

template<typename T>
class TValueUpdatedWithLogAndExponentialVector : public TValueVectorBase<T> {
private:
    std::vector<TValueUpdatedWithLogAndExponential<T>> _values;
public:
    TValueUpdatedWithLogAndExponentialVector(const size_t & Size) : TValueVectorBase<T>(){
        _values.resize(Size);
    };
    ~TValueUpdatedWithLogAndExponentialVector() override = default;
    TValueBase<T> & operator[](const size_t & i) override {
        assert(i < _values.size());
        return _values[i];
    };
    const TValueBase<T> & operator[](const size_t & i) const override{
        assert(i < _values.size());
        return _values[i];
    }
    void emplace_back(const T & value) override{
        _values.emplace_back(); // add new TValue object to vector
        _values.back().set(value);
    }
    size_t size() override{
        return _values.size();
    }
    void resize(const size_t & Size) override {
        _values.resize(Size);
    }
    void reserve(const size_t & Size) override {
        _values.reserve(Size);
    }
};

#endif //EMPTY_TVALUE_H
