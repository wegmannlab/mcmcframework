//
// Created by madleina on 17.02.21.
//

#ifndef EMPTY_TNELDERMEAD_H
#define EMPTY_TNELDERMEAD_H

#include <vector>
#include <cstdint>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include "stringFunctions.h"
#include "TLog.h"

class TVertex {
protected:
    std::vector<double> _vertex;
    double _value;

public:
    TVertex(){
        _value = 0.;
    }

    TVertex(const size_t & NumDim){
        resize(NumDim);
    }

    void resize(const size_t & NumDim){
        _vertex.resize(NumDim);
        _value = 0.;
    }

    double & operator[](const size_t & Dim){
        assert(Dim < _vertex.size());
        return _vertex[Dim];
    }

    const double & operator[](const size_t & Dim) const{
        assert(Dim < _vertex.size());
        return _vertex[Dim];
    }

    double & value(){
        return _value;
    }

    const double & value() const {
        return _value;
    }

    const std::vector<double> & coordinates() const {
        return _vertex;
    }

    size_t numDim() const{
        return _vertex.size();
    }

    // debugging
    void print() const {
        for (auto & coord : _vertex){
            std::cout << coord << ", ";
        }
        std::cout << std::endl;
    }

    void clear() {
        _vertex.clear();
        _value = 0.;
    }
};

class TSimplex {
protected:
    size_t _numVertices; // number of points (vertices) of simplex (= N+1)
    size_t _numDim; // number of dimensions (N) -> number of unknown variables of function

    // simplex coordinates
    std::vector<TVertex> _simplex;

public:
    TSimplex(){
        _numDim = 0;
        _numVertices = 0;
    }

    TSimplex(const size_t & NumDim){
        _numDim = NumDim;
        _numVertices = _numDim + 1;
        _simplex.resize(_numVertices, TVertex(_numDim));
    };

    // access simplex coordinates
    TVertex & operator[](const size_t & Vertex){
        assert(Vertex < _numVertices);
        return _simplex[Vertex];
    }

    const TVertex & operator[](const size_t & Vertex) const{
        assert(Vertex < _numVertices);
        return _simplex[Vertex];
    }

    // access simplex function values
    double & value(const size_t & Vertex){
        assert(Vertex < _numVertices);
        return _simplex[Vertex].value();
    }

    const double & value(const size_t & Vertex) const {
        assert(Vertex < _numVertices);
        return _simplex[Vertex].value();
    }

    // get simplex dimensions
    size_t numVertices() const{
        return _numVertices;
    }

    size_t numDim() const{
        return _numDim;
    }

    // debugging
    void print() const {
        std::cout << "Simplex:" << std::endl;
        for (auto & vertex : _simplex){
            vertex.print();
        }
    }

    void printVals() const {
        std::cout << "Function values at vertices:" << std::endl;
        for (auto & vertex : _simplex){
            std::cout << vertex.value() << ", ";
        }
        std::cout << std::endl;
    }

    void clear(){
        for (auto & vertex : _simplex){
            vertex.clear();
        }
        _numVertices = 0;
        _numDim = 0;
    }
};

class TNelderMead {
    // Multidimensional minimization by the downhill simplex method of Nelder and Mead.
    // code adapted from Numerical Receipes 3rd edition, pp. 504 (Section 10.5 Downhill Simplex Method in Multidimensions)

protected:
    // temporary values
    TSimplex _simplex;

    // termination criteria
    double _fractionalConvergenceTolerance;
    size_t _counterFuncEvaluations; // the number of function evaluations
    size_t _maxNumFuncEvaluations; // maximal number of function evaluations

    // dynamically adjust tolerance to terminate?
    bool _dynamicallyAdjustTolerance;
    double _valueToCompareToleranceTo; // dynamically adjust tolerance
    double _factorDynamicTolerance;
    size_t _numFunctionCallsUntilAdjustingTolerance;

    // report to logfile
    bool _report;
    TLog * _logfile;

    // utility functions
    inline void _sumSimplex_perDim(std::vector<double> & SumSimplex_PerDim) const{
        // returns vector of size N, where each element_i represents the sum of values over all vertices at dimension i
        // used to calculate centroid (in a later step)
        // vec[i] = sum_{v=1}^{N+1} x_{vi}
        for (size_t i = 0; i < _simplex.numDim(); i++) {
            double sum = 0.;
            for (size_t vertex = 0; vertex < _simplex.numVertices(); vertex++) {
                sum += _simplex[vertex][i];
            }
            SumSimplex_PerDim[i] = sum;
        }
    }

    inline void _swap(double & Value1, double & Value2){
        // swap values
        double tmp = Value1;
        Value1 = Value2;
        Value2 = tmp;
    }

    inline void _assignIndices(size_t & IndexLowestVertex, size_t & IndexNextHighestVertex, size_t & IndexHighestVertex){
        // determine which point is the highest (worst), next-highest (second-worst), and lowest (best), by looping over the points in the simplex
        IndexLowestVertex = 0;
        if (_simplex[0].value() > _simplex[1].value()){
            IndexHighestVertex = 0;
            IndexNextHighestVertex = 1;
        } else {
            IndexHighestVertex = 1;
            IndexNextHighestVertex = 0;
        }
        for (size_t vertex = 0; vertex < _simplex.numVertices(); vertex++) {
            if (_simplex[vertex].value() <= _simplex[IndexLowestVertex].value()){
                IndexLowestVertex = vertex;
            }
            if (_simplex[vertex].value() > _simplex[IndexHighestVertex].value()) {
                IndexNextHighestVertex = IndexHighestVertex;
                IndexHighestVertex = vertex;
            } else if (_simplex[vertex].value() > _simplex[IndexNextHighestVertex].value() && vertex != IndexHighestVertex){
                IndexNextHighestVertex = vertex;
            }
        }
    }

    inline double _calculateFractionalRange(const double & Val1, const double & Val2) const{
        return 2.0 * std::fabs(Val1 - Val2) / (std::fabs(Val1) + std::fabs(Val2) + 10e-10); // add 10e-10 to avoid division by zero
    }

    void _adjustTolerance(const size_t & IndexLowestVertex){
        // compare current best value with _valueToCompareToleranceTo (this is e.g. the value of the best vertex of a previous Nelder-Mead run)
        double fractionalRange = _calculateFractionalRange(_valueToCompareToleranceTo, _simplex[IndexLowestVertex].value());
        // we want to be e.g. 1000x more precise than this difference -> multiply with e.g. 0.001
        _fractionalConvergenceTolerance = fractionalRange * _factorDynamicTolerance;

        // only adjust once, i.e. set bool to false
        _dynamicallyAdjustTolerance = false;
    }

    inline bool _terminate(const size_t & IndexLowestVertex, const size_t & IndexHighestVertex){
        // check if we should adjust tolerance
        if (_dynamicallyAdjustTolerance && _counterFuncEvaluations >= _numFunctionCallsUntilAdjustingTolerance){
            _adjustTolerance(IndexLowestVertex);
        }
        // calculate fractional range, i.e. difference between best and worst function value
        double fractionalRange = _calculateFractionalRange(_simplex[IndexHighestVertex].value(), _simplex[IndexLowestVertex].value());
        if (fractionalRange < _fractionalConvergenceTolerance) { // terminate!
            if (_report) {
                _logfile->list("In Nelder-Mead algorithm: Found minimum within " + toString(_counterFuncEvaluations) + " function evaluations.");
            }
            return true;
        } else { // don't terminate yet
            // check if we reached maximal number of function evaluations
            if (_counterFuncEvaluations >= _maxNumFuncEvaluations){
                if (_report) {
                    _logfile->warning("In Nelder-Mead algorithm: Failed to find minimum. Reached maximum number of function evaluations (" + toString(_maxNumFuncEvaluations) + ")! Fractional range at end: " + toString(fractionalRange) + ".");
                }
                return true;
            }
            return false;
        }
    }

    inline TVertex _getBestVertex(const size_t & IndexLowestVertex){
        // Put best point and value in slot 0.
        _swap(_simplex[0].value(), _simplex[IndexLowestVertex].value());
        for (size_t i = 0; i < _simplex.numDim(); i++) {
            _swap(_simplex[0][i],_simplex[IndexLowestVertex][i]);
        }
        return _simplex[0];
    }

    template<class Object, class Function>
    double _reflect(std::vector<double> & SumSimplex_PerDim, const size_t & IndexHighestVertex, Object & Obj, Function & Func){
        // First extrapolate by a factor -1 through the face of the simplex across from the high point, i.e. reflect the simplex from the high point.
        return _amotry(SumSimplex_PerDim, IndexHighestVertex, -1., Obj, Func);
    }

    template<class Object, class Function>
    double _expand(std::vector<double> & SumSimplex_PerDim, const size_t & IndexHighestVertex, Object & Obj, Function & Func){
        // Try an additional extrapolation by a factor 2
        return _amotry(SumSimplex_PerDim, IndexHighestVertex, 2.0, Obj, Func);
    }

    template<class Object, class Function>
    double _contract(std::vector<double> & SumSimplex_PerDim, const size_t & IndexHighestVertex, Object & Obj, Function & Func){
        // do a one-dimensional contraction
        return _amotry(SumSimplex_PerDim, IndexHighestVertex, 0.5, Obj, Func);
    }

    template<class Object, class Function>
    void _shrink(std::vector<double> & SumSimplex_PerDim, const size_t & IndexLowestVertex, Object & Obj, Function & Func){
        // contract around the lowest (best) point
        for (size_t vertex = 0; vertex < _simplex.numVertices(); vertex++) {
            if (vertex != IndexLowestVertex) { // exclude best vertex
                for (size_t i = 0; i < _simplex.numDim(); i++){
                    _simplex[vertex][i] = SumSimplex_PerDim[i] = 0.5 * (_simplex[vertex][i] + _simplex[IndexLowestVertex][i]);
                }
                _simplex[vertex].value() = (Obj.*Func)(SumSimplex_PerDim);
            }
        }
    }

    template<class Object, class Function>
    double _amotry(std::vector<double> & SumSimplex_PerDim, const size_t & IndexHighestVertex, const double & Factor, Object & Obj, Function & Func){
        // Helper function: Extrapolates by a factor Factor through the face of the simplex across from the high point,
        // tries it, and replaces the high point if the new point is better.
        std::vector<double> newPoint(_simplex.numDim());

        // the following calculating is equivalent to
        // x_r = x_o + alpha(x_o - x_{N+1}) from the script if alpha = -Factor
        double fac1 = (1.-Factor) / _simplex.numDim();
        double fac2 = fac1 - Factor;
        for (size_t i = 0; i < _simplex.numDim(); i++) {
            newPoint[i] = SumSimplex_PerDim[i] * fac1 - _simplex[IndexHighestVertex][i] * fac2;
        }
        double valueAtNewPoint = (Obj.*Func)(newPoint); // evaluate the function at the trial point
        // avoid issues if value is invalid (nan or infinite): make it huuuuge, should always be worse
        if (std::isnan(valueAtNewPoint) || !std::isfinite(valueAtNewPoint)){
            valueAtNewPoint = std::numeric_limits<double>::max();
        }
        if (valueAtNewPoint < _simplex[IndexHighestVertex].value()) { // if it’s better than the highest (worst point), then replace the highest
            _simplex[IndexHighestVertex].value() = valueAtNewPoint;
            for (size_t i = 0; i < _simplex.numDim(); i++) {
                SumSimplex_PerDim[i] += newPoint[i] - _simplex[IndexHighestVertex][i]; // update sum (add new and subtract old)
                _simplex[IndexHighestVertex][i] = newPoint[i];
            }
        }
        return valueAtNewPoint;
    }

public:
    // default constructor
    TNelderMead(){
        // stop algorithm if decrease in the function value in the terminating step is fractionally smaller than some tolerance _fractionalConvergenceTolerance
        _fractionalConvergenceTolerance = 10e-15;
        _maxNumFuncEvaluations = 20000;

        _counterFuncEvaluations = 0;
        _dynamicallyAdjustTolerance = false;
        _factorDynamicTolerance = 0.;
        _valueToCompareToleranceTo = 0.;
        _numFunctionCallsUntilAdjustingTolerance = 0;

        _report = false;
        _logfile = nullptr;
    }

    explicit TNelderMead(TLog * Logfile) : TNelderMead() {
        report(Logfile);
    }

    // set-functions

    void setFractionalConvergenceTolerance(const double & FractionalConvergenceTolerance){
        _fractionalConvergenceTolerance = FractionalConvergenceTolerance;
    }

    void setMaxNumFunctionEvaluations(const size_t & MaxNumFunctionEvaluations){
        _maxNumFuncEvaluations = MaxNumFunctionEvaluations;
    }

    void dynamicallyAdjustTolerance(const double & ValueToCompareTo, const double & FactorPrecision, const size_t & NumFunctionCallsUntilAdjustment){
        // functions that enables Nelder-Mead to dynamically adjust the tolerance for termination
        // idea: e.g. Nelder-Mead is used within another algorithm, e.g. EM -> we want to dynamically scale tolerance
        // depending on how "far" we are in the EM, i.e. don't be precise inside NM in the beginning and be precise in the end
        // as next iteration of EM will anyways change values

        // after NumFunctionCallsUntilAdjustment function calls, Nelder-Mead will compare the current best value with ValueToCompareTo
        // (this is typically the function value of the best vertex of the last NM-Run)
        // if those are very close -> i.e. we are already close to where we were in a previous run of Nelder-Mead,
        // we want to have a low tolerance, i.e. we want to be very precise
        // if we are far away -> i.e. we already made far jumps compared to where we were before,
        // we don't want to be too precise, and have a relatively high tolerance
        // -> tolerance scales with distance of current Nelder-Mead to previous Nelder-Mead
        _dynamicallyAdjustTolerance = true;
        _valueToCompareToleranceTo = ValueToCompareTo;
        _factorDynamicTolerance = FactorPrecision;
        _numFunctionCallsUntilAdjustingTolerance = NumFunctionCallsUntilAdjustment;
    }

    void report(TLog * Logfile){
        _logfile = Logfile;
        _report = true;
    }

    // get-functions

    const TSimplex & getCurrentSimplex() const{
        return _simplex;
    }

    size_t getCounterFunctionEvaluations() const {
        return _counterFuncEvaluations;
    }

    template<class Object, class Function>
    TVertex minimize(const TVertex & InitialVertex, const double & Del, Object & Obj, Function & Func){
        // Parameter InitialVertex represents one vertex[0..ndim-1] of an initial simplex
        // Parameter Del represents a constant displacement along each coordinate direction
        // Parameter Func is the function to be minimized
        // Returns the location of the minimum.
        std::vector<double> dels(InitialVertex.numDim(), Del);
        return minimize(InitialVertex, dels, Obj, Func);
    }

    template<class Object, class Function>
    TVertex minimize(const TVertex &InitialVertex, const std::vector<double> & Dels, Object & Obj, Function & Func){
        // Alternative interface
        // Parameter InitialVertex represents one vertex of size N of an initial simplex
        // Parameter Dels represents different displacements dels of size N in different directions for the initial simplex
        // Parameter Func is the function to be minimized
        // Returns the location of the minimum.
        size_t numDim = InitialVertex.numDim();
        size_t numVertices = numDim+1;

        // create initial simplex: consists of N+1 vertices, each of length N
        TSimplex initialSimplex(numDim);
        for (size_t vertex = 0; vertex < numVertices; vertex++) {
            for (size_t i = 0; i < numDim; i++) {
                initialSimplex[vertex][i] = InitialVertex[i]; // start by setting each vertex to the same values as initialVertex
            }
            if (vertex != 0 ){ // for all vertices except the first one (= initialVertex):
                initialSimplex[vertex][vertex-1] += Dels[vertex-1]; // displace one value of the vertex
            }
        }
        return minimize(initialSimplex, Obj, Func);
    }

    template<class Object, class Function>
    TVertex minimize(const TSimplex & InitialSimplex, Object & Obj, Function & Func){
        // Most general interface
        // Parameter InitialSimplex is a 2D-vector, consisting of N+1 vertices, each of length N
        // Parameter Func is the function to be minimized
        // Returns the location of the minimum.

        // store values
        _simplex = InitialSimplex;

        // calculate function values at all vertices
        for (int vertex = 0; vertex < _simplex.numVertices(); vertex++) {
            _simplex[vertex].value() = (Obj.*Func)(_simplex[vertex].coordinates());
        }

        // initialize temporary values
        size_t indexHighestVertex; // index of highest (worst) vertex
        size_t indexLowestVertex; // index of lowest (best) vertex
        size_t indexNextHighestVertex; // index of next-highest (second-worst) vertex
        std::vector<double> sumSimplex_perDim(_simplex.numDim()); // sum of all vertices values per dimension

        // start algorithm
        _counterFuncEvaluations = 0;
        _sumSimplex_perDim(sumSimplex_perDim);
        for (;;) { // eternal loop until we break
            // first we must determine which point is the highest (worst), next-highest (second-worst), and lowest (best)
            _assignIndices(indexLowestVertex, indexNextHighestVertex, indexHighestVertex);

            // Check if we should terminate: Compute the fractional range from highest to lowest and return if satisfactory.
            if (_terminate(indexLowestVertex, indexHighestVertex)){
                return _getBestVertex(indexLowestVertex);
            }

            // precautionary add two function evaluations for function calls to _reflect() and _expand() (not executed yet)
            _counterFuncEvaluations += 2;

            // first reflect through the face of the simplex across from the high point, i.e. reflect the simplex from the high point
            double valueAtNewPoint = _reflect(sumSimplex_perDim, indexHighestVertex, Obj, Func);

            if (valueAtNewPoint <= _simplex[indexLowestVertex].value()) {
                // gives a result better than the best point, so try an additional extrapolation
                _expand(sumSimplex_perDim, indexHighestVertex, Obj, Func);
            } else if (valueAtNewPoint >= _simplex[indexNextHighestVertex].value()) {
                // the reflected point is worse than the second-highest, so look for an intermediate lower point, i.e., do a one-dimensional contraction
                double tmp_valueAtWorstPoint = _simplex[indexHighestVertex].value();
                valueAtNewPoint = _contract(sumSimplex_perDim, indexHighestVertex, Obj, Func);
                if (valueAtNewPoint >= tmp_valueAtWorstPoint) {
                    // still worse than worst point. Can’t seem to get rid of that high point.
                    // better contract around the lowest (best) point
                    _shrink(sumSimplex_perDim, indexLowestVertex, Obj, Func);

                    _sumSimplex_perDim(sumSimplex_perDim); // recompute sumVerticesPerDimension
                    _counterFuncEvaluations += _simplex.numDim(); // keep track of function evaluations: we had to do N extra function calls for shrinking
                }
            } else {
                --_counterFuncEvaluations; // Correct the evaluation count (we didn't expand)
            }
        } // Go back for the test of doneness and the next iteration
    }
};

// How to use:
// (e.g. by giving full initial simplex):
// std::vector<double> foundMinimum = nelderMead.minimize(initialSimplex, objectOfMyClass,  &myClass::myFunction);


#endif //EMPTY_TNELDERMEAD_H
