//
// Created by madleina on 11.01.21.
//

#include "TMCMCFiles.h"

//--------------------------------------------
// TMCMCFile
//--------------------------------------------
TMCMCFile::TMCMCFile(const std::string & Filename){
    // idea: give vector of TMCMCParameters whose values should be written into one file (e.g. one tracefile for all hyperparameters)
    _initialize(Filename);
}

TMCMCFile::~TMCMCFile(){
    _file.close();
}

void TMCMCFile::_initialize(const std::string & Filename){
    _name = Filename;
    _file.open(_name);
}

void TMCMCFile::addParam(const std::shared_ptr<TMCMCParameterBase>& paramInFile) {
    _paramsInFile.push_back(paramInFile);
}

void TMCMCFile::addObservation(const std::shared_ptr<TObservationsBase>& paramInFile) {
    _observationsInFile.push_back(paramInFile);
}

void TMCMCFile::writeHeader(){
    // only do this after all parameters were added!
    std::vector<std::string> header;
    for (auto & param : _paramsInFile) {
        param->fillHeader(header);
    }
    for (auto & observation : _observationsInFile) {
        observation->fillHeader(header);
    }

    _file.writeHeader(header);
}

void TMCMCFile::write(){}

void TMCMCFile::close() {
    _file.close();
}

//--------------------------------------------
TMCMCTraceFile::TMCMCTraceFile(const std::string & Filename) : TMCMCFile (Filename){}
TMCMCTraceFile::~TMCMCTraceFile() = default;

void TMCMCTraceFile::write() {
    // only write trace files for parameters (not for observations, these don't change!)
    for (auto & param : _paramsInFile)
        param->writeVals(_file);
    _file.endLine();
}

//--------------------------------------------
TMCMCMeanVarFile::TMCMCMeanVarFile(const std::string & Filename) : TMCMCFile (Filename){}
TMCMCMeanVarFile::~TMCMCMeanVarFile() = default;

void TMCMCMeanVarFile::write() {
    // only write meanVar files for parameters (not for observations, these don't change!)
    for (auto & param : _paramsInFile)
        param->writeMean(_file);
    _file.endLine();

    for (auto & param : _paramsInFile)
        param->writeVar(_file);
    _file.endLine();
}

//--------------------------------------------
TMCMCSimulationFile::TMCMCSimulationFile(const std::string & Filename) : TMCMCFile (Filename){}
TMCMCSimulationFile::~TMCMCSimulationFile() = default;

void TMCMCSimulationFile::write() {
    // write simulation files for both parameters and observations!
    for (auto & param : _paramsInFile) {
        param->writeVals(_file);
    }
    for (auto & obs : _observationsInFile) {
        obs->writeVals(_file);
    }
    _file.endLine();
}

//--------------------------------------------
TMCMCStateFile::TMCMCStateFile(const std::string & Filename) : TMCMCFile (Filename){}
TMCMCStateFile::~TMCMCStateFile() = default;

void TMCMCStateFile::write(int iteration) {
    // only write state files for parameters (not for observations, these don't change)!

    // idea: overwrite state file each time, write iteration in header
    if (!_file.isOpen()) // will be opened the first time we write, but every time after we will have to re-open it in order to overwrite
        _file.open(_name);
    _writeHeader(iteration);

    for (auto & param : _paramsInFile){
        _file << param->name();
        param->writeValsOneString(_file); // write values as comma-seperated string if array
        param->writeJumpSizeOneString(_file); // write jumpSizes as comma-seperated string if array
        _file.endLine();
    }
    _file.close();
}

void TMCMCStateFile::_writeHeader(int iteration) {
    _file << iteration << "value" << "jumpSize" << std::endl;
}

//--------------------------------------------
// TMCMCFile_Reader
//--------------------------------------------
TMCMCFile_Reader::TMCMCFile_Reader(const std::string & Filename){
    _initialize(Filename);
}

TMCMCFile_Reader::~TMCMCFile_Reader(){
    _file.close();
}

void TMCMCFile_Reader::_initialize(const std::string & Filename){
    _name = Filename;
    _file.open(_name, TFile_Filetype::header);
}

std::pair<size_t, size_t> TMCMCFile_Reader::_findFirstAndLastCol(const std::string & paramName, const size_t & expectedSize) const {
    // find first column
    std::pair<size_t, size_t> firstAndLastCol;
    firstAndLastCol.first = _file.getIndexOfColnameStartsWith_First(paramName);
    firstAndLastCol.second = _file.getIndexOfColnameStartsWith_Last(paramName)+1; // plus one such that last is not included
    if (firstAndLastCol.second - firstAndLastCol.first != expectedSize) {
        throw "Error while reading file '" + _file.name() + "' for initialization of parameter " + paramName +
              ": Expected " + toString(expectedSize) +
              " columns whose column names match the parameter name, found " + toString(firstAndLastCol.second - firstAndLastCol.first) + "!";
    }
    return firstAndLastCol;
}

void TMCMCFile_Reader::close() {
    _file.close();
}

//--------------------------------------------
TMCMCTraceFile_Reader::TMCMCTraceFile_Reader(const std::string & Filename) : TMCMCFile_Reader(Filename){}
TMCMCTraceFile_Reader::~TMCMCTraceFile_Reader() = default;

void TMCMCTraceFile_Reader::_readRelevantLine(std::vector<std::string> & vec){
    // parse file until we get to the last row (= last value of chain)
    while (_file.read(vec)){} // do nothing, just wait until end
}

//--------------------------------------------
TMCMCMeanVarFile_Reader::TMCMCMeanVarFile_Reader(const std::string & Filename) : TMCMCFile_Reader(Filename){}
TMCMCMeanVarFile_Reader::~TMCMCMeanVarFile_Reader() = default;

void TMCMCMeanVarFile_Reader::_readRelevantLine(std::vector<std::string> & vec){
    // parse first row of file (= mean)
    _file.read(vec);
}

//--------------------------------------------
TMCMCSimulationFile_Reader::TMCMCSimulationFile_Reader(const std::string & Filename) : TMCMCFile_Reader(Filename){}
TMCMCSimulationFile_Reader::~TMCMCSimulationFile_Reader() = default;

void TMCMCSimulationFile_Reader::_readRelevantLine(std::vector<std::string> & vec){
    // only one line -> parse first row
    _file.read(vec);
}

//--------------------------------------------
TMCMCStateFile_Reader::TMCMCStateFile_Reader(const std::string & Filename) : TMCMCFile_Reader(Filename){}
TMCMCStateFile_Reader::~TMCMCStateFile_Reader() = default;

void TMCMCStateFile_Reader::_readRelevantLine(std::vector<std::string> & vec){
    // throw error
    // not implemented because this gets a bit more complicated than for trace- and meanVar files:
    // values are here comma-separated string -> can't use function of base class, would have to re-implement it a bit differently
    throw std::runtime_error("Function 'void TMCMCStateFile::_readRelevantLine(TInputFile & input, std::vector<std::string> & vec)' is not implemented for class TMCMCStateFile!");
}
