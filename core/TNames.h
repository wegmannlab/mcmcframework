//
// Created by madleina on 12.04.21.
//

#ifndef BANGOLIN_TNAMES_H
#define BANGOLIN_TNAMES_H

#include "TDistances.h"
#include <memory>

//--------------------------
// TNamesEmpty
//--------------------------

class TNamesEmpty {
    // base class that returns empty strings
    // provides interface for all deriving name classes
protected:
    size_t _size;
    size_t _complexity; // how many rows/columns must be parsed to fill one name?
    std::vector<std::string> _title; // if name class is used as rowNames: what kind of colname must be written? Same size as complexity.
    char _delimNames; // what kind of delimiter must be used to connect a name with complexity > 1?
    size_t _indexVisited; // which index did we visit last?
    bool _storesNonDefaultNames;

    void _checkSizeNameVec(const std::vector<std::string> & Name, const std::string & ClassNameForError) const;

    std::string _extractFromStringAndReturnString(std::string &String, const char & DelimiterLast, const bool & ThrowIfEmpty) const;
    std::vector<std::string> _extractFromStringAndReturnVec(std::string &String, const char & DelimiterLast, const bool & ThrowIfEmpty) const;
    bool _extractFromStreamAndFillVec(std::vector<std::string> & Vec, std::istream *FilePointer, const char &DelimiterLast, const std::string & DelimiterComment, const bool & ThrowIfEmpty) const;

public:
    TNamesEmpty();
    TNamesEmpty(const size_t & Size);

    // resizing
    virtual void resize(const size_t & Size);

    // add names
    virtual void addName(const std::vector<std::string> & Name);
    virtual void addName(const std::vector<std::string> & Name, const size_t & Index);
    void addNameAndSplit(const std::string & Name);
    void addNameAndSplit(const std::string & Name, const size_t & Index);
    bool checkIfNameShouldBeKept(const std::vector<std::string> & Name, const std::string & FileName);
    std::string extractNameFromStringAndReturn(std::string &String, const char & DelimiterLast);
    void extractNameFromStringAndStore(std::string &String, const char & DelimiterLast);
    bool extractNameFromStreamAndFillIntoVec(std::istream *FilePointer, const char &DelimiterLast, const std::string & DelimiterComment, std::vector<std::string> & Vec);

    // set title
    void setTitle(const std::vector<std::string> & Names);
    void setDelimName(const char & Delim);
    std::vector<std::string> extractTitleFromString(std::string &String, const char & DelimiterLast);

    // get names
    virtual std::string operator[](const size_t & Index) const;
    virtual std::vector<std::string> getName(const size_t & Index) const;

    // filled?
    bool storesNonDefaultNames();
    virtual bool exists(const std::string & Name);
    virtual bool exists(const std::vector<std::string> & Name);
    virtual bool isFilled() const;
    virtual size_t getIndex(const std::string & Name);
    virtual void finalizeFilling();

    // get title
    const std::vector<std::string> & getTitleVec() const;
    std::string getTitle() const;
    const char & getDelimNames() const;

    // get size/complexity
    virtual size_t size() const;
    const size_t & complexity() const;
};

//--------------------------
// TNamesStrings
//--------------------------

class TNamesStrings : public TNamesEmpty {
    // name class that stores a vector of strings
    // and provides functions to fill and get those
protected:
    std::vector<std::string> _names;

public:
    TNamesStrings();
    TNamesStrings(const size_t & Size);
    TNamesStrings(const std::vector<std::string> & Names);

    // resizing
    void resize(const size_t & Size) override;

    // add names
    void addName(const std::vector<std::string> & Name) override;
    void addName(const std::vector<std::string> & Name, const size_t & Index) override;

    // getters
    std::string operator[](const size_t & Index) const override;
    bool exists(const std::string & Name) override;
    bool exists(const std::vector<std::string> & Name) override;
    size_t getIndex(const std::string & Name) override;
    size_t size() const override;
};

//--------------------------
// TNamesIndices
//--------------------------

class TNamesIndices : public TNamesEmpty {
    // name class that stores nothing except an offset
    // and simply returns the offset + index as a string
protected:
    size_t _offset;
public:
    TNamesIndices();

    TNamesIndices(const size_t & Size);

    void setOffset(const size_t & Offset);

    // getters
    std::string operator[](const size_t & Index) const override;
    bool exists(const std::string & Name) override;
    bool exists(const std::vector<std::string> & Name) override;
    size_t getIndex(const std::string & Name) override;
};

//-------------------------------
// TNamesIndicesAlphabetUpperCase
//-------------------------------

class TNamesIndicesAlphabetUpperCase : public TNamesEmpty {
    // name class that returns the index as a upper case alphabet index (Excel column naming style)
protected:
public:
    TNamesIndicesAlphabetUpperCase();
    TNamesIndicesAlphabetUpperCase(const size_t & Size);

    // getters
    std::string operator[](const size_t & Index) const override;
    bool exists(const std::string & Name) override;
    bool exists(const std::vector<std::string> & Name) override;
    size_t getIndex(const std::string & Name) override;
};

//-------------------------------
// TNamesIndicesAlphabetLowerCase
//-------------------------------

class TNamesIndicesAlphabetLowerCase : public TNamesEmpty {
    // name class that returns the index as a lower case alphabet index (Excel column naming style)
protected:
public:
    TNamesIndicesAlphabetLowerCase();
    TNamesIndicesAlphabetLowerCase(const size_t & Size);

    // getters
    std::string operator[](const size_t & Index) const override;
    bool exists(const std::string & Name) override;
    bool exists(const std::vector<std::string> & Name) override;
    size_t getIndex(const std::string & Name) override;
};

//--------------------------
// TNamesPositions
//--------------------------

class TNamesPositions : public TNamesEmpty {
    // name class that stores a pointer to positions
    // and provides functions to add and get those
protected:
    std::shared_ptr<TPositionsRaw> _positions;
    bool _orderIsJunkPos;

    void _splitName(std::string Name, std::string &Junk, size_t & Pos);

public:
    TNamesPositions();
    TNamesPositions(const std::shared_ptr<TPositionsRaw> & Positions);

    // set order
    void setOrderChrPos(const bool & OrderIsChrPos);

    // add names
    void addPositions(const std::shared_ptr<TPositionsRaw> & Positions);
    void addName(const std::vector<std::string> & Name) override;
    void addName(const std::vector<std::string> & Name, const size_t & Index) override;
    void finalizeFilling() override;

    // getters
    std::string operator[](const size_t & Index) const override;
    std::vector<std::string> getName(const size_t & Index) const override;
    bool exists(const std::string & Name) override;
    bool exists(const std::vector<std::string> & Name) override;
    size_t getIndex(const std::string & Name) override;
    size_t size() const override;
};

#endif //BANGOLIN_TNAMES_H
