//
// Created by Madleina Caduff on 03.04.20.
//

#ifndef MCMCFRAMEWORK_TMCMC_H
#define MCMCFRAMEWORK_TMCMC_H

#include "TDAGBuilder.h"

//-------------------------------------------
// TMCMCFramework
//-------------------------------------------
class TMCMCFramework {
    // pure virtual base class for TMCMC
    // manages parameter handling
protected:
    TLog * _logfile;
    TRandomGenerator * _randomGenerator;

    // DAG (contains all parameters and observations)
    std::shared_ptr<TDAGBuilder> _DAGBuilder;
    void _setDAGBuilder(const std::shared_ptr<TDAGBuilder> & DAGBuilder);

    // common attributes of all derived classes
    virtual void _prepareFiles(const std::string & Prefix) = 0;
    virtual void _closeFiles() = 0;

public:
    TMCMCFramework(TLog* Logfile, TRandomGenerator* RandomGenerator, TParameters & Parameters);
    ~TMCMCFramework() = default;
};

//-------------------------------------------
// TMCMC
//-------------------------------------------
class TMCMC : public TMCMCFramework {
protected:
    int _iterations;
    int _thinning;
    int _burnin;
    int _numBurnin;
    bool _writeBurnin;
    bool _writeStateFile;
    int _thinningStateFile;

    // files
    void _prepareFiles(const std::string & Prefix) override;
    void _closeFiles() override;

    void _init(TParameters & params);
    void _runPriorParameterInitialization();
    void _runBurnin();
    void _runMCMC();
    void _runMCMCChain();
    void _runMCMCIterationFull();
    void _reportAcceptanceRates();
    void _adjustProposalRanges();
    void _writeToTraceFiles();
    void _writeToMeanVarFiles();
    void _writeToStateFile(int iterations);

public:
    TMCMC(TLog* Logfile, TRandomGenerator* RandomGenerator, TParameters & Parameters);
    virtual ~TMCMC();

    // run MCMC
    void runMCMC(const std::string & Prefix, const std::shared_ptr<TDAGBuilder> & DAGBuilder);
};


//-------------------------------------------
// TSimulator
//-------------------------------------------
class TSimulator : public TMCMCFramework {
protected:
    void _prepareFiles(const std::string & Prefix) override;
    void _closeFiles() override;

public:
    TSimulator(TLog* Logfile, TRandomGenerator* RandomGenerator, TParameters & Parameters);
    virtual ~TSimulator();

    // simulate under prior distributions
    void simulate(const std::string & Prefix, const std::shared_ptr<TDAGBuilder> & DAGBuilder, TParameters & Parameters);
    void writeFiles(const std::string & Prefix);
};

#endif //MCMCFRAMEWORK_TMCMC_H
