//
// Created by caduffm on 7/17/20.
//

#ifndef MCMCFRAMEWORK_TPRIORWITHHYPERPRIOR_H
#define MCMCFRAMEWORK_TPRIORWITHHYPERPRIOR_H

#include "TPrior.h"

#ifdef WITH_ARMADILLO
#include <armadillo>
#endif

//-------------------------------------------
// TPriorWithHyperPrior
//-------------------------------------------

template <typename T>
class TPriorWithHyperPrior : virtual public TPrior<T> {
protected:
    virtual void _checkSizeParams(int actualSize, int expectedSize);
    virtual void _checkSizeObservations(int actualSize, int expectedSize);
    virtual void _updateTempVals(){};
    virtual void _checkMinMaxOfHyperPrior(){};
    void _checkTypesOfPriorParameters(const std::shared_ptr<TMCMCObservationsBase> & priorParam, bool throwIfNot, bool mustBeFloatingPoint, bool mustBeIntegral, bool mustBeUnsigned) const;

public:
    TPriorWithHyperPrior() = default;
    ~TPriorWithHyperPrior() override = default;
};

//-------------------------------------------
// TPriorNormalWithHyperPrior
//-------------------------------------------

template <typename T>
class TPriorNormalWithHyperPrior: public TPriorWithHyperPrior<T>, public TPriorNormal<T> {
protected:
    std::shared_ptr<TMCMCParameterBase> _mean;
    std::shared_ptr<TMCMCParameterBase> _var;
    double _oneDivTwo_log_var;

    // update mean
    virtual double _calcLLUpdateMean();
    virtual double _calcLogHUpdateMean();
    virtual void _updateMean();
    // update var
    virtual double _calcLLUpdateVar();
    virtual double _calcLogHUpdateVar();
    virtual void _updateVar();
    void _updateTempVals() override;
    // check min/max
    void _checkMinMaxOfHyperPrior() override;
    // log-densities
    double _getLogPriorDensity_oneVal(T x) override;
    double _getLogPriorRatio_oneVal(T x, T x_old) override;
    // initialize prior parameters to MLE
    void _setMeanToMLE();
    void _setVarToMLE();
public:
    TPriorNormalWithHyperPrior();
    virtual ~TPriorNormalWithHyperPrior() override = default;
    void initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, std::vector<std::shared_ptr<TObservationsBase> > & observations) override;

    // construct DAG
    void typesAreCompatible(bool throwIfNot) override;
    void constructDAG(TDAG & DAG, TDAG & temporaryDAG) override;
    void initializeStorageOfPriorParameters() override;

    // estimate initial values
    void estimateInitialPriorParameters(TLog * Logfile) override;

    // update functions
    double getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data) override;
    void updateParams() override;

    // getters
    double mean() const override;
    double var() const override;

    // simulate values
    void simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) override;
};

//-------------------------------------------
// TPriorExponentialWithHyperPrior
//-------------------------------------------

template <typename T>
class TPriorExponentialWithHyperPrior: public TPriorWithHyperPrior<T>, public TPriorExponential<T> {
protected:
    std::shared_ptr<TMCMCParameterBase> _lambda;

    // update lambda
    virtual double _calcLLUpdateLambda();
    virtual double _calcLogHUpdateLambda();
    virtual void _updateLambda();
    void _updateTempVals() override;
    void _checkMinMaxOfHyperPrior() override;
    // log-densities
    double _getLogPriorDensity_oneVal(T x) override;
    double _getLogPriorRatio_oneVal(T x, T x_old) override;
    // initialize prior parameters to MLE
    void _setLambdaToMLE();
public:
    TPriorExponentialWithHyperPrior();
    ~TPriorExponentialWithHyperPrior() override = default;
    void initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, std::vector<std::shared_ptr<TObservationsBase> > & observations) override ;

    // construct DAG
    void constructDAG(TDAG & DAG, TDAG & temporaryDAG) override;
    void typesAreCompatible(bool throwIfNot) override;
    void initializeStorageOfPriorParameters() override;

    // estimate initial values
    void estimateInitialPriorParameters(TLog * Logfile) override;

    // update functions
    double getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data) override;
    void updateParams() override;

    // getters
    double lambda() const override;

    // simulate values
    void simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) override;
};

//-------------------------------------------
// TPriorBetaWithHyperPrior
//-------------------------------------------

template <typename T>
class TPriorBetaWithHyperPrior: public TPriorWithHyperPrior<T>, public TPriorBeta<T> {
protected:
    std::shared_ptr<TMCMCParameterBase> _alpha;
    std::shared_ptr<TMCMCParameterBase> _beta;

    // update alpha
    virtual double _calcLLUpdateAlpha();
    virtual double _calcLogHUpdateAlpha();
    virtual void _updateAlpha();
    // update beta
    virtual double _calcLLUpdateBeta();
    virtual double _calcLogHUpdateBeta();
    virtual void _updateBeta();

    void _updateTempVals() override;

    void _checkMinMaxOfHyperPrior() override;

    // log-densities
    double _getLogPriorDensity_oneVal(T x) override;
    double _getLogPriorRatio_oneVal(T x, T x_old) override;

    // initialize prior parameters to method of moments estimates
    void _calculateMeanAndVar(double & mean, double & var);
    void _setAlphaToMOM(const double & mean, const double &nu);
    void _setBetaToMOM(const double & mean, const double &nu);

public:
    TPriorBetaWithHyperPrior();
    ~TPriorBetaWithHyperPrior() override = default;
    void initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, std::vector<std::shared_ptr<TObservationsBase> > & observations) override;

    // construct DAG
    void constructDAG(TDAG & DAG, TDAG & temporaryDAG) override;
    void typesAreCompatible(bool throwIfNot) override;
    void initializeStorageOfPriorParameters() override;

    // estimate initial values
    void estimateInitialPriorParameters(TLog * Logfile) override;

    // update functions
    double getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data) override;
    void updateParams() override;

    // getters
    double alpha() const override;
    double beta() const override;

    // simulate values
    void simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) override;
};


//-------------------------------------------
// TPriorBinomialWithHyperPrior
//-------------------------------------------

template <typename T>
class TPriorBinomialWithHyperPrior: public TPriorWithHyperPrior<T>, public TPriorBinomial<T> {
protected:
    std::shared_ptr<TMCMCParameterBase> _paramP;

    // update lambda
    virtual double _calcLLUpdateP();
    virtual double _calcLogHUpdateP();
    virtual void _updateP();
    void _updateTempVals() override;
    void _checkMinMaxOfHyperPrior() override;

    // initialize prior parameters to MLE
    void _setPToMLE();

public:
    TPriorBinomialWithHyperPrior();
    ~TPriorBinomialWithHyperPrior() override = default;
    void initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, std::vector<std::shared_ptr<TObservationsBase> > & observations) override ;

    // construct DAG
    void constructDAG(TDAG & DAG, TDAG & temporaryDAG) override;
    void typesAreCompatible(bool throwIfNot) override;
    void initializeStorageOfPriorParameters() override;
    bool checkMinMax(std::string &min, std::string &max, bool &minIsIncluded, bool &maxIsIncluded, bool & hasDefaultMin, bool & hasDefaultMax) override;

    // estimate initial values
    void estimateInitialPriorParameters(TLog * Logfile) override;

    // update functions
    double getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data) override;
    void updateParams() override;

    // getters
    double p() const override;

    // simulate values
    void simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) override;
};


//-------------------------------------------
// TPriorBernouilliWithHyperPrior
//-------------------------------------------

template <typename T>
class TPriorBernouilliWithHyperPrior: public TPriorWithHyperPrior<T>, public TPriorBernouilli<T>, public TEMPriorIID_base<double, size_t, size_t> {
protected:
    std::shared_ptr<TMCMCParameterBase> _pi;

    // update lambda
    virtual double _calcLLUpdatePi();
    virtual double _calcLogHUpdatePi();
    virtual void _updatePi();
    void _updateTempVals() override;
    void _checkMinMaxOfHyperPrior() override;
    // log-densities
    double _getLogPriorDensity_oneVal(T x) override;
    double _getLogPriorRatio_oneVal(T x, T x_old) override;

    // initialize prior parameters to MLE
    void _setPiToMLE();

    // initialize prior parameters with EM
    double _EMSum;
    uint64_t _EMTotal;
    bool _ranEM;

public:
    TPriorBernouilliWithHyperPrior();
    ~TPriorBernouilliWithHyperPrior() override = default;
    void initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, std::vector<std::shared_ptr<TObservationsBase> > & observations) override ;

    // construct DAG
    void constructDAG(TDAG & DAG, TDAG & temporaryDAG) override;
    void typesAreCompatible(bool throwIfNot) override;
    void initializeStorageOfPriorParameters() override;

    // initialize with MLE
    void estimateInitialPriorParameters(TLog * Logfile) override;

    // initialize with EM
    void runEMEstimation(TLatentVariable<double, size_t, size_t> & latentVariable, TLog * Logfile);
    const double operator[](const size_t & State) const override;
    void handleEMParameterInitialization(const size_t & Index, const size_t & State) override;
    void finalizeEMParameterInitialization() override;

    //EM update
    void prepareEMParameterEstimationInitial() override;
    void prepareEMParameterEstimationOneIteration() override;
    void handleEMParameterEstimationOneIteration(const size_t & Index, const TDataVector<double, size_t> & weights) override;
    void finalizeEMParameterEstimationOneIteration() override;
    void finalizeEMParameterEstimationFinal() override;
    void switchPriorClassificationAfterEM(TLog * Logfile) override;

    // MCMC update functions
    double getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data) override;
    void updateParams() override;

    // getters
    double pi() const override;

    // simulate values
    void simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) override;
};


//-------------------------------------------
// TPriorCategoricalWithHyperPrior
//-------------------------------------------

template <typename T>
class TPriorCategoricalWithHyperPrior: public TPriorWithHyperPrior<T>, public TEMPriorIID_base<double, size_t, size_t> {
protected:
    std::shared_ptr<TMCMCParameterBase> _pis;
    T _K;

    // update lambda
    double _calcLLUpdatePi();
    double _calcLogHUpdatePi();
    virtual void _updatePi(const size_t & k);
    void _normalizePis(bool onlyReSetNewValue, const size_t & k = 0);
    void _checkMinMaxOfHyperPrior() override;
    // log-densities
    double _getLogPriorDensity_oneVal(T x) override;
    double _getLogPriorRatio_oneVal(T x, T x_old) override;

    // initialize prior parameters to MLE
    void _setPisToMLE();

    // initialize prior parameters with EM
    std::vector<double> _EMSums;
    uint64_t _EMTotal;
    bool _ranEM;

public:
    TPriorCategoricalWithHyperPrior();
    ~TPriorCategoricalWithHyperPrior() override = default;
    void initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, std::vector<std::shared_ptr<TObservationsBase> > & observations) override ;

    // construct DAG
    void constructDAG(TDAG & DAG, TDAG & temporaryDAG) override;
    void typesAreCompatible(bool throwIfNot) override;
    bool checkMinMax(std::string &min, std::string &max, bool & minIsIncluded, bool & maxIsIncluded, bool & hasDefaultMin, bool & hasDefaultMax) override;
    void initializeStorageOfPriorParameters() override;

    // MCMC update functions
    double getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data) override;
    void updateParams() override;

    // getters
    double pi(const size_t& k) const;

    // initialize with MLE
    void estimateInitialPriorParameters(TLog * Logfile) override;

    // initialize with EM
    void runEMEstimation(TLatentVariable<double, size_t, size_t> & latentVariable, TLog * Logfile);
    const double operator[](const size_t & State) const override;
    void handleEMParameterInitialization(const size_t & Index, const size_t & State) override;
    void finalizeEMParameterInitialization() override;

    //EM update
    void prepareEMParameterEstimationInitial() override;
    void prepareEMParameterEstimationOneIteration() override;
    void handleEMParameterEstimationOneIteration(const size_t & Index, const TDataVector<double, size_t> & weights) override;
    void finalizeEMParameterEstimationOneIteration() override;
    void finalizeEMParameterEstimationFinal() override;

    // simulate values
    void simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) override;
};


//-------------------------------------------
// TTransitionMatrixExponentialMCMCPrior
//-------------------------------------------

template <typename T>
class TTransitionMatrixExponentialMCMCPrior : virtual public TTransitionMatrixExponentialBase<double, size_t, size_t>, public TPriorWithHyperPrior<T> {
protected:
    bool _paramsAreInitialized;

    // update transition matrices
    void _updateTransitionMatrices();
    void _resetTransitionMatrices();

    // update parameters of generating matrix
    virtual void _fillCurrentGeneratingMatrixParameters(std::vector<double> & Values) = 0;
    double _calcLLUpdateGeneratingMatrixParameter();
    double _calcLogHUpdateGeneratingMatrixParameter(std::shared_ptr<TMCMCParameterBase> & PriorParam);
    virtual void _updateGeneratingMatrixParameter(std::shared_ptr<TMCMCParameterBase> & PriorParam);

    // log-densities
    double _calcLogPOfValueGivenPrevious(const std::shared_ptr<TMCMCObservationsBase> & data, const size_t & i);
    double _calcLogPOfNextGivenValue(const std::shared_ptr<TMCMCObservationsBase> & data, const size_t & i);
    double _calcLogPOfOldValueGivenPrevious(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & i);
    double _calcLogPOfNextGivenOldValue(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & i);

    double _getLogPriorDensity_oneVal(T x) override;
    double _getLogPriorRatio_oneVal(T x, T x_old) override;
    double _getLogPriorDensity_vec(const std::shared_ptr<TMCMCObservationsBase> & data, const size_t & index) override;
    double _getLogPriorDensityOld_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index) override;
    double _getLogPriorRatio_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index) override;

    // tmp stuff for EM
    bool _ranEM;
    void _estimateInitialPriorParameters(TLog * Logfile, const bool & FixTransitionMatricesDuringEM);
    void _runEMEstimation(TLatentVariable<double, size_t, size_t> & latentVariable, TLog * Logfile, const bool & FixTransitionMatricesDuringEM);

public:
    TTransitionMatrixExponentialMCMCPrior();
    ~TTransitionMatrixExponentialMCMCPrior() override = default;

    // initialize
    void initGeneratingMatrix(const size_t & NumStates, std::unique_ptr<TGeneratingMatrixBase<double, size_t>> & GeneratingMatrix);
    void initDistances(const std::shared_ptr<TDistancesBinnedBase> & distances) override;

    // construct DAG
    bool checkMinMax(std::string &min, std::string &max, bool & minIsIncluded, bool & maxIsIncluded, bool & hasDefaultMin, bool & hasDefaultMax) override;

    // return sum of log-densities of entire vector
    double getSumLogPriorDensity(const std::shared_ptr<TMCMCObservationsBase> & data) override;

    // initialize without EM if hidden states are known
    void estimateInitialPriorParameters_HiddenStateKnown(const std::vector<std::shared_ptr<TMCMCObservationsBase>> & Parameters, TLog * Logfile);

    // simulate values
    void simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) override;
};

//-------------------------------------------
// TPriorHMMBoolWithHyperPrior
//-------------------------------------------

template <typename T>
class TPriorHMMBoolWithHyperPrior: public TTransitionMatrixExponentialNelderMead<double, size_t, size_t>, public TTransitionMatrixExponentialMCMCPrior<T> {
protected:
    std::shared_ptr<TMCMCParameterBase> _lambda1;
    std::shared_ptr<TMCMCParameterBase> _lambda2;

    // transition matrices
    void _fillCurrentGeneratingMatrixParameters(std::vector<double> & Values) override;
    void _fillParametersBasedOnGeneratingMatrix(const std::vector<double> & Values) override;

    void _checkMinMaxOfHyperPrior() override;

    // tmp stuff for EM
    bool _fixTransitionMatricesDuringEM();

public:
    TPriorHMMBoolWithHyperPrior();
    ~TPriorHMMBoolWithHyperPrior() override = default;

    // initialize
    void initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, std::vector<std::shared_ptr<TObservationsBase> > & observations) override;

    // construct DAG
    void constructDAG(TDAG & DAG, TDAG & temporaryDAG) override;
    void typesAreCompatible(bool throwIfNot) override;
    void initializeStorageOfPriorParameters() override;

    // initialize values with EM
    void estimateInitialPriorParameters(TLog * Logfile) override;
    void runEMEstimation(TLatentVariable<double, size_t, size_t> & latentVariable, TLog * Logfile) override;
    void switchPriorClassificationAfterEM(TLog * Logfile) override;

    // getters
    double lambda1() const;
    double lambda2() const;

    // update lambda1 and lambda2
    void updateParams() override;

    // return sum of log-densities of entire vector
    double getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data) override;
};

//-------------------------------------------
// TPriorHMMLadderWithHyperPrior
//-------------------------------------------

template <typename T>
class TPriorHMMLadderWithHyperPrior: public TTransitionMatrixExponentialNewtonRaphson<double, size_t, size_t>, public TTransitionMatrixExponentialMCMCPrior<T> {
protected:
    std::shared_ptr<TMCMCParameterBase> _kappa;
    std::shared_ptr<TMCMCParameterBase> _numStatesParam;

    // transition matrices
    void _fillCurrentGeneratingMatrixParameters(std::vector<double> & Values) override;
    void _fillParametersBasedOnGeneratingMatrix(const std::vector<double> & Values) override;

    void _checkMinMaxOfHyperPrior() override;

public:
    TPriorHMMLadderWithHyperPrior();
    ~TPriorHMMLadderWithHyperPrior() override = default;

    // initialize
    void initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, std::vector<std::shared_ptr<TObservationsBase> > & observations) override;
    void setNumStates(const size_t & NumStates);

    // construct DAG
    void constructDAG(TDAG & DAG, TDAG & temporaryDAG) override;
    void typesAreCompatible(bool throwIfNot) override;
    void initializeStorageOfPriorParameters() override;

    // initialize values with EM
    void estimateInitialPriorParameters(TLog * Logfile) override;
    void runEMEstimation(TLatentVariable<double, size_t, size_t> & latentVariable, TLog * Logfile) override;
    void switchPriorClassificationAfterEM(TLog * Logfile) override;

    // update kappa
    void updateParams() override;

    // getters
    double kappa() const;

    // return sum of log-densities of entire vector
    double getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data) override;
};

//-------------------------------------------
// TMultivariateNormal
//-------------------------------------------
#ifdef WITH_ARMADILLO
template <typename T>
class TMultivariateNormal: public TPriorWithHyperPrior<T> {
    // base class that gets pointers to TMCMCParameters (for mu, m, mrr and mrs) and calculates densities etc. with these
    // used by derived classes TPriorMultivariateNormalWithHyperPrior and TPriorMultivariateNormalMixedModelWithHyperPrior
protected:
    // dimensionality (stored as int and not uint, because we calculate -_D in many calculations, which gives something undefined)
    int _D;

    // temporary values
    double _minusDdiv2Log2Pi;

    // calculate dimensions
    void _calculateAndSetD();
    double _numberOfElementsInTriangularMatrix_Diagonal0(const size_t & dimensions);
    double _calc_Dimensions_from_numberOfElementsInTriangularMatrix_Diagonal0(const size_t & totalSize);
    inline size_t _linearizeIndex_Mrs(const size_t & r, const size_t & s) const{
        // this is for a triangular matrix where the diagonal is ALSO zero
        return r * (r-1) / 2 + s;
    }

    // check dimensions of prior parameters
    void _setDimensionsOfMu(const std::shared_ptr<TMCMCParameterBase> & mu, const std::shared_ptr<TNamesEmpty> & DimName);
    void _setDimensionsOfM(const std::shared_ptr<TMCMCParameterBase> & m);
    void _setDimensionsOfMrr(std::shared_ptr<TMCMCParameterBase> & mrr, const std::shared_ptr<TNamesEmpty> & DimName);
    void _setDimensionsOfMrs(const std::shared_ptr<TMCMCParameterBase> & mrs, const std::shared_ptr<TNamesEmpty> & DimName);
    void _reSetBoundaryTo0_M_Mrr(const std::shared_ptr<TMCMCParameterBase> & param);
    const std::shared_ptr<TNamesEmpty> & _getDimensionName();
    std::shared_ptr<TNamesEmpty> _generateDimNamesMrs(const std::shared_ptr<TNamesEmpty> & DimName);

    // update parameter on which prior is defined
    double _calcDoubleSum(const std::shared_ptr<TMCMCObservationsBase> & data, const TRange & row, const std::shared_ptr<TMCMCParameterBase> & mus, const std::shared_ptr<TMCMCParameterBase> & Mrr, const std::shared_ptr<TMCMCParameterBase> & Mrs);
    double _calcDoubleSum_OldParam(const std::shared_ptr<TMCMCParameterBase> & data, const TRange & row, const size_t & indexInRowThatChanged, const std::shared_ptr<TMCMCParameterBase> & mus, const std::shared_ptr<TMCMCParameterBase> & Mrr, const std::shared_ptr<TMCMCParameterBase> & Mrs);
    double _calcSumM_dr_squared(const double & mrr, const size_t & r, const std::shared_ptr<TMCMCParameterBase> & Mrs);
    double _calcSumLogMrr(const std::shared_ptr<TMCMCParameterBase> & Mrr);

    // update mus
    double _calcDoubleSum_updateParam(const std::shared_ptr<TMCMCObservationsBase> & data, const TRange & row, const size_t & indexMuThatChanged, const size_t &  min_r_DMin1, const std::shared_ptr<TMCMCParameterBase> & mus, const std::shared_ptr<TMCMCParameterBase> & Mrr, const std::shared_ptr<TMCMCParameterBase> & Mrs);
    double _calcDoubleSum_updateMu(const std::shared_ptr<TMCMCObservationsBase> & paramToUse, const TRange & row, const size_t & indexMuThatChanged, const size_t &  min_r_DMin1, const std::shared_ptr<TMCMCParameterBase> & mus, const std::shared_ptr<TMCMCParameterBase> & Mrr, const std::shared_ptr<TMCMCParameterBase> & Mrs);
    double _calcLLRatioUpdateMu(const double & oldMu, const double & newMu, const double & sumM, size_t N, const double & sumParam, const double & sum, const std::shared_ptr<TMCMCParameterBase> & m);

    // update m
    double _calcLLRatioUpdateM(const size_t & N, const double & oldM, const double & newM, const double & sum);

    // update mrr
    double _calcLLRatioUpdateMrr(const size_t & N, const double & oldMrr, const double & newMrr, const double & sum, const double & sumParam_nd_minus_mu_d, const std::shared_ptr<TMCMCParameterBase> & m);
    double _calcDoubleSum_updateMrr(const std::shared_ptr<TMCMCObservationsBase> & paramToUse, const TRange & row, const size_t & indexMrrThatChanged, double & sumParam_nd_minus_mu_d, const std::shared_ptr<TMCMCParameterBase> & mus, const std::shared_ptr<TMCMCParameterBase> & Mrs);

    // update mrs
    double _calcLLRatioUpdateMrs(const double & oldMrs, const double & newMrs, const double & sum, const double & sumParam_nd_minus_mu_d, const std::shared_ptr<TMCMCParameterBase> & m);
    double _calcDoubleSum_updateMrs(const std::shared_ptr<TMCMCObservationsBase> & paramToUse, const TRange & row, const size_t & d, const size_t & e, double & sumParam_nd_minus_mu_d, const std::shared_ptr<TMCMCParameterBase> & mus, const std::shared_ptr<TMCMCParameterBase> & Mrr, const std::shared_ptr<TMCMCParameterBase> & Mrs);

    // log-densities
    double _getLogPriorDensity_oneVal(T x) override;
    double _getLogPriorRatio_oneVal(T x, T x_old) override;
    template <typename C> double _calcLogPriorDensity(const std::shared_ptr<C> & paramOrdata, const TRange &row, const std::shared_ptr<TMCMCParameterBase> & mus, const std::shared_ptr<TMCMCParameterBase> & m,
                                const std::shared_ptr<TMCMCParameterBase> & Mrr, const std::shared_ptr<TMCMCParameterBase> & Mrs);
    double _calcLogPriorDensityOld(const std::shared_ptr<TMCMCParameterBase> & data, const TRange &row, const size_t & col, const std::shared_ptr<TMCMCParameterBase> & mus, const std::shared_ptr<TMCMCParameterBase> & m,
                                   const std::shared_ptr<TMCMCParameterBase> & Mrr, const std::shared_ptr<TMCMCParameterBase> & Mrs);
    double _calcLogPriorRatio(const std::shared_ptr<TMCMCParameterBase> & data, const TRange &row, const size_t & d, const std::shared_ptr<TMCMCParameterBase> & mus, const std::shared_ptr<TMCMCParameterBase> & m,
                              const std::shared_ptr<TMCMCParameterBase> & Mrr, const std::shared_ptr<TMCMCParameterBase> & Mrs);

    // initialize prior parameters
    void _addToSumMLEMu(std::vector<double> & sums, const TRange & row, const std::shared_ptr<TMCMCObservationsBase> & paramToUse);
    void _normalizeSumMLEMu(std::vector<double> & sums, const double & totalNumElements);
    std::vector<double> _calculateMLEMu();
    void _addToSumMLESigma(arma::mat & sums, const TRange & row, const std::shared_ptr<TMCMCObservationsBase> & paramToUse, const std::shared_ptr<TMCMCParameterBase> & mus);
    void _normalizeSumMLESigma(arma::mat & sums, const double & totalNumElements);
    arma::mat _calculateMLESigma(const std::shared_ptr<TMCMCParameterBase> & mus);
    arma::mat _calculateMFromSigma(const arma::mat & Sigma);
    void _fillParametersMFromSigma(const arma::mat & Sigma, std::shared_ptr<TMCMCParameterBase> &Mrr, std::shared_ptr<TMCMCParameterBase> &Mrs, std::shared_ptr<TMCMCParameterBase> &m);

    // simulate
    void _fillArmadilloMFromParametersM(arma::mat & M, const std::shared_ptr<TMCMCParameterBase> & Mrr, const std::shared_ptr<TMCMCParameterBase> & Mrs, const std::shared_ptr<TMCMCParameterBase> & m);
    arma::mat _getArmadilloLForSimulation(const std::shared_ptr<TMCMCParameterBase> & Mrr, const std::shared_ptr<TMCMCParameterBase> & Mrs, const std::shared_ptr<TMCMCParameterBase> & m);
    void _fillRandomMultivariateValues(std::vector<double> & x, const arma::mat &L, const std::shared_ptr<TMCMCParameterBase> &mus, TRandomGenerator * randomGenerator);
    void _simulateMultivariateNormal(const std::shared_ptr<TMCMCObservationsBase> & data, size_t index, const arma::mat &L, const std::shared_ptr<TMCMCParameterBase> & mus, TRandomGenerator * randomGenerator);

public:
    TMultivariateNormal();

    // return sum of log-densities of entire vector
    double getSumLogPriorDensity(const std::shared_ptr<TMCMCObservationsBase> & data) override;
    double getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data) override;
};
#else
// armadillo is not defined -> will throw an error, because multivariate normal requires armadillo
template <typename T>
class TMultivariateNormal: public TPriorWithHyperPrior<T> {
public:
    TMultivariateNormal() : TPriorWithHyperPrior<T>() {
        throw "The library 'armadillo' has not been found but is required to use a multivariate normal prior!";
    };
};
#endif //WITH_ARMADILLO

//-------------------------------------------
// TPriorMultivariateNormalWithHyperPrior
//-------------------------------------------
#ifdef WITH_ARMADILLO
template <typename T>
class TPriorMultivariateNormalWithHyperPrior: public TMultivariateNormal<T> {
protected:
    // parameters
    std::shared_ptr<TMCMCParameterBase> _mus;
    std::shared_ptr<TMCMCParameterBase> _m;
    std::shared_ptr<TMCMCParameterBase> _Mrr;
    std::shared_ptr<TMCMCParameterBase> _Mrs;

    // update mus
    double _calcLLUpdateMu(const size_t & r);
    double _calcLogHUpdateMu(const size_t & r);
    void _updateMu(const size_t & r);

    // update m
    double _calcLLUpdateM();
    double _calcLogHUpdateM();
    void _updateM();

    // update mrr
    double _calcLLUpdateMrr(const size_t & r);
    double _calcLogHUpdateMrr(const size_t & r);
    void _updateMrr(const size_t & r);

    // update mrs
    double _calcLLUpdateMrs(const size_t & r, const size_t & s);
    double _calcLogHUpdateMrs(const size_t & r, const size_t & s);
    void _updateMrs(const size_t & r, const size_t & s);

    // checks
    void _checkMinMaxOfHyperPrior() override;
    // log-densities
    double _getLogPriorDensity_vec(const std::shared_ptr<TMCMCObservationsBase> & data, const size_t & index) override;
    double _getLogPriorDensityOld_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index) override;
    double _getLogPriorRatio_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index) override;

    // initialize prior parameters
    void _setMusToMLE();
    void _setMToMLE();

public:
    TPriorMultivariateNormalWithHyperPrior();
    void initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, std::vector<std::shared_ptr<TObservationsBase> > & observations) override;

    // construct DAG
    void constructDAG(TDAG & DAG, TDAG & temporaryDAG) override;
    void typesAreCompatible(bool throwIfNot) override;
    void initializeStorageOfPriorParameters() override;

    // return sum of log-densities of entire vector
    double getSumLogPriorDensity(const std::shared_ptr<TMCMCObservationsBase> & data) override;
    double getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data) override;

    // estimate initial values
    void estimateInitialPriorParameters(TLog * Logfile) override;

    // update mu, m, mrr, mrs
    void updateParams() override;

    // simulate values
    void simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) override;
};
#else
// armadillo is not defined -> will throw an error, because multivariate normal requires armadillo
template <typename T>
class TPriorMultivariateNormalWithHyperPrior: public TMultivariateNormal<T> {
public:
    TPriorMultivariateNormalWithHyperPrior() : TMultivariateNormal<T>() {
        throw "The library 'armadillo' has not been found but is required to use a multivariate normal prior!";
    };
};
#endif //WITH_ARMADILLO

#include "TPriorWithHyperPrior.tpp"

#endif //MCMCFRAMEWORK_TPRIORWITHHYPERPRIOR_H
