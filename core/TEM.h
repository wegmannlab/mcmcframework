//
// Created by madleina on 12.01.21.
//

#ifndef EMPTY_TEM_H
#define EMPTY_TEM_H

#include "TLog.h"
#include "TLatentVariable.h"
#include "TEMVars.h"
#include "THMMVars.h"

//-------------------------------------
// TEM_base
// a pure virtual base class
//-------------------------------------

template <typename PrecisionType, typename NumStatesType, typename LengthType> class TEM_base{
protected:
    // latent variable
    TLatentVariable<PrecisionType, NumStatesType, LengthType>& _latentVariable;

    // logfile
    TLog* _logfile;
    bool _writeLog;

    //file for writing
    TOutputFile _out;

    //settings
    uint16_t _EMNumIterations;
    PrecisionType _EMMinDeltaLL;
    bool _EMEstimatePrior;

    //Auxiliary functions
    void _initFromParameters(TParameters & Parameters){
        //reading estimation parameters
        _EMMinDeltaLL = Parameters.getParameterWithDefault("deltaLL", 0.0001);
        _EMNumIterations = Parameters.getParameterWithDefault("iterations", 100);
    };

    void _reportParameters(){
        //reading estimation parameters
        _logfile->startIndent("EM parameters:");
        _logfile->list("Will run the EM algorithm until deltaLL < " + toString(_EMMinDeltaLL) + ". (argument 'deltaLL')");
        _logfile->list("Will run at max " + toString(_EMNumIterations) + " EM iterations. (argument 'iterations')");
        _logfile->endIndent();
    };

    //initialize EM parameters prior to EM
    void _prepareEMParameterEstimationInitial(TEMPrior_base<PrecisionType, NumStatesType, LengthType> & EMPrior) {
        _latentVariable.prepareEMParameterEstimationInitial();
        if (_EMEstimatePrior) {
            EMPrior.prepareEMParameterEstimationInitial();
        }
    }

    void _initializeEMParameters(const std::vector<LengthType> & JunkEnds, TEMPrior_base<PrecisionType, NumStatesType, LengthType> & EMPrior) {
        _latentVariable.initializeEMParameters();
        if (_EMEstimatePrior) {
            EMPrior.initializeEMParameters(_latentVariable, JunkEnds);
        }
    }

    // functions to run EM
    virtual PrecisionType _runEMOneIteration_OneJunk(const LengthType & First, const LengthType & Last, void (TEM_base::*handle)(const LengthType &)) = 0;

    PrecisionType _runEMOneIteration(const std::vector<LengthType> & JunkEnds, void (TEM_base::*handle)(const LengthType &)){
        //loop across junks when running EM
        LengthType first = 0;
        PrecisionType LL = 0.0;
        for(size_t j = 0; j < JunkEnds.size(); ++j){
            LengthType last = JunkEnds[j];
            LengthType length = last - first;

            //report junk
            if(JunkEnds.size() > 1 && _writeLog){
                _logfile->startIndent("Running junk " + toString(j + 1) + " of " + toString(JunkEnds.size()) + ":");
                _logfile->list("Junk is of length " + toString(length) + " with indexes [" + toString(first) + ", " + toString(last) + ").");
            }

            //run EM on current junk
            LL += _runEMOneIteration_OneJunk(first, last, handle);

            //update first of next
            first = last;

            //end of junk
            if(JunkEnds.size() > 1 && _writeLog){
                _logfile->endIndent();
            }
        }
        return LL;
    };

    // function to calculate LL
    virtual PrecisionType _calculateLL_OneJunk(const LengthType & First, const LengthType & Last) = 0;

    // prepare EM
    void _prepareEMParameterEstimationOneIteration(TEMPrior_base<PrecisionType, NumStatesType, LengthType> & EMPrior){
        _latentVariable.prepareEMParameterEstimationOneIteration();
        if(_EMEstimatePrior){
            EMPrior.prepareEMParameterEstimationOneIteration();
        }
    };

    //handle functions
    virtual void _handleEMParameterEstimationOneIteration(const LengthType & Index) = 0;
    virtual void _handleStatePosteriorEstimation(const LengthType & Index) = 0;
    virtual void _handleStatePosteriorEstimationAndWriting(const LengthType & Index) = 0;

    // finalize iteration and EM
    void _finalizeEMParameterEstimationOneIteration(TEMPrior_base<PrecisionType, NumStatesType, LengthType> & EMPrior){
    	if(_EMEstimatePrior){
			EMPrior.finalizeEMParameterEstimationOneIteration();
		}
    	_latentVariable.finalizeEMParameterEstimationOneIteration();
    };

    void _finalizeEMParameterEstimationFinal(TEMPrior_base<PrecisionType, NumStatesType, LengthType> & EMPrior){
        _latentVariable.finalizeEMParameterEstimationFinal();
        if (_EMEstimatePrior){
            EMPrior.finalizeEMParameterEstimationFinal();
        }
    };

    void _reportEMParameters(TEMPrior_base<PrecisionType, NumStatesType, LengthType> & EMPrior){
		if(_EMEstimatePrior){
			EMPrior.reportEMParameters();
		}
		_latentVariable.reportEMParameters();
	};

    virtual PrecisionType _runEM(const std::vector<LengthType> & JunkEnds, TEMPrior_base<PrecisionType, NumStatesType, LengthType> & EMPrior){
        if(_EMNumIterations == 0){
            throw std::runtime_error("runEM(): _EMNumIterations is zero!");
        }

        //use EM to infer parameters of the model
        if(_writeLog){ _logfile->startNumbering("Running EM iterations to estimate hierarchical parameters:"); }

        //tmp variables for EM
        PrecisionType previousLL;
        uint16_t iteration = 0;

        // prepare initial EM steps: initialize storage
        _prepareEMParameterEstimationInitial(EMPrior);

        // initialize EM parameters to some values
        _initializeEMParameters(JunkEnds, EMPrior);

        //run iterations
        for(; iteration < _EMNumIterations; ++iteration){
            if(_writeLog){ _logfile->numberWithIndent("EM iteration " + toString(iteration+1) + ":"); }

            //prepare parameter updates
            _prepareEMParameterEstimationOneIteration(EMPrior);

            //run one EM iteration
            PrecisionType LL = _runEMOneIteration(JunkEnds, &TEM_base::_handleEMParameterEstimationOneIteration);

            //update parameters
            if(_writeLog){ _logfile->listFlushTime("Updating parameters ..."); }
            _finalizeEMParameterEstimationOneIteration(EMPrior);
            if(_writeLog){
            	_logfile->done();
            	_reportEMParameters(EMPrior);
            }

            //check if we break
            if(_writeLog){ _logfile->list("Current LL = ", LL, "."); }
            if(iteration > 0){
                PrecisionType deltaLL = LL - previousLL;
                if(_writeLog){
                    _logfile->conclude("Delta LL = ", deltaLL, ".");
                }
                if(deltaLL < _EMMinDeltaLL){
                    if(_writeLog){
                        _logfile->conclude("EM has converged!");
                        _logfile->endIndent();
                    }
                    break;
                }
            }
            previousLL = LL;

            //end of iteration
            if(_writeLog){ _logfile->endIndent(); }
        }

        //report end
        if(iteration == _EMNumIterations && _writeLog){
            _logfile->conclude("Reached max number of iterations.");
        }

        // clean EM
        _finalizeEMParameterEstimationFinal(EMPrior);

        //end of EM
        if(_writeLog){
            _logfile->endNumbering();
            _logfile->endIndent();
        }

        return previousLL;
    };

    virtual PrecisionType _calculateLL(const std::vector<LengthType> & JunkEnds, TEMPrior_base<PrecisionType, NumStatesType, LengthType> & EMPrior){
        if(_writeLog){
            _logfile->startIndent("Calculating LL ...");
        }

        // prepare
        _latentVariable.prepareLLCalculation(EMPrior.numStates());

        //loop across junks
        LengthType first = 0;
        PrecisionType LL = 0.0;
        for(size_t j = 0; j < JunkEnds.size(); ++j){
            LengthType last = JunkEnds[j];
            //report junk
            if(JunkEnds.size() > 1 && _writeLog){
                _logfile->startIndent("Running junk " + toString(j + 1) + " of " + toString(JunkEnds.size()) + ":");
            }

            //calculate LL
            LL += _calculateLL_OneJunk(first, last);

            //update first of next
            first = last;

            //end of junk
            if(JunkEnds.size() > 1 && _writeLog){
                _logfile->endIndent();
            }
        }

        // finalize
        _latentVariable.finalizeLLCalculation();

        //report
        if(_writeLog){
            _logfile->endIndent();
            _logfile->conclude("LL = ", LL);
        }

        //return
        return LL;
    };

    PrecisionType _estimateStatePosteriors(const std::vector<LengthType> & JunkEnds, TEMPrior_base<PrecisionType, NumStatesType, LengthType> & EMPrior){
        if(_writeLog){
            _logfile->startIndent("Estimating State Posteriors:");
        }

        // prepare
        _latentVariable.prepareStatePosteriorEstimation(EMPrior.numStates());

        //run EM
        PrecisionType LL = _runEMOneIteration(JunkEnds, &TEM_base::_handleStatePosteriorEstimation);

        // finalize
        _latentVariable.finalizeStatePosteriorEstimation();

        //report
        if(_writeLog){
            _logfile->list("Current LL = ", LL, ".");
            _logfile->endIndent();
        }

        return LL;
    };

    virtual PrecisionType _estimateStatePosteriors(const std::vector<LengthType> & JunkEnds, const std::string & Filename, TEMPrior_base<PrecisionType, NumStatesType, LengthType> & EMPrior){
        if(_writeLog){
            _logfile->startIndent("Estimating State Posteriors:");
            _logfile->list("Writing state posteriors to '", Filename, "'.");
        }

        // assemble header
        std::vector<std::string> header;
         _latentVariable.prepareStatePosteriorEstimationAndWriting(EMPrior.numStates(), header);

        //open file
        _out.open(Filename);
        _out.writeHeader(header);

        //run forward-backward
        PrecisionType LL = _runEMOneIteration(JunkEnds, &TEM_base::_handleStatePosteriorEstimationAndWriting);

        // finalize
        _latentVariable.finalizeStatePosteriorEstimationAndWriting();

        //report
        if(_writeLog){
            _logfile->list("Current LL = ", LL, ".");
            _logfile->endIndent();
        }

        //close file
        _out.close();

        return LL;
    };

public:
    TEM_base(TLatentVariable<PrecisionType, NumStatesType, LengthType> & LatentVariable,
             TParameters & Parameters,
             TLog* Logfile):
            _latentVariable(LatentVariable){
        _logfile = Logfile;
        _writeLog = true;

        _initFromParameters(Parameters);
        _reportParameters();

        //other settings (potentially overwritten by derived classes)
        _EMEstimatePrior = true;
    };

    TEM_base(TLatentVariable<PrecisionType, NumStatesType, LengthType> & LatentVariable,
             TParameters & Parameters):
            _latentVariable(LatentVariable){
        _logfile = nullptr;
        _writeLog = false;

        _initFromParameters(Parameters);

        //other settings (potentially overwritten by derived classes)
        _EMEstimatePrior = true;
    };

    TEM_base(TLatentVariable<PrecisionType, NumStatesType, LengthType> & LatentVariable,
             const uint16_t & EMMaxNumIterations,
             const double & EMMinDeltaLL,
             const bool & EMEstimatePrior):
            _latentVariable(LatentVariable){
        _logfile = nullptr;
        _writeLog = false;

        _EMNumIterations = EMMaxNumIterations;
        _EMMinDeltaLL = EMMinDeltaLL;
        _EMEstimatePrior = EMEstimatePrior;
    };

    TEM_base(TLatentVariable<PrecisionType, NumStatesType, LengthType> & LatentVariable):
            _latentVariable(LatentVariable){
        _logfile = nullptr;
        _writeLog = false;

        _EMNumIterations = 0;
        _EMMinDeltaLL = 0.0;
        _EMEstimatePrior = false;
    };

    virtual ~TEM_base() = default;

    void setEstimatePrior(const bool & EMEstimatePrior){
    	_EMEstimatePrior = EMEstimatePrior;
    };

    void makeSilent(){
        _writeLog = false;
    };

    void report(TLog* Logfile){
        _logfile = Logfile;
        _writeLog = true;
    };
};

//-------------------------------------
// TEM
//-------------------------------------
template <typename PrecisionType, typename NumStatesType, typename LengthType> class TEM : public TEM_base<PrecisionType, NumStatesType, LengthType> {
private:
    TEMWeights<PrecisionType, NumStatesType, LengthType> _weights;
    TEMPriorIID_base<PrecisionType, NumStatesType, LengthType>& _EMPrior;

    using TEM_base<PrecisionType,NumStatesType,LengthType>::_latentVariable;
    using TEM_base<PrecisionType,NumStatesType,LengthType>::_writeLog;
    using TEM_base<PrecisionType,NumStatesType,LengthType>::_logfile;

protected:
    // function to run EM
    PrecisionType _runEMOneIteration_OneJunk(const LengthType & First, const LengthType & Last, void (TEM_base<PrecisionType,NumStatesType,LengthType>::*handle)(const LengthType &)) override{
        if(_writeLog){
            _logfile->listFlushTime("Running EM ...");
        }
        //calculate first alpha
        THMMEmission<PrecisionType, NumStatesType> emission(_EMPrior.numStates());
        _latentVariable.calculateEmissionProbabilities(First, emission);
        _weights.startCalculationsForwards(_EMPrior, emission);
        (this->*handle)(First);

        //loop across others
        for(LengthType i = First + 1; i < Last; ++i){
            //move
            _latentVariable.calculateEmissionProbabilities(i, emission);
            _weights.moveCalculationsForward(i, _EMPrior, emission);
            (this->*handle)(i);
        }

        if(_writeLog){
            _logfile->doneTime();
        }

        return _weights.LL();
    };

    // function to calculate likelihood
    PrecisionType _calculateLL_OneJunk(const LengthType & First, const LengthType & Last){
        if(_writeLog){
            _logfile->listFlushTime("Running EM ...");
        }

        THMMEmission<PrecisionType, NumStatesType> emission(_EMPrior.numStates());
        _latentVariable.calculateEmissionProbabilities(First, emission);
        _weights.startCalculationsForwards(_EMPrior, emission);

        //loop across others
        for(LengthType i = First + 1; i < Last; ++i){
            //move
            _latentVariable.calculateEmissionProbabilities(i, emission);
            _weights.moveCalculationsForward(i, _EMPrior, emission);
        }

        if(_writeLog){
            _logfile->doneTime();
        }

        return _weights.LL();
    };

    // handle functions
    void _handleStatePosteriorEstimation(const LengthType & Index) override {
        _latentVariable.handleStatePosteriorEstimation(Index, _weights.weights());
    };

    void _handleStatePosteriorEstimationAndWriting(const LengthType & Index) override {
        _latentVariable.handleStatePosteriorEstimationAndWriting(Index, _weights.weights(), this->_out);
    };

    void _handleEMParameterEstimationOneIteration(const LengthType & Index) override{
        if(this->_EMEstimatePrior){
            _EMPrior.handleEMParameterEstimationOneIteration(Index, _weights.weights());
        }
        _latentVariable.handleEMParameterEstimationOneIteration(Index, _weights.weights());
    };

public:
    TEM(TEMPriorIID_base<PrecisionType, NumStatesType, LengthType> & EMPrior,
        TLatentVariable<PrecisionType, NumStatesType, LengthType> & LatentVariable,
        TParameters & Parameters,
        TLog* Logfile)
        : TEM_base<PrecisionType,NumStatesType,LengthType>(LatentVariable, Parameters, Logfile),
                _EMPrior(EMPrior){};

    TEM(TEMPriorIID_base<PrecisionType, NumStatesType, LengthType> & EMPrior,
        TLatentVariable<PrecisionType, NumStatesType, LengthType> & LatentVariable,
        TParameters & Parameters)
        : TEM_base<PrecisionType,NumStatesType,LengthType>(LatentVariable, Parameters),
          _EMPrior(EMPrior){};


    TEM(TEMPriorIID_base<PrecisionType, NumStatesType, LengthType> & EMPrior,
        TLatentVariable<PrecisionType, NumStatesType, LengthType> & LatentVariable,
        const uint16_t & EMMaxNumIterations,
        const double & EMMinDeltaLL,
        const bool & EMEstimatePrior)
        : TEM_base<PrecisionType,NumStatesType,LengthType>(LatentVariable, EMMaxNumIterations, EMMinDeltaLL, EMEstimatePrior),
          _EMPrior(EMPrior){};

    TEM(TEMPriorIID_base<PrecisionType, NumStatesType, LengthType> & EMPrior,
        TLatentVariable<PrecisionType, NumStatesType, LengthType> & LatentVariable)
        : TEM_base<PrecisionType,NumStatesType,LengthType>(LatentVariable),
          _EMPrior(EMPrior){};

    PrecisionType runEM(const std::vector<LengthType> & JunkEnds) {
        return this->_runEM(JunkEnds, _EMPrior);
    }

    PrecisionType calculateLL(const std::vector<LengthType> & JunkEnds) {
        return this->_calculateLL(JunkEnds);
    }

    PrecisionType estimateStatePosteriors(const std::vector<LengthType> & JunkEnds){
        return this->_estimateStatePosteriors(JunkEnds, _EMPrior);
    }

    PrecisionType estimateStatePosteriors(const std::vector<LengthType> & JunkEnds, const std::string & Filename){
        return this->_estimateStatePosteriors(JunkEnds, Filename, _EMPrior);
    }
};

#endif //EMPTY_TEM_H
