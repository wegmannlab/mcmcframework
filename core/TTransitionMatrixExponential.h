//
// Created by madleina on 31.07.20.
//

#ifndef MCMCFRAMEWORK_THMM_H
#define MCMCFRAMEWORK_THMM_H

#include <cmath>
#include <vector>
#include "TMatrix.h"
#include <memory>
#include "THMM.h"
#include "TNelderMead.h"
#include "TDistances.h"
#include "TNewtonRaphson.h"

#ifdef WITH_ARMADILLO
#include <armadillo>
#endif

//-------------------------------------------
// TGeneratingMatrixBase
//-------------------------------------------
template <typename PrecisionType, typename NumStatesType>
class TGeneratingMatrixBase {
    // pure virtual base class for generating matrices
    // stores generating matrix
    // provides all functions for interface
protected:
    // generating matrix
    TBandMatrix _generatingMatrix;

public:
    TGeneratingMatrixBase();
    TGeneratingMatrixBase(const NumStatesType & NumStates, const NumStatesType & Bandwidth);
    virtual void resize(const NumStatesType & NumStates, const NumStatesType & Bandwidth);

    // fill generating matrix from values
    virtual void fillGeneratingMatrix(const std::vector<PrecisionType> & Values) = 0;

    // stationary
    virtual bool fillStationary(std::vector<PrecisionType> & Values);

    // get entries
    const PrecisionType& operator()(const NumStatesType & From, const NumStatesType & To) const;
    const TBandMatrix & getGeneratingMatrix() const;
    virtual const TMatrix & getUnparametrizedGeneratingMatrix() const;
};

template <typename PrecisionType, typename NumStatesType>
class TGeneratingMatrixBool : public TGeneratingMatrixBase<PrecisionType,NumStatesType> {
    // generating matrix with 2 states and bandwidth 1
    // Mat = (-Lambda1, Lambda1)
    //       (Lambda2, -Lambda2)

protected:
    using TGeneratingMatrixBase<PrecisionType,NumStatesType>::_generatingMatrix;

public:
    TGeneratingMatrixBool();
    void resize(const NumStatesType & NumStates, const NumStatesType & Bandwidth) override;

    // fill
    void fillGeneratingMatrix(const std::vector<PrecisionType> & Values) override;

    // stationary
    bool fillStationary(std::vector<PrecisionType> & Values) override;
};

template <typename PrecisionType, typename NumStatesType>
class TGeneratingMatrixLadder : public TGeneratingMatrixBase<PrecisionType,NumStatesType> {
    // generating matrix with N states and bandwidth 1
    // can stay, go up or go down (ladder-type transition matrix)
    // parameterized by one value (kappa)
    // Mat = kappa * (-1, 1, 0, 0, ..., 0, 0)
    //               (1, -2, 1, 0, ..., 0, 0)
    //               (0, 1, -2, 1, ..., 0, 0)
    //               (......................)
    //               (0, 0, 0, 0, ..., 1, -1)
protected:
    using TGeneratingMatrixBase<PrecisionType,NumStatesType>::_generatingMatrix;

    // unparametrized generating matrix (matrix without kappa)
    TMatrix _rawLambda;
    void _fillRawLambda(const NumStatesType &NumStates);

public:
    TGeneratingMatrixLadder();
    TGeneratingMatrixLadder(const NumStatesType & NumStates);
    void resize(const NumStatesType & NumStates, const NumStatesType & Bandwidth) override;

    // fill
    void fillGeneratingMatrix(const std::vector<PrecisionType> & Values) override;
    const TMatrix & getUnparametrizedGeneratingMatrix() const override;

    // stationary
    bool fillStationary(std::vector<PrecisionType> & Values) override;
};

//-------------------------------------------
// TTransitionMatrixExponentialBase
//-------------------------------------------
template <typename PrecisionType, typename NumStatesType, typename LengthType>
class TTransitionMatrixExponentialBase : public TTransitionMatrix_base<PrecisionType, NumStatesType, LengthType> {
    // base class for HMMs where the transition matrix Tau is given by
    // Tau = exp(d * Mat)
    // where Mat is a generating matrix (derived class of TGeneratingMatrixBase)
    // and d is some distance measure between two neighbouring points

    // this class stores two transition matrices
    // for MCMC: these are old and new transition matrices
    // for EM: old transition matrix is used to store EM sums for M-Step
protected:
    // _transitionMatrix is of size numDistanceGroups
    // _transitionMatrix[0] = stationary probabilities
    // _transitionMatrix[1] = transition probabilities for distance group 1 -> calculated in specific way
    // _transitionMatrix[2:end] = transition probabilities for all other distance groups, calculated as
    //                            _transitionMatrix[i] = _transitionMatrix[i-1] * _transitionMatrix[i-1]

    // declare member variables of base class
    using TTransitionMatrix_base<PrecisionType,NumStatesType,LengthType>::_numStates;

    // distances
    std::shared_ptr<TDistancesBinnedBase> _distances;

    // transition matrices, old and new
    std::unique_ptr<std::vector<TMatrix>> _transitionMatrices_old;
    std::unique_ptr<std::vector<TMatrix>> _transitionMatrices;

    // generating matrix
    std::unique_ptr<TGeneratingMatrixBase<PrecisionType,NumStatesType>> _generatingMatrix;

    // temporary variables for EM
    std::vector<std::vector<PrecisionType>> _EM_colSums;
    bool _firstIteration;
    bool _fixTransitionMatricesDuringEM;

    // report
    TLog * _logfile;

    // fill matrices (generating, transition and stationary)
    void _fillStationaryDistribution_SolveNormalEquations();
    void _fillStationaryDistribution();
    void _fillGeneratingMatrix(const std::vector<PrecisionType> & Values);
    virtual void _fillParametersBasedOnGeneratingMatrix(const std::vector<PrecisionType> & Values);
    void _fillTransitionProbabilities();
    void _storeGeneratingMatrix(const std::vector<PrecisionType> & Values);
    virtual std::vector<PrecisionType> _maximizeQFunction_GeneratingMatrix(const bool & LastIteration) = 0;

    // debug
    bool _checkRowSumTransitionMatrices() const;
    void _printTransitionMatrices();

public:
    TTransitionMatrixExponentialBase();
    TTransitionMatrixExponentialBase(NumStatesType numStates, const std::shared_ptr<TDistancesBinnedBase> & distances);

    // initialize
    void addGeneratingMatrix(std::unique_ptr<TGeneratingMatrixBase<PrecisionType,NumStatesType>> & GeneratingMatrix);
    void initialize(NumStatesType numStates);
    void initializeWithDistances(NumStatesType numStates, const std::shared_ptr<TDistancesBinnedBase> & distances);
    void report(TLog * LogFile);

    // Access the individual elements
    const TMatrix& operator[](const size_t & index) const;
    TMatrix& operator[](const size_t & index);
    const PrecisionType& operator()(const LengthType & Index, const NumStatesType & From, const NumStatesType & To) const override;

    // getters
    size_t numDistanceGroups() const;

    const PrecisionType& stationary(const NumStatesType & State) const override;

    // EM initialization
    void handleEMParameterInitializationTransitionProbabilities(const LengthType & Index, const NumStatesType & PreviousState, const NumStatesType & CurrentState) override;
    void finalizeEMParameterInitializationTransitionProbabilities() override;
    // EM
    void runEMEstimation(TLatentVariable<PrecisionType, NumStatesType, LengthType> & latentVariable, bool fixTransitionMatricesDuringEM, TLog * Logfile);
    void prepareEMParameterEstimationInitial() override;
    void prepareEMParameterEstimationOneIterationTransitionProbabilities() override;
    void handleEMParameterEstimationOneIterationTransitionProbabilities(const LengthType & Index, const THMMPosteriorXi<PrecisionType, NumStatesType , LengthType> & xi) override;
    void finalizeEMParameterEstimationOneIterationTransitionProbabilities() override;
    void finalizeEMParameterEstimationFinal() override;

    //Simulate
    void simulateDistances(const int & length, TRandomGenerator * RandomGenerator);
    NumStatesType sampleNextState(const LengthType & Index, const NumStatesType & State, TRandomGenerator & RandomGenerator) const override;
};

//-------------------------------------------
// TTransitionMatrixExponentialNelderMead
//-------------------------------------------

template <typename PrecisionType, typename NumStatesType, typename LengthType>
class TTransitionMatrixExponentialNelderMead : virtual public TTransitionMatrixExponentialBase<PrecisionType, NumStatesType, LengthType> {
protected:
    // declare member variables of base class
    using TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::_logfile;
    using TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::_numStates;
    using TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::_distances;
    using TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::_transitionMatrices;
    using TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::_transitionMatrices_old;
    using TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::_EM_colSums;
    using TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::_firstIteration;

    // settings: run Nelder-Mead in each EM iteration or only in very last one?
    bool _runNelderMeadInEachEMIteration;

    // temporary variables for Nelder-Mead
    TSimplex _simplexOfPreviousEMIteration;
    PrecisionType _initLogDisplacement;
    TVertex _initLogVertex;
    size_t _totalNumberOfFunctionEvaluationsNM;

    // optimize Q-function with Nelder-Mead
    std::vector<PrecisionType> _maximizeQFunction_GeneratingMatrix(const bool & LastIteration) override;

    // Nelder-Mead
    TVertex _runNelderMead_FirstEMIteration();
    TVertex _runNelderMead_OneEMIteration(bool AdjustTolerance, bool Report);
    PrecisionType _calcQTransitionMatrix_NelderMead(const std::vector<PrecisionType> & LogLambdas);
    void _printQSurface(const std::string & Filename);
    std::vector<PrecisionType> _runNelderMead(bool AdjustTolerance, bool Report);

public:

    TTransitionMatrixExponentialNelderMead();
    TTransitionMatrixExponentialNelderMead(NumStatesType numStates, const std::shared_ptr<TDistancesBinnedBase> & distances);

    void setInitialValuesNelderMead(const std::vector<PrecisionType> & InitVertex, const PrecisionType & InitDisplacement);
};

//-------------------------------------------
// TTransitionMatrixExponentialNewtonRaphson
//-------------------------------------------
template <typename PrecisionType, typename NumStatesType, typename LengthType>
class TTransitionMatrixExponentialNewtonRaphson : virtual public TTransitionMatrixExponentialBase<PrecisionType, NumStatesType, LengthType> {
protected:
    // declare member variables of base class
    using TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::_logfile;
    using TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::_numStates;
    using TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::_distances;
    using TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::_transitionMatrices;
    using TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::_transitionMatrices_old;
    using TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::_EM_colSums;
    using TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::_firstIteration;
    using TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::_generatingMatrix;

    // temporary variables
    size_t _totalNumberOfIterations;
    PrecisionType _previousKappa;
    PrecisionType _initMin;
    PrecisionType _initMax;
    PrecisionType _previousDeltaKappa;
    std::vector<TMatrix> _LambdaA;

    // settings for Newton-Raphson tuning
    double _factorMinMaxRange;
    double _factorPrecision;
    size_t _initMaxIterations;
    double _initEpsilon;

    // optimize Q-function with Newton-Raphson
    std::vector<PrecisionType> _maximizeQFunction_GeneratingMatrix(const bool & LastIteration) override;

    // Newton-Raphson
    void _initialize();
    PrecisionType _runNewtonRaphson(TNewtonRaphson & NewtonRaphson, const PrecisionType & Start, const PrecisionType & Min, const PrecisionType & Max);
    PrecisionType _runNewtonRaphson_FirstEMIteration();
    PrecisionType _runNewtonRaphson_OneEMIteration(bool AdjustTolerance, bool Report);
    void _fillMinMaxNewtonRaphson(PrecisionType & Min, PrecisionType & Max) const;
    PrecisionType _calcFirstDerivativeQTransitionMatrix(const PrecisionType & LogX);
    PrecisionType _calcSecondDerivativeQTransitionMatrix(const PrecisionType & LogX);

    // debugging
    void _printQSurface(const std::string & Filename);

public:
    TTransitionMatrixExponentialNewtonRaphson();
    TTransitionMatrixExponentialNewtonRaphson(NumStatesType numStates, const std::shared_ptr<TDistancesBinnedBase> & distances);

    // set value, min and max for Newton-Raphson of very first EM iteration
    // must be set!
    void setInitialValuesNewtonRaphson(const PrecisionType & InitVal, const PrecisionType & InitMin, const PrecisionType & InitMax);

    // setting for tuning Newton-Raphson
    // can be set if needed (but don't need to)
    void setFactorMinMaxRange(const double & Factor);
    void setFactorPrecision(const double & Factor);
    void setInitEpsilon(const double & Epsilon);
    void setInitMaxIterations(const size_t & MaxIterations);
};

#include "TTransitionMatrixExponential.tpp"

#endif //MCMCFRAMEWORK_THMM_H
