//
// Created by madleina on 08.02.21.
//

#ifndef EMPTY_TDAG_H
#define EMPTY_TDAG_H

#include <vector>
#include "TMCMCObservationsBase.h"

class TDAG {
protected:
    std::vector<std::shared_ptr<TMCMCObservationsBase>> _dag;

public:
    TDAG() = default;
    virtual ~TDAG() = default;

    // functions to build DAG
    bool nodeExists(const std::string & Name) const {
        for (auto & node : _dag){
            if (node->name() == Name){
                return true;
            }
        }
        return false;
    };

    void add(const std::shared_ptr<TMCMCObservationsBase> node){
        _dag.push_back(node);
    };

    // check functions
    void initializeStorage(){
        for (auto & node : _dag){
            node->initializeStorage();
        }
    }

    void checkForCompatibleTypes(bool throwIfNotCompatible){
        for (auto & node : _dag){
            node->typesAreCompatible(throwIfNotCompatible);
        }
    }

    // functions for initializing and updating DAG
    void estimateInitialPriorParameters(TLog * Logfile){
        // go left -> right through vector
        for (auto & node : _dag){
            node->estimateInitialPriorParameters(Logfile);
        }
    }

    void update(){
        // go left -> right through vector
        for (auto & node : _dag){
            node->updatePrior();
        }
    }

    // functions for simulating
    void simulate(TLog * Logfile, TParameters & Parameters){
        // go right -> left through vector
        for (auto node = _dag.rbegin(); node != _dag.rend(); node++ ) {
            (*node)->simulateUnderPrior(Logfile, Parameters);
        }
    }

    // debug functions
    std::vector<std::string> getNamesOfNodes(){
        std::vector<std::string> names;
        for (auto & node : _dag){
            names.push_back(node->name());
        }
        return names;
    }
};

#endif //EMPTY_TDAG_H
