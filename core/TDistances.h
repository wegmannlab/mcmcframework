//
// Created by madleina on 07.01.21.
//

#ifndef EMPTY_TDISTANCES_H
#define EMPTY_TDISTANCES_H

#include <vector>
#include "MCMCFrameworkVariables.h"
#include "TRandomGenerator.h"
#include "algorithmsAndVectors.h"

// Note: positions are stored as uint32_t
// as this should easily be sufficient for a human genome (max for uint32_t = 4'294'967'296; human genome = 3'100'000'000)
// if at some point a larger type is needed, we suggest to solve this with inheritance
// as this derived class would need to store uint64_t, but probably most positions are smaller than this -> some smart
// indexing/binning scheme using offsets would be cool)

class TPositionsRaw {
protected:
    // junks
    std::vector<std::string> _junkNames;
    // junk ends
    // -> these correspond to the indices of the positions, not to the actual positions themselves!
    std::vector<uint32_t> _junkEnds;

    // positions
    std::vector<uint32_t> _positions;

    // temporary while filling
    std::string _curJunkName;

    // some variables/functions for quick lookup
    bool _found;
    size_t _indexCurrentlyVisitedPosition;
    size_t _indexPreviouslyVisitedPosition;
    size_t _indexCurrentlyVisitedJunk;
    size_t _indexPreviouslyVisitedJunk;
    uint32_t _maxSearchDistance;
    bool _findJunk(const std::string & Junk);
    bool _findPositionAfter(const uint32_t & Position, const size_t & Start);
    bool _findPositionBefore(const uint32_t & Position, const size_t & Start);
    bool _findPosition(const uint32_t & Position);

    void _addJunk(const std::string &JunkName);
    virtual void _addPositionOnNewJunk(const size_t &Position);
    virtual void _addPositionOnExistingJunk(const size_t &Position);
    void _addToJunkEnds();

public:
    TPositionsRaw();

    // fill
    void add(const size_t & Position, const std::string & JunkName);
    void finalizeFilling();

    // get size
    size_t size() const;

    // exists / get index
    void setMaxSearchDistance(const size_t & MaxSearchDistance);
    bool exists(const uint32_t & Pos, const std::string & Junk);
    size_t getIndex(const uint32_t & Pos, const std::string & Junk) const;

    // get: linear index over all
    size_t getPosition(const size_t & Index) const;
    const std::string & getJunkName(const size_t & Index) const;

    // get junk ends
    template <typename T> std::vector<T> getJunkEnds() {
        // cast uint32_t to T
        std::vector<T> vec(_junkEnds.begin(), _junkEnds.end());
        return vec;
    }

    // get as string
    std::string getPositionJunkAsString(const size_t & Index, const std::string & Delimiter = ":") const;
    std::string getJunkPositionAsString(const size_t & Index, const std::string & Delimiter = ":") const;

    // simulate
    virtual void simulate(const std::vector<size_t> & JunkSizes, TRandomGenerator * RandomGenerator, double Lambda = 10);
};

//-------------------------------------------
// TDistanceGroup
//-------------------------------------------

struct TDistanceGroup{
    uint32_t min;
    uint32_t maxPlusOne;
    uint32_t distance;
    bool hasSites;
};

//-------------------------------------------
// TDistancesBinnedBase
//-------------------------------------------
class TDistancesBinnedBase : public TPositionsRaw {
    // untemplated base class
    // provides common interface (with size_t) for templated derived classes
protected:
public:
    TDistancesBinnedBase() : TPositionsRaw(){};
    virtual void initialize(size_t maxDist) = 0;

    // get distances
    virtual size_t numDistanceGroups() const = 0;
    virtual TDistanceGroup distanceGroup(const size_t & Group) const = 0;
    virtual bool groupHasSites(const size_t & Group) = 0;

    // get distances of loci
    virtual size_t size() const = 0;
    virtual size_t operator[](const size_t & l) const = 0;
};

//-------------------------------------------
// TDistancesBinned
//-------------------------------------------

template <class NumDistanceGroupsType>
class TDistancesBinned : public TDistancesBinnedBase {
    // templated distance class
    // stores a vector of NumDistanceGroupsType, corresponding to the distance group of each position
protected:
    //distances
    NumDistanceGroupsType _numDistanceGroups;
    std::vector<TDistanceGroup> _distanceGroups;
    std::vector<NumDistanceGroupsType> _groupMembership; //for each site, to which group does it belong?

    void _addPositionOnNewJunk(const size_t &Position) override;
    void _addPositionOnExistingJunk(const size_t &Position) override;
    void _store(const NumDistanceGroupsType& g);

    // simulate
    uint32_t _getRandomDistance(const double & lambda, TRandomGenerator * RandomGenerator);

public:
    TDistancesBinned();
    TDistancesBinned(size_t maxDist);
    void initialize(size_t maxDist) override;

    // get distances
    size_t numDistanceGroups() const override;
    TDistanceGroup distanceGroup(const size_t & Group) const override;
    bool groupHasSites(const size_t & Group) override;

    // get distances of loci
    size_t size() const override;
    size_t operator[](const size_t & l) const override;

    // simulate distances
    void simulate(const std::vector<size_t> & JunkSizes, TRandomGenerator * RandomGenerator, double Lambda = 10) override;
};

#include "TDistances.tpp"


#endif //EMPTY_TDISTANCES_H
