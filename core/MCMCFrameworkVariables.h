//
// Created by madleina on 01.02.21.
//

#ifndef EMPTY_MCMCFRAMEWORKVARIABLES_H
#define EMPTY_MCMCFRAMEWORKVARIABLES_H

#include <map>
#include <string>

//--------------------------------------------
// PRIORS
//--------------------------------------------

class MCMCPrior {
public:
    inline static const std::string uniform = "uniform";
    inline static const std::string normal = "normal";
    inline static const std::string exponential = "exponential";
    inline static const std::string chisq = "chisq";
    inline static const std::string beta = "beta";
    inline static const std::string binomial = "binomial";
    inline static const std::string bernouilli = "bernouilli";
    inline static const std::string dirichlet = "dirichlet";
    inline static const std::string hmm_bool = "hmm_bool";
    inline static const std::string hmm_ladder = "hmm_ladder";
    inline static const std::string categorical = "categorical";
    inline static const std::string normal_mixedModel = "normal_mixedModel";
    inline static const std::string normal_mixedModel_stretch = "normal_mixedModel_stretch";
    inline static const std::string multivariateNormal = "multivariateNormal";
    inline static const std::string multivariateChisq = "multivariateChisq";
    inline static const std::string multivariateChi = "multivariateChi";
    inline static const std::string multivariateNormal_mixedModel = "multivariateNormal_mixedModel";
    inline static const std::string multivariateNormal_mixedModel_stretch = "multivariateNormal_mixedModel_stretch";
};

//--------------------------------------------
// PROPOSAL KERNELS
//--------------------------------------------

namespace ProposalKernel {
    enum MCMCProposalKernel : size_t {normal, uniform, integer, boolean};


    static std::map<MCMCProposalKernel, std::string> proposalKernelToString = {
            {normal,                                "normal"},
            {uniform,                               "uniform"},
            {integer,                               "integer"},
            {boolean,                               "boolean"}
    };

    MCMCProposalKernel stringToProposalKernel(const std::string &String);
}

//--------------------------------------------
// MCMC FILES
//--------------------------------------------

namespace MCMCFile {
    enum Filetypes : size_t {trace, meanVar, simulated};

    static std::map<Filetypes, std::string> fileTypeToString = {
            {trace,                                "trace"},
            {meanVar,                              "meanVar"},
            {simulated,                            "simulated"},
    };

    Filetypes stringToFileType(const std::string &String);
}

//--------------------------------------------
// TRANSLATORS (for observations)
//--------------------------------------------
namespace Translator {
    enum Translator : size_t {phred};

    static std::map<Translator, std::string> translatorToString = {
            {phred,                                "phred"}
    };

    Translator stringToTranslator(const std::string &String);
}

#endif //EMPTY_MCMCFRAMEWORKVARIABLES_H
