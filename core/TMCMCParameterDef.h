//
// Created by madleina on 11.12.20.
//

#ifndef MCMCFRAMEWORK_TMCMCPARAMETERDEF_H
#define MCMCFRAMEWORK_TMCMCPARAMETERDEF_H

#include "MCMCFrameworkVariables.h"
#include "stringFunctions.h"
#include <numeric>

//-------------------------------------------
// TDefinitionBase
//-------------------------------------------

class TDefinitionBase {
    // pure virtual base class that provides a common interface
    // for definitions of MCMC parameters and observations
protected:
    // name of observation
    std::string _name;
    // parameter or observation?
    bool _isObservation;
    // prior and prior parameters
    std::string _prior;
    std::string _parameters;
    bool _priorParametersAreFix;
    // type (double / float / int / bool)
    std::string _type;
    // updating schemes
    bool _trackExponential;
    bool _trackLog;
    // min max boundaries
    std::string _min;
    std::string _max;
    bool _min_included;
    bool _max_included;
    bool _hasDefaultMin;
    bool _hasDefaultMax ;
    // number of dimensions of parameter
    bool _hasDefaultDimensions;
    std::vector<size_t> _dimensions;
    // file for writing simulated values
    std::string _simulationFile;

public:
    TDefinitionBase(std::string Name);
    virtual ~TDefinitionBase() = 0;
    std::string name() const;
    std::string type() const;
    // parameter or observation?
    bool isObserved() const;
    void reSetObserved(const std::string& observed);
    // size
    void setDimensions(const std::vector<size_t> & dimensions);
    void setType(const std::string & type);
    // prior and prior parameters
    void setPrior(std::string Prior);
    void reSetPrior(const std::string& Prior);
    std::string prior() const;
    void setPriorParams(std::string Params);
    void reSetPriorParams(std::string Params);
    std::string parameters() const;
    bool priorParametersAreFix() const;

    // track exp(x) and/or log(x)?
    void trackExponential(bool Track);
    bool tracksExponential() const;
    void trackLog(bool Track);
    bool tracksLog() const;
    // min max
    void setMin(std::string Min, bool MinIsIncluded);
    void setMin(std::string Min);
    void setMinIncluded(bool MinIsIncluded);
    void setMax(std::string Max, bool MaxIsIncluded);
    void setMax(std::string Max);
    void setMaxIncluded(bool MaxIsIncluded);
    bool hasDefaultMin() const;
    bool hasDefaultMax() const;
    std::string min() const;
    std::string max() const;
    bool minIncluded() const;
    bool maxIncluded() const;
    // dimensions
    std::vector<size_t> dimensions() const;
    bool hasDefaultDimensions() const;
    size_t totalSize() const;
    // files for simulating
    void setSimulationFile(const std::string & Filename);
    std::string simulationFile() const;
};

//-------------------------------------------
// TParameterDefinition
//-------------------------------------------

class TParameterDefinition : public TDefinitionBase {
protected:
    // in addition to stuff that TDefinitionBase stores,
    // this class knows if and how to update (proposal kernels etc.) and to write to files
    // -> only parameter needs to know this

    // updating schemes
    bool _isUpdated;

    // files
    bool _storeMeanVar;
    std::string _meanVarFile;
    std::string _traceFile;

    // propKernel
    std::string _initPropKernel;
    ProposalKernel::MCMCProposalKernel _propKernelDistr;
    bool _oneJumpSizeForAll;
    // initial values
    bool _hasDefaultInitVal;
    std::string _initVal;

public:
    TParameterDefinition(std::string Name);
    virtual ~TParameterDefinition();
    // updating schemes
    void update(bool IsUpdated);
    bool isUpdated() const;

    // files
    void setMeanVarFile(const std::string & filename);
    bool storeMeanVar() const;
    std::string meanVarFile() const;
    void setTraceFile(const std::string & filename);
    std::string traceFile() const;

    // propKernel
    void setJumpSizeForAll(bool oneJumpSizeForAll);
    void setInitJumpSizeProposal(const std::string & init);
    void setPropKernel(std::string distr);
    void setPropKernel(const ProposalKernel::MCMCProposalKernel & distr);
    std::string initJumpSizeProposal() const;
    ProposalKernel::MCMCProposalKernel propKernel() const;
    bool oneJumpSizeForAll() const;
    // initial values
    void setInitVal(std::string value);
    std::string initVal() const;
    bool hasDefaultInitVal() const;
};


//-------------------------------------------
// TObservationDefinition
//-------------------------------------------

class TObservationDefinition : public TDefinitionBase {
protected:
    // in addition to stuff that TDefinitionBase stores,
    // this class knows if and how to translate values

    // translate values?
    bool _translate;
    std::string _storedType;
    Translator::Translator _translator;
    double _maxStoredValue;

public:
    TObservationDefinition(std::string Name);
    virtual ~TObservationDefinition();

    // translator
    void translate(const Translator::Translator & Translator, const std::string & StoredType, const double & maxStoredValue);
    bool isTranslated() const;
    Translator::Translator translator() const;
    std::string storedType() const;
    double maxStoredValue() const;
};


#endif //MCMCFRAMEWORK_TMCMCPARAMETERDEF_H
