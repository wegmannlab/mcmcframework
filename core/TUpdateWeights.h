//
// Created by madleina on 14.05.21.
//

#ifndef BANGOLIN_TUPDATEWEIGHTS_H
#define BANGOLIN_TUPDATEWEIGHTS_H

#include "TRandomGenerator.h"
#include "TLog.h"

//-------------------------------------------
// TUpdateWeights
//-------------------------------------------
template <class TYPE>
class TUpdateWeights {
protected:
    TRandomGenerator * _randomGenerator;
    size_t _size;
    std::vector<TYPE> _cumWeights;

    // normalize and sum weights
    double _calculateAbsoluteSum(const std::vector<TYPE> & Vec);
    void _normalize_AndFillCumulativeSum(const double & Sum, const std::vector<TYPE> & Vec, std::vector<TYPE> & CumSum);

public:
    TUpdateWeights();
    TUpdateWeights(TRandomGenerator* RandomGenerator);
    void addRandomGenerator(TRandomGenerator* RandomGenerator);


    void initialize(const std::vector<TYPE> & Weights);

    // selection of index according to weights
    size_t pick();
};

#include "TUpdateWeights.tpp"

#endif //BANGOLIN_TUPDATEWEIGHTS_H
