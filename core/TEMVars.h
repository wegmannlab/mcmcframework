//
// Created by madleina on 13.01.21.
//

#ifndef EMPTY_TEMVARS_H
#define EMPTY_TEMVARS_H

#include "TDataVector.h"
#include "TEMPriorIID.h"

//-------------------------------------
// THMMEmission
//-------------------------------------
template <typename PrecisionType, typename NumStatesType> using THMMEmission = TDataVector<PrecisionType, NumStatesType>;

//-------------------------------------
// TEMWeights
//-------------------------------------
template <typename PrecisionType, typename NumStatesType, typename LengthType> class TEMWeights {
    // equivalent class to THMMForwardBackward, but only for "simple" EM Weights (without HMM)
private:
    // weights
    TDataVector<PrecisionType, NumStatesType> _weights;

    // log likelihood
    PrecisionType _LL;

protected:
    // loop
    void _moveCalculationsForward(const LengthType & Index, const TEMPriorIID_base<PrecisionType, NumStatesType, LengthType> & EMPrior, const THMMEmission<PrecisionType, NumStatesType> & EmissionProbs){
        //calculate weights at current Index: p_gi = (Emission_gi * Prior_gi) / (sum_h Emission_hi * Prior_hi)
        PrecisionType sumToNormalize = 0.0;
        const NumStatesType numStates = _weights.size();
        for(NumStatesType i = 0; i < numStates; ++i){
            _weights[i] = EmissionProbs[i] * EMPrior[i];
            sumToNormalize += _weights[i];
        }

        //normalize
        _weights.normalize(sumToNormalize);

        //add to LL
        _LL += log(sumToNormalize);
    };

    void _startCalculations(const TEMPriorIID_base<PrecisionType, NumStatesType, LengthType> & EMPrior, const THMMEmission<PrecisionType, NumStatesType> & EmissionProbs){
        _LL = 0.0;
        _moveCalculationsForward(0, EMPrior, EmissionProbs); // TODO: INDEX 0 OK?
    };


public:
    TEMWeights(){
        _LL = 0.0;
    };

    ~TEMWeights() = default;

    PrecisionType LL() const { return _LL; };

    void startCalculationsForwards(const TEMPriorIID_base<PrecisionType, NumStatesType, LengthType> & EMPrior, const THMMEmission<PrecisionType, NumStatesType> & EmissionProbs){
        //ensure size
        _weights.resize(EMPrior.numStates());

        //start!
        _startCalculations(EMPrior, EmissionProbs);
    };

    void moveCalculationsForward(const LengthType & Index, const TEMPriorIID_base<PrecisionType, NumStatesType, LengthType> & EMPrior, const THMMEmission<PrecisionType, NumStatesType> & EmissionProbs){
        _moveCalculationsForward(Index, EMPrior, EmissionProbs);
    };

    const TDataVector<PrecisionType, NumStatesType>& weights() const{
        return _weights;
    };
};

#endif //EMPTY_TEMVARS_H
