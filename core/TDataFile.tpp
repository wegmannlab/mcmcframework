//
// Created by madleina on 20.04.21.
//

template <typename T>
std::vector<T> TDataFileBase::_getAllExceptFirst(const std::vector<T> &Vec){
    // remove first (most outer one) from vector
    std::vector<T> oneBlock = Vec;
    oneBlock.erase(oneBlock.begin());

    return oneBlock;
}

//--------------------
// TDataWriterBase
//--------------------

template <typename TYPE> TDataWriterBase<TYPE>::TDataWriterBase(){
    _filePointer = nullptr;
    _isOpen = false;
}

template <typename TYPE> TDataWriterBase<TYPE>::~TDataWriterBase(){
   _closeFile();
}

template <typename TYPE> void TDataWriterBase<TYPE>::_openFile(){
    std::string ending = readAfterLast(_filename, '.');
    if(ending == "gz"){
        _filePointer = new gz::ogzstream(_filename.c_str());
    } else {
        _filePointer = new std::ofstream(_filename.c_str());
    }
    _isOpen = true;

    if(!(*_filePointer) || _filePointer->fail() || !_filePointer->good()){
        throw "Failed to open file '" + _filename + "' for writing!";
    }
}

template <typename TYPE> void TDataWriterBase<TYPE>::_closeFile() {
    if (_isOpen) {
        delete _filePointer;
        _isOpen = false;
    }
}

template <typename TYPE> void TDataWriterBase<TYPE>::write(const std::vector<std::shared_ptr<TNamesEmpty>> & Names,
                            const std::string & Filename,
                            const std::shared_ptr<TMCMCObservationsBase> &Observation,
                            const std::vector<char> & Delimiters,
                            const std::vector<bool> & NameIsWritten,
                            const colNameTypes & ColNameType) {
    // names are given as vector
    // initialize and write
    _initialize(Names, Filename, Observation, Delimiters, NameIsWritten, ColNameType);
}

template <typename TYPE> void TDataWriterBase<TYPE>::write(const std::string & Filename,
                        const std::shared_ptr<TMCMCObservationsBase> &Observation,
                        const std::vector<char> & Delimiters,
                        const std::vector<bool> & NameIsWritten,
                        const colNameTypes & ColNameType) {
    // extract names from observation
    std::vector<std::shared_ptr<TNamesEmpty>> names = _extractNamesFromObservations(Observation, Observation->numDim());
    // now initialize and write
    _initialize(names, Filename, Observation, Delimiters, NameIsWritten, ColNameType);
}

template <typename TYPE> void TDataWriterBase<TYPE>::_initialize(const std::vector<std::shared_ptr<TNamesEmpty>> & Names,
                                  const std::string & Filename,
                                  const std::shared_ptr<TMCMCObservationsBase> &Observation,
                                  const std::vector<char> & Delimiters,
                                  const std::vector<bool> & NameIsWritten,
                                  const colNameTypes & ColNameType){
    // set member variables
    _filename = Filename;
    _observation = Observation;
    _delimiters = Delimiters;
    _numDim = _observation->numDim();

    // initialize row- and colNames
    _rowNames = std::make_unique<TRowNames>(_numDim, NameIsWritten, Names, _delimiters, _filename);
    if (ColNameType == colNames_multiline) {
        _colNames = std::make_unique<TColNameMultiLine>(_numDim, NameIsWritten, Names, _delimiters, _filename);
    } else if (ColNameType == colNames_concatenated){
        _colNames = std::make_unique<TColNameConcatenated>(_numDim, NameIsWritten, Names, _delimiters, _filename);
    } else {
        _colNames = std::make_unique<TColNameFull>(_numDim, NameIsWritten, Names, _delimiters, _filename);
    }
    _colNames->initialize(_delimiters);

    // check on valid input
    _checkInput();

    // open file
    _openFile();

    // write header
    _writeHeader();

    // write rest
    _write();

    // close
    _closeFile();
}

template <typename TYPE> void TDataWriterBase<TYPE>::_checkInput(){
    this->_checkDelimiters();
    _rowNames->checkIfDimensionMatchSizeName(_filename, _observation->dimensions());
    _colNames->checkIfDimensionMatchSizeName(_filename, _observation->dimensions());
}

template <typename TYPE> std::vector<std::shared_ptr<TNamesEmpty>> TDataWriterBase<TYPE>::_extractNamesFromObservations(const std::shared_ptr<TMCMCObservationsBase> & Observation, const size_t & NumDim) {
    // fill pointer to name class from observation into vector
    // reason: at some point, we might want to use other name classes than the one the observation stores for writing
    // -> just need to replace element in vector
    std::vector<std::shared_ptr<TNamesEmpty>> names(NumDim);

    for (size_t dim = 0; dim < NumDim; dim++){
        names[dim] = Observation->getDimensionName(dim);
    }

    return names;
}

template <typename TYPE> void TDataWriterBase<TYPE>::_writeHeader() {
    _colNames->writeHeader(_filePointer, _observation->dimensions(), _delimiters, _rowNames);
}

template <typename TYPE> void TDataWriterBase<TYPE>::_write() {
    // construct delimiters and coordinates for one block (= all except most outer dimension)
    std::vector<size_t> dimensionsOneBlock = this->_getAllExceptFirst(_observation->dimensions());
    std::vector<char> delimitersOneBlock = this->_getAllExceptFirst(_delimiters);

    // create lookup tables for delimiters and coordinates
    std::vector<char> delimitersLookupOneBlock = TDataBlock::getDelimiterLookupOneBlock(delimitersOneBlock, dimensionsOneBlock, _delimiters[0]);
    std::vector<std::vector<size_t>> coordinatesOneBlock = TDataBlock::getCoordinatesOneBlock(dimensionsOneBlock);

    _write(dimensionsOneBlock, delimitersOneBlock, delimitersLookupOneBlock, coordinatesOneBlock);
}

template <typename TYPE> void TDataWriterBase<TYPE>::_ensureLastBlockEndsWithNewline(const size_t & CurBlock, const size_t & BlockSize, std::vector<char> &DelimitersLookupOneBlock){
    size_t lastBlock = _observation->dimensions()[0]-1;
    if (CurBlock == lastBlock){
        DelimitersLookupOneBlock[BlockSize-1] = '\n';
    }
}

template <typename TYPE> void TDataWriterBase<TYPE>::_write(const std::vector<size_t> & BlockDimensions, const std::vector<char> & BlockDelimiters,
                             std::vector<char> &DelimitersLookupOneBlock,
                             const std::vector<std::vector<size_t>> &CoordinatesOneBlock) {
    // write file
    size_t blockSize = DelimitersLookupOneBlock.size();
    bool newLine = true;
    for (size_t block = 0; block < _observation->dimensions()[0]; block++) {
        // new block starts
        _ensureLastBlockEndsWithNewline(block, blockSize, DelimitersLookupOneBlock);

        for (size_t i = 0; i < blockSize; i++) {
            // get full vector of coordinates
            std::vector<size_t> fullCoordinates = CoordinatesOneBlock[i];
            fullCoordinates.insert(fullCoordinates.begin(), block);

            if (newLine) {
                // write rownames
                _rowNames->write(_filePointer, fullCoordinates);
                newLine = false;
            }

            // get value of observation and delimiter
            auto value = _observation->value<TYPE>(_observation->getIndex(fullCoordinates));
            char delimiter = DelimitersLookupOneBlock[i];

            // write
            *_filePointer << value << delimiter;

            if (delimiter == '\n'){
                // next line will be a newline
                newLine = true;
            }
        }
    }
}

//--------------------
// TDataReaderBase
//--------------------

template <typename TYPE>TDataReaderBase<TYPE>::TDataReaderBase() {
    _filePointer = nullptr;
    _isOpen = false;
    _delimiterComment = "//";
    _throwIfTitleDoesntMatch = false;
    _addedArtificialFirstDimension = false;
    _isZipped = false;
}

template <typename TYPE>TDataReaderBase<TYPE>::~TDataReaderBase() {
    _closeFile();
}

template <typename TYPE> void TDataReaderBase<TYPE>::_openFile() {
    //check if file is gzipped: ending must be .gz
    std::string ending = readAfterLast(_filename, '.');
    if(ending == "gz"){
        _isZipped = true;
        _filePointer = new gz::igzstream(_filename.c_str());
    } else {
        _filePointer = new std::ifstream(_filename.c_str());
    }
    _isOpen = true;

    if(!(*_filePointer) || _filePointer->fail() || !_filePointer->good()){
        throw "Failed to open file '" + _filename + "' for reading! Does the file exists?";
    }
}

template <typename TYPE> void TDataReaderBase<TYPE>::_restartAtBeginning(){
    // will set the filePointer to the very beginning of the file
    if (_isZipped){
        // seekg is not supported in by gzstream -> close and re-open file
        _closeFile();
        _openFile();
    } else {
        _filePointer->clear();
        _filePointer->seekg(0);
    }
}

template <typename TYPE> void TDataReaderBase<TYPE>::_closeFile(){
    if(_isOpen){
        delete _filePointer;
        _isOpen = false;
    }
}

template <typename TYPE> void TDataReaderBase<TYPE>::_initialize(const std::string &Filename, const size_t &NumDim,
                                  const std::shared_ptr<TObservationsBase> &Observation,
                                  const std::vector<std::shared_ptr<TNamesEmpty>> &Names, const std::vector<char> &Delimiters,
                                  const std::vector<bool> &NameIsWritten,
                                  std::vector<size_t> &Dimensions,
                                  std::vector<bool> &DimensionsFilled,
                                  const size_t & GuessLengthUnknownDimension,
                                  const colNameTypes & ColNameType){

    // set member variables
    _filename = Filename;
    _numDim = NumDim;
    _observation = Observation;
    _delimiters = Delimiters;

    // initialize row- and colNames
    _rowNames = std::make_unique<TRowNames>(_numDim, NameIsWritten, Names, _delimiters, _filename);
    if (ColNameType == colNames_multiline) {
        _colNames = std::make_unique<TColNameMultiLine>(_numDim, NameIsWritten, Names, _delimiters, _filename);
    } else if (ColNameType == colNames_concatenated){
        _colNames = std::make_unique<TColNameConcatenated>(_numDim, NameIsWritten, Names, _delimiters, _filename);
    } else {
        _colNames = std::make_unique<TColNameFull>(_numDim, NameIsWritten, Names, _delimiters, _filename);
    }
    _colNames->initialize(_delimiters);
}

template <typename TYPE> void TDataReaderBase<TYPE>::read(const std::string &Filename, size_t NumDim,
                           const std::shared_ptr<TObservationsBase> &Observation,
                           std::vector<std::shared_ptr<TNamesEmpty>> Names,
                           std::vector<char> Delimiters,
                           std::vector<bool> NameIsWritten,
                           std::vector<size_t> Dimensions,
                           std::vector<bool> DimensionsFilled,
                           size_t GuessLengthUnknownDimension,
                           const colNameTypes & ColNameType,
                           const bool & ReadDataRaw) {

    if (NumDim == 1 && Delimiters[0] == '\t'){
        // for 1D layout: if all entries are written on one row, we have several problems (e.g. blockSize = 1, but then
        // we cannot skip & reshuffle etc) -> just add extra dimension of length 1 to the beginning; it makes everything
        // much easier & will be removed in the end from the observation!
        _addedArtificialFirstDimension = true;
        Names.insert(Names.begin(), std::make_shared<TNamesEmpty>());
        Dimensions.insert(Dimensions.begin(), 1);
        Delimiters.insert(Delimiters.begin(), '\n');
        DimensionsFilled.insert(DimensionsFilled.begin(), true);
        NameIsWritten.insert(NameIsWritten.begin(), false);
        NumDim = 2;
    }

    // set all member variables
    _initialize(Filename, NumDim, Observation, Names, Delimiters, NameIsWritten, Dimensions, DimensionsFilled, GuessLengthUnknownDimension, ColNameType);


    // check on valid input
    _checkInput(Dimensions, DimensionsFilled);

    // open file
    _openFile();

    // infer all dimensions needed to parse file
    _inferDimensions(Dimensions, DimensionsFilled, GuessLengthUnknownDimension);

    // set file pointer to begin of file
    _restartAtBeginning();

    // read again, now with known dimensions -> read and store data
    _read(Dimensions, GuessLengthUnknownDimension, ReadDataRaw);
}

template <typename TYPE> void TDataReaderBase<TYPE>::throwIfTitleDoesntMatch(const bool &ThrowIfTitleDoesntMatch) {
    _throwIfTitleDoesntMatch = ThrowIfTitleDoesntMatch;
}

template <typename TYPE> void TDataReaderBase<TYPE>::_checkInput(std::vector<size_t> &Dimensions, std::vector<bool> &DimensionsFilled){
    this->_checkDelimiters();
    _checkDimensionsFromDeveloper(Dimensions, DimensionsFilled);
    _checkIfBlockSizeCanBeInferred(DimensionsFilled);
    for (size_t dim = 0; dim < _numDim; dim++) {
        if (_rowNames->nameIsRowName(dim)) {
            _rowNames->checkForValidPrefilledNames(dim);
        } else {
            _colNames->checkForValidPrefilledNames(dim);
        }
    }
}

template <typename TYPE> void TDataReaderBase<TYPE>::_checkDimensionsFromDeveloper(std::vector<size_t> &Dimensions, std::vector<bool> &DimensionsFilled){
    if (Dimensions.size() != _numDim){
        throw std::runtime_error("In function 'TDataReaderBase::_inferDimensions': Vector DimensionsFromDeveloper of size " + toString(Dimensions.size()) + " differs from expected size based on number of dimensions (" + toString(_numDim) + ").");
    }
    if (DimensionsFilled.size() != _numDim){
        throw std::runtime_error("In function 'TDataReaderBase::_inferDimensions': Vector DimensionsFilled of size " + toString(DimensionsFilled.size()) + " differs from expected size based on number of dimensions (" + toString(_numDim) + ").");
    }
}

template <typename TYPE> void TDataReaderBase<TYPE>::_checkIfBlockSizeCanBeInferred(const std::vector<bool> &DimensionsFilled){
    // for all dimension names with the same delimiter:
    // max. 1 can have unknown dimensions, all others must be known, otherwise we can't infer dimensions
    // dimensions are known if a) set by developer or b) written in header

    // initialize map with all delimiters that are being used
    std::map<char, size_t> numUnknownDimensions_PerDelim;
    for (size_t dim = 0; dim < _numDim; dim++) {
        numUnknownDimensions_PerDelim[_delimiters[dim]] = 0;
    }

    // now count: per delimiter, how many dimensions are unknown?
    for (size_t dim = 0; dim < _numDim; dim++){
        // name is known if:
        // 1) name is written as rowname or as colname
        // 2) or dimension length is set
        if (DimensionsFilled[dim] || _rowNames->nameIsWritten(dim) || _colNames->nameIsWritten(dim)) {
            continue; // ok, we know enough
        } else {
            numUnknownDimensions_PerDelim[_delimiters[dim]]++;
        }
    }

    // finally evaluate: is there any delimiter which has >1 unknown dimensions?
    for (auto & it : numUnknownDimensions_PerDelim){
        if (it.second > 1){
            throw std::runtime_error("In function 'TDataReaderBase::_checkIfBlockSizeCanBeInferred': Can not infer dimensions of file " + _filename + ". For delimiter " + it.first + ", " + toString(it.second) + " dimensions are unknown (while at max 1 can be unknown in order to infer dimensions.");
        }
    }
}

template <typename TYPE> void TDataReaderBase<TYPE>::_inferDimensions(std::vector<size_t> &Dimensions, std::vector<bool> &DimensionsFilled, size_t & GuessLengthFirstDimension){
    // infer other dimensions
    _readHeader(Dimensions, DimensionsFilled);
    _parseFirstLine(Dimensions, DimensionsFilled);

    // check if all but first dimension are known by now
    _finalCheck_InferDimensions(Dimensions, DimensionsFilled);

    // if first dimension is known: adjust GuessLengthFirstDimension
    if (DimensionsFilled[0]){
        GuessLengthFirstDimension = Dimensions[0];
    }
}

template <typename TYPE> std::vector<std::string> TDataReaderBase<TYPE>::_readHeaderIntoVector(){
    size_t numLines = _colNames->numLinesHeader();

    // read line by line and add to vector
    std::vector<std::string> fullHeader(numLines);
    for (size_t row = 0; row < numLines; row++){
        std::string line;
        if (!readUntilDelimiter(_filePointer, line, '\n', _delimiterComment)){
            throw "Reached end of file " + _filename + " while reading header. Expected " + toString(numLines) + " header lines, but reached end of file after " + toString(row+1) + " lines!";
        }
        fullHeader[row] = line;
    }
    return fullHeader;
}

template <typename TYPE> void TDataReaderBase<TYPE>::_readHeader(std::vector<size_t> &Dimensions, std::vector<bool> &DimensionsFilled) {
    std::vector<std::string> fullHeader = _readHeaderIntoVector();
    _colNames->parseHeader(fullHeader, Dimensions, DimensionsFilled, _delimiters, _rowNames, _throwIfTitleDoesntMatch);
}

template <typename TYPE> void TDataReaderBase<TYPE>::_parseFirstLine(std::vector<size_t> & Dimensions, std::vector<bool> & DimensionsFilled){
    // prepare storage for inferring dimensionality of column dimensions
    std::map<char, size_t> delimCounter;

    std::string line;
    bool firstLine = true;
    while(readUntilDelimiter(_filePointer, line, '\n', _delimiterComment)){
        // first parse rowNames
        _rowNames->parseRowNames_InferDimensions(line, Dimensions, DimensionsFilled, firstLine);

        // now parse data: count delimiters
        _countNumberDelims_oneLine(line, delimCounter, firstLine);

        // decide if we have all information we need
        if (_rowNames->allRowNameDimensionsAreKnown(DimensionsFilled)){
            break;
        }

        firstLine = false;
    }

    // infer dimension of columns
    _inferDimensions_oneLine(delimCounter, Dimensions, DimensionsFilled);
}

template <typename TYPE> void TDataReaderBase<TYPE>::_countNumberDelims_oneLine(const std::string & Line, std::map<char, size_t> & DelimCounter, const bool & FirstLine){
    // count the total number of occurrences per delimiter
    std::set<char> uniqueDelims(_delimiters.begin(), _delimiters.end());
    for (auto &delim : uniqueDelims) {
        size_t count = std::count(Line.begin(), Line.end(), delim);

        if (FirstLine) {
            // first line: store
            DelimCounter[delim] = count;
        } else {
            // all other lines: check if the same as first line
            if (DelimCounter[delim] != count){
                std::string delimString = {delim};
                throw "While reading file " + _filename + ": Count for delimiter " + delimString + " has already been set to " + toString(DelimCounter[delim]) + ", which differs from the length " + toString(count) + " that was inferred based on a subsequent line.";
            }
        }
    }
}

template <typename TYPE> size_t TDataReaderBase<TYPE>::_getProductOverAllDim_WithSameDelim(const size_t & Dim, const std::vector<size_t> & Dimensions, const std::vector<bool> & DimensionsFilled){
    size_t productAllOtherDimWithSameDelim = 1;
    for (size_t otherDim = 0; otherDim < _numDim; otherDim++) {
        if (otherDim != Dim && _delimiters[otherDim] == _delimiters[Dim]) {
            // other dimension with same delimiter
            if (!DimensionsFilled[otherDim]) {
                throw std::runtime_error("In function 'TDataReaderBase::_getProductOverAllDim_WithSameDelim': Should not get here! Dimension " + toString(otherDim) + " must be known in order to infer dimension " + toString(Dim) + "!");
            }
            productAllOtherDimWithSameDelim *= Dimensions[otherDim];
        }
    }

    return productAllOtherDimWithSameDelim;
}

template <typename TYPE> size_t TDataReaderBase<TYPE>::_removeAllUpperColumns_FromDelimCounter(const size_t & TotalCountsDelim, const size_t & Dim, const std::vector<size_t> & Dimensions, const std::vector<bool> & DimensionsFilled){
    // remove upper dimensions
    size_t countsWithoutRepetition = TotalCountsDelim;
    for (size_t previousDim = 0; previousDim < Dim; previousDim++){
        if (_colNames->nameIsColName(previousDim) && _delimiters[previousDim] != _delimiters[Dim]) {
            if (!DimensionsFilled[previousDim]){
                throw std::runtime_error("In function 'TDataReaderBase::_removeAllUpperColumns': Should not get here! Dimension " + toString(previousDim) + " must be known in order to infer dimension " + toString(Dim) + "!");
            }
            countsWithoutRepetition /= Dimensions[previousDim];
        }
    }

    return countsWithoutRepetition;
}

template <typename TYPE> void TDataReaderBase<TYPE>::_inferDimensions_oneLine(std::map<char, size_t> & DelimCounter, std::vector<size_t> & Dimensions, std::vector<bool> & DimensionsFilled){
    // infer dimensions of columns based on delimiters
    for (size_t dim = 0; dim < _numDim; dim++){
        if (_colNames->nameIsColName(dim) && !DimensionsFilled[dim]){
            size_t productAllOtherDimWithSameDelim = _getProductOverAllDim_WithSameDelim(dim, Dimensions, DimensionsFilled);

            size_t totalCounterDelim = DelimCounter[_delimiters[dim]];
            size_t counterDelimWithoutRepetition = _removeAllUpperColumns_FromDelimCounter(totalCounterDelim, dim, Dimensions, DimensionsFilled);
            size_t counterDim = counterDelimWithoutRepetition + 1; // we always have one more element than delimiters

            size_t length = counterDim / productAllOtherDimWithSameDelim;

            _colNames->setLengthUnnamedDimension(length, dim, Dimensions, DimensionsFilled);
        }
    }
}

template <typename TYPE> void TDataReaderBase<TYPE>::_finalCheck_InferDimensions(const std::vector<size_t> & Dimensions, const std::vector<bool> & DimensionsFilled){
    for (size_t dim = 1; dim < _numDim; dim++){
        if (!DimensionsFilled[dim]){
            throw std::runtime_error("In function 'TDataReaderBase::_parseFirstLine': Should not get here! Dimension " +
                                     toString(dim) + " is still unknown at the end of parsing header and first lines!");
        }
        if (Dimensions[dim] == 0){
            throw std::runtime_error("In function 'TDataReaderBase::_parseFirstLine': Should not get here! Dimension of " +
                                     toString(dim) + " is still zero at the end of parsing header and first lines!");
        }
    }
}


template <typename TYPE> void TDataReaderBase<TYPE>::_read(std::vector<size_t> & Dimensions, const size_t & GuessLengthUnknownDimension, const bool & ReadDataRaw) {
    // parse header again and check if colnames make sense with the dimensions that we've inferred
    std::vector<std::string> fullHeader = _readHeaderIntoVector();
    _colNames->checkIfHeaderMatchesInferredDimensions(fullHeader, Dimensions, _delimiters, _rowNames);

    // construct delimiters and coordinates for one block (= all except most outer dimension)
    std::vector<size_t> dimensionsOneBlock = this->_getAllExceptFirst(Dimensions);
    std::vector<char> delimitersOneBlock = this->_getAllExceptFirst(_delimiters);

    // create lookup tables for delimiters and coordinates
    std::vector<char> delimitersLookupOneBlock = TDataBlock::getDelimiterLookupOneBlock(delimitersOneBlock, dimensionsOneBlock, _delimiters[0]);
    std::vector<std::vector<size_t>> coordinatesOneBlock = TDataBlock::getCoordinatesOneBlock(dimensionsOneBlock);

    // translate these block coordinates to indices where to store each cell inside observation
    _translateBlockCoordinates_toIndexInObservation(dimensionsOneBlock, coordinatesOneBlock);

    // finally read!
    _read(dimensionsOneBlock, delimitersOneBlock, coordinatesOneBlock, delimitersLookupOneBlock, GuessLengthUnknownDimension, ReadDataRaw);
}

template <typename TYPE> void TDataReaderBase<TYPE>::_translateBlockCoordinates_toIndexInObservation(const std::vector<size_t> & BlockDimensions, const std::vector<std::vector<size_t>> &CoordinatesOneBlock) {
    // prepare storage:
    // per element in a cell: do we want to store it, and if yes, at what index inside one block?
    size_t numDim_Block = BlockDimensions.size();
    size_t blockSize = TDataBlock::getBlockSize(BlockDimensions);
    // what are the dimensions of the observation (not necessarily the same as dimensions in file, if elements are skipped)
    _dimensionsOfObservation.resize(_numDim);

    std::vector<std::vector<size_t>> coordinateToStore(blockSize, std::vector<size_t>(numDim_Block, 0));
    std::vector<std::vector<bool>> shouldBeStored(blockSize, std::vector<bool>(numDim_Block, false));
    // go over all cells of one block
    for (size_t i = 0; i < blockSize; i++) {
        // go over all dimensions of one block
        for (size_t blockDim = 0; blockDim < numDim_Block; blockDim++) {
            size_t actualDim = blockDim+1; // actual dimension is +1, because block leaves out first dimension
            // get index inside current dimension
            size_t indexInDim = CoordinatesOneBlock[i][blockDim];
            if (_rowNames->nameIsRowName(actualDim)) {
                // ask rowName
                if (_rowNames->elementIsStored(actualDim, indexInDim)) {
                    shouldBeStored[i][blockDim] = true;
                    coordinateToStore[i][blockDim] = _rowNames->indexToStoreElement(actualDim, indexInDim);
                }
            } else {
                // ask colName
                if (_colNames->elementIsStored(actualDim, indexInDim)) {
                    shouldBeStored[i][blockDim] = true;
                    coordinateToStore[i][blockDim] = _colNames->indexToStoreElement(actualDim, indexInDim);
                }
            }
            // infer dimensions of observation
            _dimensionsOfObservation[actualDim] = std::max(_dimensionsOfObservation[actualDim], coordinateToStore[i][blockDim]+1); // size of dimension is max index + 1
        }
    }

    // check if a dimension is fully omitted -> currently now allowed
    for (size_t dim = 1; dim < _numDim; dim++){
        if (_dimensionsOfObservation[dim] == 0){
            throw std::runtime_error("In function 'TDataReaderBase::_translateBlockCoordinates_toIndexInObservation': Dimension " +
                                             toString(dim) + " is fully omitted in file " + _filename + ". This feature is currently not implemented.");
        }
    }

    // now translate coordinates to a linear index
    _translateCoordinates_toIndexInObservation(blockSize, numDim_Block, shouldBeStored, coordinateToStore);
}

template <typename TYPE> void TDataReaderBase<TYPE>::_translateCoordinates_toIndexInObservation(const size_t & BlockSize,
                                                                              const size_t & NumDimBlock,
                                                                              const std::vector<std::vector<bool>> & ShouldBeStored,
                                                                              const std::vector<std::vector<size_t>> & CoordinatesToStore) {
    std::vector<size_t> dimensionsOneBlockObservation = this->_getAllExceptFirst(_dimensionsOfObservation);

    // go over all cells of one block
    _indexCell_inOneBlockObservation.resize(BlockSize, 0);
    _storeCell.resize(BlockSize, false);
    for (size_t i = 0; i < BlockSize; i++) {
        // finally translate coordinates into linear indices
        if (std::all_of(ShouldBeStored[i].begin(), ShouldBeStored[i].end(), [](bool v){ return v; })){ // all are true
            _storeCell[i] = true;
            _indexCell_inOneBlockObservation[i] = TDimension::getIndex(CoordinatesToStore[i], NumDimBlock,
                                                                       dimensionsOneBlockObservation);
        }
    }
}



template <typename TYPE> void TDataReaderBase<TYPE>::_prepareStorageObservation(const size_t & GuessLengthUnknownDimension){
    auto allKnownDimensions = this->_getAllExceptFirst(_dimensionsOfObservation);
    _observation->prepareFillData(GuessLengthUnknownDimension, allKnownDimensions);
}

template <typename TYPE> void TDataReaderBase<TYPE>::_finalizeFillingObservation(){
    _observation->finalizeFillData();

    for (size_t dim = 0; dim < _numDim; dim++){
        if (_rowNames->nameIsRowName(dim)){
            _rowNames->finalizeFillNames(dim, _observation->dimensions()[dim]);
            auto name = _rowNames->getDimensionName(dim);
            _observation->setDimensionName(name, dim);
        } else {
            _colNames->finalizeFillNames(dim, _observation->dimensions()[dim]);
            auto name = _colNames->getDimensionName(dim);
            _observation->setDimensionName(name, dim);
        }
    }

    // resize dimensions (remove extra dimension that was introduced in the beginning for 1D, 1 row layout)
    if (_addedArtificialFirstDimension){
        _observation->resize(this->_getAllExceptFirst(_dimensionsOfObservation));
    }
}

template <typename TYPE> void TDataReaderBase<TYPE>::_storeDataInTempVec(const std::string & DataPoint, std::vector<TYPE> & OneBlock_forObservation, const bool & KeepLine, const size_t & ElementInBlock){
    // store in temporary vector: only if 1) most outer rowname should be kept (keepLine) and 2) element should be kept
    if (KeepLine && _storeCell[ElementInBlock]) {
        size_t index = _indexCell_inOneBlockObservation[ElementInBlock];
        if (index >= OneBlock_forObservation.size()){
            throw std::runtime_error("In function 'TDataReaderBase::_storeDataInTempVec': Index " + toString(index) + " is larger or equal than size of block for observation (" +
                                             toString(OneBlock_forObservation.size()) + ")!");
        }
        OneBlock_forObservation[index] = convertStringCheck<TYPE>(DataPoint);
    }
}

template <typename TYPE> void TDataReaderBase<TYPE>::_storeInObservation(const std::vector<TYPE> & OneBlock_forObservation, const bool & KeepLine, const bool & ReadDataRaw){
    // reached end of block
    if (KeepLine) {
        // store in observation
        for (auto & data : OneBlock_forObservation) {
            if (ReadDataRaw) {
                _observation->fillDataRaw(data);
            } else {
                _observation->fillDataAndTranslate(data);
            }
        }
    }
}

template <typename TYPE> void TDataReaderBase<TYPE>::_read(const std::vector<size_t> & BlockDimensions, const std::vector<char> & BlockDelimiters,
                            const std::vector<std::vector<size_t>> &CoordinatesOneBlock,
                            const std::vector<char> &DelimitersLookupOneBlock,
                            const size_t & GuessLengthUnknownDimension,
                            const bool & ReadDataRaw) {
    size_t blockSize_File = DelimitersLookupOneBlock.size();
    size_t blockSize_Observation = TDataBlock::getBlockSize(this->_getAllExceptFirst(_dimensionsOfObservation));
    std::vector<TYPE> oneBlock_forObservation(blockSize_Observation, 0.);
    _prepareStorageObservation(GuessLengthUnknownDimension);

    // parse file
    size_t elementInBlock = 0;
    std::string dataPoint;
    bool newLine = true;
    bool keepLine = true;
    bool newBlock = true;
    for(;;) {
        if (newLine) {
            // read rownames
            if (!_rowNames->extractRowNames(_filePointer, _delimiterComment, CoordinatesOneBlock[elementInBlock], keepLine, newBlock)){
                break; // end of file
            }
            newLine = false;
            newBlock = false;
        }

        // get relevant delim
        char delim = DelimitersLookupOneBlock[elementInBlock];
        if (delim == '\n'){
            // next line will be a newline
            newLine = true;
        }

        // read cell
        if (!readUntilDelimiter(_filePointer, dataPoint, delim, _delimiterComment)){
            break;
        }

        // store in temporary vector, in right order
        _storeDataInTempVec(dataPoint, oneBlock_forObservation, keepLine, elementInBlock);

        if (elementInBlock == blockSize_File-1) {
            // store in observation
            _storeInObservation(oneBlock_forObservation, keepLine, ReadDataRaw);
            elementInBlock = 0;
            newBlock = true;
        } else {
            elementInBlock++;
        }
    }
    _finalizeFillingObservation();
}
