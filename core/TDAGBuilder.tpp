//
// Created by caduffm on 7/7/20.
//

//-------------------------------------------
// TMCMCParametersAssembler
// templated functions
//-------------------------------------------

template<template<typename> class TFunctor>
void TDAGBuilder::buildScaffoldDAG(TParameters & Parameters) {
    // first parse user parameters
    TMCMCUserInterface userInterface(_logfile);
    userInterface.parseUserConfiguration(_paramDefs, _observationDefs, Parameters);

    // now assemble the parameters: go over all parameters and create instance of TMCMCParameter
    bool throwIfTypesDontMatch = true;
    if (Parameters.parameterExists("ignoreTypes")){
        throwIfTypesDontMatch = false; // throw error if types of parameter and prior are not compatible. Only set to false if there is a good reason!
    }
    _buildDAG<TFunctor>(throwIfTypesDontMatch, Parameters);
}

template<template<typename> class TFunctor>
void TDAGBuilder::_buildDAG(bool throwIfTypesDontMatch, TParameters & Parameters){
    // first "bundle" observations/parameters that share a common prior and must be initialized together
    std::vector< std::vector<std::shared_ptr<TDefinitionBase>>> definitionsWithSharedPriorsVector;
    _bundleParametersWithSharedPriors(definitionsWithSharedPriorsVector);

    // check if all prior parameters are either numbers or do exist
    _checkIfAllPriorParamameterDefinitionsExist();

    // now loop over definitions, as long as not all parameters/observations are initialized
    while (_nameToParamMap.size() + _nameToObservationsMap.size() < _allDefs.size()){
        // loop over definitions that share a prior (and should be initialized together)
        for (auto & definitionsWithSharedPrior : definitionsWithSharedPriorsVector){
            if (_definitionsAreBuilt(definitionsWithSharedPrior)) // definitions are already initialized
                continue;

            // check if definitions can be initialized (i.e. if all required prior parameters are initialized)
            if (_allPriorParametersAreBuilt(definitionsWithSharedPrior[0]->parameters()))
                _buildParameter<TFunctor>(definitionsWithSharedPrior, Parameters);
        }
    }

    // assemble DAG
    _createOneDAG(throwIfTypesDontMatch);
}

template<template<typename> class TFunctor>
void TDAGBuilder::_buildParameter(std::vector<std::shared_ptr<TDefinitionBase>> & definitionsWithSharedPrior, TParameters & Parameters){
    // fill (already initialized) TMCMCParameters and observations that represent the hyperparameters into vector
    std::vector<std::shared_ptr<TMCMCParameterBase> > initializedParams;
    std::vector<std::shared_ptr<TObservationsBase> > initializedObservations;
    // assumes that values of paramsWithSharedPriorsVec have minimal size of 1 and that all elements in vector have same prior and parameters (name is only used for throwing)
    _findHyperParameters(definitionsWithSharedPrior[0], initializedParams, initializedObservations);

    // all ok -> now initialize!
    _buildTemplatedParameter<TFunctor>(definitionsWithSharedPrior, initializedParams, initializedObservations, Parameters);
}

template<template<typename> class TFunctor>
void TDAGBuilder::_buildTemplatedParameter(std::vector<std::shared_ptr<TDefinitionBase>> & defs,
                                           std::vector<std::shared_ptr<TMCMCParameterBase>> & initializedParams,
                                           std::vector<std::shared_ptr<TObservationsBase>> & initializedObservations,
                                           TParameters & Parameters){
    // assumes that defs has minimal one element and that all elements in defs have the same type
    if (defs[0]->type() == "double") {
        _buildParams<TFunctor, double>(defs, initializedParams, initializedObservations, Parameters);
    } else if (defs[0]->type() == "float") {
        _buildParams<TFunctor, float>(defs, initializedParams, initializedObservations, Parameters);
    } else if (defs[0]->type() == "bool") {
        _buildParams<TFunctor, bool>(defs, initializedParams, initializedObservations, Parameters);
    } else if (defs[0]->type() == "int") {
        _buildParams<TFunctor, int>(defs, initializedParams, initializedObservations, Parameters);
    } else if (defs[0]->type() == "uint8_t") {
        _buildParams<TFunctor, uint8_t>(defs, initializedParams, initializedObservations, Parameters);
    } else if (defs[0]->type() == "uint16_t") {
        _buildParams<TFunctor, uint16_t>(defs, initializedParams, initializedObservations, Parameters);
    } else if (defs[0]->type() == "uint32_t") {
        _buildParams<TFunctor, uint32_t>(defs, initializedParams, initializedObservations, Parameters);
    } else if (defs[0]->type() == "uint64_t") {
        _buildParams<TFunctor, uint64_t>(defs, initializedParams, initializedObservations, Parameters);
    } else throw std::runtime_error("Unknown type " + defs[0]->type() + "!");
}

template<template<typename> class TFunctor, class T>
void TDAGBuilder::_buildParams(std::vector<std::shared_ptr<TDefinitionBase>> & defs,
                               std::vector<std::shared_ptr<TMCMCParameterBase>> & initializedParams,
                               std::vector<std::shared_ptr<TObservationsBase>> & initializedObservations,
                               TParameters & Parameters){
    // first construct prior
    std::shared_ptr<TPrior<T>> prior = _buildPrior<TFunctor, T>(defs[0], initializedParams, initializedObservations); // (just pass first definition here, because all defs share prior)
    prior->setAdditionalParameters(_randomGenerator, _logfile, Parameters);

    for (auto & def : defs){
        // check if min/max are compatible with prior and adjust if they aren't
        _checkMinMaxPrior(prior, def);
        // decide if current definition is for a parameter or an observation -> initialize correspondingly
        if (def->isObserved()){ // observation
            _buildObservation(def->name(), prior);
        } else { // parameter
            _buildParameter(def->name(), prior);
        }
    }
}

template<template<typename> class TFunctor, class T>
std::shared_ptr<TPrior<T>> TDAGBuilder::_buildPrior(const std::shared_ptr<TDefinitionBase> & def,
                                                    std::vector<std::shared_ptr<TMCMCParameterBase>> & initializedParams,
                                                    std::vector<std::shared_ptr<TObservationsBase>> & initializedObservations){
    if (def->priorParametersAreFix()) {
        // parameter whose prior parameters are not learned get a TPrior object
        return _initPrior<T>(def->prior(), def->parameters(), def->name());
    } else {
        // parameter whose prior parameters are inferred get a TPriorWithHyperPrior object
        return _initPriorWithHyperPrior<TFunctor, T>(def->prior(), initializedParams, initializedObservations, def->name());
    }
}

template<typename T> void TDAGBuilder::_buildObservation(const std::string & Name, const std::shared_ptr<TPrior<T>> & prior){
    // get pointer to "full" observation definition (def from above is only pointer to base class, which does not have all features)
    std::shared_ptr<TObservationDefinition> observationsDef = _getPointerToObservationDefinition(Name);
    // build observation
    if (observationsDef->isTranslated()){ // translated
        _buildTranslatedObservation(observationsDef, prior);
    } else { // not translated
        std::shared_ptr<TObservationsBase> observation(new TObservationsRaw<T>(prior, observationsDef, _randomGenerator));
        observation->handPointerToPrior(observation);
        _nameToObservationsMap[observationsDef->name()] = observation;
    }
}

template<typename TranslatedType, typename StoredType> void TDAGBuilder::_buildTranslatedObservation_templated(const std::shared_ptr<TObservationDefinition> & observationsDef, const std::shared_ptr<TPrior<TranslatedType>> & prior){
    // build translator
    std::unique_ptr<TDataTranslator<TranslatedType, StoredType>> translator;
    switch (observationsDef->translator()) {
        case Translator::phred:
            translator = std::make_unique<TDataTranslatorPhredScores<TranslatedType, StoredType>>(static_cast<StoredType>(observationsDef->maxStoredValue()));
            break;
            // add translators here, as soon as others are implemented
    }
    std::shared_ptr<TObservationsBase> observation(new TObservationsTranslated<TranslatedType, StoredType>(prior, observationsDef, _randomGenerator, translator));
    observation->handPointerToPrior(observation);
    _nameToObservationsMap[observationsDef->name()] = observation;
}

template<typename T> void TDAGBuilder::_buildTranslatedObservation(const std::shared_ptr<TObservationDefinition> & observationsDef, const std::shared_ptr<TPrior<T>> & prior){
    // decide on type of stored values
    // assumes that defs has minimal one element and that all elements in defs have the same type
    if (observationsDef->storedType() == "double") {
        _buildTranslatedObservation_templated<T, double>(observationsDef, prior);
    } else if (observationsDef->storedType() == "float") {
        _buildTranslatedObservation_templated<T, float>(observationsDef, prior);
    } else if (observationsDef->storedType() == "bool") {
        _buildTranslatedObservation_templated<T, bool>(observationsDef, prior);
    } else if (observationsDef->storedType() == "int") {
        _buildTranslatedObservation_templated<T, int>(observationsDef, prior);
    } else if (observationsDef->storedType() == "uint8_t") {
        _buildTranslatedObservation_templated<T, uint8_t>(observationsDef, prior);
    } else if (observationsDef->storedType() == "uint16_t") {
        _buildTranslatedObservation_templated<T, uint16_t>(observationsDef, prior);
    } else if (observationsDef->storedType() == "uint32_t") {
        _buildTranslatedObservation_templated<T, uint32_t>(observationsDef, prior);
    } else if (observationsDef->storedType() == "uint64_t") {
        _buildTranslatedObservation_templated<T, uint64_t>(observationsDef, prior);
    } else throw std::runtime_error("Can not initialize observation " + observationsDef->name() + ": Unknown StorageType " + observationsDef->storedType() + "!");
}

template<typename T> void TDAGBuilder::_buildParameter(const std::string & Name, const std::shared_ptr<TPrior<T>> & prior){
    // get pointer to "full" parameter definition (def from above is only pointer to base class, which does not have all features)
    std::shared_ptr<TParameterDefinition> paramDef = _getPointerToParameterDefinition(Name);
    // build parameter
    std::shared_ptr<TMCMCParameterBase> parameter(new TMCMCParameter<T>(prior, paramDef, _randomGenerator));
    parameter->handPointerToPrior(parameter);
    _nameToParamMap[paramDef->name()] = parameter;
}

template<typename T> void TDAGBuilder::_checkMinMaxPrior(const std::shared_ptr<TPrior<T>> & prior, std::shared_ptr<TDefinitionBase> & def){
    // this function re-adjusts min/max of PARAMETER (e.g. prior is exponential -> min of parameter must be 0)
    std::string min = def->min();
    std::string max = def->max();
    bool minIncluded = def->minIncluded();
    bool maxIncluded = def->maxIncluded();
    bool hasDefaultMin = def->hasDefaultMin();
    bool hasDefaultMax = def->hasDefaultMax();
    if (!prior->checkMinMax(min, max, minIncluded, maxIncluded, hasDefaultMin, hasDefaultMax)) {
        if (!hasDefaultMin) def->setMin(min, minIncluded);
        if (!hasDefaultMax) def->setMax(max, maxIncluded);
    }
}

template<typename T> std::shared_ptr<TPrior<T>> TDAGBuilder::_initPrior(const std::string & priorName, std::string priorParams, const std::string& paramName){
    // decide which prior to use
    trimString(priorParams);

    std::shared_ptr<TPrior<T>> prior;
    // match distribution and parameters
    // at the moment, no developer-defined priors can have fixed priorParams (I assume they are always MCMC parameters.)
    // If this is ever an issue, eiter 1) add a second object and function template in order to pass another function or
    // 2) just pretend it uses MCMC parameters, and pass information via those (probably easier)
    if (priorName == MCMCPrior::uniform) {
        prior = std::make_shared<TPriorUniform<T>>();
    } else if (priorName == MCMCPrior::normal){
        prior = std::make_shared<TPriorNormal<T>>();
    } else if (priorName == MCMCPrior::exponential){
        prior = std::make_shared<TPriorExponential<T>>();
    } else if (priorName == MCMCPrior::chisq){
        prior = std::make_shared<TPriorChisq<T>>();
    } else if (priorName == MCMCPrior::beta){
        prior = std::make_shared<TPriorBeta<T>>();
    } else if (priorName == MCMCPrior::multivariateChisq){
        prior = std::make_shared<TPriorMultivariateChisq<T>>();
    } else if (priorName == MCMCPrior::multivariateChi){
        prior = std::make_shared<TPriorMultivariateChi<T>>();
    } else if (priorName == MCMCPrior::binomial) {
        prior = std::make_shared<TPriorBinomial<T>>();
    } else if (priorName == MCMCPrior::bernouilli) {
        prior = std::make_shared<TPriorBernouilli<T>>();
    } else if (priorName == MCMCPrior::dirichlet) {
        prior = std::make_shared<TPriorDirichlet<T>>();
    }  // all other priors only makes sense for arrays -> not listed here
    else {
        throw std::runtime_error("Can not create parameter '" + paramName + "': Prior (with fixed prior parameters) with name '" + priorName + "' does not exist!");
    }
    prior->initialize(priorParams);
    return prior;
}

template<template<typename> class TFunctor, class T>
std::shared_ptr<TPrior<T>> TDAGBuilder::_initPriorWithHyperPrior(const std::string & priorName,
                                                                 std::vector<std::shared_ptr<TMCMCParameterBase> > & initializedParams,
                                                                 std::vector<std::shared_ptr<TObservationsBase>> & initializedObservations,
                                                                 const std::string& paramName){
    // now create TPrior with these parameters
    std::shared_ptr<TPrior<T>> prior;
    // first try with developer-defined priors -> at the moment, I assume that developer-defined priors always are inferred priors, i.e. they need initializedParams
    prior = TFunctor<T>()(priorName);
    if (!prior) { // prior is not developer-defined -> try standard mcmcframework priors
        if (priorName == MCMCPrior::exponential) {
            prior = std::make_shared<TPriorExponentialWithHyperPrior<T>>();
        } else if (priorName == MCMCPrior::beta) {
            prior = std::make_shared<TPriorBetaWithHyperPrior<T>>();
        } else if (priorName == MCMCPrior::normal) {
            prior = std::make_shared<TPriorNormalWithHyperPrior<T>>();
        } else if (priorName == MCMCPrior::binomial) {
            prior = std::make_shared<TPriorBinomialWithHyperPrior<T>>();
        } else if (priorName == MCMCPrior::bernouilli) {
            prior = std::make_shared<TPriorBernouilliWithHyperPrior<T>>();
        } else if (priorName == MCMCPrior::categorical) {
            prior = std::make_shared<TPriorCategoricalWithHyperPrior<T>>();
        } else if (priorName == MCMCPrior::hmm_bool) {
            prior = std::make_shared<TPriorHMMBoolWithHyperPrior<T>>();
        } else if (priorName == MCMCPrior::hmm_ladder) {
            prior = std::make_shared<TPriorHMMLadderWithHyperPrior<T>>();
        } else if (priorName == MCMCPrior::multivariateNormal) {
            prior = std::make_shared<TPriorMultivariateNormalWithHyperPrior<T>>();
        } else if (priorName == MCMCPrior::normal_mixedModel) {
            prior = std::make_shared<TPriorNormalMixedModelWithHyperPrior<T>>();
        } else if (priorName == MCMCPrior::normal_mixedModel_stretch) {
            prior = std::make_shared<TPriorNormalTwoMixedModelsWithHyperPrior<T>>();
        } else if (priorName == MCMCPrior::multivariateNormal_mixedModel) {
            prior = std::make_shared<TPriorMultivariateNormalMixedModelWithHyperPrior<T>>();
        } else if (priorName == MCMCPrior::multivariateNormal_mixedModel_stretch) {
            prior = std::make_shared<TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>>();
        } else {
            throw std::runtime_error("Can not create parameter '" + paramName + "': Prior (with inferred prior parameters) with name '" + priorName + "' does not exist!");
        }
    }
    prior->initPriorWithHyperPrior(initializedParams, initializedObservations);
    return prior;
}