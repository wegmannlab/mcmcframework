//
// Created by madleina on 30.11.20.
//

#ifndef MCMCFRAMEWORK_TPRIORWITHHYPERPRIOR_MIXEDMODELS_H
#define MCMCFRAMEWORK_TPRIORWITHHYPERPRIOR_MIXEDMODELS_H

#include "TPriorWithHyperPrior.h"
#include "TLatentVariable.h"
#include "algorithmsAndVectors.h"

//-------------------------------------------
// TPriorNormalMixedModelWithHyperPrior
//-------------------------------------------

template <typename T>
class TPriorNormalMixedModelWithHyperPrior: public TPriorWithHyperPrior<T>, public TLatentVariable<double, size_t, size_t>  {
protected:
    std::shared_ptr<TMCMCParameterBase> _z;
    std::vector<std::shared_ptr<TMCMCParameterBase>> _mus;
    std::vector<std::shared_ptr<TMCMCParameterBase>> _vars;

    // number of mixture components
    size_t _K;

    // temporary values (stored for speed)
    double _oneDivTwo_log2Pi;

    // EM
    double * _EM_update_temp;

    // initialize parameter before EM
    double _dist(const T & x, const T & y);
    void _fillMeanParameterValues(std::vector<double> & meanParamValues);
    void _findMostDistantPoints(std::vector<T> & minValues, const std::vector<double> & meanParameterValues);
    void _setInitialZ(std::vector<T> & minValues, const std::vector<double> & meanParameterValues);
    virtual void _setInitialMeanAndVar();

    // update one mean_k
    virtual double _calcLLUpdateMean(const size_t& k);
    double _calcLogHUpdateMean(const size_t& k);
    void _updateMean(const size_t& k);

    // update one var_k
    virtual double _calcLLUpdateVar(const size_t& k);
    double _calcLogHUpdateVar(const size_t& k);
    void _updateVar(const size_t& k);
    // update gammas
    virtual double _calcLLUpdateZ(const size_t& i);
    double _calcLogHUpdateZ(const size_t& i);
    void _updateZ(const size_t& i);

    void _updateTempVals() override;
    // check min/max
    void _checkMinMaxOfHyperPrior() override;
    // log-densities
    virtual double _getLogPriorDensity(const T & x, const size_t & k);
    double _getLogPriorDensity_oneVal(T x) override;
    double _getLogPriorRatio_oneVal(T x, T x_old) override;
    double _getLogPriorDensity_vec(const std::shared_ptr<TMCMCObservationsBase> & data, const size_t & index) override;
    double _getLogPriorDensityOld_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index) override;
    double _getLogPriorRatio_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index) override;

public:
    TPriorNormalMixedModelWithHyperPrior();
    ~TPriorNormalMixedModelWithHyperPrior() = default;

    // initialization
    void initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, std::vector<std::shared_ptr<TObservationsBase> > & observations) override;

    // construct DAG
    void constructDAG(TDAG & DAG, TDAG & temporaryDAG) override;
    void typesAreCompatible(bool throwIfNot) override;
    void initializeStorageOfPriorParameters() override;

    // estimate initial values
    void estimateInitialPriorParameters(TLog * Logfile) override;

    // full log densities
    double getSumLogPriorDensity(const std::shared_ptr<TMCMCObservationsBase> & data) override;
    double getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data) override;

    // update all hyperprior parameters
    void updateParams() override;

    // simulate values
    void simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) override;

    // initialize parameters before EM
    void initializeEMParameters() override;
    size_t getHiddenState(const size_t & Index) const override;
    // EM
    void calculateEmissionProbabilities(const size_t & Index, TDataVector<double, size_t> & Emission) override;
    void prepareEMParameterEstimationInitial() override;
    void prepareEMParameterEstimationOneIteration() override;
    void handleEMParameterEstimationOneIteration(const size_t & index, const TDataVector<double, size_t> & Weights) override;
    void finalizeEMParameterEstimationOneIteration() override;
    void finalizeEMParameterEstimationFinal() override;
    void handleStatePosteriorEstimation(const size_t & index, const TDataVector<double, size_t> & StatePosteriorProbabilities) override;
};


//-------------------------------------------
// TPriorNormalMixedModelWithHyperPrior
//-------------------------------------------

template <typename T>
class TPriorNormalTwoMixedModelsWithHyperPrior: public TPriorNormalMixedModelWithHyperPrior<T> {
protected:
    // temporary values (stored for speed)
    double _variance1;

    // update mean
    double _calcLLUpdateMean(const size_t& k) override;
    // update var
    double _getVarForThisK(const size_t & k);
    double _getVar1();
    double _calcLLUpdateVar(const size_t& k) override;
    double _calcLLUpdateVar_0();
    double _calcLLUpdateVar_1();

    void _updateTempVals() override;
    // log-densities
    double _getLogPriorDensity(const T & x, const size_t & k) override;
    double _getLogPriorDensity_oneVal(T x) override;
    double _getLogPriorRatio_oneVal(T x, T x_old) override;
    double _getLogPriorRatio_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index) override;

    // initialize parameters before EM
    double _setInitialMean_ReturnVariance();
    void _initializeEMParams_basedOnCutoff(const double & var);
    void _initializeEMParams_refinement();
    void _setInitialMeanAndVar() override;
    void _setInitialVar();
    void _switchEMLabels(TLog * Logfile);

public:
    TPriorNormalTwoMixedModelsWithHyperPrior();
    ~TPriorNormalTwoMixedModelsWithHyperPrior() override = default;
    void initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, std::vector<std::shared_ptr<TObservationsBase> > & observations) override;

    // estimate initial values
    void estimateInitialPriorParameters(TLog * Logfile) override;

    // initialize parameters before EM
    void initializeEMParameters() override;
    // EM
    void calculateEmissionProbabilities(const size_t & Index, TDataVector<double, size_t> & Emission) override;
    void prepareEMParameterEstimationInitial() override;
    void prepareEMParameterEstimationOneIteration() override;
    void handleEMParameterEstimationOneIteration(const size_t & index, const TDataVector<double, size_t> & Weights) override;
    void finalizeEMParameterEstimationOneIteration() override;

    // simulate values
    void simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) override;
};

//-------------------------------------------------
// TPriorMultivariateNormalMixedModelWithHyperPrior
//------------------------------------------------
#ifdef WITH_ARMADILLO
template <typename T>
class TPriorMultivariateNormalMixedModelWithHyperPrior: public TMultivariateNormal<T>, public TLatentVariable<double, size_t, size_t> {
protected:
    // parameters
    std::vector<std::shared_ptr<TMCMCParameterBase>> _mus;
    std::vector<std::shared_ptr<TMCMCParameterBase>> _m;
    std::vector<std::shared_ptr<TMCMCParameterBase>> _Mrr;
    std::vector<std::shared_ptr<TMCMCParameterBase>> _Mrs;
    std::shared_ptr<TMCMCParameterBase> _z;

    // number of mixture components
    size_t _K;
    void _checkDimensions();

    // EM
    double * _EM_update_Mus_temp;
    arma::mat * _EM_update_Sigma_temp;
    double * _EM_update_Weights_temp;

    // initialize parameter before EM
    void _fillMeanParameterValues(std::vector<std::vector<double>> & meanParamValues);
    void _findMostDistantPoints(std::vector<std::vector<T>> & minValues, const std::vector<std::vector<double>> & meanParameterValues);
    void _setInitialZ(std::vector<std::vector<T>> & minValues, const std::vector<std::vector<double>> & meanParameterValues);
    void _setInitialMeanAndM();
    virtual void _setMusToMLE();
    virtual void _setMToMLE();

    // EM
    void _handleEMMaximizationOneIteration_updateSigma(const size_t & k, const arma::mat * sumXpiMinusMuSquare, const TDataVector<double, size_t> & Weights);
    void _updateEMParametersOneIteration_Sigma(arma::mat & Sigma, const size_t & k);

    // update z
    virtual double _calcLLUpdateZ(const size_t & n);
    double _calcLogHUpdateZ(const size_t & n);
    void _updateZ(const size_t & n);

    // update mus
    virtual double _calcLLUpdateMu(const size_t & k, const size_t & d);
    double _calcLogHUpdateMu(const size_t & k, const size_t & d);
    virtual void _updateMu(const size_t & k, const size_t & d);

    // update m
    virtual double _calcLLUpdateM(const size_t & k);
    double _calcLogHUpdateM(const size_t & k);
    virtual void _updateM(const size_t & k);

    // update mrr
    virtual double _calcLLUpdateMrr(const size_t & k, const size_t & r);
    double _calcLogHUpdateMrr(const size_t & k, const size_t & d);
    virtual void _updateMrr(const size_t & k, const size_t & r);

    // update mrs
    virtual void _updateMrs(const size_t & k, const size_t & r, const size_t & s);
    virtual double _calcLLUpdateMrs(const size_t & k, const size_t & r, const size_t & s);
    double _calcLogHUpdateMrs(const size_t & k, const size_t & r, const size_t & s);

    // check min/max
    void _checkMinMaxOfHyperPrior() override;

    // log-densities
    double _getLogPriorDensity_vec(const std::shared_ptr<TMCMCObservationsBase> & data, const size_t & index) override;
    double _getLogPriorDensityOld_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index) override;
    double _getLogPriorRatio_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index) override;

public:
    TPriorMultivariateNormalMixedModelWithHyperPrior();
    virtual ~TPriorMultivariateNormalMixedModelWithHyperPrior() = default;
    void initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, std::vector<std::shared_ptr<TObservationsBase> > & observations) override;

    // construct DAG
    void constructDAG(TDAG & DAG, TDAG & temporaryDAG) override;
    void typesAreCompatible(bool throwIfNot) override;
    void initializeStorageOfPriorParameters() override;

    // estimate initial values
    void estimateInitialPriorParameters(TLog * Logfile) override;

    // return sum of log-densities of entire vector
    double getSumLogPriorDensity(const std::shared_ptr<TMCMCObservationsBase> & data) override;
    double getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data) override;

    // update mu, m, mrr, mrs, mu1, gammas and rho
    void updateParams() override;

    // simulate values
    void simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) override;

    // initialize parameters before EM
    void initializeEMParameters() override;
    size_t getHiddenState(const size_t & Index) const override;
    // EM
    void calculateEmissionProbabilities(const size_t & Index, TDataVector<double, size_t> & Emission) override;
    void prepareEMParameterEstimationInitial() override;
    void prepareEMParameterEstimationOneIteration() override;
    void handleEMParameterEstimationOneIteration(const size_t & index, const TDataVector<double, size_t> & Weights) override;
    void finalizeEMParameterEstimationOneIteration() override;
    void finalizeEMParameterEstimationFinal() override;
    void handleStatePosteriorEstimation(const size_t & index, const TDataVector<double, size_t> & StatePosteriorProbabilities) override;
};
#else
// armadillo is not defined -> will throw an error, because multivariate normal mixed model requires armadillo
template <typename T>
class TPriorMultivariateNormalMixedModelWithHyperPrior: public TMultivariateNormal<T> {
public:
    // armadillo is not defined -> will throw an error, because multivariate normal mixed model requires armadillo
    TPriorMultivariateNormalMixedModelWithHyperPrior() : TMultivariateNormal<T>() {
        throw "The library 'armadillo' has not been found but is required to use a multivariate normal prior with mixed model!";
    };
};
#endif //WITH_ARMADILLO


//-------------------------------------------------
// TPriorMultivariateNormalMixedModelWithHyperPrior
//------------------------------------------------
#ifdef WITH_ARMADILLO
 class TVarCovarMatrix {
protected:
     // vector of noise which will be added to diagonal of Sigma in case taking the inverse fails
     // corresponds to exp(-18:0) in R
     std::vector<double> _noiseInverseSigma = {1.52299797447126e-08,4.13993771878517e-08,1.12535174719259e-07,3.05902320501826e-07,8.31528719103568e-07,2.26032940698105e-06,6.14421235332821e-06,1.67017007902457e-05,4.53999297624849e-05,0.00012340980408668,0.000335462627902512,0.000911881965554516,0.00247875217666636,0.00673794699908547,0.0183156388887342,0.0497870683678639,0.135335283236613,0.367879441171442,1};

 public:
    arma::mat M;
    arma::mat oldM;
    arma::mat Sigma;
    arma::mat oldSigma;

    TVarCovarMatrix(){};

    void init(int D){
        M.eye(D, D); // set diagonal to 1 and rest to 0
        oldM = M;
        calculateSigma();
        oldSigma = Sigma;
    }

    void calculateSigma(){
        oldSigma = Sigma;
        Sigma = arma::inv_sympd(M * M.t());
    }

    void calculateM() {
        oldM = M;

        arma::mat invSigma;
        if (!arma::inv_sympd(invSigma, Sigma)){
            // add noise to diagonal
            arma::mat SigmaNoisy = Sigma;
            SigmaNoisy.diag() += _noiseInverseSigma[0];
            size_t counter = 1;
            while (!arma::inv_sympd(invSigma, SigmaNoisy)){
                if (counter == _noiseInverseSigma.size()){
                    // if still cannot take inverse: just use diagonal matrix (I want to avoid throwing errors in any case!)
                    invSigma.eye(Sigma.n_rows, Sigma.n_cols);
                    break;
                }
                // add more and more noise
                SigmaNoisy.diag() += _noiseInverseSigma[counter];
                counter++;

                std::cout << "Sigma: " << std::endl;
                std::cout << Sigma << std::endl;
                std::cout << "SigmaNoisy: " << std::endl;
                std::cout << SigmaNoisy << std::endl;
            }
        }
        if (!arma::chol(M, invSigma, "lower")){
            // failed to do Cholesky decomposition
            // I observed that this sometimes happens if Sigma is singular, but armadillo still finds some absurd solution
            // with inv_sympd (results in huge values of invSigma) -> but afterwards, Cholesky fails
            // Using arma::solve for the inverse in such cases yields better results (not sure if
            // that is always the case, but in my example it worked) -> try doing this
            invSigma = arma::solve(Sigma, arma::eye(Sigma.n_rows, Sigma.n_cols));
            if (!arma::chol(M, invSigma, "lower")){
                // if still cannot do Cholesky: just use diagonal matrix (I want to avoid throwing errors in any case!)
                M.eye(Sigma.n_rows, Sigma.n_cols);
            }

            std::cout << "Sigma: " << std::endl;
            std::cout << Sigma << std::endl;
            std::cout << "invSigma: " << std::endl;
            std::cout << invSigma << std::endl;
        }
    }

    void reset(){
        M = oldM;
        Sigma = oldSigma;
    }
};
template <typename T>
class TPriorMultivariateNormalTwoMixedModelsWithHyperPrior: public TPriorMultivariateNormalMixedModelWithHyperPrior<T> {
protected:
    // parameters (all others are inherited from multivariate normal prior)
    std::shared_ptr<TMCMCParameterBase> _rhos;

    // class containing armadillo matrices for both models
    TVarCovarMatrix _zeroModel;
    TVarCovarMatrix _oneModel;

    // functions to calculate stretching of varCovarMatrices
    arma::vec _stretchEigenVals(const arma::vec& Lambda_0);
    void _stretchSigma1();
    void _fill_M0();
    void _fill_M1();
    void _fill_M0_afterUpdateMrr(const double & newVal, const size_t & r);
    void _fill_M0_afterUpdateMrs(const double & newVal, const size_t & r, const size_t & s);
    void _fill_M0_afterUpdate_m(const double & newVal, const double & oldVal);
    double _calcDoubleSum_oldMinNew_M1(const std::shared_ptr<TMCMCObservationsBase> & paramToUse, const TRange & row);

    // initialize parameter before EM
    void _setMusToMLE() override;
    void _setMToMLE() override;
    void _setAllParametersAfterEM(TLog * Logfile);
    void _switchEMLabels(TLog * Logfile);
    void _initializeEMParams_basedOnCutoff(const arma::mat & M);
    void _initializeEMParams_refinement();
    void _setRhoAfterEM(const arma::vec & Lambda_0, const arma::vec & Lambda_1);

        // update z
    void _calcDoubleSum_updateZ(const std::shared_ptr<TMCMCObservationsBase> & paramToUse, const TRange & row, double & outerSum_0Model, double & outerSum_1Model);
    void _calcSumLogMrr_updateZ(double & sumlogMrr0, double & sumlogMrr1);
    double _calcLLUpdateZ(const size_t & n);

    // update rhos
    double _calcLLUpdateRho(const size_t & d);
    double _calcLogHUpdateRho(const size_t & d);
    void _updateRho(const size_t & d);

    // update mus
    double _calcLLUpdateMu(const size_t & k, const size_t & d) override;

    // update m0
    void _updateM(const size_t & k) override;
    double _calcLLUpdateM(const size_t & k) override;

    // update mrr0
    double _calcLLRatioUpdateM1(const size_t & N, const double & sum_1Model);
    void _updateMrr(const size_t & k, const size_t & r) override;
    double _calcLLUpdateMrr(const size_t & k, const size_t & r) override;

    // update mrs0
    void _updateMrs(const size_t & k, const size_t & r, const size_t & s) override;
    double _calcLLUpdateMrs(const size_t & k, const size_t & r, const size_t & s) override;

    // check min/max
    void _checkMinMaxOfHyperPrior() override;

    // log-densities
    double _calcSumM1_dr_squared(const size_t & r);
    double _calcDoubleSum_M1_updateParam(const std::shared_ptr<TMCMCObservationsBase> & data, const TRange & row, const size_t & indexThatChanged, const size_t & min_r_DMin1);
    double _calcDoubleSum_M1_updateMu(const std::shared_ptr<TMCMCObservationsBase> & paramToUse, const TRange & row, const size_t & indexThatChanged, const size_t & min_r_DMin1);
    double _calcDoubleSumOld_M1(const std::shared_ptr<TMCMCParameterBase> & data, const TRange & row, const size_t & indexInRowThatChanged);
    double _calcDoubleSum_M1(const std::shared_ptr<TMCMCObservationsBase> & data, const TRange & row);
    double _calcSumLogMrr1();
    double _calcLogPriorDensity_M1_vec(const std::shared_ptr<TMCMCObservationsBase> & data, const TRange &row);
    double _calcLogPriorDensityOld_M1_vec(const std::shared_ptr<TMCMCParameterBase> & data, const TRange &row, const size_t & col);
    double _getLogPriorDensity_vec(const std::shared_ptr<TMCMCObservationsBase> & data, const size_t & index) override;
    double _getLogPriorDensityOld_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index) override;
    double _calcLogPriorRatio_M1_vec(const std::shared_ptr<TMCMCParameterBase> & data, const TRange &row, const size_t & d);
    double _getLogPriorRatio_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index) override;

public:
    TPriorMultivariateNormalTwoMixedModelsWithHyperPrior();
    void initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, std::vector<std::shared_ptr<TObservationsBase> > & observations) override;

    // construct DAG
    void constructDAG(TDAG & DAG, TDAG & temporaryDAG) override;
    void typesAreCompatible(bool throwIfNot) override;
    void initializeStorageOfPriorParameters() override;

    // estimate initial values
    void estimateInitialPriorParameters(TLog * Logfile) override;

    // return sum of log-densities of entire vector
    double getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data) override;

    // update mu, m, mrr, mrs, mu1, gammas and rho
    void updateParams() override;

    // simulate values
    void simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) override;

    // initialize parameters before EM
    void initializeEMParameters() override;
    // EM
    void calculateEmissionProbabilities(const size_t & Index, TDataVector<double, size_t> & Emission) override;
    void prepareEMParameterEstimationInitial() override;
    void prepareEMParameterEstimationOneIteration() override;
    void handleEMParameterEstimationOneIteration(const size_t & index, const TDataVector<double, size_t> & Weights) override;
    void finalizeEMParameterEstimationOneIteration() override;
    void finalizeEMParameterEstimationFinal() override;
};
#else
// armadillo is not defined -> will throw an error, because multivariate normal mixed model requires armadillo
template <typename T>
class TPriorMultivariateNormalTwoMixedModelsWithHyperPrior: public TPriorMultivariateNormalMixedModelWithHyperPrior<T> {
public:
    // armadillo is not defined -> will throw an error, because multivariate normal mixed model requires armadillo
    TPriorMultivariateNormalTwoMixedModelsWithHyperPrior() : TPriorMultivariateNormalMixedModelWithHyperPrior<T>() {
        throw "The library 'armadillo' has not been found but is required to use a multivariate normal prior with mixed model!";
    };
};
#endif //WITH_ARMADILLO


#include "TPriorWithHyperPrior_MixedModels.tpp"

#endif //MCMCFRAMEWORK_TPRIORWITHHYPERPRIOR_MIXEDMODELS_H
