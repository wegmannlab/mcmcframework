//
// Created by Madleina Caduff on 01.04.20.
//

#ifndef MCMCFRAMEWORK_TMCMCPARAMETERBASE_H
#define MCMCFRAMEWORK_TMCMCPARAMETERBASE_H

#include "TMCMCObservationsBase.h"
#include "TRandomGenerator.h"
#include "TLatentVariable.h"
#include "counters.h"

//-------------------------------------------
// TMCMCParameterBase
//-------------------------------------------
class TMCMCParameterBase : public TMCMCObservationsBase {
    // abstract base class for templated MCMC parameters
    // provides the interface to parameters (-> i.e. all functions that developer uses)
protected:
    virtual void _oldValue(double & dest) const = 0;
    virtual void _oldValue(float & dest) const = 0;
    virtual void _oldValue(int & dest) const = 0;
    virtual void _oldValue(uint8_t & dest) const = 0;
    virtual void _oldValue(uint16_t & dest) const = 0;
    virtual void _oldValue(uint32_t & dest) const = 0;
    virtual void _oldValue(uint64_t & dest) const = 0;
    virtual void _oldValue(bool & dest) const = 0;

    virtual void _oldValue(const size_t & i, double & dest) const = 0;
    virtual void _oldValue(const size_t & i, float & dest) const = 0;
    virtual void _oldValue(const size_t & i, int & dest) const = 0;
    virtual void _oldValue(const size_t & i, uint8_t & dest) const = 0;
    virtual void _oldValue(const size_t & i, uint16_t & dest) const = 0;
    virtual void _oldValue(const size_t & i, uint32_t & dest) const = 0;
    virtual void _oldValue(const size_t & i, uint64_t & dest) const = 0;
    virtual void _oldValue(const size_t & i, bool & dest) const = 0;

public:
    TMCMCParameterBase()= default;
    virtual ~TMCMCParameterBase() = default;

    // initialization
    virtual void runEMEstimation(TLatentVariable<double, size_t, size_t> &latentVariable, TLog * Logfile) = 0;
    virtual void switchPriorClassificationAfterEM(TLog * Logfile) = 0;

    // updates
    virtual bool isUpdated() const = 0;

    // values
    template<class T> T oldValue() const{
        auto tmp = T{};
        _oldValue(tmp);
        return tmp;
    }
    template<class T> T oldValue(const size_t & i) const{
        auto tmp = T{};
        _oldValue(i, tmp);
        return tmp;
    }
    virtual double oldLogValue() const = 0;
    virtual double oldLogValue(const size_t & i) const = 0;
    virtual double oldExpValue() const = 0;
    virtual double oldExpValue(const size_t & i) const = 0;

    // updating
    virtual bool update() = 0;
    virtual bool update(const size_t & i) = 0;
    virtual double getLogPriorRatio() = 0;
    virtual double getLogPriorDensityOld() = 0;
    virtual double getLogPriorRatio(const size_t & i) = 0;
    virtual double getLogPriorDensityOld(const size_t & i) = 0;

    // accepting / rejecting
    virtual bool acceptOrReject(const double & log_h) = 0;
    virtual bool acceptOrReject(const size_t & i, const double & log_h) = 0;
    virtual bool acceptOrRejectBatch(const TRange & Range, const double & log_h) = 0;

    // proposal stuff
    virtual void adjustProposalAndResetCounters() = 0;
    virtual void printAccRateToLogfile(TLog* & logfile) = 0;

    // accessing (all possible types)
    virtual void setVal(double val) = 0;
    virtual void setVal(const size_t & i, double val) = 0;

    // reset
    virtual void reset() = 0;
    virtual void reset(const size_t & i) = 0;

    // writers
    virtual void writeValsOneString(TOutputFile & file) = 0;
    virtual void writeMean(TOutputFile & file) = 0;
    virtual void writeVar(TOutputFile & file) = 0;
    virtual void writeJumpSizeOneString(TOutputFile & file) = 0;
};

#endif //MCMCFRAMEWORK_TMCMCPARAMETERBASE_H