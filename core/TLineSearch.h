//
// Created by madleina on 24.03.21.
//

#ifndef EMPTY_TLINESEARCH_H
#define EMPTY_TLINESEARCH_H

#include <cstdint>
#include <cmath>
#include "stringFunctions.h"
#include "TLog.h"

class TLineSearch {
private:
    TLog * _logfile;
    bool _report;

    static bool _turnAround_Max(const double & f_x, const double & previous_f_x){
        // if f_x gets smaller -> missed the peak
        if (f_x < previous_f_x){
            return true;
        } else {
            return false;
        }
    }

    static bool _turnAround_Min(const double & f_x, const double & previous_f_x){
        // if f_x gets larger -> missed the peak
        if (f_x > previous_f_x){
            return true;
        } else {
            return false;
        }
    }

    static bool _turnAround_Zero(const double & f_x, const double & previous_f_x){
        // if f_x and previous_f_x have different signs -> missed zero
        if (f_x * previous_f_x <= 0.){
            return true;
        } else {
            return false;
        }
    }

    template<class Object, class Function, class TurnAroundFunction>
    double _lineSearch(Object & Obj, Function & F, TurnAroundFunction & TurnAround, const std::string & What, const double & Start, const double & InitialStep, const double & Epsilon, const size_t & MaxIterations){
        double x = Start;
        double f_x = (Obj.*F)(x);
        double previous_f_x = f_x;
        double step = InitialStep;
        double minusExp1 = -exp(1.);

        for (size_t j = 0; j < MaxIterations; j++) {
            // make step
            x += step;
            // calculate f_x
            previous_f_x = f_x;
            f_x = (Obj.*F)(x);
            // check if peak was missed -> turn around
            if (TurnAround(f_x, previous_f_x)){
                step /= minusExp1;
            }
            // check for convergence
            if (std::fabs(step) < Epsilon){
                if (_report){
                    _logfile->list("Line search: Found " + What + " within " + toString(j) + " iterations. Absolute step at end: " + toString(std::fabs(step)) + ".");
                }
                return x;
            }
        }
        if (_report) {
            _logfile->warning("Line search: Failed to find " + What + ", reached maximum number of iterations (" + toString(MaxIterations) + "). Absolute step at end: " + toString(std::fabs(step)) + ".");
        }
        return x;
    }
public:
    TLineSearch(){
        _logfile = nullptr;
        _report = false;
    }

    TLineSearch(TLog * Logfile) : TLineSearch(){
        report(Logfile);
    }

    void report(TLog * Logfile){
        _logfile = Logfile;
        _report = true;
    }

    template<class Object, class Function>
    double findMax(Object & Obj, Function & F, const double & Start, const double & InitialStep, const double & Epsilon = 10e-10, const size_t & MaxIterations = 1000){
        return _lineSearch(Obj, F, TLineSearch::_turnAround_Max, "maximum",  Start, InitialStep, Epsilon, MaxIterations);
    }

    template<class Object, class Function>
    double findMin(Object & Obj, Function & F, const double & Start, const double & InitialStep, const double & Epsilon = 10e-10, const size_t & MaxIterations = 1000){
        return _lineSearch(Obj, F, TLineSearch::_turnAround_Min, "minimum", Start, InitialStep, Epsilon, MaxIterations);
    }

    template<class Object, class Function>
    double findZero(Object & Obj, Function & F, const double & Start, const double & InitialStep, const double & Epsilon = 10e-10, const size_t & MaxIterations = 1000){
        // ATTENTION: method is not very robust. Only guaranteed to work if function is constantly increasing/decreasing (based on my tests)...
        // first make sure that initial step points to the correct direction
        double step = InitialStep;
        double f_x = (Obj.*F)(Start);
        double f_next_x = (Obj.*F)(Start + step);
        if (std::fabs(f_next_x) > std::fabs(f_x) && f_x * f_next_x > 0.){ // if function gets larger after making the step -> we are walking in the wrong direction, turn around
            step = -step;
        }
        return _lineSearch(Obj, F, TLineSearch::_turnAround_Zero, "root", Start, step, Epsilon, MaxIterations);
    }
};

#endif //EMPTY_TLINESEARCH_H