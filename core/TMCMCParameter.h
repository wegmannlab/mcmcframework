//
// Created by madleina on 01.07.20.
//

#ifndef MCMCFRAMEWORK_TMCMCPARAMETER_H
#define MCMCFRAMEWORK_TMCMCPARAMETER_H

#include "TPriorWithHyperPrior_MixedModels.h"
#include "TProposalKernel.h"
#include "TMCMCFiles.h"

//-------------------------------------------
// TMCMCParameters
//-------------------------------------------

template <typename T>
class TMCMCParameter : public TMCMCParameterBase, public std::enable_shared_from_this<TMCMCParameterBase>{
protected:
    // definition
    std::shared_ptr<TParameterDefinition> _def;

    // values and dimensionality
    std::shared_ptr<TMultiDimensionalStorage<T>> _storage;

    // prior
    std::shared_ptr<TPrior<T>> _prior;

    // proposal kernel stuff
    TRandomGenerator* _randomGenerator;
    bool _isUpdated;
    std::unique_ptr<TPropKernel<T>> _propKernel;
    TBoundary<T> _boundary;
    std::vector<T> _jumpSizeProposalVec;
    int _totalUpdates;
    std::vector<int> _acceptedUpdates;
    bool _sharedJumpSize;

    // posterior mean and variance
    std::vector<TMeanVar<double>> _meanVar;

    // initialization
    void _initialize();
    void _initMeanVar_InitVals_JumpSizes();
    void _readVals(const std::string& initVal, std::vector<T> & storage);
    void _readValsOnlyOneNumber(const std::string& initVal, std::vector<T> & storage) const;
    void _readValsFromVec(const std::string& initVal, std::vector<T> & storage) const;
    void _readValsFromFile(const std::string& filename, std::vector<T> & storage) const;
    void _readValsFromFile_oneCol(std::vector<std::string> & vec, TInputFile & file) const;
    void _readValsFromFile_oneRow(std::vector<std::string> & vec, TInputFile & file) const;
    bool _readValsFromFile_oneColOrRow(const std::string& filename, std::vector<T> & storage) const;
    // initial values
    void _readInitVals(const std::string& initVal, bool hasDefaultInitVal);
    T _checkDefaultInitVal(T initVal, bool hasDefaultInitVal);
    // initial jump sizes
    void _readInitJumpSizesVals(bool oneJumpSizeForAll, const std::string& initJumpSize);
    void _initJumpSizeVal_shared(const std::string &initJumpSize);
    void _initJumpSizeVal_unique(const std::string &initJumpSize);

    // initializing boundary and proposal kernel
    void _initBoundary(bool hasDefaultMin, bool hasDefaultMax, const std::string & min, const std::string & max, bool minIncluded, bool maxIncluded);
    template <typename U = T,
            std::enable_if_t<std::is_same_v<U, T>, int> = 0,
            std::enable_if_t<std::is_floating_point<U>::value, int> = 0>
    std::unique_ptr<TPropKernel<T>> _initPropKernel(ProposalKernel::MCMCProposalKernel propKernelDistr){
        // initPropKernel for floating points: we have all the choices
        if (propKernelDistr == ProposalKernel::normal) {
            return std::make_unique<TPropKernelNormal<T>>();
        } if (propKernelDistr == ProposalKernel::uniform) {
            return std::make_unique<TPropKernelUniform<T>>();
        } if (propKernelDistr == ProposalKernel::integer) {
            return std::make_unique<TPropKernelInteger<T>>();
        } if (propKernelDistr == ProposalKernel::boolean) {
            return std::make_unique<TPropKernelBool<T>>();
        } throw std::runtime_error("Can not initialize parameter '" + _name + "': Proposal kernel distribution with name '" + ProposalKernel::proposalKernelToString[propKernelDistr] + "' does not exist!");
    };
    template <typename U = T,
            std::enable_if_t<std::is_same_v<U, T>, int> = 0,
            std::enable_if_t<!std::is_same_v<U, bool>, int> = 0,
            std::enable_if_t<std::is_integral<U>::value, int> = 0>
    std::unique_ptr<TPropKernel<T>> _initPropKernel(ProposalKernel::MCMCProposalKernel propKernelDistr){
        // initPropKernel for integers: only allow proposal kernel for integers
        return std::make_unique<TPropKernelInteger<T>>();
    };
    template <typename U = T,
            std::enable_if_t<std::is_same_v<U, T>, int> = 0,
            std::enable_if_t<std::is_same_v<U, bool>, int> = 0,
            std::enable_if_t<std::is_integral<U>::value, int> = 0>
    std::unique_ptr<TPropKernel<T>> _initPropKernel(ProposalKernel::MCMCProposalKernel propKernelDistr){
        // initPropKernel for booleans: only allow proposal kernel for bools
        return std::make_unique<TPropKernelBool<T>>();
    };

    // updates
    virtual void _propose(const size_t & i); // virtual for mocking
    void _reject(const size_t & i);

    // mean and var
    double _mean(const size_t & i);
    double _var(const size_t & i);
    double _sd(const size_t & i);
    void _addToMeanVar(const size_t & i);

    // propKernel stuff
    void _setPropKernel(T val);
    void _setPropKernel(T val, size_t i);
    void _clearCountersAfterBurnin();
    double _restrictScaleToNumericalLimit(double scale, T jump);
    void _adjustJumpSize(size_t i);
    T _jumpSize(size_t i) const;
    T _jumpSize() const;

    // get values
    void _value(double & dest) const override;
    void _value(float & dest) const override;
    void _value(int & dest) const override;
    void _value(uint8_t & dest) const override;
    void _value(uint16_t & dest) const override;
    void _value(uint32_t & dest) const override;
    void _value(uint64_t & dest) const override;
    void _value(bool & dest) const override;

    void _value(const size_t & i, double & dest) const override;
    void _value(const size_t & i, float & dest) const override;
    void _value(const size_t & i, int & dest) const override;
    void _value(const size_t & i, uint8_t & dest) const override;
    void _value(const size_t & i, uint16_t & dest) const override;
    void _value(const size_t & i, uint32_t & dest) const override;
    void _value(const size_t & i, uint64_t & dest) const override;
    void _value(const size_t & i, bool & dest) const override;

    void _oldValue(double & dest) const override;
    void _oldValue(float & dest) const override;
    void _oldValue(int & dest) const override;
    void _oldValue(uint8_t & dest) const override;
    void _oldValue(uint16_t & dest) const override;
    void _oldValue(uint32_t & dest) const override;
    void _oldValue(uint64_t & dest) const override;
    void _oldValue(bool & dest) const override;

    void _oldValue(const size_t & i, double & dest) const override;
    void _oldValue(const size_t & i, float & dest) const override;
    void _oldValue(const size_t & i, int & dest) const override;
    void _oldValue(const size_t & i, uint8_t & dest) const override;
    void _oldValue(const size_t & i, uint16_t & dest) const override;
    void _oldValue(const size_t & i, uint32_t & dest) const override;
    void _oldValue(const size_t & i, uint64_t & dest) const override;
    void _oldValue(const size_t & i, bool & dest) const override;

    // set values
    template<class U> void _set(U val);
    template<class U> void _set(const size_t &i, U val);

public:
    // initializing
    TMCMCParameter();
    TMCMCParameter(std::shared_ptr<TPrior<T>> Prior, const std::shared_ptr<TParameterDefinition> & Def, TRandomGenerator* RandomGenerator);
    ~TMCMCParameter() override;
    void initializeStorage() override;
    void initializeStorageSingleElementBasedOnPrior() override;
    void initializeStorageBasedOnPrior(const std::vector<size_t> & ExpectedDimensions, const std::vector<std::shared_ptr<TNamesEmpty>> & DimensionNames) override;

    // functions for DAG
    void handPointerToPrior(const std::shared_ptr<TMCMCObservationsBase>& pointer) override;
    void constructDAG(TDAG & DAG, TDAG & temporaryDAG) override;

    // initialization
    void estimateInitialPriorParameters(TLog * Logfile) override;
    void runEMEstimation(TLatentVariable<double, size_t, size_t> &latentVariable, TLog * Logfile) override;
    void switchPriorClassificationAfterEM(TLog * Logfile) override;

    // fill names
    const std::shared_ptr<TNamesEmpty> & getDimensionName(const size_t & Dim) override;

    // getters
    bool isUpdated() const override;

    // values
    double logValue() const override;
    double logValue(const size_t & i) const override;
    double oldLogValue() const override;
    double oldLogValue(const size_t & i) const override;
    double expValue() const override;
    double expValue(const size_t & i) const override;
    double oldExpValue() const override;
    double oldExpValue(const size_t & i) const override;
    size_t getValueUntranslated(const size_t & i) override;

    // indexing and dimensions
    size_t getIndex(const std::vector<size_t>& coord) override;
    TRange getRange(const std::vector<size_t> & startCoord, const std::vector<size_t> & endCoord) override;
    TRange getFull() override;
    TRange getDiagonal() override;
    TRange get1DSlice(size_t dim, const std::vector<size_t> & startCoord) override;
    size_t numDim() const override;
    std::vector<size_t> dimensions() const override;
    size_t totalSize() const override;
    std::vector<size_t> getSubscripts(size_t linearIndex) const override;

    // boundary
    void reSetBoundaries(bool hasDefaultMin, bool hasDefaultMax, std::string min, std::string max, bool minIncluded, bool maxIncluded) override;
    bool valueIsInsideBoundary(const double & Value) const override;
    double min() const override;
    double max() const override;
    bool minIncluded() const;
    bool maxIncluded() const;

    // check for compatible template types
    void typesAreCompatible(bool throwIfNot) const override;
    bool isFloatingPoint() const override;
    bool isIntegral() const override;
    bool isUnsigned() const override;

    // updating
    bool update() override;
    bool update(const size_t & i) override;
    void updatePrior() override;

    // accept or reject
    bool acceptOrReject(const double & log_h) override;
    bool acceptOrReject(const size_t & i, const double & log_h) override;
    bool acceptOrRejectBatch(const TRange & Range, const double & log_h) override;
    double getLogPriorRatio() override;
    double getLogPriorDensity() override;
    double getLogPriorDensityOld() override;
    double getLogPriorRatio(const size_t & i) override;
    double getLogPriorDensity(const size_t & i) override;
    double getLogPriorDensityOld(const size_t & i) override;
    double getLogPriorDensityFull() override;

    // proposal stuff
    void adjustProposalAndResetCounters() override;
    double getAcceptanceRate(size_t i);
    virtual double getAcceptanceRate(); // virtual for mocking
    double getMeanAcceptanceRate();
    void printAccRateToLogfile(TLog* & logfile) override;

    // accessing
    void set(double val) override;
    void set(float val) override;
    void set(int val) override;
    void set(uint8_t val) override;
    void set(uint16_t val) override;
    void set(uint32_t val) override;
    void set(uint64_t val) override;
    void set(bool val) override;
    void set(const size_t & i, double val)  override;
    void set(const size_t & i, float val) override;
    void set(const size_t & i, int val) override;
    void set(const size_t & i, uint8_t val) override;
    void set(const size_t & i, uint16_t val) override;
    void set(const size_t & i, uint32_t val) override;
    void set(const size_t & i, uint64_t val) override;
    void set(const size_t & i, bool val) override;
    void reset() override;
    void reset(const size_t & i) override;
    void setVal(double val) override;
    void setVal(const size_t & i, double val) override;

    // set distances (for HMM prior)
    void setDistances(const std::shared_ptr<TDistancesBinnedBase> & distances) override;

    // simulate values under prior distribution
    void simulateUnderPrior(TLog *logfile, TParameters & Parameters) override;

    // write functions
    void fillHeader(std::vector<std::string> & header) override;
    void writeVals(TOutputFile & file) override;
    void writeValsOneString(TOutputFile & file) override;
    void writeMean(TOutputFile & file) override;
    void writeVar(TOutputFile & file) override;
    void writeJumpSizeOneString(TOutputFile & file) override;
};

// template specializations for bools (declared in cpp file)
template<> void TMCMCParameter<bool>::_initJumpSizeVal_unique(const std::string &initJumpSize);
template<> void TMCMCParameter<bool>::_initJumpSizeVal_shared(const std::string &initJumpSize);
template<> void TMCMCParameter<bool>::_readVals(const std::string& initVal, std::vector<bool> & storage);
template<> void TMCMCParameter<bool>::_adjustJumpSize(size_t i);

#include "TMCMCParameter.tpp"

#endif //MCMCFRAMEWORK_TMCMCPARAMETER_H
