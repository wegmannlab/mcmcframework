//
// Created by madleina on 13.01.21.
//

#ifndef EMPTY_TEMPRIOR_H
#define EMPTY_TEMPRIOR_H

#include "TParameters.h"
#include "TLog.h"
#include "TRandomGenerator.h"
#include "TDataVector.h"
#include <cmath>
#include <vector>

//forward declare TLatentVariable
template <typename PrecisionType, typename NumStatesType, typename LengthType> class TLatentVariable;

//--------------------------------------------
// TEMPrior_base
// A pure virtual class
// Provides a minimal interface for standard EM priors and HMM transition matrices
//--------------------------------------------
template <typename PrecisionType, typename NumStatesType, typename LengthType> class TEMPrior_base{
protected:
    NumStatesType _numStates; //number of states

public:
    TEMPrior_base(){
        _numStates = 0;
    };
    TEMPrior_base(const NumStatesType& NumStates){
        _numStates = NumStates;
    };

    virtual ~TEMPrior_base() = 0;

    //getters
    const NumStatesType numStates() const { return _numStates; };

    //EM initialization (functions can stay empty if parameters should not be initialized prior to EM)
    virtual void initializeEMParameters(const TLatentVariable<PrecisionType, NumStatesType, LengthType> & LatentVariable, const std::vector<LengthType> & JunkEnds){};
    // handleEMParameterInitialization is specific to derived class
    virtual void finalizeEMParameterInitialization(){};

    //EM update
    virtual void prepareEMParameterEstimationInitial(){};
    virtual void prepareEMParameterEstimationOneIteration(){};

    // handleEMParameterEstimationOneIteration is specific to derived class (take different arguments)
    virtual void finalizeEMParameterEstimationOneIteration(){};
    virtual void finalizeEMParameterEstimationFinal(){};
    virtual void reportEMParameters(){};

    //output
    virtual void addStateHeader(std::vector<std::string> & header){
        for(NumStatesType s = 0; s < _numStates; ++s){
            header.push_back("State_" + toString(s+1));
        }
    };
};

template <typename PrecisionType, typename NumStatesType, typename LengthType> TEMPrior_base<PrecisionType, NumStatesType, LengthType>::~TEMPrior_base(){};


#endif //EMPTY_TEMPRIOR_H
