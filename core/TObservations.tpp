
//-------------------------
// TObservations
//-------------------------
template <typename TranslatedType, typename StoredType> TObservationsTranslated<TranslatedType, StoredType>::TObservationsTranslated() : TObservationsBase(){
    this->_name = "default";
    _counterSize = 0;

    // initialize storage
    _storage.init({}, false, false, false);

    // initialize boundary
    _boundary.init(true, true, toString(std::numeric_limits<TranslatedType>::lowest()), toString(std::numeric_limits<TranslatedType>::max()), true, true);
}

template <typename TranslatedType, typename StoredType> TObservationsTranslated<TranslatedType, StoredType>::TObservationsTranslated(const std::shared_ptr<TPrior<TranslatedType>> Prior, const std::shared_ptr<TObservationDefinition> & def, TRandomGenerator* RandomGenerator, std::unique_ptr<TDataTranslator<TranslatedType, StoredType>> & Translator) : TObservationsBase(){
    _translator = std::move(Translator);
    _initialize(Prior, def, RandomGenerator);
}

template <typename TranslatedType, typename StoredType> TObservationsTranslated<TranslatedType, StoredType>::TObservationsTranslated(const std::shared_ptr<TPrior<TranslatedType>> Prior, const std::shared_ptr<TObservationDefinition> & def, TRandomGenerator* RandomGenerator) : TObservationsBase(){
    _translator = nullptr;
    _initialize(Prior, def, RandomGenerator);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::_initialize(const std::shared_ptr<TPrior<TranslatedType>> Prior, const std::shared_ptr<TObservationDefinition> & def, TRandomGenerator* RandomGenerator){
    this->_name = def->name();
    _prior = Prior;
    _randomGenerator = RandomGenerator;
    _counterSize = 0;

    // initialize storage
    _storage.init(def->dimensions(), def->tracksExponential(), def->tracksLog(), false);

    // initialize boundary
    _boundary.init(def->hasDefaultMin(), def->hasDefaultMax(), def->min(), def->max(), def->minIncluded(), def->maxIncluded());
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::handPointerToPrior(const std::shared_ptr<TMCMCObservationsBase> &pointer) {
    _prior->initParameter(pointer);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::constructDAG(TDAG &DAG, TDAG & temporaryDAG) {
    temporaryDAG.add(shared_from_this());
    _prior->constructDAG(DAG, temporaryDAG);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::initializeStorage() {
    _prior->initializeStorageOfPriorParameters();
}


template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::setDistances(const std::shared_ptr<TDistancesBinnedBase> &distances) {
    _prior->initDistances(distances);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::initializeStorageSingleElementBasedOnPrior() {
    // TODO: I think this function should never be called, as storage ob observation is filled by developer and prior doesn't have control of it -> but maybe I'm wrong!!
    throw std::runtime_error("In function 'template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::initializeStorageSingleElementBasedOnPrior()': Should never be called.");
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::initializeStorageBasedOnPrior(const std::vector<size_t> &ExpectedDimensions, const std::vector<std::shared_ptr<TNamesEmpty>> & DimensionNames) {
    // TODO: I think this function should never be called, as storage ob observation is filled by developer and prior doesn't have control of it -> but maybe I'm wrong!!
    throw std::runtime_error("In function 'template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::initializeStorageBasedOnPrior(const std::vector<size_t> &ExpectedDimensions, const std::vector<std::shared_ptr<TNamesBase>> & DimensionNames) {': Should never be called.");
}

template <typename TranslatedType, typename StoredType> inline TranslatedType TObservationsTranslated<TranslatedType, StoredType>::_getValue(const size_t & i) const {
    // get value and translate
    StoredType value = this->_storage.value(i);
    return _translator->translate(value);
}

template <typename TranslatedType, typename StoredType> inline void TObservationsTranslated<TranslatedType, StoredType>::_value(double &dest) const {
    dest = static_cast<double>(_getValue(0));
}

template <typename TranslatedType, typename StoredType> inline void TObservationsTranslated<TranslatedType, StoredType>::_value(float &dest) const {
    dest = static_cast<float>(_getValue(0));
}

template <typename TranslatedType, typename StoredType> inline void TObservationsTranslated<TranslatedType, StoredType>::_value(int &dest) const {
    dest = static_cast<int>(_getValue(0));
}

template <typename TranslatedType, typename StoredType> inline void TObservationsTranslated<TranslatedType, StoredType>::_value(uint8_t &dest) const {
    dest = static_cast<uint8_t>(_getValue(0));
}

template <typename TranslatedType, typename StoredType> inline void TObservationsTranslated<TranslatedType, StoredType>::_value(uint16_t &dest) const {
    dest = static_cast<uint16_t>(_getValue(0));
}

template <typename TranslatedType, typename StoredType> inline void TObservationsTranslated<TranslatedType, StoredType>::_value(uint32_t &dest) const {
    dest = static_cast<uint32_t>(_getValue(0));
}

template <typename TranslatedType, typename StoredType> inline void TObservationsTranslated<TranslatedType, StoredType>::_value(uint64_t &dest) const {
    dest = static_cast<uint64_t>(_getValue(0));
}

template <typename TranslatedType, typename StoredType> inline void TObservationsTranslated<TranslatedType, StoredType>::_value(bool &dest) const {
    dest = static_cast<bool>(_getValue(0));
}

template <typename TranslatedType, typename StoredType> inline void TObservationsTranslated<TranslatedType, StoredType>::_value(const size_t & i, double &dest) const {
    dest = static_cast<double>(_getValue(i));
}

template <typename TranslatedType, typename StoredType> inline void TObservationsTranslated<TranslatedType, StoredType>::_value(const size_t & i, float &dest) const {
    dest = static_cast<float>(_getValue(i));
}

template <typename TranslatedType, typename StoredType> inline void TObservationsTranslated<TranslatedType, StoredType>::_value(const size_t & i, int &dest) const {
    dest = static_cast<int>(_getValue(i));
}

template <typename TranslatedType, typename StoredType> inline void TObservationsTranslated<TranslatedType, StoredType>::_value(const size_t & i, uint8_t &dest) const {
    dest = static_cast<uint8_t>(_getValue(i));
}

template <typename TranslatedType, typename StoredType> inline void TObservationsTranslated<TranslatedType, StoredType>::_value(const size_t & i, uint16_t &dest) const {
    dest = static_cast<uint16_t>(_getValue(i));
}

template <typename TranslatedType, typename StoredType> inline void TObservationsTranslated<TranslatedType, StoredType>::_value(const size_t & i, uint32_t &dest) const {
    dest = static_cast<uint32_t>(_getValue(i));
}

template <typename TranslatedType, typename StoredType> inline void TObservationsTranslated<TranslatedType, StoredType>::_value(const size_t & i, uint64_t &dest) const {
    dest = static_cast<uint64_t>(_getValue(i));
}

template <typename TranslatedType, typename StoredType> inline void TObservationsTranslated<TranslatedType, StoredType>::_value(const size_t & i, bool &dest) const {
    dest = static_cast<bool>(_getValue(i));
}

template <typename TranslatedType, typename StoredType> inline double TObservationsTranslated<TranslatedType, StoredType>::logValue() const {
    // return log of translated value
    return log(_getValue(0));
}

template <typename TranslatedType, typename StoredType> inline double TObservationsTranslated<TranslatedType, StoredType>::logValue(const size_t & i) const {
    // return log of translated value
    return log(_getValue(i));
}

template <typename TranslatedType, typename StoredType> inline double TObservationsTranslated<TranslatedType, StoredType>::expValue() const {
    // return exp of translated value
    return exp(_getValue(0));
}

template <typename TranslatedType, typename StoredType> inline double TObservationsTranslated<TranslatedType, StoredType>::expValue(const size_t & i) const {
    // return exp of translated value
    return exp(_getValue(i));
}

template <typename TranslatedType, typename StoredType> inline size_t TObservationsTranslated<TranslatedType, StoredType>::getValueUntranslated(const size_t &i) {
    // get value and DON'T translate
    StoredType value = this->_storage.value(i);
    return static_cast<size_t>(value);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::prepareFillData(const size_t & guessLengthOfUnknownDimension, const std::vector<size_t> & allKnownDimensions){
    // idea: usually we know all but one dimension
    // this unknown dimension must be the most outer one (i.e. the first one, for example the number of rows in a matrix)
    // usually we can approximately know how long this unknown dimension will be (e.g. 1 oder 1'000'000) -> give this guess as a second parameter
    // will resize after all data have been filled in

    // check if any dimension is = 0 (invalid)
    if (guessLengthOfUnknownDimension < 1){
        throw std::runtime_error("In function 'template <class T> void TObservations<T>::prepareFillData(const size_t & guessLengthOfUnknownDimension, const std::vector<size_t> & allKnownDimensions)': Invalid guess of first (unknown) dimension of observation " + this->_name + ": Size should be > 0!");
    }
    for (auto & dim : allKnownDimensions){
        if (dim == 0){
            throw std::runtime_error("In function 'template <class T> void TObservations<T>::prepareFillData(const size_t & guessLengthOfUnknownDimension, const std::vector<size_t> & allKnownDimensions)': Invalid length of dimension vector of observation " + this->_name + ": Size should be > 0!");
        }
    }

    // clear previously allocated memory of dimensions
    _storage.resize({0});

    // create vector of dimensions: first element is unknown dimension, all others are from vector
    std::vector<size_t> dimensions = {guessLengthOfUnknownDimension};
    dimensions.insert( dimensions.end(), allKnownDimensions.begin(), allKnownDimensions.end() );
    _storage.reserve(dimensions);
    _counterSize = 0;
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::resize(const std::vector<size_t> &Dimensions) {
    _storage.resize(Dimensions);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::_fillDataRaw(const StoredType & value){
    // data must be given ordered - i.e. no re-ordering will take place!
    // directly fills storage (without translating back)
    _storage.emplace_back(value);
    // increase counter
    _counterSize++;
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::_fillDataAndTranslate(const TranslatedType & value){
    // data must be given ordered - i.e. no re-ordering will take place!
    // first translates to StoredType, and then stores
    StoredType valStored = _translator->translateToStoredType(value);
    _storage.emplace_back(valStored);
    // increase counter
    _counterSize++;
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::finalizeFillData(){
    // determine length of first dimension

    // 1) get total size of all known dimensions
    double totalSizeAllButFirstDim = 1;
    if (_storage.numDim() > 1) {
        for (size_t dim = 1; dim < _storage.numDim(); dim++){
            totalSizeAllButFirstDim *= _storage.dimensions()[dim]; // product of all but first dimension
        }
    }
    // 2) from this, we can calculate the size of the first dimension
    double lengthFirst = static_cast<double>(_counterSize)/totalSizeAllButFirstDim;

    // check if integer number
    if (static_cast<size_t>(lengthFirst) != lengthFirst) {
        throw std::runtime_error("Error while filling data: Data seems to be ragged. Expected the total number of data points to be a multiple of " +
              toString(totalSizeAllButFirstDim) + ", but got a factor of " + toString(lengthFirst) +
              " which is not a integer number.");
    }

    // resize storage to only contain filled values (std::vector will probably allocate more inside push_back than we actually need in the end)
    std::vector<size_t> dimensions = _storage.dimensions();
    dimensions[0] = static_cast<size_t>(lengthFirst);
    _storage.resize(dimensions);

    // set _hasFixedInitialValue to true: we have initialized all values, should not be overwritten
    this->_hasFixedInitialValue = true;
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::setDimensionName(const std::shared_ptr<TNamesEmpty> &Name, const size_t &Dim) {
    // check if storage is prepared (does not need to be filled yet, but we need to know how many dimensions there are, i.e. prepareFillData must have been called)
    if (_storage.numDim() < 1){
        throw std::runtime_error("In function 'TObservationsTranslated<TranslatedType, StoredType>::setDimensionNames()': Can not set name for observation " + _name + " because storage is not initialized yet (numDim = " + toString(_storage.numDim()) + ")!");
    }

    _storage.setDimensionName(Name, Dim);
}

template <typename TranslatedType, typename StoredType> const std::shared_ptr<TNamesEmpty> & TObservationsTranslated<TranslatedType, StoredType>::getDimensionName(const size_t &Dim) {
    return _storage.getDimensionName(Dim);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::fillDataRaw(const double & value){
    _fillDataRaw(static_cast<StoredType>(value));
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::fillDataRaw(const float & value){
    _fillDataRaw(static_cast<StoredType>(value));
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::fillDataRaw(const int & value){
    _fillDataRaw(static_cast<StoredType>(value));
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::fillDataRaw(const uint8_t & value){
    _fillDataRaw(static_cast<StoredType>(value));
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::fillDataRaw(const uint16_t & value){
    _fillDataRaw(static_cast<StoredType>(value));
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::fillDataRaw(const uint32_t & value){
    _fillDataRaw(static_cast<StoredType>(value));
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::fillDataRaw(const uint64_t & value){
    _fillDataRaw(static_cast<StoredType>(value));
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::fillDataRaw(const bool & value){
    _fillDataRaw(static_cast<StoredType>(value));
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::fillDataAndTranslate(const double & value){
    _fillDataAndTranslate(static_cast<TranslatedType>(value));
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::fillDataAndTranslate(const float & value){
    _fillDataAndTranslate(static_cast<TranslatedType>(value));
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::fillDataAndTranslate(const int & value){
    _fillDataAndTranslate(static_cast<TranslatedType>(value));
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::fillDataAndTranslate(const uint8_t & value){
    _fillDataAndTranslate(static_cast<TranslatedType>(value));
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::fillDataAndTranslate(const uint16_t & value){
    _fillDataAndTranslate(static_cast<TranslatedType>(value));
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::fillDataAndTranslate(const uint32_t & value){
    _fillDataAndTranslate(static_cast<TranslatedType>(value));
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::fillDataAndTranslate(const uint64_t & value){
    _fillDataAndTranslate(static_cast<TranslatedType>(value));
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::fillDataAndTranslate(const bool & value){
    _fillDataAndTranslate(static_cast<TranslatedType>(value));
}

template <typename TranslatedType, typename StoredType> size_t TObservationsTranslated<TranslatedType, StoredType>::getIndex(const std::vector<size_t> &coord) {
    return _storage.getIndex(coord);
}

template <typename TranslatedType, typename StoredType> TRange TObservationsTranslated<TranslatedType, StoredType>::getRange(const std::vector<size_t> &startCoord, const std::vector<size_t> &endCoord) {
    return _storage.getRange(startCoord, endCoord);
}

template <typename TranslatedType, typename StoredType> TRange TObservationsTranslated<TranslatedType, StoredType>::getFull() {
    return _storage.getFull();
}

template <typename TranslatedType, typename StoredType> TRange TObservationsTranslated<TranslatedType, StoredType>::getDiagonal() {
    return _storage.getDiagonal();
}

template <typename TranslatedType, typename StoredType> TRange TObservationsTranslated<TranslatedType, StoredType>::get1DSlice(size_t dim, const std::vector<size_t> &startCoord) {
    return _storage.get1DSlice(dim, startCoord);
}

template <typename TranslatedType, typename StoredType> size_t TObservationsTranslated<TranslatedType, StoredType>::numDim() const {
    return _storage.numDim();
}

template <typename TranslatedType, typename StoredType> std::vector<size_t> TObservationsTranslated<TranslatedType, StoredType>::dimensions() const {
    return _storage.dimensions();
}

template <typename TranslatedType, typename StoredType> size_t TObservationsTranslated<TranslatedType, StoredType>::totalSize() const {
    return _storage.totalSize();
}

template <typename TranslatedType, typename StoredType> std::vector<size_t> TObservationsTranslated<TranslatedType, StoredType>::getSubscripts(size_t linearIndex) const {
    return _storage.getSubscripts(linearIndex);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::estimateInitialPriorParameters(TLog *Logfile) {
    _prior->estimateInitialPriorParameters(Logfile);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::reSetBoundaries(bool hasDefaultMin, bool hasDefaultMax, std::string min, std::string max, bool minIncluded, bool maxIncluded) {
    _boundary.resetBoundary(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::typesAreCompatible(bool throwIfNot) const {
    _prior->typesAreCompatible(throwIfNot);
}

template <typename TranslatedType, typename StoredType> bool TObservationsTranslated<TranslatedType, StoredType>::valueIsInsideBoundary(const double & Value) const {
    // check if value is within range of TranslatedType
    return _boundary.checkRange(static_cast<TranslatedType>(Value));
}

template <typename TranslatedType, typename StoredType> bool TObservationsTranslated<TranslatedType, StoredType>::isFloatingPoint() const {
    // checks for compatibility of translated type, because this is the type that is "visible" from outside
    return std::is_floating_point<TranslatedType>::value;
}

template <typename TranslatedType, typename StoredType> bool TObservationsTranslated<TranslatedType, StoredType>::isIntegral() const {
    // checks for compatibility of translated type, because this is the type that is "visible" from outside
    return std::is_integral<TranslatedType>::value;
}

template <typename TranslatedType, typename StoredType> bool TObservationsTranslated<TranslatedType, StoredType>::isUnsigned() const {
    // checks for compatibility of translated type, because this is the type that is "visible" from outside
    return std::is_unsigned<TranslatedType>::value;
}

template <typename TranslatedType, typename StoredType>
template <class U>
void TObservationsTranslated<TranslatedType, StoredType>::_setAndTranslateBack(const size_t & i, U val){
    if (i >= this->_storage.totalSize()) throw std::runtime_error("Set() for index i = " + toString(i) + " for array " + this->_name + " is not possible! (Size of array = " + toString(this->_storage.totalSize()) + ").");
    // check if value is within range of TranslatedType
    _boundary.checkRangeAndThrow(static_cast<TranslatedType>(val), this->_name);

    // translate to StorageType
    StoredType tmp = _translator->translateToStoredType(val);
    this->_storage.set(i, tmp);
}

template <typename TranslatedType, typename StoredType>
template <class U>
void TObservationsTranslated<TranslatedType, StoredType>::_setAndTranslateBack(U val){
    // sets value of first element
    _setAndTranslateBack(0, val);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::set(double val){
    _setAndTranslateBack(val);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::set(float val){
    _setAndTranslateBack(val);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::set(int val){
    _setAndTranslateBack(val);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::set(uint8_t val){
    _setAndTranslateBack(val);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::set(uint16_t val){
    _setAndTranslateBack(val);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::set(uint32_t val){
    _setAndTranslateBack(val);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::set(uint64_t val){
    _setAndTranslateBack(val);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::set(bool val){
    _setAndTranslateBack(val);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::set(const size_t & i, double val){
    _setAndTranslateBack(i, val);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::set(const size_t & i, float val){
    _setAndTranslateBack(i, val);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::set(const size_t & i, int val){
    _setAndTranslateBack(i, val);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::set(const size_t & i, uint8_t val){
    _setAndTranslateBack(i, val);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::set(const size_t & i, uint16_t val){
    _setAndTranslateBack(i, val);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::set(const size_t & i, uint32_t val){
    _setAndTranslateBack(i, val);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::set(const size_t & i, uint64_t val){
    _setAndTranslateBack(i, val);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::set(const size_t & i, bool val){
    _setAndTranslateBack(i, val);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::updatePrior() {
    _prior->updateParams();
}

template <typename TranslatedType, typename StoredType> double TObservationsTranslated<TranslatedType, StoredType>::getLogPriorDensity(){
    // = likelihood: P(observations | prior)
    // returns getLogPriorDensity of first element
    return getLogPriorDensity(0);
}

template <typename TranslatedType, typename StoredType> double TObservationsTranslated<TranslatedType, StoredType>::getLogPriorDensity(const size_t & i){
    // = likelihood: P(observations | prior)
    if (i >= this->_storage.totalSize())
        throw std::runtime_error("Index i = " + toString(i) + " for array " + this->_name + " does not exist! (Size of array = " + toString(this->_storage.totalSize()) + ").");
    return _prior->getLogPriorDensity(shared_from_this(), i);
}

template <typename TranslatedType, typename StoredType> double TObservationsTranslated<TranslatedType, StoredType>::getLogPriorDensityFull(){
    return _prior->getLogPriorDensityFull(shared_from_this());
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::fillHeader(std::vector<std::string> &header) {
    _storage.appendToVectorOfAllFullDimensionNamesWithPrefix(header, _name);
}

template <typename TranslatedType, typename StoredType> void TObservationsTranslated<TranslatedType, StoredType>::writeVals(TOutputFile &file) {
    // in case of translated observations: write values as StoredType -> compatible with fillData!
    // TODO ok?
    TRange range = _storage.getFull();
    for (size_t i = range.first; i < range.last; i+= range.increment)
        file << toString(_storage.value(i)); // use toString in order to print unsigned ints to file
    // without needing to cast them to a smaller container (e.g. if T=uint64_t and we would cast to int here -> cut off)
}

template <typename TranslatedType, typename StoredType> double TObservationsTranslated<TranslatedType, StoredType>::min() const {
    return static_cast<double>(_boundary.min());
}

template <typename TranslatedType, typename StoredType> double TObservationsTranslated<TranslatedType, StoredType>::max() const {
    return static_cast<double>(_boundary.max());
}

template <typename TranslatedType, typename StoredType>
void TObservationsTranslated<TranslatedType, StoredType>::simulateUnderPrior(TLog *logfile, TParameters & Parameters) {
    // fill storage with values randomly drawn from prior distribution
    _prior->simulateUnderPrior(this->_randomGenerator, logfile, Parameters);
}

//-------------------------
// TObservationsRaw
//-------------------------
template <class T> TObservationsRaw<T>::TObservationsRaw() : TObservationsTranslated<T, T>(){
}

template <class T> TObservationsRaw<T>::TObservationsRaw(const std::shared_ptr<TPrior<T>> Prior, const std::shared_ptr<TObservationDefinition> & def, TRandomGenerator* RandomGenerator) : TObservationsTranslated<T, T>(Prior, def, RandomGenerator){
}

template <typename T> void TObservationsRaw<T>::_fillDataAndTranslate(const T &value) {
    // just don't translate
    this->_fillDataRaw(value);
}

template <class T> inline T TObservationsRaw<T>::_getValue(const size_t & i) const {
    return this->_storage.value(i);
}

template <class T> inline double TObservationsRaw<T>::logValue() const {
    // just returns value of first element
    return this->_storage.logValue();
}

template <class T> inline double TObservationsRaw<T>::logValue(const size_t & i) const {
    return this->_storage.logValue(i);
}

template <class T> inline double TObservationsRaw<T>::expValue() const {
    // just returns value of first element
    return this->_storage.expValue();
}

template <class T> inline double TObservationsRaw<T>::expValue(const size_t & i) const {
    return this->_storage.expValue(i);
}

template <class T>
template <class U>
void TObservationsRaw<T>::_set(const size_t & i, U val){
    if (i >= this->_storage.totalSize()) throw std::runtime_error("Set() for index i = " + toString(i) + " for array " + this->_name + " is not possible! (Size of array = " + toString(this->_storage.totalSize()) + ").");
    this->_storage.set(i, static_cast<T>(val));
    this->_boundary.checkRangeAndThrow(this->_storage.value(i), this->_name);
}

template <class T>
template <class U>
void TObservationsRaw<T>::_set(U val){
    // sets value of first element
    _set(0, val);
}

template <class T> void TObservationsRaw<T>::set(double val){
    _set(val);
}

template <class T> void TObservationsRaw<T>::set(float val){
    _set(val);
}

template <class T> void TObservationsRaw<T>::set(int val){
    _set(val);
}

template <class T> void TObservationsRaw<T>::set(uint8_t val){
    _set(val);
}

template <class T> void TObservationsRaw<T>::set(uint16_t val){
    _set(val);
}

template <class T> void TObservationsRaw<T>::set(uint32_t val){
    _set(val);
}

template <class T> void TObservationsRaw<T>::set(uint64_t val){
    _set(val);
}

template <class T> void TObservationsRaw<T>::set(bool val){
    _set(val);
}

template <class T> void TObservationsRaw<T>::set(const size_t & i, double val){
    _set(i, val);
}

template <class T> void TObservationsRaw<T>::set(const size_t & i, float val){
    _set(i, val);
}

template <class T> void TObservationsRaw<T>::set(const size_t & i, int val){
    _set(i, val);
}

template <class T> void TObservationsRaw<T>::set(const size_t & i, uint8_t val){
    _set(i, val);
}

template <class T> void TObservationsRaw<T>::set(const size_t & i, uint16_t val){
    _set(i, val);
}

template <class T> void TObservationsRaw<T>::set(const size_t & i, uint32_t val){
    _set(i, val);
}

template <class T> void TObservationsRaw<T>::set(const size_t & i, uint64_t val){
    _set(i, val);
}

template <class T> void TObservationsRaw<T>::set(const size_t & i, bool val){
    _set(i, val);
}