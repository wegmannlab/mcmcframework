//
// Created by madleina on 11.12.20.
//

#ifndef MCMCFRAMEWORK_TPROPOSALKERNEL_H
#define MCMCFRAMEWORK_TPROPOSALKERNEL_H

#include "MCMCFrameworkVariables.h"
#include "TBoundary.h"
#include "TRandomGenerator.h"
#include <cassert>

//-------------------------------------------
// TPropKernel
//-------------------------------------------

template <class T>
class TPropKernel {
protected:
    ProposalKernel::MCMCProposalKernel _name;
    T _maxPropKernel(T range) const{
        // prevent overshooting while mirroring: maximal proposal width is range/2
        return static_cast<T>(range / 2.);
    };

    // default proposal width for float/double: 0.1
    template <typename U = T,
            std::enable_if_t<std::is_same_v<U, T>, int> = 0,
            std::enable_if_t<std::is_floating_point<U>::value, int> = 0>
    T _defaultPropKernel(){
        return 0.1;
    }

    // default proposal width for int/uint: 1
    template <typename U = T,
            std::enable_if_t<std::is_same_v<U, T>, int> = 0,
            std::enable_if_t<std::is_integral<U>::value, int> = 0>
    T _defaultPropKernel(){
        return 1;
    }

public:
    explicit TPropKernel() = default;
    virtual ~TPropKernel() = default;
    virtual T propose(const T & value, const TBoundary<T>& boundary, const T & propSd, TRandomGenerator* randomGenerator){
        throw std::runtime_error("TPropKernel is a base class that can not propose!");
    };

    virtual T adjustPropKernelIfTooBig(T propKernel, const TBoundary<T> & boundary, const std::string& name){
        if (propKernel < 0) {
            throw "Proposal width (" + toString(propKernel) + ") for parameter '" + name + "' is negative!";
        } else if (propKernel > _maxPropKernel(boundary.range())) {
            propKernel = _maxPropKernel(boundary.range());
        } else if (propKernel == 0) { // if parameter was fixed and you re-start from initVals file, initJumpSize will be 0 -> adjust this automatically
            propKernel = _defaultPropKernel();
        }

        return propKernel;
    };

    ProposalKernel::MCMCProposalKernel name(){
        return _name;
    };
};

template<class T>
class TPropKernelNormal : public TPropKernel<T> {
public:
    template <typename U = T,
            std::enable_if_t<std::is_same_v<U, T>, int> = 0,
            std::enable_if_t<std::is_floating_point<U>::value, int> = 0>
    explicit TPropKernelNormal(){
        // constructor for floating point numbers: doesn't throw
        this->_name = ProposalKernel::normal;
    }
    template <typename U = T,
            std::enable_if_t<std::is_same_v<U, T>, int> = 0,
            std::enable_if_t<std::is_integral<U>::value, int> = 0>
    explicit TPropKernelNormal(){
        // constructor for integers: throws
        throw std::runtime_error("Can not create class TPropKernelNormal for parameter of type " + std::string(typeid(T).name()) + "!");
    }

    ~TPropKernelNormal() override = default;
    T propose(const T & value, const TBoundary<T>& boundary, const T & propSd, TRandomGenerator* randomGenerator) override{
        // assert that propSd is restricted (this does not fully prevent issues with overshooting and numerical overflow
        // , but at least limits them to few cases -> proper handling is done in while-loop below)
        assert(propSd <= boundary.range()/2.);

        // random value should always be within [-(max-min)/2, +(max-min)/2] -> prevents issues with overshooting while mirroring & numerical overflow
        double jump = randomGenerator->getNormalRandom(0, propSd);
        while (jump < (-this->_maxPropKernel(boundary.range())) || jump > this->_maxPropKernel(boundary.range())){
            jump = randomGenerator->getNormalRandom(0, propSd);
        }

        // now mirror
        return boundary.updateAndMirror(value, jump);
    };
};

template<class T>
class TPropKernelUniform : public TPropKernel<T> {
public:
    template <typename U = T,
            std::enable_if_t<std::is_same_v<U, T>, int> = 0,
            std::enable_if_t<std::is_floating_point<U>::value, int> = 0>
    explicit TPropKernelUniform(){
        // constructor for floating point numbers: doesn't throw
        this->_name = ProposalKernel::uniform;
    };
    template <typename U = T,
            std::enable_if_t<std::is_same_v<U, T>, int> = 0,
            std::enable_if_t<std::is_integral<U>::value, int> = 0>
    explicit TPropKernelUniform(){
        // constructor for integers: throws
        throw std::runtime_error("Can not create class TPropKernelUniform for parameter of type " + std::string(typeid(T).name()) + "!");
    }

    ~TPropKernelUniform() override = default;
    T propose(const T & value, const TBoundary<T>& boundary, const T & propWidth, TRandomGenerator* randomGenerator) override{
        // assert that propWidth is restricted (otherwise we have issues with overshooting and numerical overflow)
        assert(propWidth <= boundary.range()/2.);

        double jump = randomGenerator->getRand() * propWidth - propWidth / 2.0;

        // now mirror
        return boundary.updateAndMirror(value, jump);
    };
};

template<class T>
class TPropKernelInteger : public TPropKernel<T> {
protected:
    std::vector<double> _cumulProbs;

    void _fillCumulProbabilities(const T &propWidth){
        size_t numElements = propWidth*2 + 1;
        _cumulProbs.resize(numElements);
        for (size_t i = 0; i < numElements; i++){
            _cumulProbs[i] = static_cast<double>(i+1.)/static_cast<double>(numElements);
        }
    };

public:
    template <typename U = T,
            std::enable_if_t<std::is_same_v<U, T>, int> = 0,
            std::enable_if_t<!std::is_same_v<U, bool>, int> = 0,
            std::enable_if_t<std::is_integral<U>::value, int> = 0>
    explicit TPropKernelInteger(){
        // constructor for integers: doesn't throw
        this->_name = ProposalKernel::integer;
    };
    template <typename U = T,
            std::enable_if_t<std::is_same_v<U, T>, int> = 0,
            std::enable_if_t<std::is_same_v<U, bool>, int> = 0,
            std::enable_if_t<std::is_integral<U>::value, int> = 0>
    explicit TPropKernelInteger(){
        // constructor for bools: throw
        throw std::runtime_error("Can not create class TPropKernelInteger for parameter of type " + std::string(typeid(T).name()) + "!");
    };
    template <typename U = T,
            std::enable_if_t<std::is_same_v<U, T>, int> = 0,
            std::enable_if_t<std::is_floating_point<U>::value, int> = 0>
    explicit TPropKernelInteger(){
        // constructor for floating point numbers: throw
        throw std::runtime_error("Can not create class TPropKernelInteger for parameter of type " + std::string(typeid(T).name()) + "!");
    };

    ~TPropKernelInteger() override = default;
    T propose(const T & value, const TBoundary<T>& boundary, const T & propWidth, TRandomGenerator* randomGenerator) override{
        // assert that propWidth is restricted (otherwise we have issues with overshooting and numerical overflow)
        assert(propWidth <= boundary.range()/2.);

        // pick one element from -propWidth:propWidth
        // e.g. propWidth = 3 -> get values 0, 1, 2, 3, 4, 5, 6 with equal probabilities from pickOne (given by cumulProbs)
        // -> shift by propWidth to have it symmetric around 0 (-> results in value -3, -2, -1, 0, 1, 2, 3)
        // -> if jump = 0 -> draw another number, because 0 is just a waste, we don't update the value at all
        int64_t jump = static_cast<int64_t>(randomGenerator->pickOne(_cumulProbs)) - propWidth;
        while (jump == 0){
            jump = static_cast<int64_t>(randomGenerator->pickOne(_cumulProbs)) - propWidth;
        }

        // mirror
        return boundary.updateAndMirror(value, jump);
    };

    T adjustPropKernelIfTooBig(T propKernel, const TBoundary<T> & boundary, const std::string& name) override{
        // overridden to fill cumulative probabilities whenever propKernel is changed
        T adjustedPropKernel = TPropKernel<T>::adjustPropKernelIfTooBig(propKernel, boundary, name);

        // fill cumulative probabilities
        _fillCumulProbabilities(adjustedPropKernel);
        return adjustedPropKernel;
    }
};

template<class T>
class TPropKernelBool : public TPropKernel<T> {
public:
    template <typename U = T,
            std::enable_if_t<std::is_same_v<U, T>, int> = 0,
            std::enable_if_t<std::is_same_v<U, bool>, int> = 0,
            std::enable_if_t<std::is_integral<U>::value, int> = 0>
    explicit TPropKernelBool(){
        // constructor for bools: doesn't throw
        this->_name = ProposalKernel::boolean;
    };
    template <typename U = T,
            std::enable_if_t<std::is_same_v<U, T>, int> = 0,
            std::enable_if_t<!std::is_same_v<U, bool>, int> = 0>
    explicit TPropKernelBool(){
        // constructor for everything that is not a bool
        throw std::runtime_error("Can not create class TPropKernelBool for parameter of type " + std::string(typeid(T).name()) + "!");
    };

    ~TPropKernelBool() override = default;
    T propose(const T & value, const TBoundary<T>& boundary, const T & propWidth, TRandomGenerator* randomGenerator) override{
        return 1-value;
    };
};

#endif //MCMCFRAMEWORK_TPROPOSALKERNEL_H
