//
// Created by madleina on 15.07.20.
//

#ifndef MCMCFRAMEWORK_TPRIOR_H
#define MCMCFRAMEWORK_TPRIOR_H

#include "TMCMCParameterDef.h"
#include "TBoundary.h"
#include "TTransitionMatrixExponential.h"
#include "counters.h"
#include "TDAG.h"
#include "TMCMCParameterBase.h"

#define cutoffSimulateUniformPrior 1000

//-------------------------------------------
// TPrior
//-------------------------------------------

template <class T>
class TPrior{
protected:
    std::string _name;
    std::vector<std::weak_ptr<TMCMCObservationsBase>> _parameters; // pointer to parameter(s) on which this prior is specified (multiple parameters can have the same prior, i.e. relevant for priorWithHyperPrior)

    // read parameters from string
    virtual std::vector<double> _processPriorParameterString(const std::string& params, int expectedSize, bool check = true);

    // checks
    void _checkTypes(bool throwIfNot, bool mustBeFloatingPoint, bool mustBeIntegral, bool mustBeUnsigned);
    std::shared_ptr<TMCMCObservationsBase> _getSharedPtrParam(std::weak_ptr<TMCMCObservationsBase> & param);

    // construct DAG
    bool _allParamChildrenExistInDAG(const TDAG &DAG);
    void _addFirstChildToDAG(TDAG &DAG);
    void _constructDAG(TDAG &DAG, TDAG & temporaryDAG, const std::vector<std::shared_ptr<TMCMCObservationsBase>> & PriorParameters);

    // log prior densities and ratios (separated in protected virtual and public non-virtual classes for proper dealing with hiding rule,
    // see https://isocpp.org/wiki/faq/strange-inheritance)
    virtual double _getLogPriorDensity_oneVal(T x);
    virtual double _getLogPriorRatio_oneVal(T x, T x_old);
    virtual double _getLogPriorRatio_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index);
    virtual double _getLogPriorDensity_vec(const std::shared_ptr<TMCMCObservationsBase> & data, const size_t & index);
    virtual double _getLogPriorDensityOld_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index);

public:
    TPrior()= default;
    virtual ~TPrior()= default;

    // initialize
    virtual void initialize(const std::string & params);

    // construct DAG
    virtual void initializeStorageOfPriorParameters();
    virtual void initParameter(const std::shared_ptr<TMCMCObservationsBase> & parameter);
    virtual void constructDAG(TDAG & DAG, TDAG & temporaryDAG);

    // initial values with MLE/EM
    // not all priors need to do EM, if they can't, nothing happens
    virtual void runEMEstimation(TLatentVariable<double, size_t, size_t> & latentVariable, TLog * Logfile){};
    virtual void switchPriorClassificationAfterEM(TLog * Logfile);

    // getters
    std::string name();

    // check boundaries and types
    virtual bool checkMinMax(std::string &min, std::string &max, bool & minIsIncluded, bool & maxIsIncluded, bool & hasDefaultMin, bool & hasDefaultMax);
    virtual void typesAreCompatible(bool throwIfNot);

    // log prior densities and ratios (separated in protected virtual and public non-virtual classes for proper dealing with hiding rule,
    // see https://isocpp.org/wiki/faq/strange-inheritance)
    double getLogPriorDensity(T x);
    double getLogPriorRatio(T x, T x_old);
    double getLogPriorDensity(const std::shared_ptr<TMCMCObservationsBase> & data, const size_t & index);
    double getLogPriorDensityOld(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index);
    double getLogPriorRatio(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index);

    // stuff for prior with hyper prior
    virtual void updateParams() {};
    virtual void initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, std::vector<std::shared_ptr<TObservationsBase> > & observations){};
    virtual void initDistances(const std::shared_ptr<TDistancesBinnedBase> & distances){ /* function stays empty if prior does not require distances */ };
    virtual void setAdditionalParameters(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters){ /* function stays empty if prior does not require additional user-defined parameters */ };
    virtual void estimateInitialPriorParameters(TLog * Logfile){};
    // return sum of log-densities of entire vector
    virtual double getSumLogPriorDensity(const std::shared_ptr<TMCMCObservationsBase> & data);
    // return sum of log-densities of entire vector + all log-densities of priors
    virtual double getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data);

    // simulate values
    virtual void simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters);
};

template <class T>
class TPriorUniform: virtual public TPrior<T>{
protected:
    T _min, _max;
    double _logDensity;

    double _getLogPriorDensity_oneVal(T x) override;
    double _getLogPriorRatio_oneVal(T x, T x_old) override;
public:
    TPriorUniform();
    ~TPriorUniform() override = default;
    void initialize(const std::string & params) override ;
    bool checkMinMax(std::string &min, std::string &max, bool & minIsIncluded, bool & maxIsIncluded, bool & hasDefaultMin, bool & hasDefaultMax) override;
    void typesAreCompatible(bool throwIfNot) override;

    // simulate values
    void simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) override;
};

template <class T>
class TPriorNormal: virtual public TPrior<T>{
private:
    double _mean, _var;
protected:
    double _twoVar;
    double _sqrtTwoPi;
    double _getLogPriorDensity_oneVal(T x) override;
    double _getLogPriorRatio_oneVal(T x, T x_old) override;
public:
    TPriorNormal();
    ~TPriorNormal() override = default;
    void initialize(const std::string & params) override ;
    void typesAreCompatible(bool throwIfNot) override;
    virtual double mean() const;
    virtual double var() const;

    // simulate values
    void simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) override;
};

template <class T>
class TPriorExponential: virtual public TPrior<T>{
private:
    double _lambda;
protected:
    double _logLambda;
    double _getLogPriorDensity_oneVal(T x) override;
    double _getLogPriorRatio_oneVal(T x, T x_old) override;
public:
    TPriorExponential();
    ~TPriorExponential() override = default;
    void initialize(const std::string & params) override ;
    void typesAreCompatible(bool throwIfNot) override;
    virtual double lambda() const;
    bool checkMinMax(std::string &min, std::string &max, bool & minIsIncluded, bool & maxIsIncluded, bool & hasDefaultMin, bool & hasDefaultMax) override;

    // simulate values
    void simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) override;
};

template <class T>
class TPriorChisq : virtual public TPrior<T>{
protected:
    uint32_t _k;
    double _kDiv2Min1;
    double _getLogPriorDensity_oneVal(T x) override;
    double _getLogPriorRatio_oneVal(T x, T x_old) override;
public:
    TPriorChisq();
    ~TPriorChisq() override = default;
    void initialize(const std::string & params) override ;
    void typesAreCompatible(bool throwIfNot) override;
    uint32_t k() const;
    bool checkMinMax(std::string &min, std::string &max, bool & minIsIncluded, bool & maxIsIncluded, bool & hasDefaultMin, bool & hasDefaultMax) override;

    // simulate values
    void simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) override;
};

template <typename T>
class TPriorMultivariateChisq: public TPrior<T> {
    // chisq prior with multiple degrees of freedom (k) - one k per parameter
protected:
    int _D;

    // parameters
    std::vector<uint64_t> _ks;
    void _fillK_fromDimensions(const size_t & D);

    // log-densities
    double _getLogPriorDensity_oneVal(T x) override;
    double _getLogPriorRatio_oneVal(T x, T x_old) override;
    double _getLogPriorDensity_vec(const std::shared_ptr<TMCMCObservationsBase> & data, const size_t & index) override;
    double _getLogPriorDensityOld_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index) override;
    double _getLogPriorRatio_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index) override;

public:
    TPriorMultivariateChisq();
    void initialize(const std::string & params) override ;
    void typesAreCompatible(bool throwIfNot) override;
    void initializeStorageOfPriorParameters() override;

    // return sum of log-densities of entire vector
    double getSumLogPriorDensity(const std::shared_ptr<TMCMCObservationsBase> & data) override;

    bool checkMinMax(std::string &min, std::string &max, bool & minIsIncluded, bool & maxIsIncluded, bool & hasDefaultMin, bool & hasDefaultMax) override;

    // simulate values
    void simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) override;
};

template <typename T>
class TPriorMultivariateChi: public TPriorMultivariateChisq<T> {
    // chi prior with multiple degrees of freedom (k) - one k per parameter
    // used for prior on mrr of multivariate normal distribution
    // m_rr^2 ~ Chisq(K)
    // and hence m_rr ~ Chi(K)

protected:
    // log-densities
    double _getLogPriorDensity_vec(const std::shared_ptr<TMCMCObservationsBase> & data, const size_t & index) override;
    double _getLogPriorDensityOld_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index) override;
    double _getLogPriorRatio_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index) override;

public:
    TPriorMultivariateChi();

    // simulate values
    void simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) override;
};

template <class T>
class TPriorBeta : virtual public TPrior<T>{
private:
    double _alpha;
    double _beta;
protected:
    double _alphaMin1;
    double _betaMin1;
    double _getLogPriorDensity_oneVal(T x) override;
    double _getLogPriorRatio_oneVal(T x, T x_old) override;
public:
    TPriorBeta();
    ~TPriorBeta() override = default;
    void initialize(const std::string & params) override ;
    void typesAreCompatible(bool throwIfNot) override;
    virtual double alpha() const;
    virtual double beta() const;
    bool checkMinMax(std::string &min, std::string &max, bool & minIsIncluded, bool & maxIsIncluded, bool & hasDefaultMin, bool & hasDefaultMax) override;

    // simulate values
    void simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) override;
};

template <class T>
class TPriorBinomial : virtual public TPrior<T>{
protected:
    double _p;
    double _log_p;
    double _log_q;
    // log-densities
    double _getLogPriorDensity_oneVal(T x) override;
    double _getLogPriorRatio_oneVal(T x, T x_old) override;
    double _getLogPriorDensity_vec(const std::shared_ptr<TMCMCObservationsBase> & data, const size_t & index) override;
    double _getLogPriorDensityOld_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index) override;
    double _getLogPriorRatio_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index) override;
public:
    TPriorBinomial();
    ~TPriorBinomial() override = default;
    void initialize(const std::string & params) override;
    void initializeStorageOfPriorParameters() override;

    void typesAreCompatible(bool throwIfNot) override;
    virtual double p() const;
    bool checkMinMax(std::string &min, std::string &max, bool & minIsIncluded, bool & maxIsIncluded, bool & hasDefaultMin, bool & hasDefaultMax) override;

    // return sum of log-densities of entire vector
    double getSumLogPriorDensity(const std::shared_ptr<TMCMCObservationsBase> & data) override;

    // simulate values
    void simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) override;
};

template <class T>
class TPriorBernouilli: virtual public TPrior<T>{
private:
    double _pi;
protected:
    double _logPi;
    double _log1MinPi;
    double _getLogPriorDensity_oneVal(T x) override;
    double _getLogPriorRatio_oneVal(T x, T x_old) override;
public:
    TPriorBernouilli();
    ~TPriorBernouilli() override = default;
    void initialize(const std::string & params) override ;
    void typesAreCompatible(bool throwIfNot) override;
    virtual double pi() const;
    bool checkMinMax(std::string &min, std::string &max, bool & minIsIncluded, bool & maxIsIncluded, bool & hasDefaultMin, bool & hasDefaultMax) override;

    // simulate values
    void simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) override;
};

template <class T>
class TPriorDirichlet : virtual public TPrior<T>{
protected:
    TDirichletDistrReUsable dirichletDistribution;

    // log-densities
    double _getLogPriorDensity_oneVal(T x) override;
    double _getLogPriorRatio_oneVal(T x, T x_old) override;
    double _getLogPriorDensity_vec(const std::shared_ptr<TMCMCObservationsBase> & data, const size_t & index) override;
    double _getLogPriorDensityOld_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index) override;
    double _getLogPriorRatio_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index) override;

public:
    TPriorDirichlet();
    ~TPriorDirichlet() override = default;
    void initialize(const std::string & params) override ;
    void initializeStorageOfPriorParameters() override;

    void typesAreCompatible(bool throwIfNot) override;
    bool checkMinMax(std::string &min, std::string &max, bool & minIsIncluded, bool & maxIsIncluded, bool & hasDefaultMin, bool & hasDefaultMax) override;
    std::vector<double> alpha() const;

    // return sum of log-densities of entire vector
    double getSumLogPriorDensity(const std::shared_ptr<TMCMCObservationsBase> & data) override;

    // simulate values
    void simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) override;
};

#include "TPrior.tpp"

#endif //MCMCFRAMEWORK_TPRIOR_H
