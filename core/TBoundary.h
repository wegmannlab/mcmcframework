//
// Created by madleina on 08.05.20.
//

#ifndef MCMCFRAMEWORK_TBOUNDARY_H
#define MCMCFRAMEWORK_TBOUNDARY_H

#include <memory>
#include <utility>
#include <cmath>
#include <cassert>
#include "stringFunctions.h"

#define cutoffFloat 0.00001

//-------------------------------------------
// TBoundary
//-------------------------------------------
template <class T>
class TBoundary {
protected:
    T _min;
    T _max;
    T _range;
    bool _min_included;
    bool _max_included;

    void _setRange(){
        // set range variable
        _range = _max - _min;

        // check for numeric overflow
        // if e.g. _max = std::numeric_limits<T>::max() and _min = std::numeric_limits<T>::lowest() (default)
        // -> range would be _max - _min, but this gives a number that is potentially bigger than the numerical limits of that type!
        // -> set range to numerical maximum
        if (_min < 0 && _max > 0){
            if (_max >= _min - std::numeric_limits<T>::lowest()) { // _max - _min would cause numeric overflow
                _range = std::numeric_limits<T>::max();
            }
        }
    }

    // if we jump exactly on minimum/maximum, but it is not included -> we jump back a very small value
    // for double and floats, this can be done with std::nextafter,
    // however, nextafter will return a super small value for values close to 0 -> this is a problem e.g. for beta prior:
    // value is so small that we never propose something smaller -> stuck forever
    // if this is the case: just add/subtract small value
    template <typename U = T,
            std::enable_if_t<std::is_same_v<U, T>, int> = 0,
            std::enable_if_t<std::is_floating_point<U>::value, int> = 0>
    T _nextRepresentableValue(T from, T to) const{
        T next = std::nextafter(from, to);
        if (std::abs(from - next) < cutoffFloat) { // cut at 0.00001
            if (from < to) { // from = min
                return from + cutoffFloat; // min+0.00001
            } else { // from = max
                return from - cutoffFloat; // max-0.00001
            }
        } else {
            return next;
        }
    }

    // if we jump exactly on minimum/maximum, but it is not included -> we jump back the smallest possible value
    // for integers, this can not be done with std::nextafter. Instead, we just jump back 1 towards the middle.
    template <typename U = T,
            std::enable_if_t<std::is_same_v<U, T>, int> = 0,
            std::enable_if_t<std::is_integral<U>::value, int> = 0>
    T _nextRepresentableValue(T from, T to) const {
        if (from < to){ // from = min
            return from + 1; // min+1
        } else { // from = max
            return from - 1; // max-1
        }
    }

    template <typename U = T,
            std::enable_if_t<std::is_same_v<U, T>, int> = 0,
            std::enable_if_t<std::is_floating_point<U>::value, int> = 0>
    bool _canComputeValueMinusMin(T value) const{
        return value <= _min - std::numeric_limits<T>::lowest();
    }

    template <typename U = T,
            std::enable_if_t<std::is_same_v<U, T>, int> = 0,
            std::enable_if_t<std::is_integral<U>::value, int> = 0>
    T _canComputeValueMinusMin(T value) const {
        // other from floating point, because lowest() of integers is one bigger than max() -> if value = 0 and min = lowest(),
        // 0 - lowest() is one too big in order to be stored as a positive integer
        return value < _min - std::numeric_limits<T>::lowest();
    }

public:
    TBoundary() {
        // set lower and upper boundary to numerical limits of that type
        _min = std::numeric_limits<T>::lowest();
        _max = std::numeric_limits<T>::max();
        _min_included = true;
        _max_included = true;
        _setRange();
    }
    virtual ~TBoundary() = default;

    void init(bool hasDefaultMin, bool hasDefaultMax, const std::string & Min, const std::string & Max, bool min_included, bool max_included){
        if (!hasDefaultMin) {
            T minT = convertStringCheck<T>(Min); // first cast
            setMin(minT, min_included);
        }
        if (!hasDefaultMax) {
            T maxT = convertStringCheck<T>(Max); // first cast
            setMax(maxT, max_included);
        }
        checkMinMax();
        _setRange();
    }

    void setMin(T Min, bool min_included){
        _min = Min;
        _min_included = min_included;
        _setRange();
    }

    void setMax(T Max, bool max_included){
        _max = Max;
        _max_included = max_included;
        _setRange();
    }

    void checkMinMax() const{
        if (_min >= _max)
            throw std::runtime_error("Minimum >= maximum!");
    }

    void checkRangeAndThrow(T val, const std::string & name) const{
        // check if a given value 'val' is within interval; throw if it doesn't
        if (_min_included && val < _min) {
            throw std::runtime_error("Cannot initialize parameter '" + name + "': The value = " + toString(val) + " is smaller than the minimum " + toString(_min) + "!");
        } else if (!_min_included && val <= _min) {
            throw std::runtime_error("Cannot initialize parameter '" + name + "': The value = " + toString(val) + " is smaller or equal than the minimum " + toString(_min) + "!");
        } else if (_max_included && val > _max) {
            throw std::runtime_error("Cannot initialize parameter '" + name + "': The value = " + toString(val) + " is larger than the maximum " + toString(_max) + "!");
        } else if (!_max_included && val >= _max) {
            throw std::runtime_error("Cannot initialize parameter '" + name + "': The value = " + toString(val) + " is larger or equal than the maximum " + toString(_max) + "!");
        } else {
            // all fine -> no need to do anything
        }
    }

    bool checkRange(const T & val) const{
        // check if a given value 'val' is within interval; return false if it doesn't
        if ((_min_included && val < _min) || (!_min_included && val <= _min) || (_max_included && val > _max) || (!_max_included && val >= _max)) {
            return false;
        } else {
            return true;
        }
    }

    // template function updateAndMirror for unsigned integers (uint8_t/uint16_t/...) -> specialized because jump can be negative, so we can't pass
    // it as type T to this function (uint can not be negative). This function gets jump as int (specifically as int64_t).
    template <typename U = T,
            std::enable_if_t<std::is_same_v<U, T>, int> = 0,
            std::enable_if_t<std::is_integral<U>::value, int> = 0,
            std::enable_if_t<std::is_unsigned<U>::value, int> = 0>
    T updateAndMirror(T value, int64_t jump) const {
        // Assert: jump should always be < range/2, otherwise we have a bunch of issues (e.g. overshoot after mirroring)
        assert(std::abs(jump) <= _range/2.);

        if (jump < 0 && std::abs(jump) > value - _min){ // cross minimum
            // std::abs(jump) always results in positive values (usually we do -jump, but we can't do this here, because
            // we would then compare signed to unsigned -> signed jump would be casted to unsigned -> undefined behaviour).
            // So we need to take std::abs(jump), but then we also need to check if jump<0, because this is the only case
            // where mirroring is relevant - if we didn't check, we would also mirror for jump>0, which would be wrong.
            return _min - jump - (value - _min); // mirror: equivalent to 2*_min - (value + jump), but overflow-safe (2*min results in overflow if min is large)
        } else if (jump > 0 && jump > _max - value){ // cross maximum
            // check if jump > 0 because if jump was negative, it would be casted to unsigned
            // (as soon as one side of comparison is unsigned, the other is casted to unsigned too)
            // -> negative unsigned results in something undefined
            return _max - (jump - (_max - value)); // mirror: equivalent to 2*_max - (value + jump), but overflow-safe (2*max results in overflow if max = numerical_limit)
        } else if (!_min_included && jump < 0 && std::abs(jump) == value - _min){ // exactly jump on the minimum
            return _nextRepresentableValue(_min, _max); // return next representable value
        } else if (!_max_included && jump > 0 && jump == _max - value){ // exactly jump on the maximum
            return _nextRepresentableValue(_max, _min); // return next representable value
        } else {
            return value + jump; // all ok, no need to mirror
        }
    }

    // template function updateAndMirror for signed types (double/float/int etc.) -> different from template for unsigned types
    // we need to check for range overflow
    // e.g. value = -10 and max = std::numeric_limits -> calculate max - value already results in overflow and undefined
    // behaviour! -> Always check if for value.
    template <typename U = T,
            std::enable_if_t<std::is_same_v<U, T>, int> = 0,
            std::enable_if_t<std::is_signed<U>::value, int> = 0>
    T updateAndMirror(T value, T jump) const {
        // Assert: jump should always be < range/2, otherwise we have a bunch of issues (e.g. overshoot after mirroring
        // and overflow issues)
        assert(std::abs(jump) <= _range/2.);

        if (value >= 0 && _min < 0){
            // we might have a numerical problem calculating value-min, if min is very small (e.g. numeric lower bound)
            if (_canComputeValueMinusMin(value)){ // ok, it is safe to calculate value-min
                if (-jump > value - _min){ // crossed minimum -> mirror
                    return _min - jump - (value - _min); // mirror: equivalent to 2*_min - (value + jump), but overflow-safe (2*min results in overflow if min=numerical_limit)
                } else if (!_min_included && -jump == value - _min){ // exactly jump on minimum, but minimum is not included
                    return _nextRepresentableValue(_min, _max); // return next representable value
                } // else don't mirror
            } // else value-min > numeric limits of that type -> no need to mirror, as jump can never be this big (restricted to range/2)
        } else if (-jump > value - _min){ // this is now safe to calculate, as value-min will always be within numeric limits
            return _min - jump - (value - _min); // mirror: equivalent to 2*_min - (value + jump), but overflow-safe (2*min results in overflow if min=numerical_limit)
        } else if (!_min_included && -jump == value - _min){ // exactly jump on minimum, but minimum is not included
            return _nextRepresentableValue(_min, _max); // return next representable value
        }
        // now do the same for the maximum
        if (value < 0 && _max > 0){
            // we might have a numerical problem calculating max-value, if max is very large (e.g. numeric upper bound)
            if (-value <= std::numeric_limits<T>::max() - _max){ // ok, it is safe to calculate max-value
                if (jump > _max - value){ // crossed maximum -> mirror
                    return _max - (jump - (_max - value)); // mirror: equivalent to 2*_max - (value + jump), but overflow-safe (2*max results in overflow if max = numerical_limit)
                } else if (!_max_included && jump == _max - value){ // exactly jump on maximum, but maximum is not included
                    return _nextRepresentableValue(_max, _min); // return next representable value
                } // else don't mirror
            } // else _max - value > numeric limits of that type -> no need to mirror, as jump can never be this big (restricted to range/2)
        } else if (jump > _max - value){ // this is now safe to calculate, as max-value will always be within numeric limits
            return _max - (jump - (_max - value)); // mirror: equivalent to 2*_max - (value + jump), but overflow-safe (2*max results in overflow if max = numerical_limit)
        } else if (!_max_included && jump == _max - value){ // exactly jump on maximum, but maximum is not included
            return _nextRepresentableValue(_max, _min); // return next representable value
        }

        return value + jump; // all ok, no need to mirror
    }

    void resetBoundary(bool hasDefaultMin, bool hasDefaultMax, const std::string & min, const std::string & max, bool minIncluded, bool maxIncluded){
        // can only make the (max-min) smaller -> will never cross the hard limits given by the numerical limits
        if (!hasDefaultMin){
            T minT = convertStringCheck<T>(min); // first cast
            if (_min < minT) {
                setMin(minT, minIncluded);
            } else if (_min == minT && !minIncluded){
                setMin(minT, minIncluded);
            }
        } if (!hasDefaultMax){
            T maxT = convertStringCheck<T>(max); // first cast
            if (_max > maxT) {
                setMax(maxT, maxIncluded);
            } else if (_max == maxT && !maxIncluded){
                setMax(maxT, maxIncluded);
            }
        }
        // check if min < max
        checkMinMax();
    }

    // getters
    T min() const{
        return _min;
    }

    T max() const{
        return _max;
    }

    bool minIncluded() const{
        return _min_included;
    }

    bool maxIncluded() const{
        return _max_included;
    }

    T range() const{
        return _range;
    }
};

#endif //MCMCFRAMEWORK_TBOUNDARY_H