//
// Created by madleina on 22.01.21.
//

#ifndef EMPTY_TDATATRANSLATOR_H
#define EMPTY_TDATATRANSLATOR_H

#include "MCMCFrameworkVariables.h"
#include <cmath>
#include "stringFunctions.h"

//-------------------------
// TDataTranslator
//-------------------------

template <typename TranslatedType, typename StoredType> class TDataTranslator {
    // base class
protected:
    Translator::Translator _name;

public:
    TDataTranslator() = default;
    virtual ~TDataTranslator() = default;
    // StoredType -> TranslatedType (used for getting values)
    virtual TranslatedType translate(const StoredType & value) = 0;
    // TranslatedType -> StoredType (used for setting values)
    virtual StoredType translateToStoredType(const TranslatedType & value) = 0;
};

//-------------------------
// TDataTranslatorPhredScores
//-------------------------
template<class TranslatedType, class StoredType, class Enable = void>
class TDataTranslatorPhredScores : public TDataTranslator<TranslatedType, StoredType> {
    // generic class template for all types that are not allowed (i.e. floating point numbers, signed integers and booleans)
    // throws upon initialization
    // needed because otherwise doesn't compile: e.g. for [i], i can not be floating point
public:
    TDataTranslatorPhredScores(const StoredType & Max){
        throw std::runtime_error("Can not create TDataTranslatorPhredScores for StoredType that is not an unsigned integer!");
    }
    ~TDataTranslatorPhredScores() override = default;
    TranslatedType translate(const StoredType & value) override{return 0;}
    StoredType translateToStoredType(const TranslatedType & value) override{return 0;}
};

template <class TranslatedType, class StoredType>
class TDataTranslatorPhredScores<TranslatedType, StoredType,
        typename std::enable_if<std::is_integral<StoredType>::value
                                && std::is_unsigned<StoredType>::value
                                && !std::is_same<StoredType, bool>::value
                                >::type>
        : public TDataTranslator<TranslatedType, StoredType> {
    // class template for all types that are allowed (i.e. unsigned integers)
protected:
    TranslatedType * _phredToGTLMap;
    StoredType _max;

    TranslatedType _phredToGTL(const StoredType & phred) {
        return pow(10., -static_cast<double>(phred)/10.);
    };

public:
    // enable constructor only for StorageType = uint
    TDataTranslatorPhredScores(const StoredType & Max){
        this->_name = Translator::phred;

        // fill map
        _max = Max;
        if (_max == std::numeric_limits<StoredType>::max()){
            // if max is already at numeric limit -> we shouldn't add +1, because this results in numeric overflow
            _max = _max - 1;
        }
        _phredToGTLMap = new TranslatedType[_max+1];
        for (StoredType phred = 0; phred <= _max; phred++) {
            _phredToGTLMap[phred] = _phredToGTL(phred);
        }
    };
    ~TDataTranslatorPhredScores() override{
        delete [] _phredToGTLMap;
    }

    TranslatedType translate(const StoredType & value) override{
        // converts phred score to GTL
        if (value >= _max){
            return _phredToGTLMap[_max];
        }
        return _phredToGTLMap[value];
    };

    StoredType translateToStoredType(const TranslatedType & value) override{
        if (value < 0 || value > 1){
            throw std::runtime_error("In function 'StoredType TDataTranslatorPhredScores::translateToStoredType(const TranslatedType & value) override': Value " + toString(value) + " is outside range [0,1]! Can not convert to phred score.");
        }
        // converts GTL to phred score
        double tmp = std::round(-10. * log10(value));
        if (tmp > _max){
            return _max;
        } else {
            return static_cast<StoredType>(tmp);
        }
    }
};


#endif //EMPTY_TDATATRANSLATOR_H
