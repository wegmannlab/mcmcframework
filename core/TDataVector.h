/*
 * THMMVectors.h
 *
 *  Created on: Dec 15, 2020
 *      Author: phaentu
 */

#ifndef CORE_TDATAVECTOR_H_
#define CORE_TDATAVECTOR_H_

#include <stdexcept>
#include <inttypes.h>
#include <iostream>
#include <cstdint>

//-------------------------------------
// TDataVector_base
// a pure abstract class
//-------------------------------------
template <typename PrecisionType, typename SizeType> class TDataVector_base{
protected:
	PrecisionType* _current;
	SizeType _size;
	bool _hasStorage;

	virtual void _init(){
		_current = nullptr;
		_size = 0;
		_hasStorage = false;
	};

public:
	TDataVector_base(){
		_init();
	};

	virtual ~TDataVector_base(){
		clear();
	};

	virtual void clear(){
		if(_hasStorage){
			delete[] _current;
			_current = nullptr;
			_hasStorage = false;
		}
		_size = 0;
	};

	virtual void resize(const SizeType & NumStates) = 0;

	virtual const SizeType& size() const { return _size; };

	virtual void set(const PrecisionType & Value){
		for(SizeType i = 0; i< _size; ++i){
			_current[i] = Value;
		}
	};

    virtual PrecisionType sum() const{
        PrecisionType s = 0;
        for(SizeType i = 0; i< _size; ++i){
            s+= _current[i];
        }
        return s;
    };

    virtual PrecisionType max() const{
        PrecisionType s = _current[0];
        for(SizeType i = 1; i< _size; ++i){
            if (_current[i] > s){
                s = _current[i];
            }
        }
        return s;
    };

    virtual SizeType maxIndex() const{
        SizeType index = 0;
        PrecisionType s = _current[index];
        for(SizeType i = 1; i< _size; ++i){
            if (_current[i] > s){
                index = i;
                s = _current[i];
            }
        }
        return index;
    };

    virtual void normalize(const PrecisionType & SumToNormalize){
		for(SizeType i = 0; i< _size; ++i){
			_current[i] /= SumToNormalize;
		}
	};

    virtual void print() const{
		for(SizeType i = 0; i< _size; ++i){
			if(i > 0){
				std::cout << ", ";
			}
			std::cout << _current[i];
		}
	};

    //iterator
    struct Iterator {
    public:
    	// iterator traits
		using difference_type = PrecisionType;
		using value_type = PrecisionType;
		using pointer = const PrecisionType*;
		using reference = const PrecisionType&;
		using iterator_category = std::forward_iterator_tag;

		// constructors
		Iterator(pointer ptr) : _ptr(ptr) {}

		// operators
	    reference operator*() const { return *_ptr; }
	    pointer operator->() { return _ptr; }

	    Iterator& operator++() { _ptr++; return *this; }
	    Iterator operator++(int) { Iterator tmp = *this; ++(*this); return tmp; }

	    friend bool operator== (const Iterator& a, const Iterator& b) { return a._ptr == b._ptr; };
	    friend bool operator!= (const Iterator& a, const Iterator& b) { return a._ptr != b._ptr; };
    private:
        	pointer _ptr;
    };

    Iterator begin() { return Iterator(&_current[0]); };
    Iterator end()   { return Iterator(&_current[_size]); };

    struct Const_Iterator {
	public:
		// iterator traits
		using difference_type = PrecisionType;
		using value_type = PrecisionType;
		using pointer = const PrecisionType*;
		using reference = const PrecisionType&;
		using iterator_category = std::forward_iterator_tag;

		// constructors
		Const_Iterator(pointer ptr) : _ptr(ptr) {}

		// operators
		const reference operator*() const { return *_ptr; }
		const pointer operator->() { return _ptr; }

		Const_Iterator& operator++() { _ptr++; return *this; }
		Const_Iterator operator++(int) { Iterator tmp = *this; ++(*this); return tmp; }

		friend bool operator== (const Const_Iterator& a, const Const_Iterator& b) { return a._ptr == b._ptr; };
		friend bool operator!= (const Const_Iterator& a, const Const_Iterator& b) { return a._ptr != b._ptr; };
	private:
			pointer _ptr;
	};

    Const_Iterator cbegin() const { return Const_Iterator(&_current[0]); };
    Const_Iterator cend()   const { return Const_Iterator(&_current[_size]); };
};

//-------------------------------------
// TDataVector
//-------------------------------------
template <typename PrecisionType, typename SizeType> class TDataVector : TDataVector_base<PrecisionType, SizeType>{
protected:
	using TDataVector_base<PrecisionType, SizeType>::_current;
	using TDataVector_base<PrecisionType, SizeType>::_size;
	using TDataVector_base<PrecisionType, SizeType>::_hasStorage;

	using TDataVector_base<PrecisionType, SizeType>::_init;

public:
	TDataVector(){};

	TDataVector(const SizeType & NumStates){
		resize(NumStates);
	};

	TDataVector(PrecisionType* Pointer, const SizeType & NumStates){
	    // creates another pointer, but does not copy memory -> hasStorage = false, memory is deleted elsewhere
		_current = Pointer;
		_size = NumStates;
		_hasStorage = false;
	};

	TDataVector(const TDataVector & other){
	    // copy constructor: creates another pointer, but does not copy memory -> hasStorage = false, memory is deleted elsewhere
		_current = other._current;
		_size = other._size;
		_hasStorage = false;
	};

	TDataVector(TDataVector && other){
	    // move constructor: takes members of other instance, but clears memory if there is any
		_current = other._current;
		_size = other._size;
		_hasStorage = other._hasStorage;

		clear();
	};

	virtual ~TDataVector(){
		clear();
	};

	virtual void clear(){
		if(_hasStorage){
			delete[] _current;
			_current = nullptr;
			_hasStorage = false;
		}
		_size = 0;
	};

	virtual void resize(const SizeType & NumStates){
		if(NumStates != _size){
			clear();
			if(NumStates > 0){
				_size = NumStates;
				_current = new PrecisionType[_size];
				_hasStorage = true;
			}
		}
	};

	virtual const SizeType& size() const { return _size; };
	const PrecisionType& operator[](const SizeType & State) const { return _current[State]; };
	PrecisionType& operator[](const SizeType & State){ return _current[State]; };

	virtual TDataVector<PrecisionType, SizeType> current() const {
		return TDataVector<PrecisionType, SizeType>(_current, _size);
	};

	using TDataVector_base<PrecisionType, SizeType>::set;
    using TDataVector_base<PrecisionType, SizeType>::sum;
    using TDataVector_base<PrecisionType, SizeType>::max;
    using TDataVector_base<PrecisionType, SizeType>::maxIndex;
    using TDataVector_base<PrecisionType, SizeType>::normalize;
	using TDataVector_base<PrecisionType, SizeType>::print;

	using TDataVector_base<PrecisionType, SizeType>::begin;
	using TDataVector_base<PrecisionType, SizeType>::end;
	using TDataVector_base<PrecisionType, SizeType>::cbegin;
	using TDataVector_base<PrecisionType, SizeType>::cend;
};

//-------------------------------------
// TDataVectorPair
//-------------------------------------
template <typename PrecisionType, typename SizeType> class TDataVectorPair : public TDataVector<PrecisionType, SizeType>{
    // manages two pointers: _current and _vec.
    // -> _vec: points to an array of length 2*_size.
    // -> _current: points to an element of _vec.
    //              This is either the first element of _vec (= _vec), or the "middle" element of _vec (= _vec + _size).
    // used for alpha and beta of HMM, where we always need to remember the current and previous/next element, respectively
private:
	using TDataVector<PrecisionType, SizeType>::_current;
	using TDataVector<PrecisionType, SizeType>::_size;
	using TDataVector<PrecisionType, SizeType>::_hasStorage;
	PrecisionType* _vec;

	void _init(){
		TDataVector<PrecisionType, SizeType>::_init();
		_vec = nullptr;
		_hasStorage = false;
	};

public:
	TDataVectorPair(){
		_init();
	};
	TDataVectorPair(const SizeType & NumStates){
		_init();
		resize(NumStates);
	};
	~TDataVectorPair(){
		clear();
	};

	void clear(){
		if(_hasStorage){
			delete[] _vec;
			_hasStorage = false;
			_vec = nullptr;
			_current = nullptr;
		}
		_size = 0;
	};

	void resize(const SizeType & NumStates){
		if(NumStates != _size){
			this->clear();
			if(NumStates > 0){
				_size = NumStates;
				_vec = new PrecisionType[2 * _size];
				_hasStorage = true;
			}
		}
		_current = _vec;
	};

	void swap(){
		if(_current == _vec){
			_current = _vec + _size;
			// _current now points to "middle" element of _vec
		} else {
			_current = _vec;
            // _current now points to first element in _vec
		}
	};

	TDataVector<PrecisionType, SizeType> other() const {
		if(_current == _vec){
		    // return data vector containing the second part of _vec
			return TDataVector<PrecisionType, SizeType>(_current + _size, _size);
		} else {
            // return data vector containing the first part of _vec
            return TDataVector<PrecisionType, SizeType>(_current - _size, _size);
		}
	};

	using TDataVector_base<PrecisionType, SizeType>::set;
	using TDataVector_base<PrecisionType, SizeType>::sum;
	using TDataVector_base<PrecisionType, SizeType>::max;
	using TDataVector_base<PrecisionType, SizeType>::maxIndex;
	using TDataVector_base<PrecisionType, SizeType>::normalize;
	using TDataVector_base<PrecisionType, SizeType>::print;

	using TDataVector_base<PrecisionType, SizeType>::begin;
	using TDataVector_base<PrecisionType, SizeType>::end;
	using TDataVector_base<PrecisionType, SizeType>::cbegin;
	using TDataVector_base<PrecisionType, SizeType>::cend;
};

//-------------------------------------
// TDataVectorMulti
//-------------------------------------
template <typename PrecisionType, typename SizeType, typename LengthType> class TDataVectorMulti : public TDataVector<PrecisionType, SizeType>{
    // manages three pointers: _vec, _end and _current.
    // -> _vec: points to an array of length _capacity (= _size * _length).
    // -> _end: points to the end of _vec (i.e. to _vec+_size*_length).
    // -> _current: points to an element of _vec.
    //              This is any element whose index is a multiple of _size (e.g. _vec, _vec+_size, _vec+2*_size, ..., _end-_size).
    // used for alpha and beta of HMM if the entire chain must be stored -> _size would be numStates and _length would be length of chain
protected:
	using TDataVector<PrecisionType, SizeType>::_current;
	using TDataVector<PrecisionType, SizeType>::_size;
	using TDataVector<PrecisionType, SizeType>::_hasStorage;
	PrecisionType* _vec;
	LengthType _length;
	LengthType _capacity;

	//iterator
	PrecisionType* _end;

	void _init(){
		_vec = nullptr;
		_size = 0;
		_length = 0;
		_capacity = 0;
		_hasStorage = false;

		_current = nullptr;
		_end = nullptr;
	};

	void _ensureCapacity(){
		if(_size * _length > _capacity){
			_capacity = _size * _length;
			if(_hasStorage){
				delete[] _vec;
			}
			_vec = new PrecisionType[_capacity];
			_hasStorage = true;
		}
	};

public:
	TDataVectorMulti(){
		_init();
	};

	TDataVectorMulti(const SizeType & NumStates, LengthType & Length){
		_init();
		resize(NumStates, Length);
	};

	~TDataVectorMulti(){
		clear();
	};

	void clear(){
		if(_hasStorage){
			delete[] _vec;
		}
		_length = 0;
		_capacity = 0;
		_hasStorage = false;
	};

	void resize(const SizeType & NumStates){
		resize(NumStates, _length);
	};

	void resize(const SizeType & NumStates, LengthType & Length){
		_size = NumStates;
		_length = Length;
		_ensureCapacity();
		_end = &_vec[_length * _size];
		_current = _vec;
	};

	const LengthType& length() const { return _length; };
	const LengthType& capacity() const { return _capacity; };

	//loop
	void startForwards(){
		_current = _vec;
	};

	bool moveForward(){
		if(_current == _end || _current + _size == _end){
			return false;
		}
		_current += _size;
		return true;
	};

	bool startBackwards(){
		if(_vec == _end){ // start = end -> probably _length not initialized
			return false;
		}
		_current = _end - _size;
		return true;
	};

	bool moveBackward(){
		if(_current == _vec){
			return false;
		}
		_current -= _size;
		return true;
	};

	//access as THMMVector
	TDataVector<PrecisionType, SizeType> previous() const {
		if(_current == _vec){
			throw std::runtime_error("THMMMultiVector::previous(): already at beginning!");
		}
		return TDataVector<PrecisionType, SizeType>(_current - _size, _size);
	};

	TDataVector<PrecisionType, SizeType> current() const {
		if(_current == _end){
			throw std::runtime_error("THMMMultiVector::current(): at end!");
		}
		return TDataVector<PrecisionType, SizeType>(_current, _size);
	};

	TDataVector<PrecisionType, SizeType> next() const {
		if(_current == _end){
			throw std::runtime_error("THMMMultiVector::next(): at end!");
		}
		if(_current + _size == _end){
			throw std::runtime_error("THMMMultiVector::next(): at end - _size!");
		}
		return TDataVector<PrecisionType, SizeType>(_current + _size, _size);
	};

	using TDataVector_base<PrecisionType, SizeType>::set;
	using TDataVector_base<PrecisionType, SizeType>::sum;
	using TDataVector_base<PrecisionType, SizeType>::max;
	using TDataVector_base<PrecisionType, SizeType>::maxIndex;
	using TDataVector_base<PrecisionType, SizeType>::normalize;
	using TDataVector_base<PrecisionType, SizeType>::print;

	using TDataVector_base<PrecisionType, SizeType>::begin;
	using TDataVector_base<PrecisionType, SizeType>::end;
	using TDataVector_base<PrecisionType, SizeType>::cbegin;
	using TDataVector_base<PrecisionType, SizeType>::cend;
};

//-------------------------------------
// TDataSquareMatrix
//-------------------------------------
template <typename PrecisionType, typename SizeType> class TDataSquareMatrix : TDataVector_base<PrecisionType, SizeType>{
    // A linearized square matrix
    // Suitable for data storage but not suitable for matrix operations
    // dimension of matrix: _dim times _dim
    // manages one pointer _current of length _size (where _size = _dim*_dim)
    // used e.g. for storing xi of HMM
protected:
	using TDataVector_base<PrecisionType, SizeType>::_current;
	using TDataVector_base<PrecisionType, SizeType>::_size;
	using TDataVector_base<PrecisionType, SizeType>::_hasStorage;

	SizeType _dim; // _size = _dim*_dim

public:
	TDataSquareMatrix() : TDataVector_base<PrecisionType, SizeType>(){};
	TDataSquareMatrix(const SizeType & NumStates) : TDataVector_base<PrecisionType, SizeType>(){
		resize(NumStates);
	};

	~TDataSquareMatrix() = default;

	void resize(const SizeType & NumStates) override{
		if(NumStates*NumStates != _size){
			this->clear();
			if(NumStates > 0){
			    _dim = NumStates;
				_size = _dim*_dim;
				_current = new PrecisionType[_size];
				_hasStorage = true;
			}
		}
	};

	// accessing elements with ()
	PrecisionType& operator()(const SizeType & From, const SizeType & To){ return _current[From + _dim * To]; };
	const PrecisionType& operator()(const SizeType & From, const SizeType & To) const { return _current[From + _dim * To]; };

	// size
    const SizeType& size() const override { return _dim; };
    const SizeType& linearSize() const { return _size; };

    // functions from base class
    using TDataVector_base<PrecisionType, SizeType>::set;
    using TDataVector_base<PrecisionType, SizeType>::sum;
    using TDataVector_base<PrecisionType, SizeType>::max;
    using TDataVector_base<PrecisionType, SizeType>::maxIndex;
    using TDataVector_base<PrecisionType, SizeType>::normalize;
    using TDataVector_base<PrecisionType, SizeType>::print;

	using TDataVector_base<PrecisionType, SizeType>::begin;
	using TDataVector_base<PrecisionType, SizeType>::end;
	using TDataVector_base<PrecisionType, SizeType>::cbegin;
	using TDataVector_base<PrecisionType, SizeType>::cend;

};



#endif /* CORE_TDATAVECTOR_H_ */
