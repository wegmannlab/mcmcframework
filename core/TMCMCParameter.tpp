
//--------------------------------------------
// TMCMCParameters
//--------------------------------------------
template <typename T> TMCMCParameter<T>::TMCMCParameter()
        : TMCMCParameterBase(){
    _totalUpdates = 0;
    _sharedJumpSize = false;
    _randomGenerator = nullptr;
    _isUpdated = true;
}

template <typename T> TMCMCParameter<T>::TMCMCParameter(std::shared_ptr<TPrior<T>> Prior, const std::shared_ptr<TParameterDefinition> & Def, TRandomGenerator* RandomGenerator)
        : TMCMCParameterBase(){
    _totalUpdates = 0;
    _sharedJumpSize = false;
    _isUpdated = false;

    _randomGenerator = RandomGenerator;

    _prior = std::move(Prior);
    _def = Def;

    _initialize();
}

template <class T> TMCMCParameter<T>::~TMCMCParameter() = default;

template <class T> void TMCMCParameter<T>::_initialize(){
    // name and isUpdates
    _name = _def->name();
    _isUpdated = _def->isUpdated();

    // boundary
    _initBoundary(_def->hasDefaultMin(), _def->hasDefaultMax(), _def->min(), _def->max(), _def->minIncluded(), _def->maxIncluded());
    // propKernels
    if (_isUpdated) {
        _propKernel = _initPropKernel(_def->propKernel());
    }
}

template <class T> void TMCMCParameter<T>::initializeStorage(){
    _prior->initializeStorageOfPriorParameters();
}

template <class T> void TMCMCParameter<T>::initializeStorageBasedOnPrior(const std::vector<size_t> & ExpectedDimensions, const std::vector<std::shared_ptr<TNamesEmpty>> & DimensionNames){
    // check if ExpectedDimensions (expected based on use as prior parameter) and dimensions set by developer match
    assert(ExpectedDimensions.size() == DimensionNames.size());
    if (!_def->hasDefaultDimensions()){
        if (ExpectedDimensions.size() != _def->dimensions().size()){
            throw std::runtime_error("In function 'template <class T> void TMCMCParameter<T>::initializeStorageBasedOnPrior(const std::vector<size_t> & ExpectedDimensions)': size of dimensions of parameter " + _def->name() + " (" + toString(_def->dimensions().size()) + ") differs from what is expected based on prior (" + toString(ExpectedDimensions.size()) + ")!");
        }
        for (size_t d = 0; d < ExpectedDimensions.size(); d++){
            if (ExpectedDimensions[d] != _def->dimensions()[d]){
                throw std::runtime_error("In function 'template <class T> void TMCMCParameter<T>::initializeStorageBasedOnPrior(const std::vector<size_t> & ExpectedDimensions)': dimensions " + toString(d) + " of parameter " + _def->name() + " (" + toString(_def->dimensions()[d]) + ") differs from what is expected based on prior (" + toString(ExpectedDimensions[d]) + ")!");
            }
        }
    }

    // initialize vector of current and old values
    if (ExpectedDimensions.size() == 1 && ExpectedDimensions[0] == 0){ // is empty parameter (can happen if parameter does not exist for certain dimensions, e.g. off-diagonal elements of variance-covariance matrix for 1D)
        _storage = std::make_shared<TMultiDimensionalStorage<T>>(ExpectedDimensions, _def->tracksExponential(), _def->tracksLog(), _def->isUpdated());
    } else if (ExpectedDimensions.at(0) > 1 || ExpectedDimensions.size() > 1) { // is array
        _storage = std::make_shared<TMultiDimensionalStorage<T>>(ExpectedDimensions, _def->tracksExponential(), _def->tracksLog(), _def->isUpdated());
    } else { // is single
        _storage = std::make_shared<TSingleStorage<T>>(_def->tracksExponential(), _def->tracksLog(), _def->isUpdated());
    }

    // set dimension names
    _storage->setDimensionNames(DimensionNames);

    // initialize all the rest (meanVar / initValues / jumpsizes / fixedInitialValue)
    _initMeanVar_InitVals_JumpSizes();
}

template <class T> void TMCMCParameter<T>::initializeStorageSingleElementBasedOnPrior(){
    // function to initialize storage of a parameter of size 1
    // check if this matches what developer set in definition
    if (!_def->hasDefaultDimensions()){
        if (_def->dimensions().size() != 1 || _def->totalSize() != 1){
            throw std::runtime_error("In function 'template <class T> void TMCMCParameter<T>::initializeStorageSingleElementBasedOnPrior()': size of dimensions of parameter " + _def->name() + " (" + toString(_def->dimensions().size()) + ") differs from what is expected based on prior (1)!");
        }
    }

    // initialize vector of current and old values
    _storage = std::make_shared<TSingleStorage<T>>(_def->tracksExponential(), _def->tracksLog(), _def->isUpdated());

    // initialize all the rest (meanVar / initValues / jumpsizes / fixedInitialValue)
    _initMeanVar_InitVals_JumpSizes();
}

template <class T> void TMCMCParameter<T>::_initMeanVar_InitVals_JumpSizes(){
    // store mean and variance
    if (_def->storeMeanVar()) {
        _meanVar.assign(_storage->totalSize(), TMeanVar<double>());
    }
    // read initial values
    _readInitVals(_def->initVal(), _def->hasDefaultInitVal());
    // read initial proposal widths
    if (_isUpdated) {
        _readInitJumpSizesVals(_def->oneJumpSizeForAll(), _def->initJumpSizeProposal());
    }
    // does parameter have fixed initial value? Or is it the default?
    this->_hasFixedInitialValue = !_def->hasDefaultInitVal();
}

template <class T> const std::shared_ptr<TNamesEmpty> & TMCMCParameter<T>::getDimensionName(const size_t &Dim) {
    return _storage->getDimensionName(Dim);
}

template <class T> void TMCMCParameter<T>::handPointerToPrior(const std::shared_ptr<TMCMCObservationsBase>& pointer){
    _prior->initParameter(pointer);
}

template <class T> void TMCMCParameter<T>::constructDAG(TDAG &DAG, TDAG & temporaryDAG) {
    temporaryDAG.add(shared_from_this());
    _prior->constructDAG(DAG, temporaryDAG);
}

template <class T> void TMCMCParameter<T>::runEMEstimation(TLatentVariable<double, size_t, size_t> &latentVariable, TLog * Logfile) {
    _prior->runEMEstimation(latentVariable, Logfile);
}

template <class T> void TMCMCParameter<T>::estimateInitialPriorParameters(TLog * Logfile) {
    _prior->estimateInitialPriorParameters(Logfile);
}

template <class T> void TMCMCParameter<T>::switchPriorClassificationAfterEM(TLog * Logfile) {
    _prior->switchPriorClassificationAfterEM(Logfile);
}

template <class T> void TMCMCParameter<T>::setDistances(const std::shared_ptr<TDistancesBinnedBase> &distances) {
    _prior->initDistances(distances);
}

template <class T> void TMCMCParameter<T>::_initJumpSizeVal_shared(const std::string &initJumpSize){
    // jumpSizeProposalVec will be of size 1
    _sharedJumpSize = true;
    T initJumpSizeProposal;
    try {
        std::string s = initJumpSize;
        eraseAllWhiteSpaces(s);
        initJumpSizeProposal = convertStringCheck<T>(s);
    } catch (...) {
        throw std::runtime_error("Invalid initial jump size (" + initJumpSize + ") for parameter " + _name + "! Should be a number and inside the numerical boundaries of that parameter.");
    }
    _jumpSizeProposalVec.reserve(1); // size 1, as all have the same proposal width
    _setPropKernel(initJumpSizeProposal);
    _acceptedUpdates.push_back(0);
}

template <class T> void TMCMCParameter<T>::_initJumpSizeVal_unique(const std::string &initJumpSize){
    // first fill vec
    std::vector<T> tmp;
    _readVals(initJumpSize, tmp);
    // now fill jumpSizes, and check if they are valid
    _jumpSizeProposalVec.reserve(_storage->totalSize());
    TRange range = _storage->getFull();
    for (size_t i = range.first; i < range.last; i+= range.increment)
        _setPropKernel(tmp[i], i);
    _acceptedUpdates.assign(_storage->totalSize(), 0);
}

template <class T> void TMCMCParameter<T>::_readInitJumpSizesVals(bool oneJumpSizeForAll, const std::string &initJumpSize) {
    // read initial proposal widths
    if (oneJumpSizeForAll) _initJumpSizeVal_shared(initJumpSize);
    else _initJumpSizeVal_unique(initJumpSize);
}

template <class T> void TMCMCParameter<T>::_readInitVals(const std::string& initVal, bool hasDefaultInitVal){
    // first fill vec
    std::vector<T> temp;
    _readVals(initVal, temp);
    // now fill initial values, and check if they are valid
    TRange range = _storage->getFull();
    for (size_t i = range.first; i < range.last; i+= range.increment) {
        temp[i] = _checkDefaultInitVal(temp[i], hasDefaultInitVal);
        _storage->initBoth(i, temp[i]);
        _boundary.checkRangeAndThrow(_storage->value(i), _name);
    }
}

template <class T> T TMCMCParameter<T>::_checkDefaultInitVal(T initVal, bool hasDefaultInitVal){
    // idea: default init values should respect the boundaries of the prior (e.g. if you have a beta prior, it is annoying
    // if default initVal=0 as this throws an error every time you forget to manually set it).
    // If values are set by user (->not default), it is his own responsibility to give meaningful values,
    // and they will not be changed (but still checked on validity)
    if (hasDefaultInitVal)
        return _boundary.updateAndMirror(initVal, 0.);
    return initVal;
}

template <class T> void TMCMCParameter<T>::_readVals(const std::string& initVal, std::vector<T> & storage){
    // initVal can either be..
    //          1) a vector of size _storage->totalSize().
    //          2) a single value
    //          3) a filename
    if (stringContains(initVal, ",")) // case 1): probably a comma-separated vector
        _readValsFromVec(initVal, storage);
    else {
        if (stringIsProbablyANumber(initVal)) // case 2): probably only one number -> initialize all elements in array to this value
            _readValsOnlyOneNumber(initVal, storage);
        else // case 3): probably a file -> try opening it!
            _readValsFromFile(initVal, storage);
    }
}

template <class T> void TMCMCParameter<T>::_readValsOnlyOneNumber(const std::string &initVal, std::vector<T> &storage) const{
    T val;
    try {
        std::string s = initVal;
        eraseAllWhiteSpaces(s);
        val = convertStringCheck<T>(s);
    } catch (...) {
        throw "Invalid initial value (" + initVal + ") for parameter " + _name + "! Should be a number and inside numeric boundaries of that parameter type.";
    }
    for (size_t i = 0; i < _storage->totalSize(); i++)
        storage.push_back(val);
}

template <class T> void TMCMCParameter<T>::_readValsFromVec(const std::string& initVal, std::vector<T> & storage) const{
    std::vector<std::string> vals;
    try {
        fillContainerFromStringAny(initVal, vals, ",", true, true);
    } catch (...) {
        throw "Invalid initial value (" + initVal + ") for parameter " + _name + "! The vector should only contain numbers that are inside numeric boundaries of that parameter type.";
    }
    if (vals.size() != _storage->totalSize())
        throw std::runtime_error("Size of vector (" + toString(vals.size()) + ") does not match size of array of parameter " + _name + " (" + toString(_storage->totalSize()) + ")!");
    for (size_t i = 0; i < _storage->totalSize(); i++){
        storage.push_back(convertStringCheck<T>(vals[i]));
    }
}

template <class T> void TMCMCParameter<T>::_readValsFromFile_oneCol(std::vector<std::string> & vec, TInputFile & file) const{
    std::vector<std::string> tmp;
    while (file.read(tmp)) { // tmp will be cleared after every line
        vec.push_back(tmp[0]);
    }
    if (file.lineNumber() != _storage->totalSize()) {
        throw "Wrong number of lines in file " + file.name() + " (" + toString(file.lineNumber()) + ")! Expected " + toString(_storage->totalSize()) + " lines.";
    }
}

template <class T> void TMCMCParameter<T>::_readValsFromFile_oneRow(std::vector<std::string> & vec, TInputFile & file) const{
    while (file.read(vec)){} // no need to do anything, will fill storage
    if (file.lineNumber() > 1)
        throw "Too many lines in file " + file.name() + " (" + toString(file.lineNumber()) + ")! Expected one line.";
}

template <class T> bool TMCMCParameter<T>::_readValsFromFile_oneColOrRow(const std::string& filename, std::vector<T> & storage) const{
    // open file: no header
    TInputFile file(filename, fixed);

    std::vector<std::string> vec;
    if (file.numCols() == 1){ // 1 col, N lines
        _readValsFromFile_oneCol(vec, file);
    } else if (file.numCols() == _storage->totalSize()) { // N cols, 1 line
        _readValsFromFile_oneRow(vec, file);
    } else {
        return false;
    }
    // fill storage vector
    for (size_t i = 0; i < vec.size(); i++){
        // convert to number
        T val;
        try {
            val = convertString<T>(vec[i]);
        } catch (...) {
            throw "Invalid initial value '" + vec[i] + "' in entry " + toString(i) + " of file '" + filename + "' for parameter " + _name + "!";
        }
        storage.push_back(val);
    }
    return true;
}

template <class T> void TMCMCParameter<T>::_readValsFromFile(const std::string& filename, std::vector<T> & storage) const{
    // 5 options:
    // 1) read trace file and pick column(s) for that parameter
    // 2) read simulation file and pick column(s) for that parameter
    if (stringContains(filename, MCMCFile::fileTypeToString[MCMCFile::trace]) || stringContains(filename, MCMCFile::fileTypeToString[MCMCFile::simulated])){
        TMCMCTraceFile_Reader file(filename);
        file.readParameterValuesIntoVec(_name, _storage->totalSize(), storage);
    }
    // 3) read meanVar file and pick column(s) for that parameter
    else if (stringContains(filename, MCMCFile::fileTypeToString[MCMCFile::meanVar])){
        TMCMCMeanVarFile_Reader file(filename);
        file.readParameterValuesIntoVec(_name, _storage->totalSize(), storage);
    }
    // 4) read a file with one column
    // 5) read a file with one row
    else if (!_readValsFromFile_oneColOrRow(filename, storage)){ // file format: all in one col
        throw "Invalid file format of " + filename + ". Expected a file whose filename contains 'trace', 'simulated' or 'meanVar', or then a file with either 1 or " + toString(_storage->totalSize()) + " lines.";
    }
}

template <class T> void TMCMCParameter<T>::_propose(const size_t & i){
    // is only called via update(), so we don't need to check if i <= size and if isUpdated()
    if (_sharedJumpSize){
       T newVal = _propKernel->propose(_storage->value(i), _boundary, _jumpSizeProposalVec[0], _randomGenerator);
       _storage->set(i, newVal);
       ++_acceptedUpdates[0];
    } else {
        T newVal = _propKernel->propose(_storage->value(i), _boundary, _jumpSizeProposalVec[i], _randomGenerator);
        _storage->set(i, newVal);
        ++_acceptedUpdates[i];
    }
    ++_totalUpdates;
}

template <class T> bool TMCMCParameter<T>::update(){
    // updates first element
    return update(0);
}

template <class T> bool TMCMCParameter<T>::update(const size_t & i){
    if (i >= _storage->totalSize()) throw std::runtime_error("Index i = " + toString(i) + " for array " + _name + " does not exist! (Size of array = " + toString(_storage->totalSize()) + ").");
    if (_isUpdated)
        _propose(i);
    return _isUpdated;
}

template <class T> double TMCMCParameter<T>::getLogPriorRatio(){
    // returns logPriorRatio of first element
    // use getLogPriorRatio(int i) instead!
    return getLogPriorRatio(0);
}

template <class T> double TMCMCParameter<T>::getLogPriorRatio(const size_t & i){
    if (i >= _storage->totalSize()) throw std::runtime_error("Index i = " + toString(i) + " for array " + _name + " does not exist! (Size of array = " + toString(_storage->totalSize()) + ").");
    //return log(proposal ratio) + log(prior ratio)
    //log(proposal ratio) is 0.0 as this update is symmetric
    if (_isUpdated)
        return _prior->getLogPriorRatio(shared_from_this(), i);
    return 0.;
}

template <class T> double TMCMCParameter<T>::getLogPriorDensity(){
    // returns getLogPriorDensity of first element
    return getLogPriorDensity(0);
}

template <class T> double TMCMCParameter<T>::getLogPriorDensity(const size_t & i){
    if (i >= _storage->totalSize())
        throw std::runtime_error("Index i = " + toString(i) + " for array " + _name + " does not exist! (Size of array = " + toString(_storage->totalSize()) + ").");
    if (_isUpdated)
        return _prior->getLogPriorDensity(shared_from_this(), i);
    return 0.;
}

template <class T> double TMCMCParameter<T>::getLogPriorDensityOld(){
    // returns getLogPriorDensityOld of first element
    return getLogPriorDensityOld(0);
}

template <class T> double TMCMCParameter<T>::getLogPriorDensityOld(const size_t & i){
    if (i >= _storage->totalSize()) throw std::runtime_error("Index i = " + toString(i) + " for array " + _name + " does not exist! (Size of array = " + toString(_storage->totalSize()) + ").");
    if (_isUpdated)
        return _prior->getLogPriorDensityOld(shared_from_this(), i);
    return 0.;
}

template <class T> double TMCMCParameter<T>::getLogPriorDensityFull(){
    if (_isUpdated)
        return _prior->getLogPriorDensityFull(shared_from_this());
    return 0.;
}

template <class T> void TMCMCParameter<T>::_reject(const size_t & i){
    // only accessed through acceptOrReject(), so no need to check if i < size and if isUpdated() is true
    _storage->reset(i);
    if (_sharedJumpSize) --_acceptedUpdates[0];
    else --_acceptedUpdates[i];
}

template <class T> bool TMCMCParameter<T>::acceptOrReject(const double & log_h){
    // accepts/rejects first element
    return acceptOrReject(0, log_h);
}

template <class T> bool TMCMCParameter<T>::acceptOrReject(const size_t & i, const double & log_h){
    if (i >= _storage->totalSize()) throw std::runtime_error("Index i = " + toString(i) + " for array " + _name + " does not exist! (Size of array = " + toString(_storage->totalSize()) + ").");
    if (_isUpdated) {
        if (log(_randomGenerator->getRand()) > log_h) {
            _reject(i);
            _addToMeanVar(i);
            return false;
        }
        _addToMeanVar(i);
        return true;
    }
    return _isUpdated;
}

template <class T> bool TMCMCParameter<T>::acceptOrRejectBatch(const TRange & Range, const double & log_h){
    // accept or reject all elements inside Range at once
    if (_isUpdated) {
        if (log(_randomGenerator->getRand()) > log_h) {
            for (size_t i = Range.first; i < Range.last; i += Range.increment){
                _reject(i);
                _addToMeanVar(i);
            }
            return false;
        }
        for (size_t i = Range.first; i < Range.last; i += Range.increment) {
            _addToMeanVar(i);
        }
        return true;
    }
    return _isUpdated;
}

template <class T> void TMCMCParameter<T>::_addToMeanVar(const size_t & i){
    // only accessed through acceptOrReject(), so no need to check if i < size and if isUpdated() is true
    if (!_meanVar.empty())
        _meanVar[i].add(_storage->value(i));
}

template <class T> double TMCMCParameter<T>::_mean(const size_t & i){
    if (i >= _storage->totalSize()) throw std::runtime_error("Mean for index i = " + toString(i) + " for array " + _name + " does not exist! (Size of array = " + toString(_storage->totalSize()) + ").");
    if (!_meanVar.empty())
        return _meanVar[i].mean();
    else throw std::runtime_error("Mean and var were not stored for parameter " + _name + "!");
}

template <class T> double TMCMCParameter<T>::_var(const size_t & i){
    if (i >= _storage->totalSize()) throw std::runtime_error("Var for index i = " + toString(i) + " for array " + _name + " does not exist! (Size of array = " + toString(_storage->totalSize()) + ").");
    if (!_meanVar.empty())
        return _meanVar[i].variance();
    else throw std::runtime_error("Mean and var were not stored for parameter " + _name + "!");
}

template <class T> double TMCMCParameter<T>::_sd(const size_t & i){
    if (i >= _storage->totalSize()) throw std::runtime_error("Sd for index i = " + toString(i) + " for array " + _name + " does not exist! (Size of array = " + toString(_storage->totalSize()) + ").");
    if (!_meanVar.empty())
        return _meanVar[i].sd();
    else throw std::runtime_error("Mean and var were not stored for parameter " + _name + "!");
}

template <class T>
template <class U>
void TMCMCParameter<T>::_set(U val){
    // sets value of first element
    _set(0, val);
}

template <class T>
template <class U>
void TMCMCParameter<T>::_set(const size_t & i, U val){
    if (i >= _storage->totalSize()) throw std::runtime_error("Set() for index i = " + toString(i) + " for array " + _name + " is not possible! (Size of array = " + toString(_storage->totalSize()) + ").");
    if (_isUpdated) {
        _storage->set(i, static_cast<T>(val));
        _boundary.checkRangeAndThrow(_storage->value(i), _name);
    }
}

template <class T> void TMCMCParameter<T>::set(double val){
    _set(val);
}

template <class T> void TMCMCParameter<T>::set(float val){
    _set(val);
}

template <class T> void TMCMCParameter<T>::set(int val){
    _set(val);
}

template <class T> void TMCMCParameter<T>::set(uint8_t val){
    _set(val);
}

template <class T> void TMCMCParameter<T>::set(uint16_t val){
    _set(val);
}
template <class T> void TMCMCParameter<T>::set(uint32_t val){
    _set(val);
}

template <class T> void TMCMCParameter<T>::set(uint64_t val){
    _set(val);
}

template <class T> void TMCMCParameter<T>::set(bool val){
    _set(val);
}

template <class T> void TMCMCParameter<T>::set(const size_t & i, double val){
    _set(i, val);
}

template <class T> void TMCMCParameter<T>::set(const size_t & i, float val){
   _set(i, val);
}

template <class T> void TMCMCParameter<T>::set(const size_t & i, int val){
    _set(i, val);
}

template <class T> void TMCMCParameter<T>::set(const size_t & i, uint8_t val){
    _set(i, val);
}

template <class T> void TMCMCParameter<T>::set(const size_t & i, uint16_t val){
    _set(i, val);
}

template <class T> void TMCMCParameter<T>::set(const size_t & i, uint32_t val){
    _set(i, val);
}

template <class T> void TMCMCParameter<T>::set(const size_t & i, uint64_t val){
    _set(i, val);
}

template <class T> void TMCMCParameter<T>::set(const size_t & i, bool val){
    _set(i, val);
}

template <class T> void TMCMCParameter<T>::reset(){
    reset(0);
}

template <class T> void TMCMCParameter<T>::reset(const size_t & i){
    if (i >= _storage->totalSize()) throw std::runtime_error("reset() for index i = " + toString(i) + " for array " + _name + " is not possible! (Size of array = " + toString(_storage->totalSize()) + ").");
    if (_isUpdated)
        _storage->reset(i);
}

template <class T> void TMCMCParameter<T>::setVal(double val) {
    setVal(0, val);
}
template <class T> void TMCMCParameter<T>::setVal(const size_t &i, double val) {
    if (i >= _storage->totalSize()) throw std::runtime_error("setVal() for index i = " + toString(i) + " for array " + _name + " is not possible! (Size of array = " + toString(_storage->totalSize()) + ").");
    if (_isUpdated) {
        _storage->setVal(i, static_cast<T>(val));
        _boundary.checkRangeAndThrow(_storage->value(i), _name);
    }
}

template <class T> void TMCMCParameter<T>::_setPropKernel(T val) {
    // only accessed through _readInitJumpSizesVals(), so no need to check if isUpdated() is true
    if (_sharedJumpSize) _jumpSizeProposalVec[0] = _propKernel->adjustPropKernelIfTooBig(val, _boundary, _name);
    else throw std::runtime_error("All parameters in array have different propKernels, please specify the index!");
}

template <class T> void TMCMCParameter<T>::_setPropKernel(T val, size_t i) {
    // only accessed through _readInitJumpSizesVals(), so no need to check if i < _size and if isUpdated() is true
    if (!_sharedJumpSize) _jumpSizeProposalVec[i] = _propKernel->adjustPropKernelIfTooBig(val, _boundary, _name);
    else throw std::runtime_error("All parameters in array have the same propKernels, don't specify index!");
}

template <class T> double TMCMCParameter<T>::getMeanAcceptanceRate(){
    if (_isUpdated){
        double sumAccepted = std::accumulate(_acceptedUpdates.begin(), _acceptedUpdates.end(), 0.);
        return (sumAccepted + 1.) / (_totalUpdates + 1.);
    }
    return 0.;
}

template <class T> double TMCMCParameter<T>::getAcceptanceRate(size_t i){
    // returns acceptance rate for one specific element in array
    if (_isUpdated) {
        if (_sharedJumpSize) return getAcceptanceRate();

        double totalUpdatesOneParam = static_cast<double>(_totalUpdates) / static_cast<double>(_storage->totalSize());
        return (_acceptedUpdates[i] + 1.) / (totalUpdatesOneParam + 1.);
    }
    return 0.;
}

template <class T> double TMCMCParameter<T>::getAcceptanceRate(){
    if (_isUpdated) {
        if (!_sharedJumpSize) return getMeanAcceptanceRate();

        // currently, if all parameters in array have same propKernel, I just count the total number of accepted updates,
        // i.e. no information about acceptance rates of individual parameters
        // pro: less memory, not relevant for algorithm itself
        // con: would provide interesting information, i.e. are there parameters that are accepted much less often?
        else return (_acceptedUpdates[0] + 1.) / (_totalUpdates + 1.);
    }
    return 0.;
}

template <class T> void TMCMCParameter<T>::printAccRateToLogfile(TLog* & logfile){
    if (_isUpdated){
        if (_sharedJumpSize)
            logfile->conclude("Acceptance rate " + _name + " = " + toString(getAcceptanceRate()));
        else
            logfile->conclude("Mean acceptance rate " + _name + " = " + toString(getMeanAcceptanceRate()));
    }
}

template <class T> T TMCMCParameter<T>::_jumpSize(size_t i) const {
    if (i >= _storage->totalSize())
        throw std::runtime_error("Jump size for " + _name + " with index i = " + toString(i) + " does not exist! (Size of array = " + toString(_storage->totalSize()) + ").");
    if (_isUpdated){
        if (!_sharedJumpSize) return _jumpSizeProposalVec[i];
        else return _jumpSizeProposalVec[0];
    }
    return 0.;
}

template <class T> T TMCMCParameter<T>::_jumpSize() const {
    if (_isUpdated){
        return _jumpSizeProposalVec[0];
    }
    return 0.;
}

template <class T> void TMCMCParameter<T>::adjustProposalAndResetCounters() {
    if (_isUpdated) {
        if (_sharedJumpSize) _adjustJumpSize(0);
        else {
            TRange range = _storage->getFull();
            for (size_t i = range.first; i < range.last; i+= range.increment)
                _adjustJumpSize(i);
        }

        if (!_meanVar.empty()){ // re-set mean and var after burnin
            TRange range = _storage->getFull();
            for (size_t i = range.first; i < range.last; i+= range.increment)
                _meanVar[i].clear();
        }
        _clearCountersAfterBurnin();
    }
}

template <class T> double TMCMCParameter<T>::_restrictScaleToNumericalLimit(double scale, T jump) {
    // make sure we don't cross upper numeric limit of that type when multiplying with scale
    // scale * jump <= range/2 -> then, everything is ok
    // if scale > range/(2*jump) -> restrict scale to max
    double maxScale = static_cast<double>(_boundary.range())/(2.*jump);
    if (scale > maxScale){
        scale = maxScale;
    }
    return scale;
}

template <class T> void TMCMCParameter<T>::_adjustJumpSize(size_t i){
    if(_totalUpdates > 0){
        //get scaling
        double scale = 3.0 * getAcceptanceRate(i);
        if(scale < 0.1) scale = 0.1;

        // make sure we don't cross upper numeric limit of that type when multiplying with scale
        scale = _restrictScaleToNumericalLimit(scale, _jumpSizeProposalVec[i]);

        //update
        _jumpSizeProposalVec[i] *= scale;
        _jumpSizeProposalVec[i] = _propKernel->adjustPropKernelIfTooBig(_jumpSizeProposalVec[i], _boundary, _name);
    }
}

template <class T> void TMCMCParameter<T>::_clearCountersAfterBurnin(){
    for (auto &it : _acceptedUpdates)
        it = 0;
    _totalUpdates = 0;
}

template <class T> inline void TMCMCParameter<T>::_value(double &dest) const {
    dest = static_cast<double>(_storage->value());
}

template <class T> inline void TMCMCParameter<T>::_value(float &dest) const {
    dest = static_cast<float>(_storage->value());
}

template <class T> inline void TMCMCParameter<T>::_value(int &dest) const {
    dest = static_cast<int>(_storage->value());
}

template <class T> inline void TMCMCParameter<T>::_value(uint8_t &dest) const {
    dest = static_cast<uint8_t>(_storage->value());
}

template <class T> inline void TMCMCParameter<T>::_value(uint16_t &dest) const {
    dest = static_cast<uint16_t>(_storage->value());
}

template <class T> inline void TMCMCParameter<T>::_value(uint32_t &dest) const {
    dest = static_cast<uint32_t>(_storage->value());
}

template <class T> inline void TMCMCParameter<T>::_value(uint64_t &dest) const {
    dest = static_cast<uint64_t>(_storage->value());
}

template <class T> inline void TMCMCParameter<T>::_value(bool &dest) const {
    dest = static_cast<bool>(_storage->value());
}

template <class T> inline void TMCMCParameter<T>::_value(const size_t & i, double &dest) const {
    dest = static_cast<double>(_storage->value(i));
}

template <class T> inline void TMCMCParameter<T>::_value(const size_t & i, float &dest) const {
    dest = static_cast<float>(_storage->value(i));
}

template <class T> inline void TMCMCParameter<T>::_value(const size_t & i, int &dest) const {
    dest = static_cast<int>(_storage->value(i));
}

template <class T> inline void TMCMCParameter<T>::_value(const size_t & i, uint8_t &dest) const {
    dest = static_cast<uint8_t>(_storage->value(i));
}

template <class T> inline void TMCMCParameter<T>::_value(const size_t & i, uint16_t &dest) const {
    dest = static_cast<uint16_t>(_storage->value(i));
}

template <class T> inline void TMCMCParameter<T>::_value(const size_t & i, uint32_t &dest) const {
    dest = static_cast<uint32_t>(_storage->value(i));
}

template <class T> inline void TMCMCParameter<T>::_value(const size_t & i, uint64_t &dest) const {
    dest = static_cast<uint64_t>(_storage->value(i));
}

template <class T> inline void TMCMCParameter<T>::_value(const size_t & i, bool &dest) const {
    dest = static_cast<bool>(_storage->value(i));
}

template <class T> inline void TMCMCParameter<T>::_oldValue(double &dest) const {
    dest = static_cast<double>(_storage->oldValue());
}

template <class T> inline void TMCMCParameter<T>::_oldValue(float &dest) const {
    dest = static_cast<float>(_storage->oldValue());
}

template <class T> inline void TMCMCParameter<T>::_oldValue(int &dest) const {
    dest = static_cast<int>(_storage->oldValue());
}

template <class T> inline void TMCMCParameter<T>::_oldValue(uint8_t &dest) const {
    dest = static_cast<uint8_t>(_storage->oldValue());
}

template <class T> inline void TMCMCParameter<T>::_oldValue(uint16_t &dest) const {
    dest = static_cast<uint16_t>(_storage->oldValue());
}

template <class T> inline void TMCMCParameter<T>::_oldValue(uint32_t &dest) const {
    dest = static_cast<uint32_t>(_storage->oldValue());
}

template <class T> inline void TMCMCParameter<T>::_oldValue(uint64_t &dest) const {
    dest = static_cast<uint64_t>(_storage->oldValue());
}

template <class T> inline void TMCMCParameter<T>::_oldValue(bool &dest) const {
    dest = static_cast<bool>(_storage->oldValue());
}

template <class T> inline void TMCMCParameter<T>::_oldValue(const size_t & i, double &dest) const {
    dest = static_cast<double>(_storage->oldValue(i));
}

template <class T> inline void TMCMCParameter<T>::_oldValue(const size_t & i, float &dest) const {
    dest = static_cast<float>(_storage->oldValue(i));
}

template <class T> inline void TMCMCParameter<T>::_oldValue(const size_t & i, int &dest) const {
    dest = static_cast<int>(_storage->oldValue(i));
}

template <class T> inline void TMCMCParameter<T>::_oldValue(const size_t & i, uint8_t &dest) const {
    dest = static_cast<uint8_t>(_storage->oldValue(i));
}

template <class T> inline void TMCMCParameter<T>::_oldValue(const size_t & i, uint16_t &dest) const {
    dest = static_cast<uint16_t>(_storage->oldValue(i));
}

template <class T> inline void TMCMCParameter<T>::_oldValue(const size_t & i, uint32_t &dest) const {
    dest = static_cast<uint32_t>(_storage->oldValue(i));
}

template <class T> inline void TMCMCParameter<T>::_oldValue(const size_t & i, uint64_t &dest) const {
    dest = static_cast<uint64_t>(_storage->oldValue(i));
}

template <class T> inline void TMCMCParameter<T>::_oldValue(const size_t & i, bool &dest) const {
    dest = static_cast<bool>(_storage->oldValue(i));
}

template <class T> inline double TMCMCParameter<T>::logValue() const {
    // just returns value of first element
    return _storage->logValue();
}

template <class T> inline double TMCMCParameter<T>::logValue(const size_t & i) const {
    return _storage->logValue(i);
}

template <class T> inline double TMCMCParameter<T>::oldLogValue() const {
    // just returns old value of first element
    return _storage->oldLogValue();
}

template <class T> inline double TMCMCParameter<T>::oldLogValue(const size_t & i) const {
    return _storage->oldLogValue(i);
}

template <class T> inline double TMCMCParameter<T>::expValue() const {
    // just returns value of first element
    return _storage->expValue();
}

template <class T> inline double TMCMCParameter<T>::expValue(const size_t & i) const {
    return _storage->expValue(i);
}

template <class T> inline double TMCMCParameter<T>::oldExpValue() const {
    // just returns old value of first element
    return _storage->oldExpValue();
}

template <class T> inline double TMCMCParameter<T>::oldExpValue(const size_t & i) const {
    return _storage->oldExpValue(i);
}

template <class T> size_t TMCMCParameter<T>::getValueUntranslated(const size_t & i) {
    throw std::runtime_error("In function 'size_t TMCMCParameter::getValueUntranslated': Should not get here - untranslated values only make sense for observations!");
}

template <class T> void TMCMCParameter<T>::updatePrior(){
    _prior->updateParams();
}

template <class T> void TMCMCParameter<T>::fillHeader(std::vector<std::string> &header) {
    _storage->appendToVectorOfAllFullDimensionNamesWithPrefix(header, _name);
}

template <class T> void TMCMCParameter<T>::writeVals(TOutputFile &file) {
    TRange range = _storage->getFull();
    for (size_t i = range.first; i < range.last; i+= range.increment)
        file << toString(_storage->value(i)); // use toString in order to print unsigned ints to file
        // without needing to cast them to a smaller container (e.g. if T=size_t and we would cast to int here -> cut off)
}

template <class T> void TMCMCParameter<T>::writeValsOneString(TOutputFile &file) {
    // values (comma-separated)
    TRange range = _storage->getFull();
    std::string vals = toString(_storage->value(range.first));
    for (size_t i = range.first + 1; i < range.last; i+= range.increment)
        vals += "," + toString(_storage->value(i));
    file << vals;
}

template <class T> void TMCMCParameter<T>::writeMean(TOutputFile &file) {
    TRange range = _storage->getFull();
    if (!_isUpdated){
        for (size_t i = range.first; i < range.last; i+= range.increment)
            file << toString(_storage->value(i)); // write value if parameter is fixed
    } else {
        for (size_t i = range.first; i < range.last; i+= range.increment)
            file << _mean(i);
    }
}

template <class T> void TMCMCParameter<T>::writeVar(TOutputFile &file) {
    TRange range = _storage->getFull();
    for (size_t i = range.first; i < range.last; i+= range.increment)
        file << _var(i);
}

template <class T> void TMCMCParameter<T>::writeJumpSizeOneString(TOutputFile &file) {
    TRange range = _storage->getFull();
    std::string jumpSizes = toString(_jumpSize(range.first));
    for (size_t i = range.first + 1; i < range.last; i+= range.increment)
        jumpSizes += "," + toString(_jumpSize(i));
    file << jumpSizes;
}


template <typename T> double TMCMCParameter<T>::min() const {
    return static_cast<double>(_boundary.min());
}

template <typename T> double TMCMCParameter<T>::max() const {
    return static_cast<double>(_boundary.max());
}

template <class T> bool TMCMCParameter<T>::minIncluded() const{
    return _boundary.minIncluded();
}

template <class T> bool TMCMCParameter<T>::maxIncluded() const{
    return _boundary.maxIncluded();
}

template <class T> void TMCMCParameter<T>::_initBoundary(bool hasDefaultMin, bool hasDefaultMax, const std::string & min, const std::string & max, bool minIncluded, bool maxIncluded){
    _boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
}

template <class T> void TMCMCParameter<T>::reSetBoundaries(bool hasDefaultMin, bool hasDefaultMax, std::string min, std::string max, bool minIncluded, bool maxIncluded) {
    // this function is called by TMCMCParameter in order to re-adjust the boundaries of priors with hyper priors
    // e.g. your parameter array has a normal prior with mean and sd. sd itself is a TMCMCParameter that is inferred. However, the user forgot
    // to specify min = 0 for sd. In this function, I re-set this min (and/or max) to what it should be (given by arguments).
    // However, maybe the user specified min = 2 for sd. In this case, I don't want to overwrite min, as it is mathematically valid.
    _boundary.resetBoundary(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
}

template <class T> bool TMCMCParameter<T>::valueIsInsideBoundary(const double & Value) const {
    return _boundary.checkRange(static_cast<T>(Value));
}

template <class T> void TMCMCParameter<T>::typesAreCompatible(bool throwIfNot) const {
    _prior->typesAreCompatible(throwIfNot);
}

template <class T> bool TMCMCParameter<T>::isFloatingPoint() const {
    return std::is_floating_point<T>::value;
}
template <class T> bool TMCMCParameter<T>::isIntegral() const {
    return std::is_integral<T>::value;
}
template <class T> bool TMCMCParameter<T>::isUnsigned() const {
    return std::is_unsigned<T>::value;
}

template<typename T> size_t TMCMCParameter<T>::getIndex(const std::vector<size_t> &coord) {
    return _storage->getIndex(coord);
}

template<typename T> TRange TMCMCParameter<T>::getRange(const std::vector<size_t> &startCoord, const std::vector<size_t> &endCoord) {
    return _storage->getRange(startCoord, endCoord);
}

template<typename T> TRange TMCMCParameter<T>::getFull() {
    return _storage->getFull();
}

template<typename T> TRange TMCMCParameter<T>::getDiagonal() {
    return _storage->getDiagonal();
}

template <typename T> TRange TMCMCParameter<T>::get1DSlice(size_t dim, const std::vector<size_t> &startCoord) {
    return _storage->get1DSlice(dim, startCoord);
}

template<typename T> size_t TMCMCParameter<T>::numDim() const {
    return _storage->numDim();
}

template<typename T> std::vector<size_t> TMCMCParameter<T>::dimensions() const {
    return _storage->dimensions();
}

template <typename T> size_t TMCMCParameter<T>::totalSize() const {
    return _storage->totalSize();
}

template <typename T> std::vector<size_t> TMCMCParameter<T>::getSubscripts(size_t linearIndex) const {
    return _storage->getSubscripts(linearIndex);
}

template <typename T> bool TMCMCParameter<T>::isUpdated() const {
    return _isUpdated;
}

template <typename T>
void TMCMCParameter<T>::simulateUnderPrior(TLog *logfile, TParameters & Parameters) {
    _prior->simulateUnderPrior(this->_randomGenerator, logfile, Parameters);
}

