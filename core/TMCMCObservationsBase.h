//
// Created by madleina on 22.01.21.
//

#ifndef EMPTY_TMCMCOBSERVATIONSBASE_H
#define EMPTY_TMCMCOBSERVATIONSBASE_H

#include "TDistances.h"
#include "TLog.h"
#include "TMultiDimensionalStorage.h"
#include "TFile.h"
#include "TParameters.h"
#include <memory>
#include <utility>
#include <numeric>

// forward declare TDAG
class TDAG;

//-------------------------------------------
// TMCMCObservationsBase
//-------------------------------------------
class TMCMCObservationsBase {
    // abstract base class for TMCMCParameterBase and TObservations
    // provides all functions for a parameter/data object on which a prior is defined
protected:
    // name
    std::string _name;

    // is this parameter/observation part of a prior (e.g. a prior parameter or covariate observation)?
    // is false: is at bottom of DAG
    bool _isPartOfPrior;

    // has this parameter a user-defined initial value?
    // if no -> we will initialize it with MLE (or EM/MOM)
    //       -> or simulate under the prior distribution
    bool _hasFixedInitialValue;

    virtual void _value(double & dest) const = 0;
    virtual void _value(float & dest) const = 0;
    virtual void _value(int & dest) const = 0;
    virtual void _value(uint8_t & dest) const = 0;
    virtual void _value(uint16_t & dest)const  = 0;
    virtual void _value(uint32_t & dest) const = 0;
    virtual void _value(uint64_t & dest) const  = 0;
    virtual void _value(bool & dest) const = 0;

    virtual void _value(const size_t & i, double & dest) const = 0;
    virtual void _value(const size_t & i, float & dest) const = 0;
    virtual void _value(const size_t & i, int & dest) const = 0;
    virtual void _value(const size_t & i, uint8_t & dest) const = 0;
    virtual void _value(const size_t & i, uint16_t & dest) const = 0;
    virtual void _value(const size_t & i, uint32_t & dest) const = 0;
    virtual void _value(const size_t & i, uint64_t & dest) const = 0;
    virtual void _value(const size_t & i, bool & dest) const = 0;

public:
    TMCMCObservationsBase(){
        _isPartOfPrior = false;
        _hasFixedInitialValue = false;
    }
    virtual ~TMCMCObservationsBase() = default;

    // functions for DAG
    virtual void initializeStorage() = 0;
    virtual void initializeStorageSingleElementBasedOnPrior() = 0;
    virtual void initializeStorageBasedOnPrior(const std::vector<size_t> & ExpectedDimensions, const std::vector<std::shared_ptr<TNamesEmpty>> & DimensionNames) = 0;
    virtual void handPointerToPrior(const std::shared_ptr<TMCMCObservationsBase>& pointer) = 0;
    virtual void constructDAG(TDAG & DAG, TDAG & temporaryDAG) = 0;
    bool isPartOfPrior(){
        return _isPartOfPrior;
    }
    void setIsPartOfPrior(){
        _isPartOfPrior = true;
    }

    // name of parameter and dimensions
    virtual std::string name() const{
        return _name;
    };
    virtual const std::shared_ptr<TNamesEmpty> & getDimensionName(const size_t & Dim) = 0;

    // values
    template<class T> T value() const{
        auto tmp = T{};
        _value(tmp);
        return tmp;
    }
    template<class T> T value(const size_t & i) const{
        auto tmp = T{};
        _value(i, tmp);
        return tmp;
    }

    virtual double logValue() const = 0;
    virtual double logValue(const size_t & i) const = 0;
    virtual double expValue() const = 0;
    virtual double expValue(const size_t & i) const = 0;
    // get raw value (for observations)
    virtual size_t getValueUntranslated(const size_t & i) = 0;

    // boundaries
    virtual double min() const = 0;
    virtual double max() const = 0;

    // initialize prior parameters (e.g. with MLE)
    bool hasFixedInitialValue(){
        return _hasFixedInitialValue;
    }
    virtual void estimateInitialPriorParameters(TLog * Logfile) = 0;

    // accepting / rejecting
    virtual void updatePrior() = 0;
    virtual double getLogPriorDensity() = 0;
    virtual double getLogPriorDensity(const size_t & i) = 0;
    virtual double getLogPriorDensityFull() = 0;

    // checks: boundaries, types
    virtual void reSetBoundaries(bool hasDefaultMin, bool hasDefaultMax, std::string min, std::string max, bool minIncluded, bool maxIncluded) = 0;
    virtual void typesAreCompatible(bool throwIfNot) const = 0;
    virtual bool valueIsInsideBoundary(const double & Value) const = 0;
    virtual bool isFloatingPoint() const = 0;
    virtual bool isIntegral() const = 0;
    virtual bool isUnsigned() const = 0;

    // indexing
    virtual size_t getIndex(const std::vector<size_t>& coord) = 0;
    virtual TRange getRange(const std::vector<size_t> & startCoord, const std::vector<size_t> & endCoord) = 0;
    virtual TRange getFull() = 0;
    virtual TRange getDiagonal() = 0;
    virtual TRange get1DSlice(size_t dim, const std::vector<size_t> & startCoord) = 0;
    virtual size_t numDim() const = 0;
    virtual std::vector<size_t> dimensions() const = 0;
    virtual size_t totalSize() const = 0;
    virtual std::vector<size_t> getSubscripts(size_t linearIndex) const = 0;

    // set distances (for HMM priors)
    virtual void setDistances(const std::shared_ptr<TDistancesBinnedBase> & distances) = 0;

    // set values
    virtual void set(double val) = 0;
    virtual void set(float val) = 0;
    virtual void set(int val) = 0;
    virtual void set(uint8_t val) = 0;
    virtual void set(uint16_t val) = 0;
    virtual void set(uint32_t val) = 0;
    virtual void set(uint64_t val) = 0;
    virtual void set(bool val) = 0;

    virtual void set(const size_t & i, double val) = 0;
    virtual void set(const size_t & i, float val) = 0;
    virtual void set(const size_t & i, int val) = 0;
    virtual void set(const size_t & i, uint8_t val) = 0;
    virtual void set(const size_t & i, uint16_t val) = 0;
    virtual void set(const size_t & i, uint32_t val) = 0;
    virtual void set(const size_t & i, uint64_t val) = 0;
    virtual void set(const size_t & i, bool val) = 0;

    // simulate values under prior distribution
    virtual void simulateUnderPrior(TLog *logfile, TParameters & Parameters) = 0;

    // writers
    virtual void writeVals(TOutputFile & file) = 0;
    virtual void fillHeader(std::vector<std::string> & header) = 0;
};

//-------------------------
// TObservationsBase
//-------------------------
class TObservationsBase : public TMCMCObservationsBase {
    // pure virtual base class for observations
    // not templated
    // provides interface necessary to interact with observations
protected:

public:
    TObservationsBase() = default;
    ~TObservationsBase() override = default;

    // fill data
    virtual void prepareFillData(const size_t & guessLengthOfUnknownDimension, const std::vector<size_t> & allKnownDimensions) = 0;

    virtual void fillDataRaw(const double & value) = 0;
    virtual void fillDataRaw(const float & value) = 0;
    virtual void fillDataRaw(const int & value) = 0;
    virtual void fillDataRaw(const uint8_t & value) = 0;
    virtual void fillDataRaw(const uint16_t & value) = 0;
    virtual void fillDataRaw(const uint32_t & value) = 0;
    virtual void fillDataRaw(const uint64_t & value) = 0;
    virtual void fillDataRaw(const bool & value) = 0;

    virtual void fillDataAndTranslate(const double & value) = 0;
    virtual void fillDataAndTranslate(const float & value) = 0;
    virtual void fillDataAndTranslate(const int & value) = 0;
    virtual void fillDataAndTranslate(const uint8_t & value) = 0;
    virtual void fillDataAndTranslate(const uint16_t & value) = 0;
    virtual void fillDataAndTranslate(const uint32_t & value) = 0;
    virtual void fillDataAndTranslate(const uint64_t & value) = 0;
    virtual void fillDataAndTranslate(const bool & value) = 0;

    virtual void finalizeFillData() = 0;

    // fill dimension names
    virtual void setDimensionName(const std::shared_ptr<TNamesEmpty> & Name, const size_t & Dim) = 0;
    virtual void resize(const std::vector<size_t> & Dimensions) = 0;
};


#endif //EMPTY_TMCMCOBSERVATIONSBASE_H
