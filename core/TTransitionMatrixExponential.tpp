﻿//
// Created by madleina on 31.07.20.
//

//-------------------------------------------
// TGeneratingMatrix
//-------------------------------------------
template <typename PrecisionType, typename NumStatesType>
TGeneratingMatrixBase<PrecisionType,NumStatesType>::TGeneratingMatrixBase() {
    _generatingMatrix.resize(0, 0, 0.);
}

template <typename PrecisionType, typename NumStatesType>
TGeneratingMatrixBase<PrecisionType,NumStatesType>::TGeneratingMatrixBase(const NumStatesType &NumStates, const NumStatesType & Bandwidth) {
    resize(NumStates, Bandwidth);
}

template <typename PrecisionType, typename NumStatesType>
void TGeneratingMatrixBase<PrecisionType,NumStatesType>::resize(const NumStatesType &NumStates, const NumStatesType &Bandwidth) {
    _generatingMatrix.resize(NumStates, Bandwidth, 0.);
}

template <typename PrecisionType, typename NumStatesType>
const PrecisionType& TGeneratingMatrixBase<PrecisionType,NumStatesType>::operator()(const NumStatesType &From, const NumStatesType &To) const {
    return _generatingMatrix(From, To);
}

template <typename PrecisionType, typename NumStatesType>
const TBandMatrix & TGeneratingMatrixBase<PrecisionType,NumStatesType>::getGeneratingMatrix() const {
    return _generatingMatrix;
}

template <typename PrecisionType, typename NumStatesType>
bool TGeneratingMatrixBase<PrecisionType,NumStatesType>::fillStationary(std::vector<PrecisionType> &Values) {
    // default: generating matrix does not know how to calculate stationary distribution of Markov Chain -> return false
    Values.resize(_generatingMatrix.size(), 0.);
    return false;
}

template <typename PrecisionType, typename NumStatesType>
const TMatrix & TGeneratingMatrixBase<PrecisionType,NumStatesType>::getUnparametrizedGeneratingMatrix() const {
    // for some generating matrices (those that only have one parameter),
    // it is possible to seperate the generating matrix into parameter * Matrix
    // where Matrix is then unparametrized
    throw std::runtime_error("Function 'const TMatrix & TGeneratingMatrixBase::getUnparametrizedGeneratingMatrix() const': not implemented for base class TGeneratingMatrixBase!");
}

//-------------------------------------------
// TGeneratingMatrixBool
//-------------------------------------------

template <typename PrecisionType, typename NumStatesType>
TGeneratingMatrixBool<PrecisionType,NumStatesType>::TGeneratingMatrixBool() : TGeneratingMatrixBase<PrecisionType, NumStatesType>() {
    // generating matrix for bools always has 2 states
    resize(2, 1);
}

template <typename PrecisionType, typename NumStatesType>
void TGeneratingMatrixBool<PrecisionType,NumStatesType>::resize(const NumStatesType &NumStates, const NumStatesType &Bandwidth) {
    if (NumStates != 2){
        throw std::runtime_error("In function 'void TGeneratingMatrixBool::resize(const NumStatesType &NumStates, const NumStatesType &Bandwidth)': Generating matrix for booleans can only have 2 states (not " + toString(NumStates) + ")!");
    }
    if (Bandwidth != 1){
        throw std::runtime_error("In function 'void TGeneratingMatrixBool::resize(const NumStatesType &NumStates, const NumStatesType &Bandwidth)': Generating matrix for booleans must have bandwidth 1 (not " + toString(Bandwidth) + ")!");
    }
    TGeneratingMatrixBase<PrecisionType,NumStatesType>::resize(NumStates, Bandwidth);
}

template <typename PrecisionType, typename NumStatesType>
void TGeneratingMatrixBool<PrecisionType,NumStatesType>::fillGeneratingMatrix(const std::vector<PrecisionType> & Values) {
    assert(Values.size() == 2);

    // fill generating matrix
    _generatingMatrix(0,0) = -Values[0]; _generatingMatrix(0,1) = Values[0];
    _generatingMatrix(1,0) = Values[1]; _generatingMatrix(1,1) = -Values[1];
}

template <typename PrecisionType, typename NumStatesType>
bool TGeneratingMatrixBool<PrecisionType,NumStatesType>::fillStationary(std::vector<PrecisionType> &Values) {
    // stationary distribution of Markov Chain can be directly computed from parameters of generating matrix
    double lambda1 = _generatingMatrix(0,1);
    double lambda2 = _generatingMatrix(1,0);
    double ePowerLambda1Lambda2 = exp(lambda1 + lambda2);
    double P0 = (-lambda2 * ePowerLambda1Lambda2 + lambda2) / (-ePowerLambda1Lambda2 * (lambda1 + lambda2) + lambda1 + lambda2);
    Values = {P0, 1.-P0};

    return true;
}

//-------------------------------------------
// TGeneratingMatrixLadder
//-------------------------------------------

template <typename PrecisionType, typename NumStatesType>
TGeneratingMatrixLadder<PrecisionType,NumStatesType>::TGeneratingMatrixLadder() : TGeneratingMatrixBase<PrecisionType,NumStatesType>() {}

template <typename PrecisionType, typename NumStatesType>
TGeneratingMatrixLadder<PrecisionType,NumStatesType>::TGeneratingMatrixLadder(const NumStatesType &NumStates) {
    resize(NumStates, 1);
}

template <typename PrecisionType, typename NumStatesType>
void TGeneratingMatrixLadder<PrecisionType,NumStatesType>::resize(const NumStatesType &NumStates, const NumStatesType &Bandwidth) {
    if (NumStates < 2){
        throw std::runtime_error("In function 'void TGeneratingMatrixLadder::resize(const NumStatesType &NumStates, const NumStatesType &Bandwidth)': Generating matrix for ladder-type transition matrix must have at least 2 states (not " + toString(NumStates) + ")!");
    }
    if (Bandwidth != 1){
        throw std::runtime_error("In function 'void TGeneratingMatrixLadder::resize(const NumStatesType &NumStates, const NumStatesType &Bandwidth)': Generating matrix for ladder-type transition matrix must have bandwidth 1 (not " + toString(Bandwidth) + ")!");
    }
    TGeneratingMatrixBase<PrecisionType,NumStatesType>::resize(NumStates, Bandwidth);

    // fill raw lambda
    _fillRawLambda(NumStates);
}

template <typename PrecisionType, typename NumStatesType>
void TGeneratingMatrixLadder<PrecisionType,NumStatesType>::_fillRawLambda(const NumStatesType &NumStates) {
    _rawLambda.resize(NumStates);

    // fill "raw" lambda matrix -> matrix without kappa
    _rawLambda(0, 0) = -1; _rawLambda(0, 1) = 1;
    for (NumStatesType i = 1; i < NumStates-1; i++){
        _rawLambda(i, i-1) = 1 ; _rawLambda(i, i) = -2; _rawLambda(i, i+1) = 1;
    }
    _rawLambda(NumStates-1, NumStates-2) = 1; _rawLambda(NumStates-1, NumStates-1) = -1;
}

template <typename PrecisionType, typename NumStatesType>
void TGeneratingMatrixLadder<PrecisionType,NumStatesType>::fillGeneratingMatrix(const std::vector<PrecisionType> & Values) {
    // only expect one value: kappa
    assert(Values.size() == 1);

    PrecisionType kappa = Values[0];
    NumStatesType numStates = _generatingMatrix.size();

    // fill generating matrix
    _generatingMatrix(0, 0) = -kappa; _generatingMatrix(0, 1) = kappa;
    for (NumStatesType i = 1; i < numStates-1; i++){
        _generatingMatrix(i, i-1) = kappa ; _generatingMatrix(i, i) = -2.*kappa; _generatingMatrix(i, i+1) = kappa;
    }
    _generatingMatrix(numStates-1, numStates-2) = kappa; _generatingMatrix(numStates-1, numStates-1) = -kappa;
}

template <typename PrecisionType, typename NumStatesType>
bool TGeneratingMatrixLadder<PrecisionType,NumStatesType>::fillStationary(std::vector<PrecisionType> &Values) {
    // stationary distribution of Markov Chain can be directly computed from parameters of generating matrix
    // 1/NStates for each state: probability to go up/down is exactly the same for all states
    NumStatesType numStates = _generatingMatrix.size();
    Values.resize(numStates, 1./numStates);

    return true;
}

template <typename PrecisionType, typename NumStatesType>
const TMatrix & TGeneratingMatrixLadder<PrecisionType,NumStatesType>::getUnparametrizedGeneratingMatrix() const {
    return _rawLambda;
}

//-------------------------------------------
// TTransitionMatrixExponentialBase
//-------------------------------------------
template <typename PrecisionType, typename NumStatesType, typename LengthType>
TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::TTransitionMatrixExponentialBase() : TTransitionMatrix_base<PrecisionType,NumStatesType,LengthType>() {
    _numStates = 0;
    _fixTransitionMatricesDuringEM = false;
    _firstIteration = true;

    _logfile = nullptr;
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::TTransitionMatrixExponentialBase(NumStatesType numStates, const std::shared_ptr<TDistancesBinnedBase> & distances) : TTransitionMatrix_base<PrecisionType,NumStatesType,LengthType>(numStates) {
    _numStates = 0;
    _fixTransitionMatricesDuringEM = false;
    _firstIteration = true;

    _logfile = nullptr;

    initializeWithDistances(numStates, distances);
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::addGeneratingMatrix(std::unique_ptr<TGeneratingMatrixBase<PrecisionType,NumStatesType>> &GeneratingMatrix) {
    _generatingMatrix = std::move(GeneratingMatrix);
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::initialize(NumStatesType numStates) {
    _numStates = numStates;
    this->_init(numStates);
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::initializeWithDistances(NumStatesType numStates, const std::shared_ptr<TDistancesBinnedBase> & distances) {
    initialize(numStates);
    if (!distances)
        throw std::runtime_error("In function 'void THMMBase::initialize(mixtureType numStates, const std::shared_ptr<TDistancesBinned> & distances)': distances are not initialized!");
    if (distances->size() == 0)
        throw std::runtime_error("In function 'void THMMBase::initialize(mixtureType numStates, const std::shared_ptr<TDistancesBinned> & distances)': distances are not filled!");
    _distances = distances;
    _transitionMatrices = std::make_unique<std::vector<TMatrix>>(_distances->numDistanceGroups(), TMatrix(_numStates, _numStates, 0.));
    _transitionMatrices_old = std::make_unique<std::vector<TMatrix>>(_distances->numDistanceGroups(), TMatrix(_numStates, _numStates, 0.));
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::_fillGeneratingMatrix(const std::vector<PrecisionType> & Values){
    _generatingMatrix->fillGeneratingMatrix(Values);
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::_fillParametersBasedOnGeneratingMatrix(const std::vector<PrecisionType> & Values) {
    // function can stay empty
    // overridden in derived classes that update the parameters of the generating matrix in an MCMC
    // used after EM has finished: need to get the values of the parameters of the generating matrix and set the MCMC parameters correspondingly
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::_fillTransitionProbabilities() {
    // (*_transitionMatrices)[0] contains stationary distribution -> filled in function _fillStationaryDistribution

    //get exponential of lambda (-> distance = 1)
    (*_transitionMatrices)[1].fillAsExponential(_generatingMatrix->getGeneratingMatrix());

    //fill all other transition matrices by squaring the previous one (-> all other distances)
    for (size_t i = 2; i < _transitionMatrices->size(); i++){
        (*_transitionMatrices)[i].fillFromSquare((*_transitionMatrices)[i-1]);
    }
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::_fillStationaryDistribution() {
    std::vector<PrecisionType> values;
    if (_generatingMatrix->fillStationary(values)){
        // stationary can be calculated directly from generating matrix parameters -> no need to do armadillo stuff
        // fill pi into Q0
        for (NumStatesType i = 0; i < _numStates; i++){
            for (NumStatesType j = 0; j < _numStates; j++){
                (*_transitionMatrices)[0](i, j) = values[j];
            }
        }
    } else {
        _fillStationaryDistribution_SolveNormalEquations();
    }
}

#ifdef WITH_ARMADILLO
template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::_fillStationaryDistribution_SolveNormalEquations() {
    // fill armadillo matrix with
    // t(I - Q(1))
    // where Q(1) is the transition matrix for distanceGroup 1
    arma::mat A(_numStates+1, _numStates);
    for (NumStatesType i = 0; i < _numStates; i++){
        for (NumStatesType j = 0; j < _numStates; j++){
            if (i == j){
                A(i, j) = 1. - (*_transitionMatrices)[1](j, i);
            } else {
                A(i, j) = -(*_transitionMatrices)[1](j, i);
            }
        }
    }
    // fill last row with 1's
    for (NumStatesType j = 0; j < _numStates; j++){
        A(_numStates, j) = 1.;
    }

    // construct column vector of size _numStates filled with zeros, and add an extra row with a 1
    arma::mat b(_numStates+1, 1, arma::fill::zeros);
    b(_numStates, 0) = 1.;

    // solve for pi (the stationary distribution)
    arma::vec pi = arma::solve(A.t() * A, A.t() * b);

    // fill pi into Q0
    for (NumStatesType i = 0; i < _numStates; i++){
        for (NumStatesType j = 0; j < _numStates; j++){
            (*_transitionMatrices)[0](i, j) = pi(j);
        }
    }
}
# else
// armadillo is not defined -> will throw an error, because general transition matrices require armadillo
template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::_fillStationaryDistribution_SolveNormalEquations() {
    throw std::runtime_error("Function 'void TTransitionMatrixExponential::_fillStationaryDistribution_SolveNormalEquations()' requires armadillo to be installed!");
}
#endif // WITH_ARMADILLO

template <typename PrecisionType, typename NumStatesType, typename LengthType>
const TMatrix& TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::operator[](const size_t &index) const {
    if (index >= _transitionMatrices->size())
        throw std::runtime_error("In function 'const TMatrix& TTransitionMatrixExponential::operator[](const size_t &index) const': dist >= numDistanceGroups!");
    return (*_transitionMatrices)[index];
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
TMatrix& TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::operator[](const size_t &index) {
    if (index >= _transitionMatrices->size())
        throw std::runtime_error("In function 'TMatrix& TTransitionMatrixExponential::operator[](const size_t &index)': dist >= numDistanceGroups!");
    return (*_transitionMatrices)[index];
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
const PrecisionType& TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::operator()(const LengthType & Index, const NumStatesType & From, const NumStatesType & To) const{
    size_t distGroup = _distances->operator[](Index);
    return (*_transitionMatrices)[distGroup](From, To);
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
const PrecisionType& TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::stationary(const NumStatesType &State) const {
    return (*_transitionMatrices)[0](State, State);
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
size_t TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::numDistanceGroups() const {
    return _distances->numDistanceGroups();
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
bool TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::_checkRowSumTransitionMatrices() const {
    // returns false if any row of any transition matrix does not sum to 1 (allow for +- absError difference)
    double absError = 0.0001;

    for (size_t d = 1; d < _distances->numDistanceGroups(); d++) {
        for (NumStatesType i = 0; i < _numStates; i++) { // rows
            if (std::fabs(1. - (*_transitionMatrices)[d].rowSum(i)) > absError){
                return false;
            }
        }
    }
    return true;
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::handleEMParameterInitializationTransitionProbabilities(const LengthType & Index, const NumStatesType & PreviousState, const NumStatesType & CurrentState) {
    // initialize transition matrices prior to EM, assuming z are set to some initial values -> count transitions
    // abuse _transitionMatrices_old to count transitions

    size_t distGroup = _distances->operator[](Index);
    if (distGroup == 0){
        throw std::runtime_error("In function 'void TTransitionMatrixExponential::handleEMParameterInitializationTransitionProbabilities(const LengthType & Index, const NumStatesType & PreviousState, const NumStatesType & CurrentState)': distGroup is 0. This should never happen, as distGroup=0 is at beginning of junk!");
    }

    // count transition z_{t-1} -> z_t
    (*_transitionMatrices_old)[distGroup](PreviousState, CurrentState)++;
    _EM_colSums[distGroup][PreviousState]++;
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::finalizeEMParameterInitializationTransitionProbabilities() {
    // add +1 if a transition never happened (would result in nan)
    for (size_t d = 1; d < _distances->numDistanceGroups(); d++){
        for (NumStatesType i = 0; i < _numStates; i++) { // z_{t-1}
            for (NumStatesType j = 0; j < _numStates; j++) { // z_t
                if ((*_transitionMatrices_old)[d](i, j) == 0){
                    (*_transitionMatrices_old)[d](i, j)++;
                    _EM_colSums[d][i]++;
                }
            }
        }
    }

    // initialize transition matrices
    finalizeEMParameterEstimationOneIterationTransitionProbabilities();
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::runEMEstimation(TLatentVariable<PrecisionType, NumStatesType, LengthType> & latentVariable, bool fixTransitionMatricesDuringEM, TLog * Logfile){
    // update transition matrices in EM?
    _fixTransitionMatricesDuringEM = fixTransitionMatricesDuringEM;
    _logfile = Logfile;
    _firstIteration = true;

    THMM<PrecisionType, NumStatesType, LengthType > hmm(*this, latentVariable, 100, 0.0001, true);
    hmm.report(_logfile);

    // get junk ends from distances
    std::vector<LengthType> junkEnds = _distances->getJunkEnds<LengthType>();

    hmm.runEM(junkEnds);

    // to initialize z
    hmm.estimateStatePosteriors(junkEnds, false);
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::report(TLog * LogFile) {
    _logfile = LogFile;
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::prepareEMParameterEstimationInitial() {
    _EM_colSums.resize(_distances->numDistanceGroups(), std::vector<PrecisionType>(_numStates, 0.));
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::prepareEMParameterEstimationOneIterationTransitionProbabilities(){
    for (size_t d = 1; d < _distances->numDistanceGroups(); d++){
        std::fill(_EM_colSums[d].begin(), _EM_colSums[d].end(), 0.);
        (*_transitionMatrices_old)[d].set(0.);
    }
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::handleEMParameterEstimationOneIterationTransitionProbabilities(const LengthType & Index, const THMMPosteriorXi<PrecisionType, NumStatesType , LengthType> & xi){
    // we abuse _transitionMatrices_old to store EM sums
    size_t distGroup = _distances->operator[](Index);
    if (distGroup == 0){
        throw std::runtime_error("In function 'void TTransitionMatrixExponential::handleEMParameterEstimationOneIterationTransitionProbabilities(const LengthType & Index, const THMMPosteriorXi<PrecisionType, NumStatesType , LengthType> & xi)': distGroup is 0. This should never happen, as distGroup=0 is at beginning of junk!");
    }
    TMatrix& sum = (*_transitionMatrices_old)[distGroup];
    for (NumStatesType i = 0; i < _numStates; i++){ // z_{t-1}
        for (NumStatesType j = 0; j < _numStates; j++){ // z_t
            sum(i, j) += xi(i, j);
            _EM_colSums[distGroup][i] += xi(i, j);
        }
    }
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::_storeGeneratingMatrix(const std::vector<PrecisionType> & Values){
    // fill into generator matrix
    _fillGeneratingMatrix(Values);
    // fill all other matrices
    _fillTransitionProbabilities();
    _fillStationaryDistribution();
    // fill MCMC parameters with estimated values (overridden in derived class, if needed)
    _fillParametersBasedOnGeneratingMatrix(Values);
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::finalizeEMParameterEstimationOneIterationTransitionProbabilities() {
    if (!_fixTransitionMatricesDuringEM) {
        std::vector<PrecisionType> values = _maximizeQFunction_GeneratingMatrix(false);
        _storeGeneratingMatrix(values);
    }
    // switch bool
    if (_firstIteration){
        _firstIteration = !_firstIteration;
    }

    // assert that all rows of all transition matrices sum up to 1
    assertm(_checkRowSumTransitionMatrices(), "Row of transition matrix does not sum to 1!");
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::finalizeEMParameterEstimationFinal() {
    // get estimate of generator matrix from transition matrices
    if (!_fixTransitionMatricesDuringEM) {
        std::vector<PrecisionType> values = _maximizeQFunction_GeneratingMatrix(true);
        _storeGeneratingMatrix(values);
    }

    // clear EM memory
    _EM_colSums.clear();
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::simulateDistances(const int & length, TRandomGenerator * RandomGenerator){
    // simulate random distances - only if user has not filled them!
    if (!_distances){
        _distances = std::make_shared<TDistancesBinned<uint8_t>>(20);
        // define junk sizes: by default, take size of parameter and split it in two
        std::vector<LengthType > junkSizes;
        if (length % 2 == 0){ // even size
            LengthType len = length/2.;
            junkSizes = {len, len};
        } else { // odd size
            LengthType len = (length-1)/2.;
            junkSizes = {len+1, len};
        }
        _distances->simulate(junkSizes, RandomGenerator);
    }

    _transitionMatrices = std::make_unique<std::vector<TMatrix>>(_distances->numDistanceGroups(), TMatrix(_numStates, _numStates, 0.));
    _transitionMatrices_old = std::make_unique<std::vector<TMatrix>>(_distances->numDistanceGroups(), TMatrix(_numStates, _numStates, 0.));
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
NumStatesType TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::sampleNextState(const LengthType & Index, const NumStatesType & State, TRandomGenerator & RandomGenerator) const{
    size_t distGroup = _distances->operator[](Index);
    TMatrix transitionMatrix = operator[](distGroup);

    double random = RandomGenerator.getRand();
    NumStatesType s = 0;
    double cumul = transitionMatrix(State, s);

    while(cumul <= random){
        ++s;
        cumul += transitionMatrix(State, s);
    }

    return s;
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>::_printTransitionMatrices(){
    for (size_t d = 0; d < _distances->numDistanceGroups(); d++){
        std::cout << "transition matrix for distGroup " << (int) d << ": " << std::endl;
        (*_transitionMatrices)[d].print();
    }
}

//-------------------------------------------
// TTransitionMatrixExponentialNelderMead
//-------------------------------------------

template <typename PrecisionType, typename NumStatesType, typename LengthType>
TTransitionMatrixExponentialNelderMead<PrecisionType,NumStatesType,LengthType>::TTransitionMatrixExponentialNelderMead() : TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>() {
    _totalNumberOfFunctionEvaluationsNM = 0;
    _runNelderMeadInEachEMIteration = true;
    _initLogDisplacement = 0.;
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
TTransitionMatrixExponentialNelderMead<PrecisionType,NumStatesType,LengthType>::TTransitionMatrixExponentialNelderMead(NumStatesType numStates, const std::shared_ptr<TDistancesBinnedBase> &distances)
        : TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>(numStates, distances) {
    _totalNumberOfFunctionEvaluationsNM = 0;
    _runNelderMeadInEachEMIteration = true;
    _initLogDisplacement = 0.;
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialNelderMead<PrecisionType,NumStatesType,LengthType>::setInitialValuesNelderMead(const std::vector<PrecisionType> &InitVertex, const PrecisionType &InitDisplacement) {
    assert(!InitVertex.empty());

    // transform initial estimate to log
    _initLogVertex.resize(InitVertex.size());
    for (size_t l = 0; l < InitVertex.size(); l++) {
        _initLogVertex[l] = log(InitVertex[l]);
    }

    // set displacement
    _initLogDisplacement = log(InitDisplacement);
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
std::vector<PrecisionType> TTransitionMatrixExponentialNelderMead<PrecisionType,NumStatesType,LengthType>::_maximizeQFunction_GeneratingMatrix(const bool & LastIteration) {
    if (LastIteration){
        // don't adjust tolerance
        std::vector<PrecisionType> result = _runNelderMead(false, true); // don't adjust tolerance, we want to be precise now
        _logfile->list("Ran a total of " + toString(_totalNumberOfFunctionEvaluationsNM) + " Nelder-Mead function evaluations over the entire EM algorithm.");
        return result;
    } else {
        if (_runNelderMeadInEachEMIteration){
            // adjust tolerance, but don't report
            return _runNelderMead(true, false);
        } else {
            for (size_t d = 1; d < _distances->numDistanceGroups(); d++) {
                TMatrix &transitionMatrixOld = (*_transitionMatrices_old)[d];
                TMatrix &transitionMatrix = (*_transitionMatrices)[d];
                for (NumStatesType i = 0; i < _numStates; i++) { // z_{t-1}
                    for (NumStatesType j = 0; j < _numStates; j++) { // z_t
                        transitionMatrix(i, j) = transitionMatrixOld(i, j) / _EM_colSums[d][i];
                    }
                }
            }
            return {};
        }
    }
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
TVertex TTransitionMatrixExponentialNelderMead<PrecisionType,NumStatesType,LengthType>::_runNelderMead_FirstEMIteration(){
    // run Nelder-Mead!
    TNelderMead nelderMead;
    // set low tolerance and maxNumFunctionEvaluations, as this is the first iteration and EM will change values in next iteration anyways
    nelderMead.setFractionalConvergenceTolerance(10e-5);
    nelderMead.setMaxNumFunctionEvaluations(1000);
    auto ptr = &TTransitionMatrixExponentialNelderMead::_calcQTransitionMatrix_NelderMead;
    TVertex logPeak = nelderMead.minimize(_initLogVertex, _initLogDisplacement, *this, ptr);

    // store simplex to start next EM iteration with this
    _simplexOfPreviousEMIteration = nelderMead.getCurrentSimplex();
    _totalNumberOfFunctionEvaluationsNM += nelderMead.getCounterFunctionEvaluations();

    return logPeak;
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
TVertex TTransitionMatrixExponentialNelderMead<PrecisionType,NumStatesType,LengthType>::_runNelderMead_OneEMIteration(bool AdjustTolerance, bool Report) {
    // start Nelder-Mead with simplex of previous iteration
    TNelderMead nelderMead;
    if (Report){
        nelderMead.report(_logfile);
    }
    if (AdjustTolerance) {
        nelderMead.dynamicallyAdjustTolerance(_simplexOfPreviousEMIteration[0].value(), 0.001, 10);
    }
    auto ptr = &TTransitionMatrixExponentialNelderMead::_calcQTransitionMatrix_NelderMead;
    TVertex logPeak = nelderMead.minimize(_simplexOfPreviousEMIteration, *this, ptr);

    // store simplex to start next EM iteration with this
    _simplexOfPreviousEMIteration = nelderMead.getCurrentSimplex();
    _totalNumberOfFunctionEvaluationsNM += nelderMead.getCounterFunctionEvaluations();

    return logPeak;
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
std::vector<PrecisionType> TTransitionMatrixExponentialNelderMead<PrecisionType,NumStatesType,LengthType>::_runNelderMead(bool AdjustTolerance, bool Report){
    // maximize the transition-part of the Q-function numerically with Nelder-Mead
    TVertex logPeak;
    if (_firstIteration) {
        logPeak = _runNelderMead_FirstEMIteration();
    } else {
        logPeak = _runNelderMead_OneEMIteration(AdjustTolerance, Report);
    }

    // get rid of log
    std::vector<PrecisionType> peak(logPeak.numDim());
    for (size_t l = 0; l < logPeak.numDim(); l++) {
        peak[l] = exp(logPeak[l]);
    }

    return peak;
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
PrecisionType TTransitionMatrixExponentialNelderMead<PrecisionType,NumStatesType,LengthType>::_calcQTransitionMatrix_NelderMead(const std::vector<PrecisionType> & LogLambdas){
    // calculate sum_{i=1}^N sum_{z_i} sum_{z_{i-1}} xi(z_i, z_{i-1}) log P(z_i|z_{i-1})
    // (relevant term for transition probabilities from the Q-function)

    // take exp of all entries of LogLambdas to get Lambdas
    std::vector<PrecisionType> Lambdas(LogLambdas.size());
    for (size_t l = 0; l < LogLambdas.size(); l++){
        Lambdas[l] = exp(LogLambdas[l]);
    }
    // fill generator matrix and corresponding transition matrices
    this->_fillGeneratingMatrix(Lambdas);
    this->_fillTransitionProbabilities();

    // calculate sum
    double sum = 0.;
    for (size_t d = 1; d < _distances->numDistanceGroups(); d++){ // skip first distance group (= initial probability, estimated separately)
        for (NumStatesType i = 0; i < _numStates; i++) { // z_{t-1}
            for (NumStatesType j = 0; j < _numStates; j++) { // z_t
                sum += (*_transitionMatrices_old)[d](i, j) * log((*_transitionMatrices)[d](i, j));
            }
        }
    }

    return -sum; // return -sum, because we want to maximize Q, but Nelder-Mead minimizes
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialNelderMead<PrecisionType,NumStatesType,LengthType>::_printQSurface(const std::string & Filename){
    int numPoints = 100;

    // Lambda1
    std::vector<PrecisionType> lambda1(numPoints);
    std::vector<PrecisionType> lambda2(numPoints);
    PrecisionType start = 0.0000001;
    for (size_t i = 0; i < numPoints; i++){
        PrecisionType val = log(start + i/50.);
        lambda1[i] = val;
        lambda2[i] = val;
    }

    // calculate Q-function
    std::vector<std::vector<PrecisionType>> values(numPoints, std::vector<PrecisionType>(numPoints, 0.));
    for (size_t i = 0; i < numPoints; i++){
        for (size_t j = 0; j < numPoints; j++){
            values[i][j] = _calcQTransitionMatrix_NelderMead({lambda1[i], lambda2[j]});
        }
    }

    // print to file
    TOutputFile file(Filename, numPoints);
    for (size_t i = 0; i < numPoints; i++){
        file.writeLine(values[i]);
    }
    file.close();
}

//-------------------------------------------
// TTransitionMatrixExponentialNewtonRaphson
//-------------------------------------------

template <typename PrecisionType, typename NumStatesType, typename LengthType>
TTransitionMatrixExponentialNewtonRaphson<PrecisionType,NumStatesType,LengthType>::TTransitionMatrixExponentialNewtonRaphson() : TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>() {
    _initialize();
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
TTransitionMatrixExponentialNewtonRaphson<PrecisionType,NumStatesType,LengthType>::TTransitionMatrixExponentialNewtonRaphson(NumStatesType numStates, const std::shared_ptr<TDistancesBinnedBase> &distances)
        : TTransitionMatrixExponentialBase<PrecisionType,NumStatesType,LengthType>(numStates, distances) {
    _initialize();
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialNewtonRaphson<PrecisionType,NumStatesType,LengthType>::_initialize(){
    _totalNumberOfIterations = 0;
    _previousKappa = 0.;
    _initMin = 0.;
    _initMax = 0.;
    _previousDeltaKappa = 0.;

    _factorMinMaxRange = 10.;
    _factorPrecision = 1000.;
    _initEpsilon = 10e-5;
    _initMaxIterations = 100;
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialNewtonRaphson<PrecisionType,NumStatesType,LengthType>::setInitialValuesNewtonRaphson(const PrecisionType & InitVal, const PrecisionType & InitMin, const PrecisionType & InitMax){
    // in very first EM iteration: just take some random (or fixed) guess of starting value, minimum and maximum
    _previousKappa = InitVal;
    _initMin = InitMin;
    _initMax = InitMax;
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialNewtonRaphson<PrecisionType,NumStatesType,LengthType>::setFactorMinMaxRange(const double &Factor) {
    // factor to adjust min and max of Newton-Raphson boundaries after each EM-iteration
    _factorMinMaxRange = Factor;
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialNewtonRaphson<PrecisionType,NumStatesType,LengthType>::setFactorPrecision(const double &Factor) {
    // factor for termination of Newton-Raphson: be _factorPrecision times more precise than guess of last EM iteration
    _factorPrecision = Factor;
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialNewtonRaphson<PrecisionType,NumStatesType,LengthType>::setInitEpsilon(const double &Epsilon) {
    // epsilon of Newton-Raphson (termination criterion) for very first EM iteration
    _initEpsilon = Epsilon;
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialNewtonRaphson<PrecisionType,NumStatesType,LengthType>::setInitMaxIterations(const size_t &MaxIterations) {
    // maximal number of iterations of Newton-Raphson (termination criterion) for very first EM iteration
    _initMaxIterations = MaxIterations;
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
std::vector<PrecisionType> TTransitionMatrixExponentialNewtonRaphson<PrecisionType,NumStatesType,LengthType>::_maximizeQFunction_GeneratingMatrix(const bool & LastIteration) {
    // maximize the transition-part of the Q-function numerically with Newton-Raphson
    PrecisionType result;
    if (_firstIteration) {
        result = _runNewtonRaphson_FirstEMIteration();
    } else if (LastIteration) {
        // don't adjust tolerance, don't report
        result = _runNewtonRaphson_OneEMIteration(false, false);
        _LambdaA.clear();
        _logfile->list("Ran a total of " + toString(_totalNumberOfIterations) + " Newton-Raphson iterations over the entire EM algorithm.");
    } else {
        // adjust tolerance, don't report
        result = _runNewtonRaphson_OneEMIteration(true, false);
    }

    return {result};
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
PrecisionType TTransitionMatrixExponentialNewtonRaphson<PrecisionType,NumStatesType,LengthType>::_runNewtonRaphson(TNewtonRaphson & NewtonRaphson, const PrecisionType & Start, const PrecisionType & Min, const PrecisionType & Max){
    // _printQSurface("NewtonRaphson_Q_Surface_" + toString(Start) + ".txt");
    auto d1_ptr = &TTransitionMatrixExponentialNewtonRaphson::_calcFirstDerivativeQTransitionMatrix;
    auto d2_ptr = &TTransitionMatrixExponentialNewtonRaphson::_calcSecondDerivativeQTransitionMatrix;
    PrecisionType MLE = NewtonRaphson.runNewtonRaphson_withBisection(*this, d1_ptr, d2_ptr, Start, Min, Max);

    // store total distance that Newton-Raphson has covered (from start to MLE)
    _previousDeltaKappa = std::fabs(Start - MLE);
    // store MLE to start next EM iteration with this
    _previousKappa = MLE;
    _totalNumberOfIterations += NewtonRaphson.getCounterIterations();

    return MLE;
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
PrecisionType TTransitionMatrixExponentialNewtonRaphson<PrecisionType,NumStatesType,LengthType>::_runNewtonRaphson_FirstEMIteration(){
    // prepare temporary storage
    _LambdaA.resize(_distances->numDistanceGroups(), TMatrix(_numStates, _numStates, 0.));

    // set low tolerance and maxNumFunctionEvaluations, as this is the first iteration and EM will change values in next iteration anyways
    TNewtonRaphson newtonRaphson;
    newtonRaphson.setMaxIterations(_initMaxIterations);
    newtonRaphson.setEpsilon(_initEpsilon);
    return _runNewtonRaphson(newtonRaphson, _previousKappa, _initMin, _initMax);
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
PrecisionType TTransitionMatrixExponentialNewtonRaphson<PrecisionType,NumStatesType,LengthType>::_runNewtonRaphson_OneEMIteration(bool AdjustTolerance, bool Report) {
    // start Newton-Raphson with kappa of previous iteration
    TNewtonRaphson newtonRaphson;
    if (Report){
        newtonRaphson.report(_logfile);
    }
    if (AdjustTolerance) {
        newtonRaphson.dynamicallyAdjustTolerance(_factorPrecision);
    }
    PrecisionType min;
    PrecisionType max;
    _fillMinMaxNewtonRaphson(min, max);
    return _runNewtonRaphson(newtonRaphson, _previousKappa, min, max);
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialNewtonRaphson<PrecisionType,NumStatesType,LengthType>::_fillMinMaxNewtonRaphson(PrecisionType & Min, PrecisionType & Max) const{
    // set new min and max for boundaries of Newton-Raphson with
    // previousKappa +- distance that previous EM has covered * some factor (>1)
    Min = _previousKappa - _previousDeltaKappa*_factorMinMaxRange;
    if (Min < _initMin){
        Min = _initMin;
    }
    Max = _previousKappa + _previousDeltaKappa*_factorMinMaxRange;
    if (Max > _initMax){
        Max = _initMax;
    }
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
PrecisionType TTransitionMatrixExponentialNewtonRaphson<PrecisionType,NumStatesType,LengthType>::_calcFirstDerivativeQTransitionMatrix(const PrecisionType & Kappa){
    // calculate:
    // sum_{l=1}^L delta_i sum_{i=1}^J sum_{j=1}^J xi_l(i, j) * (Lambda * A_l)_{ij} / A_l_{ij}

    // fill generator matrix and corresponding transition matrices for current kappa
    this->_fillGeneratingMatrix({Kappa});
    this->_fillTransitionProbabilities();

    // calculate sum
    PrecisionType sum = 0.;
    for (size_t d = 1; d < _distances->numDistanceGroups(); d++){ // skip first distance group (= initial probability, estimated separately)
        TMatrix & A = (*_transitionMatrices)[d];
        TMatrix & xi = (*_transitionMatrices_old)[d];

        // do matrix multiplication of Lambda %*% TransitionMatrix for each distance group
        // and store the result in vector (same is needed for second derivative)
        _LambdaA[d] = _generatingMatrix->getUnparametrizedGeneratingMatrix() * A;

        PrecisionType tmpSum = 0.;
        for (NumStatesType i = 0; i < _numStates; i++) { // z_{t-1}
            for (NumStatesType j = 0; j < _numStates; j++) { // z_t
                // calculate fraction
                double frac = _LambdaA[d](i, j) / A(i, j);
                tmpSum += frac * xi(i, j);
            }
        }
        sum += _distances->distanceGroup(d).min * tmpSum;
    }
    return sum;
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
PrecisionType TTransitionMatrixExponentialNewtonRaphson<PrecisionType,NumStatesType,LengthType>::_calcSecondDerivativeQTransitionMatrix(const PrecisionType & Kappa){
    // assumes that _calcFirstDerivativeQTransitionMatrix has already filled generating matrix, transition probabilities and _LambdaA with new kappa!

    // calculate sum
    PrecisionType sum = 0.;
    for (size_t d = 1; d < _distances->numDistanceGroups(); d++){ // skip first distance group (= initial probability, estimated separately)
        TMatrix & A = (*_transitionMatrices)[d];
        TMatrix & xi = (*_transitionMatrices_old)[d];
        TMatrix & LambdaA = _LambdaA[d];

        // do matrix multiplication of Lambda %*% Lambda %*% TransitionMatrix for each distance group
        TMatrix LambdaSquaredA = _generatingMatrix->getUnparametrizedGeneratingMatrix() * LambdaA;

        PrecisionType tmpSum = 0.;
        for (NumStatesType i = 0; i < _numStates; i++) { // z_{t-1}
            for (NumStatesType j = 0; j < _numStates; j++) { // z_t
                // calculate fraction
                double & lambdaA_ij = LambdaA(i, j);
                double & a_ij = A(i, j);
                double frac = (LambdaSquaredA(i, j) * A(i, j) - lambdaA_ij*lambdaA_ij) / (a_ij*a_ij);
                tmpSum += frac * xi(i, j);
            }
        }
        sum += _distances->distanceGroup(d).min * tmpSum;
    }
    return sum;
}

template <typename PrecisionType, typename NumStatesType, typename LengthType>
void TTransitionMatrixExponentialNewtonRaphson<PrecisionType,NumStatesType,LengthType>::_printQSurface(const std::string & Filename){
    int numPoints = 1000;

    // kappa
    std::vector<PrecisionType> kappa(numPoints);
    PrecisionType start = 0.0000001;
    for (size_t i = 0; i < numPoints; i++){
        kappa[i] = start + i/5.;
    }

    // calculate Q-function
    std::vector<PrecisionType> d1(numPoints, 0.);
    std::vector<PrecisionType> d2(numPoints, 0.);
    for (size_t i = 0; i < numPoints; i++){
        d1[i] = _calcFirstDerivativeQTransitionMatrix(kappa[i]);
        d2[i] = _calcSecondDerivativeQTransitionMatrix(kappa[i]);
    }

    // print to file
    TOutputFile file(Filename);
    std::vector<std::string> header = {"kappa", "d1", "d2"};
    file.writeHeader(header);

    for (size_t i = 0; i < numPoints; i++){
        file << kappa[i] << d1[i] << d2[i] << std::endl;
    }

    file.close();
}
