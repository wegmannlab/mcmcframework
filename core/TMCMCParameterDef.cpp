//
// Created by madleina on 11.12.20.
//

#include "TMCMCParameterDef.h"

//--------------------------------------------
// TDefinitionBase
//--------------------------------------------

TDefinitionBase::TDefinitionBase(std::string Name){
    _name = std::move(Name);
    _isObservation = false;
    _prior = MCMCPrior::uniform;
    _priorParametersAreFix = true;
    _type = "double";
    _trackExponential = false;
    _trackLog = false;
    _min = toString(std::numeric_limits<double>::lowest());
    _max = toString(std::numeric_limits<double>::max());
    _min_included = true;
    _max_included = true;
    _hasDefaultMin = true;
    _hasDefaultMax = true;
    _dimensions = {1};
    _hasDefaultDimensions = true;
    _simulationFile = "-";
}

TDefinitionBase::~TDefinitionBase() = default;

std::string TDefinitionBase::type() const{
    return _type;
}

std::string TDefinitionBase::name() const{
    return _name;
}

bool TDefinitionBase::isObserved() const {
    return _isObservation;
}

void TDefinitionBase::reSetObserved(const std::string& observed) {
    bool isObserved = convertString<bool>(observed);
    if (isObserved != _isObservation){
        // changed parameter to observation or vice-versa -> not allowed, throw
        throw "Error when building parameter '" + _name + "': Can not change parameter to observation or vice-versa!";
    }
}

void TDefinitionBase::setType(const std::string & type) {
    _type = type;
}

void TDefinitionBase::setDimensions(const std::vector<size_t> &dimensions) {
    _hasDefaultDimensions = false;
    _dimensions = dimensions;
}

void TDefinitionBase::reSetPrior(const std::string& Prior) {
    // can reSet the prior if prior parameters are fixed numbers (not inferred)
    if (!priorParametersAreFix() && Prior != _prior)
        throw "Prior can not be re-set for definition '" + _name + " Can only re-set priors with fixed prior parameters, but " + _parameters + " are not fix (not numbers)!";

    setPrior(Prior);
}

void TDefinitionBase::setPrior(std::string Prior) {
    trimString(Prior);
    _prior = Prior;
}

std::string TDefinitionBase::prior() const{
    return _prior;
}

void TDefinitionBase::setPriorParams(std::string Params) {
    _parameters = std::move(Params);

    // check if valid
    // prior parameters can be...
    // 1) empty
    // 2) a comma-separated string of 1 or more numbers
    // 3) a comma-separated string of 1 or more words (= other parameters)
    // but not a mix!
    if (_parameters.empty()){
        _priorParametersAreFix = true;
    }
    std::vector<std::string> temp;
    fillContainerFromStringAny(_parameters, temp, ",", true);
    if (std::all_of(temp.begin(), temp.end(), stringIsProbablyANumber)) { // all are numbers
        _priorParametersAreFix = true;
    } else if (std::any_of(temp.begin(), temp.end(), stringIsProbablyANumber)){ // mix of numbers and words
        throw "Invalid prior parameters (" + _parameters + ") for definition " + _name + ": Prior parameters can either be a vector of numbers or a vector of words, but not a mix.";
    } else {
        _priorParametersAreFix = false;
    }
}

void TDefinitionBase::reSetPriorParams(std::string Params) {
    // can reSet the prior parameters if prior parameters are fixed numbers (not inferred)
    if (!priorParametersAreFix() && Params != _parameters)
        throw "Prior parameters can not be re-set for definition '" + _name + " Can only re-set priors with fixed prior parameters, but " + _parameters + " are not fix (not numbers)!";

    setPriorParams(std::move(Params));
}

std::string TDefinitionBase::parameters() const {
    return _parameters;
}

bool TDefinitionBase::priorParametersAreFix() const {
    return _priorParametersAreFix;
}

void TDefinitionBase::trackExponential(bool Track) {
    _trackExponential = Track;
}

bool TDefinitionBase::tracksExponential() const {
    return _trackExponential;
}

void TDefinitionBase::trackLog(bool Track) {
    _trackLog = Track;
}

bool TDefinitionBase::tracksLog() const {
    return _trackLog;
}

void TDefinitionBase::setMin(std::string Min, bool MinIsIncluded){
    _min = Min;
    _hasDefaultMin = false;
    _min_included = MinIsIncluded;
}

void TDefinitionBase::setMin(std::string Min){
    _min = Min;
    _hasDefaultMin = false;
}

void TDefinitionBase::setMinIncluded(bool MinIsIncluded){
    _hasDefaultMin = false;
    _min_included = MinIsIncluded;
}

void TDefinitionBase::setMax(std::string Max, bool MaxIsIncluded){
    _max = Max;
    _hasDefaultMax = false;
    _max_included = MaxIsIncluded;
}

void TDefinitionBase::setMax(std::string Max){
    _max = Max;
    _hasDefaultMax = false;
}

void TDefinitionBase::setMaxIncluded(bool MaxIsIncluded){
    _hasDefaultMax = false;
    _max_included = MaxIsIncluded;
}

bool TDefinitionBase::hasDefaultMin() const{
    return _hasDefaultMin;
}

bool TDefinitionBase::hasDefaultMax() const{
    return _hasDefaultMax;
}

std::string TDefinitionBase::min() const{
    return _min;
}

std::string TDefinitionBase::max() const{
    return _max;
}

bool TDefinitionBase::minIncluded() const{
    return _min_included;
}

bool TDefinitionBase::maxIncluded() const{
    return _max_included;
}

std::vector<size_t> TDefinitionBase::dimensions() const {
    return _dimensions;
}

bool TDefinitionBase::hasDefaultDimensions() const {
    return _hasDefaultDimensions;
}

size_t TDefinitionBase::totalSize() const {
    return std::accumulate(std::begin(_dimensions), std::end(_dimensions), 1, std::multiplies<>());
}

void TDefinitionBase::setSimulationFile(const std::string &Filename) {
    _simulationFile = Filename;
}

std::string TDefinitionBase::simulationFile() const {
    return _simulationFile;
}

//--------------------------------------------
// TParameterDefinition
//--------------------------------------------

TParameterDefinition::TParameterDefinition(std::string Name) : TDefinitionBase(Name){
    _isObservation = false;
    _isUpdated = true;
    _initVal = "0";
    _hasDefaultInitVal = true;
    _storeMeanVar = false;
    _meanVarFile = "-";
    _traceFile = "-";
    _initPropKernel = "0.1";
    _propKernelDistr = ProposalKernel::normal;
    _oneJumpSizeForAll = true;
}

TParameterDefinition::~TParameterDefinition() = default;

void TParameterDefinition::update(bool IsUpdated) {
    _isUpdated = IsUpdated;
}

bool TParameterDefinition::isUpdated() const {
    return _isUpdated;
}

void TParameterDefinition::setMeanVarFile(const std::string & filename){
    _meanVarFile = filename;
    _storeMeanVar = true;
}

bool TParameterDefinition::storeMeanVar() const{
    return _storeMeanVar;
}

std::string TParameterDefinition::meanVarFile() const{
    return  _meanVarFile;
}

void TParameterDefinition::setTraceFile(const std::string &filename) {
    _traceFile = filename;
}

std::string TParameterDefinition::traceFile() const{
    return _traceFile;
}

void TParameterDefinition::setJumpSizeForAll(bool oneJumpSizeForAll){
    _oneJumpSizeForAll = oneJumpSizeForAll;
}

void TParameterDefinition::setInitJumpSizeProposal(const std::string & init){
    _initPropKernel = init;
}

void TParameterDefinition::setPropKernel(std::string distr){
    trimString(distr);
    _propKernelDistr = ProposalKernel::stringToProposalKernel(distr);
}

void TParameterDefinition::setPropKernel(const ProposalKernel::MCMCProposalKernel & distr){
    _propKernelDistr = distr;
}

std::string TParameterDefinition::initJumpSizeProposal() const{
    return _initPropKernel;
}

ProposalKernel::MCMCProposalKernel TParameterDefinition::propKernel() const{
    return _propKernelDistr;
}

void TParameterDefinition::setInitVal(std::string value) {
    _initVal = std::move(value);
    _hasDefaultInitVal = false;
}

std::string TParameterDefinition::initVal() const{
    return _initVal;
}

bool TParameterDefinition::hasDefaultInitVal() const {
    return _hasDefaultInitVal;
}

bool TParameterDefinition::oneJumpSizeForAll() const{
    return _oneJumpSizeForAll;
}

//--------------------------------------------
// TObservationDefinition
//--------------------------------------------

TObservationDefinition::TObservationDefinition(std::string Name) : TDefinitionBase(Name){
    _isObservation = true;

    _translator = Translator::phred;
    _translate = false;
    _maxStoredValue = std::numeric_limits<double>::max();
    _storedType = _type;
}

TObservationDefinition::~TObservationDefinition() = default;

void TObservationDefinition::translate(const Translator::Translator &Translator, const std::string &StoredType, const double & MaxStoredValue) {
    _translate = true;
    _translator = Translator;
    _storedType = StoredType;
    _maxStoredValue = MaxStoredValue;
}

bool TObservationDefinition::isTranslated() const{
    return _translate;
}

Translator::Translator TObservationDefinition::translator() const{
    if (!_translate){
        throw std::runtime_error("In function 'Translator::Translator TObservationDefinition::translator() const': Observation " + _name + " is not translated - can not return translator!");
    }
    return _translator;
}

std::string TObservationDefinition::storedType() const {
    return _storedType;
}

double TObservationDefinition::maxStoredValue() const {
    return _maxStoredValue;
}