//
// Created by madleina on 30.11.20.
//

//-------------------------------------------
// TPriorNormalMixedModelWithHyperPrior
//-------------------------------------------

template <typename T> TPriorNormalMixedModelWithHyperPrior<T>::TPriorNormalMixedModelWithHyperPrior() : TPriorWithHyperPrior<T>(),
                                                                                                        TLatentVariable<double, size_t, size_t>() {
    this->_name = MCMCPrior::normal_mixedModel;
    _oneDivTwo_log2Pi = 0.5 * 1.83787706640935; // log(2*pi) = 1.83787706640935

    _EM_update_temp = nullptr;
}

template <typename T> void TPriorNormalMixedModelWithHyperPrior<T>::initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, std::vector<std::shared_ptr<TObservationsBase> > & observations){
    // requires at least 3 parameters: mus (K times), vars (K times), z (in this order!)
    if (params.size() < 3){
        throw std::runtime_error("In function 'template <typename T> void TPriorNormalMixedModelWithHyperPrior<T>::initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params)': Size of TMCMCParameter vector (" + toString(params.size()) + ") does not match the number of parameters of a " + this->_name + " distribution (at least 3: mus, vars, z)!");
    }
    this->_checkSizeObservations(observations.size(), 0);

    // calculate K = (params.size - 1)/2 where -1 for z, and the rest should be half mu and half var -> /2
    double tmp = (params.size() - 1.)/2.;
    _K = static_cast<size_t>(tmp);
    if (static_cast<double>(_K) != tmp){
        throw std::runtime_error("In function 'template <typename T> void TPriorNormalMixedModelWithHyperPrior<T>::initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params)': Provided wrong number of parameters to function! Guessed K = (params.size() - 1) / 2 = " + toString(tmp) + ", which is not an integer.");
    }

    // now fill parameter vectors and check if size is ok
    for (size_t k = 0; k < _K; k++){
        _mus.push_back(params[k]);
    }
    for (size_t k = _K; k < _K + _K; k++){
        _vars.push_back(params[k]);
    }
    _z = params.back();

    _checkMinMaxOfHyperPrior();
}

template <class T> void TPriorNormalMixedModelWithHyperPrior<T>::initializeStorageOfPriorParameters() {
    // derive N and dimNames from dimensionality of parameters below
    std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(this->_parameters[0]);
    size_t N = paramToUse->totalSize();
    const std::shared_ptr<TNamesEmpty> dimNames = paramToUse->getDimensionName(0);

    for (auto & param : this->_parameters) {
        paramToUse = this->_getSharedPtrParam(param);
        // check if N is the same
        if (paramToUse->totalSize() != N){
            throw std::runtime_error("In function 'template <typename T> void TPriorNormalMixedModelWithHyperPrior<T>::initializeStorageOfPriorParameters()': Total size of parameter " + paramToUse->name() + " (=" + toString(paramToUse->totalSize()) + ") and of other shared parameter (=" + toString(N) + ") do not match!");
        }
        // check if ptr to dimension names is the same
        if (paramToUse->getDimensionName(0) != dimNames){
            throw std::runtime_error("In function 'template <typename T> void TPriorNormalMixedModelWithHyperPrior<T>::initializeStorageOfPriorParameters()': Pointer to dimension name class of parameter " + paramToUse->name() + " differs from that of other shared parameter!");
        }
    }

    // go over all parameters and set size
    for (auto & mu : _mus){
        mu->initializeStorageSingleElementBasedOnPrior();
    }
    for (auto & var : _vars){
        var->initializeStorageSingleElementBasedOnPrior();
    }
    _z->initializeStorageBasedOnPrior({N}, {dimNames});

    _updateTempVals();
}

template <typename T> void TPriorNormalMixedModelWithHyperPrior<T>::_updateTempVals(){}

template <typename T> void TPriorNormalMixedModelWithHyperPrior<T>::_checkMinMaxOfHyperPrior(){
    // idea: parameter _vars should always be > 0 -> re-set boundaries of _vars
    for (auto &v : _vars){
        v->reSetBoundaries(false, true, "0.", toString(std::numeric_limits<double>::max()), false, true);
    }
    // _z must be between 0 and (K-1)
    _z->reSetBoundaries(false, false, "0", toString(_K-1), true, true);
}

template <class T> void TPriorNormalMixedModelWithHyperPrior<T>::constructDAG(TDAG & DAG, TDAG & temporaryDAG){
    // construct vector of prior parameters
    std::vector<std::shared_ptr<TMCMCObservationsBase>> params;
    for (auto &v : _mus){
        params.push_back(v);
    }
    for (auto &v : _vars){
        params.push_back(v);
    }
    params.push_back(_z);

    this->_constructDAG(DAG, temporaryDAG, params);
}

template <class T> void TPriorNormalMixedModelWithHyperPrior<T>::typesAreCompatible(bool throwIfNot) {
    // parameters on which normal mixed prior is defined must be floating point type
    this->_checkTypes(throwIfNot, true, false, false);

    // in addition: check if prior parameters have expected type
    // all _mus and _vars must be floating point
    for (auto &v : _vars){
        this->_checkTypesOfPriorParameters(v, throwIfNot, true, false, false);
    }
    for (auto &v : _mus){
        this->_checkTypesOfPriorParameters(v, throwIfNot, true, false, false);
    }
    // _z must be unsigned integer
    this->_checkTypesOfPriorParameters(_z, throwIfNot, false, true, true);

    // check if the prior distribution on the prior parameters is ok
    for (auto &v : _vars){
        v->typesAreCompatible(throwIfNot);
    }
    for (auto &v : _mus){
        v->typesAreCompatible(throwIfNot);
    }
    _z->typesAreCompatible(throwIfNot);
}

template <typename T> double TPriorNormalMixedModelWithHyperPrior<T>::_getLogPriorDensity_oneVal(T x){
    throw std::runtime_error("In function 'template <typename T> double TPriorNormalMixedModelWithHyperPrior<T>::_getLogPriorDensity_oneVal(T x)': Need to know index of parameter that has changed -> do not call function _getLogPriorDensity_oneVal(T x) with single elements!");
}

template <typename T> double TPriorNormalMixedModelWithHyperPrior<T>::_getLogPriorRatio_oneVal(T x, T x_old){
    throw std::runtime_error("In function 'template <typename T> double TPriorNormalMixedModelWithHyperPrior<T>::_getLogPriorRatio_oneVal(T x, T x_old)': Need to know index of parameter that has changed -> do not call function _getLogPriorDensity_oneVal(T x) with single elements!");
}

template <typename T> double TPriorNormalMixedModelWithHyperPrior<T>::_getLogPriorDensity(const T & x, const size_t & k){
    double tmp = x - _mus[k]->value<double>();
    return -0.5*log(_vars[k]->value<double>()) - _oneDivTwo_log2Pi - (tmp * tmp)/(2.*_vars[k]->value<double>());
}

template <typename T> double TPriorNormalMixedModelWithHyperPrior<T>::_getLogPriorDensity_vec(const std::shared_ptr<TMCMCObservationsBase> & data, const size_t & index){
    // note: expensive to calculate, call getLogPriorRatio() if ever possible
    size_t k = _z->value<size_t>(index);
    return _getLogPriorDensity(data->value<T>(index), k);
}

template <typename T> double TPriorNormalMixedModelWithHyperPrior<T>::_getLogPriorDensityOld_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index){
    // note: expensive to calculate, call getLogPriorRatio() if ever possible
    size_t k = _z->value<size_t>(index);
    return _getLogPriorDensity(data->oldValue<T>(index), k);
}

template <typename T> double TPriorNormalMixedModelWithHyperPrior<T>::_getLogPriorRatio_vec(const std::shared_ptr<TMCMCParameterBase> &data, const size_t &index) {
    size_t k = _z->value<size_t>(index);
    double tmp = data->value<T>(index) - _mus[k]->value<double>();
    double tmp_old = data->oldValue<T>(index) - _mus[k]->value<double>();
    return (tmp_old * tmp_old - tmp * tmp) / (2.*_vars[k]->value<double>());
}

template <typename T> void TPriorNormalMixedModelWithHyperPrior<T>::updateParams(){
    // update mus, vars and z (done with range-based for loop, because derived class can then inherit (has only 1 mu, not K))
    size_t k = 0;
    for (auto & mu : _mus){
        if (mu->update()){
            _updateMean(k);
            _updateTempVals();
        }
        k++;
    }
    k = 0;
    for (auto & var : _vars){
        if (var->update()){
            _updateVar(k);
            _updateTempVals(); // not important in this class, but it is important in derived classes
        }
        k++;
    }
    TRange range = _z->getFull();
    for (size_t i = range.first; i < range.last; i += range.increment){
        if (_z->update(i)){
            _updateZ(i);
        }
    }
}

template <typename T> double TPriorNormalMixedModelWithHyperPrior<T>::_calcLLUpdateMean(const size_t & k){
    // compute log likelihood
    double LL = 0.;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange range = paramToUse->getFull();
        double tmp = 0.;
        for (size_t i = range.first; i < range.last; i += range.increment) {
            if (_z->value<size_t>(i) == k){
                double tmpOld = paramToUse->value<T>(i) - _mus[k]->oldValue<double>();
                double tmpNew = paramToUse->value<T>(i) - _mus[k]->value<double>();
                tmp += tmpOld * tmpOld - tmpNew * tmpNew;
            }
        }
        LL += tmp/(2*_vars[k]->value<double>());
    }
    return LL;
}

template <typename T> double TPriorNormalMixedModelWithHyperPrior<T>::_calcLogHUpdateMean(const size_t & k){
    // compute log likelihood
    double LL = _calcLLUpdateMean(k);
    // compute prior
    double logPrior = _mus[k]->getLogPriorRatio();
    return LL + logPrior;
}

template <typename T> void TPriorNormalMixedModelWithHyperPrior<T>::_updateMean(const size_t & k){
    // compute log(Hastings ratio)
    double logH = _calcLogHUpdateMean(k);
    // accept or reject
    _mus[k]->acceptOrReject(logH);
}

template <typename T> double TPriorNormalMixedModelWithHyperPrior<T>::_calcLLUpdateVar(const size_t & k) {
    // compute log likelihood
    double LL = 0.;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange range = paramToUse->getFull();
        double sum = 0.;
        size_t counter = 0;
        for (size_t i = range.first; i < range.last; i += range.increment) {
            if (_z->value<size_t>(i) == k) {
                double tmp = paramToUse->value<T>(i) - _mus[k]->value<double>();
                sum += tmp * tmp;
                counter++;
            }
        }

        LL += 0.5 * (counter * log(_vars[k]->oldValue<double>() / _vars[k]->value<double>()) +
                (1./_vars[k]->oldValue<double>() - 1./_vars[k]->value<double>()) * sum);
    }
    return LL;
}

template <typename T> double TPriorNormalMixedModelWithHyperPrior<T>::_calcLogHUpdateVar(const size_t & k) {
    // compute LL
    double LL = _calcLLUpdateVar(k);
    // compute prior
    double logPrior = _vars[k]->getLogPriorRatio();
    // compute log(Hastings ratio)
    return LL + logPrior;
}

template <typename T> void TPriorNormalMixedModelWithHyperPrior<T>::_updateVar(const size_t & k){
    // compute log(Hastings ratio)
    double logH = _calcLogHUpdateVar(k);
    // accept or reject
    _vars[k]->acceptOrReject(logH);
}

template <typename T> double TPriorNormalMixedModelWithHyperPrior<T>::_calcLLUpdateZ(const size_t &i) {
    // compute log likelihood
    double LL = 0.;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        double PZ_old = _getLogPriorDensity(paramToUse->value<T>(i), _z->oldValue<size_t>(i));
        double PZ_new = _getLogPriorDensity(paramToUse->value<T>(i), _z->value<size_t>(i));
        LL += PZ_new - PZ_old;
    }
    return LL;
}

template <typename T> double TPriorNormalMixedModelWithHyperPrior<T>::_calcLogHUpdateZ(const size_t &i) {
    // compute LL
    double LL = _calcLLUpdateZ(i);
    // compute prior
    double logPrior = _z->getLogPriorRatio(i);
    // compute log(Hastings ratio)
    return LL + logPrior;
}

template <typename T> void TPriorNormalMixedModelWithHyperPrior<T>::_updateZ(const size_t &i) {
    // compute log(Hastings ratio)
    double logH = _calcLogHUpdateZ(i);
    // accept or reject
    _z->acceptOrReject(i, logH);
}

template <class T> double TPriorNormalMixedModelWithHyperPrior<T>::getSumLogPriorDensity(const std::shared_ptr<TMCMCObservationsBase> & data) {
    double sum = 0.;
    TRange range = data->getFull();
    for (size_t n = range.first; n < range.last; n+=range.increment) {
        sum += _getLogPriorDensity_vec(data, n);
    }
    return sum;
}

template <class T> double TPriorNormalMixedModelWithHyperPrior<T>::getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data) {
    // returns sum of log-density of array + log density of mean + log density of var + log density of mean1 + log density of var1 + log density of z
    double tmp = 0.;
    for (auto & mu : _mus){
        tmp += mu->getLogPriorDensity();
    }
    for (auto & var : _vars){
        tmp += var->getLogPriorDensity();
    }
    return getSumLogPriorDensity(data) + tmp + _z->getLogPriorDensityFull();
}

template <class T> void TPriorNormalMixedModelWithHyperPrior<T>::simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) {
    // first update temporary values: might have changed in the meantime
    _updateTempVals();

    // now sample random normal values for these means and vars
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        if (!paramToUse->hasFixedInitialValue()) { // only simulate if parameter has not a fixed value
            TRange range = paramToUse->getFull();
            for (size_t i = range.first; i < range.last; i += range.increment) {
                size_t k = _z->value<size_t>(i);
                double value = RandomGenerator->getNormalRandom(_mus[k]->value<double>(),
                                                                sqrt(_vars[k]->value<double>()));
                while (!paramToUse->valueIsInsideBoundary(value)) { // propose again if value is too large, to prevent overshooting while mirroring
                    value = RandomGenerator->getNormalRandom(_mus[k]->value<double>(), sqrt(_vars[k]->value<double>()));
                }
                // set value
                paramToUse->set(i, value);
            }
        }
    }
}

template <class T> void TPriorNormalMixedModelWithHyperPrior<T>::estimateInitialPriorParameters(TLog * Logfile) {
    if (!_z->hasFixedInitialValue()) { // latent variable not known -> run EM
        _z->runEMEstimation(*this, Logfile);
    } else {
        _setInitialMeanAndVar();
    }
}

template <class T> double TPriorNormalMixedModelWithHyperPrior<T>::_dist(const T & x, const T & y) {
    // calculate Euclidean distance
    return (std::fabs(x - y));
}

// EM
template <class T> void TPriorNormalMixedModelWithHyperPrior<T>::_fillMeanParameterValues(std::vector<double> & meanParamValues) {
    // fill a vector with the mean values of all parameters
    meanParamValues.resize(_z->totalSize(), 0.);

    for (auto & param : this->_parameters) {

        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange range = paramToUse->getFull();
        for (size_t i = range.first; i < range.last; i += range.increment) {
            meanParamValues[i] += paramToUse->value<T>(i);
        }
    }

    for (auto & it : meanParamValues){
        it /= this->_parameters.size();
    }
}

template <class T> void TPriorNormalMixedModelWithHyperPrior<T>::_findMostDistantPoints(std::vector<T> & minValues,
                                                                                        const std::vector<double> & meanParameterValues) {
    std::vector<double> minDistances(_z->totalSize());
    minValues.resize(_K, 0);

    if (_mus[0]->hasFixedInitialValue()) {
        minValues[0] = minValues[0];
    } else{
        // 1st value = 1st group
        minValues[0] = meanParameterValues[0];
    }

    for (size_t k = 1; k < _K; k++){
        for (size_t i = 0; i < _z->totalSize(); i++){
            // find minimal distance among all previous k
            double minDist = _dist(meanParameterValues[i], minValues[0]);
            for (size_t smallerK = 1; smallerK < k; smallerK++) {
                double tmp = _dist(meanParameterValues[i], minValues[smallerK]);
                if (tmp < minDist) {
                    minDist = tmp;
                }
            }
            minDistances[i] = minDist;
        }

        // assign the data point that maximizes minDistances to the next K
        size_t maxIndex =  std::distance(minDistances.begin(), std::max_element(minDistances.begin(), minDistances.end()));
        if (_mus[k]->hasFixedInitialValue()) {
            minValues[k] = minValues[k];
        } else {
            minValues[k] = meanParameterValues[maxIndex];
        }
    }
}

template <class T> void TPriorNormalMixedModelWithHyperPrior<T>::_setInitialZ(std::vector<T> & minValues,
                                                                              const std::vector<double> & meanParameterValues) {
    if (!_z->hasFixedInitialValue()) {
        // go over the data again and assign the closest group to each point
        for (size_t i = 0; i < _z->totalSize(); i++) {
            size_t minK = 0;
            for (size_t k = 1; k < _K; k++) {
                if (_dist(meanParameterValues[i], minValues[k]) < _dist(meanParameterValues[i], minValues[minK])) {
                    minK = k;
                }
            }
            _z->set(i, minK);
        }
    }
}

template <class T> void TPriorNormalMixedModelWithHyperPrior<T>::_setInitialMeanAndVar() {
    std::vector<TMeanVar<double>> meanVars(_K);
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange range = paramToUse->getFull();
        for (size_t i = range.first; i < range.last; i += range.increment) {
            size_t k = _z->value<size_t>(i);
            meanVars[k].add(paramToUse->value<T>(i));
        }
    }

    for (size_t k = 0; k < _K; k++){
        if (!_mus[k]->hasFixedInitialValue()) {
            _mus[k]->set(meanVars[k].mean());
        }
        if (!_vars[k]->hasFixedInitialValue()) {
            _vars[k]->set(meanVars[k].variance());
        }
    }
}

template <class T> void TPriorNormalMixedModelWithHyperPrior<T>::initializeEMParameters() {
    // calculate the mean value per entry (over all parameters)
    std::vector<double> meanParameterValues;
    _fillMeanParameterValues(meanParameterValues);

    // find points that are most far apart
    std::vector<T> minValues;
    _findMostDistantPoints(minValues, meanParameterValues);

    // go over the data again and assign the closest group to each point
    _setInitialZ(minValues, meanParameterValues);

    // calculate mean and variance according to this classification
    _setInitialMeanAndVar();
}

template<class T> size_t TPriorNormalMixedModelWithHyperPrior<T>::getHiddenState(const size_t &Index) const {
    return _z->value<size_t>(Index);
}

template <class T> void TPriorNormalMixedModelWithHyperPrior<T>::calculateEmissionProbabilities(const size_t & Index, TDataVector<double, size_t> & Emission){
    // initialize emissions with zero
    for (size_t k = 0; k < _K; k++) {
        Emission[k] = 0.;
    }

    // go over all parameters and over all components and sum emission probability
    // we want emission not in log, but we will use the log to sum and then de-log in the end
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        // log P(x | z = k, theta)
        for (size_t k = 0; k < _K; k++) {
            Emission[k] += TNormalDistr::density(paramToUse->value<double>(Index), _mus[k]->value<double>(), sqrt(_vars[k]->value<double>()), true);
        }
    }

    // get rid of log
    for (size_t k = 0; k < _K; k++) {
        Emission[k] = exp(Emission[k]);
    }
};

template <class T> void TPriorNormalMixedModelWithHyperPrior<T>::prepareEMParameterEstimationInitial() {
    // initialize data: per component, we need to store three sums
    // 1) sum_{i=1}^N weights_ki * sum_{p=1}^P x_pi
    // 2) sum_{i=1}^N weights_ki * sum_{p=1}^P (x_pi - mu_k)^2
    // 3) sum_{i=1}^N weights_ki
    _EM_update_temp = new double[3*_K];
}

template <class T> void TPriorNormalMixedModelWithHyperPrior<T>::prepareEMParameterEstimationOneIteration(){
    // fill data with zeros
    for (size_t i = 0; i < 3*_K; i++){
        _EM_update_temp[i] = 0.;
    }
};

template <class T> void TPriorNormalMixedModelWithHyperPrior<T>::handleEMParameterEstimationOneIteration(const size_t & index, const TDataVector<double, size_t> & Weights){
    // calculate
    // 1) sum_{i=1}^N sum_{p=1}^P x_pi
    // 2) sum_{i=1}^N sum_{p=1}^P (x_pi - mu_k)^2
    double sumXpi = 0.;
    std::vector<double> sumXpiMinusMuSquare(_K, 0.);
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        sumXpi += paramToUse->value<double>(index);
        for (size_t k = 0; k < _K; k++) {
            double tmp = paramToUse->value<double>(index) - _mus[k]->value<double>();
            sumXpiMinusMuSquare[k] += tmp*tmp;
        }
    }

    // add to sums
    for (size_t k = 0; k < _K; k++) {
        // add to sum x_i * weights_ki
        _EM_update_temp[k*3] += sumXpi * Weights[k];
        // add to sum (x_i - mu_k)^2 * weights_ki
        _EM_update_temp[k*3+1] += sumXpiMinusMuSquare[k] * Weights[k];
        // add to sum weights
        _EM_update_temp[k*3+2] += Weights[k];
    }
}

template <class T> void TPriorNormalMixedModelWithHyperPrior<T>::finalizeEMParameterEstimationOneIteration(){
    // M-step: update parameters
    for (size_t k = 0; k < _K; k++){
        if (!_mus[k]->hasFixedInitialValue()) {
            _mus[k]->set(_EM_update_temp[k*3] / (this->_parameters.size() * _EM_update_temp[k*3+2]));
        }
        if (!_vars[k]->hasFixedInitialValue()) {
            double newValue = _EM_update_temp[k*3+1] / (this->_parameters.size() * _EM_update_temp[k*3+2]);
            if (newValue == 0.){
                newValue = 0.0000001; // avoid var = 0
            }
            _vars[k]->set(newValue);
        }
    }
}

template <class T> void TPriorNormalMixedModelWithHyperPrior<T>::finalizeEMParameterEstimationFinal() {
    // clean memory
    delete [] _EM_update_temp;
}

template <class T> void TPriorNormalMixedModelWithHyperPrior<T>::handleStatePosteriorEstimation(const size_t & index, const TDataVector<double, size_t> & StatePosteriorProbabilities){
    // initialize z to posterior mode
    if (!_z->hasFixedInitialValue()) {
        _z->set(index, StatePosteriorProbabilities.maxIndex());
    }
}

//-------------------------------------------
// TPriorNormalMixedModelWithHyperPrior
//-------------------------------------------

template <typename T> TPriorNormalTwoMixedModelsWithHyperPrior<T>::TPriorNormalTwoMixedModelsWithHyperPrior() : TPriorNormalMixedModelWithHyperPrior<T>() {
    this->_name = MCMCPrior::normal_mixedModel_stretch;
    _variance1 = 0.;
}

template <typename T> void TPriorNormalTwoMixedModelsWithHyperPrior<T>::initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, std::vector<std::shared_ptr<TObservationsBase> > & observations){
    // requires 4 parameters: mean, var0, var1, z (in this order!)
    this->_checkSizeParams(params.size(), 4);
    this->_checkSizeObservations(observations.size(), 0);
    this->_mus.push_back(params[0]);
    this->_vars.push_back(params[1]);
    this->_vars.push_back(params[2]);
    this->_z = params[3];

    this->_K = 2;

    this->_checkMinMaxOfHyperPrior();
}

template <typename T> void TPriorNormalTwoMixedModelsWithHyperPrior<T>::_updateTempVals(){
    // need to be updated whenever var or var1 has changed
    // variance of 1-model = sigma_0^2 + sigma_1^2
    _variance1 = _getVar1();
}

template <typename T> double TPriorNormalTwoMixedModelsWithHyperPrior<T>::_getLogPriorDensity_oneVal(T x){
    throw std::runtime_error("In function 'template <typename T> double TPriorNormalMixedModelWithHyperPrior<T>::_getLogPriorDensity_oneVal(T x)': Need to know index of parameter that has changed -> do not call function _getLogPriorDensity_oneVal(T x) with single elements!");
}

template <typename T> double TPriorNormalTwoMixedModelsWithHyperPrior<T>::_getLogPriorRatio_oneVal(T x, T x_old){
    throw std::runtime_error("In function 'template <typename T> double TPriorNormalMixedModelWithHyperPrior<T>::_getLogPriorRatio_oneVal(T x, T x_old)': Need to know index of parameter that has changed -> do not call function _getLogPriorDensity_oneVal(T x) with single elements!");
}

template <typename T> double TPriorNormalTwoMixedModelsWithHyperPrior<T>::_getVar1(){
    return this->_vars[0]->template value<double>() + this->_vars[1]->template value<double>();
}

template <typename T> double TPriorNormalTwoMixedModelsWithHyperPrior<T>::_getVarForThisK(const size_t & k){
    if (k == 0){
        return this->_vars[0]->template value<double>();
    } else {
        return _variance1;
    }
}

template <typename T> double TPriorNormalTwoMixedModelsWithHyperPrior<T>::_getLogPriorDensity(const T & x, const size_t & k){
    // take correct variance
    double var = _getVarForThisK(k);

    double tmp = x - this->_mus[0]->template value<double>();
    return -0.5*log(var) - this->_oneDivTwo_log2Pi - (tmp * tmp)/(2.*var);
}

template <typename T> double TPriorNormalTwoMixedModelsWithHyperPrior<T>::_getLogPriorRatio_vec(const std::shared_ptr<TMCMCParameterBase> &data, const size_t &index) {
    double var = _getVarForThisK(this->_z->template value<size_t>(index));
    double tmp = data->value<T>(index) - this->_mus[0]->template value<double>();
    double tmp_old = data->oldValue<T>(index) - this->_mus[0]->template value<double>();
    return (tmp_old * tmp_old - tmp * tmp) / (2.*var);
}

template <typename T> double TPriorNormalTwoMixedModelsWithHyperPrior<T>::_calcLLUpdateMean(const size_t & k){
    // compute log likelihood
    double LL = 0.;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange range = paramToUse->getFull();
        double tmp_0 = 0.;
        double tmp_1 = 0.;
        for (size_t i = range.first; i < range.last; i += range.increment) {
            double tmpOld = paramToUse->value<T>(i) - this->_mus[0]->template oldValue<double>();
            double tmpNew = paramToUse->value<T>(i) - this->_mus[0]->template value<double>();
            double res = tmpOld * tmpOld - tmpNew * tmpNew;
            if (this->_z->template value<size_t>(i) == 0){
                tmp_0 += res;
            } else {
                tmp_1 += res;
            }
        }
        LL += tmp_0/(2.*this->_vars[0]->template value<double>()) + tmp_1/(2.*_variance1);
    }
    return LL;
}

template <typename T> double TPriorNormalTwoMixedModelsWithHyperPrior<T>::_calcLLUpdateVar(const size_t & k) {
    // need to take densities of both 0-model and 1-model into account
    if (k == 0){
        return _calcLLUpdateVar_0();
    } else {
        return _calcLLUpdateVar_1();
    }
}

template <typename T> double TPriorNormalTwoMixedModelsWithHyperPrior<T>::_calcLLUpdateVar_0() {
    // need to take densities of both 0-model and 1-model into account

    // compute log likelihood
    double LL = 0.;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange range = paramToUse->getFull();

        size_t counter1 = 0;
        double sum0 = 0.;
        double sum1 = 0.;
        for (size_t i = range.first; i < range.last; i += range.increment) {
            double tmp = paramToUse->value<T>(i) - this->_mus[0]->template value<double>();
            double square = tmp*tmp;
            if (this->_z->template value<size_t>(i) == 0) {
                sum0 += square;
            } else {
                sum1 += square;
                counter1++;
            }
        }
        size_t counter0 = this->_z->totalSize() - counter1;

        double variance1_new = _getVar1();

        LL += 0.5 * (counter0 * log(this->_vars[0]->template oldValue<double>() / this->_vars[0]->template value<double>()) +
                     counter1 * log(_variance1 / variance1_new) +
                     (1./this->_vars[0]->template oldValue<double>() - 1./this->_vars[0]->template value<double>()) * sum0 +
                     (1./_variance1 - 1./variance1_new) * sum1);
    }
    return LL;
}

template <typename T> double TPriorNormalTwoMixedModelsWithHyperPrior<T>::_calcLLUpdateVar_1() {
    // var1 is NOT the variance of the 1-model!
    // the variance of the 1-model is var0 + var1

    // compute log likelihood
    double LL = 0.;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange range = paramToUse->getFull();

        size_t counter1 = 0;
        double sum1 = 0.;
        for (size_t i = range.first; i < range.last; i += range.increment) {
            if (this->_z->template value<size_t>(i) == 1) {
                double tmp = paramToUse->value<T>(i) - this->_mus[0]->template value<double>();
                sum1 += tmp * tmp;
                counter1++;
            }
        }

        double variance1_new = _getVar1();
        LL += 0.5 * counter1 * log(_variance1 / variance1_new) + 0.5 * (1./_variance1 - 1./variance1_new) * sum1;
    }
    return LL;
}

template <class T> void TPriorNormalTwoMixedModelsWithHyperPrior<T>::simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) {
    // first update temporary values: might have changed in the meantime
    _updateTempVals();

    // now sample random normal values
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        if (!paramToUse->hasFixedInitialValue()) { // only simulate if parameter has not a fixed value
            TRange range = paramToUse->getFull();
            for (size_t i = range.first; i < range.last; i += range.increment) {
                size_t k = this->_z->template value<size_t>(i);
                double var = _getVarForThisK(k);
                double value = RandomGenerator->getNormalRandom(this->_mus[0]->template value<double>(), sqrt(var));
                while (!paramToUse->valueIsInsideBoundary(value)) { // propose again if value is too large, to prevent overshooting while mirroring
                    value = RandomGenerator->getNormalRandom(this->_mus[0]->template value<double>(), sqrt(var));
                }
                // set value
                paramToUse->set(i, value);
            }
        }
    }
}

template <class T> void TPriorNormalTwoMixedModelsWithHyperPrior<T>::_switchEMLabels(TLog * Logfile) {
    // switch z
    if (!this->_z->hasFixedInitialValue()) {
        for (size_t i = 0; i < this->_z->totalSize(); i++) {
            this->_z->set(i, 1 - this->_z->template value<size_t>(i)); // set to opposite
        }
        // switch prior on z
        this->_z->switchPriorClassificationAfterEM(Logfile);
    }
}

template <class T> void TPriorNormalTwoMixedModelsWithHyperPrior<T>::estimateInitialPriorParameters(TLog * Logfile) {
    // run EM to initialize prior parameters, and pretend that the two variances are independent
    TPriorNormalMixedModelWithHyperPrior<T>::estimateInitialPriorParameters(Logfile);

    // now adjust to model: set larger variance to variance of 1-model
    if (this->_vars[0]->template value<double>() > _variance1){
        if (!this->_vars[0]->hasFixedInitialValue() && !this->_vars[1]->hasFixedInitialValue()) {
            // both var0 and var1 are estimated
            // -> set var0 = variance1
            // -> and variance1 = var0
            // -> and calculate var1 as the difference between the two
            double tmp0 = this->_vars[0]->template value<double>();
            this->_vars[0]->set(_variance1);
            _variance1 = tmp0;
            this->_vars[1]->set(_variance1 - this->_vars[0]->template value<double>());

            _switchEMLabels(Logfile);
        } else if (!this->_vars[1]->hasFixedInitialValue()){
            // var0 is fix, var1 is estimated
            // -> leave var0 as it is
            // -> set var1 to a very small value
            this->_vars[1]->set(0.000001);
            _updateTempVals();

            // don't switch z: labels are still the same
        } else if (!this->_vars[0]->hasFixedInitialValue()){
            // var0 is estimated, var1 if fix
            // -> leave var1 as it is
            // -> set var0
            _variance1 = this->_vars[0]->template value<double>();
            this->_vars[0]->set(_variance1 - this->_vars[1]->template value<double>());

            _switchEMLabels(Logfile);
        } else {
            // both are fix -> won't be updated in EM, so var0 should never be larger than var1!
            throw std::runtime_error("In function 'template <class T> void TPriorNormalTwoMixedModelsWithHyperPrior<T>::estimateInitialPriorParameters(TLog * Logfile)': should have never gotten here! If both var0 and var1 are not updated in EM, we shouldn't switch labels anyways!");
        }
    } else {
        // set _vars[1] as variance1 - sigma_0^2
        if (!this->_vars[1]->hasFixedInitialValue()){
            this->_vars[1]->set(_variance1 - this->_vars[0]->template value<double>());
        }
    }

    // update temporary values
    _updateTempVals();

    // assert that condition holds
    assert(this->_vars[0]->template value<double>() > 0);
    assert(this->_vars[1]->template value<double>() > 0);
    assert(_variance1 > 0);
    assert(this->_vars[0]->template value<double>() + this->_vars[1]->template value<double>() == _variance1);
}

template <class T> void TPriorNormalTwoMixedModelsWithHyperPrior<T>::_setInitialMeanAndVar() {
    _setInitialMean_ReturnVariance();
    _setInitialVar();
}

template <class T> double TPriorNormalTwoMixedModelsWithHyperPrior<T>::_setInitialMean_ReturnVariance() {
    // set mu to MLE (shared by all x)
    TMeanVar<double> meanVar;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange range = paramToUse->getFull();
        for (size_t i = range.first; i < range.last; i += range.increment) {
            meanVar.add(paramToUse->value<T>(i));
        }
    }

    if (!this->_mus[0]->hasFixedInitialValue()) {
        this->_mus[0]->set(meanVar.mean());
    }
    // return overall MLE variance
    return meanVar.variance();
}

template <class T> void TPriorNormalTwoMixedModelsWithHyperPrior<T>::_initializeEMParams_basedOnCutoff(const double & var) {
    // before EM: initialize EM parameters
    // this is the first round: go over all data, calculate log density under overall mean and variance
    // and assign a certain percentage of x that have the highest log density to model 0 and the rest to model 1

    // calculate log density
    std::vector<double> logDens(this->_z->totalSize(), 0.);
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange range = paramToUse->getFull();
        for (size_t i = range.first; i < range.last; i += range.increment) {
            logDens[i] += TNormalDistr::density(paramToUse->value<double>(i), this->_mus[0]->template value<double>(), sqrt(var), true);
        }
    }
    // normalize
    for (auto & it : logDens){
        it /= this->_parameters.size();
    }

    // define cutoff: 10% of all x will go into model 1 and 90% into model 0
    double cutoffPercentage = 0.1;
    // sort densities and define cutoff value
    std::vector<double> sortedLogDens = logDens;
    std::sort(sortedLogDens.begin(), sortedLogDens.end());
    double cutoffValue = sortedLogDens[this->_z->totalSize() * cutoffPercentage];

    // define components
    if (!this->_z->hasFixedInitialValue()) {
        // go over the data again and assign components
        for (size_t i = 0; i < this->_z->totalSize(); i++) {
            if (logDens[i] >= cutoffValue){ // z = 0
                this->_z->set(i, 0);
            } else { // z = 1
                this->_z->set(i, 1);
            }
        }
    }
    // re-calculate variances for these components
    _setInitialVar();
}

template <class T> void TPriorNormalTwoMixedModelsWithHyperPrior<T>::_initializeEMParams_refinement() {
    // before EM: initialize EM parameters
    // this is the second round: go over all data again, calculate log density under overall mean and the two variances we've calculated in the first round
    // assign x that have a higher log density under the 0-model to z=0 and the rest to z=1

    // re-calculate log density with new variances
    std::vector<double> logDens0(this->_z->totalSize(), 0.);
    std::vector<double> logDens1(this->_z->totalSize(), 0.);
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange range = paramToUse->getFull();
        for (size_t i = range.first; i < range.last; i += range.increment) {
            logDens0[i] = _getLogPriorDensity(paramToUse->value<double>(i), 0);
            logDens1[i] = _getLogPriorDensity(paramToUse->value<double>(i), 1);
        }
    }
    for (size_t i = 0; i < this->_z->totalSize(); i ++) {
        logDens0[i] /= this->_parameters.size();
        logDens1[i] /= this->_parameters.size();
    }

    // re-assign components to the model that maximizes the log density
    if (!this->_z->hasFixedInitialValue()) {
        // go over the data again and assign components
        for (size_t i = 0; i < this->_z->totalSize(); i++) {
            if (logDens0[i] >= logDens1[i]){ // z = 0
                this->_z->set(i, 0);
            } else { // z = 1
                this->_z->set(i, 1);
            }
        }
    }

    // re-calculate variances for these components
    _setInitialVar();
}

template <class T> void TPriorNormalTwoMixedModelsWithHyperPrior<T>::_setInitialVar() {
    // set var0 and var1 to MLE
    std::vector<TMeanVar<double>> meanVars(this->_K);
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange range = paramToUse->getFull();
        for (size_t i = range.first; i < range.last; i += range.increment) {
            size_t k = this->_z->template value<size_t>(i);
            meanVars[k].add(paramToUse->value<T>(i));
        }
    }

    // var0
    if (!this->_vars[0]->hasFixedInitialValue()) {
        this->_vars[0]->set(meanVars[0].variance());
    }

    // var1
    if (!this->_vars[0]->hasFixedInitialValue() || !this->_vars[1]->hasFixedInitialValue()) {
        _variance1 = meanVars[1].variance();
    }
}

template <class T> void TPriorNormalTwoMixedModelsWithHyperPrior<T>::initializeEMParameters() {
    // calculate overall mean and variance
    double var = _setInitialMean_ReturnVariance();

    // first round: go over all data and assign a certain percentage to z=0 and the rest to z=1 based on the log density under overall mean and variance
    _initializeEMParams_basedOnCutoff(var);

    // second round: go over all data again and assign z to the model that maximizes the log density (based on the variances we've initialized in the first round)
    _initializeEMParams_refinement();
}

template <class T> void TPriorNormalTwoMixedModelsWithHyperPrior<T>::calculateEmissionProbabilities(const size_t & Index, TDataVector<double, size_t> & Emission){
    // initialize emissions with zero
    for (size_t k = 0; k < this->_K; k++) {
        Emission[k] = 0.;
    }

    // go over all parameters and over all components and sum emission probability
    // we want emission not in log, but we will use the log to sum and then de-log in the end
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        // log P(x | z = k, theta)
        for (size_t k = 0; k < this->_K; k++) {
            Emission[k] += _getLogPriorDensity(paramToUse->value<double>(Index), k);
        }
    }

    // get rid of log
    for (size_t k = 0; k < this->_K; k++) {
        Emission[k] = exp(Emission[k]);
    }
};

template <class T> void TPriorNormalTwoMixedModelsWithHyperPrior<T>::prepareEMParameterEstimationInitial() {
    // initialize data: per component, we need to store two sums
    // 1) sum_{i=1}^N weights_ki * sum_{p=1}^P (x_pi - mu)^2
    // 2) sum_{i=1}^N weights_ki
    this->_EM_update_temp = new double[2*this->_K];
}

template <class T> void TPriorNormalTwoMixedModelsWithHyperPrior<T>::prepareEMParameterEstimationOneIteration(){
    // fill data with zeros
    for (size_t i = 0; i < 2*this->_K; i++){
        this->_EM_update_temp[i] = 0.;
    }
};

template <class T> void TPriorNormalTwoMixedModelsWithHyperPrior<T>::handleEMParameterEstimationOneIteration(const size_t & index, const TDataVector<double, size_t> & Weights){
    // calculate
    // sum_{p=1}^P sum_{i=1}^N (x_pi - mu)^2
    double sumXpiMinusMuSquare = 0.;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        double tmp = paramToUse->value<double>(index) - this->_mus[0]->template value<double>();
        sumXpiMinusMuSquare += tmp*tmp;
    }

    // add to sums
    for (size_t k = 0; k < this->_K; k++) {
        // add to sum (x_i - mu)^2 * weights_ki
        this->_EM_update_temp[k*2] += sumXpiMinusMuSquare * Weights[k];
        // add to sum weights
        this->_EM_update_temp[k*2+1] += Weights[k];
    }
}

template <class T> void TPriorNormalTwoMixedModelsWithHyperPrior<T>::finalizeEMParameterEstimationOneIteration(){
    // M-step: update parameters

    // 0-model
    if (!this->_vars[0]->hasFixedInitialValue()){
        this->_vars[0]->set(this->_EM_update_temp[0*2] / (this->_parameters.size() * this->_EM_update_temp[0*2+1]));
    }

    // 1-model
    if (!this->_vars[0]->hasFixedInitialValue() || !this->_vars[1]->hasFixedInitialValue()){
        _variance1 = this->_EM_update_temp[1*2] / (this->_parameters.size() * this->_EM_update_temp[1*2+1]);
    }
}

//-------------------------------------------------
// TPriorMultivariateNormalMixedModelWithHyperPrior
//-------------------------------------------------
#ifdef WITH_ARMADILLO
template <typename T> TPriorMultivariateNormalMixedModelWithHyperPrior<T>::TPriorMultivariateNormalMixedModelWithHyperPrior() : TMultivariateNormal<T>(), TLatentVariable<double, size_t, size_t>() {
    this->_name = MCMCPrior::multivariateNormal_mixedModel;

    _EM_update_Sigma_temp = nullptr;
    _EM_update_Weights_temp = nullptr;
    _EM_update_Mus_temp = nullptr;
}

template <typename T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, std::vector<std::shared_ptr<TObservationsBase> > & observations){
    // requires at least 5 parameters: K times mus (1D), K times m (single), K times Mrr (1D), K times Mrs (1D), and z (1D)
    if (params.size() < 5){
        throw std::runtime_error("In function 'template <typename T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params)': Size of TMCMCParameter vector (" + toString(params.size()) + ") does not match the number of parameters of a " + this->_name + " distribution (at least 5: mus, m, mrr, mrs, z)!");
    }
    this->_checkSizeObservations(observations.size(), 0);

    // calculate K = (params.size - 1)/4 where -1 for z, and the rest should be 1/4 mu, m, mrr and mrs each -> /4
    double tmp = (params.size() - 1.)/4.;
    _K = static_cast<size_t>(tmp);
    if (static_cast<double>(_K) != tmp){
        throw std::runtime_error("In function 'template <typename T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params)': Provided wrong number of parameters to function! Guessed K = (params.size() - 1) / 4 = " + toString(tmp) + ", which is not an integer.");
    }

    // now fill parameter vectors and check if size is ok
    size_t k = 0;
    // mu
    for (; k < _K; k++){
        _mus.push_back(params[k]);
    }
    // m
    for (; k < 2*_K; k++){
        _m.push_back(params[k]);
    }
    // Mrr
    for (; k < 3*_K; k++){
        _Mrr.push_back(params[k]);
    }
    // mrs
    for (; k < 4*_K; k++){
        _Mrs.push_back(params[k]);
    }
    // z
    _z = params.back();
}

template <typename T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_checkMinMaxOfHyperPrior(){
    // parameter _Mrr and _m should always be > 0 -> re-set boundaries of _Mrr and _m
    for (auto & mrr : _Mrr){
        this->_reSetBoundaryTo0_M_Mrr(mrr);
    }
    for (auto & m : _m) {
        this->_reSetBoundaryTo0_M_Mrr(m);
    }
    // parameter _z should always be between 0 and K-1 -> reset boundaries
    _z->reSetBoundaries(false, false, "0", toString(_K-1), true, true);
}

template <class T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::initializeStorageOfPriorParameters() {
    // derive D from dimensionality of parameters below
    TMultivariateNormal<T>::_calculateAndSetD();
    const std::shared_ptr<TNamesEmpty> dimNameD = this->_getDimensionName();

    // derive N and dimension names from dimensionality of parameters below
    std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(this->_parameters[0]);
    size_t N = paramToUse->dimensions()[0];
    const std::shared_ptr<TNamesEmpty> dimNamesFirstDim = paramToUse->getDimensionName(0);

    for (auto & param : this->_parameters) {
        paramToUse = this->_getSharedPtrParam(param);
        if (paramToUse->dimensions()[0] != N){
            throw std::runtime_error("In function 'template <typename T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::initializeStorageOfPriorParameters()': N of parameter " + paramToUse->name() + " (=" + toString(paramToUse->dimensions()[0]) + ") and of other shared parameter (=" + toString(N) + ") do not match!");
        }
        // check if ptr to dimension names is the same
        if (paramToUse->getDimensionName(0) != dimNamesFirstDim){
            throw std::runtime_error("In function 'template <typename T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::initializeStorageOfPriorParameters()': Pointer to dimension name class of dimension 0 of parameter " + paramToUse->name() + " differs from that of other shared parameter!");
        }
    }

    for (auto & mu : _mus){
        this->_setDimensionsOfMu(mu, dimNameD);
    }
    // m
    for (auto & m : _m){
        this->_setDimensionsOfM(m);
    }
    // Mrr
    for (auto & mrr : _Mrr){
        this->_setDimensionsOfMrr(mrr, dimNameD);
    }
    // Mrs
    for (auto & mrs : _Mrs) {
        this->_setDimensionsOfMrs(mrs, dimNameD);
    }
    _z->initializeStorageBasedOnPrior({N}, {dimNamesFirstDim});
}

template <class T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::constructDAG(TDAG & DAG, TDAG & temporaryDAG){
    // construct vector of prior parameters
    std::vector<std::shared_ptr<TMCMCObservationsBase>> params;
    for (auto & mu : _mus){
        params.push_back(mu);
    }
    for (auto & m : _m){
        params.push_back(m);
    }
    for (auto & mrr : _Mrr){
        params.push_back(mrr);
    }
    for (auto & mrs : _Mrs) {
        params.push_back(mrs);
    }
    params.push_back(_z);

    this->_constructDAG(DAG, temporaryDAG, params);
}


template <class T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::typesAreCompatible(bool throwIfNot) {
    // parameters on which multivariate normal mixed prior is defined must be floating point type
    this->_checkTypes(throwIfNot, true, false, false);

    // in addition: check if prior parameters have expected type
    // all _mu, _m, _Mrr and _Mrs must be floating point
    for (auto & mu : _mus){
        this->_checkTypesOfPriorParameters(mu, throwIfNot, true, false, false);
    }
    for (auto & m : _m){
        this->_checkTypesOfPriorParameters(m, throwIfNot, true, false, false);
    }
    for (auto & mrr : _Mrr){
        this->_checkTypesOfPriorParameters(mrr, throwIfNot, true, false, false);
    }
    for (auto & mrs : _Mrs) {
        this->_checkTypesOfPriorParameters(mrs, throwIfNot, true, false, false);
    }
    // _z must be unsigned integer
    this->_checkTypesOfPriorParameters(_z, throwIfNot, false, true, true);

    // check if the prior distribution on the prior parameters is ok
    for (auto & mu : _mus){
        mu->typesAreCompatible(throwIfNot);
    }
    for (auto & m : _m){
        m->typesAreCompatible(throwIfNot);
    }
    for (auto & mrr : _Mrr){
        mrr->typesAreCompatible(throwIfNot);
    }
    for (auto & mrs : _Mrs) {
        mrs->typesAreCompatible(throwIfNot);
    }
    _z->typesAreCompatible(throwIfNot);
}

template <typename T> double TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_getLogPriorDensity_vec(const std::shared_ptr<TMCMCObservationsBase> & data, const size_t & index){
    // calculates the log prior density of a given parameter value

    // index corresponds to index in linear array
    // -> calculate index in multi-array:
    std::vector<size_t> subscript_updatedParam = data->getSubscripts(index);
    size_t n = subscript_updatedParam[0];
    TRange row = data->get1DSlice(1, {n, 0}); // one row of Tdata
    size_t k = _z->value<size_t>(n);
    return this->_calcLogPriorDensity(data, row, _mus[k], _m[k], _Mrr[k], _Mrs[k]);
}

template <typename T> double TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_getLogPriorDensityOld_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index){
    // index corresponds to index in linear array
    // -> calculate index in multi-array:
    std::vector<size_t> subscript_updatedParam = data->getSubscripts(index);
    size_t n = subscript_updatedParam[0];
    size_t d = subscript_updatedParam[1];
    TRange row = data->get1DSlice(1, {n, 0}); // one row of Tdata
    size_t k = _z->value<size_t>(n);
    return this->_calcLogPriorDensityOld(data, row, d, _mus[k], _m[k], _Mrr[k], _Mrs[k]);
}

template <typename T> double  TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_getLogPriorRatio_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index){
    // calculates log prior ratio for a given parameter value

    // index corresponds to index in linear array
    // -> calculate index in multi-array:
    std::vector<size_t> subscript_updatedParam = data->getSubscripts(index);
    size_t n = subscript_updatedParam[0];
    size_t d = subscript_updatedParam[1];
    TRange row = data->get1DSlice(1, {n, 0}); // one row of Tdata
    size_t k = _z->value<size_t>(n);
    return this->_calcLogPriorRatio(data, row, d, _mus[k], _m[k], _Mrr[k], _Mrs[k]);
}

template <typename T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::updateParams(){
    // update mus
    for (size_t k = 0; k < _K; k++) {
        for (size_t d = 0; d < this->_D; d++) {
            if (_mus[k]->update(d))
                _updateMu(k, d);
        }
        // update m
        if (_m[k]->update())
            _updateM(k);
        // update Mrr and Mrs (only if >1 dimensions to avoid overfitting)
        if (this->_D > 1) {
            for (size_t s = 0; s < this->_D; s++) {
                for (size_t r = s; r < this->_D; r++) {
                    if (s == r) {
                        if (_Mrr[k]->update(r))
                            _updateMrr(k, r);
                    } else {
                        if (_Mrs[k]->update(this->_linearizeIndex_Mrs(r, s)))
                            _updateMrs(k, r, s);
                    }
                }
            }
        }
    }
    // update z
    TRange range = _z->getFull();
    for (size_t n = range.first; n < range.last; n += range.increment){
        if (_z->update(n))
            _updateZ(n);
    }
}

template <typename T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_updateMrr(const size_t & k, const size_t & r) {
    // compute log(Hastings ratio)
    double logH = _calcLogHUpdateMrr(k, r);
    // accept or reject
    _Mrr[k]->acceptOrReject(r, logH);
}

template <typename T> double TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_calcLogHUpdateMrr(const size_t & k, const size_t & r){
    // compute log likelihood
    double LL = _calcLLUpdateMrr(k, r);
    // compute prior
    double logPrior = _Mrr[k]->getLogPriorRatio(r);
    return LL + logPrior;
}

template <typename T> double TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_calcLLUpdateMrr(const size_t & k, const size_t & r){
    // compute log likelihood
    double LL = 0;

    double oldMrr = _Mrr[k]->oldValue<double>(r);
    double newMrr = _Mrr[k]->value<double>(r);
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        size_t N = paramToUse->dimensions()[0];

        size_t  counter = 0;
        double sumParam_nd_minus_mu_d = 0.;
        double sum = 0.;
        for (size_t n = 0; n < N; n++) {
            // calculate simplified ratio
            if (_z->value<size_t>(n) == k) {
                TRange row = paramToUse->get1DSlice(1, {n, 0});
                sum += this->_calcDoubleSum_updateMrr(paramToUse, row, r, sumParam_nd_minus_mu_d, _mus[k], _Mrs[k]);
                counter++;
            }
        }

        LL += this->_calcLLRatioUpdateMrr(counter, oldMrr, newMrr, sum, sumParam_nd_minus_mu_d, _m[k]);
    }
    return LL;
}

template <typename T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_updateMrs(const size_t & k, const size_t & r, const size_t & s) {
    // compute log(Hastings ratio)
    double logH = _calcLogHUpdateMrs(k, r, s);
    // accept or reject
    _Mrs[k]->acceptOrReject(this->_linearizeIndex_Mrs(r, s), logH);
}

template <typename T> double TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_calcLogHUpdateMrs(const size_t & k, const size_t & r, const size_t & s){
    // compute log likelihood
    double LL = _calcLLUpdateMrs(k, r, s);
    // compute prior
    double logPrior = _Mrs[k]->getLogPriorRatio(this->_linearizeIndex_Mrs(r, s));
    return LL + logPrior;
}

template <typename T> double TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_calcLLUpdateMrs(const size_t & k, const size_t & r, const size_t & s) {
    // compute log likelihood
    double LL = 0;

    size_t linearIndex = this->_linearizeIndex_Mrs(r, s);
    double oldMrs = _Mrs[k]->oldValue<double>(linearIndex);
    double newMrs = _Mrs[k]->value<double>(linearIndex);

    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        size_t N = paramToUse->dimensions()[0];

        double sum = 0.;
        double sumParam_nd_minus_mu_d = 0.;
        for (size_t n = 0; n < N; n++) {
            // calculate simplified ratio
            if (_z->value<size_t>(n) == k) {
                TRange row = paramToUse->get1DSlice(1, {n, 0});
                sum += this->_calcDoubleSum_updateMrs(paramToUse, row, r, s, sumParam_nd_minus_mu_d, _mus[k], _Mrr[k], _Mrs[k]);
            }
        }

        LL += this->_calcLLRatioUpdateMrs(oldMrs, newMrs, sum, sumParam_nd_minus_mu_d, _m[k]);
    }
    return LL;
}

template <typename T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_updateMu(const size_t & k, const size_t & r){
    // compute log(Hastings ratio)
    double logH = _calcLogHUpdateMu(k, r);
    // accept or reject
    _mus[k]->acceptOrReject(r, logH);
}

template <typename T> double TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_calcLogHUpdateMu(const size_t & k, const size_t & r) {
    // compute log likelihood
    double LL = _calcLLUpdateMu(k, r);
    // compute prior
    double logPrior = _mus[k]->getLogPriorRatio(r);
    return LL + logPrior;
}

template <typename T> double TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_calcLLUpdateMu(const size_t & k, const size_t & r) {
    // compute log likelihood
    double LL = 0.;

    double oldMu = _mus[k]->oldValue<double>(r);
    double newMu = _mus[k]->value<double>(r);
    double mrr = _Mrr[k]->value<double>(r);

    size_t min_r_DMin1 = std::min(r+1, static_cast<size_t>(this->_D-1));

    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        size_t N = paramToUse->dimensions()[0];

        double sum = 0.;
        double sumParam = 0.;
        size_t counter = 0;
        for (size_t n = 0; n < N; n++) {
            // calculate simplified ratio
            if (_z->value<size_t>(n) == k) {
                TRange row = paramToUse->get1DSlice(1, {n, 0});
                sum += this->_calcDoubleSum_updateMu(paramToUse, row, r, min_r_DMin1, _mus[k], _Mrr[k], _Mrs[k]);
                sumParam += paramToUse->value<T>(row.first + r * row.increment);
                counter++;
            }
        }
        double sumM = this->_calcSumM_dr_squared(mrr, r, _Mrs[k]);

        LL += this->_calcLLRatioUpdateMu(oldMu, newMu, sumM, counter, sumParam, sum, _m[k]);
    }
    return LL;
}

template <typename T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_updateM(const size_t & k){
    // compute log(Hastings ratio)
    double logH = _calcLogHUpdateM(k);
    // accept or reject
    _m[k]->acceptOrReject(logH);
}

template <typename T> double TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_calcLogHUpdateM(const size_t & k) {
    // compute log likelihood
    double LL = _calcLLUpdateM(k);
    // compute prior
    double logPrior = _m[k]->getLogPriorRatio();
    return LL + logPrior;
}

template <typename T> double TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_calcLLUpdateM(const size_t & k) {
    // compute log likelihood
    double LL = 0;

    double oldM = _m[k]->oldValue<double>();
    double newM = _m[k]->value<double>();

    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        size_t N = paramToUse->dimensions()[0];

        size_t  counter = 0;
        double sum = 0.;
        for (size_t n = 0; n < paramToUse->dimensions()[0]; n++) {
            if (_z->value<size_t>(n) == k) {
                TRange row = paramToUse->get1DSlice(1, {n, 0});
                // calculate
                // sum_s (sum_r mrs(beta_nr - mu_r)^2)
                sum += this->_calcDoubleSum(paramToUse, row, _mus[k], _Mrr[k], _Mrs[k]);
                counter++;
            }
        }

        LL += this->_calcLLRatioUpdateM(counter, oldM, newM, sum);
    }
    return LL;
}

template <typename T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_updateZ(const size_t &n) {
    // compute log(Hastings ratio)
    double logH = _calcLogHUpdateZ(n);
    // accept or reject
    _z->acceptOrReject(n, logH);
}

template <typename T> double TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_calcLogHUpdateZ(const size_t &n) {
    // compute log likelihood
    double LL = _calcLLUpdateZ(n);
    // compute prior
    double logPrior = _z->getLogPriorRatio(n);
    return LL + logPrior;
}

template <typename T> double TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_calcLLUpdateZ(const size_t &n) {
    // compute log likelihood
    double LL = 0;

    size_t k = _z->oldValue<size_t>(n);
    size_t j = _z->value<size_t>(n);

    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange row = paramToUse->get1DSlice(1, {n, 0});

        double PZ_k = this->_calcLogPriorDensity(paramToUse, row, _mus[k], _m[k], _Mrr[k], _Mrs[k]);
        double PZ_j = this->_calcLogPriorDensity(paramToUse, row, _mus[j], _m[j], _Mrr[j], _Mrs[j]);

        LL += PZ_j - PZ_k;
    }
    return LL;
}

template <class T> double TPriorMultivariateNormalMixedModelWithHyperPrior<T>::getSumLogPriorDensity(const std::shared_ptr<TMCMCObservationsBase> & data) {
    double sum = 0.;
    // always take first element of each row
    for (size_t n = 0; n < data->dimensions()[0]; n++) {
        TRange row = data->get1DSlice(1, {n, 0});
        sum += _getLogPriorDensity_vec(data, row.first);
    }
    return sum;
}

template <class T> double TPriorMultivariateNormalMixedModelWithHyperPrior<T>::getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data) {
    // returns sum of log-density of array + log density of mu + log density of m + log density of mrr + log density of mrs + log density of z
    double sum = 0.;
    for (size_t k = 0; k < _K; k++){
        sum += _mus[k]->getLogPriorDensityFull();
        sum += _m[k]->getLogPriorDensity();
        if (this->_D > 1){
            sum += _Mrr[k]->getLogPriorDensityFull();
            sum += _Mrs[k]->getLogPriorDensityFull();
        }
    }
    sum += _z->getLogPriorDensityFull();
    sum += this->getSumLogPriorDensity(data);
    return  sum;
}

template <class T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) {
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        if (!paramToUse->hasFixedInitialValue()) { // only simulate if parameter has not a fixed value
            assert(paramToUse->dimensions()[1] == this->_D); // number of columns of data must match D

            // get all L's ready (don't want to compute them for each row again)
            std::vector<arma::mat> Ls;
            for (size_t k = 0; k < _K; k++) {
                Ls.emplace_back(this->_getArmadilloLForSimulation(_Mrr[k], _Mrs[k], _m[k]));
            }

            for (size_t n = 0; n < paramToUse->dimensions()[0]; n++) { // go over all rows
                TRange row = paramToUse->get1DSlice(1, {n, 0});
                size_t k = _z->value<size_t>(n);
                this->_simulateMultivariateNormal(paramToUse, row.first, Ls[k], _mus[k], RandomGenerator);
            }
        }
    }
}

template <class T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::estimateInitialPriorParameters(TLog * Logfile) {
    // check: either set all M-parameters (m, Mrr and Mrs) of a component, or none. Throw if only part of them should be initialized
    for (size_t k = 0; k < _K; k++){
        if (!_Mrr[k]->hasFixedInitialValue() || !_Mrs[k]->hasFixedInitialValue() || !_m[k]->hasFixedInitialValue()) { // at least one should be estimated
            if (!_Mrr[k]->hasFixedInitialValue() && !_Mrs[k]->hasFixedInitialValue() && !_m[k]->hasFixedInitialValue()) {
                // all are estimated -> ok
            } else {
                throw "Error when initializing " + this->_name + " prior: parameters m, Mrr and Mrs for component " + toString(k) + " must either all or none have an initial value. Can not initialize only partially!";
            }
        }
    }

    if (!_z->hasFixedInitialValue()) { // latent variable not known -> run EM
        _z->runEMEstimation(*this, Logfile);
    } else {
        _setInitialMeanAndM();
    }
}

// EM
template <class T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_fillMeanParameterValues(std::vector<std::vector<double>> & meanParamValues) {
    // fill a vector with the mean values of all parameters
    // dimensions: N times D
    meanParamValues.resize(_z->totalSize(), std::vector<double>(this->_D, 0.));

    for (auto & param : this->_parameters) {

        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        for (size_t i = 0; i < paramToUse->dimensions()[0]; i++) { // go over all rows (N)
            TRange row = paramToUse->get1DSlice(1, {i, 0});
            size_t c = 0;
            for (size_t d = row.first; d < row.last; d += row.increment, c++) { // go over all cols of current row (D)
                meanParamValues[i][c] += paramToUse->value<T>(d);
            }
        }
    }

    // normalize
    for (auto & it : meanParamValues){
        for (auto & it2 : it) {
            it2 /= this->_parameters.size();
        }
    }
}

template <class T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_findMostDistantPoints(std::vector<std::vector<T>> & minValues,
                                                                                        const std::vector<std::vector<double>> & meanParameterValues) {
    std::vector<double> minDistances(_z->totalSize());
    minValues.resize(_K, std::vector<T>(this->_D, 0)); // K times D

    // 1st value = 1st group
    for (size_t d = 0; d < this->_D; d++){
        minValues[0][d] = meanParameterValues[0][d];
    }

    for (size_t k = 1; k < _K; k++){
        for (size_t i = 0; i < _z->totalSize(); i++){
            // find minimal distance among all previous k
            double minDist = euclideanDistance(meanParameterValues[i], minValues[0]);
            for (size_t smallerK = 1; smallerK < k; smallerK++) {
                double tmp = euclideanDistance(meanParameterValues[i], minValues[smallerK]);
                if (tmp < minDist) {
                    minDist = tmp;
                }
            }
            minDistances[i] = minDist;
        }

        // assign the data point that maximizes minDistances to the next K
        size_t maxIndex =  std::distance(minDistances.begin(), std::max_element(minDistances.begin(), minDistances.end()));
        for (size_t d = 0; d < this->_D; d++){
            minValues[k][d] = meanParameterValues[maxIndex][d];
        }
    }
}

template <class T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_setInitialZ(std::vector<std::vector<T>> & minValues,
                                                                              const std::vector<std::vector<double>> & meanParameterValues) {
    if (!_z->hasFixedInitialValue()) {
        // go over the data again and assign the closest group to each point
        for (size_t i = 0; i < _z->totalSize(); i++) {
            size_t minK = 0;
            for (size_t k = 1; k < _K; k++) {
                if (euclideanDistance(meanParameterValues[i], minValues[k]) < euclideanDistance(meanParameterValues[i], minValues[minK])) {
                    minK = k;
                }
            }
            _z->set(i, minK);
        }
    }
}

template <class T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_setInitialMeanAndM() {
    _setMusToMLE();
    _setMToMLE();
}

template <class T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_setMusToMLE(){
    // calculate MLE of mus (based on classification by z)

    // initialize data: K times D
    std::vector<std::vector<double>> sums(_K, std::vector<double>(this->_D, 0.));
    std::vector<double> totalNumElements(_K, 0.);

    for (auto &param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        for (size_t n = 0; n < paramToUse->dimensions()[0]; n++) { // go over all rows (N)
            TRange row = paramToUse->get1DSlice(1, {n, 0});
            // get component
            size_t k = _z->value<size_t>(n);
            // add to sum
            this->_addToSumMLEMu(sums[k], row, paramToUse);
            totalNumElements[k]++;
        }
    }

    // normalize and set mus
    for (size_t k = 0; k < _K; k++) {
        this->_normalizeSumMLEMu(sums[k], totalNumElements[k]);
        if (!_mus[k]->hasFixedInitialValue()) {
            for (size_t d = 0; d < this->_D; d++) {
                _mus[k]->set(d, sums[k][d]);
            }
        }
    }
}

template <class T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_setMToMLE(){
    // calculate MLE of M (based on classification by z)

    // initialize data for sums: vector of size K with armadillo matrices of dimensions D times D
    std::vector<arma::mat> sums(_K, arma::mat(this->_D, this->_D, arma::fill::zeros));

    std::vector<double> totalNumElements(_K, 0.);
    for (auto &param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        for (size_t n = 0; n < paramToUse->dimensions()[0]; n++) { // go over all rows (N)
            TRange row = paramToUse->get1DSlice(1, {n, 0});
            // get component
            size_t k = _z->value<size_t>(n);
            // add to sum
            this->_addToSumMLESigma(sums[k], row, paramToUse, _mus[k]);
            totalNumElements[k]++;
        }
    }

    // normalize
    for (size_t k = 0; k < _K; k++) {
        this->_normalizeSumMLESigma(sums[k], totalNumElements[k]);
    }

    // fill parameters of M
    for (size_t k = 0; k < _K; k++) {
        if (!_Mrr[k]->hasFixedInitialValue() && !_Mrs[k]->hasFixedInitialValue() && !_m[k]->hasFixedInitialValue()) {
            this->_fillParametersMFromSigma(sums[k], _Mrr[k], _Mrs[k], _m[k]);
        } // else don't set them, do nothing
    }
}

template <class T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::initializeEMParameters() {
    // calculate the mean value per entry (over all parameters)
    std::vector<std::vector<double>> meanParameterValues;
    _fillMeanParameterValues(meanParameterValues);

    // find points that are most far apart
    std::vector<std::vector<T>> minValues;
    _findMostDistantPoints(minValues, meanParameterValues);

    // go over the data again and assign the closest group to each point
    _setInitialZ(minValues, meanParameterValues);

    // calculate mean and M according to this classification
    _setInitialMeanAndM();
}

template<class T> size_t TPriorMultivariateNormalMixedModelWithHyperPrior<T>::getHiddenState(const size_t &Index) const {
    return _z->value<size_t>(Index);
}

template <class T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::calculateEmissionProbabilities(const size_t & Index, TDataVector<double, size_t> & Emission){
    // initialize emissions with zero
    for (size_t k = 0; k < _K; k++) {
        Emission[k] = 0.;
    }

    // go over all parameters and over all components and sum emission probability
    // we want emission not in log, but we will use the log to sum and then de-log in the end
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        // log P(x | z = k, theta)
        for (size_t k = 0; k < _K; k++) {
            TRange row = paramToUse->get1DSlice(1, {Index, 0}); // one row of param
            Emission[k] += this->_calcLogPriorDensity(paramToUse, row, _mus[k], _m[k], _Mrr[k], _Mrs[k]);
        }
    }

    // get rid of log
    for (size_t k = 0; k < _K; k++) {
        Emission[k] = exp(Emission[k]);
    }
};

template <class T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::prepareEMParameterEstimationInitial() {
    // initialize data: per component, we need to store three sums
    // 1) sum_{i=1}^N weights_ki * sum_{p=1}^P x_pi
    // 2) sum_{i=1}^N weights_ki * sum_{p=1}^P (x_pi - mu_k)(x_pi - mu_k)^T
    // 3) sum_{i=1}^N weights_ki
    _EM_update_Mus_temp = new double[_K*this->_D];
    _EM_update_Sigma_temp = new arma::mat[_K];
    for (size_t i = 0; i < _K; i++){
        _EM_update_Sigma_temp[i] = arma::mat(this->_D, this->_D, arma::fill::zeros);
    }
    _EM_update_Weights_temp = new double[_K];
}

template <class T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::prepareEMParameterEstimationOneIteration(){
    // fill data with zeros
    for (size_t i = 0; i < _K*this->_D; i++){
        _EM_update_Mus_temp[i] = 0.;
    }
    for (size_t i = 0; i < _K; i++){
        _EM_update_Sigma_temp[i].zeros();
        _EM_update_Weights_temp[i] = 0.;
    }
};

template <class T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_handleEMMaximizationOneIteration_updateSigma(const size_t & k, const arma::mat * sumXpiMinusMuSquare, const TDataVector<double, size_t> & Weights){
    // add to sum (x_pi - mu_k)(x_pi - mu_k)^T * weights_ki
    for (size_t d=0; d < this->_D; d++) {
        for (size_t e=0; e < this->_D; e++) {
            _EM_update_Sigma_temp[k](d, e) += sumXpiMinusMuSquare[k](d, e) * Weights[k];
        }
    }
}

template <class T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::handleEMParameterEstimationOneIteration(const size_t & index, const TDataVector<double, size_t> & Weights){
    // calculate
    // 1) sum_{p=1}^P sum_{i=1}^N x_pi
    // 2) sum_{p=1}^P sum_{i=1}^N (x_pi - mu_k)(x_pi - mu_k)^T

    // initialize data for sums
    std::vector<double> sumXpi(this->_D, 0.);
    arma::mat * sumXpiMinusMuSquare; // 3 dimensions: K times an Armadillo matrix with D times D dimensions
    sumXpiMinusMuSquare = new arma::mat [_K];
    for (size_t k = 0; k < _K; k++){
        sumXpiMinusMuSquare[k] = arma::mat(this->_D, this->_D, arma::fill::zeros);
    }

    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange row = paramToUse->get1DSlice(1, {index, 0}); // one row of param
        for (size_t d = 0; d < this->_D; d++) {
            sumXpi[d] += paramToUse->value<double>(row.first + d*row.increment);
            for (size_t k = 0; k < _K; k++) {
                for (size_t e = 0; e < this->_D; e++){
                    sumXpiMinusMuSquare[k](d, e) += (paramToUse->value<double>(row.first + d*row.increment) - _mus[k]->value<double>(d))
                                                    * (paramToUse->value<double>(row.first + e*row.increment) - _mus[k]->value<double>(e));
                }
            }
        }
    }

    // add to sums
    for (size_t k = 0; k < _K; k++) {
        // add to sum x_pi * weights_ki
        for (size_t d=0; d < this->_D; d++) {
            _EM_update_Mus_temp[k*this->_D + d] += sumXpi[d] * Weights[k];
        }
        // add to sum (x_pi - mu_k)(x_pi - mu_k)^T * weights_ki
        _handleEMMaximizationOneIteration_updateSigma(k, sumXpiMinusMuSquare, Weights);
        // add to sum weights
        _EM_update_Weights_temp[k] += Weights[k];
    }
}

template <class T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_updateEMParametersOneIteration_Sigma(arma::mat & Sigma, const size_t & k){
    Sigma.zeros(this->_D, this->_D);
    for (size_t d=0; d < this->_D; d++) {
        for (size_t e=0; e < this->_D; e++) {
            Sigma(d, e) = _EM_update_Sigma_temp[k](d, e) / (this->_parameters.size() * _EM_update_Weights_temp[k]);
        }
    }
}

template <class T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::finalizeEMParameterEstimationOneIteration(){
    // M-step: update parameters
    for (size_t k = 0; k < _K; k++){
        // mus
        if (!_mus[k]->hasFixedInitialValue()) {
            for (size_t d=0; d < this->_D; d++) {
                double value = _EM_update_Mus_temp[k*this->_D + d] / (this->_parameters.size() * _EM_update_Weights_temp[k]);
                _mus[k]->set(d, value);
            }
        }

        // Sigma
        if (!_Mrr[k]->hasFixedInitialValue() && !_Mrs[k]->hasFixedInitialValue() && !_m[k]->hasFixedInitialValue()) {
            // first calculate EM update
            arma::mat Sigma;
            _updateEMParametersOneIteration_Sigma(Sigma, k);
            // decompose Sigma and set parameters of M
            this->_fillParametersMFromSigma(Sigma, _Mrr[k], _Mrs[k], _m[k]);
        }  // else don't set them, do nothing
    }
}

template <class T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::finalizeEMParameterEstimationFinal() {
    // clean memory
    delete [] _EM_update_Mus_temp;
    delete [] _EM_update_Sigma_temp;
    delete [] _EM_update_Weights_temp;
}

template <class T> void TPriorMultivariateNormalMixedModelWithHyperPrior<T>::handleStatePosteriorEstimation(const size_t & index, const TDataVector<double, size_t> & StatePosteriorProbabilities){
    // initialize z to posterior mode
    if (!_z->hasFixedInitialValue()) {
        _z->set(index, StatePosteriorProbabilities.maxIndex());
    }
}

#endif // WITH_ARMADILLO

//-------------------------------------------------
// TPriorMultivariateNormalTwoMixedModelsWithHyperPrior
//-------------------------------------------------
#ifdef WITH_ARMADILLO
template <typename T> TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::TPriorMultivariateNormalTwoMixedModelsWithHyperPrior() : TPriorMultivariateNormalMixedModelWithHyperPrior<T>() {
    this->_name = MCMCPrior::multivariateNormal_mixedModel_stretch;
}

template <typename T> arma::vec TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_stretchEigenVals(const arma::vec& Lambda_0){
    arma::vec Lambda_1(this->_D);
    for (size_t d = 0; d < this->_D; d++){
        Lambda_1(d) = Lambda_0(d) * (1. + _rhos->value<double>(d)); // element-wise multiplication
    }
    return Lambda_1;
}

template <typename T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_stretchSigma1(){
    arma::vec Lambda_0;
    arma::mat Q;
    // eigen decomposition
    // -> Lambda_0 will contain eigenvalues (in ascending order, attention: different than in R!)
    // -> Q contains the eigenvectors, sorted corresponding to eigenvalues
    arma::eig_sym(Lambda_0, Q, _zeroModel.Sigma); // directly fills Lambda_0 and Q

    // stretch eigenvalues
    arma::vec Lambda_1 = _stretchEigenVals(Lambda_0);

    // calculate Sigma1
    _oneModel.oldSigma = _oneModel.Sigma;
    _oneModel.Sigma = arma::symmatu(Q * arma::diagmat(Lambda_1) * Q.i()); // not always symmetric because of rounding errors in Q -> symmatu makes it symmetric
}

template <typename T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_fill_M0(){
    _zeroModel.oldM = _zeroModel.M;

    // check if m and mrr are too small (if m is super small -> update it -> issues with taking the inverse of M0*M0.t())
    if (this->_m[0]->template value<double>() < 0.0001){
        this->_m[0]->set(1.);
    }
    for (size_t d = 0; d < this->_D; d++){
        if (this->_Mrr[0]->template value<double>(d) < 0.0001) {
            this->_Mrr[0]->set(d, 1.);
        }
    }

    this->_fillArmadilloMFromParametersM(_zeroModel.M, this->_Mrr[0], this->_Mrs[0], this->_m[0]);

    // calculate Sigma0
    _zeroModel.calculateSigma();
}

template <typename T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_fill_M0_afterUpdateMrr(const double & newVal, const size_t & r){
    _zeroModel.oldM = _zeroModel.M;
    // update armadillo matrix, always divide by m
    _zeroModel.M(r, r) = newVal / this->_m[0]->template value<double>();

    // calculate Sigma0
    _zeroModel.calculateSigma();
}

template <typename T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_fill_M0_afterUpdateMrs(const double & newVal, const size_t & r, const size_t & s){
    _zeroModel.oldM = _zeroModel.M;
    // update armadillo matrix, always divide by m
    _zeroModel.M(r, s) = newVal / this->_m[0]->template value<double>();

    // calculate Sigma0
    _zeroModel.calculateSigma();
}

template <typename T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_fill_M0_afterUpdate_m(const double & newVal, const double & oldVal){
    _zeroModel.oldM = _zeroModel.M;
    // update armadillo matrix -> first multiply out effect of old m, then divide by new m
    _zeroModel.M *= (oldVal / newVal);

    // calculate Sigma0
    _zeroModel.calculateSigma();
}

template <typename T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_fill_M1(){
    // calculate Sigma1
    _stretchSigma1();

    // calculate M1
    _oneModel.calculateM();
}

template <typename T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::initPriorWithHyperPrior(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, std::vector<std::shared_ptr<TObservationsBase> > & observations){
    // we need mus (1D), m (single), Mrr (1D), Mrs (1D), z (1D) and rho (1D)
    this->_checkSizeParams(params.size(), 6);
    this->_checkSizeObservations(observations.size(), 0);

    // initialize parameters: mus, m, M, z, rho
    this->_mus = {params[0]};
    this->_m = {params[1]};
    this->_Mrr = {params[2]};
    this->_Mrs = {params[3]};
    this->_z = params[4];
    _rhos = params[5];

    // set K = 1 (actually we have 2 components, but one of them will be treated in a special way (stretching), such that we only use 1 component from base class)
    this->_K = 1;
}

template <typename T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_checkMinMaxOfHyperPrior(){
    // parameter _Mrr and _m should always be > 0 -> re-set boundaries of _Mrr and _m
    TPriorMultivariateNormalMixedModelWithHyperPrior<T>::_checkMinMaxOfHyperPrior();

    // parameter _rho should always be > 0 -> re-set boundaries of _rho
    _rhos->reSetBoundaries(false, true, "0.", toString(std::numeric_limits<double>::max()), false, true);
    // parameter _z should always be between 0 and 1 -> re-set boundaries of _z (base class sets it to 0, because we set K=1 for it)
    this->_z->reSetBoundaries(false, false, "0", "1", true, true);
}

template <class T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::initializeStorageOfPriorParameters() {
    TPriorMultivariateNormalMixedModelWithHyperPrior<T>::initializeStorageOfPriorParameters();

    // in addition: set storage for rho
    const std::shared_ptr<TNamesEmpty> & dimName = this->_getDimensionName();
    _rhos->initializeStorageBasedOnPrior({static_cast<size_t>(this->_D)}, {dimName});

    // initialize armadillo matrices
    _zeroModel.init(this->_D);
    _oneModel.init(this->_D);

    // fill M0 from current parameters and calculate M1
    _fill_M0();
    _fill_M1();
}

template <class T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::constructDAG(TDAG & DAG, TDAG & temporaryDAG){
    // construct vector of prior parameters
    std::vector<std::shared_ptr<TMCMCObservationsBase>> params;
    params.push_back(this->_mus[0]);
    params.push_back(this->_m[0]);
    params.push_back(this->_Mrr[0]);
    params.push_back(this->_Mrs[0]);
    params.push_back(this->_z);
    params.push_back(_rhos);

    this->_constructDAG(DAG, temporaryDAG, params);
}

template <class T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::typesAreCompatible(bool throwIfNot) {
    // will check parameter and _mu, _m, _Mrr, _Mrs and _z
    TPriorMultivariateNormalMixedModelWithHyperPrior<T>::typesAreCompatible(throwIfNot);

    // in addition: check if rho have expected type (must be floating point)
    this->_checkTypesOfPriorParameters(_rhos, throwIfNot, true, false, false);
    _rhos->typesAreCompatible(throwIfNot);
}

template <typename T> double TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_calcDoubleSum_oldMinNew_M1(const std::shared_ptr<TMCMCObservationsBase> & paramToUse, const TRange & row){
    // calculates sum_s ( (sum_r M1[r,s] * (param[r] - mu[r]))^2 - (sum_r M1[r,s]' * (param_r - mu_r))^2 )
    // used for updates where M1 has changed -> i.e. update of m1, mrr1, mrs1 and rho
    double outerSum = 0.;
    for (size_t s = 0; s < this->_D; s++) {
        double innerSum = 0.;
        double innerSumOld = 0.;
        for (size_t r = s; r < this->_D; r++) {
            // take value for all data elements
            double valueMinMu = paramToUse->value<T>(row.first + r*row.increment) - this->_mus[0]->template value<double>(r);
            innerSum += _oneModel.M(r, s) * valueMinMu;
            innerSumOld += _oneModel.oldM(r, s) * valueMinMu;
        }
        outerSum += innerSumOld * innerSumOld - innerSum * innerSum;
    }

    return outerSum;
}

template <typename T>double TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_calcDoubleSum_M1(const std::shared_ptr<TMCMCObservationsBase> & data, const TRange & row){
    // calculates sum_s ( (sum_r M1[r,s] * (param[r] - mu[r]))^2 )
    // used for calculating the log prior density of a given parameter value, for the one-model
    double outerSum = 0;
    for (size_t s = 0; s < this->_D; s++) {
        double innerSum = 0.;
        for (size_t r = s; r < this->_D; r++) {
            // take value for all data elements
            innerSum += _oneModel.M(r, s) * (data->value<T>(row.first + r*row.increment) - this->_mus[0]->template value<double>(r));
        }
        outerSum += innerSum * innerSum;
    }

    return outerSum;
}

template <typename T> double TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_calcSumLogMrr1(){
    // calculates sum_0^D log(M1[d,d])
    // used for calculating the log prior density of a given parameter value or oldValue, for the one-model
    double sumlogMrr = 0;
    for (size_t d = 0; d < this->_D; d++) {
        sumlogMrr += log(_oneModel.M(d, d));
    }

    return sumlogMrr;
}

template <typename T> double TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_calcLogPriorDensity_M1_vec(const std::shared_ptr<TMCMCObservationsBase> & data, const TRange &row){
    // calculates the log prior density of a given parameter value, for the one-model (z = 1)
    double sumlogMrr1 = _calcSumLogMrr1();
    double doubleSum = _calcDoubleSum_M1(data, row);
    return sumlogMrr1 + this->_minusDdiv2Log2Pi - 0.5 * doubleSum;
}

template <typename T> double TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_getLogPriorDensity_vec(const std::shared_ptr<TMCMCObservationsBase> & data, const size_t & index){
    // calculates the log prior density of a given parameter value

    // index corresponds to index in linear array
    // -> calculate index in multi-array:
    std::vector<size_t> subscript_updatedParam = data->getSubscripts(index);
    size_t n = subscript_updatedParam[0];
    TRange row = data->get1DSlice(1, {n, 0}); // one row of Tdata

    if (this->_z->template value<size_t>(n) == 1){ // z = 1
        return _calcLogPriorDensity_M1_vec(data, row);
    } else { // z = 0
        return this->_calcLogPriorDensity(data, row, this->_mus[0], this->_m[0], this->_Mrr[0], this->_Mrs[0]);
    }
}

template <typename T> double TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_calcDoubleSumOld_M1(const std::shared_ptr<TMCMCParameterBase> & data, const TRange & row, const size_t & indexInRowThatChanged){
    // calculates sum_s ( (sum_r M1[r,s] * (paramOld[r] - mu[r]))^2 )
    // used for calculating the log prior density of a given parameter oldValue, for the one-model
    double outerSum = 0;
    for (size_t s = 0; s < this->_D; s++) {
        double innerSum = 0.;
        for (size_t r = s; r < this->_D; r++) {
            // get correct value for data (old or new)
            double value;
            if (r == indexInRowThatChanged) {
                value = data->oldValue<T>(row.first + r * row.increment);
            } else {
                value = data->value<T>(row.first + r * row.increment);
            }
            // add to sum
            innerSum += _oneModel.M(r, s) * (value - this->_mus[0]->template value<double>(r));
        }
        outerSum += innerSum * innerSum;
    }

    return outerSum;
}

template <typename T> double TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_calcLogPriorDensityOld_M1_vec(const std::shared_ptr<TMCMCParameterBase> & data, const TRange &row, const size_t & col){
    // calculates the log prior density of a given parameter oldValue, for the one-model (z = 1)
    double sumlogMrr1 = _calcSumLogMrr1();
    double doubleSum = _calcDoubleSumOld_M1(data, row, col);
    return sumlogMrr1 + this->_minusDdiv2Log2Pi - 0.5 * doubleSum;
}

template <typename T> double TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_getLogPriorDensityOld_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index){
    // index corresponds to index in linear array
    // -> calculate index in multi-array:
    std::vector<size_t> subscript_updatedParam = data->getSubscripts(index);
    size_t n = subscript_updatedParam[0];
    TRange row = data->get1DSlice(1, {n, 0}); // one row of Tdata

    if (this->_z->template value<size_t>(n) == 1){ // z = 1
        return _calcLogPriorDensityOld_M1_vec(data, row, subscript_updatedParam[1]);
    } else { // z = 0
        return this->_calcLogPriorDensityOld(data, row, subscript_updatedParam[1], this->_mus[0], this->_m[0], this->_Mrr[0], this->_Mrs[0]);
    }
}

template <typename T> double TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_calcLogPriorRatio_M1_vec(const std::shared_ptr<TMCMCParameterBase> & data, const TRange &row, const size_t & d){
    // calculates the log prior ratio of a given parameter value, for the one-model (z = 1)

    // store values
    double oldParam = data->oldValue<T>(row.first + d*row.increment);
    double newParam = data->value<T>(row.first + d*row.increment);

    // calculate simplified hastings ratio
    double sumM = _calcSumM1_dr_squared(d);
    size_t min_r_DMin1 = std::min(d+1, static_cast<size_t>(this->_D-1));
    double sum = _calcDoubleSum_M1_updateParam(data, row, d, min_r_DMin1);
    double tmp = sumM *(oldParam*oldParam - newParam*newParam + 2.*this->_mus[0]->template value<double>(d)*(newParam - oldParam)) + 2.*(oldParam - newParam) * sum;

    return 0.5 * tmp;
}

template <typename T> double  TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_getLogPriorRatio_vec(const std::shared_ptr<TMCMCParameterBase> & data, const size_t & index){
    // calculates log prior ratio for a given parameter value

    // index corresponds to index in linear array
    // -> calculate index in multi-array:
    std::vector<size_t> subscript_updatedParam = data->getSubscripts(index);
    size_t n = subscript_updatedParam[0];
    size_t d = subscript_updatedParam[1];
    TRange row = data->get1DSlice(1, {n, 0}); // one row of Tdata

    if (this->_z->template value<size_t>(n) == 1){
        return _calcLogPriorRatio_M1_vec(data, row, d);
    } else {
        return this->_calcLogPriorRatio(data, row, d, this->_mus[0], this->_m[0], this->_Mrr[0], this->_Mrs[0]);
    }
}

template <typename T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::updateParams(){
    //  update mus, m, Mrr, Mrs
    TPriorMultivariateNormalMixedModelWithHyperPrior<T>::updateParams();

    // update rhos
    for (size_t d = 0; d < this->_D; d++){
        if (_rhos->update(d))
            _updateRho(d);
    }
}

template <typename T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_updateMrr(const size_t & k, const size_t & r) {
    // compute log(Hastings ratio)
    double logH = this->_calcLogHUpdateMrr(k, r);
    // accept or reject
    if(!this->_Mrr[k]->acceptOrReject(r, logH)){
        // rejected -> reset M0, Sigma0, M1 and Sigma1
        _zeroModel.reset();
        _oneModel.reset();
    }
}

template <typename T> double TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_calcLLRatioUpdateM1(const size_t & N, const double & sum){
    // used in updates that change M1 -> i.e. m, mrr, mrs and rho
    // calculates prior ratio for any update that changes M1

    // calculate sum_r^D( log(mrr1'/mrr1) ) for 1-model
    double sumlogMrr_ratio = 0.;
    for (size_t d = 0; d < this->_D; d++){
        sumlogMrr_ratio += log(_oneModel.M(d,d) / _oneModel.oldM(d,d));
    }
    return N*sumlogMrr_ratio + 0.5 * sum;
}

template <typename T> double TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_calcLLUpdateMrr(const size_t & k, const size_t & r){
    // compute log likelihood
    double LL = 0;

    // store for calculations
    double oldMrr = this->_Mrr[k]->template oldValue<double>(r);
    double newMrr = this->_Mrr[k]->template value<double>(r);

    // update M0, Sigma0, Sigma1 and M1
    _fill_M0_afterUpdateMrr(newMrr, r);
    _fill_M1();

    double sum_0Model = 0.;
    double sum_1Model = 0.;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        size_t N = paramToUse->dimensions()[0];

        double sumParam_nd_minus_mu_d = 0.;
        size_t counter_z1 = 0;
        for (size_t n = 0; n < N; n++) {
            TRange row = paramToUse->get1DSlice(1, {n, 0});

            if (this->_z->template value<size_t>(n) == 1){ // z = 1
                sum_1Model += _calcDoubleSum_oldMinNew_M1(paramToUse, row);
                counter_z1++;
            } else{ // z = 0
                sum_0Model += this->_calcDoubleSum_updateMrr(paramToUse, row, r, sumParam_nd_minus_mu_d, this->_mus[k], this->_Mrs[k]);
            }
        }

        size_t counter_z0 = N - counter_z1;
        double ratio_0Model = this->_calcLLRatioUpdateMrr(counter_z0, oldMrr, newMrr, sum_0Model, sumParam_nd_minus_mu_d, this->_m[k]);
        double ratio_1Model = _calcLLRatioUpdateM1(counter_z1, sum_1Model);

        LL += ratio_0Model + ratio_1Model;
    }
    return LL;
}

template <typename T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_updateMrs(const size_t & k, const size_t & r, const size_t & s) {
    // compute log(Hastings ratio)
    double logH = this->_calcLogHUpdateMrs(k, r, s);
    // accept or reject
    if(!this->_Mrs[k]->acceptOrReject(this->_linearizeIndex_Mrs(r, s), logH)){
        // rejected -> reset M0, Sigma0, M1 and Sigma1
        _zeroModel.reset();
        _oneModel.reset();
    }
}

template <typename T> double TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_calcLLUpdateMrs(const size_t & k, const size_t & r, const size_t & s) {
    // compute log likelihood
    double LL = 0;

    // store for calculations
    size_t linearIndex = this->_linearizeIndex_Mrs(r, s);
    double oldMrs = this->_Mrs[k]->template oldValue<double>(linearIndex);
    double newMrs = this->_Mrs[k]->template value<double>(linearIndex);

    // update M0, Sigma0, Sigma1 and M1
    _fill_M0_afterUpdateMrs(newMrs, r, s);
    _fill_M1();

    double sum_0Model = 0.;
    double sum_1Model = 0.;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        size_t N = paramToUse->dimensions()[0];

        double sumParam_nd_minus_mu_d = 0.;
        size_t counter_z1 = 0;
        for (size_t n = 0; n < N; n++) {
            TRange row = paramToUse->get1DSlice(1, {n, 0});

            if (this->_z->template value<size_t>(n) == 1){ // z = 1
                sum_1Model += _calcDoubleSum_oldMinNew_M1(paramToUse, row);
                counter_z1++;
            } else{ // z = 0
                sum_0Model += this->_calcDoubleSum_updateMrs(paramToUse, row, r, s, sumParam_nd_minus_mu_d, this->_mus[k], this->_Mrr[k], this->_Mrs[k]);
            }
        }

        double ratio_0Model = this->_calcLLRatioUpdateMrs(oldMrs, newMrs, sum_0Model, sumParam_nd_minus_mu_d, this->_m[k]);
        double ratio_1Model = _calcLLRatioUpdateM1(counter_z1, sum_1Model);

        LL += ratio_0Model + ratio_1Model;
    }
    return LL;
}

template <typename T> double TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_calcLLUpdateMu(const size_t & k, const size_t & r) {
    // update mu

    // compute log likelihood
    double LL = 0.;

    double oldMu = this->_mus[k]->template oldValue<double>(r);
    double newMu = this->_mus[k]->template value<double>(r);
    double mrr = this->_Mrr[k]->template value<double>(r);

    size_t min_r_DMin1 = std::min(r+1, static_cast<size_t>(this->_D-1));

    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        size_t N = paramToUse->dimensions()[0];

        double sum0 = 0.;
        double sumParam0 = 0.;
        double sum1 = 0.;
        double sumParam1 = 0.;
        size_t counter_z0 = 0;
        for (size_t n = 0; n < N; n++) {
            TRange row = paramToUse->get1DSlice(1, {n, 0});

            if (this->_z->template value<size_t>(n) == 0) { // z = 0
                // calculate simplified ratio
                sum0 += this->_calcDoubleSum_updateMu(paramToUse, row, r, min_r_DMin1, this->_mus[k], this->_Mrr[k], this->_Mrs[k]);
                sumParam0 += paramToUse->value<T>(row.first + r * row.increment);
                counter_z0++;
            } else {
                sum1 += _calcDoubleSum_M1_updateMu(paramToUse, row, r, min_r_DMin1);
                sumParam1 += paramToUse->value<T>(row.first + r * row.increment);
            }
        }
        double sumM0 = this->_calcSumM_dr_squared(mrr, r, this->_Mrs[k]);
        double sumM1 = _calcSumM1_dr_squared(r);

        size_t counter_z1 = N - counter_z0;
        LL += 0.5 * ( counter_z1 * (oldMu*oldMu - newMu*newMu) * sumM1 + 2.*(newMu - oldMu) * (sumM1*sumParam1 + sum1) );
        LL += 1. / (2. * this->_m[k]->template value<double>() * this->_m[k]->template value<double>()) * ( counter_z0 * (oldMu*oldMu - newMu*newMu) * sumM0 + 2.*(newMu - oldMu) * (sumM0*sumParam0 + sum0) );
    }
    return LL;
}

template <typename T> double TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_calcSumM1_dr_squared(const size_t & r) {
    // calculates sum_d^r M1(d,r)^2
    // used for updates of param (getLogPriorRatio_vec) and of mu1
    double sumM = 0.;
    for (size_t d = 0; d <= r; d++){
        sumM += _oneModel.M(r, d) * _oneModel.M(r, d);
    }
    return sumM;
}

template <typename T>
double TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_calcDoubleSum_M1_updateParam(const std::shared_ptr<TMCMCObservationsBase> & data, const TRange & row, const size_t & indexThatChanged, const size_t & min_r_DMin1){
    // calculates sum_s_0^min(r, D-1) ( M1[d,s] * (sum_r_r=s^D M1[r,s] * (param[r] - mu1[r]))^2 )
    // used in updates of param (getPriorRatio_vec)
    double sum = 0.;
    // go over all s = 1:min(r, D-1)
    for (size_t s = 0; s < min_r_DMin1; s++) {
        // go over all r = s:D, r != d
        double tmp = 0.;
        for (size_t r = s; r < this->_D; r++) {
            if (r == indexThatChanged) continue;
            tmp += _oneModel.M(r,s) * (data->value<T>(row.first + r * row.increment) - this->_mus[0]->template value<double>(r));
        }
        sum += tmp * _oneModel.M(indexThatChanged, s);
    }

    return sum;
}

template <typename T>
double TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_calcDoubleSum_M1_updateMu(const std::shared_ptr<TMCMCObservationsBase> & paramToUse, const TRange & row, const size_t & indexThatChanged, const size_t & min_r_DMin1){
    // calculates sum_s_0^min(r, D-1) ( M1[d,s] * (sum_r_r=s^D M1[r,s] * (param[r] - mu1[r]))^2 )
    // used in updates of mu1
    double sum = 0.;
    // go over all s = 1:min(r, D-1)
    for (size_t s = 0; s < min_r_DMin1; s++) {
        // go over all r = s:D, r != d
        double tmp = 0.;
        for (size_t r = s; r < this->_D; r++) {
            if (r == indexThatChanged) continue;
            tmp += _oneModel.M(r,s) * (paramToUse->value<T>(row.first + r * row.increment) - this->_mus[0]->template value<double>(r));
        }
        sum += tmp * _oneModel.M(indexThatChanged, s);
    }

    return sum;
}

template <typename T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_updateM(const size_t & k) {
    // compute log(Hastings ratio)
    double logH = this->_calcLogHUpdateM(k);
    // accept or reject
    if(!this->_m[k]->acceptOrReject(logH)){
        // rejected -> reset M0, Sigma0, M1 and Sigma1
        _zeroModel.reset();
        _oneModel.reset();
    }
}

template <typename T> double TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_calcLLUpdateM(const size_t & k) {
    // compute log likelihood
    double LL = 0;

    // store for calculations
    double oldM = this->_m[k]->template oldValue<double>();
    double newM = this->_m[k]->template value<double>();

    // update M0, Sigma0, Sigma1 and M1
    _fill_M0_afterUpdate_m(newM, oldM);
    _fill_M1();

    double sum0 = 0.;
    double sum1 = 0.;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        size_t N = paramToUse->dimensions()[0];

        size_t counter_z1 = 0;
        for (size_t n = 0; n < N; n++) {
            TRange row = paramToUse->get1DSlice(1, {n, 0});

            if (this->_z->template value<size_t>(n) == 1){ // z = 1
                sum1 += _calcDoubleSum_oldMinNew_M1(paramToUse, row);
                counter_z1++;
            } else{ // z = 0
                sum0 += this->_calcDoubleSum(paramToUse, row, this->_mus[k], this->_Mrr[k], this->_Mrs[k]);
            }
        }

        size_t counter_z0 = N - counter_z1;
        double ratio_0Model = this->_calcLLRatioUpdateM(counter_z0, oldM, newM, sum0);
        double ratio_1Model = _calcLLRatioUpdateM1(counter_z1, sum1);

        LL += ratio_0Model + ratio_1Model;
    }
    return LL;
}


template <typename T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_updateRho(const size_t & d){
    // compute log(Hastings ratio)
    double logH = _calcLogHUpdateRho(d);
    // accept or reject
    if (!_rhos->acceptOrReject(d, logH)){
        // rejected -> reset M1 and Sigma1 (but not M0 and Sigma0, as these didn't change)
        _oneModel.reset();
    }
}

template <typename T> double TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_calcLogHUpdateRho(const size_t & d) {
    // compute log likelihood
    double LL = _calcLLUpdateRho(d);
    // compute prior
    double logPrior = _rhos->getLogPriorRatio(d);
    return LL + logPrior;
}

template <typename T> double TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_calcLLUpdateRho(const size_t &d) {
    // compute log likelihood
    double LL = 0;

    // update Sigma1 and M1
    _fill_M1();

    double sum_1Model = 0.;
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        size_t N = paramToUse->dimensions()[0];

        size_t counter_z1 = 0;
        for (size_t n = 0; n < N; n++) {
            TRange row = paramToUse->get1DSlice(1, {n, 0});

            if (this->_z->template value<size_t>(n) == 1){ // z = 1
                sum_1Model += _calcDoubleSum_oldMinNew_M1(paramToUse, row);
                counter_z1++;
            }
        }

        LL += _calcLLRatioUpdateM1(counter_z1, sum_1Model);;
    }
    return LL;
}

template <typename T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_calcDoubleSum_updateZ(const std::shared_ptr<TMCMCObservationsBase> & paramToUse, const TRange & row, double & outerSum_0Model, double & outerSum_1Model) {
    // calculates sum_s ( (sum_r M0[r,s]*(param_r - mu0_r)^2 ) as well as sum_s( (sum_r M1[r,s]*(param_r - mu1_r)^2) )
    // used when updating z
    // directly fills outerSum_0Model and outerSum_1Model
    outerSum_0Model = 0;
    outerSum_1Model = 0;
    for (size_t s = 0; s < this->_D; s++) {
        double innerSum_0Model = 0.;
        double innerSum_1Model = 0.;
        for (size_t r = s; r < this->_D; r++) {
            // take value for all data elements
            double value = paramToUse->value<T>(row.first + r*row.increment) - this->_mus[0]->template value<double>(r);
            innerSum_0Model += _zeroModel.M(r, s) * value;
            innerSum_1Model += _oneModel.M(r, s) * value;
        }
        outerSum_0Model += innerSum_0Model * innerSum_0Model;
        outerSum_1Model += innerSum_1Model * innerSum_1Model;
    }
}

template <typename T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_calcSumLogMrr_updateZ(double & sumlogMrr0, double & sumlogMrr1) {
    // calculates sum_r log(Mrr1) and sum_r log(Mrr0)
    // used when updating z
    // directly fills sumlogMrr0 an sumlogMrr1
    sumlogMrr0 = 0.;
    sumlogMrr1 = 0.;
    for (size_t d = 0; d < this->_D; d++){
        sumlogMrr0 += log(_zeroModel.M(d,d));
        sumlogMrr1 += log(_oneModel.M(d,d));
    }
}

template <typename T> double TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_calcLLUpdateZ(const size_t &n) {
    // compute log likelihood
    double LL = 0;

    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange row = paramToUse->get1DSlice(1, {n, 0});

        // calculate double sums
        double outerSum_0Model = 0.;
        double outerSum_1Model = 0.;
        _calcDoubleSum_updateZ(paramToUse, row, outerSum_0Model, outerSum_1Model);

        // calculate sums of mrr
        double sumlogMrr0 = 0.;
        double sumlogMrr1 = 0.;
        _calcSumLogMrr_updateZ(sumlogMrr0, sumlogMrr1);

        // calculate P(z = 0)
        double PZ0 = sumlogMrr0 - 0.5 * outerSum_0Model;

        // calculate P(z = 1)
        double PZ1 = sumlogMrr1 - 0.5 * outerSum_1Model;

        if (this->_z->template value<size_t>(n) == 1){ // we updated z=0 -> z=1
            LL += PZ1 - PZ0;
        } else { // we updated z=1 - > z=0
            LL += PZ0 - PZ1;
        }
    }
    return LL;
}

template <class T> double TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::getLogPriorDensityFull(const std::shared_ptr<TMCMCObservationsBase> & data) {
    // returns sum of log-density of array + log density of mu + log density of m + log density of mrr + log density of mrs
    double full = TPriorMultivariateNormalMixedModelWithHyperPrior<T>::getLogPriorDensityFull(data);
    full += _rhos->getLogPriorDensityFull();

    return full;
}

template <class T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::simulateUnderPrior(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters) {
    // first fill M0 from current parameters and calculate M1: might have changed in the meantime
    _fill_M0();
    _fill_M1();

    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        if (!paramToUse->hasFixedInitialValue()) { // only simulate if parameter has not a fixed value
            assert(paramToUse->dimensions()[1] == this->_D); // number of columns of data must match D

            // get all L's ready (don't want to compute them for each row again)
            std::vector<arma::mat> Ls;
            Ls.emplace_back(arma::chol(_zeroModel.Sigma, "lower"));
            Ls.emplace_back(arma::chol(_oneModel.Sigma, "lower"));

            for (size_t n = 0; n < paramToUse->dimensions()[0]; n++) { // go over all rows
                TRange row = paramToUse->get1DSlice(1, {n, 0});
                size_t k = this->_z->template value<size_t>(n);
                this->_simulateMultivariateNormal(paramToUse, row.first, Ls[k], this->_mus[0], RandomGenerator);
            }
        }
    }
}

// EM

template <class T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::estimateInitialPriorParameters(TLog * Logfile) {
    // run EM to initialize prior parameters, and pretend that the two variances are independent
    TPriorMultivariateNormalMixedModelWithHyperPrior<T>::estimateInitialPriorParameters(Logfile);

    // now adjust to model: set larger variance to variance of 1-model
    _setAllParametersAfterEM(Logfile);
}

template <class T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_switchEMLabels(TLog * Logfile) {
    // switch z
    if (!this->_z->hasFixedInitialValue()) {
        for (size_t i = 0; i < this->_z->totalSize(); i++) {
            this->_z->set(i, 1 - this->_z->template value<size_t>(i)); // set to opposite
        }
        // switch prior on z
        this->_z->switchPriorClassificationAfterEM(Logfile);
    }
}

template <class T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_setRhoAfterEM(const arma::vec & Lambda_0, const arma::vec & Lambda_1) {
    // calculate rho
    if (!_rhos->hasFixedInitialValue()) {
        arma::vec rho = Lambda_1 / Lambda_0 - 1.;
        for (size_t d = 0; d < this->_D; d++) {
            if (rho(d) <= 0.) {
                rho(d) = 0.00001;
            }
            _rhos->set(d, rho(d));
        }
    }
}

template <class T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_setAllParametersAfterEM(TLog * Logfile) {
    // first fill zero-model armadillo matrices
    if (!this->_Mrr[0]->hasFixedInitialValue() && !this->_Mrs[0]->hasFixedInitialValue() && !this->_m[0]->hasFixedInitialValue()) {
        this->_fillArmadilloMFromParametersM(_zeroModel.M, this->_Mrr[0], this->_Mrs[0], this->_m[0]);
        _zeroModel.calculateSigma();
    }  // else don't set them, do nothing

    // do eigendecomposition
    arma::vec Lambda_0;
    arma::mat Q0;
    arma::eig_sym(Lambda_0, Q0, _zeroModel.Sigma);

    arma::vec Lambda_1;
    arma::mat Q1;
    arma::eig_sym(Lambda_1, Q1, _oneModel.Sigma);

    // calculate norm of eigenvalues
    double norm0 = arma::norm(Lambda_0);
    double norm1 = arma::norm(Lambda_1);

    if (norm0 > norm1){ // we have to switch labels
        if (!this->_Mrr[0]->hasFixedInitialValue() && !this->_Mrs[0]->hasFixedInitialValue() && !this->_m[0]->hasFixedInitialValue()) {
            // M0 (and possibly rho) should be initialized
            // switch M0 and Sigma0 with M1 and Sigma1
            arma::mat tempM1 = _oneModel.M;
            arma::mat tempSigma1 = _oneModel.Sigma;
            _oneModel.M = _zeroModel.M;
            _oneModel.Sigma = _zeroModel.Sigma;
            _zeroModel.M = tempM1;
            _zeroModel.Sigma = tempSigma1;

            // fill new M0 parameters
            this->_fillParametersMFromSigma(_zeroModel.Sigma, this->_Mrr[0], this->_Mrs[0], this->_m[0]);

            // set rho (pass Lambda_0 and Lambda_1 in opposite order as we switched them)
            _setRhoAfterEM(Lambda_1, Lambda_0);

            // switch z
            _switchEMLabels(Logfile);
        } else if (!_rhos->hasFixedInitialValue()){
            // M0 are fix, rhos should be initialized
            // -> set rho to a very small value
            for (size_t d = 0; d < this->_D; d++){
                _rhos->set(d, 0.00001);
            }

            // don't switch z, we didn't change labels
        } else {
            // both M0 and rhos are fix -> should never be changed in EM, and therefore we should never get here!
            throw std::runtime_error("In function 'template <class T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_setAllParametersAfterEM():' Should never have gotten here! Both M0 and rho are fix - the condition that M1 must be larger than M0 should never be violated!");
        }
    } else {
        // labels are ok -> simply set rho
        _setRhoAfterEM(Lambda_0, Lambda_1);
    }

    // re-calculate M's and Sigma's to make sure everything is initialized
    _fill_M0();
    _fill_M1();
}

template <class T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_setMusToMLE(){
    // calculate MLE of mus
    std::vector<double> means = this->_calculateMLEMu();

    // set mus
    if (!this->_mus[0]->hasFixedInitialValue()) {
        for (size_t d = 0; d < this->_D; d++) {
            this->_mus[0]->set(d, means[d]);
        }
    }
}

template <class T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_setMToMLE(){
    // calculate MLE of M (based on classification by z)

    // initialize data for sums: vector of size K with armadillo matrices of dimensions D times D
    std::vector<arma::mat> sums(2, arma::mat(this->_D, this->_D, arma::fill::zeros));

    std::vector<double> totalNumElements(2, 0.);
    for (auto &param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        for (size_t n = 0; n < paramToUse->dimensions()[0]; n++) { // go over all rows (N)
            TRange row = paramToUse->get1DSlice(1, {n, 0});
            // get component
            size_t k = this->_z->template value<size_t>(n);
            // add to sum
            this->_addToSumMLESigma(sums[k], row, paramToUse, this->_mus[0]);
            totalNumElements[k]++;
        }
    }

    // normalize
    for (size_t k = 0; k < 2; k++) {
        this->_normalizeSumMLESigma(sums[k], totalNumElements[k]);
    }

    // fill parameters of M0
    if (!this->_Mrr[0]->hasFixedInitialValue() && !this->_Mrs[0]->hasFixedInitialValue() && !this->_m[0]->hasFixedInitialValue()) {
        this->_fillParametersMFromSigma(sums[0], this->_Mrr[0], this->_Mrs[0], this->_m[0]);
    } // else don't set them, do nothing

    // fill M1 (only if M0 or rho should be initialized)
    if ((!this->_Mrr[0]->hasFixedInitialValue() && !this->_Mrs[0]->hasFixedInitialValue() && !this->_m[0]->hasFixedInitialValue()) || !_rhos->hasFixedInitialValue()) {
        _oneModel.Sigma = sums[1];
        _oneModel.calculateM();
    }
}

template <class T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_initializeEMParams_basedOnCutoff(const arma::mat & M) {
    // before EM: initialize EM parameters
    // this is the first round: go over all data, calculate log density under overall mean and variance
    // and assign a certain percentage of x that have the highest log density to model 0 and the rest to model 1

    // calculate log density (mean over all parameters)
    // set M1 to M, to simplify calculation.
    _oneModel.M = M;
    std::vector<double> logDens(this->_z->totalSize(), 0.);
    for (auto &param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        for (size_t n = 0; n < paramToUse->dimensions()[0]; n++) { // go over all rows (N)
            TRange row = paramToUse->get1DSlice(1, {n, 0});
            logDens[n] += _calcLogPriorDensity_M1_vec(paramToUse, row);
        }
    }
    // normalize
    for (auto & it : logDens){
        it /= this->_parameters.size();
    }

    // define cutoff: 10% of all x will go into model 1 and 90% into model 0
    double cutoffPercentage = 0.1;
    // sort densities and define cutoff value
    std::vector<double> sortedLogDens = logDens;
    std::sort(sortedLogDens.begin(), sortedLogDens.end());
    size_t cutoffIndex = this->_z->totalSize() * cutoffPercentage;
    if (cutoffIndex < 1){
        cutoffIndex = 1; // avoid cases where all have z = 0
    }
    double cutoffValue = sortedLogDens[cutoffIndex];

    // define components
    if (!this->_z->hasFixedInitialValue()) {
        // go over the data again and assign components
        for (size_t i = 0; i < this->_z->totalSize(); i++) {
            if (logDens[i] >= cutoffValue){ // z = 0
                this->_z->set(i, 0);
            } else { // z = 1
                this->_z->set(i, 1);
            }
        }
    }
    // re-calculate variances for these components
    _setMToMLE();
}

template <class T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::_initializeEMParams_refinement() {
    // before EM: initialize EM parameters
    // this is the second round: go over all data again, calculate log density under overall mean and the two variances we've calculated in the first round
    // assign x that have a higher log density under the 0-model to z=0 and the rest to z=1

    // re-calculate log density with new variances
    std::vector<double> logDens0(this->_z->totalSize(), 0.);
    std::vector<double> logDens1(this->_z->totalSize(), 0.);
    for (auto &param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        for (size_t n = 0; n < paramToUse->dimensions()[0]; n++) { // go over all rows (N)
            TRange row = paramToUse->get1DSlice(1, {n, 0});
            logDens0[n] += this->_calcLogPriorDensity(paramToUse, row, this->_mus[0], this->_m[0], this->_Mrr[0], this->_Mrs[0]);
            logDens1[n] += _calcLogPriorDensity_M1_vec(paramToUse, row);
        }
    }
    // normalize
    for (size_t n = 0; n < this->_z->totalSize(); n++) {
        logDens0[n] /= this->_parameters.size();
        logDens1[n] /= this->_parameters.size();
    }

    // re-assign components to the model that maximizes the log density
    if (!this->_z->hasFixedInitialValue()) {
        // go over the data again and assign components
        for (size_t i = 0; i < this->_z->totalSize(); i++) {
            if (logDens0[i] >= logDens1[i]){ // z = 0
                this->_z->set(i, 0);
            } else { // z = 1
                this->_z->set(i, 1);
            }
        }
    }

    // re-calculate variances for these components
    _setMToMLE();
}

template <class T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::initializeEMParameters() {
    // calculate overall mean and Sigma
    _setMusToMLE();
    arma::mat Sigma = this->_calculateMLESigma(this->_mus[0]);
    arma::mat M = this->_calculateMFromSigma(Sigma);

    // first round: go over all data and assign a certain percentage to z=0 and the rest to z=1 based on the log density under overall mean and variance
    _initializeEMParams_basedOnCutoff(M);

    // second round: go over all data again and assign z to the model that maximizes the log density (based on the variances we've initialized in the first round)
    _initializeEMParams_refinement();
}

template <class T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::calculateEmissionProbabilities(const size_t & Index, TDataVector<double, size_t> & Emission){
    // initialize emissions with zero
    for (size_t k = 0; k < 2; k++) {
        Emission[k] = 0.;
    }

    // go over all parameters and over all components and sum emission probability
    // we want emission not in log, but we will use the log to sum and then de-log in the end
    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        // log P(x | z = k, theta)
        TRange row = paramToUse->get1DSlice(1, {Index, 0}); // one row of param
        Emission[0] += this->_calcLogPriorDensity(paramToUse, row, this->_mus[0], this->_m[0], this->_Mrr[0], this->_Mrs[0]);
        Emission[1] += _calcLogPriorDensity_M1_vec(paramToUse, row);
    }

    // get rid of log
    for (size_t k = 0; k < 2; k++) {
        Emission[k] = exp(Emission[k]);
    }
};

template <class T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::prepareEMParameterEstimationInitial() {
    // initialize data: per component, we need to store two sums
    // 2) sum_{i=1}^N weights_ki * sum_{p=1}^P (x_pi - mu_k)(x_pi - mu_k)^T
    // 3) sum_{i=1}^N weights_ki
    this->_EM_update_Sigma_temp = new arma::mat[2];
    for (size_t i = 0; i < 2; i++){
        this->_EM_update_Sigma_temp[i] = arma::mat(this->_D, this->_D, arma::fill::zeros);
    }
    this->_EM_update_Weights_temp = new double[2];
}

template <class T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::prepareEMParameterEstimationOneIteration(){
    // fill data with zeros
    for (size_t i = 0; i < 2; i++){
        this->_EM_update_Sigma_temp[i].zeros();
        this->_EM_update_Weights_temp[i] = 0.;
    }
};

template <class T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::handleEMParameterEstimationOneIteration(const size_t & index, const TDataVector<double, size_t> & Weights){
    // calculate
    // sum_{p=1}^P sum_{i=1}^N (x_pi - mu_k)(x_pi - mu_k)^T

    // initialize data for sums
    arma::mat * sumXpiMinusMuSquare; // 3 dimensions: K times an Armadillo matrix with D times D dimensions
    sumXpiMinusMuSquare = new arma::mat [2];
    for (size_t k = 0; k < 2; k++){
        sumXpiMinusMuSquare[k] = arma::mat(this->_D, this->_D, arma::fill::zeros);
    }

    for (auto & param : this->_parameters) {
        std::shared_ptr<TMCMCObservationsBase> paramToUse = this->_getSharedPtrParam(param);
        TRange row = paramToUse->get1DSlice(1, {index, 0}); // one row of param
        for (size_t d = 0; d < this->_D; d++) {
            for (size_t k = 0; k < 2; k++) {
                for (size_t e = 0; e < this->_D; e++){
                    sumXpiMinusMuSquare[k](d, e) += (paramToUse->value<double>(row.first + d*row.increment) - this->_mus[0]->template value<double>(d))
                                                    * (paramToUse->value<double>(row.first + e*row.increment) - this->_mus[0]->template value<double>(e));
                }
            }
        }
    }

    // add to sums
    for (size_t k = 0; k < 2; k++) {
        // add to sum (x_pi - mu_k)(x_pi - mu_k)^T * weights_ki
        this->_handleEMMaximizationOneIteration_updateSigma(k, sumXpiMinusMuSquare, Weights);
        // add to sum weights
        this->_EM_update_Weights_temp[k] += Weights[k];
    }
}

template <class T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::finalizeEMParameterEstimationOneIteration(){
    // M-step: update parameters

    // 0-model: only if M0 should be initialized
    if (!this->_Mrr[0]->hasFixedInitialValue() && !this->_Mrs[0]->hasFixedInitialValue() && !this->_m[0]->hasFixedInitialValue()) {
        arma::mat Sigma;
        this->_updateEMParametersOneIteration_Sigma(Sigma, 0);

        this->_fillParametersMFromSigma(Sigma, this->_Mrr[0], this->_Mrs[0], this->_m[0]);
    } // else don't set them, do nothing

    // 1-model: only if either M0 or rho should be initialized
    if ((!this->_Mrr[0]->hasFixedInitialValue() && !this->_Mrs[0]->hasFixedInitialValue() && !this->_m[0]->hasFixedInitialValue()) || !_rhos->hasFixedInitialValue()) {
        arma::mat Sigma;
        this->_updateEMParametersOneIteration_Sigma(Sigma, 1);
        _oneModel.Sigma = Sigma;
        _oneModel.calculateM();
    }
}


template <class T> void TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<T>::finalizeEMParameterEstimationFinal() {
    // clean memory
    delete [] this->_EM_update_Sigma_temp;
    delete [] this->_EM_update_Weights_temp;
}

#endif // WITH_ARMADILLO