//
// Created by madleina on 01.02.21.
//

#include "TDAGBuilder.h"

//--------------------------------------------
// TMCMCUserInterface
//--------------------------------------------

TMCMCUserInterface::TMCMCUserInterface(TLog * logfile){
    _logfile = logfile;
};

TMCMCUserInterface::~TMCMCUserInterface() = default;

void TMCMCUserInterface::parseUserConfiguration(std::vector<std::shared_ptr<TParameterDefinition> > & paramDefs, std::vector<std::shared_ptr<TObservationDefinition> > & observationDefs, TParameters & parameters) {
    // first read parameter configuration file
    _readParamConfigFile(paramDefs, observationDefs, parameters);
    // then read file with initial values and jump sizes
    _readInitValFile(paramDefs, observationDefs, parameters);
    // now check if user specified any command line arguments concerning parameter configurations / initial values
    _parseCommandLineParamConfigs(paramDefs, observationDefs, parameters);
    _parseCommandLineParamInitVals(paramDefs, observationDefs, parameters);
}

void TMCMCUserInterface::_checkHeaderConfigFile(const std::string & filename, const std::vector<std::string> & actualColNames){
    std::vector<std::string> parsedColNames; // keep track of which colnames have been parsed -> check for duplicate colnames

    for (auto & colName : actualColNames) {
        // is the given colname valid?
        auto it = std::find(_expectedColNames.begin(), _expectedColNames.end(), colName);
        if (it != _expectedColNames.end()) { // colname is valid
            // did we parse this colname already (i.e. is it a duplicate)?
            auto it2 = std::find(parsedColNames.begin(), parsedColNames.end(), colName);
            if (it2 != parsedColNames.end()) // it is a duplicate -> throw
                throw  "Duplicate colname '" + colName + "' in config file '" + filename + "'!";
            parsedColNames.push_back(colName);
        } else
            throw "Invalid colname '" + colName + "' in config file '" + filename + "'!"; // colName is different from what is expected
    }

    // check if column with "name" exists
    auto it = std::find(parsedColNames.begin(), parsedColNames.end(), "name");
    if (it == parsedColNames.end())
        throw "Mandatory colname 'name' is missing in file '" + filename + "'!";
}

bool TMCMCUserInterface::_val1IsSmallerThanVal2(const std::string & val1, const std::string & val2, const std::string & type){
    if (type == "double") {
        auto val1T = convertString<double>(val1);
        auto val2T = convertString<double>(val2);
        return val1T < val2T;
    } else if (type == "float") {
        auto val1T = convertString<float>(val1);
        auto val2T = convertString<float>(val2);
        return val1T < val2T;
    } else if (type == "bool") {
        auto val1T = convertString<bool>(val1);
        auto val2T = convertString<bool>(val2);
        return val1T < val2T;
    } else if (type == "int") {
        auto val1T = convertString<int>(val1);
        auto val2T = convertString<int>(val2);
        return val1T < val2T;
    } else if (type == "uint8_t") {
        auto val1T = convertString<uint8_t>(val1);
        auto val2T = convertString<uint8_t>(val2);
        return val1T < val2T;
    } else if (type == "uint16_t") {
        auto val1T = convertString<uint16_t>(val1);
        auto val2T = convertString<uint16_t>(val2);
        return val1T < val2T;
    } else if (type == "uint32_t") {
        auto val1T = convertString<uint32_t>(val1);
        auto val2T = convertString<uint32_t>(val2);
        return val1T < val2T;
    } else if (type == "uint64_t") {
        auto val1T = convertString<uint64_t>(val1);
        auto val2T = convertString<uint64_t>(val2);
        return val1T < val2T;
    } else throw std::runtime_error("Unknown type " + type + "!");
}

void TMCMCUserInterface::_checkMin(const std::string & newMin, const std::string & oldMin, const std::string & type, const std::string& name){
    // check if min is smaller than current min
    if (_val1IsSmallerThanVal2(newMin, oldMin, type)){
        throw "The minimum for parameter '" + name + "' is " + toString(oldMin) +
              ". Can not re-set the minimum to a smaller value (" + toString(newMin) + ")!";
    }
    _tempMin.newValue = newMin;
    _tempMin.valueHasChanged = true;
}

void TMCMCUserInterface::_checkMax(const std::string & newMax, const std::string & oldMax, const std::string & type, const std::string& name){
    // check if max is larger than current max
    if (_val1IsSmallerThanVal2(oldMax, newMax, type)) {
        throw "The maximum for parameter '" + name + "' is " + toString(oldMax) +
              ". Can not re-set the maximum to a larger value (" + toString(newMax) + ")!";
    }
    _tempMax.newValue = newMax;
    _tempMax.valueHasChanged = true;
}

void TMCMCUserInterface::_matchConfig(std::shared_ptr<TObservationDefinition> & def, const std::string & config, const std::string & val) {
    // function to match configurations of observations
    // user can modify: prior and parameters, simulationFile, min, minIncluded, max, maxIncluded.
    // if user attempts to modify attributes that are not defined for observations (e.g. trace file, isUpdated, proposal kernels etc.), an error is thrown

    if (val.empty()) // directly quit if value is not defined (->keep default)
        return;

    if (config == _expectedColNames.at(1)) { // observation
        def->reSetObserved(val);
    } else if (config == _expectedColNames.at(2)) { // prior
        def->reSetPrior(val);
    } else if (config == _expectedColNames.at(3)) { // parameters
        def->reSetPriorParams(val);
    } else if (config == _expectedColNames.at(4)) { // trace
        throw "Error while reading config file: Can not set trace file for an observation '" + def->name() + "'. Observations are not updated.";
    } else if (config == _expectedColNames.at(5)) { // meanVar
        throw "Error while reading config file: Can not set meanVar file for an observation '" + def->name() + "'. Observations are not updated.";
    } else if (config == _expectedColNames.at(6)) { // simulationFile
        def->setSimulationFile(val);
    } else if (config == _expectedColNames.at( 7)) { // update
        throw "Error while reading config file: Can not update the observation '" + def->name() + "'.";
    } else if (config == _expectedColNames.at(8)){ // propKernel
        throw "Error while reading config file: Can not set proposal kernel for an observation '" + def->name() + "'. Observations are not updated.";
    } else if (config == _expectedColNames.at(9)){ // min
        _checkMin(val, def->min(), def->type(), def->name());
    } else if (config == _expectedColNames.at(10)){ // minIncluded
        bool minIncluded = convertString<bool>(val);
        _tempMin.newIncluded = minIncluded;
        _tempMin.includedHasChanged = true;
    } else if (config == _expectedColNames.at(11)){ // max
        _checkMax(val, def->max(), def->type(), def->name());
    } else if (config == _expectedColNames.at(12)){ // maxIncluded
        bool maxIncluded = convertStringCheck<bool>(val);
        _tempMax.newIncluded = maxIncluded;
        _tempMax.includedHasChanged = true;
    } else if (config == _expectedColNames.at(13)){ // sharedJumpSize
        throw "Error while reading config file: Can not set shared jump size for an observation '" + def->name() + "'. Observations are not updated.";
    }
}

void TMCMCUserInterface::_matchConfig(std::shared_ptr<TParameterDefinition> & def, const std::string & config, const std::string & val) {
    if (val.empty()) // directly quit if value is not defined (-> keep default)
        return;

    if (config == _expectedColNames.at(1)) { // observation
        def->reSetObserved(val);
    } else if (config == _expectedColNames.at(2)) { // prior
        def->reSetPrior(val);
    } else if (config == _expectedColNames.at(3)) { // parameters
        def->reSetPriorParams(val);
    } else if (config == _expectedColNames.at(4)) { // trace
        def->setTraceFile(val);
    } else if (config == _expectedColNames.at(5)) { // meanVar
        def->setMeanVarFile(val);
    } else if (config == _expectedColNames.at(6)) { // simulation
        def->setSimulationFile(val);
    } else if (config == _expectedColNames.at(7)) { // update
        bool update = convertStringCheck<bool>(val);
        def->update(update);
    } else if (config == _expectedColNames.at(8)){ // propKernel
        def->setPropKernel(val);
    } else if (config == _expectedColNames.at(9)){ // min
        _checkMin(val, def->min(), def->type(), def->name());
    } else if (config == _expectedColNames.at(10)){ // minIncluded
        bool minIncluded = convertString<bool>(val);
        _tempMin.newIncluded = minIncluded;
        _tempMin.includedHasChanged = true;
    } else if (config == _expectedColNames.at(11)){ // max
        _checkMax(val, def->max(), def->type(), def->name());
    } else if (config == _expectedColNames.at(12)){ // maxIncluded
        bool maxIncluded = convertStringCheck<bool>(val);
        _tempMax.newIncluded = maxIncluded;
        _tempMax.includedHasChanged = true;
    } else if (config == _expectedColNames.at(13)){ // sharedJumpSize
        bool sharedJumpSize = convertString<bool>(val);
        def->setJumpSizeForAll(sharedJumpSize);
    }
}

void TMCMCUserInterface::_assertMinIsValid(const std::shared_ptr<TDefinitionBase> & def) const{
    // min
    if (_tempMin.includedHasChanged){
        // 1) user didn't change min, but changed minIncluded = F -> minIncluded = T ---> throw, crosses boundaries!
        if (!_tempMin.valueHasChanged && !def->minIncluded() && _tempMin.newIncluded)
            throw "The minimum for parameter '" + def->name() + "' is " + toString(def->min()) +
                  ", excluding the minimum value. Can not re-include the minimum value!";
            // 2) user changed min to same value as before, but changed minIncluded = F -> minIncluded = T ---> throw, crosses boundaries!
        else if (_tempMin.newValue == def->min() && !def->minIncluded() && _tempMin.newIncluded)
            throw "The minimum for parameter '" + def->name() + "' is " + toString(def->min()) +
                  ", excluding the minimum value. Can not re-include the minimum value!";
        // 3) user changed min to a bigger value than before -> ok, accept both!
    }
    if (_tempMin.valueHasChanged)
        def->setMin(_tempMin.newValue);
    if (_tempMin.includedHasChanged)
        def->setMinIncluded(_tempMin.newIncluded);
}

void TMCMCUserInterface::_assertMaxIsValid(const std::shared_ptr<TDefinitionBase> & def) const{
    // max
    if (_tempMax.includedHasChanged){
        // 1) user didn't change max, but changed maxIncluded = F -> maxIncluded = T ---> throw, crosses boundaries!
        if (!_tempMax.valueHasChanged && !def->maxIncluded() && _tempMax.newIncluded)
            throw "The maximum for parameter '" + def->name() + "' is " + toString(def->max()) +
                  ", excluding the maximum value. Can not re-include the maximum value!";
            // 2) user changed max to same value as before, but changed maxIncluded = F -> maxIncluded = T ---> throw, crosses boundaries!
        else if (_tempMax.newValue == def->max() && !def->maxIncluded() && _tempMax.newIncluded)
            throw "The maximum for parameter '" + def->name() + "' is " + toString(def->max()) +
                  ", excluding the maximum value. Can not re-include the maximum value!";
        // 3) user changed max to a smaller value than before -> ok, accept both!
    }
    if (_tempMax.valueHasChanged)
        def->setMax(_tempMax.newValue);
    if (_tempMax.includedHasChanged)
        def->setMaxIncluded(_tempMax.newIncluded);
}

void TMCMCUserInterface::_assertMinAndMaxAreValid(const std::shared_ptr<TDefinitionBase> & def){
    _assertMinIsValid(def);
    _assertMaxIsValid(def);
    if (!_val1IsSmallerThanVal2(def->min(),  def->max(), def->type())) // check if min > max
        throw "The minimum (" + def->min() + ") is larger or equal than the maximum " + def->max() + " for parameter " + def->name() + "!";
}

void TMCMCUserInterface::_parseParamConfigurations(std::vector<std::shared_ptr<TParameterDefinition> > & paramDefs, std::vector<std::shared_ptr<TObservationDefinition> > & observationDefs, const std::vector<std::string> & line){
    // get definition with this name
    size_t colWithName = _configFile.getIndexOfColname("name");
    std::string name = line.at(colWithName);
    // search inside parameter definitions
    for (auto & def : paramDefs){
        // found matching definition!
        if (def->name() == name){
            _tempMin.clear();
            _tempMax.clear();
            // now go over all configurations of this parameter
            size_t c = 0;
            for (auto & col : line){
                _matchConfig(def, _configFile.headerAt(c), col);
                c++;
            }
            // finally: check if min and max are inside hard limits set by the developer
            _assertMinAndMaxAreValid(def);
            return;
        }
    }
    // search inside observation definitions
    for (auto & def : observationDefs){
        // found matching definition!
        if (def->name() == name){
            _tempMin.clear();
            _tempMax.clear();
            // now go over all configurations of this parameter
            size_t c = 0;
            for (auto & col : line){
                _matchConfig(def, _configFile.headerAt(c), col);
                c++;
            }
            // finally: check if min and max are inside hard limits set by the developer
            _assertMinAndMaxAreValid(def);
            return;
        }
    }
    throw "Error while parsing config file " + _configFile.name() + ": No parameter or observation with name '" + name + "' exists!";
}

void TMCMCUserInterface::_parseInitVals(std::vector<std::shared_ptr<TParameterDefinition>> & paramDefs, std::vector<std::shared_ptr<TObservationDefinition> > & observationDefs, const std::vector<std::string> &line) {
    // search inside parameter definitions for this name
    for (auto & def : paramDefs){
        // found matching definition!
        if (def->name() == line.at(0)){
            // set initial value and jumpSize
            if (!line.at(1).empty())
                def->setInitVal(line.at(1));
            if (!line.at(2).empty())
                def->setInitJumpSizeProposal(line.at(2));
            return;
        }
    }
    // search inside observation definitions for this name
    for (auto & def : observationDefs){
        // found matching definition -> throw, because we can not set initial values for observations -> TODO: design choice, change if needed
        if (def->name() == line.at(0)){
            throw "Can not set initial values for observation '" + def->name() + "'!";
        }
    }
    throw "No parameter with name '" + line.at(0) + "' exists!";
}

void TMCMCUserInterface::_readInitValFile(std::vector<std::shared_ptr<TParameterDefinition>> & paramDefs, std::vector<std::shared_ptr<TObservationDefinition> > & observationDefs, TParameters &parameters) {
    std::string fileNameInitVal = parameters.getParameterFilename("initVals", false);
    if (fileNameInitVal.empty()) // file not given -> no need to do anything
        return;
    // file with 3 cols: name of parameter, initial value and initial jump size. Must have this order
    _logfile->listFlush("Reading initial values and jumpSizes from file '" + fileNameInitVal + "'...");
    TInputFile file(fileNameInitVal, header);
    if (file.numCols() != 3)
        throw "Wrong format of file '" + fileNameInitVal + "': expected 3 columns, detected " + toString(file.numCols()) + "!";
    if (file.headerAt(1) != "value")
        throw "Wrong format of file '" + fileNameInitVal + "': column 2 must contain initial values and colname must be 'value'!";
    if (file.headerAt(2) != "jumpSize")
        throw "Wrong format of file '" + fileNameInitVal + "': column 3 must contain initial jumpSizes and colname must be 'jumpSize'!";

    std::vector<std::string> line;
    while (file.read(line)){
        // parse line (= one parameter)
        _parseInitVals(paramDefs, observationDefs, line);
    }
    _logfile->done();
}

void TMCMCUserInterface::_readParamConfigFile(std::vector<std::shared_ptr<TParameterDefinition>> & paramDefs, std::vector<std::shared_ptr<TObservationDefinition> > & observationDefs, TParameters & parameters){
    std::string filename = parameters.getParameterFilename("config", false);
    if (filename.empty()) // file not given -> no need to do anything
        return;
    _logfile->listFlush("Reading parameter configurations from file '" + filename + "'...");
    _configFile.open(filename, header);

    // check if colnames are ok
    _checkHeaderConfigFile(_configFile.name(), _configFile.header());

    std::vector<std::string> line;
    while (_configFile.read(line)){
        // parse line (= one parameter)
        _parseParamConfigurations(paramDefs, observationDefs, line);
    }
    _logfile->done();
}

void TMCMCUserInterface::_parseCommandLineParamConfigs(std::vector<std::shared_ptr<TParameterDefinition>> & paramDefs, std::vector<std::shared_ptr<TObservationDefinition> > & observationDefs, TParameters & parameters){
    // go over all parameter definitions
    for (auto &def : paramDefs){
        _tempMin.clear();
        _tempMax.clear();
        // go over all parameter configurations
        for (auto & config : _expectedColNames){
            // construct name
            std::string argName = def->name() + "." + config;
            // check if argument with this name is given on command line
            if (parameters.parameterExists(argName)){
                // yes, it is given -> get corresponding value
                auto val = parameters.getParameter<std::string>(argName);
                _matchConfig(def, config, val);
            }
        }
        // finally: check if min and max are inside hard limits set by the developer
        _assertMinAndMaxAreValid(def);
    }
    // go over all observation definitions
    for (auto &def : observationDefs){
        _tempMin.clear();
        _tempMax.clear();
        // go over all parameter configurations
        for (auto & config : _expectedColNames){
            // construct name
            std::string argName = def->name() + "." + config;
            // check if argument with this name is given on command line
            if (parameters.parameterExists(argName)){
                // yes, it is given -> get corresponding value
                auto val = parameters.getParameter<std::string>(argName);
                _matchConfig(def, config, val);
            }
        }
        // finally: check if min and max are inside hard limits set by the developer
        _assertMinAndMaxAreValid(def);
    }
}

void TMCMCUserInterface::_parseCommandLineParamInitVals(std::vector<std::shared_ptr<TParameterDefinition>> & paramDefs, std::vector<std::shared_ptr<TObservationDefinition> > & observationDefs, TParameters & parameters){
    // go over all parameter definitions
    for (auto &def : paramDefs){
        // construct name
        std::string argName = def->name() + "." + "value";
        // check if argument with this name is given on command line
        if (parameters.parameterExists(argName)){
            // yes, it is given -> get corresponding value
            auto val = parameters.getParameter<std::string>(argName);
            // set initial value and jumpSize
            def->setInitVal(val);
        }
        // construct name
        argName = def->name() + "." + "jumpSize";
        // check if argument with this name is given on command line
        if (parameters.parameterExists(argName)){
            // yes, it is given -> get corresponding value
            auto val = parameters.getParameter<std::string>(argName);
            // set initial value and jumpSize
            def->setInitJumpSizeProposal(val);
        }
    }
    // go over all observation definitions
    for (auto &def : observationDefs){
        std::string argName = def->name() + "." + "value";
        if (parameters.parameterExists(argName)){
            // found matching definition -> throw, because we can not set initial values for observations -> TODO: design choice, change if needed
            throw "Can not set initial values for observation '" + def->name() + "' from command line!";
        }
        argName = def->name() + "." + "jumpSize";
        if (parameters.parameterExists(argName)){
            throw "Can not set initial jump sizes for observation '" + def->name() + "' from command line!";
        }
    }
}

void TMCMCUserInterface::writeAllConfigurationsToFile(const std::string & prefix, std::vector<std::shared_ptr<TParameterDefinition>> & paramDefs, std::vector<std::shared_ptr<TObservationDefinition> > & observationDefs){
    TOutputFile file(prefix + "config.txt", _expectedColNames);

    // write all configurations for each observation (leave fields that do not apply to observations empty)
    for (auto & def : observationDefs){
        file << def->name() << def->isObserved() << def->prior() << def->parameters()
             << "" << "" << def->simulationFile() << ""
             << "" << def->min() << def->minIncluded() << def->max()
             << def->maxIncluded() << "" << std::endl;
    }

    // write all configurations for each parameter
    for (auto & def : paramDefs){
        file << def->name() << def->isObserved() << def->prior() << def->parameters()
             << def->traceFile() << def->meanVarFile() << def->simulationFile() << def->isUpdated()
             << ProposalKernel::proposalKernelToString[def->propKernel()] << def->min() << def->minIncluded() << def->max()
             << def->maxIncluded() << def->oneJumpSizeForAll() << std::endl;
    }
}

//-------------------------------------------
// TMCMCParametersAssembler
//-------------------------------------------

TDAGBuilder::TDAGBuilder(TRandomGenerator * RandomGenerator, TLog * Logfile){
    _logfile = Logfile;
    _randomGenerator = RandomGenerator;
    _writeStateFile = false;
}

TDAGBuilder::~TDAGBuilder() = default;

void TDAGBuilder::addParameter(const TParameterDefinition& def) {
    // do we already have a parameter with this name?
    _checkForUniqueNames(def.name());
    auto defPtr = std::make_shared<TParameterDefinition>(def);
    _paramDefs.emplace_back(defPtr);
    _allDefs.emplace_back(defPtr);
}

void TDAGBuilder::addObservation(const TObservationDefinition& def) {
    // do we already have a observation with this name?
    _checkForUniqueNames(def.name());
    auto defPtr = std::make_shared<TObservationDefinition>(def);
    _observationDefs.emplace_back(defPtr);
    _allDefs.emplace_back(defPtr);
}

void TDAGBuilder::initializeStorage(){
    _dag.initializeStorage();
}

void TDAGBuilder::_bundleParametersWithSharedPriors(std::vector< std::vector<std::shared_ptr<TDefinitionBase>> > & definitionsWithSharedPriorsVector){
    // go over all definitions
    for (size_t i = 0; i < _allDefs.size(); i++){
        auto def = _allDefs[i];

        // is this definition already built? If yes -> skip
        if (_definitionExistsInVector(def->name(), definitionsWithSharedPriorsVector)) {
            continue;
        }

        bool wasBuilt = false;
        for (size_t j = i+1; j < _allDefs.size(); j++){
            auto otherDef = _allDefs[j];
            bool definitionsSharePrior = _matchSharedPriorWithHyperPriors(def, otherDef, definitionsWithSharedPriorsVector);
            if (definitionsSharePrior) {
                wasBuilt = true;
            }
        }

        // if paramDef was not initialized: no shared priors / not yet initialized mates -> initialize now
        if (!wasBuilt) {
            definitionsWithSharedPriorsVector.push_back({def});
        }
    }
}

bool TDAGBuilder::_definitionExistsInVector(const std::string & name, std::vector< std::vector<std::shared_ptr<TDefinitionBase>> > & definitionsWithSharedPriorsVector){
    // is paramDef already inside the vector -> if yes, don't add it again
    bool exists = false;
    for (auto & it : definitionsWithSharedPriorsVector){ // loop over all unique hyperpriors
        for (auto & it2 : it) { // loop over all parameters of one hyperprior
            if (it2->name() == name)
                exists = true;
        }
    }
    return exists;
}

bool TDAGBuilder::_definitionsSharePrior(std::shared_ptr<TDefinitionBase> & def, std::shared_ptr<TDefinitionBase> & otherDef){
    if (def->parameters() == otherDef->parameters()){
        if (def->prior() == otherDef->prior() && def->type() == otherDef->type()){
            // same prior!
            // e.g. N(mu, sd) and N2(mu, sd) or exp(5) and exp(5)
            return true;
        }
        else if (def->priorParametersAreFix() && otherDef->priorParametersAreFix()){
            // the same parameters, but not the same prior/type -> but still ok as prior parameters are simply numbers (not names of other parameters)
            // e.g. chisq(5) and exp(5) -> prior parameter is the same (5) and prior isn't, but that's totally ok as we don't learn '5'
            return false;
        }
        else
            // the same parameters, but not the same prior/type -> not ok!
            // e.g. N(mu, sd) and beta(mu, sd)
            throw "Parameters " + def->name() + " and " + otherDef->name() + " have the same prior parameters, but differ in their type and/or prior!";
    }
    // different prior parameters
    // e.g. N(mu, sd) and N(mean, sigma) or exp(10) and exp(3)
    return false;
}

void TDAGBuilder::_addToDefinitionsVector(std::shared_ptr<TDefinitionBase> & defToAdd, std::shared_ptr<TDefinitionBase> & existingDef, std::vector< std::vector<std::shared_ptr<TDefinitionBase>> > & definitionsWithSharedPriorsVector){
    for (auto &hyperprior : definitionsWithSharedPriorsVector) { // loop over all unique hyperpriors
        for (auto &def : hyperprior) { // loop over all parameters of one hyperprior
            if (def == existingDef) { // find the otherParamDef -> we need to add paraDef to this vector
                hyperprior.push_back(defToAdd);
                return;
            }
        }
    }
}

bool TDAGBuilder::_matchSharedPriorWithHyperPriors(std::shared_ptr<TDefinitionBase> & def, std::shared_ptr<TDefinitionBase> & otherDef, std::vector< std::vector<std::shared_ptr<TDefinitionBase>> > & definitionsWithSharedPriorsVector){
    if (_definitionsSharePrior(def, otherDef)) {
        if (_definitionExistsInVector(def->name(), definitionsWithSharedPriorsVector)) {
            _addToDefinitionsVector(otherDef, def, definitionsWithSharedPriorsVector);
        } else {
            definitionsWithSharedPriorsVector.push_back({def, otherDef});
        }
        return true;
    }
    return false;
}

bool TDAGBuilder::_definitionsAreBuilt(const std::vector<std::shared_ptr<TDefinitionBase>> & definitionsWithSharedPrior){
    std::vector<bool> definitionIsBuilt;

    for (auto & def : definitionsWithSharedPrior){
        // go over all definitions and search for their name inside _nameToParamMap and _nameToObservationMap
        auto it = _nameToParamMap.find(def->name());
        if (it != _nameToParamMap.end()) { // found -> definition is already initialized as a parameter
            definitionIsBuilt.push_back(true);
        } else {
            auto it2 = _nameToObservationsMap.find(def->name());
            if (it2 != _nameToObservationsMap.end()) { // found -> definition is already initialized as an observation
                definitionIsBuilt.push_back(true);
            } else {
                definitionIsBuilt.push_back(false);
            }
        }
    }

    // check if all are either true or false (otherwise something went wrong in the algorithm)
    if (std::all_of(definitionIsBuilt.begin(), definitionIsBuilt.end(), [](bool init) { return init; })) // all are true
        return true;
    else if (std::all_of(definitionIsBuilt.begin(), definitionIsBuilt.end(), [](bool init) { return !init; })) // all are false
        return false;
    else throw std::runtime_error("In function 'bool TMCMCParameterManager::_definitionsAreBuilt(const std::vector<std::shared_ptr<TMCMCParameterDef>> & paramsWithSharedPrior)': some of the parameters that share a prior are initialized, but not all!");
}

bool TDAGBuilder::_allPriorParametersAreBuilt(const std::string & PriorParameters){
    // check if prior parameters already exist in nameToParamMap or nameToObservationsMap!
    std::vector<std::string> temp;
    fillContainerFromStringAny(PriorParameters, temp, ",", true);

    int counterInitialized = 0;
    for (auto &priorParamName : temp){
        if (stringIsProbablyANumber(priorParamName)) // is a number -> ok, "initialized"
            counterInitialized++;
        else {
            auto it = _nameToParamMap.find(priorParamName);
            if (it != _nameToParamMap.end()) { // found -> param is already initialized
                counterInitialized++;
            } else {
                auto it2 = _nameToObservationsMap.find(priorParamName);
                if (it2 != _nameToObservationsMap.end()) // found -> observation is already initialized
                    counterInitialized++;
            }
        }
    }
    if (counterInitialized == temp.size()) // all are initialized!
        return true;
    return false;
}



void TDAGBuilder::_checkIfAllPriorParamameterDefinitionsExist(){
    // collect all parameter names from definitions
    std::vector<std::string> allParamNames;
    for (auto & def : _allDefs)
        allParamNames.push_back(def->name());

    // now go over definitions again and check if their prior parameters exist
    for (auto & def : _allDefs){
        if (def->priorParametersAreFix()){
            // prior parameters are fix -> ok, no need to check any further
            continue;
        }
        // prior parameter are not fix -> check if prior parameters exits
        // read parameter names into vector
        std::vector<std::string> params;
        fillContainerFromStringAny(def->parameters(), params, ",", true);

        // can we find parameters with these names inside the other parameter definitions?
        for (auto &priorParam : params) {
            auto it = std::find(allParamNames.begin(), allParamNames.end(), priorParam);
            if (it == allParamNames.end())
                throw std::runtime_error("Error when creating parameter " + def->name() + ": No prior with name " + priorParam + " exists!");
        }
    }
}

void TDAGBuilder::_checkForValidDAG() const {
    if (_nameToObservationsMap.empty()){
        throw std::runtime_error("In function 'void TDAGBuilder::_checkForValidDAG() const': no a valid DAG! Need at least 1 observation.");
    }
    for (auto &it : _nameToParamMap){
        if (!it.second->isPartOfPrior()){
            throw std::runtime_error("In function 'void TDAGBuilder::_checkForValidDAG() const': no a valid DAG! A parameter (" + it.second->name() + ") can not be at the bottom of a DAG.");
        }
    }
}

void TDAGBuilder::_checkForUniqueNames(const std::string & name) const{
    for (auto & paramDef : _allDefs) {
        if (paramDef->name() == name) {
            throw std::runtime_error("Parameter with name '" + name + "' already exists! Please provide unique parameter names.");
        }
    }
}

void TDAGBuilder::_findHyperParameters(std::shared_ptr<TDefinitionBase> & def, std::vector<std::shared_ptr<TMCMCParameterBase> > & initializedParams, std::vector<std::shared_ptr<TObservationsBase> > & initializedObservations){
    // parameter whose prior parameters are only numbers are not inferred -> no need to find matching hyperparameters
    if (def->priorParametersAreFix())
        return;

    // first read which parameters are the hyperpriors
    std::vector<std::string> params;
    fillContainerFromStringAny(def->parameters(), params, ",", true);

    // can we find parameters with these names inside the MCMCParameters or observations we've already initialized?
    for (auto &it : params) {
        // first search inside parameters
        auto match = _nameToParamMap.find(it);
        if (match != _nameToParamMap.end()) { // found it!
            // check if parameter is already used as a hyperparameter on another array
            if (match->second->isPartOfPrior())
                throw std::runtime_error("Parameter " + match->second->name() + " is already used as a prior parameter on another prior!");
            match->second->setIsPartOfPrior();
            initializedParams.push_back(match->second); // save pointer in vector
        } else {
            auto match2 = _nameToObservationsMap.find(it);
            if (match2 != _nameToObservationsMap.end()) { // found it!
                if (match2->second->isPartOfPrior())
                    throw std::runtime_error("Observation " + match->second->name() + " is already used as a covariate observation on another prior!");
                match2->second->setIsPartOfPrior();
                initializedObservations.push_back(match2->second); // save pointer in vector
            } else {
                // should never get here, checked this already before
                throw std::runtime_error("Error when creating parameter " + def->name() + ": No prior with name " + it + " exists!");
            }
        }
    }
}



void TDAGBuilder::_createOneDAG(bool throwIfTypesDontMatch) {
    _checkForValidDAG();

    TDAG temporaryDAG;

    // go through all observations
    for (auto &it : _nameToObservationsMap){
        if (!it.second->isPartOfPrior()) { // observation is at bottom of DAG
            it.second->constructDAG(_dag, temporaryDAG);
        }
    }

    // check if DAG is ok with types
    _dag.checkForCompatibleTypes(throwIfTypesDontMatch);
}

std::shared_ptr<TObservationDefinition> TDAGBuilder::_getPointerToObservationDefinition(const std::string & Name) {
    for (auto &def : _observationDefs) {
        if (def->name() == Name) {
            return def;
        }
    }
    // should never get here
    throw std::runtime_error("In function 'std::shared_ptr<TObservationsDef> TMCMCParametersAssembler::_getPointerToObservationDefinition(const std::string & Name)': no observation with name '" + Name + "' exists!");
}

std::shared_ptr<TParameterDefinition> TDAGBuilder::_getPointerToParameterDefinition(const std::string & Name) {
    for (auto &def : _paramDefs) {
        if (def->name() == Name) {
            return def;
        }
    }
    // should never get here
    throw std::runtime_error("In function 'std::shared_ptr<TMCMCParameterDef> TMCMCParametersAssembler::_getPointerToParameterDefinition(const std::string & Name)': no parameter with name '" + Name + "' exists!");
}

void TDAGBuilder::_prepareFiles(MCMCFile::Filetypes mcmcFiletype, std::vector<std::unique_ptr<TMCMCFile> > & fileVec, const std::string & prefix){
    // parameter definitions: search for trace, meanVar and simulation files
    for (auto &def : _paramDefs) {
        std::string filename;
        if (mcmcFiletype == MCMCFile::trace){ // write simulated values in same parameter-bundles as trace files
            filename = def->traceFile();
        } else if (mcmcFiletype == MCMCFile::meanVar) {
            filename = def->meanVarFile();
        } else {
            filename = def->simulationFile();
        }
        _bundleParameterFiles(mcmcFiletype, def->name(), filename, fileVec, prefix);
    }
    // observation definitions: search for simulation files only
    for (auto &def : _observationDefs) {
        if (mcmcFiletype == MCMCFile::simulated) { // write simulated values in same parameter-bundles as trace files
            std::string filename = def->simulationFile();
            _bundleParameterFiles(mcmcFiletype, def->name(), filename, fileVec, prefix);
        }
    }

    // write header
    for (auto &it : fileVec){
        it->writeHeader();
    }
}

void TDAGBuilder::_prepareStateFiles(const std::string & prefix){
    if (_writeStateFile) {
        _stateFile = std::make_shared<TMCMCStateFile>(prefix + "MCMC_state.txt");
        // add all parameters to state file (but not observations! These are fix, no sense to write them into state file)
        for (auto &it : _nameToParamMap)
            _stateFile->addParam(it.second);
    }
}

void TDAGBuilder::_bundleParameterFiles(MCMCFile::Filetypes mcmcFiletype, const std::string & paramName, const std::string& filename, std::vector<std::unique_ptr<TMCMCFile> > & fileVec, const std::string & prefix){
    if (filename == "-") // do not write file
        return;
    // create full filename (prefix + name + type of file + txt)
    std::string fullFilename = prefix + filename + "_" + MCMCFile::fileTypeToString[mcmcFiletype] + ".txt"; // e.g. myParams_trace.txt
    // go over existing files -> does file already exist?
    for (auto &it : fileVec) {
        if (it->name() == fullFilename) { // file already exists!
            // search inside parameters
            auto param = _nameToParamMap.find(paramName);
            if (param != _nameToParamMap.end()) {
                it->addParam(param->second);
            } else {
                // search inside observations
                auto obs = _nameToObservationsMap.find(paramName);
                if (obs != _nameToObservationsMap.end()) {
                    it->addObservation(obs->second);
                } else {
                    throw std::runtime_error("Should have never gotten here: Parameter definition with name " + paramName + " does not exist in _nameToParamMap!");
                }
            }
            return;
        }
    }
    // file was not found -> create a new one!
    // search inside parameters
    auto param = _nameToParamMap.find(paramName);
    if (param != _nameToParamMap.end()) {
        if (mcmcFiletype == MCMCFile::trace){
            fileVec.emplace_back(new TMCMCTraceFile(fullFilename));
        } else if (mcmcFiletype == MCMCFile::meanVar) {
            fileVec.emplace_back(new TMCMCMeanVarFile(fullFilename));
        } else {
            fileVec.emplace_back(new TMCMCSimulationFile(fullFilename));
        }
        fileVec.back()->addParam(param->second);
    } else {
        // search inside observations
        auto obs = _nameToObservationsMap.find(paramName);
        if (obs != _nameToObservationsMap.end()) {
            // add simulation file (trace and meanVar files are not possible for observations)
            fileVec.emplace_back(new TMCMCSimulationFile(fullFilename));
            fileVec.back()->addObservation(obs->second);
        } else {
            throw std::runtime_error("Should have never get here: Parameter definition with name " + paramName + " does not exist in _nameToParamMap!");
        }
    }
}

void TDAGBuilder::_prepareAllMCMCFiles(const std::string & Prefix, bool WriteStateFile){
    _writeStateFile = WriteStateFile;

    // make files ready (initialize, write header etc.)
    _prepareFiles(MCMCFile::trace, _traceFiles, Prefix);
    _prepareFiles(MCMCFile::meanVar, _meanVarFiles, Prefix);
    _prepareStateFiles(Prefix);

    // finally: write a file where all parameter configurations are listed for each parameter
    TMCMCUserInterface userInterface(_logfile);
    userInterface.writeAllConfigurationsToFile(Prefix, _paramDefs, _observationDefs);
}

void TDAGBuilder::_prepareAllSimulationFiles(const std::string &Prefix) {
    // make files ready (initialize, write header etc.)
    _prepareFiles(MCMCFile::simulated, _simulationFiles, Prefix);
}

void TDAGBuilder::_closeAllMCMCFiles(){
    for (auto & file : _traceFiles){
        file->close();
    }
    for (auto & file : _meanVarFiles){
        file->close();
    }
    if (_writeStateFile) {
        _stateFile->close();
    }
    _traceFiles.clear();
    _meanVarFiles.clear();
    _stateFile.reset();
}

void TDAGBuilder::_closeAllSimulationFiles() {
    for (auto & file : _simulationFiles){
        file->close();
    }
    _simulationFiles.clear();
}

void TDAGBuilder::_estimateInitialPriorParameters() {
    _dag.estimateInitialPriorParameters(_logfile);
}

void TDAGBuilder::_updateParameters_MCMC() {
    _dag.update();
}

void TDAGBuilder::_simulate(TParameters & Parameters){
    _dag.simulate(_logfile, Parameters);
}

const std::shared_ptr<TObservationsBase> & TDAGBuilder::getPointerToObservation(const std::string& name){
    // allows developer to get access to observations -> can fill them himself
    auto match = _nameToObservationsMap.find(name);
    if (match != _nameToObservationsMap.end()) { // found it!
        return match->second;
    } else {
        throw std::runtime_error("No observation with name " + name + " exists!");
    }
}

const std::shared_ptr<TMCMCParameterBase> & TDAGBuilder::getPointerToParameter(const std::string &name) {
    // allows developer to get access to parameters -> can tweak stuff
    auto match = _nameToParamMap.find(name);
    if (match != _nameToParamMap.end()) { // found it!
        return match->second;
    } else {
        throw std::runtime_error("No parameter with name " + name + " exists!");
    }
}

const std::shared_ptr<TDefinitionBase> & TDAGBuilder::getPointerToDefinition(const std::string &name) {
    // allows developer to get access to parameters -> can tweak stuff
    for (auto & it : _allDefs){
        if (it->name() == name){
            return it;
        }
    }
    throw std::runtime_error("No definition with name " + name + " exists!");
}

const std::shared_ptr<TParameterDefinition> & TDAGBuilder::getPointerToParameterDefinition(const std::string &name) {
    // allows developer to get access to parameters -> can tweak stuff
    for (auto & it : _paramDefs){
        if (it->name() == name){
            return it;
        }
    }
    throw std::runtime_error("No parameter definition with name " + name + " exists!");
}

const std::map<std::string, std::shared_ptr<TMCMCParameterBase> > & TDAGBuilder::getAllParameters() {
    return _nameToParamMap;
}
