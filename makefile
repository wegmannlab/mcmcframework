SRC = $(wildcard *.cpp) $(wildcard src/*.cpp) $(wildcard commonutilities/*.cpp) $(wildcard commonutilities/IntegrationTests/*.cpp)
GIT_HEADER = commonutilities/gitversion.cpp

# check if commonutilities/gitversion.cpp already exists (if it doesn't, we need to add it to SRC)
ifeq (,$(findstring $(GIT_HEADER),$(SRC)))
    SRC += $(GIT_HEADER)
endif

OBJ = $(SRC:%.cpp=%.o)
BIN = mcmcframework

.PHONY : all
all : $(GIT_HEADER) $(BIN)

# create gitversion.cpp file
$(GIT_HEADER): .git/HEAD .git/COMMIT_EDITMSG .git/index
	touch $@
	echo "#include \"gitversion.h\"" > $@
	echo "std::string getGitVersion(){" >> $@
	echo "return \"$(shell git rev-parse HEAD)\";" >> $@
	echo "}" >> $@

# include libraries
ifeq ($(ARM),)
BINFLAG = -lz -larmadillo
OBJFLAG = -std=c++1y -DWITH_ARMADILLO
else
BINFLAG = -lz -lblas -llapack
OBJFLAG = -Iarmadillo-8.400.0/include -DARMA_DONT_USE_WRAPPER -lblas -llapack -std=c++1y
endif

# if you want to use gperftools: add -lprofiler to BINFLAG and OBJFLAG above

$(BIN): $(OBJ)
	$(CXX) -O3 -o $(BIN) $(OBJ) $(BINFLAG)

.git/COMMIT_EDITMSG :
	touch $@

%.o: %.cpp
	$(CXX) -O3 -c -Icommonutilities $(OBJFLAG) $< -o $@


.PHONY : clean
clean:
	rm -rf $(BIN) $(OBJ)

