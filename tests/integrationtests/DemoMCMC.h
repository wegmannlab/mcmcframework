//
// Created by madleina on 28.04.21.
//

#ifndef BANGOLIN_DEMOMCMC_H
#define BANGOLIN_DEMOMCMC_H

#include "IntegrationTests/TTest.h"
#include "TMCMC.h"

class TDemoMCMC {
public:
    void define(const std::shared_ptr<TDAGBuilder> & DagBuilder){
        std::string filename = "MCMCFrameworkTest_demo";

        // define observation
        TObservationDefinition observation("X");
        observation.setDimensions({1000});
        observation.setPrior(MCMCPrior::normal);
        observation.setPriorParams("mu, sigma");
        observation.setSimulationFile(filename + "_observations");
        DagBuilder->addObservation(observation);

        // define parameter(s)
        TParameterDefinition mu("mu");
        mu.setPrior(MCMCPrior::normal);
        mu.setPriorParams("0, 1");
        mu.setTraceFile(filename);
        mu.setMeanVarFile(filename);
        mu.setSimulationFile(filename);
        DagBuilder->addParameter(mu);

        TParameterDefinition sigma("sigma");
        sigma.setPrior(MCMCPrior::exponential);
        sigma.setPriorParams("1");
        sigma.setTraceFile(filename);
        sigma.setMeanVarFile(filename);
        sigma.setSimulationFile(filename);
        DagBuilder->addParameter(sigma);
    }

    void runMCMC(TRandomGenerator * RandomGenerator, TLog * Logfile, TParameters & Parameters){
        auto dagBuilder = std::make_shared<TDAGBuilder>(RandomGenerator, Logfile);

        // define model
        define(dagBuilder);

        // build DAG
        dagBuilder->buildScaffoldDAG<TDAGBuilderFunctor>(Parameters);
        dagBuilder->initializeStorage();

        // simulate
        TSimulator simulator(Logfile, RandomGenerator, Parameters);
        simulator.simulate("./", dagBuilder, Parameters);

        // infer
        TMCMC mcmc(Logfile, RandomGenerator, Parameters);
        mcmc.runMCMC("./", dagBuilder);
    }
};

class TDemoMCMC_Task: public TTask {
public:
    TDemoMCMC_Task() {
        _citations.insert("Caduff et al. (2020) SOMEWHERE");
        _explanation = "Demonstrating the power of mcmcframework!";
    };

    void run(TParameters & Parameters, TLog* Logfile) override {
        TDemoMCMC demo;
        demo.runMCMC(_randomGenerator, Logfile, Parameters);
    };
};

#endif //BANGOLIN_DEMOMCMC_H
