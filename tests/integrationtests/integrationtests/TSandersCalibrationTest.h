//
// Created by madleina on 05.03.21.
//

#ifndef EMPTY_TSANDERSCALIBRATIONTEST_H
#define EMPTY_TSANDERSCALIBRATIONTEST_H

#include "TSandersCalibration.h"
#include <map>
#include "TFile.h"
#include "TLog.h"

class TSandersCalibrationTest {
    // Sander's Calibration test for binary outcomes

protected:
    // store values for Sander's Calibration test in map
    double _cutoffSignificance_SCTest;
    uint16_t _numBins;
    size_t _numReps;

    // relevant columns in trace- and simulated file
    std::vector<size_t> _relevantCols;

    // maps
    std::map<std::string, std::vector<double>> _simulatedValues;
    std::map<std::string, std::vector<std::vector<double>>> _posteriorProbabilities;
    bool _numStatesInitialized;
    size_t _numStates;

    // vector indicating model switch
    bool _modelsAreSwitched;
    std::vector<size_t> _switchedModels;

    // vector storing SC and p-value after each replicate
    std::map<std::string, std::vector<double>> _sc_afterEachRep;
    std::map<std::string, std::vector<double>> _pValue_afterEachRep;

    void _calculateMeanPosteriorProbability_PerModel(const size_t & ModelIndex, const std::vector<double> & SimulatedValues, const std::vector<std::vector<double>> & PosteriorProbabilitiesPerModel, std::vector<double> & MeanPerModel);
    void _assignSwitchedModels(const std::vector<double> & SimulatedValues, const std::vector<std::vector<double>> & PosteriorProbabilitiesPerModel);
    void _reAssignSimulatedValues(std::vector<double> & SimulatedValues);
    void _parseTraceFile(TInputFile & TraceFile, std::vector<std::vector<double>> & SumOverIterationsPerParam);
    void _saveSimulatedValues(std::vector<double> &SimulatedValues, const std::vector<std::string> & Header, const size_t &Replicate);
    void _prepareVectorsForSandersTest(const size_t & Model, const std::vector<std::vector<double>> & PosteriorProbabilitiesAllModels, const std::vector<double> & SimulatedValuesAllModels, std::vector<double> & PosteriorProbabilityThisModel, std::vector<double> & SimulatedThisModel, const size_t & CurRep);
    void _writePosteriorProbsToFile(const std::pair<std::string, std::vector<std::vector<double>>> & PosteriorProbsParam, const std::pair<std::string, std::vector<double>> & SimulatedValues) const;

    void _addToPValMap(const std::string & Name, const size_t & Rep, const double & PValue);
    void _addToSCMap(const std::string & Name, const size_t & Rep, const double & ObservedSC);

public:
    TSandersCalibrationTest();

    // setters
    void setNumStates(const size_t & NumStates);
    void setCutoffSignificance(const double & Cutoff);
    void setNumBins(const uint16_t & NumBins);
    void prepareMaps(const std::string & Name, const size_t & NumReps);
    void fillRelevantColumns(const std::vector<std::string> & Header);

    // getters
    size_t numParameters() const;
    uint16_t numBins() const;
    bool modelsSwitched() const;
    const std::vector<size_t> & getSwitchedModelOrder() const;
    const std::map<std::string, std::vector<double>> & getPValuesAllReps() const;
    const std::map<std::string, std::vector<double>> & getObservedSCsAllReps() const;

    // evaluate files
    void evaluateTraceFile(std::vector<double> & SimulatedValues, TInputFile & TraceFile, const size_t & Replicate);

    // run test!
    bool passSandersCalibrationTest(TLog *Logfile, TRandomGenerator * RandomGenerator, bool WriteFile, const std::string & Prefix, const size_t & Rep);
};


#endif //EMPTY_TSANDERSCALIBRATIONTEST_H
