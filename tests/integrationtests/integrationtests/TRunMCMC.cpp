//
// Created by Madleina Caduff on 01.05.20.
//

#include "TRunMCMC.h"

//--------------------------------------------
// TMCMCFrameworkTest
//--------------------------------------------

TMCMCFrameworkTest::TMCMCFrameworkTest() : TTest(){
    _testingPrefix = "MCMCFrameworkTest";
    _numReps = 0;
    _runSCAfterEachRep = false;
};

bool TMCMCFrameworkTest::run(TParameters & Params, TLog* Logfile, TTaskList* TaskList){
    // build DAG, read parameters, prepare maps etc.
    _prepare(Params, Logfile);
    _buildDAG(Params, Logfile);

    // run simulation-MCMC replicates!
    Logfile->startNumbering("Running " + toString(_numReps) + " replicates:");
    for (size_t rep = 0; rep < _numReps; rep++) {
        Logfile->number("Replicate " + toString(rep + 1) + ":");
        Logfile->addIndent(2);
        _runReplicate(Params, Logfile, rep);
        Logfile->removeIndent(2);
    }
    Logfile->endNumbering();


    bool writePosteriorFilesSCTest = false;
    if (Params.parameterExists("writePosteriorProbsFile")){
        writePosteriorFilesSCTest = true;
    }
    // run Kolmogorov-Smirnov test, evaluate all t-tests and run Sanders calibration test
    // return true if all passed
    bool KSTestPasses = KSTest.passKolmogorovSmirnovTest(Logfile);
    bool SCTestPasses = SCTest.passSandersCalibrationTest(Logfile, &_randomGenerator, writePosteriorFilesSCTest, _prefix, _numReps-1);

    if (_runSCAfterEachRep){
        _writeSCAndPValsToFile();
    }

    return KSTestPasses && SCTestPasses;
}

void TMCMCFrameworkTest::_buildDAG(TParameters & Params, TLog* Logfile){
    // initialize DAG builder object
    _dagBuilder = std::make_shared<TDAGBuilder>(&_randomGenerator, Logfile);

    // define parameters & observations
    _define();

    // build DAG
    _dagBuilder->buildScaffoldDAG<TDAGBuilderFunctor>(Params);
    _dagBuilder->initializeStorage();

    // prepare maps to store results of single runs
    auto parameters = _dagBuilder->getAllParameters();
    _prepareMaps(parameters);
}

void TMCMCFrameworkTest::_prepare(TParameters & Params, TLog* Logfile){
    // get prefix where files are written to
    _prefix = Params.getParameterWithDefault<std::string>("prefix", "./");

    // define number of replicates and cutoff values for significance
    _numReps = Params.getParameterWithDefault("numReplicates", 2000);

    // define number of bins for Sander's Calibration
    size_t numBins = Params.getParameterWithDefault("numBins", _numReps/100);
    if (numBins < 2){
        numBins = 2;
    }
    SCTest.setNumBins(numBins);

    // run SC test after each replicate?
    if (Params.parameterExists("runSCAfterEachRep")){
        _runSCAfterEachRep = true;
    }

    double cutoffSignificance_KSTest = Params.getParameterWithDefault("cutoff_KS_test", 0.05);
    KSTest.setCutoffSignificance(cutoffSignificance_KSTest);
    double cutoffSignificance_SCTest = Params.getParameterWithDefault("cutoff_SC_test", 0.05);
    SCTest.setCutoffSignificance(cutoffSignificance_SCTest);
}

void TMCMCFrameworkTest::_reSetRandomGenerator(TParameters & Parameters, TLog* Logfile, size_t rep){
    Logfile->listFlush("Setting seed for random generator ...");
    if(Parameters.parameterExists("fixedSeed")){
        _randomGenerator.setSeed(Parameters.getParameter<int>("fixedSeed"), true);
    } else if(Parameters.parameterExists("addToSeed")){
        _randomGenerator.setSeed(Parameters.getParameter<int>("addToSeed"), false);
    } else if(Parameters.parameterExists("addRepToSeed")){
        _randomGenerator.setSeed(rep, false);
    } else {
        _randomGenerator.setSeed(0);
    }
    Logfile->write(" done with seed " + toString(_randomGenerator.usedSeed) + "!");
};

void TMCMCFrameworkTest::_runReplicate(TParameters & Params, TLog* Logfile, size_t rep){
    // simulate
    _reSetRandomGenerator(Params, Logfile, rep);
    TSimulator simulator(Logfile, &_randomGenerator, Params);
    simulator.simulate(_prefix, _dagBuilder, Params);

    // run MCMC
    _reSetRandomGenerator(Params, Logfile, rep);
    TMCMC mcmc(Logfile, &_randomGenerator, Params);
    mcmc.runMCMC(_prefix, _dagBuilder);

    // evaluate result of single run
    _evaluateOneReplicate(rep, Logfile);
}

bool TMCMCFrameworkTest::_paramNeedsSCTest(const std::shared_ptr<TMCMCParameterBase> & parameter){
    if (parameter->isIntegral() && parameter->isUnsigned() && parameter->max() < 255){ // not sure yet whether this is sufficient!
        return true;
    } else {
        return false;
    }
}

void TMCMCFrameworkTest::_addToMap(const std::string & Name, bool SC, const double & Max){
    if (SC){ // add to Sander-Calibration map
        SCTest.setNumStates(static_cast<size_t>(Max) + 1);
        SCTest.prepareMaps(Name, _numReps);
    } else { // add to Kolmogorov-Smirnov map
        KSTest.prepareMaps(Name, _numReps);
    }
}

void TMCMCFrameworkTest::_prepareMaps(const std::map<std::string, std::shared_ptr<TMCMCParameterBase>> & Parameters){
    for (auto & param : Parameters){
        if (param.second->isUpdated()){ // only add updated parameters to map
            std::vector<std::string> header;
            param.second->fillHeader(header);
            for (auto & it : header) {
                _addToMap(it, _paramNeedsSCTest(param.second), param.second->max());
            }
        }
    }
}

void TMCMCFrameworkTest::_openAndCheckFiles(TInputFile & simulatedFile, TInputFile & traceFile, TInputFile & meanVar){
    // open simulated file
    simulatedFile.open(_prefix + _filename + "_simulated.txt", TFile_Filetype::header);

    // open trace file
    traceFile.open(_prefix + _filename + "_trace.txt", TFile_Filetype::header);

    // open meanVar file
    meanVar.open(_prefix + _filename + "_meanVar.txt", TFile_Filetype::header);

    // check if as many parameters as in map
    if (simulatedFile.numCols() != KSTest.numParameters() + SCTest.numParameters()){
        throw std::runtime_error("Simulated file '" + simulatedFile.name() + "' has different number of columns (" + toString(simulatedFile.numCols()) + ") than expected based on map (" + toString(KSTest.numParameters() + SCTest.numParameters()) + ")!");
    }
    // check if simulated and trace file have the same number of parameters
    if (simulatedFile.numCols() != traceFile.numCols()){
        throw std::runtime_error("Simulated file '" + simulatedFile.name() + "' has different number of columns (" + toString(simulatedFile.numCols()) + ") than trace file '" + traceFile.name() + "' (" + toString(traceFile.numCols()) + ")!");
    }
    // check if simulated and meanVar file have the same number of parameters
    if (simulatedFile.numCols() != meanVar.numCols()){
        throw std::runtime_error("Simulated file '" + simulatedFile.name() + "' has different number of columns (" + toString(simulatedFile.numCols()) + ") than meanVar file '" + meanVar.name() + "' (" + toString(meanVar.numCols()) + ")!");
    }
    // check if order of parameters matches between simulated and trace/meanVar file
    for (size_t col = 0; col < simulatedFile.numCols(); col++){
        if (simulatedFile.headerAt(col) != traceFile.headerAt(col)){
            throw std::runtime_error("Simulated file '" + simulatedFile.name() + "' has different colname at position " + toString(col) + " (" + simulatedFile.headerAt(col) + ") than trace file '" + traceFile.name() + "' (" + traceFile.headerAt(col)  + ")!");
        }
        if (simulatedFile.headerAt(col) != meanVar.headerAt(col)){
            throw std::runtime_error("Simulated file '" + simulatedFile.name() + "' has different colname at position " + toString(col) + " (" + simulatedFile.headerAt(col) + ") than meanVar file '" + meanVar.name() + "' (" + meanVar.headerAt(col)  + ")!");
        }
    }
}

void TMCMCFrameworkTest::_evaluateTraceFile(std::vector<double> & SimulatedValues, TInputFile & TraceFile, const size_t & Replicate){
    // assign column names: which columns should run a SC test and which should run a KS test?
    if (Replicate == 0) {
        KSTest.fillRelevantColumns(TraceFile.header());
        SCTest.fillRelevantColumns(TraceFile.header());

        // give KS-test information about parameters that can not be distinguished
        // e.g. m and Mrr in multivariate normal prior are estimated as a compound effect,
        // we only want to find out whether we estimate Mrr/m correctly
        std::map<std::string, std::vector<std::vector<std::string>>> operatorToJointParametersMap;
        _fillIndistinguishableParameters(operatorToJointParametersMap);
        KSTest.addMapOfIndistinguishableParameters(operatorToJointParametersMap);
    }

    // parse trace file for SC test
    if (SCTest.numParameters() > 0) {
        SCTest.evaluateTraceFile(SimulatedValues, TraceFile, Replicate);
        _accountForSwitchedModels(SimulatedValues, TraceFile.header());
    }
    // parse trace file again for KS test. Close and reopen to re-start at beginning of file (there is probably a better way)
    std::string filename = TraceFile.name();
    TraceFile.close();
    TraceFile.open(filename, header);
    KSTest.evaluateTraceFile(SimulatedValues, TraceFile, Replicate);
}

void TMCMCFrameworkTest::_accountForSwitchedModels(std::vector<double> & SimulatedValues, const std::vector<std::string> & Header){
    if (SCTest.modelsSwitched()){
        std::vector<size_t> newOrder = SCTest.getSwitchedModelOrder();
        KSTest.accountForModelSwitch(newOrder, Header, SimulatedValues, _paramNamesMixedModels);
    }
}

void TMCMCFrameworkTest::_fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &OperatorToJointParametersMap) {
    // by default empty, can be overridden if necessary
}

void TMCMCFrameworkTest::_evaluateOneReplicate(const size_t & Replicate, TLog* Logfile){
    // open simulated and trace file and check if they have same format
    TInputFile simulatedFile;
    TInputFile traceFile;
    TInputFile meanVarFile;
    _openAndCheckFiles(simulatedFile, traceFile, meanVarFile);

    // read simulated values into vector
    std::vector<double> simulatedValues;
    simulatedFile.read(simulatedValues, true);

    // evaluate trace file
    _evaluateTraceFile(simulatedValues, traceFile, Replicate);

    // run SC test for checking convergence
    if (_runSCAfterEachRep && Replicate < _numReps-1) { // don't run the very last one - this is still done above, together with KS
        SCTest.passSandersCalibrationTest(Logfile, &_randomGenerator, false, _prefix, Replicate);
    }
}

void TMCMCFrameworkTest::_writeMapToFile(const std::string & Filename, const std::map<std::string, std::vector<double>> & Map){
    TOutputFile file(Filename);

    // header
    std::vector<std::string> header;
    for (auto & it : Map){
        header.push_back(it.first);
    }
    file.writeHeader(header);

    // write rest
    for (size_t rep = 0; rep < _numReps; rep++){
        for (auto & it : Map){
            file << it.second[rep];
        }
        file.endLine();
    }

    file.close();
}

void TMCMCFrameworkTest::_writeSCAndPValsToFile(){
    // write observed SC values to file
    std::string filename = _prefix + _filename + "_observedSandersAllReps.txt";
    std::map<std::string, std::vector<double>> SCVals = SCTest.getObservedSCsAllReps();
    _writeMapToFile(filename, SCVals);

    // write p-values to file
    filename = _prefix + _filename + "_pValsAllReps.txt";
    std::map<std::string, std::vector<double>> pVals = SCTest.getPValuesAllReps();
    _writeMapToFile(filename, pVals);
}

/*
 * ATTENTION: the integration test requires all mixed-model parameters to be named as param_model
 * e.g. mu_0, mu_1 etc.
 * in order to properly account for model switching!!
 */

//--------------------------------------------
// TMCMCFrameworkTest_Normal
//--------------------------------------------

TMCMCFrameworkTest_Normal::TMCMCFrameworkTest_Normal() : TMCMCFrameworkTest(){
    _name = "normal";
};

void TMCMCFrameworkTest_Normal::_define() {
    _filename = _testingPrefix + "_" + _name;

    // define observation
    TObservationDefinition observation("X");
    observation.setDimensions({1000});
    observation.setPrior(MCMCPrior::normal);
    observation.setPriorParams("mu, sigma");
    observation.setSimulationFile(_filename + "_observations");
    _dagBuilder->addObservation(observation);

    // define parameter(s)
    TParameterDefinition mu("mu");
    mu.setPrior(MCMCPrior::normal);
    mu.setPriorParams("0, 1");
    mu.setTraceFile(_filename);
    mu.setMeanVarFile(_filename);
    mu.setSimulationFile(_filename);
    _dagBuilder->addParameter(mu);

    TParameterDefinition sigma("sigma");
    sigma.setPrior(MCMCPrior::exponential);
    sigma.setPriorParams("1");
    sigma.setTraceFile(_filename);
    sigma.setMeanVarFile(_filename);
    sigma.setSimulationFile(_filename);
    _dagBuilder->addParameter(sigma);
}

//--------------------------------------------
// TMCMCFrameworkTest_Exponential
//--------------------------------------------

TMCMCFrameworkTest_Exponential::TMCMCFrameworkTest_Exponential() : TMCMCFrameworkTest(){
    _name = "exponential";
};

void TMCMCFrameworkTest_Exponential::_define() {
    _filename = _testingPrefix + "_" + _name;

    // define observation
    TObservationDefinition observation("X");
    observation.setDimensions({1000});
    observation.setPrior(MCMCPrior::exponential);
    observation.setPriorParams("lambda");
    observation.setSimulationFile(_filename + "_observations");
    _dagBuilder->addObservation(observation);

    // define parameter(s)
    TParameterDefinition lambda("lambda");
    lambda.setPrior(MCMCPrior::exponential);
    lambda.setPriorParams("1");
    lambda.setTraceFile(_filename);
    lambda.setMeanVarFile(_filename);
    lambda.setSimulationFile(_filename);
    _dagBuilder->addParameter(lambda);
}

//--------------------------------------------
// TMCMCFrameworkTest_Beta
//--------------------------------------------

TMCMCFrameworkTest_Beta::TMCMCFrameworkTest_Beta() : TMCMCFrameworkTest(){
    _name = "beta";
};

void TMCMCFrameworkTest_Beta::_define() {
    _filename = _testingPrefix + "_" + _name;

    // define observation
    TObservationDefinition observation("X");
    observation.setDimensions({1000});
    observation.setPrior(MCMCPrior::beta);
    observation.setPriorParams("alpha, beta");
    observation.setSimulationFile(_filename + "_observations");
    _dagBuilder->addObservation(observation);

    // define parameter(s)
    TParameterDefinition alpha("alpha");
    alpha.setPrior(MCMCPrior::exponential);
    alpha.setPriorParams("1");
    alpha.setTraceFile(_filename);
    alpha.setMeanVarFile(_filename);
    alpha.setSimulationFile(_filename);
    _dagBuilder->addParameter(alpha);

    TParameterDefinition beta("beta");
    beta.setPrior(MCMCPrior::exponential);
    beta.setPriorParams("1");
    beta.setTraceFile(_filename);
    beta.setMeanVarFile(_filename);
    beta.setSimulationFile(_filename);
    _dagBuilder->addParameter(beta);
}

//--------------------------------------------
// TMCMCFrameworkTest_Binomial
//--------------------------------------------

TMCMCFrameworkTest_Binomial::TMCMCFrameworkTest_Binomial() : TMCMCFrameworkTest(){
    _name = "binomial";
};

void TMCMCFrameworkTest_Binomial::_define() {
    _filename = _testingPrefix + "_" + _name;

    // define observation
    TObservationDefinition observation("X");
    observation.setDimensions({1000, 2});
    observation.setType("uint8_t");
    observation.setPrior(MCMCPrior::binomial);
    observation.setMax("5");
    observation.setPriorParams("p");
    observation.setSimulationFile(_filename + "_observations");
    _dagBuilder->addObservation(observation);

    // define parameter(s)
    TParameterDefinition p("p");
    p.setPrior(MCMCPrior::beta);
    p.setPriorParams("0.7, 0.7");
    p.setTraceFile(_filename);
    p.setMeanVarFile(_filename);
    p.setSimulationFile(_filename);
    _dagBuilder->addParameter(p);
}

//--------------------------------------------
// TMCMCFrameworkTest_Bernouilli
//--------------------------------------------

TMCMCFrameworkTest_Bernouilli::TMCMCFrameworkTest_Bernouilli() : TMCMCFrameworkTest(){
    _name = "bernouilli";
};

void TMCMCFrameworkTest_Bernouilli::_define() {
    _filename = _testingPrefix + "_" + _name;

    // define observation
    TObservationDefinition observation("X");
    observation.setDimensions({1000});
    observation.setType("uint8_t");
    observation.setPrior(MCMCPrior::bernouilli);
    observation.setPriorParams("pi");
    observation.setSimulationFile(_filename + "_observations");
    _dagBuilder->addObservation(observation);

    // define parameter(s)
    TParameterDefinition pi("pi");
    pi.setPrior(MCMCPrior::beta);
    pi.setPriorParams("2,2");
    pi.setTraceFile(_filename);
    pi.setMeanVarFile(_filename);
    pi.setSimulationFile(_filename);
    _dagBuilder->addParameter(pi);
}

//--------------------------------------------
// TMCMCFrameworkTest_Categorical
//--------------------------------------------

TMCMCFrameworkTest_Categorical::TMCMCFrameworkTest_Categorical() : TMCMCFrameworkTest(){
    _name = "categorical";
};

void TMCMCFrameworkTest_Categorical::_define() {
    _filename = _testingPrefix + "_" + _name;

    // define observation
    TObservationDefinition observation("X");
    observation.setDimensions({1000});
    observation.setType("uint8_t");
    observation.setPrior(MCMCPrior::categorical);
    observation.setPriorParams("pis");
    observation.setMax("2");
    observation.setSimulationFile(_filename + "_observations");
    _dagBuilder->addObservation(observation);

    // define parameter(s)
    TParameterDefinition pis("pis");
    pis.setDimensions({3});
    pis.setPrior(MCMCPrior::dirichlet);
    pis.setPriorParams("100,90,10");
    pis.setTraceFile(_filename);
    pis.setMeanVarFile(_filename);
    pis.setSimulationFile(_filename);
    _dagBuilder->addParameter(pis);
}

//--------------------------------------------
// TMCMCFrameworkTest_HMMBool
//--------------------------------------------

TMCMCFrameworkTest_HMMBool::TMCMCFrameworkTest_HMMBool() : TMCMCFrameworkTest(){
    _name = "hmm_bool";
};

void TMCMCFrameworkTest_HMMBool::_define() {
    _filename = _testingPrefix + "_" + _name;

    // define observation
    TObservationDefinition observation("X");
    observation.setDimensions({1000});
    observation.setType("uint8_t");
    observation.setPrior(MCMCPrior::hmm_bool);
    observation.setPriorParams("lambda_1,lambda_2");
    observation.setSimulationFile(_filename + "_observations");
    _dagBuilder->addObservation(observation);

    // define parameter(s)
    TParameterDefinition lambda1("lambda_1");
    lambda1.setPrior(MCMCPrior::exponential);
    lambda1.setPriorParams("1");
    lambda1.setInitVal("0.01");
    lambda1.setTraceFile(_filename);
    lambda1.setMeanVarFile(_filename);
    lambda1.setSimulationFile(_filename);
    _dagBuilder->addParameter(lambda1);

    TParameterDefinition lambda2("lambda_2");
    lambda2.setPrior(MCMCPrior::exponential);
    lambda2.setPriorParams("1");
    lambda2.setInitVal("1");
    lambda2.setTraceFile(_filename);
    lambda2.setMeanVarFile(_filename);
    lambda2.setSimulationFile(_filename);
    _dagBuilder->addParameter(lambda2);
}

//--------------------------------------------
// TMCMCFrameworkTest_HMMLadder
//--------------------------------------------

TMCMCFrameworkTest_HMMLadder::TMCMCFrameworkTest_HMMLadder() : TMCMCFrameworkTest(){
    _name = "hmm_ladder";
};

void TMCMCFrameworkTest_HMMLadder::_define() {
    _filename = _testingPrefix + "_" + _name;

    // define observation
    TObservationDefinition observation("X");
    observation.setDimensions({1000});
    observation.setType("uint8_t");
    observation.setPrior(MCMCPrior::hmm_ladder);
    observation.setPriorParams("kappa,numStates");
    observation.setSimulationFile(_filename + "_observations");
    _dagBuilder->addObservation(observation);

    // define parameter(s)
    TParameterDefinition kappa("kappa");
    kappa.setPrior(MCMCPrior::exponential);
    kappa.setPriorParams("1");
    kappa.setInitVal("0.5");
    kappa.setTraceFile(_filename);
    kappa.setMeanVarFile(_filename);
    kappa.setSimulationFile(_filename);
    _dagBuilder->addParameter(kappa);

    TParameterDefinition numStates("numStates");
    numStates.update(false);
    numStates.setInitVal("5");
    numStates.setType("uint8_t");
    _dagBuilder->addParameter(numStates);
}

//--------------------------------------------
// TMCMCFrameworkTest_MultivariateNormal
//--------------------------------------------

TMCMCFrameworkTest_MultivariateNormal::TMCMCFrameworkTest_MultivariateNormal() : TMCMCFrameworkTest(){
    _name = "multivariateNormal";
};

void TMCMCFrameworkTest_MultivariateNormal::_define() {
    _filename = _testingPrefix + "_" + _name;

    // define observation
    TObservationDefinition observation("X");
    observation.setDimensions({100, 3});
    observation.setPrior(MCMCPrior::multivariateNormal);
    observation.setPriorParams("mus,m,Mrr,Mrs");
    observation.setSimulationFile(_filename + "_observations");
    _dagBuilder->addObservation(observation);

    // define parameter(s)
    TParameterDefinition mus("mus");
    mus.setDimensions({3});
    mus.setPrior(MCMCPrior::normal);
    mus.setPriorParams("0,1");
    mus.setTraceFile(_filename);
    mus.setMeanVarFile(_filename);
    mus.setSimulationFile(_filename);
    _dagBuilder->addParameter(mus);

    TParameterDefinition m("m");
    m.setPrior(MCMCPrior::exponential);
    m.setPriorParams("5");
    m.setTraceFile(_filename);
    m.setMeanVarFile(_filename);
    m.setSimulationFile(_filename);
    _dagBuilder->addParameter(m);

    TParameterDefinition Mrr("Mrr");
    Mrr.setDimensions({3});
    Mrr.setPrior(MCMCPrior::multivariateChi);
    Mrr.setPriorParams("3,2,1"); // D-r
    Mrr.setMin("0.001"); // set min for Mrr (Mrr with df=1 can get very small in simulations, which results in crazy Var-Covar-Matrices)
    Mrr.setTraceFile(_filename);
    Mrr.setMeanVarFile(_filename);
    Mrr.setSimulationFile(_filename);
    _dagBuilder->addParameter(Mrr);

    TParameterDefinition Mrs("Mrs");
    Mrs.setDimensions({3});
    Mrs.setPrior(MCMCPrior::normal);
    Mrs.setPriorParams("0,1");
    Mrs.setTraceFile(_filename);
    Mrs.setMeanVarFile(_filename);
    Mrs.setSimulationFile(_filename);
    _dagBuilder->addParameter(Mrs);
}

void TMCMCFrameworkTest_MultivariateNormal::_fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &OperatorToJointParametersMap) {
    // parameters m and Mrr / Mrs can not be distinguished
    for (size_t i = 1; i <= 3; i++) {
        OperatorToJointParametersMap["/"].push_back({"Mrr_" + toString(i), "m"});
        for (size_t j = 1; j < i; j++) {
            OperatorToJointParametersMap["/"].push_back({"Mrs_"+ toString(i) + "_" + toString(j), "m"});
        }
    }
}

//--------------------------------------------
// TMCMCFrameworkTest_NormalMixedModel
//--------------------------------------------

TMCMCFrameworkTest_NormalMixedModel::TMCMCFrameworkTest_NormalMixedModel() : TMCMCFrameworkTest(){
    _name = "normalMixedModel";
};

void TMCMCFrameworkTest_NormalMixedModel::_define() {
    _filename = _testingPrefix + "_" + _name;
    _paramNamesMixedModels = {"mu", "var", "pis"}; // these parameters can potentially be switched

    // define observation
    TObservationDefinition observation("X");
    observation.setDimensions({1000});
    observation.setPrior(MCMCPrior::normal_mixedModel);
    observation.setPriorParams("mu_0, mu_1, mu_2, var_0, var_1, var_2, z");
    observation.setSimulationFile(_filename + "_observations");
    _dagBuilder->addObservation(observation);

    // define parameter(s)
    TParameterDefinition mu0("mu_0");
    mu0.setPrior(MCMCPrior::normal);
    mu0.setPriorParams("2, 0.1");
    mu0.setTraceFile(_filename);
    mu0.setMeanVarFile(_filename);
    mu0.setSimulationFile(_filename);
    _dagBuilder->addParameter(mu0);

    TParameterDefinition mu1("mu_1");
    mu1.setPrior(MCMCPrior::normal);
    mu1.setPriorParams("-2, 0.1");
    mu1.setTraceFile(_filename);
    mu1.setMeanVarFile(_filename);
    mu1.setSimulationFile(_filename);
    _dagBuilder->addParameter(mu1);

    TParameterDefinition mu2("mu_2");
    mu2.setPrior(MCMCPrior::normal);
    mu2.setPriorParams("0, 0.1");
    mu2.setTraceFile(_filename);
    mu2.setMeanVarFile(_filename);
    mu2.setSimulationFile(_filename);
    _dagBuilder->addParameter(mu2);

    TParameterDefinition var0("var_0");
    var0.setPrior(MCMCPrior::exponential);
    var0.setPriorParams("5");
    var0.setTraceFile(_filename);
    var0.setMeanVarFile(_filename);
    var0.setSimulationFile(_filename);
    _dagBuilder->addParameter(var0);

    TParameterDefinition var1("var_1");
    var1.setPrior(MCMCPrior::exponential);
    var1.setPriorParams("5");
    var1.setTraceFile(_filename);
    var1.setMeanVarFile(_filename);
    var1.setSimulationFile(_filename);
    _dagBuilder->addParameter(var1);

    TParameterDefinition var2("var_2");
    var2.setPrior(MCMCPrior::exponential);
    var2.setPriorParams("5");
    var2.setTraceFile(_filename);
    var2.setMeanVarFile(_filename);
    var2.setSimulationFile(_filename);
    _dagBuilder->addParameter(var2);

    TParameterDefinition z("z");
    z.setMax("2");
    z.setDimensions({1000});
    z.setType("uint8_t");
    z.setPrior(MCMCPrior::categorical);
    z.setPriorParams("pis");
    z.setTraceFile(_filename);
    z.setMeanVarFile(_filename);
    z.setSimulationFile(_filename);
    _dagBuilder->addParameter(z);

    TParameterDefinition pis("pis");
    pis.setDimensions({3});
    pis.setPrior(MCMCPrior::dirichlet);
    pis.setPriorParams("100,86,14");
    pis.setTraceFile(_filename);
    pis.setMeanVarFile(_filename);
    pis.setSimulationFile(_filename);
    _dagBuilder->addParameter(pis);
}

//--------------------------------------------
// TMCMCFrameworkTest_NormalMixedModel_Stretch
//--------------------------------------------

TMCMCFrameworkTest_NormalMixedModel_Stretch::TMCMCFrameworkTest_NormalMixedModel_Stretch() : TMCMCFrameworkTest(){
    _name = "normalMixedModelStretch";
};

void TMCMCFrameworkTest_NormalMixedModel_Stretch::_define() {
    _filename = _testingPrefix + "_" + _name;
    _paramNamesMixedModels = {"var"}; // these parameters can potentially be switched

    // define observation
    TObservationDefinition observation("X");
    observation.setDimensions({1000});
    observation.setPrior(MCMCPrior::normal_mixedModel_stretch);
    observation.setPriorParams("mu, var_0, var_1, z");
    observation.setSimulationFile(_filename + "_observations");
    _dagBuilder->addObservation(observation);

    // define parameter(s)
    TParameterDefinition mu("mu");
    mu.setPrior(MCMCPrior::normal);
    mu.setPriorParams("0, 1");
    mu.setTraceFile(_filename);
    mu.setMeanVarFile(_filename);
    mu.setSimulationFile(_filename);
    _dagBuilder->addParameter(mu);

    TParameterDefinition var0("var_0");
    var0.setPrior(MCMCPrior::exponential);
    var0.setPriorParams("10");
    var0.setTraceFile(_filename);
    var0.setMeanVarFile(_filename);
    var0.setSimulationFile(_filename);
    _dagBuilder->addParameter(var0);

    TParameterDefinition var1("var_1");
    var1.setPrior(MCMCPrior::exponential);
    var1.setPriorParams("0.1");
    var1.setTraceFile(_filename);
    var1.setMeanVarFile(_filename);
    var1.setSimulationFile(_filename);
    _dagBuilder->addParameter(var1);

    TParameterDefinition z("z");
    z.setMax("1");
    z.setDimensions({1000});
    z.setType("bool");
    z.setPrior(MCMCPrior::bernouilli);
    z.setPriorParams("pi");
    z.setTraceFile(_filename);
    z.setMeanVarFile(_filename);
    z.setSimulationFile(_filename);
    _dagBuilder->addParameter(z);

    TParameterDefinition pi("pi");
    pi.setPrior(MCMCPrior::exponential);
    pi.setPriorParams("1");
    pi.setTraceFile(_filename);
    pi.setMeanVarFile(_filename);
    pi.setSimulationFile(_filename);
    _dagBuilder->addParameter(pi);
}

//------------------------------------------------
// TMCMCFrameworkTest_NormalMixedModel_Stretch_HMM
//------------------------------------------------

TMCMCFrameworkTest_NormalMixedModel_Stretch_HMM::TMCMCFrameworkTest_NormalMixedModel_Stretch_HMM() : TMCMCFrameworkTest(){
    _name = "normalMixedModelStretch_HMM";
};

void TMCMCFrameworkTest_NormalMixedModel_Stretch_HMM::_define() {
    _filename = _testingPrefix + "_" + _name;
    _paramNamesMixedModels = {"var"}; // these parameters can potentially be switched

    // define observation
    TObservationDefinition observation("X");
    observation.setDimensions({1000});
    observation.setPrior(MCMCPrior::normal_mixedModel_stretch);
    observation.setPriorParams("mu, var_0, var_1, z");
    observation.setSimulationFile(_filename + "_observations");
    _dagBuilder->addObservation(observation);

    // define parameter(s)
    TParameterDefinition mu("mu");
    mu.setPrior(MCMCPrior::normal);
    mu.setPriorParams("0, 1");
    mu.setTraceFile(_filename);
    mu.setMeanVarFile(_filename);
    mu.setSimulationFile(_filename);
    _dagBuilder->addParameter(mu);

    TParameterDefinition var0("var_0");
    var0.setPrior(MCMCPrior::exponential);
    var0.setPriorParams("10");
    var0.setTraceFile(_filename);
    var0.setMeanVarFile(_filename);
    var0.setSimulationFile(_filename);
    _dagBuilder->addParameter(var0);

    TParameterDefinition var1("var_1");
    var1.setPrior(MCMCPrior::exponential);
    var1.setPriorParams("0.1");
    var1.setTraceFile(_filename);
    var1.setMeanVarFile(_filename);
    var1.setSimulationFile(_filename);
    _dagBuilder->addParameter(var1);

    TParameterDefinition z("z");
    z.setMax("1");
    z.setDimensions({1000});
    z.setType("bool");
    z.setPrior(MCMCPrior::hmm_bool);
    z.setPriorParams("lambda_1, lambda_2");
    z.setTraceFile(_filename);
    z.setMeanVarFile(_filename);
    z.setSimulationFile(_filename);
    _dagBuilder->addParameter(z);

    TParameterDefinition lambda1("lambda_1");
    lambda1.setPrior(MCMCPrior::exponential);
    lambda1.setPriorParams("1");
    lambda1.setInitVal("0.05");
    lambda1.setTraceFile(_filename);
    lambda1.setMeanVarFile(_filename);
    lambda1.setSimulationFile(_filename);
    _dagBuilder->addParameter(lambda1);

    TParameterDefinition lambda2("lambda_2");
    lambda2.setPrior(MCMCPrior::exponential);
    lambda2.setPriorParams("10");
    lambda2.setInitVal("1");
    lambda2.setTraceFile(_filename);
    lambda2.setMeanVarFile(_filename);
    lambda2.setSimulationFile(_filename);
    _dagBuilder->addParameter(lambda2);
}


//--------------------------------------------
// TMCMCFrameworkTest_MultivariateNormalMixedModel
//--------------------------------------------

TMCMCFrameworkTest_MultivariateNormalMixedModel::TMCMCFrameworkTest_MultivariateNormalMixedModel() : TMCMCFrameworkTest(){
    _name = "multivariateNormalMixedModel";
};

void TMCMCFrameworkTest_MultivariateNormalMixedModel::_define() {
    _filename = _testingPrefix + "_" + _name;
    _paramNamesMixedModels = {"mu", "m", "mrr", "mrs", "pis"}; // these parameters can potentially be switched

    // define observation
    TObservationDefinition observation("X");
    observation.setDimensions({1000, 3});
    observation.setPrior(MCMCPrior::multivariateNormal_mixedModel);
    observation.setPriorParams("mu_0, mu_1, mu_2, m_0, m_1, m_2, mrr_0, mrr_1, mrr_2, mrs_0, mrs_1, mrs_2, z");
    observation.setSimulationFile(_filename + "_observations");
    _dagBuilder->addObservation(observation);

    // define parameter(s)
    // mus
    TParameterDefinition mu0("mu_0");
    mu0.setDimensions({3});
    mu0.setPrior(MCMCPrior::normal);
    mu0.setPriorParams("2, 0.1");
    mu0.setTraceFile(_filename);
    mu0.setMeanVarFile(_filename);
    mu0.setSimulationFile(_filename);
    _dagBuilder->addParameter(mu0);

    TParameterDefinition mu1("mu_1");
    mu1.setDimensions({3});
    mu1.setPrior(MCMCPrior::normal);
    mu1.setPriorParams("-2, 0.1");
    mu1.setTraceFile(_filename);
    mu1.setMeanVarFile(_filename);
    mu1.setSimulationFile(_filename);
    _dagBuilder->addParameter(mu1);

    TParameterDefinition mu2("mu_2");
    mu2.setDimensions({3});
    mu2.setPrior(MCMCPrior::normal);
    mu2.setPriorParams("0, 0.1");
    mu2.setTraceFile(_filename);
    mu2.setMeanVarFile(_filename);
    mu2.setSimulationFile(_filename);
    _dagBuilder->addParameter(mu2);

    // m's
    TParameterDefinition m0("m_0");
    m0.setPrior(MCMCPrior::exponential);
    m0.setPriorParams("5");
    m0.setTraceFile(_filename);
    m0.setMeanVarFile(_filename);
    m0.setSimulationFile(_filename);
    _dagBuilder->addParameter(m0);

    TParameterDefinition m1("m_1");
    m1.setPrior(MCMCPrior::exponential);
    m1.setPriorParams("5");
    m1.setTraceFile(_filename);
    m1.setMeanVarFile(_filename);
    m1.setSimulationFile(_filename);
    _dagBuilder->addParameter(m1);

    TParameterDefinition m2("m_2");
    m2.setPrior(MCMCPrior::exponential);
    m2.setPriorParams("5");
    m2.setTraceFile(_filename);
    m2.setMeanVarFile(_filename);
    m2.setSimulationFile(_filename);
    _dagBuilder->addParameter(m2);

    // mrr's
    TParameterDefinition mrr0("mrr_0");
    mrr0.setDimensions({3});
    mrr0.setPrior(MCMCPrior::multivariateChi);
    mrr0.setPriorParams("3,2,1"); // D-r
    mrr0.setMin("0.001"); // set min for Mrr (Mrr with df=1 can get very small in simulations, which results in crazy Var-Covar-Matrices)
    mrr0.setTraceFile(_filename);
    mrr0.setMeanVarFile(_filename);
    mrr0.setSimulationFile(_filename);
    _dagBuilder->addParameter(mrr0);

    TParameterDefinition mrr1("mrr_1");
    mrr1.setDimensions({3});
    mrr1.setPrior(MCMCPrior::multivariateChi);
    mrr1.setPriorParams("3,2,1"); // D-r
    mrr1.setMin("0.001"); // set min for Mrr (Mrr with df=1 can get very small in simulations, which results in crazy Var-Covar-Matrices)
    mrr1.setTraceFile(_filename);
    mrr1.setMeanVarFile(_filename);
    mrr1.setSimulationFile(_filename);
    _dagBuilder->addParameter(mrr1);

    TParameterDefinition mrr2("mrr_2");
    mrr2.setDimensions({3});
    mrr2.setPrior(MCMCPrior::multivariateChi);
    mrr2.setPriorParams("3,2,1"); // D-r
    mrr2.setMin("0.001"); // set min for Mrr (Mrr with df=1 can get very small in simulations, which results in crazy Var-Covar-Matrices)
    mrr2.setTraceFile(_filename);
    mrr2.setMeanVarFile(_filename);
    mrr2.setSimulationFile(_filename);
    _dagBuilder->addParameter(mrr2);

    // mrs's
    TParameterDefinition mrs0("mrs_0");
    mrs0.setDimensions({3});
    mrs0.setPrior(MCMCPrior::normal);
    mrs0.setPriorParams("0,1");
    mrs0.setTraceFile(_filename);
    mrs0.setMeanVarFile(_filename);
    mrs0.setSimulationFile(_filename);
    _dagBuilder->addParameter(mrs0);

    TParameterDefinition mrs1("mrs_1");
    mrs1.setDimensions({3});
    mrs1.setPrior(MCMCPrior::normal);
    mrs1.setPriorParams("0,1");
    mrs1.setTraceFile(_filename);
    mrs1.setMeanVarFile(_filename);
    mrs1.setSimulationFile(_filename);
    _dagBuilder->addParameter(mrs1);

    TParameterDefinition mrs2("mrs_2");
    mrs2.setDimensions({3});
    mrs2.setPrior(MCMCPrior::normal);
    mrs2.setPriorParams("0,1");
    mrs2.setTraceFile(_filename);
    mrs2.setMeanVarFile(_filename);
    mrs2.setSimulationFile(_filename);
    _dagBuilder->addParameter(mrs2);

    // z
    TParameterDefinition z("z");
    z.setMax("2");
    z.setDimensions({1000});
    z.setType("uint8_t");
    z.setPrior(MCMCPrior::categorical);
    z.setPriorParams("pis");
    z.setTraceFile(_filename);
    z.setMeanVarFile(_filename);
    z.setSimulationFile(_filename);
    _dagBuilder->addParameter(z);

    TParameterDefinition pis("pis");
    pis.setDimensions({3});
    pis.setPrior(MCMCPrior::dirichlet);
    pis.setPriorParams("100,86,14");
    pis.setTraceFile(_filename);
    pis.setMeanVarFile(_filename);
    pis.setSimulationFile(_filename);
    _dagBuilder->addParameter(pis);
}

void TMCMCFrameworkTest_MultivariateNormalMixedModel::_fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &OperatorToJointParametersMap) {
    // parameters m and Mrr / Mrs can not be distinguished
    for (size_t i = 1; i <= 3; i++) {
        OperatorToJointParametersMap["/"].push_back({"mrr_0_" + toString(i), "m_0"});
        OperatorToJointParametersMap["/"].push_back({"mrr_1_" + toString(i), "m_1"});
        OperatorToJointParametersMap["/"].push_back({"mrr_2_" + toString(i), "m_2"});
        for (size_t j = 1; j < i; j++) {
            OperatorToJointParametersMap["/"].push_back({"mrs_0_" + toString(i) + "_" + toString(j), "m_0"});
            OperatorToJointParametersMap["/"].push_back({"mrs_1_" + toString(i) + "_" + toString(j), "m_1"});
            OperatorToJointParametersMap["/"].push_back({"mrs_2_"+ toString(i) + "_" + toString(j), "m_2"});
        }
    }
}

//--------------------------------------------
// TMCMCFrameworkTest_MultivariateNormalMixedModel
//--------------------------------------------

TMCMCFrameworkTest_MultivariateNormalMixedModel_Stretch::TMCMCFrameworkTest_MultivariateNormalMixedModel_Stretch() : TMCMCFrameworkTest(){
    _name = "multivariateNormalMixedModelStretch";
};

void TMCMCFrameworkTest_MultivariateNormalMixedModel_Stretch::_define() {
    _filename = _testingPrefix + "_" + _name;

    // define observation
    TObservationDefinition observation("X");
    observation.setDimensions({1000, 3});
    observation.setPrior(MCMCPrior::multivariateNormal_mixedModel_stretch);
    observation.setPriorParams("mu, m, mrr, mrs, z, rho");
    observation.setSimulationFile(_filename + "_observations");
    _dagBuilder->addObservation(observation);

    // define parameter(s)
    // mu
    TParameterDefinition mu("mu");
    mu.setDimensions({3});
    mu.setPrior(MCMCPrior::normal);
    mu.setPriorParams("0, 0.1");
    mu.setTraceFile(_filename);
    mu.setMeanVarFile(_filename);
    mu.setSimulationFile(_filename);
    _dagBuilder->addParameter(mu);

    // m
    TParameterDefinition m("m");
    m.setPrior(MCMCPrior::exponential);
    m.setPriorParams("5");
    m.setTraceFile(_filename);
    m.setMeanVarFile(_filename);
    m.setSimulationFile(_filename);
    _dagBuilder->addParameter(m);

    // mrr
    TParameterDefinition mrr("mrr");
    mrr.setDimensions({3});
    mrr.setPrior(MCMCPrior::multivariateChi);
    mrr.setPriorParams("3,2,1"); // D-r
    mrr.setMin("0.001"); // set min for Mrr (Mrr with df=1 can get very small in simulations, which results in crazy Var-Covar-Matrices)
    mrr.setTraceFile(_filename);
    mrr.setMeanVarFile(_filename);
    mrr.setSimulationFile(_filename);
    _dagBuilder->addParameter(mrr);

    // mrs
    TParameterDefinition mrs("mrs");
    mrs.setDimensions({3});
    mrs.setPrior(MCMCPrior::normal);
    mrs.setPriorParams("0,1");
    mrs.setTraceFile(_filename);
    mrs.setMeanVarFile(_filename);
    mrs.setSimulationFile(_filename);
    _dagBuilder->addParameter(mrs);

    // z
    TParameterDefinition z("z");
    z.setMax("1");
    z.setDimensions({1000});
    z.setType("bool");
    z.setPrior(MCMCPrior::bernouilli);
    z.setPriorParams("pi");
    z.setTraceFile(_filename);
    z.setMeanVarFile(_filename);
    z.setSimulationFile(_filename);
    _dagBuilder->addParameter(z);

    TParameterDefinition pi("pi");
    pi.setPrior(MCMCPrior::exponential);
    pi.setPriorParams("1");
    pi.setTraceFile(_filename);
    pi.setMeanVarFile(_filename);
    pi.setSimulationFile(_filename);
    _dagBuilder->addParameter(pi);

    // rho
    TParameterDefinition rho("rho");
    rho.setDimensions({3});
    rho.setPrior(MCMCPrior::exponential);
    rho.setPriorParams("0.5");
    rho.setTraceFile(_filename);
    rho.setMeanVarFile(_filename);
    rho.setSimulationFile(_filename);
    _dagBuilder->addParameter(rho);
}

void TMCMCFrameworkTest_MultivariateNormalMixedModel_Stretch::_fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &OperatorToJointParametersMap) {
    // parameters m and Mrr / Mrs can not be distinguished
    for (size_t i = 1; i <= 3; i++) {
        OperatorToJointParametersMap["/"].push_back({"mrr_" + toString(i), "m"});
        for (size_t j = 1; j < i; j++) {
            OperatorToJointParametersMap["/"].push_back({"mrs_"+ toString(i) + "_" + toString(j), "m"});
        }
    }
}

//--------------------------------------------
// TMCMCFrameworkTest_MultivariateNormalMixedModel_Stretch_HMM
//--------------------------------------------

TMCMCFrameworkTest_MultivariateNormalMixedModel_Stretch_HMM::TMCMCFrameworkTest_MultivariateNormalMixedModel_Stretch_HMM() : TMCMCFrameworkTest(){
    _name = "multivariateNormalMixedModelStretch_HMM";
};

void TMCMCFrameworkTest_MultivariateNormalMixedModel_Stretch_HMM::_define() {
    _filename = _testingPrefix + "_" + _name;

    // define observation
    TObservationDefinition observation("X");
    observation.setDimensions({1000, 3});
    observation.setPrior(MCMCPrior::multivariateNormal_mixedModel_stretch);
    observation.setPriorParams("mu, m, mrr, mrs, z, rho");
    observation.setSimulationFile(_filename + "_observations");
    _dagBuilder->addObservation(observation);

    // define parameter(s)
    // mu
    TParameterDefinition mu("mu");
    mu.setDimensions({3});
    mu.setPrior(MCMCPrior::normal);
    mu.setPriorParams("0, 0.1");
    mu.setTraceFile(_filename);
    mu.setMeanVarFile(_filename);
    mu.setSimulationFile(_filename);
    _dagBuilder->addParameter(mu);

    // m
    TParameterDefinition m("m");
    m.setPrior(MCMCPrior::exponential);
    m.setPriorParams("5");
    m.setTraceFile(_filename);
    m.setMeanVarFile(_filename);
    m.setSimulationFile(_filename);
    _dagBuilder->addParameter(m);

    // mrr
    TParameterDefinition mrr("mrr");
    mrr.setDimensions({3});
    mrr.setPrior(MCMCPrior::multivariateChi);
    mrr.setPriorParams("3,2,1"); // D-r
    mrr.setMin("0.001"); // set min for Mrr (Mrr with df=1 can get very small in simulations, which results in crazy Var-Covar-Matrices)
    mrr.setTraceFile(_filename);
    mrr.setMeanVarFile(_filename);
    mrr.setSimulationFile(_filename);
    _dagBuilder->addParameter(mrr);

    // mrs
    TParameterDefinition mrs("mrs");
    mrs.setDimensions({3});
    mrs.setPrior(MCMCPrior::normal);
    mrs.setPriorParams("0,1");
    mrs.setTraceFile(_filename);
    mrs.setMeanVarFile(_filename);
    mrs.setSimulationFile(_filename);
    _dagBuilder->addParameter(mrs);

    // z
    TParameterDefinition z("z");
    z.setMax("1");
    z.setDimensions({1000});
    z.setType("bool");
    z.setPrior(MCMCPrior::hmm_bool);
    z.setPriorParams("lambda_1, lambda_2");
    z.setTraceFile(_filename);
    z.setMeanVarFile(_filename);
    z.setSimulationFile(_filename);
    _dagBuilder->addParameter(z);

    TParameterDefinition lambda1("lambda_1");
    lambda1.setPrior(MCMCPrior::exponential);
    lambda1.setPriorParams("1");
    lambda1.setInitVal("0.2");
    lambda1.setTraceFile(_filename);
    lambda1.setMeanVarFile(_filename);
    lambda1.setSimulationFile(_filename);
    _dagBuilder->addParameter(lambda1);

    TParameterDefinition lambda2("lambda_2");
    lambda2.setPrior(MCMCPrior::exponential);
    lambda2.setPriorParams("10");
    lambda2.setInitVal("1");
    lambda2.setTraceFile(_filename);
    lambda2.setMeanVarFile(_filename);
    lambda2.setSimulationFile(_filename);
    _dagBuilder->addParameter(lambda2);

    // rho
    TParameterDefinition rho("rho");
    rho.setDimensions({3});
    rho.setPrior(MCMCPrior::exponential);
    rho.setPriorParams("0.5");
    rho.setTraceFile(_filename);
    rho.setMeanVarFile(_filename);
    rho.setSimulationFile(_filename);
    _dagBuilder->addParameter(rho);
}

void TMCMCFrameworkTest_MultivariateNormalMixedModel_Stretch_HMM::_fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> &OperatorToJointParametersMap) {
    // parameters m and Mrr / Mrs can not be distinguished
    for (size_t i = 1; i <= 3; i++) {
        OperatorToJointParametersMap["/"].push_back({"mrr_" + toString(i), "m"});
        for (size_t j = 1; j < i; j++) {
            OperatorToJointParametersMap["/"].push_back({"mrs_"+ toString(i) + "_" + toString(j), "m"});
        }
    }
}
