//
// Created by madleina on 05.03.21.
//

#ifndef EMPTY_TKOLMOGOROVSMIRNOVTEST_H
#define EMPTY_TKOLMOGOROVSMIRNOVTEST_H

#include <map>
#include "stringFunctions.h"
#include "TFile.h"
#include "TLog.h"
#include "mathFunctions.h"
#include "MCMCFrameworkVariables.h"
#include <functional>

using ArithmeticFunction = std::function<double(const std::vector<double>&)>;

class TKolmogorovSmirnovTest {
    // Kolmogorov-Smirnov test for evaluating posterior sampling

protected:
    // store values for Kolmogorov-Smirnov test in map
    double _cutoffSignificance_KSTest;
    std::map<std::string, std::vector<double>> _valueInCDF;

    // relevant columns
    std::vector<size_t> _relevantCols;
    std::vector<std::string> _paramNameOfRelevantCols;
    size_t _numRedundantParameters;

    std::map<size_t, std::pair<std::vector<size_t>, ArithmeticFunction>> _columnToJointColumnAndOperatorMap;

    void _parseTraceFile(std::vector<double> & SimulatedValues, TInputFile & TraceFile, std::vector<double> & CountersCDF);
    static double _cumulativeDistrFuncUniform01(const double & x);

    // arithmetic functions that are needed for calculating compound effects of parameters
    static double _add(const std::vector<double>& x);
    static double _sub(const std::vector<double>& x);
    static double _mul(const std::vector<double>& x);
    static double _div(const std::vector<double>& x);
    static double _dotProd(const std::vector<double>& x);
    static ArithmeticFunction _getArithmeticFunction(const std::string & op);
    void _combineIndistinguishableParameters(std::vector<double> & Values);

public:
    TKolmogorovSmirnovTest();

    // setters
    void setCutoffSignificance(const double & Cutoff);
    void prepareMaps(const std::string & Name, const size_t & NumReps);
    void fillRelevantColumns(const std::vector<std::string> & Header);
    void addMapOfIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> & OperatorToJointParametersMap);

    // getters
    size_t numParameters();

    // evaluate files
    void evaluateTraceFile(std::vector<double> & SimulatedValues, TInputFile & traceFile, const size_t & Replicate);
    void accountForModelSwitch(const std::vector<size_t> & switchedModels, const std::vector<std::string> & Header, std::vector<double> & SimulatedValues, const std::vector<std::string> & MixedModelParamNames);

    // run!
    bool passKolmogorovSmirnovTest(TLog * Logfile);
};

#endif //EMPTY_TKOLMOGOROVSMIRNOVTEST_H
