//
// Created by Madleina Caduff on 01.05.20.
//

#ifndef MCMCFRAMEWORK_TRUNMCMC_H
#define MCMCFRAMEWORK_TRUNMCMC_H

#include "IntegrationTests/TTest.h"
#include "TMCMC.h"
#include "TSandersCalibrationTest.h"
#include "TKolmogorovSmirnovTest.h"

template<class T>
struct TDAGBuilderFunctor {
    // function to pass to DAG: contains project-specific priors (none for us)
    std::shared_ptr<TPrior<T>> operator()(const std::string & priorName){
        return std::shared_ptr<TPrior<T>>(nullptr); // return nullptr: we don't have specific priors
    }
};

class TMCMCFrameworkTest : public TTest {
protected:
    // DAG builder object
    std::shared_ptr<TDAGBuilder> _dagBuilder;

    // vector with mixed-model parameters (than can possibly switch)
    std::vector<std::string> _paramNamesMixedModels;

    // tests
    TKolmogorovSmirnovTest KSTest;
    TSandersCalibrationTest SCTest;
    bool _runSCAfterEachRep;

    // files
    std::string _testingPrefix;
    std::string _filename;
    std::string _prefix;

    // random generator
    size_t _numReps;
    TRandomGenerator _randomGenerator;

    // define DAG: pure virtual
    virtual void _define() = 0;

    // fill map of parameters with compound effects
    virtual void _fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> & OperatorToJointParametersMap);

    // prepare
    virtual void _buildDAG(TParameters & Params, TLog* Logfile);
    void _reSetRandomGenerator(TParameters & Parameters, TLog* Logfile, size_t rep);
    void _prepare(TParameters & Params, TLog* Logfile);
    void _prepareMaps(const std::map<std::string, std::shared_ptr<TMCMCParameterBase>> & Parameters);
    void _addToMap(const std::string & Name, bool SC, const double & Max);
    bool _paramNeedsSCTest(const std::shared_ptr<TMCMCParameterBase> & parameter);

    // evaluate MCMC
    virtual void _runReplicate(TParameters & Params, TLog* Logfile, size_t rep);
    void _openAndCheckFiles(TInputFile & simulatedFile, TInputFile & traceFile, TInputFile & meanVar);
    void _evaluateOneReplicate(const size_t & Replicate, TLog* Logfile);
    void _evaluateTraceFile(std::vector<double> & SimulatedValues, TInputFile & TraceFile, const size_t & Replicate);
    void _accountForSwitchedModels(std::vector<double> & SimulatedValues, const std::vector<std::string> & Header);
    void _writeMapToFile(const std::string & Filename, const std::map<std::string, std::vector<double>> & Map);
    void _writeSCAndPValsToFile();

public:
    TMCMCFrameworkTest();
    bool run(TParameters & Params, TLog* logfile, TTaskList * TaskList) override;
};

class TMCMCFrameworkTest_Normal : public TMCMCFrameworkTest {
protected:
    void _define() override;
public:
    TMCMCFrameworkTest_Normal();
};

class TMCMCFrameworkTest_Exponential : public TMCMCFrameworkTest {
protected:
    void _define() override;
public:
    TMCMCFrameworkTest_Exponential();
};

class TMCMCFrameworkTest_Beta : public TMCMCFrameworkTest {
protected:
    void _define() override;
public:
    TMCMCFrameworkTest_Beta();
};

class TMCMCFrameworkTest_Binomial : public TMCMCFrameworkTest {
protected:
    void _define() override;
public:
    TMCMCFrameworkTest_Binomial();
};

class TMCMCFrameworkTest_Bernouilli : public TMCMCFrameworkTest {
protected:
    void _define() override;
public:
    TMCMCFrameworkTest_Bernouilli();
};

class TMCMCFrameworkTest_Categorical : public TMCMCFrameworkTest {
protected:
    void _define() override;
public:
    TMCMCFrameworkTest_Categorical();
};

class TMCMCFrameworkTest_HMMBool : public TMCMCFrameworkTest {
protected:
    void _define() override;
public:
    TMCMCFrameworkTest_HMMBool();
};

class TMCMCFrameworkTest_HMMLadder : public TMCMCFrameworkTest {
protected:
    void _define() override;
public:
    TMCMCFrameworkTest_HMMLadder();
};

class TMCMCFrameworkTest_MultivariateNormal : public TMCMCFrameworkTest {
protected:
    void _define() override;
    void _fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> & OperatorToJointParametersMap) override;
public:
    TMCMCFrameworkTest_MultivariateNormal();
};

class TMCMCFrameworkTest_NormalMixedModel : public TMCMCFrameworkTest {
protected:
    void _define() override;
public:
    TMCMCFrameworkTest_NormalMixedModel();
};

class TMCMCFrameworkTest_NormalMixedModel_Stretch : public TMCMCFrameworkTest {
protected:
    void _define() override;
public:
    TMCMCFrameworkTest_NormalMixedModel_Stretch();
};

class TMCMCFrameworkTest_NormalMixedModel_Stretch_HMM : public TMCMCFrameworkTest {
protected:
    void _define() override;
public:
    TMCMCFrameworkTest_NormalMixedModel_Stretch_HMM();
};

class TMCMCFrameworkTest_MultivariateNormalMixedModel : public TMCMCFrameworkTest {
protected:
    void _define() override;
    void _fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> & OperatorToJointParametersMap) override;
public:
    TMCMCFrameworkTest_MultivariateNormalMixedModel();
};

class TMCMCFrameworkTest_MultivariateNormalMixedModel_Stretch : public TMCMCFrameworkTest {
protected:
    void _define() override;
    void _fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> & OperatorToJointParametersMap) override;
public:
    TMCMCFrameworkTest_MultivariateNormalMixedModel_Stretch();
};

class TMCMCFrameworkTest_MultivariateNormalMixedModel_Stretch_HMM : public TMCMCFrameworkTest {
protected:
    void _define() override;
    void _fillIndistinguishableParameters(std::map<std::string, std::vector<std::vector<std::string>>> & OperatorToJointParametersMap) override;
public:
    TMCMCFrameworkTest_MultivariateNormalMixedModel_Stretch_HMM();
};


#endif //MCMCFRAMEWORK_TRUNMCMC_H
