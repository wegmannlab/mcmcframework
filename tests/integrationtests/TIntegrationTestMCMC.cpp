//
// Created by madleina on 16.11.20.
//

#include "TMain.h"
#include "integrationtests/TRunMCMC.h"
#include "DemoMCMC.h"

void addTasks(TMain & main){
    main.addDebugTask("demo", new TDemoMCMC_Task());
}

void addTests(TMain & main){
    main.addTest("empty", new TTest());
    main.addTest("normal", new TMCMCFrameworkTest_Normal());
    main.addTest("exponential", new TMCMCFrameworkTest_Exponential());
    main.addTest("beta", new TMCMCFrameworkTest_Beta());
    main.addTest("binomial", new TMCMCFrameworkTest_Binomial());
    main.addTest("bernouilli", new TMCMCFrameworkTest_Bernouilli());
    main.addTest("categorical", new TMCMCFrameworkTest_Categorical());
    main.addTest("hmm_bool", new TMCMCFrameworkTest_HMMBool());
    main.addTest("hmm_ladder", new TMCMCFrameworkTest_HMMLadder());
    main.addTest("multivariateNormal", new TMCMCFrameworkTest_MultivariateNormal());
    main.addTest("normalMixedModel", new TMCMCFrameworkTest_NormalMixedModel());
    main.addTest("normalMixedModelStretch", new TMCMCFrameworkTest_NormalMixedModel_Stretch());
    main.addTest("normalMixedModelStretch_HMM", new TMCMCFrameworkTest_NormalMixedModel_Stretch_HMM());
    main.addTest("multivariateNormalMixedModel", new TMCMCFrameworkTest_MultivariateNormalMixedModel());
    main.addTest("multivariateNormalMixedModelStretch", new TMCMCFrameworkTest_MultivariateNormalMixedModel_Stretch());
    main.addTest("multivariateNormalMixedModelStretch_HMM", new TMCMCFrameworkTest_MultivariateNormalMixedModel_Stretch_HMM());


    main.addTestSuite("all", {"normal", "exponential", "beta",
                              "binomial", "bernouilli", "categorical",
                              "hmm_bool", "hmm_ladder", "multivariateNormal", "normalMixedModel",
                              "normalMixedModelStretch", "normalMixedModelStretch_HMM",
                              "multivariateNormalMixedModel",
                              "multivariateNormalMixedModelStretch", "multivariateNormalMixedModelStretch_HMM"});
}

int main(int argc, char **argv) {
    // runs integration test
    TMain main("MCMCFramework", "1.0", "University of Fribourg", "https://bitbucket.org/wegmannlab/mcmcframework", "madleina.caduff@unifr.ch");

    addTasks(main);
    addTests(main);

    //now run program
    return main.run(argc, argv);
}