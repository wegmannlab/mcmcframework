
# -------------------------------------------------------------------------------------------------------------------
# General functions
# -------------------------------------------------------------------------------------------------------------------

plotTrace <- function(path, prefix, test, maxNumPlots = 40, paramName = ""){
  # read files
  traces <- read.table(paste(path, prefix, test, "_trace.txt", sep = ""), header = T)
  meanVar <- read.table(paste(path, prefix, test, "_meanVar.txt", sep = ""), header = T)
  truth <- read.table(paste(path, prefix, test, "_simulated.txt", sep = ""), header = T)
  
  # define number of plots
  numPlots <- min(ncol(traces), maxNumPlots)
  
  # define columns to plot
  colsToPlot <- 1:numPlots
  if (paramName != ""){
    # start at first column with this name
    colsToPlot <- which(grepl(paste("^", paramName, sep = "") , colnames(traces)))[1:numPlots]
  }

  par(mfrow = c(2,2))
  for (i in 1:numPlots){
    limits <- range(traces[,colsToPlot[i]], truth[,colsToPlot[i]])
    # plot trace
    plot(traces[,colsToPlot[i]], ylab = "value", xlab = "iteration", main = paste0("Trace ", colnames(traces)[colsToPlot[i]]), ylim = limits)
    abline(h = truth[,colsToPlot[i]], col = "orange2", lty = 2)
    abline(h = meanVar[1,colsToPlot[i]], col = "dodgerblue", lty = 2)
    
    # plot density
    plot(density(traces[,colsToPlot[i]]), ylab = "Density", xlab = "value", main = paste0("Density ", colnames(traces)[colsToPlot[i]]), xlim = limits)
    abline(v = truth[,colsToPlot[i]], col = "orange2", lty = 2)
    abline(v = meanVar[1,colsToPlot[i]], col = "dodgerblue", lty = 2)
  }

  # add legend
  legend("topright", c("trace", "posterior mean", "truth"), 
         col = c("black", "orange2", "dodgerblue"), lty = c(1,2,2), cex = 0.6)
}

plotModels <- function(path, prefix, test, name, numStates){
  # read files
  traces <- read.table(paste(path, prefix, test, "_trace.txt", sep = ""), header = T)
  truth <- read.table(paste(path, prefix, test, "_simulated.txt", sep = ""), header = T)

  # get column names that start with name (e.g. "z")
  columns <- grepl( paste("^", name, sep = "") , colnames(traces))
  
  cols <- rep("black", nrow(traces))
  colfunc <- colorRampPalette(c("navy","red"))
  colorStates <- colfunc(numStates)
  
  for (state in 0:(numStates-1)){
    cols[truth[columns] == state] <- colorStates[state+1]
  }
  
  # plot posterior mean of model components
  plot(colMeans(traces[, columns]), col = cols)
}

# -------------------------------------------------------------------------------------------------------------------
# Plot
# -------------------------------------------------------------------------------------------------------------------

#path <- "/data/bangolin/debug/integrationTests/26_05_2021/"
path <- "~/git/bangolin/build/"
#prefix <- "MCMCFrameworkTest_"
prefix <- "BangolinTest_bangolin_"
test <- "onlyU"

plotTrace(path, prefix, test, 40, "u")

# for mixed models, you might also want to plot the posterior mean of z
#plotModels(path, prefix, test, "z", 2)
