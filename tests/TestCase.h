//
// Created by madleina on 12.05.20.
//

#ifndef MCMCFRAMEWORK_TESTCASE_H
#define MCMCFRAMEWORK_TESTCASE_H

#include "../../commonutilities/tests/TestCase.h"
#include <typeinfo>

// some mocked classes

template <typename T> char const* str_type( T const& obj ) {
    // used to test if correct child classes are instantiated as unique_ptrs
    return typeid(obj).name();
}

#endif //MCMCFRAMEWORK_TESTCASE_H