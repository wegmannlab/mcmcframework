//
// Created by madleina on 12.05.20.
//

#include "../../core/TProposalKernel.h"
#include "../TestCase.h"
using namespace testing;

//-------------------------------------------
// TPropKernel - double
//-------------------------------------------

TEST(TPropKernelTest, adjust_default_double){
    // min and max are given by numeric limits -> range is just the maximum value that type can take
    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = toString(std::numeric_limits<double>::max());
    TBoundary<double> boundary;
    boundary.init(true, true, min, max, true, true);
    TPropKernel<double> propKernel;

    // check what happens if propkernel = 0 -> should be 0.1
    EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(0, boundary, "dumber"), 0.1);
    // smaller than max -> don't adjust
    EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(0.1, boundary, "dumber"), 0.1);
    EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(std::numeric_limits<double>::max()/2. - std::nextafter(std::numeric_limits<double>::max()/2., 0.), boundary, "dumber"), std::numeric_limits<double>::max()/2.- std::nextafter(std::numeric_limits<double>::max()/2., 0.));
    // exactly max -> don't adjust
    EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(std::numeric_limits<double>::max()/2., boundary, "dumber"), std::numeric_limits<double>::max()/2.);
    // larger than max -> adjust
    EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(std::numeric_limits<double>::max()/2. + std::nextafter(std::numeric_limits<double>::max()/2., std::numeric_limits<double>::max()), boundary, "dumber"), std::numeric_limits<double>::max()/2.);
}

TEST(TPropKernelTest, adjust_nonDefault_double){
    std::string min = "-10";
    std::string max = "1.";
    TBoundary<double> boundary;
    boundary.init(false, false, min, max, true, true);
    TPropKernel<double> propKernel;

    // smaller than max -> don't adjust
    EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(0.1, boundary, "dumber"), 0.1);
    // exactly max -> don't adjust
    EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(5.5, boundary, "dumber"), 5.5);
    // larger than max -> adjust
    EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(6, boundary, "dumber"), 5.5);
}

//-------------------------------------------
// TPropKernel - integer
//-------------------------------------------
TEST(TPropKernelTest, adjust_default_integer){
    // min and max are given by numeric limits -> range is just the maximum value that type can take
    std::string min = toString(std::numeric_limits<int>::lowest());
    std::string max = toString(std::numeric_limits<int>::max());
    TBoundary<int> boundary;
    boundary.init(true, true, min, max, true, true);
    TPropKernel<int> propKernel;

    // check what happens if propkernel = 0 -> should be 1
    EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(0, boundary, "dumber"), 1);
    // smaller than max -> don't adjust
    EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(1, boundary, "dumber"), 1);
    EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(std::numeric_limits<int>::max()/2. - 1, boundary, "dumber"), std::numeric_limits<int>::max()/2. - 1);
    // exactly max -> don't adjust
    EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(std::numeric_limits<int>::max()/2., boundary, "dumber"), std::numeric_limits<int>::max()/2.);
    // larger than max -> adjust
    EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(std::numeric_limits<int>::max()/2. + 1, boundary, "dumber"), std::numeric_limits<int>::max()/2.);
}

TEST(TPropKernelTest, adjust_nonDefault_integer){
    std::string min = "-10";
    std::string max = "1";
    TBoundary<int> boundary;
    boundary.init(false, false, min, max, true, true);
    TPropKernel<int> propKernel;

    // smaller than max -> don't adjust
    EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(1, boundary, "dumber"), 1);
    // exactly max -> don't adjust
    EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(5, boundary, "dumber"), 5);
    // larger than max -> adjust
    EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(6, boundary, "dumber"), 5);
}

TEST(TPropKernelTest, adjust_default_uint8){
    std::string min = toString(std::numeric_limits<uint8_t>::lowest());
    std::string max = toString(std::numeric_limits<uint8_t>::max());
    TBoundary<uint8_t> boundary;
    boundary.init(true, true, min, max, true, true);
    TPropKernel<uint8_t> propKernel;

    // check what happens if propkernel = 0 -> should be 1
    EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(0, boundary, "dumber"), 1);
    // smaller than max -> don't adjust
    EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(1, boundary, "dumber"), 1);
    EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(std::numeric_limits<uint8_t>::max()/2. - 1, boundary, "dumber"), std::numeric_limits<uint8_t>::max()/2. - 1);
    // exactly max -> don't adjust
    EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(std::numeric_limits<uint8_t>::max()/2., boundary, "dumber"), std::numeric_limits<uint8_t>::max()/2.);
    // larger than max -> adjust
    EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(std::numeric_limits<uint8_t>::max()/2. + 1, boundary, "dumber"), std::numeric_limits<uint8_t>::max()/2.);
}

TEST(TPropKernelTest, adjust_nonDefault_uint8){
    std::string min = "1";
    std::string max = "10";
    TBoundary<uint8_t> boundary;
    boundary.init(false, false, min, max, true, true);
    TPropKernel<uint8_t> propKernel;


    // smaller than max -> don't adjust
    EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(1, boundary, "dumber"), 1);
    // exactly max -> don't adjust
    EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(4, boundary, "dumber"), 4);
    // larger than max -> adjust
    EXPECT_THAT(propKernel.adjustPropKernelIfTooBig(5, boundary, "dumber"), 4);
}

//-------------------------------------------
struct TPropKernelNormalTest : Test {
    bool minIncluded = true;
    bool maxIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;

    TBoundary<double> boundary;
    TPropKernelNormal<double> propKernel;

    double value;
};

TEST_F(TPropKernelNormalTest, propose_positive) {
    MockRandomGenerator randomGenerator;
    EXPECT_CALL(randomGenerator, getNormalRandom(0., 1.))
            .Times(1)
            .WillOnce(Return(0.1));

    // default min max
    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = toString(std::numeric_limits<double>::max());
    boundary.init(true, true, min, max, minIncluded, maxIncluded);
    value = 1.;

    value = propKernel.propose(value, boundary, 1, &randomGenerator);

    EXPECT_EQ(value, 1.1);
}

TEST_F(TPropKernelNormalTest, propose_negative) {
    MockRandomGenerator randomGenerator;
    EXPECT_CALL(randomGenerator, getNormalRandom(0., 1.))
            .Times(1)
            .WillOnce(Return(-0.1));

    // default min max
    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = toString(std::numeric_limits<double>::max());
    boundary.init(true, true, min, max, minIncluded, maxIncluded);
    value = -1.;

    value = propKernel.propose(value, boundary, 1, &randomGenerator);

    EXPECT_EQ(value, -1.1);
}

TEST_F(TPropKernelNormalTest, proposeWithoutMirroring_minMax) {
    MockRandomGenerator randomGenerator;
    EXPECT_CALL(randomGenerator, getNormalRandom(0., 0.5))
            .Times(1)
            .WillOnce(Return(0.1));

    std::string min = "0.";
    std::string max = "1.";
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    value = 0.5;

    value = propKernel.propose(value, boundary, 0.5, &randomGenerator);

    EXPECT_EQ(value, 0.6);
}

TEST_F(TPropKernelNormalTest, proposeInsideRange_withMirroring_minMax) {
    MockRandomGenerator randomGenerator;
    EXPECT_CALL(randomGenerator, getNormalRandom(0., 0.5))
            .Times(1)
            .WillOnce(Return(0.1));

    std::string min = "0.";
    std::string max = "1.";
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    value = 1.;

    value = propKernel.propose(value, boundary, 0.5, &randomGenerator);

    EXPECT_FLOAT_EQ(value, 0.9); // 1.1 -> then mirrors -> 0.9
}

TEST_F(TPropKernelNormalTest, proposeOutsideRangePositive_minMax) {
    MockRandomGenerator randomGenerator;
    EXPECT_CALL(randomGenerator, getNormalRandom(0., 0.5))
            // always propose outside range/2
            .Times(AtLeast(1))
            .WillOnce(Return(2.))
            .WillOnce(Return(-1.2))
            .WillOnce(Return(1.8))
            .WillOnce(Return(1.12))
            .WillOnce(Return(-1.))
            .WillOnce(Return(1.))
                    // now its ok, the proposed value is exactly range/2
            .WillOnce(Return(0.5));

    std::string min = "0.";
    std::string max = "1.";
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    value = 1.;

    value = propKernel.propose(value, boundary, 0.5, &randomGenerator);
    EXPECT_EQ(value, 0.5); // adds 0.5 to 1 -> 1.5 -> mirrors -> 0.5
}

TEST_F(TPropKernelNormalTest, proposeOutsideRangePositiveNegative_minMax) {
    MockRandomGenerator randomGenerator;
    EXPECT_CALL(randomGenerator, getNormalRandom(0., 1.))
            .Times(AtLeast(1))
            .WillOnce(Return(2.2))
            .WillOnce(Return(-4.2))
            .WillOnce(Return(7.8))
            .WillOnce(Return(2.))
            .WillOnce(Return(-2.))
                    // this one is not too big anymore
            .WillOnce(Return(1.));


    std::string min = "-1.";
    std::string max = "1.";
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    value = 1.;
    value = propKernel.propose(value, boundary, 1, &randomGenerator);

    EXPECT_EQ(value, 0.); // -> adds 1 to 1 = 2 -> mirrors -> 0
}

TEST_F(TPropKernelNormalTest, proposeOutsideRangeNegative_minMax) {
    MockRandomGenerator randomGenerator;
    EXPECT_CALL(randomGenerator, getNormalRandom(0., 1.))
            .Times(AtLeast(1))
            .WillOnce(Return(4.6))
            .WillOnce(Return(-4.2))
            .WillOnce(Return(7.8))
            .WillOnce(Return(-3.))
            // this one is not too big anymore
            .WillOnce(Return(-1.5));

    std::string min = "-10.";
    std::string max = "-7.";
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    value = -9;
    value = propKernel.propose(value, boundary, 1, &randomGenerator);

    EXPECT_EQ(value, -9.5); // -1.5 - 9 = -10.5 -> mirrors -> -9.5
}

TEST_F(TPropKernelNormalTest, propose_defaultMinMax) {
    boundary.init(true, true, toString(std::numeric_limits<double>::lowest()), toString(std::numeric_limits<double>::max()), true, true);
    value = 0.;

    TRandomGenerator randomGenerator;
    double propWidth = 0.5;

    // draw 1000 random values and check if value behaves as expected
    double oldValue = value;
    for (size_t i = 0; i < 100000; i++){
        value = propKernel.propose(value, boundary, propWidth, &randomGenerator);
        EXPECT_TRUE(value != oldValue);
        EXPECT_TRUE(std::abs(oldValue - value) <= std::numeric_limits<double>::max()/2); // we didn't propose > max/2
        oldValue = value;
    }
}

TEST_F(TPropKernelNormalTest, propose_nonDefaultMinMax) {
    std::string min = "-1.";
    std::string max = "1.";
    boundary.init(false, false, min, max, false, false);
    value = 0.;

    TRandomGenerator randomGenerator;
    double propWidth = 1.; // maximum allowed

    // draw 1000 random values and check if value behaves as expected
    double oldValue = value;
    for (size_t i = 0; i < 100000; i++){
        value = propKernel.propose(value, boundary, propWidth, &randomGenerator);
        // EXPECT_TRUE(value.value() != value.oldValue()); -> can happen when mirroring
        EXPECT_TRUE(std::abs(oldValue - value) <= 1.); // we didn't propose > max/2
        EXPECT_TRUE(value > convertString<double>(min)); // we mirrored
        EXPECT_TRUE(value < convertString<double>(max)); // we mirrored
    }
}


TEST_F(TPropKernelNormalTest, integerTypes) {
    EXPECT_THROW(TPropKernelNormal<int> propKernel, std::runtime_error);
    EXPECT_THROW(TPropKernelNormal<uint8_t> propKernel, std::runtime_error);
    EXPECT_THROW(TPropKernelNormal<uint16_t> propKernel, std::runtime_error);
    EXPECT_THROW(TPropKernelNormal<uint32_t> propKernel, std::runtime_error);
    EXPECT_THROW(TPropKernelNormal<uint64_t> propKernel, std::runtime_error);
    EXPECT_THROW(TPropKernelNormal<bool> propKernel, std::runtime_error);
}

//-------------------------------------------
struct TPropKernelUniformTest : Test {
    bool minIncluded = true;
    bool maxIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;

    TBoundary<double> boundary;
    TPropKernelUniform<double> propKernel;

    double value;
};

TEST_F(TPropKernelUniformTest, propose_positive) {
    MockRandomGenerator randomGenerator;
    EXPECT_CALL(randomGenerator, getRand())
            .Times(1)
            .WillOnce(Return(0.1));

    // default min max
    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = toString(std::numeric_limits<double>::max());
    boundary.init(true, true, min, max, minIncluded, maxIncluded);
    value = 1.;

    value = propKernel.propose(value, boundary, 1, &randomGenerator);  // 0.1*1 - 1/2 = -0.4

    EXPECT_EQ(value, 0.6); // directly adds it to TValue
}

TEST_F(TPropKernelUniformTest, propose_negative) {
    MockRandomGenerator randomGenerator;
    EXPECT_CALL(randomGenerator, getRand())
            .Times(1)
            .WillOnce(Return(-0.1));

    // default min max
    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = toString(std::numeric_limits<double>::max());
    boundary.init(true, true, min, max, minIncluded, maxIncluded);
    value = -1.;

    value = propKernel.propose(value, boundary, 1, &randomGenerator); // -0.1*1 - 1/2 = -0.6

    EXPECT_EQ(value, -1.6); // directly adds it to TValue
}

TEST_F(TPropKernelUniformTest, propose_minMax_withoutMirroring) {
    MockRandomGenerator randomGenerator;
    EXPECT_CALL(randomGenerator, getRand())
            .Times(1)
            .WillOnce(Return(-0.1));

    // default min max
    std::string min = "0.";
    std::string max = "1.";
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    value = 0.7;

    value = propKernel.propose(value, boundary, 0.5, &randomGenerator); // -0.1*0.5 - 0.5/2 = -0.3

    EXPECT_FLOAT_EQ(value, 0.4); // directly adds it to TValue
}

TEST_F(TPropKernelUniformTest, propose_minMax_withMirroring) {
    MockRandomGenerator randomGenerator;
    EXPECT_CALL(randomGenerator, getRand())
            .Times(1)
            .WillOnce(Return(-0.1));

    // default min max
    std::string min = "0.";
    std::string max = "1.";
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    value = 0.1;

    value = propKernel.propose(value, boundary, 0.5, &randomGenerator);  // -0.1*0.5 - 0.5/2 = -0.3

    EXPECT_FLOAT_EQ(value, 0.2); // directly adds it to TValue and mirrors
}

TEST_F(TPropKernelUniformTest, propose_defaultMinMax) {
    boundary.init(true, true, toString(std::numeric_limits<double>::lowest()), toString(std::numeric_limits<double>::max()), true, true);
    value = 0.;

    TRandomGenerator randomGenerator;
    double propWidth = 0.5;

    // draw 100000 random values and check if value behaves as expected
    double oldValue = value;
    for (size_t i = 0; i < 100000; i++){
        value = propKernel.propose(value, boundary, propWidth, &randomGenerator);
        EXPECT_TRUE(value != oldValue);
        EXPECT_TRUE(std::abs(oldValue - value) <= std::numeric_limits<double>::max()/2); // we didn't propose > max/2
        oldValue = value;
    }
}

TEST_F(TPropKernelUniformTest, propose_nonDefaultMinMax) {
    std::string min = "-1.";
    std::string max = "1.";
    boundary.init(false, false, min, max, false, false);
    value = 0.;

    TRandomGenerator randomGenerator;
    double propWidth = 1.;

    // draw 10000 random values and check if value behaves as expected
    double oldValue = value;
    for (size_t i = 0; i < 1000000; i++){
        value = propKernel.propose(value, boundary, propWidth, &randomGenerator);
        // EXPECT_TRUE(value.value() != value.oldValue()); -> can happen when mirroring
        EXPECT_TRUE(std::abs(oldValue - value) <= 1.); // we didn't propose > max/2
        EXPECT_TRUE(value > convertString<double>(min)); // we mirrored
        EXPECT_TRUE(value < convertString<double>(max)); // we mirrored
        oldValue = value;
    }
}


TEST_F(TPropKernelUniformTest, integerTypes) {
    EXPECT_THROW(TPropKernelUniform<int> propKernel, std::runtime_error);
    EXPECT_THROW(TPropKernelUniform<uint8_t> propKernel, std::runtime_error);
    EXPECT_THROW(TPropKernelUniform<uint16_t> propKernel, std::runtime_error);
    EXPECT_THROW(TPropKernelUniform<uint32_t> propKernel, std::runtime_error);
    EXPECT_THROW(TPropKernelUniform<uint64_t> propKernel, std::runtime_error);
    EXPECT_THROW(TPropKernelUniform<bool> propKernel, std::runtime_error);
}

//-------------------------------------------
template <class T>
class BridgeTPropKernelInteger : public TPropKernelInteger<T> {
public:
    BridgeTPropKernelInteger() : TPropKernelInteger<T>(){};
    void _fillCumulProbabilities(const T &propWidth){
        TPropKernelInteger<T>::_fillCumulProbabilities(propWidth);
    }
    std::vector<double> getCumulProbs(){
        return this->_cumulProbs;
    }
};

struct TPropKernelIntTest : Test {
    bool minIncluded = true;
    bool maxIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;

    TBoundary<int> boundary;
    BridgeTPropKernelInteger<int> propKernel;

    int value;
};

TEST_F(TPropKernelIntTest, _fillCumulProbabilities_propWidth1) {
    int propWidth = 1;
    propKernel._fillCumulProbabilities(propWidth);
    std::vector<double> cumulProbs = propKernel.getCumulProbs();

    EXPECT_EQ(cumulProbs.size(), 3);
    EXPECT_FLOAT_EQ(cumulProbs[0], 1./3.);
    EXPECT_FLOAT_EQ(cumulProbs[1], 2./3.);
    EXPECT_FLOAT_EQ(cumulProbs[2], 1.);
}

TEST_F(TPropKernelIntTest, _fillCumulProbabilities_propWidth3) {
    int propWidth = 3;
    propKernel._fillCumulProbabilities(propWidth);
    std::vector<double> cumulProbs = propKernel.getCumulProbs();

    EXPECT_EQ(cumulProbs.size(), 7);
    EXPECT_FLOAT_EQ(cumulProbs[0], 1./7.);
    EXPECT_FLOAT_EQ(cumulProbs[1], 2./7.);
    EXPECT_FLOAT_EQ(cumulProbs[2], 3./7.);
    EXPECT_FLOAT_EQ(cumulProbs[3], 4./7.);
    EXPECT_FLOAT_EQ(cumulProbs[4], 5./7.);
    EXPECT_FLOAT_EQ(cumulProbs[5], 6./7.);
    EXPECT_FLOAT_EQ(cumulProbs[6], 1.);
}

TEST_F(TPropKernelIntTest, pickOne_cumulProbs) {
    // check if function pickOne always returns values within the given propWidth
    int propWidth = 3;
    propKernel._fillCumulProbabilities(propWidth);
    std::vector<double> cumulProbs = propKernel.getCumulProbs();

    TRandomGenerator randomGenerator;

    // draw 1000 random values and check if (randomValue - propWidth) is indeed inside range -propWidth:propWidth
    for (size_t i = 0; i < 100000; i++){
        int val = randomGenerator.pickOne(cumulProbs) - propWidth;
        EXPECT_TRUE(val >= -propWidth && val <= propWidth);
    }
}

TEST_F(TPropKernelIntTest, propose_defaultMinMax) {
    boundary.init(true, true, toString(std::numeric_limits<int>::lowest()), toString(std::numeric_limits<int>::max()), true, true);
    value = 0.;
    int propWidth = 3;
    propKernel.adjustPropKernelIfTooBig(propWidth, boundary, "def");

    TRandomGenerator randomGenerator;

    // draw 1000 random values and check if value behaves as expected
    // (it is really hard to mock templated functions, so I don't work with mocks here)
    int oldValue = value;
    for (size_t i = 0; i < 100000; i++){
        value = propKernel.propose(value, boundary, propWidth, &randomGenerator);
        EXPECT_TRUE(value != oldValue); // we never proposed 0
        EXPECT_TRUE(std::abs(oldValue - value) <= propWidth); // we didn't propose > propWidth
        oldValue = value;
    }
}

TEST_F(TPropKernelIntTest, propose_nonDefaultMinMax) {
    boundary.init(false, false, "-10", "10", false, false);
    value = 0;
    int propWidth = 3;
    propKernel.adjustPropKernelIfTooBig(propWidth, boundary, "def");

    TRandomGenerator randomGenerator;

    // draw 1000 random values and check if value behaves as expected
    // (it is really hard to mock templated functions, so I don't work with mocks here)
    int oldValue = value;
    for (size_t i = 0; i < 100000; i++){
        value = propKernel.propose(value, boundary, propWidth, &randomGenerator);
        // EXPECT_TRUE(value.value() != value.oldValue()); // can happen because of mirroring
        EXPECT_TRUE(std::abs(oldValue - value) <= propWidth); // we didn't propose > propWidth
        EXPECT_TRUE(value > -10); // we mirrored
        EXPECT_TRUE(value < 10); // we mirrored
        oldValue = value;
    }
}

TEST_F(TPropKernelIntTest, boolean) {
    EXPECT_THROW(TPropKernelInteger<bool> propKernel, std::runtime_error);
    EXPECT_THROW(TPropKernelInteger<double> propKernel, std::runtime_error);
    EXPECT_THROW(TPropKernelInteger<float> propKernel, std::runtime_error);
}

//-----------------------------------
// TPropKernelBool
//-----------------------------------

struct TPropKernelBoolTest : Test {
    bool minIncluded = true;
    bool maxIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;

    TBoundary<bool> boundary;
    TPropKernelBool<bool> propKernel;
    TRandomGenerator randomGenerator;

    bool value;
};

TEST_F(TPropKernelBoolTest, propose) {
    boundary.init(true, true, "0", "1", false, false);
    value = false;

    // false -> true
    value = propKernel.propose(value, boundary, 1, &randomGenerator);
    EXPECT_EQ(value, true);

    // true -> false
    value = propKernel.propose(value, boundary, 1, &randomGenerator);
    EXPECT_EQ(value, false);

    // false -> true
    value = propKernel.propose(value, boundary, 1, &randomGenerator);
    EXPECT_EQ(value, true);
}

TEST_F(TPropKernelBoolTest, invalid_types) {
    EXPECT_THROW(TPropKernelBool<int> propKernel, std::runtime_error);
    EXPECT_THROW(TPropKernelBool<uint8_t> propKernel, std::runtime_error);
    EXPECT_THROW(TPropKernelBool<uint16_t> propKernel, std::runtime_error);
    EXPECT_THROW(TPropKernelBool<uint32_t> propKernel, std::runtime_error);
    EXPECT_THROW(TPropKernelBool<uint64_t> propKernel, std::runtime_error);
    EXPECT_THROW(TPropKernelBool<double> propKernel, std::runtime_error);
    EXPECT_THROW(TPropKernelBool<float> propKernel, std::runtime_error);
}