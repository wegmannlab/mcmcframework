//
// Created by madleina on 04.02.21.
//

#include "../TestCase.h"
#include "../../core/TValue.h"
using namespace testing;

// idea: test all features for double, logic should be the same. Then check out all special cases for bool, int and float

//-------------------------------------------
// TValueFixed
//-------------------------------------------
TEST(TValueFixedTest, init){
    TValueFixed<double> val;
    EXPECT_EQ(val.value(), 0.);
    EXPECT_EQ(val.oldValue(), 0.);
}

TEST(TValueFixedTest, initBoth){
    TValueFixed<double> val;
    val.initBoth(0.837);
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.837);
}

TEST(TValueFixedTest, setVal){
    TValueFixed<double> val;
    val.setVal(0.837);
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.837);
}

TEST(TValueFixedTest, set){
    TValueFixed<double> val;
    val.set(0.837);
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.837);
}

TEST(TValueFixedTest, reset){
    TValueFixed<double> val;
    val.set(0.837);
    val.reset(); // nothing happens, doesn't remember old valu
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.837);
}

TEST(TValueFixedTest, expValue){
    TValueFixed<double> val;
    // first set to something which is not 0
    val.set(10.3);
    EXPECT_EQ(val.expValue(), exp(10.3));
    EXPECT_EQ(val.oldExpValue(), exp(10.3));
}

TEST(TValueFixedTest, logValue){
    TValueFixed<double> val;
    // first set to something which is not 0
    val.set(10.3);
    EXPECT_EQ(val.logValue(), log(10.3));
    EXPECT_EQ(val.oldLogValue(), log(10.3));
}

//-------------------------------------------
// TValueFixedWithExponential
//-------------------------------------------
TEST(TValueFixedWithExponentialTest, init){
    TValueFixedWithExponential<double> val;
    EXPECT_EQ(val.value(), 0.);
    EXPECT_EQ(val.oldValue(), 0.);
    EXPECT_EQ(val.expValue(), exp(0.));
    EXPECT_EQ(val.oldExpValue(), exp(0.));
}

TEST(TValueFixedWithExponentialTest, initBoth){
    TValueFixedWithExponential<double> val;
    val.initBoth(0.837);
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.837);
    EXPECT_EQ(val.expValue(), exp(0.837));
    EXPECT_EQ(val.oldExpValue(), exp(0.837));
}

TEST(TValueFixedWithExponentialTest, setVal){
    TValueFixedWithExponential<double> val;
    val.setVal(0.837);
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.837);
    EXPECT_EQ(val.expValue(), exp(0.837));
    EXPECT_EQ(val.oldExpValue(), exp(0.837));
}

TEST(TValueFixedWithExponentialTest, set){
    TValueFixedWithExponential<double> val;
    val.set(0.837);
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.837);
    EXPECT_EQ(val.expValue(), exp(0.837));
    EXPECT_EQ(val.oldExpValue(), exp(0.837));
}

TEST(TValueFixedWithExponentialTest, reset){
    TValueFixedWithExponential<double> val;
    val.set(0.837);
    val.reset(); // nothing happens, doesn't remember old valu
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.837);
    EXPECT_EQ(val.expValue(), exp(0.837));
    EXPECT_EQ(val.oldExpValue(), exp(0.837));
}

//-------------------------------------------
// TValueFixedWithLog
//-------------------------------------------
TEST(TValueFixedWithLogTest, init){
    TValueFixedWithLog<double> val;
    EXPECT_EQ(val.value(), 0.000001);
    EXPECT_EQ(val.oldValue(), 0.000001);
    EXPECT_EQ(val.logValue(), log(0.000001));
    EXPECT_EQ(val.oldLogValue(), log(0.000001));
}

TEST(TValueFixedWithLogTest, init_integer){
    TValueFixedWithLog<int> val;
    EXPECT_EQ(val.value(), 1);
    EXPECT_EQ(val.oldValue(), 1);
    EXPECT_EQ(val.logValue(), log(1));
    EXPECT_EQ(val.oldLogValue(), log(1));
}

TEST(TValueFixedWithLogTest, init_bool){
    TValueFixedWithLog<bool> val;
    EXPECT_EQ(val.value(), 1);
    EXPECT_EQ(val.oldValue(), 1);
    EXPECT_EQ(val.logValue(), log(1));
    EXPECT_EQ(val.oldLogValue(), log(1));
}

TEST(TValueFixedWithLogTest, initBoth){
    TValueFixedWithLog<double> val;
    val.initBoth(0.837);
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.837);
    EXPECT_EQ(val.logValue(), log(0.837));
    EXPECT_EQ(val.oldLogValue(), log(0.837));

    EXPECT_DEATH(val.initBoth(0.), "");
    EXPECT_DEATH(val.initBoth(-1.), "");
}

TEST(TValueFixedWithLogTest, setVal){
    TValueFixedWithLog<double> val;
    val.setVal(0.837);
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.837);
    EXPECT_EQ(val.logValue(), log(0.837));
    EXPECT_EQ(val.oldLogValue(), log(0.837));

    EXPECT_DEATH(val.setVal(0.), "");
    EXPECT_DEATH(val.setVal(-1.), "");
}

TEST(TValueFixedWithLogTest, set){
    TValueFixedWithLog<double> val;
    val.set(0.837);
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.837);
    EXPECT_EQ(val.logValue(), log(0.837));
    EXPECT_EQ(val.oldLogValue(), log(0.837));

    EXPECT_DEATH(val.set(0.), "");
    EXPECT_DEATH(val.set(-1.), "");
}

TEST(TValueFixedWithLogTest, reset){
    TValueFixedWithLog<double> val;
    val.set(0.837);
    val.reset(); // nothing happens, doesn't remember old value
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.837);
    EXPECT_EQ(val.logValue(), log(0.837));
    EXPECT_EQ(val.oldLogValue(), log(0.837));
}

//-------------------------------------------
// TValueFixedWithLogAndExponential
//-------------------------------------------

TEST(TValueFixedWithLogAndExponentialTest, init){
    TValueFixedWithLogAndExponential<double> val;
    EXPECT_EQ(val.value(), 0.000001);
    EXPECT_EQ(val.oldValue(), 0.000001);
    EXPECT_EQ(val.logValue(), log(0.000001));
    EXPECT_EQ(val.oldLogValue(), log(0.000001));
    EXPECT_EQ(val.expValue(), exp(0.000001));
    EXPECT_EQ(val.oldExpValue(), exp(0.000001));
}

TEST(TValueFixedWithLogAndExponentialTest, init_integer){
    TValueFixedWithLogAndExponential<int> val;
    EXPECT_EQ(val.value(), 1);
    EXPECT_EQ(val.oldValue(), 1);
    EXPECT_EQ(val.logValue(), log(1));
    EXPECT_EQ(val.oldLogValue(), log(1));
    EXPECT_EQ(val.expValue(), exp(1));
    EXPECT_EQ(val.oldExpValue(), exp(1));
}

TEST(TValueFixedWithLogAndExponentialTest, init_bool){
    TValueFixedWithLogAndExponential<bool> val;
    EXPECT_EQ(val.value(), 1);
    EXPECT_EQ(val.oldValue(), 1);
    EXPECT_EQ(val.logValue(), log(1));
    EXPECT_EQ(val.oldLogValue(), log(1));
    EXPECT_EQ(val.expValue(), exp(1));
    EXPECT_EQ(val.oldExpValue(), exp(1));
}

TEST(TValueFixedWithLogAndExponentialTest, initBoth){
    TValueFixedWithLogAndExponential<double> val;
    val.initBoth(0.837);
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.837);
    EXPECT_EQ(val.logValue(), log(0.837));
    EXPECT_EQ(val.oldLogValue(), log(0.837));
    EXPECT_EQ(val.expValue(), exp(0.837));
    EXPECT_EQ(val.oldExpValue(), exp(0.837));

    EXPECT_DEATH(val.initBoth(0.), "");
    EXPECT_DEATH(val.initBoth(-1.), "");
}

TEST(TValueFixedWithLogAndExponentialTest, setVal){
    TValueFixedWithLogAndExponential<double> val;
    val.setVal(0.837);
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.837);
    EXPECT_EQ(val.logValue(), log(0.837));
    EXPECT_EQ(val.oldLogValue(), log(0.837));
    EXPECT_EQ(val.expValue(), exp(0.837));
    EXPECT_EQ(val.oldExpValue(), exp(0.837));

    EXPECT_DEATH(val.setVal(0.), "");
    EXPECT_DEATH(val.setVal(-1.), "");
}

TEST(TValueFixedWithLogAndExponentialTest, set){
    TValueFixedWithLogAndExponential<double> val;
    val.set(0.837);
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.837);
    EXPECT_EQ(val.logValue(), log(0.837));
    EXPECT_EQ(val.oldLogValue(), log(0.837));
    EXPECT_EQ(val.expValue(), exp(0.837));
    EXPECT_EQ(val.oldExpValue(), exp(0.837));

    EXPECT_DEATH(val.set(0.), "");
    EXPECT_DEATH(val.set(-1.), "");
}

TEST(TValueFixedWithLogAndExponentialTest, reset){
    TValueFixedWithLogAndExponential<double> val;
    val.set(0.837);
    val.reset(); // nothing happens, doesn't remember old value
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.837);
    EXPECT_EQ(val.logValue(), log(0.837));
    EXPECT_EQ(val.oldLogValue(), log(0.837));
    EXPECT_EQ(val.expValue(), exp(0.837));
    EXPECT_EQ(val.oldExpValue(), exp(0.837));
}

//-------------------------------------------
// TValueUpdated
//-------------------------------------------
TEST(TValueUpdatedTest, init){
    TValueUpdated<double> val;
    EXPECT_EQ(val.value(), 0.);
    EXPECT_EQ(val.oldValue(), 0.);
}

TEST(TValueUpdatedTest, initBoth){
    TValueUpdated<double> val;
    // positive
    val.initBoth(0.837);
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.837);

    // negative
    val.initBoth(-2.74);
    EXPECT_EQ(val.value(), -2.74);
    EXPECT_EQ(val.oldValue(), -2.74);

    // zero
    val.initBoth(0.);
    EXPECT_EQ(val.value(), 0.);
    EXPECT_EQ(val.oldValue(), 0.);
}

TEST(TValueUpdatedTest, setVal){
    TValueUpdated<double> val;
    // positive
    val.setVal(0.837);
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.);

    // negative
    val.setVal(-2.74);
    EXPECT_EQ(val.value(), -2.74);
    EXPECT_EQ(val.oldValue(), 0.);

    // zero
    val.setVal(0.);
    EXPECT_EQ(val.value(), 0.);
    EXPECT_EQ(val.oldValue(), 0.);
}

TEST(TValueUpdatedTest, set){
    TValueUpdated<double> val;
    // positive
    val.set(0.837);
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.);

    // negative
    val.set(-2.74);
    EXPECT_EQ(val.value(), - 2.74);
    EXPECT_EQ(val.oldValue(), 0.837);

    // zero
    val.set(0.);
    EXPECT_EQ(val.value(), 0.);
    EXPECT_EQ(val.oldValue(), - 2.74);
}

TEST(TValueUpdatedTest, reset){
    TValueUpdated<double> val;
    // first set to something which is not 0
    val.set(10.3);

    // reset
    val.reset();
    EXPECT_EQ(val.value(), 0.);
    EXPECT_EQ(val.oldValue(), 0.);
}

TEST(TValueUpdatedTest, expValue){
    TValueUpdated<double> val;
    // first set to something which is not 0
    val.set(10.3);

    // expValue

    EXPECT_EQ(val.expValue(), exp(10.3));
    EXPECT_EQ(val.oldExpValue(), exp(0.));
}

// int
TEST(TValueUpdatedTest, integer){
    TValueUpdated<int> val;

    // init
    EXPECT_EQ(val.value(), 0);
    EXPECT_EQ(val.oldValue(), 0);

    // initBoth
    val.initBoth(1);
    EXPECT_EQ(val.value(), 1);
    EXPECT_EQ(val.oldValue(), 1);

    // set
    val.set(3);
    EXPECT_EQ(val.value(), 3);
    EXPECT_EQ(val.oldValue(), 1);

    // setVal
    val.setVal(4);
    EXPECT_EQ(val.value(), 4);
    EXPECT_EQ(val.oldValue(), 1);

    // set
    val.set(-4);
    EXPECT_EQ(val.value(), -4);
    EXPECT_EQ(val.oldValue(), 4);

    // reset
    val.reset();
    EXPECT_EQ(val.value(), 4);
    EXPECT_EQ(val.oldValue(), 4);

    // expValue
    EXPECT_EQ(val.expValue(), exp(4));
    EXPECT_EQ(val.oldExpValue(), exp(4));
}

// int
TEST(TValueUpdatedTest, boolean){
    TValueUpdated<bool> val;

    // init
    EXPECT_EQ(val.value(), false);
    EXPECT_EQ(val.oldValue(), false);

    // initBoth
    val.initBoth(true);
    EXPECT_EQ(val.value(), true);
    EXPECT_EQ(val.oldValue(), true);

    // set
    val.set(false);
    EXPECT_EQ(val.value(), false);
    EXPECT_EQ(val.oldValue(), true);

    // reset
    val.reset();
    EXPECT_EQ(val.value(), true);
    EXPECT_EQ(val.oldValue(), true);

    // setVal
    val.setVal(false);
    EXPECT_EQ(val.value(), false);
    EXPECT_EQ(val.oldValue(), true);

    // expValue
    EXPECT_EQ(val.expValue(), exp(0));
    EXPECT_EQ(val.oldExpValue(), exp(1));
}

//-------------------------------------------

TEST(TValueUpdatedWithExponentialTest, init){
    TValueUpdatedWithExponential<double> val;
    EXPECT_EQ(val.value(), 0.);
    EXPECT_EQ(val.oldValue(), 0.);
    EXPECT_EQ(val.expValue(), 1.);
    EXPECT_EQ(val.oldExpValue(), 1.);
}

TEST(TValueUpdatedWithExponentialTest, initBoth){
    TValueUpdatedWithExponential<double> val;
    // positive
    val.initBoth(0.837);
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.837);
    EXPECT_EQ(val.expValue(), exp(0.837));
    EXPECT_EQ(val.oldExpValue(), exp(0.837));

    // negative
    val.initBoth(-2.74);
    EXPECT_EQ(val.value(), - 2.74);
    EXPECT_EQ(val.oldValue(), - 2.74);
    EXPECT_EQ(val.expValue(), exp(- 2.74));
    EXPECT_EQ(val.oldExpValue(), exp(- 2.74));

    // zero
    val.initBoth(0.);
    EXPECT_EQ(val.value(), 0.);
    EXPECT_EQ(val.oldValue(), 0.);
    EXPECT_EQ(val.expValue(), exp(0.));
    EXPECT_EQ(val.oldExpValue(), exp(0.));
}

TEST(TValueUpdatedWithExponentialTest, setVal){
    TValueUpdatedWithExponential<double> val;
    // positive
    val.setVal(0.837);
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.);
    EXPECT_EQ(val.expValue(), exp(0.837));
    EXPECT_EQ(val.oldExpValue(), exp(0.));

    // negative
    val.setVal(-2.74);
    EXPECT_EQ(val.value(), - 2.74);
    EXPECT_EQ(val.oldValue(), 0.);
    EXPECT_EQ(val.expValue(), exp(- 2.74));
    EXPECT_EQ(val.oldExpValue(), exp(0.));

    // zero
    val.setVal(0.);
    EXPECT_EQ(val.value(), 0.);
    EXPECT_EQ(val.oldValue(), 0.);
    EXPECT_EQ(val.expValue(), exp(0.));
    EXPECT_EQ(val.oldExpValue(), exp(0.));
}

TEST(TValueUpdatedWithExponentialTest, set){
    TValueUpdatedWithExponential<double> val;
    // positive
    val.set(0.837);
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.);
    EXPECT_EQ(val.expValue(), exp(0.837));
    EXPECT_EQ(val.oldExpValue(), exp(0.));

    // negative
    val.set(-2.74);
    EXPECT_EQ(val.value(), - 2.74);
    EXPECT_EQ(val.oldValue(), 0.837);
    EXPECT_EQ(val.expValue(), exp(- 2.74));
    EXPECT_EQ(val.oldExpValue(), exp(0.837));

    // zero
    val.set(0.);
    EXPECT_EQ(val.value(), 0.);
    EXPECT_EQ(val.oldValue(), - 2.74);
    EXPECT_EQ(val.expValue(), exp(0.));
    EXPECT_EQ(val.oldExpValue(), exp(- 2.74));
}

TEST(TValueUpdatedWithExponentialTest, reset){
    TValueUpdatedWithExponential<double> val;
    // first set to something which is not 0
    val.set(10.3);

    // reset
    val.reset();
    EXPECT_EQ(val.value(), 0.);
    EXPECT_EQ(val.oldValue(), 0.);
    EXPECT_EQ(val.expValue(), exp(0.));
    EXPECT_EQ(val.oldExpValue(), exp(0.));
}

// int
TEST(TValueUpdatedWithExponentialTest, integer){
    TValueUpdatedWithExponential<int> val;

    // init
    EXPECT_EQ(val.value(), 0);
    EXPECT_EQ(val.oldValue(), 0);
    EXPECT_EQ(val.expValue(), exp(0));
    EXPECT_EQ(val.oldExpValue(), exp(0));

    // initBoth
    val.initBoth(1);
    EXPECT_EQ(val.value(), 1);
    EXPECT_EQ(val.oldValue(), 1);
    EXPECT_EQ(val.expValue(), exp(1));
    EXPECT_EQ(val.oldExpValue(), exp(1));

    // set
    val.set(3);
    EXPECT_EQ(val.value(), 3);
    EXPECT_EQ(val.oldValue(), 1);
    EXPECT_EQ(val.expValue(), exp(3));
    EXPECT_EQ(val.oldExpValue(), exp(1));

    // setVal
    val.setVal(4);
    EXPECT_EQ(val.value(), 4);
    EXPECT_EQ(val.oldValue(), 1);
    EXPECT_EQ(val.expValue(), exp(4));
    EXPECT_EQ(val.oldExpValue(), exp(1));

    // set
    val.set(-4);
    EXPECT_EQ(val.value(), -4);
    EXPECT_EQ(val.oldValue(), 4);
    EXPECT_EQ(val.expValue(), exp(-4));
    EXPECT_EQ(val.oldExpValue(), exp(4));

    // reset
    val.reset();
    EXPECT_EQ(val.value(), 4);
    EXPECT_EQ(val.oldValue(), 4);
    EXPECT_EQ(val.expValue(), exp(4));
    EXPECT_EQ(val.oldExpValue(), exp(4));
}

// bool
TEST(TValueUpdatedWithExponentialTest, boolean){
    TValueUpdatedWithExponential<bool> val;
    EXPECT_EQ(val.value(), false);
    EXPECT_EQ(val.oldValue(), false);
    EXPECT_EQ(val.expValue(), exp(0));
    EXPECT_EQ(val.oldExpValue(), exp(0));

    // set
    val.set(true);
    EXPECT_EQ(val.value(), true);
    EXPECT_EQ(val.oldValue(), false);
    EXPECT_EQ(val.expValue(), exp(1));
    EXPECT_EQ(val.oldExpValue(), exp(0));
}

//-------------------------------------------

TEST(TValueUpdatedWithLogTest, init){
    TValueUpdatedWithLog<double> val;
    EXPECT_EQ(val.value(), 0.000001);
    EXPECT_EQ(val.oldValue(), 0.000001);
    EXPECT_EQ(val.logValue(), log(0.000001));
    EXPECT_EQ(val.oldLogValue(), log(0.000001));
}

TEST(TValueUpdatedWithLogTest, initBoth){
    TValueUpdatedWithLog<double> val;
    // positive
    val.initBoth(0.837);
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.837);
    EXPECT_EQ(val.logValue(), log(0.837));
    EXPECT_EQ(val.oldLogValue(), log(0.837));

    // negative
    EXPECT_DEATH(val.initBoth(-2.74), ""); // negative is invalid for log

    // zero
    EXPECT_DEATH(val.initBoth(0.), ""); // 0 is invalid for log
}

TEST(TValueUpdatedWithLogTest, setVal){
    TValueUpdatedWithLog<double> val;
    // positive
    val.setVal(0.837);
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.000001);
    EXPECT_EQ(val.logValue(), log(0.837));
    EXPECT_EQ(val.oldLogValue(), log(0.000001));

    // negative
    EXPECT_DEATH(val.setVal(-2.74), ""); // negative is invalid for log

    // zero
    EXPECT_DEATH(val.setVal(0), ""); // negative is invalid for log
}

TEST(TValueUpdatedWithLogTest, set){
    TValueUpdatedWithLog<double> val;
    // positive
    val.set(0.837);
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.000001);
    EXPECT_EQ(val.logValue(), log(0.837));
    EXPECT_EQ(val.oldLogValue(), log(0.000001));

    // negative
    EXPECT_DEATH(val.set(-2.74), ""); // negative is invalid for log

    // zero
    EXPECT_DEATH(val.set(0), ""); // negative is invalid for log
}

TEST(TValueUpdatedWithLogTest, reset){
    TValueUpdatedWithLog<double> val;
    // first set to something which is not 0
    val.set(10.3);

    // reset
    val.reset();
    EXPECT_EQ(val.value(), 0.000001);
    EXPECT_EQ(val.oldValue(), 0.000001);
    EXPECT_EQ(val.logValue(), log(0.000001));
    EXPECT_EQ(val.oldLogValue(), log(0.000001));
}

// int
TEST(TValueUpdatedWithLogTest, integer){
    TValueUpdatedWithLog<int> val;

    // init
    EXPECT_EQ(val.value(), 1);
    EXPECT_EQ(val.oldValue(), 1);
    EXPECT_EQ(val.logValue(), 0);
    EXPECT_EQ(val.oldLogValue(), 0);

    // initBoth
    val.initBoth(1);
    EXPECT_EQ(val.value(), 1);
    EXPECT_EQ(val.oldValue(), 1);
    EXPECT_EQ(val.logValue(), log(1));
    EXPECT_EQ(val.oldLogValue(), log(1));

    // set
    val.set(3);
    EXPECT_EQ(val.value(), 3);
    EXPECT_EQ(val.oldValue(), 1);
    EXPECT_EQ(val.logValue(), log(3));
    EXPECT_EQ(val.oldLogValue(), log(1));

    // setVal
    val.setVal(4);
    EXPECT_EQ(val.value(), 4);
    EXPECT_EQ(val.oldValue(), 1);
    EXPECT_EQ(val.logValue(), log(4));
    EXPECT_EQ(val.oldLogValue(), log(1));

    // set
    val.set(8);
    EXPECT_EQ(val.value(), 8);
    EXPECT_EQ(val.oldValue(), 4);
    EXPECT_EQ(val.logValue(), log(8));
    EXPECT_EQ(val.oldLogValue(), log(4));

    // reset
    val.reset();
    EXPECT_EQ(val.value(), 4);
    EXPECT_EQ(val.oldValue(), 4);
    EXPECT_EQ(val.logValue(), log(4));
    EXPECT_EQ(val.oldLogValue(), log(4));
}

// bool
TEST(TValueUpdatedWithLogTest, boolean){
    TValueUpdatedWithLog<bool> val;
    EXPECT_EQ(val.value(), true);
    EXPECT_EQ(val.oldValue(), true);
    EXPECT_EQ(val.logValue(), log(1));
    EXPECT_EQ(val.oldLogValue(), log(1));

    EXPECT_DEATH(val.set(false), ""); // 0 is invalid for log
}

//-------------------------------------------

TEST(TValueUpdatedWithLogAndExponentialTest, init){
    TValueUpdatedWithLogAndExponential<double> val;
    EXPECT_EQ(val.value(), 0.000001);
    EXPECT_EQ(val.oldValue(), 0.000001);
    EXPECT_EQ(val.logValue(), log(0.000001));
    EXPECT_EQ(val.oldLogValue(), log(0.000001));
    EXPECT_EQ(val.expValue(), exp(0.000001));
    EXPECT_EQ(val.oldExpValue(), exp(0.000001));
}

TEST(TValueUpdatedWithLogAndExponentialTest, initBoth){
    TValueUpdatedWithLogAndExponential<double> val;
    // positive
    val.initBoth(0.837);
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.837);
    EXPECT_EQ(val.logValue(), log(0.837));
    EXPECT_EQ(val.oldLogValue(), log(0.837));
    EXPECT_EQ(val.expValue(), exp(0.837));
    EXPECT_EQ(val.oldExpValue(), exp(0.837));
    // negative
    EXPECT_DEATH(val.initBoth(-2.74), ""); // negative is invalid for log

    // zero
    EXPECT_DEATH(val.initBoth(0.), ""); // 0 is invalid for log
}

TEST(TValueUpdatedWithLogAndExponentialTest, setVal){
    TValueUpdatedWithLogAndExponential<double> val;
    // positive
    val.setVal(0.837);
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.000001);
    EXPECT_EQ(val.logValue(), log(0.837));
    EXPECT_EQ(val.oldLogValue(), log(0.000001));
    EXPECT_EQ(val.expValue(), exp(0.837));
    EXPECT_EQ(val.oldExpValue(), exp(0.000001));
    // negative
    EXPECT_DEATH(val.setVal(-2.74), ""); // negative is invalid for log

    // zero
    EXPECT_DEATH(val.setVal(0), ""); // negative is invalid for log
}

TEST(TValueUpdatedWithLogAndExponentialTest, set){
    TValueUpdatedWithLogAndExponential<double> val;
    // positive
    val.set(0.837);
    EXPECT_EQ(val.value(), 0.837);
    EXPECT_EQ(val.oldValue(), 0.000001);
    EXPECT_EQ(val.logValue(), log(0.837));
    EXPECT_EQ(val.oldLogValue(), log(0.000001));
    EXPECT_EQ(val.expValue(), exp(0.837));
    EXPECT_EQ(val.oldExpValue(), exp(0.000001));

    // negative
    EXPECT_DEATH(val.set(-2.74), ""); // negative is invalid for log

    // zero
    EXPECT_DEATH(val.set(0), ""); // negative is invalid for log
}

TEST(TValueUpdatedWithLogAndExponentialTest, reset){
    TValueUpdatedWithLogAndExponential<double> val;
    // first set to something which is not 0
    val.set(10.3);

    // reset
    val.reset();
    EXPECT_EQ(val.value(), 0.000001);
    EXPECT_EQ(val.oldValue(), 0.000001);
    EXPECT_EQ(val.logValue(), log(0.000001));
    EXPECT_EQ(val.oldLogValue(), log(0.000001));
    EXPECT_EQ(val.expValue(), exp(0.000001));
    EXPECT_EQ(val.oldExpValue(), exp(0.000001));
}

// int
TEST(TValueUpdatedWithLogAndExponentialTest, integer){
    TValueUpdatedWithLogAndExponential<int> val;

    // init
    EXPECT_EQ(val.value(), 1);
    EXPECT_EQ(val.oldValue(), 1);
    EXPECT_EQ(val.logValue(), 0);
    EXPECT_EQ(val.oldLogValue(), 0);
    EXPECT_EQ(val.expValue(), exp(1));
    EXPECT_EQ(val.oldExpValue(), exp(1));

    // initBoth
    val.initBoth(1);
    EXPECT_EQ(val.value(), 1);
    EXPECT_EQ(val.oldValue(), 1);
    EXPECT_EQ(val.logValue(), log(1));
    EXPECT_EQ(val.oldLogValue(), log(1));
    EXPECT_EQ(val.expValue(), exp(1));
    EXPECT_EQ(val.oldExpValue(), exp(1));

    // set
    val.set(3);
    EXPECT_EQ(val.value(), 3);
    EXPECT_EQ(val.oldValue(), 1);
    EXPECT_EQ(val.logValue(), log(3));
    EXPECT_EQ(val.oldLogValue(), log(1));
    EXPECT_EQ(val.expValue(), exp(3));
    EXPECT_EQ(val.oldExpValue(), exp(1));

    // setVal
    val.setVal(4);
    EXPECT_EQ(val.value(), 4);
    EXPECT_EQ(val.oldValue(), 1);
    EXPECT_EQ(val.logValue(), log(4));
    EXPECT_EQ(val.oldLogValue(), log(1));
    EXPECT_EQ(val.expValue(), exp(4));
    EXPECT_EQ(val.oldExpValue(), exp(1));

    // set
    val.set(8);
    EXPECT_EQ(val.value(), 8);
    EXPECT_EQ(val.oldValue(), 4);
    EXPECT_EQ(val.logValue(), log(8));
    EXPECT_EQ(val.oldLogValue(), log(4));
    EXPECT_EQ(val.expValue(), exp(8));
    EXPECT_EQ(val.oldExpValue(), exp(4));

    // reset
    val.reset();
    EXPECT_EQ(val.value(), 4);
    EXPECT_EQ(val.oldValue(), 4);
    EXPECT_EQ(val.logValue(), log(4));
    EXPECT_EQ(val.oldLogValue(), log(4));
    EXPECT_EQ(val.expValue(), exp(4));
    EXPECT_EQ(val.oldExpValue(), exp(4));
}

// bool
TEST(TValueUpdatedWithLogAndExponentialTest, boolean){
    TValueUpdatedWithLogAndExponential<bool> val;

    EXPECT_EQ(val.value(), true);
    EXPECT_EQ(val.oldValue(), true);
    EXPECT_EQ(val.logValue(), 0);
    EXPECT_EQ(val.oldLogValue(), 0);
    EXPECT_EQ(val.expValue(), exp(1));
    EXPECT_EQ(val.oldExpValue(), exp(1));

    EXPECT_DEATH(val.set(false), ""); // 0 is invalid for log
}
