//
// Created by madleina on 24.03.21.
//

#include "../../core/TNewtonRaphson.h"
#include "../TestCase.h"
using namespace testing;

//---------------------------------
// One-Dimensional Newton-Raphson
//---------------------------------

class TPolynomial {
    // use polynomial function y = x^3 - 2*x + 2
public:
    TPolynomial(){};

    double polynomial(const double & x) {
        return x*x*x - 2*x + 2;
    }

    double firstDerivative(const double & x){
        return 3.*x*x - 2.;
    }
};

class TNewtonRaphsonTest : public Test {
public:

    TPolynomial polynomial;
    TNewtonRaphson newtonRaphson;
    TLog logfile;
    TRandomGenerator randomGenerator;

    // solution
    double root = -1.769292; // from WolframAlpha
};

TEST_F(TNewtonRaphsonTest, newtonRaphson_bisection){
    auto ptr1 = &TPolynomial::polynomial;
    auto ptr2 = &TPolynomial::firstDerivative;
    //newtonRaphson.report(&logfile);
    newtonRaphson.setEpsilon(10e-10);
    newtonRaphson.setMaxIterations(100000);

    size_t numRep = 1000;
    for (size_t i = 0; i < numRep; i++){
        // get random starting value and initial step size
        double min = randomGenerator.getRand(-10., root);
        double max = randomGenerator.getRand(root, 10.);
        double result = newtonRaphson.runNewtonRaphson_withBisection(polynomial, ptr1, ptr2, min, max);
        EXPECT_FLOAT_EQ(result, root);
    }
}

TEST_F(TNewtonRaphsonTest, newtonRaphson_backtracking){
    auto ptr1 = &TPolynomial::polynomial;
    auto ptr2 = &TPolynomial::firstDerivative;
    //newtonRaphson.report(&logfile);
    newtonRaphson.setEpsilon(10e-10);
    newtonRaphson.setMaxIterations(100000);

    size_t numRep = 1000;
    for (size_t i = 0; i < numRep; i++){
        // get random starting value and initial step size
        double start = randomGenerator.getRand(-10., 10.);
        double result = newtonRaphson.runNewtonRaphson_withBacktracking(polynomial, ptr1, ptr2, start);
        EXPECT_FLOAT_EQ(result, root);
    }
}

//---------------------------------
// Multi-Dimensional Newton-Raphson
//---------------------------------
class THimmelblau {
    // use Himmelblau's function: https://en.wikipedia.org/wiki/Himmelblau%27s_function
    // this is a nice example of a function that should be optimized
    // solutions are known -> we can check if we get close to these!
public:
    THimmelblau(){};

    double himmelblau(const std::vector<double> & Values) {
        assert(Values.size() == 2);
        double x = Values[0];
        double y = Values[1];

        double tmp1 = x * x + y - 11;
        double tmp2 = x + y * y - 7;
        return tmp1 * tmp1 + tmp2 * tmp2;
    }

    std::vector<double> gradient(const std::vector<double> & Values){
        assert(Values.size() == 2);
        double x = Values[0];
        double y = Values[1];

        std::vector<double> gradient(2);
        gradient[0] = 4. * x * (x*x + y - 11.) + 2. * (x + y*y - 7.);
        gradient[1] = 2. * (x*x + y - 11.) + 4. * y * (x + y*y - 7.);

        return gradient;
    }

    arma::mat hessian(const std::vector<double> & Values, const std::vector<double> & Gradient){
        assert(Values.size() == 2);
        double x = Values[0];
        double y = Values[1];

        arma::mat hessian(2, 2);
        hessian(0,0) = 4. * (x*x + y - 11.) + 8. * x*x + 2.;
        hessian(0,1) = 4. * x + 4. * y;
        hessian(1,0) = 4. * x + 4. * y;
        hessian(1,1) = 4. * (x + y*y -  7.) + 8. * y*y + 2.;

        return hessian;
    }
};


class TMultiDimensionalNewtonRaphsonTest : public Test {
public:
    THimmelblau himmelblau;
    TMultiDimensionalNewtonRaphson newtonRaphson;
    TLog logfile;
    TRandomGenerator randomGenerator;

    // there are 4 possible solutions for minima's for Himmelblau's function (from https://en.wikipedia.org/wiki/Himmelblau%27s_function)
    std::vector<std::vector<double>> minima = {{3., 2.},
                                               {-2.805118, 3.131312},
                                               {-3.779310, -3.283186},
                                               {3.584428, -1.848126}};
};

TEST_F(TMultiDimensionalNewtonRaphsonTest, newtonRaphson){
    auto ptr1 = &THimmelblau::gradient;
    auto ptr2 = &THimmelblau::hessian;
    newtonRaphson.report(&logfile);

    size_t numRep = 1000;
    size_t counterFailed = 0;
    double absDifference = 10e-5;
    randomGenerator.setSeed(1, true);
    for (size_t i = 0; i < numRep; i++){
        bool convergedOnDeltaX = false;
        std::vector<double> values;
        do {
            // restart
            // get random starting value and initial step size
            double start_x = randomGenerator.getRand(-10., 10.);
            double start_y = randomGenerator.getRand(-10., 10.);
            values = {start_x, start_y};
            newtonRaphson.runNewtonRaphson_withBacktracking(himmelblau, ptr1, ptr2, values, convergedOnDeltaX);
        } while(convergedOnDeltaX);

        // check if they are somewhat close to known minima
        bool closeToMinimum = false;
        for (auto & knownMinimum : minima){
            // check if both x and y are close to known minimum
            if (std::fabs(knownMinimum[0] - values[0]) < absDifference && std::fabs(knownMinimum[1] - values[1]) < absDifference){
                closeToMinimum = true;
                break;
            }
        }
        // check if the found minimum is not close to any known minimum
        // todo: algorithm frequently converges to saddle points (-0.127961, -1.95371), (-0.270845, -0.923039), (0.0866775, 2.88425), (3.38515, 0.0738519) -> derivative is zero
        // -> we could check if this is the case with hess.is_sympd() -> at saddle points, the Hessian is not positive definite
        // Numerical Recipes doesn't do this, why? Should we add this check??
        if (!closeToMinimum){
            std::cout << values[0] << ", " << values[1] << std::endl;
            std::cout << himmelblau.himmelblau(values) << std::endl;
            auto grad = himmelblau.gradient(values);
            std::cout << grad[0] << ", " << grad[1] << std::endl;
            auto hess = himmelblau.hessian(values, grad);
            std::cout << hess(0,0) << ", " << hess(0,1) << ", " <<  hess(1,0) << ", " <<  hess(1,1) << std::endl;
            if (!hess.is_sympd()){
                std::cout << "Matrix is not positive definite -> Saddle point!" << std::endl;
            }
            counterFailed++;
        }
    }

    EXPECT_EQ(counterFailed, 0);
}

TEST_F(TMultiDimensionalNewtonRaphsonTest, newtonRaphson_JacobianApproximation){
    auto ptr1 = &THimmelblau::gradient;
    TApproxJacobian approxJacobian(himmelblau, ptr1);
    auto ptr2 = &TApproxJacobian<THimmelblau, typeof(ptr1)>::approximateJacobian;
    newtonRaphson.report(&logfile);

    size_t numRep = 1000;
    size_t counterFailed = 0;
    double absDifference = 10e-5;
    for (size_t i = 0; i < numRep; i++){
        bool convergedOnDeltaX = false;
        std::vector<double> values;
        do {
            // restart
            // get random starting value and initial step size
            double start_x = randomGenerator.getRand(-10., 10.);
            double start_y = randomGenerator.getRand(-10., 10.);
            values = {start_x, start_y};
            newtonRaphson.runNewtonRaphson_withBacktracking(himmelblau, ptr1, approxJacobian, ptr2, values, convergedOnDeltaX);
        } while(convergedOnDeltaX);

        // check if they are somewhat close to known minima
        bool closeToMinimum = false;
        for (auto & knownMinimum : minima){
            // check if both x and y are close to known minimum
            if (std::fabs(knownMinimum[0] - values[0]) < absDifference && std::fabs(knownMinimum[1] - values[1]) < absDifference){
                closeToMinimum = true;
                break;
            }
        }
        // check if the found minimum is not close to any known minimum
        if (!closeToMinimum){
            std::cout << values[0] << ", " << values[1] << std::endl;
            std::cout << himmelblau.himmelblau(values) << std::endl;
            auto grad = himmelblau.gradient(values);
            std::cout << grad[0] << ", " << grad[1] << std::endl;
            auto hess = himmelblau.hessian(values, grad);
            std::cout << hess(0,0) << ", " << hess(0,1) << ", " <<  hess(1,0) << ", " <<  hess(1,1) << std::endl;
            if (!hess.is_sympd()){
                std::cout << "Matrix is not positive definite -> Saddle point!" << std::endl;
            }
            counterFailed++;
        }
    }

    EXPECT_EQ(counterFailed, 0);
}


