//
// Created by madleina on 17.02.21.
//

#include "../../core/TNelderMead.h"
#include "../TestCase.h"
using namespace testing;

class THimmelblauSimple {
    // use Himmelblau's function: https://en.wikipedia.org/wiki/Himmelblau%27s_function
    // this is a nice example of a function that should be optimized
    // solutions are known -> we can check if we get close to these!
public:
    THimmelblauSimple(){};

    double himmelblau(const std::vector<double> & Values) {
        assert(Values.size() == 2);
        double x = Values[0];
        double y = Values[1];

        double tmp1 = x * x + y - 11;
        double tmp2 = x + y * y - 7;
        return tmp1 * tmp1 + tmp2 * tmp2;
    }
};

class TNelderMeadTest : public Test {
public:

    THimmelblauSimple himmelblau;
    TNelderMead nelderMead;
    TRandomGenerator randomGenerator;

    // there are 4 possible solutions for minima's for Himmelblau's function (from https://en.wikipedia.org/wiki/Himmelblau%27s_function)
    std::vector<std::vector<double>> minima = {{3., 2.},
                                               {-2.805118, 3.131312},
                                               {-3.779310, -3.283186},
                                               {3.584428, -1.848126}};
};

TEST_F(TNelderMeadTest, minimize_delIsOneNumber){

    // run algorithm 1000x, draw random starting values for simplex and check if we get close to one of the minima
    size_t numReplicates = 1000;
    size_t counterFailed = 0;
    double absDifference = 0.01;
    for (size_t rep = 0; rep < numReplicates; rep++){
        // get initial starting values (between -4 and 4, this the "interesting" range for Himmelblau's function
        TVertex initialVertex(2);
        initialVertex[0] = randomGenerator.getRand(-4., 4.);
        initialVertex[1] = randomGenerator.getRand(-4., 4.);

        // define displacement for initialVertex
        double displacement = 2.;

        // run Nelder-Mead
        auto ptr = &THimmelblauSimple::himmelblau;
        TVertex foundMinimum = nelderMead.minimize(initialVertex, displacement, himmelblau, ptr);

        // check if they are somewhat close to known minima
        bool closeToMinimum = false;
        for (auto & knownMinimum : minima){
            // check if both x and y are close to known minimum
            if (std::fabs(knownMinimum[0] - foundMinimum[0]) < absDifference && std::fabs(knownMinimum[1] - foundMinimum[1]) < absDifference){
                closeToMinimum = true;
                break;
            }
        }
        // check if the found minimum is not close to any known minimum
        if (!closeToMinimum){
            counterFailed++;
        }
    }

    EXPECT_EQ(counterFailed, 0);
}


TEST_F(TNelderMeadTest, minimize_delIsVector){

    // run algorithm 1000x, draw random starting values for simplex and check if we get close to one of the minima
    size_t numReplicates = 1000;
    size_t counterFailed = 0;
    double absDifference = 0.0001;
    for (size_t rep = 0; rep < numReplicates; rep++){
        // get initial starting values (between -4 and 4, this the "interesting" range for Himmelblau's function)
        // create initial vertex
        TVertex initialVertex(2);
        initialVertex[0] = randomGenerator.getRand(-4., 4.);
        initialVertex[1] = randomGenerator.getRand(-4., 4.);

        // define displacement vector for initialVertex
        double del1 = randomGenerator.getRand(-3., 3.);
        double del2 = randomGenerator.getRand(-3., 3.);
        std::vector<double> displacements = {del1, del2};

        // run Nelder-Mead
        auto ptr = &THimmelblauSimple::himmelblau;
        TVertex foundMinimum = nelderMead.minimize(initialVertex, displacements, himmelblau,  ptr);

        // check if they are somewhat close to known minima
        bool closeToMinimum = false;
        for (auto & knownMinimum : minima){
            // check if both x and y are close to known minimum
            if (std::fabs(knownMinimum[0] - foundMinimum[0]) < absDifference && std::fabs(knownMinimum[1] - foundMinimum[1]) < absDifference){
                closeToMinimum = true;
                break;
            }
        }
        // check if the found minimum is not close to any known minimum
        if (!closeToMinimum){
            counterFailed++;
        }
    }

    EXPECT_EQ(counterFailed, 0);
}


TEST_F(TNelderMeadTest, minimize_provideInitialSimplex){

    // run algorithm 1000x, draw random starting values for simplex and check if we get close to one of the minima
    size_t numReplicates = 1000;
    size_t counterFailed = 0;
    double absDifference = 0.0001;
    for (size_t rep = 0; rep < numReplicates; rep++){
        // get initial starting values (between -4 and 4, this the "interesting" range for Himmelblau's function)
        // create initial simplex
        TSimplex initialSimplex(2);
        for (size_t vertex = 0; vertex < 3; vertex++){
            initialSimplex[vertex][0] = randomGenerator.getRand(-4., 4.);
            initialSimplex[vertex][1] = randomGenerator.getRand(-4., 4.);
        }

        // run Nelder-Mead
        auto ptr = &THimmelblauSimple::himmelblau;
        TVertex foundMinimum = nelderMead.minimize(initialSimplex, himmelblau, ptr);

        // check if they are somewhat close to known minima
        bool closeToMinimum = false;
        for (auto & knownMinimum : minima){
            // check if both x and y are close to known minimum
            if (std::fabs(knownMinimum[0] - foundMinimum[0]) < absDifference && std::fabs(knownMinimum[1] - foundMinimum[1]) < absDifference){
                closeToMinimum = true;
                break;
            }
        }
        // check if the found minimum is not close to any known minimum
        if (!closeToMinimum){
            counterFailed++;
        }
    }

    EXPECT_EQ(counterFailed, 0);
}
