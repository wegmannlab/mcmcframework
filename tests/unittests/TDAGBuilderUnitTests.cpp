//
// Created by caduffm on 7/7/20.
//

#include "../TestCase.h"
#include "../../core/TMCMC.h"

using namespace testing;

//-------------------------------------------
// TDAGBuilder
//-------------------------------------------
template<class T>
struct TDAGBuilderFunctor {
    // function to pass to DAG: contains project-specific priors (none for us)
    std::shared_ptr<TPrior<T>> operator()(const std::string & priorName){
        return std::shared_ptr<TPrior<T>>(nullptr); // return nullptr: we don't have specific priors
    }
};

class BridgeTDAGBuilder : public TDAGBuilder {
public:
    BridgeTDAGBuilder(TRandomGenerator * RandomGenerator, TLog * logfile) : TDAGBuilder(RandomGenerator, logfile){};
    ~BridgeTDAGBuilder() override = default;

    void _bundleParametersWithSharedPriors(std::vector<std::vector<std::shared_ptr<TDefinitionBase>>> & definitionsWithSharedPriorsVector){
        TDAGBuilder::_bundleParametersWithSharedPriors(definitionsWithSharedPriorsVector);
    }
    void _buildDAG(bool throwIfTypesDontMatch, TParameters & Parameters){
        TDAGBuilder::_buildDAG<TDAGBuilderFunctor>(throwIfTypesDontMatch, Parameters);
    }
    bool _definitionExistsInVector(const std::string & name, std::vector< std::vector<std::shared_ptr<TDefinitionBase>> > & definitionsWithSharedPriorsVector){
        return TDAGBuilder::_definitionExistsInVector(name, definitionsWithSharedPriorsVector);
    }
    bool _definitionsSharePrior(std::shared_ptr<TDefinitionBase> & paramDef, std::shared_ptr<TDefinitionBase> & otherParamDef){
        return TDAGBuilder::_definitionsSharePrior(paramDef, otherParamDef);
    }
    void _findHyperParameters(std::shared_ptr<TDefinitionBase> & paramDef, std::vector<std::shared_ptr<TMCMCParameterBase> > & initializedParams, std::vector<std::shared_ptr<TObservationsBase> > & initializedObservations){
        TDAGBuilder::_findHyperParameters(paramDef, initializedParams, initializedObservations);
    }
    void _prepareFiles(MCMCFile::Filetypes mcmcFiletype, std::vector<std::unique_ptr<TMCMCFile> > & fileVec, const std::string & prefix){
        TDAGBuilder::_prepareFiles(mcmcFiletype, fileVec, prefix);
    }
    std::shared_ptr<TPrior<double>> _initPrior(const std::string & priorName, std::string priorParams, const std::string & paramName) {
        return TDAGBuilder::_initPrior<double>(priorName, priorParams, paramName);
    }
    std::shared_ptr<TPrior<float>> _initPriorFloat(const std::string & priorName, std::string priorParams, const std::string & paramName) {
        return TDAGBuilder::_initPrior<float>(priorName, priorParams, paramName);
    }
    std::shared_ptr<TPrior<int>> _initPriorInt(const std::string & priorName, std::string priorParams, const std::string & paramName) {
        return TDAGBuilder::_initPrior<int>(priorName, priorParams, paramName);
    }
    std::shared_ptr<TPrior<bool>> _initPriorBool(const std::string & priorName, std::string priorParams, const std::string & paramName) {
        return TDAGBuilder::_initPrior<bool>(priorName, priorParams, paramName);
    }
    void _checkMinMaxPrior(const std::shared_ptr<TPrior<double>> & prior, std::shared_ptr<TDefinitionBase> & def)  {
        TDAGBuilder::_checkMinMaxPrior(prior, def);
    }
    std::shared_ptr<TPrior<double>> _initPriorWithHyperPrior(const std::string & priorName, std::vector<std::shared_ptr<TMCMCParameterBase> > & initializedParams, std::vector<std::shared_ptr<TObservationsBase>> & initializedObservations, const std::string& paramName) {
        return TDAGBuilder::_initPriorWithHyperPrior<TDAGBuilderFunctor, double>(priorName, initializedParams, initializedObservations, paramName);
    }
    void _buildParams(std::vector<std::shared_ptr<TDefinitionBase>> & defs, std::vector<std::shared_ptr<TMCMCParameterBase>> & initializedParams, std::vector<std::shared_ptr<TObservationsBase>> & initializedObservations, TParameters & Parameters){
        return TDAGBuilder::_buildParams<TDAGBuilderFunctor, double>(defs, initializedParams, initializedObservations, Parameters);
    }
    std::map<std::string, std::shared_ptr<TObservationsBase>> getObservations(){
        return TDAGBuilder::_nameToObservationsMap;
    }
    std::map<std::string, std::shared_ptr<TMCMCParameterBase>> getParameters(){
        return TDAGBuilder::_nameToParamMap;
    }
    std::vector<std::unique_ptr<TMCMCFile> > & getTraceFiles(){
        return TDAGBuilder::_traceFiles;
    }
    std::vector<std::unique_ptr<TMCMCFile> > & getMeanVarFiles(){
        return TDAGBuilder::_meanVarFiles;
    }
    std::shared_ptr<TMCMCStateFile> & getStateFile(){
        return TDAGBuilder::_stateFile;
    }
    TDAG getDAG(){
        return TDAGBuilder::_dag;
    }
    void _prepareAllMCMCFiles(const std::string & Prefix, bool WriteStateFile){
        TDAGBuilder::_prepareAllMCMCFiles(Prefix, WriteStateFile);
    }
};

class TDAGBuilderTest : public Test {
public:
    TLog logfile;
    TRandomGenerator randomGenerator;
    TParameters parameters;
    BridgeTDAGBuilder *DAGBuilder = nullptr;
    std::vector<std::vector<std::shared_ptr<TDefinitionBase>>> definitionsWithSharedPriorsVector;
    std::vector<std::shared_ptr<TMCMCParameterBase> > initializedParams;
    std::vector<std::shared_ptr<TObservationsBase> > initializedObservations;

    void SetUp() override {
        DAGBuilder = new BridgeTDAGBuilder(&randomGenerator, &logfile);
        initializedObservations = {};
    }

    void TearDown() override {
        delete DAGBuilder;
    }
};

TEST_F(TDAGBuilderTest, addParameter) {
    TParameterDefinition def("def");
    DAGBuilder->addParameter(def);
    // other name
    TParameterDefinition def1("def1");
    DAGBuilder->addParameter(def1);
    // same name
    TParameterDefinition def2("def1");
    EXPECT_THROW(DAGBuilder->addParameter(def2), std::runtime_error);
}

TEST_F(TDAGBuilderTest, addObservation) {
    TObservationDefinition def("def");
    DAGBuilder->addObservation(def);
    // other name
    TObservationDefinition def1("def1");
    DAGBuilder->addObservation(def1);
    // same name
    TObservationDefinition def2("def1");
    EXPECT_THROW(DAGBuilder->addObservation(def2), std::runtime_error);
}

TEST_F(TDAGBuilderTest, addObservation_addParameter) {
    TObservationDefinition def("def");
    DAGBuilder->addObservation(def);
    // other name
    TParameterDefinition def1("def1");
    DAGBuilder->addParameter(def1);
    // same name
    TObservationDefinition def2("def1");
    EXPECT_THROW(DAGBuilder->addObservation(def2), std::runtime_error);
    TParameterDefinition def3("def1");
    EXPECT_THROW(DAGBuilder->addParameter(def3), std::runtime_error);
}

// test: buildDAG()
TEST_F(TDAGBuilderTest, buildDAG) {
    TObservationDefinition def1("def1");
    DAGBuilder->addObservation(def1);

    EXPECT_NO_THROW(DAGBuilder->_buildDAG(true, parameters));
}

TEST_F(TDAGBuilderTest, _definitionExistsInVector) {
    TParameterDefinition def1("def1");
    auto def1Ptr = std::make_shared<TParameterDefinition>(def1);
    EXPECT_FALSE(DAGBuilder->_definitionExistsInVector(def1.name(), definitionsWithSharedPriorsVector));
    definitionsWithSharedPriorsVector.push_back({def1Ptr});
    EXPECT_TRUE(DAGBuilder->_definitionExistsInVector(def1.name(), definitionsWithSharedPriorsVector));

    // add observation
    TObservationDefinition def2("def2");
    EXPECT_FALSE(DAGBuilder->_definitionExistsInVector(def2.name(), definitionsWithSharedPriorsVector));
    auto def2Ptr = std::make_shared<TObservationDefinition>(def2);
    definitionsWithSharedPriorsVector.push_back({def2Ptr});
    EXPECT_TRUE(DAGBuilder->_definitionExistsInVector(def2.name(), definitionsWithSharedPriorsVector));
}

TEST_F(TDAGBuilderTest, _definitionsSharePrior_letters) {
    // do share prior (same prior, parameters and type)
    TParameterDefinition def1("def1");
    TParameterDefinition def2("def2");
    def1.setPrior("normal"); def2.setPrior("normal");
    def1.setPriorParams("mu, sd"); def2.setPriorParams("mu, sd");
    def1.setType("int"); def2.setType("int");
    std::shared_ptr<TDefinitionBase> def1Ptr = std::make_shared<TParameterDefinition>(def1);
    std::shared_ptr<TDefinitionBase> def2Ptr = std::make_shared<TParameterDefinition>(def2);

    EXPECT_TRUE(DAGBuilder->_definitionsSharePrior(def1Ptr, def2Ptr));
}

TEST_F(TDAGBuilderTest, _definitionsSharePrior_numbers) {
    // do share prior (same prior, parameters and type)
    TParameterDefinition def1("def1");
    TParameterDefinition def2("def2");
    def1.setPrior("normal"); def2.setPrior("normal");
    def1.setPriorParams("0, 1"); def2.setPriorParams("0, 1");
    def1.setType("int"); def2.setType("int");
    std::shared_ptr<TDefinitionBase> def1Ptr = std::make_shared<TParameterDefinition>(def1);
    std::shared_ptr<TDefinitionBase> def2Ptr = std::make_shared<TParameterDefinition>(def2);

    EXPECT_TRUE(DAGBuilder->_definitionsSharePrior(def1Ptr, def2Ptr));
}

TEST_F(TDAGBuilderTest, _definitionsSharePrior_differentParameters_numbers){
    // do share prior (same prior, parameters and type)
    TParameterDefinition def1("def1");
    TParameterDefinition def2("def2");
    def1.setPrior("normal"); def2.setPrior("normal");
    def1.setPriorParams("0, -1"); def2.setPriorParams("0, 1");
    def1.setType("int"); def2.setType("int");
    std::shared_ptr<TDefinitionBase> def1Ptr = std::make_shared<TParameterDefinition>(def1);
    std::shared_ptr<TDefinitionBase> def2Ptr = std::make_shared<TParameterDefinition>(def2);

    EXPECT_FALSE(DAGBuilder->_definitionsSharePrior(def1Ptr, def2Ptr));
}

TEST_F(TDAGBuilderTest, _definitionsSharePrior_differentParameters_letters){
    // do share prior (same prior, parameters and type)
    TParameterDefinition def1("def1");
    TParameterDefinition def2("def2");
    def1.setPrior("normal"); def2.setPrior("normal");
    def1.setPriorParams("a, b"); def2.setPriorParams("b, c");
    def1.setType("int"); def2.setType("int");
    std::shared_ptr<TDefinitionBase> def1Ptr = std::make_shared<TParameterDefinition>(def1);
    std::shared_ptr<TDefinitionBase> def2Ptr = std::make_shared<TParameterDefinition>(def2);

    EXPECT_FALSE(DAGBuilder->_definitionsSharePrior(def1Ptr, def2Ptr));
}

TEST_F(TDAGBuilderTest, _definitionsSharePrior_notSamePrior_numbers) {
    // do share prior (same prior, parameters and type)
    TParameterDefinition def1("def1");
    TParameterDefinition def2("def2");
    def1.setPrior("beta"); def2.setPrior("normal");
    def1.setPriorParams("0, 1"); def2.setPriorParams("0, 1");
    def1.setType("int"); def2.setType("int");
    std::shared_ptr<TDefinitionBase> def1Ptr = std::make_shared<TParameterDefinition>(def1);
    std::shared_ptr<TDefinitionBase> def2Ptr = std::make_shared<TParameterDefinition>(def2);

    EXPECT_FALSE(DAGBuilder->_definitionsSharePrior(def1Ptr, def2Ptr));
}

TEST_F(TDAGBuilderTest, _definitionsSharePrior_notSameType_numbers) {
    // do share prior (same prior, parameters and type)
    TParameterDefinition def1("def1");
    TParameterDefinition def2("def2");
    def1.setPrior("normal"); def2.setPrior("normal");
    def1.setPriorParams("0, 1"); def2.setPriorParams("0, 1");
    def1.setType("float"); def2.setType("int");
    std::shared_ptr<TDefinitionBase> def1Ptr = std::make_shared<TParameterDefinition>(def1);
    std::shared_ptr<TDefinitionBase> def2Ptr = std::make_shared<TParameterDefinition>(def2);

    EXPECT_FALSE(DAGBuilder->_definitionsSharePrior(def1Ptr, def2Ptr));
}

TEST_F(TDAGBuilderTest, _definitionsSharePrior_notSamePrior_letters) {
    // do share prior (same prior, parameters and type)
    TParameterDefinition def1("def1");
    TParameterDefinition def2("def2");
    def1.setPrior("beta"); def2.setPrior("normal");
    def1.setPriorParams("mu, sd"); def2.setPriorParams("mu, sd");
    def1.setType("int"); def2.setType("int");
    std::shared_ptr<TDefinitionBase> def1Ptr = std::make_shared<TParameterDefinition>(def1);
    std::shared_ptr<TDefinitionBase> def2Ptr = std::make_shared<TParameterDefinition>(def2);

    EXPECT_THROW(try{DAGBuilder->_definitionsSharePrior(def1Ptr, def2Ptr);} catch (...){throw std::runtime_error("blah");}, std::runtime_error);
}

TEST_F(TDAGBuilderTest, _definitionsSharePrior_notSameType_letters) {
    // do share prior (same prior, parameters and type)
    TParameterDefinition def1("def1");
    TParameterDefinition def2("def2");
    def1.setPrior("normal"); def2.setPrior("normal");
    def1.setPriorParams("mu, sd"); def2.setPriorParams("mu, sd");
    def1.setType("float"); def2.setType("int");
    std::shared_ptr<TDefinitionBase> def1Ptr = std::make_shared<TParameterDefinition>(def1);
    std::shared_ptr<TDefinitionBase> def2Ptr = std::make_shared<TParameterDefinition>(def2);

    EXPECT_THROW(try{DAGBuilder->_definitionsSharePrior(def1Ptr, def2Ptr);} catch (...){throw std::runtime_error("blah");}, std::runtime_error);
}

// test: _bundleParametersWithSharedPriors
TEST_F(TDAGBuilderTest, _bundleParametersWithSharedPriors_samePrior){
    // two parameter definitions with same prior
    TParameterDefinition def1("def1");
    def1.setDimensions({10});
    def1.setPriorParams("one,two");
    def1.setPrior("normal");
    DAGBuilder->addParameter(def1);

    TParameterDefinition def2("def2");
    def2.setDimensions({10});
    def2.setPriorParams("one,two");
    def2.setPrior("normal");
    DAGBuilder->addParameter(def2);

    DAGBuilder->_bundleParametersWithSharedPriors(definitionsWithSharedPriorsVector);

    EXPECT_EQ(definitionsWithSharedPriorsVector.size(), 1);
    auto it = definitionsWithSharedPriorsVector[0];
    EXPECT_EQ(it.size(), 2);
    EXPECT_EQ(it[0]->name(), "def1"); EXPECT_EQ(it[1]->name(), "def2");
    EXPECT_EQ(it[0]->prior(), MCMCPrior::normal); EXPECT_EQ(it[1]->prior(), MCMCPrior::normal);
    EXPECT_EQ(it[0]->parameters(), "one,two"); EXPECT_EQ(it[1]->parameters(), "one,two");
}

TEST_F(TDAGBuilderTest, _bundleParametersWithSharedPriors_sameNumbers){
    // two parameter definitions with same prior
    TParameterDefinition def1("def1");
    def1.setPriorParams("0,1");
    def1.setPrior("normal");
    DAGBuilder->addParameter(def1);

    TParameterDefinition def2("def2");
    def2.setPriorParams("0,1");
    def2.setPrior("normal");
    DAGBuilder->addParameter(def2);

    DAGBuilder->_bundleParametersWithSharedPriors(definitionsWithSharedPriorsVector);

    EXPECT_EQ(definitionsWithSharedPriorsVector.size(), 1);
    auto it = definitionsWithSharedPriorsVector[0];
    EXPECT_EQ(it.size(), 2);
    EXPECT_EQ(it[0]->name(), "def1"); EXPECT_EQ(it[1]->name(), "def2");
    EXPECT_EQ(it[0]->prior(), MCMCPrior::normal); EXPECT_EQ(it[1]->prior(), MCMCPrior::normal);
    EXPECT_EQ(it[0]->parameters(), "0,1"); EXPECT_EQ(it[1]->parameters(), "0,1");
}

TEST_F(TDAGBuilderTest, _bundleParametersWithSharedPriors_differentPrior_SameNumbers){
    // two parameter definitions with same prior
    TParameterDefinition def1("def1");
    def1.setPriorParams("0.1,1");
    def1.setPrior("beta");
    DAGBuilder->addParameter(def1);

    TParameterDefinition def2("def2");
    def2.setPriorParams("0.1,1");
    def2.setPrior("normal");
    DAGBuilder->addParameter(def2);

    DAGBuilder->_bundleParametersWithSharedPriors(definitionsWithSharedPriorsVector);

    EXPECT_EQ(definitionsWithSharedPriorsVector.size(), 2);
    auto it = definitionsWithSharedPriorsVector[0];
    EXPECT_EQ(it.size(), 1);
    EXPECT_EQ(it[0]->name(), "def1");
    EXPECT_EQ(it[0]->prior(), MCMCPrior::beta);
    EXPECT_EQ(it[0]->parameters(), "0.1,1");
    it = definitionsWithSharedPriorsVector[1];
    EXPECT_EQ(it.size(), 1);
    EXPECT_EQ(it[0]->name(), "def2");
    EXPECT_EQ(it[0]->prior(), MCMCPrior::normal);
    EXPECT_EQ(it[0]->parameters(), "0.1,1");
}

TEST_F(TDAGBuilderTest, _bundleParametersWithSharedPriors_differentPrior){
    // two parameter definitions with different prior -> throw
    TParameterDefinition def1("def1");
    def1.setDimensions({10});
    def1.setPriorParams("one,two");
    def1.setPrior("normal");
    def1.setType("double");
    DAGBuilder->addParameter(def1);

    TParameterDefinition def2("def2");
    def2.setDimensions({10});
    def2.setPriorParams("one,two");
    def2.setPrior("exponential");
    def2.setType("double");
    DAGBuilder->addParameter(def2);

    EXPECT_THROW({try {DAGBuilder->_bundleParametersWithSharedPriors(definitionsWithSharedPriorsVector);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TDAGBuilderTest, _bundleParametersWithSharedPriors_differentType){
    // two parameter definitions with same prior, but different type -> throw
    TParameterDefinition def1("def1");
    def1.setDimensions({10});
    def1.setPriorParams("one,two");
    def1.setPrior("normal");
    def1.setType("double");
    DAGBuilder->addParameter(def1);

    TParameterDefinition def2("def2");
    def2.setDimensions({10});
    def2.setPriorParams("one,two");
    def2.setPrior("normal");
    def2.setType("bool");
    DAGBuilder->addParameter(def2);

    EXPECT_THROW({try {DAGBuilder->_bundleParametersWithSharedPriors(definitionsWithSharedPriorsVector);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TDAGBuilderTest, _bundleParametersWithSharedPriors_differentType_Numbers){
    // two parameter definitions with same prior
    TParameterDefinition def1("def1");
    def1.setPriorParams("0,1");
    def1.setPrior("normal");
    def1.setType("double");
    DAGBuilder->addParameter(def1);

    TParameterDefinition def2("def2");
    def2.setPriorParams("0,1");
    def2.setPrior("normal");
    def2.setType("bool");
    DAGBuilder->addParameter(def2);

    DAGBuilder->_bundleParametersWithSharedPriors(definitionsWithSharedPriorsVector);

    EXPECT_EQ(definitionsWithSharedPriorsVector.size(), 2);
    auto it = definitionsWithSharedPriorsVector[0];
    EXPECT_EQ(it.size(), 1);
    EXPECT_EQ(it[0]->name(), "def1");
    EXPECT_EQ(it[0]->prior(), MCMCPrior::normal);
    EXPECT_EQ(it[0]->parameters(), "0,1");
    it = definitionsWithSharedPriorsVector[1];
    EXPECT_EQ(it.size(), 1);
    EXPECT_EQ(it[0]->name(), "def2");
    EXPECT_EQ(it[0]->prior(), MCMCPrior::normal);
    EXPECT_EQ(it[0]->parameters(), "0,1");
}

TEST_F(TDAGBuilderTest, _bundleParametersWithSharedPriors_empty){
    // this is the case for uniform
    TParameterDefinition def1("def1");
    DAGBuilder->addParameter(def1);

    TParameterDefinition def2("def2");
    DAGBuilder->addParameter(def2);

    DAGBuilder->_bundleParametersWithSharedPriors(definitionsWithSharedPriorsVector);

    EXPECT_EQ(definitionsWithSharedPriorsVector.size(), 1);
    auto it = definitionsWithSharedPriorsVector[0];
    EXPECT_EQ(it.size(), 2);
    EXPECT_EQ(it[0]->name(), "def1"); EXPECT_EQ(it[1]->name(), "def2");
    EXPECT_EQ(it[0]->prior(), MCMCPrior::uniform); EXPECT_EQ(it[1]->prior(), MCMCPrior::uniform);
    EXPECT_EQ(it[0]->parameters(), ""); EXPECT_EQ(it[1]->parameters(), "");
}

TEST_F(TDAGBuilderTest, _bundleParametersWithSharedPriors_empty_differentPrior){
    TParameterDefinition def1("def1");
    def1.setPrior("beta");
    DAGBuilder->addParameter(def1);

    TParameterDefinition def2("def2");
    def2.setPrior("normal");
    DAGBuilder->addParameter(def2);

    DAGBuilder->_bundleParametersWithSharedPriors(definitionsWithSharedPriorsVector);

    EXPECT_EQ(definitionsWithSharedPriorsVector.size(), 2);
    auto it = definitionsWithSharedPriorsVector[0];
    EXPECT_EQ(it.size(), 1);
    EXPECT_EQ(it[0]->name(), "def1");
    EXPECT_EQ(it[0]->prior(), MCMCPrior::beta);
    EXPECT_EQ(it[0]->parameters(), "");
    it = definitionsWithSharedPriorsVector[1];
    EXPECT_EQ(it.size(), 1);
    EXPECT_EQ(it[0]->name(), "def2");
    EXPECT_EQ(it[0]->prior(), MCMCPrior::normal);
    EXPECT_EQ(it[0]->parameters(), "");
}

TEST_F(TDAGBuilderTest, _bundleParametersWithSharedPriors_empty_differentType){
    // this is the case for uniform
    TParameterDefinition def1("def1");
    def1.setType("double");
    DAGBuilder->addParameter(def1);

    TParameterDefinition def2("def2");
    def2.setType("bool");
    DAGBuilder->addParameter(def2);

    DAGBuilder->_bundleParametersWithSharedPriors(definitionsWithSharedPriorsVector);

    EXPECT_EQ(definitionsWithSharedPriorsVector.size(), 2);
    auto it = definitionsWithSharedPriorsVector[0];
    EXPECT_EQ(it.size(), 1);
    EXPECT_EQ(it[0]->name(), "def1");
    EXPECT_EQ(it[0]->prior(), MCMCPrior::uniform);
    EXPECT_EQ(it[0]->parameters(), "");
    it = definitionsWithSharedPriorsVector[1];
    EXPECT_EQ(it.size(), 1);
    EXPECT_EQ(it[0]->name(), "def2");
    EXPECT_EQ(it[0]->prior(), MCMCPrior::uniform);
    EXPECT_EQ(it[0]->parameters(), "");
}

TEST_F(TDAGBuilderTest, _bundleParametersWithSharedPriors_differentPriorParameters){
    // two parameter definitions with same prior
    TParameterDefinition def1("def1");
    def1.setDimensions({10});
    def1.setPriorParams("one,two");
    def1.setPrior("normal");
    def1.setType("double");
    DAGBuilder->addParameter(def1);

    TParameterDefinition def2("def2");
    def2.setDimensions({10});
    def2.setPriorParams("three,four");
    def2.setPrior("normal");
    def2.setType("double");
    DAGBuilder->addParameter(def2);

    DAGBuilder->_bundleParametersWithSharedPriors(definitionsWithSharedPriorsVector);

    EXPECT_EQ(definitionsWithSharedPriorsVector.size(), 2);
    auto it = definitionsWithSharedPriorsVector[0];
    EXPECT_EQ(it.size(), 1);
    EXPECT_EQ(it[0]->name(), "def1");
    EXPECT_EQ(it[0]->prior(), MCMCPrior::normal);
    EXPECT_EQ(it[0]->parameters(), "one,two");
    it = definitionsWithSharedPriorsVector[1];
    EXPECT_EQ(it.size(), 1);
    EXPECT_EQ(it[0]->name(), "def2");
    EXPECT_EQ(it[0]->prior(), MCMCPrior::normal);
    EXPECT_EQ(it[0]->parameters(), "three,four");
}

TEST_F(TDAGBuilderTest, _bundleParametersWithSharedPriors_parameterAndObservation){
    // shouldn't make any difference if we have two parameters or one parameter and one observation
    // just replicate one of the tests above, not all of them

    // a parameter definition and an observation definition with same prior
    TParameterDefinition def1("def1");
    def1.setDimensions({10});
    def1.setPriorParams("one,two");
    def1.setPrior("normal");
    DAGBuilder->addParameter(def1);

    TObservationDefinition def2("def2");
    def2.setDimensions({10});
    def2.setPriorParams("one,two");
    def2.setPrior("normal");
    DAGBuilder->addObservation(def2);

    DAGBuilder->_bundleParametersWithSharedPriors(definitionsWithSharedPriorsVector);

    EXPECT_EQ(definitionsWithSharedPriorsVector.size(), 1);
    auto it = definitionsWithSharedPriorsVector[0];
    EXPECT_EQ(it.size(), 2);
    EXPECT_EQ(it[0]->name(), "def1"); EXPECT_EQ(it[1]->name(), "def2");
    EXPECT_EQ(it[0]->prior(), MCMCPrior::normal); EXPECT_EQ(it[1]->prior(), MCMCPrior::normal);
    EXPECT_EQ(it[0]->parameters(), "one,two"); EXPECT_EQ(it[1]->parameters(), "one,two");
}

// test: _buildDAG()
TEST_F(TDAGBuilderTest, _buildDAG_validParams) {
    // add mean
    TParameterDefinition def_meanOnNormal("mean");
    DAGBuilder->addParameter(def_meanOnNormal);
    // add sd
    TParameterDefinition def_sdOnNormal("sd");
    DAGBuilder->addParameter(def_sdOnNormal);
    // add observation
    TObservationDefinition def_normalPrior("observation");
    def_normalPrior.setDimensions({5});
    def_normalPrior.setPrior("normal");
    def_normalPrior.setPriorParams("mean, sd");
    DAGBuilder->addObservation(def_normalPrior);
    // add some other observation
    TObservationDefinition def_whatever("whatever");
    DAGBuilder->addObservation(def_whatever);

    // now initialize
    EXPECT_NO_THROW(DAGBuilder->_buildDAG(true, parameters));
    EXPECT_NO_THROW(DAGBuilder->initializeStorage());

    // now get map
    std::map<std::string, std::shared_ptr<TObservationsBase>> map = DAGBuilder->getObservations();

    EXPECT_THAT(map.size(), 2);
    EXPECT_EQ(map["observation"]->name(), "observation");
    EXPECT_EQ(map["whatever"]->name(), "whatever");

    std::map<std::string, std::shared_ptr<TMCMCParameterBase>> mapParams = DAGBuilder->getParameters();

    EXPECT_THAT(mapParams.size(), 2);
    EXPECT_EQ(mapParams["mean"]->name(), "mean");
    EXPECT_EQ(mapParams["sd"]->name(), "sd");
}

TEST_F(TDAGBuilderTest, _buildDAG_ParamThatDoesntExist) {
    // add mean
    TParameterDefinition def_meanOnNormal("mean");
    DAGBuilder->addParameter(def_meanOnNormal);
    // forget to add sd
    // add observation
    TObservationDefinition def_normalPrior("observation");
    def_normalPrior.setDimensions({5});
    def_normalPrior.setPrior("normal");
    def_normalPrior.setPriorParams("mean, sd");
    DAGBuilder->addObservation(def_normalPrior);

    // now initialize
    EXPECT_THROW(DAGBuilder->_buildDAG(true, parameters), std::runtime_error);
}

TEST_F(TDAGBuilderTest, _buildDAG_priorOnlyPartiallyShared) {
    // add mean
    TParameterDefinition def_meanOnNormal("mean");
    DAGBuilder->addParameter(def_meanOnNormal);
    // add sd
    TParameterDefinition def_sdOnNormal("sd");
    DAGBuilder->addParameter(def_sdOnNormal);
    // add another sd
    TParameterDefinition def_sd2OnNormal("sd2");
    DAGBuilder->addParameter(def_sd2OnNormal);
    // add observation
    TObservationDefinition def_normalPrior("observation");
    def_normalPrior.setDimensions({5});
    def_normalPrior.setPrior("normal");
    def_normalPrior.setPriorParams("mean, sd");
    DAGBuilder->addObservation(def_normalPrior);
    // add another observation
    TObservationDefinition def2_normalPrior("observation2");
    def2_normalPrior.setDimensions({5});
    def2_normalPrior.setPrior("normal");
    def2_normalPrior.setPriorParams("mean, sd2"); // both observations have mean, but one has sd and the other sd2 -> throw! Mean is used for 2 independent priors
    DAGBuilder->addObservation(def2_normalPrior);

    // now initialize
    EXPECT_THROW(try {DAGBuilder->_buildDAG(true, parameters);} catch (...) {throw std::runtime_error("Caught error");}, std::runtime_error);
}

TEST_F(TDAGBuilderTest, _buildDAG_noObservation) {
    // add parameter
    TParameterDefinition def("def");
    DAGBuilder->addParameter(def);
    // don't add observation

    // now initialize
    EXPECT_THROW(DAGBuilder->_buildDAG(true, parameters), std::runtime_error);
}

TEST_F(TDAGBuilderTest, _buildDAG_parameterWithoutObservation) {
    // build one valid DAG with observation
    // add mean
    TParameterDefinition def_meanOnNormal("mean");
    DAGBuilder->addParameter(def_meanOnNormal);
    // add sd
    TParameterDefinition def_sdOnNormal("sd");
    DAGBuilder->addParameter(def_sdOnNormal);
    // add observation
    TObservationDefinition def_normalPrior("observation");
    def_normalPrior.setDimensions({5});
    def_normalPrior.setPrior("normal");
    def_normalPrior.setPriorParams("mean, sd");
    DAGBuilder->addObservation(def_normalPrior);

    // add some other parameter, unrelated to rest
    TParameterDefinition def_whatever("whatever");
    DAGBuilder->addParameter(def_whatever);

    // now initialize
    EXPECT_THROW(DAGBuilder->_buildDAG(true, parameters), std::runtime_error);
}

TEST_F(TDAGBuilderTest, initPriorUniform) {
    std::shared_ptr<TPrior<double>> prior = DAGBuilder->_initPrior(MCMCPrior::uniform, "", "param");
    EXPECT_EQ(prior->name(), MCMCPrior::uniform);
}

TEST_F(TDAGBuilderTest, initPriorNormal) {
    std::shared_ptr<TPrior<double>> prior = DAGBuilder->_initPrior(MCMCPrior::normal, "0,1", "param");
    EXPECT_EQ(prior->name(), MCMCPrior::normal);
}

TEST_F(TDAGBuilderTest, initPriorUniformFloat) {
    std::shared_ptr<TPrior<float>> prior = DAGBuilder->_initPriorFloat(MCMCPrior::uniform, "", "param");
    EXPECT_EQ(prior->name(), MCMCPrior::uniform);
}

TEST_F(TDAGBuilderTest, initPriorNormalFloat) {
    std::shared_ptr<TPrior<float>> prior = DAGBuilder->_initPriorFloat(MCMCPrior::normal, "0,1", "param");
    EXPECT_EQ(prior->name(), MCMCPrior::normal);
}

TEST_F(TDAGBuilderTest, initPriorUniformInt) {
    std::shared_ptr<TPrior<int>> prior = DAGBuilder->_initPriorInt(MCMCPrior::uniform, "", "param");
    EXPECT_EQ(prior->name(), MCMCPrior::uniform);
}

TEST_F(TDAGBuilderTest, checkMinMaxPrior) {
    std::shared_ptr<TDefinitionBase> def = std::make_shared<TParameterDefinition>("param");
    def->setPrior("beta");
    def->setPriorParams("0.7,0.7");
    std::shared_ptr<TPrior<double>> prior = DAGBuilder->_initPrior(def->prior(), def->parameters(), def->name());
    DAGBuilder->_checkMinMaxPrior(prior, def); // should adjust min and max inside def
    EXPECT_EQ(def->min(), "0.");
    EXPECT_EQ(def->max(), "1.");
    EXPECT_EQ(def->minIncluded(), false);
    EXPECT_EQ(def->maxIncluded(), false);
}

TEST_F(TDAGBuilderTest, initPriorWithHyperPrior_normal) {
    // first construct mean
    auto defMean = std::make_shared<TParameterDefinition>("mean");
    auto priorMean = std::make_unique<TPriorNormal<double>>();
    priorMean->initialize("0,1");
    auto mean = std::make_shared<TMCMCParameter<double>>(std::move(priorMean), defMean, &randomGenerator);
    // ...and sd
    auto defSd = std::make_shared<TParameterDefinition>("sd");
    auto priorSd = std::make_unique<TPriorExponential<double>>();
    priorSd->initialize("1");
    auto sd = std::make_shared<TMCMCParameter<double>>(std::move(priorSd), defSd, &randomGenerator);
    // now initialize initializedParams (needs to have right size)
    initializedParams.push_back(mean);
    initializedParams.push_back(sd);

    std::shared_ptr<TPrior<double>> prior = DAGBuilder->_initPriorWithHyperPrior(MCMCPrior::normal, initializedParams, initializedObservations, "param");
    EXPECT_EQ(prior->name(), MCMCPrior::normal);
}

TEST_F(TDAGBuilderTest, checkMinMaxPrior_normal) {
    // first construct mean
    auto defMean = std::make_shared<TParameterDefinition>("mean");
    auto priorMean = std::make_unique<TPriorNormal<double>>();
    priorMean->initialize("0,1");
    auto mean = std::make_shared<TMCMCParameter<double>>(std::move(priorMean), defMean, &randomGenerator);
    // ...and sd
    auto defSd = std::make_shared<TParameterDefinition>("sd");
    auto priorSd = std::make_unique<TPriorExponential<double>>();
    priorSd->initialize("1");
    auto sd = std::make_shared<TMCMCParameter<double>>(std::move(priorSd), defSd, &randomGenerator);
    // now initialize initializedParams (needs to have right size)
    initializedParams.push_back(mean);
    initializedParams.push_back(sd);

    std::shared_ptr<TPrior<double>> prior = DAGBuilder->_initPriorWithHyperPrior(MCMCPrior::normal, initializedParams, initializedObservations, "param");

    std::shared_ptr<TDefinitionBase> def = std::make_shared<TParameterDefinition>("param");
    DAGBuilder->_checkMinMaxPrior(prior, def); // should not adjust min and max inside def
    EXPECT_EQ(def->min(), toString(std::numeric_limits<double>::lowest()));
    EXPECT_EQ(def->minIncluded(), true);
    EXPECT_EQ(def->max(), toString(std::numeric_limits<double>::max()));
    EXPECT_EQ(def->maxIncluded(), true);
}

TEST_F(TDAGBuilderTest, checkMinMaxPrior_exponential) {
    // first construct lambda
    auto defLambda = std::make_shared<TParameterDefinition>("lambda");
    auto priorLambda = std::make_unique<TPriorExponential<double>>();
    priorLambda->initialize("1");
    auto lambda = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda), defLambda, &randomGenerator);
    // now initialize initializedParams (needs to have right size)
    initializedParams.push_back(lambda);

    std::shared_ptr<TPrior<double>> prior = DAGBuilder->_initPriorWithHyperPrior(MCMCPrior::exponential, initializedParams, initializedObservations, "param");

    std::shared_ptr<TDefinitionBase> def = std::make_shared<TParameterDefinition>("param");
    DAGBuilder->_checkMinMaxPrior(prior, def); // should adjust min and max inside def
    EXPECT_EQ(def->min(), "0.");
    EXPECT_EQ(def->minIncluded(), true);
}

TEST_F(TDAGBuilderTest, checkMinMaxPrior_beta) {
    // first construct alpha
    auto defAlpha = std::make_shared<TParameterDefinition>("alpha");
    auto priorAlpha = std::make_unique<TPriorExponential<double>>();
    priorAlpha->initialize("1");
    auto alpha = std::make_shared<TMCMCParameter<double>>(std::move(priorAlpha), defAlpha, &randomGenerator);
    // ...and beta
    auto defBeta = std::make_shared<TParameterDefinition>("beta");
    auto priorbeta = std::make_unique<TPriorExponential<double>>();
    priorbeta->initialize("1");
    auto beta = std::make_shared<TMCMCParameter<double>>(std::move(priorbeta), defBeta, &randomGenerator);
    // now initialize initializedParams (needs to have right size)
    initializedParams.push_back(alpha);
    initializedParams.push_back(beta);

    std::shared_ptr<TPrior<double>> prior = DAGBuilder->_initPriorWithHyperPrior(MCMCPrior::beta, initializedParams, initializedObservations, "param");

    std::shared_ptr<TDefinitionBase> def = std::make_shared<TParameterDefinition>("param");
    DAGBuilder->_checkMinMaxPrior(prior, def); // should adjust min and max inside def
    EXPECT_EQ(def->min(), "0.");
    EXPECT_EQ(def->max(), "1.");
    EXPECT_EQ(def->minIncluded(), false);
    EXPECT_EQ(def->maxIncluded(), false);
}

TEST_F(TDAGBuilderTest, _buildParams_oneParamPerPrior) {
    // first construct alpha
    auto defAlpha = std::make_shared<TParameterDefinition>("alpha");
    auto priorAlpha = std::make_unique<TPriorExponential<double>>();
    auto alpha = std::make_shared<TMCMCParameter<double>>(std::move(priorAlpha), defAlpha, &randomGenerator);
    // ...and beta
    auto defBeta = std::make_shared<TParameterDefinition>("beta");
    auto priorbeta = std::make_unique<TPriorExponential<double>>();
    auto beta = std::make_shared<TMCMCParameter<double>>(std::move(priorbeta), defBeta, &randomGenerator);
    // now initialize initializedParams (needs to have right size)
    initializedParams.push_back(alpha);
    initializedParams.push_back(beta);

    // initialize definition for parameter array
    TParameterDefinition def("array");
    def.setPrior("beta");
    def.setPriorParams("alpha, beta");
    def.setDimensions({100});
    DAGBuilder->addParameter(def);
    auto defPtr = std::make_shared<TParameterDefinition>(def);
    std::vector<std::shared_ptr<TDefinitionBase>> defs = {defPtr};

    // build parameter array
    DAGBuilder->_buildParams(defs, initializedParams, initializedObservations, parameters);

    // get maps from manager
    std::map<std::string, std::shared_ptr<TMCMCParameterBase>> map = DAGBuilder->getParameters();

    EXPECT_EQ(map.size(), 1);
    EXPECT_EQ(map["array"]->name(), "array");
}

TEST_F(TDAGBuilderTest, _buildParams_twoParamsPerPrior) {
    // first construct alpha
    auto defAlpha = std::make_shared<TParameterDefinition>("alpha");
    auto priorAlpha = std::make_unique<TPriorExponential<double>>();
    priorAlpha->initialize("1");
    auto alpha = std::make_shared<TMCMCParameter<double>>(std::move(priorAlpha), defAlpha, &randomGenerator);
    // ...and beta
    auto defBeta = std::make_shared<TParameterDefinition>("beta");
    auto priorbeta = std::make_unique<TPriorExponential<double>>();
    priorbeta->initialize("1");
    auto beta = std::make_shared<TMCMCParameter<double>>(std::move(priorbeta), defBeta, &randomGenerator);
    // now initialize initializedParams (needs to have right size)
    initializedParams.push_back(alpha);
    initializedParams.push_back(beta);

    // initialize definition for parameter array
    TParameterDefinition def("array");
    def.setDimensions({100});
    def.setPriorParams("alpha, beta");
    def.setPrior("beta");
    DAGBuilder->addParameter(def);
    auto defPtr = std::make_shared<TParameterDefinition>(def);

    TParameterDefinition def2("array2");
    def2.setDimensions({5, 29});
    def2.setPriorParams("alpha, beta");
    def2.setPrior("beta");
    DAGBuilder->addParameter(def2);
    auto defPtr2 = std::make_shared<TParameterDefinition>(def2);
    std::vector<std::shared_ptr<TDefinitionBase>> defs = {defPtr, defPtr2};

    // build parameter array
    DAGBuilder->_buildParams(defs, initializedParams, initializedObservations, parameters);

    // get maps from manager
    std::map<std::string, std::shared_ptr<TMCMCParameterBase>> map = DAGBuilder->getParameters();

    EXPECT_EQ(map.size(), 2);
    EXPECT_EQ(map["array"]->name(), "array");
    EXPECT_EQ(map["array2"]->name(), "array2");
}

TEST_F(TDAGBuilderTest, _buildParams_translatedObservation) {
    // first construct alpha
    auto defAlpha = std::make_shared<TParameterDefinition>("alpha");
    auto priorAlpha = std::make_unique<TPriorExponential<double>>();
    auto alpha = std::make_shared<TMCMCParameter<double>>(std::move(priorAlpha), defAlpha, &randomGenerator);
    // ...and beta
    auto defBeta = std::make_shared<TParameterDefinition>("beta");
    auto priorbeta = std::make_unique<TPriorExponential<double>>();
    auto beta = std::make_shared<TMCMCParameter<double>>(std::move(priorbeta), defBeta, &randomGenerator);
    // now initialize initializedParams (needs to have right size)
    initializedParams.push_back(alpha);
    initializedParams.push_back(beta);

    // initialize definition for translated observation
    TObservationDefinition def("observation");
    def.translate(Translator::phred, "uint8_t", 255);
    def.setPrior("beta");
    def.setPriorParams("alpha, beta");
    def.setDimensions({100});
    DAGBuilder->addObservation(def);
    auto defPtr = std::make_shared<TObservationDefinition>(def);
    std::vector<std::shared_ptr<TDefinitionBase>> defs = {defPtr};

    // build parameter array
    DAGBuilder->_buildParams(defs, initializedParams, initializedObservations, parameters);

    // get maps from manager
    std::map<std::string, std::shared_ptr<TObservationsBase>> map = DAGBuilder->getObservations();

    EXPECT_EQ(map.size(), 1);
    EXPECT_EQ(map["observation"]->name(), "observation");
}

TEST_F(TDAGBuilderTest, bundleParameterFilesNoFile) {
    TObservationDefinition obs("observation");
    obs.setPrior("exponential");
    obs.setPriorParams("def");
    DAGBuilder->addObservation(obs);

    TParameterDefinition def("def");
    DAGBuilder->addParameter(def);
    DAGBuilder->buildScaffoldDAG<TDAGBuilderFunctor>(parameters);
    DAGBuilder->initializeStorage();
    DAGBuilder->_prepareAllMCMCFiles("./", false);

    std::vector<std::unique_ptr<TMCMCFile> > traceFileVec = std::move(DAGBuilder->getTraceFiles());
    std::vector<std::unique_ptr<TMCMCFile> > meanVarFileVec = std::move(DAGBuilder->getMeanVarFiles());

    EXPECT_TRUE(traceFileVec.empty());
    EXPECT_TRUE(meanVarFileVec.empty());
}

TEST_F(TDAGBuilderTest, bundleParameterFilesOnlyTrace) {
    TObservationDefinition obs("observation");
    obs.setPrior("exponential");
    obs.setPriorParams("def");
    DAGBuilder->addObservation(obs);

    TParameterDefinition def("def");
    def.setTraceFile("dummy");
    DAGBuilder->addParameter(def);
    DAGBuilder->buildScaffoldDAG<TDAGBuilderFunctor>(parameters);
    DAGBuilder->initializeStorage();
    DAGBuilder->_prepareAllMCMCFiles("./", false);

    std::vector<std::unique_ptr<TMCMCFile> > traceFileVec = std::move(DAGBuilder->getTraceFiles());
    std::vector<std::unique_ptr<TMCMCFile> > meanVarFileVec = std::move(DAGBuilder->getMeanVarFiles());

    EXPECT_EQ(traceFileVec.size(), 1);
    EXPECT_TRUE(meanVarFileVec.empty());
    remove("dummy_trace.txt");
    //remove("MCMC_state.txt"); // initializeParams() will create an MCMC state file (empty) -> delete it here
}

TEST_F(TDAGBuilderTest, bundleParameterFilesOnlyMeanVar) {
    TObservationDefinition obs("observation");
    obs.setPrior("exponential");
    obs.setPriorParams("def");
    DAGBuilder->addObservation(obs);

    TParameterDefinition def("def");
    def.setMeanVarFile("dummy");
    DAGBuilder->addParameter(def);
    DAGBuilder->buildScaffoldDAG<TDAGBuilderFunctor>(parameters);
    DAGBuilder->initializeStorage();
    DAGBuilder->_prepareAllMCMCFiles("./", false);

    std::vector<std::unique_ptr<TMCMCFile> > traceFileVec = std::move(DAGBuilder->getTraceFiles());
    std::vector<std::unique_ptr<TMCMCFile> > meanVarFileVec = std::move(DAGBuilder->getMeanVarFiles());

    EXPECT_TRUE(traceFileVec.empty());
    EXPECT_EQ(meanVarFileVec.size(), 1);
    remove("dummy_meanVar.txt");
    //remove("MCMC_state.txt"); // initializeParams() will create an MCMC state file (empty) -> delete it here
}

TEST_F(TDAGBuilderTest, bundleParameterFilesTraceAndMeanVar) {
    TObservationDefinition obs("observation");
    obs.setPrior("beta");
    obs.setPriorParams("def1, def2");
    DAGBuilder->addObservation(obs);

    TParameterDefinition def1("def1");
    def1.setTraceFile("dummy1");

    TParameterDefinition def2("def2");
    def2.setMeanVarFile("dummy2");
    DAGBuilder->addParameter(def1);
    DAGBuilder->addParameter(def2);
    DAGBuilder->buildScaffoldDAG<TDAGBuilderFunctor>(parameters);
    DAGBuilder->initializeStorage();
    DAGBuilder->_prepareAllMCMCFiles("./", false);

    std::vector<std::unique_ptr<TMCMCFile> > traceFileVec = std::move(DAGBuilder->getTraceFiles());
    std::vector<std::unique_ptr<TMCMCFile> > meanVarFileVec = std::move(DAGBuilder->getMeanVarFiles());

    EXPECT_EQ(traceFileVec.size(), 1);
    EXPECT_EQ(meanVarFileVec.size(), 1);
    remove("dummy1_trace.txt");
    remove("dummy2_meanVar.txt");
    //remove("MCMC_state.txt"); // initializeParams() will create an MCMC state file (empty) -> delete it here
}

TEST_F(TDAGBuilderTest, bundleParameterFilesTraceAndMeanVarSameName) {
    TObservationDefinition obs("observation");
    obs.setPrior("beta");
    obs.setPriorParams("def1, def2");
    DAGBuilder->addObservation(obs);

    TParameterDefinition def1("def1");
    def1.setTraceFile("dummy");

    TParameterDefinition def2("def2");
    def2.setMeanVarFile("dummy");
    DAGBuilder->addParameter(def1);
    DAGBuilder->addParameter(def2);
    DAGBuilder->buildScaffoldDAG<TDAGBuilderFunctor>(parameters);
    DAGBuilder->initializeStorage();
    DAGBuilder->_prepareAllMCMCFiles("./", false);

    std::vector<std::unique_ptr<TMCMCFile> > traceFileVec = std::move(DAGBuilder->getTraceFiles());
    std::vector<std::unique_ptr<TMCMCFile> > meanVarFileVec = std::move(DAGBuilder->getMeanVarFiles());

    EXPECT_EQ(traceFileVec.size(), 1);
    EXPECT_EQ(meanVarFileVec.size(), 1);
    remove("dummy_trace.txt");
    remove("dummy_meanVar.txt");
    //remove("MCMC_state.txt"); // initializeParams() will create an MCMC state file (empty) -> delete it here
}

TEST_F(TDAGBuilderTest, bundleParameterFilesTwoDifferentTraces) {
    TObservationDefinition obs("observation");
    obs.setPrior("beta");
    obs.setPriorParams("def1, def2");
    DAGBuilder->addObservation(obs);

    TParameterDefinition def1("def1");
    def1.setTraceFile("dummy1");

    TParameterDefinition def2("def2");
    def2.setTraceFile("dummy2");
    DAGBuilder->addParameter(def1);
    DAGBuilder->addParameter(def2);
    DAGBuilder->buildScaffoldDAG<TDAGBuilderFunctor>(parameters);
    DAGBuilder->initializeStorage();
    DAGBuilder->_prepareAllMCMCFiles("./", false);

    std::vector<std::unique_ptr<TMCMCFile> > traceFileVec = std::move(DAGBuilder->getTraceFiles());
    std::vector<std::unique_ptr<TMCMCFile> > meanVarFileVec = std::move(DAGBuilder->getMeanVarFiles());

    EXPECT_EQ(traceFileVec.size(), 2);
    EXPECT_TRUE(meanVarFileVec.empty());
    remove("dummy1_trace.txt");
    remove("dummy2_trace.txt");
    //remove("MCMC_state.txt"); // initializeParams() will create an MCMC state file (empty) -> delete it here
}

TEST_F(TDAGBuilderTest, bundleParameterFilesTwoDifferentMeanVars) {
    TObservationDefinition obs("observation");
    obs.setPrior("beta");
    obs.setPriorParams("def1, def2");
    DAGBuilder->addObservation(obs);

    TParameterDefinition def1("def1");
    def1.setMeanVarFile("dummy1");

    TParameterDefinition def2("def2");
    def2.setMeanVarFile("dummy2");
    DAGBuilder->addParameter(def1);
    DAGBuilder->addParameter(def2);
    DAGBuilder->buildScaffoldDAG<TDAGBuilderFunctor>(parameters);
    DAGBuilder->initializeStorage();
    DAGBuilder->_prepareAllMCMCFiles("./", false);

    std::vector<std::unique_ptr<TMCMCFile> > traceFileVec = std::move(DAGBuilder->getTraceFiles());
    std::vector<std::unique_ptr<TMCMCFile> > meanVarFileVec = std::move(DAGBuilder->getMeanVarFiles());

    EXPECT_TRUE(traceFileVec.empty());
    EXPECT_EQ(meanVarFileVec.size(), 2);
    remove("dummy1_meanVar.txt");
    remove("dummy2_meanVar.txt");
    //remove("MCMC_state.txt"); // initializeParams() will create an MCMC state file (empty) -> delete it here
}

TEST_F(TDAGBuilderTest, bundleParameterFilesSameTraceDifferentMeanVar) {
    TObservationDefinition obs("observation");
    obs.setPrior("beta");
    obs.setPriorParams("def1, def2");
    DAGBuilder->addObservation(obs);

    TParameterDefinition def1("def1");
    def1.setTraceFile("dummy");
    def1.setMeanVarFile("dummy1");

    TParameterDefinition def2("def2");
    def2.setTraceFile("dummy");
    def2.setMeanVarFile("dummy2");
    DAGBuilder->addParameter(def1);
    DAGBuilder->addParameter(def2);
    DAGBuilder->buildScaffoldDAG<TDAGBuilderFunctor>(parameters);
    DAGBuilder->initializeStorage();
    DAGBuilder->_prepareAllMCMCFiles("./", false);

    std::vector<std::unique_ptr<TMCMCFile> > traceFileVec = std::move(DAGBuilder->getTraceFiles());
    std::vector<std::unique_ptr<TMCMCFile> > meanVarFileVec = std::move(DAGBuilder->getMeanVarFiles());

    EXPECT_EQ(traceFileVec.size(), 1);
    EXPECT_EQ(meanVarFileVec.size(), 2);
    remove("dummy_trace.txt");
    remove("dummy1_meanVar.txt");
    remove("dummy2_meanVar.txt");
    remove("MCMC_state.txt"); // initializeParams() will create an MCMC state file (empty) -> delete it here
}

TEST_F(TDAGBuilderTest, _createOneDAG) {
    // make a complicated DAG with shared priors (corresponds to DAG drawn in documentation)
    TObservationDefinition y("y");
    y.setPrior("exponential");
    y.setPriorParams("a");
    DAGBuilder->addObservation(y);

    TObservationDefinition x1("x1");
    x1.setPrior("beta");
    x1.setPriorParams("b,c");
    DAGBuilder->addObservation(x1);

    TObservationDefinition x2("x2");
    x2.setPrior("beta");
    x2.setPriorParams("b,c");
    DAGBuilder->addObservation(x2);

    TObservationDefinition z("z");
    z.setPrior("exponential");
    z.setPriorParams("d");
    DAGBuilder->addObservation(z);

    TParameterDefinition a("a");
    a.setPrior("normal");
    a.setPriorParams("paramE,f");
    DAGBuilder->addParameter(a);

    TParameterDefinition b("b");
    b.setPrior("normal");
    b.setPriorParams("paramE,f");
    DAGBuilder->addParameter(b);

    TParameterDefinition c("c");
    c.setPrior("normal");
    c.setPriorParams("paramE,f");
    DAGBuilder->addParameter(c);

    TParameterDefinition d("d");
    d.setPrior("normal");
    d.setPriorParams("g,h");
    DAGBuilder->addParameter(d);

    TParameterDefinition paramE("paramE");
    paramE.setPrior("normal");
    paramE.setPriorParams("i,j");
    DAGBuilder->addParameter(paramE);

    TParameterDefinition f("f");
    f.setPrior("normal");
    f.setPriorParams("g,h");
    DAGBuilder->addParameter(f);

    TParameterDefinition g("g");
    g.setPrior("normal");
    g.setPriorParams("i,j");
    DAGBuilder->addParameter(g);

    TParameterDefinition h("h");
    h.setPrior("normal");
    h.setPriorParams("0,1");
    DAGBuilder->addParameter(h);

    TParameterDefinition i("i");
    i.setPrior("normal");
    i.setPriorParams("0,2");
    DAGBuilder->addParameter(i);

    TParameterDefinition j("j");
    DAGBuilder->addParameter(j);

    // build DAG
    DAGBuilder->buildScaffoldDAG<TDAGBuilderFunctor>(parameters);
    DAGBuilder->initializeStorage();

    // check if DAG is correct
    TDAG dag = DAGBuilder->getDAG();
    std::vector<std::string> names = dag.getNamesOfNodes();

    std::vector<std::string> expectedNames = {"y", "x1", "a", "z", "d", "paramE", "i", "j", "h"};
    EXPECT_EQ(names.size(), expectedNames.size());

    for (size_t i = 0; i < names.size(); i++) {
        auto it = std::find(expectedNames.begin(), expectedNames.end(), names[i]);
        EXPECT_TRUE(it != expectedNames.end()); // found! (order does not have to be exactly the same)
    }
}

//--------------------------------------------
// TMCMCUserInterface
//--------------------------------------------

class BridgeTMCMCUserInterface : public TMCMCUserInterface {
public:
    BridgeTMCMCUserInterface(TLog * logfile) : TMCMCUserInterface(logfile) {};
    ~BridgeTMCMCUserInterface() override = default;

    void _matchConfig(std::shared_ptr<TParameterDefinition> & def, const std::string & config, const std::string & val) {
        TMCMCUserInterface::_matchConfig(def, config, val);
    }
    void _matchConfig(std::shared_ptr<TObservationDefinition> & def, const std::string & config, const std::string & val) {
        TMCMCUserInterface::_matchConfig(def, config, val);
    }
    void _checkHeaderConfigFile(const std::string & filename, const std::vector<std::string> & actualColNames) {
        TMCMCUserInterface::_checkHeaderConfigFile(filename, actualColNames);
    }
    void _parseInitVals(std::vector<std::shared_ptr<TParameterDefinition>> & paramDefs, std::vector<std::shared_ptr<TObservationDefinition> > & observationDefs, const std::vector<std::string> & line) {
        TMCMCUserInterface::_parseInitVals(paramDefs, observationDefs, line);
    }
    void _readParamConfigFile(std::vector<std::shared_ptr<TParameterDefinition>> & paramDefs, std::vector<std::shared_ptr<TObservationDefinition> > & observationDefs, TParameters & parameters)  {
        TMCMCUserInterface::_readParamConfigFile(paramDefs, observationDefs, parameters);
    }
    void _readInitValFile(std::vector<std::shared_ptr<TParameterDefinition>> & paramDefs, std::vector<std::shared_ptr<TObservationDefinition> > & observationDefs, TParameters & parameters) {
        TMCMCUserInterface::_readInitValFile(paramDefs, observationDefs, parameters);
    }
    void _parseCommandLineParamConfigs(std::vector<std::shared_ptr<TParameterDefinition>> & paramDefs, std::vector<std::shared_ptr<TObservationDefinition> > & observationDefs, TParameters & parameters) {
        TMCMCUserInterface::_parseCommandLineParamConfigs(paramDefs, observationDefs, parameters);
    }
    void _parseCommandLineParamInitVals(std::vector<std::shared_ptr<TParameterDefinition>> & paramDefs, std::vector<std::shared_ptr<TObservationDefinition> > & observationDefs, TParameters & parameters) {
        TMCMCUserInterface::_parseCommandLineParamInitVals(paramDefs, observationDefs, parameters);
    }
};

class TMCMCUserInterfaceTest : public Test {
public:
    TParameters parameters;
    TLog logfile;
    BridgeTMCMCUserInterface * interface;
    std::shared_ptr<TParameterDefinition> def;
    std::shared_ptr<TObservationDefinition> obsDef;
    std::vector<std::shared_ptr<TParameterDefinition>> defs;
    std::vector<std::shared_ptr<TObservationDefinition>> observationDefs = {};

    void SetUp() override {
        def = std::make_shared<TParameterDefinition>("def");
        defs.emplace_back(def);
        defs.emplace_back(std::make_shared<TParameterDefinition>("def2"));

        obsDef = std::make_shared<TObservationDefinition>("obs");
        observationDefs.emplace_back(obsDef);

        interface = new BridgeTMCMCUserInterface(&logfile);
    }

    void TearDown() override {
        delete interface;
    }
};

TEST_F(TMCMCUserInterfaceTest, _matchConfig_prior){
    std::string config = "prior";

    // nothing happens if val is empty:
    interface->_matchConfig(def, config, "");
    EXPECT_EQ(def->prior(), MCMCPrior::uniform);

    // parameter that has a no prior parameter -> ok, can be changed!
    def->setPriorParams("");
    interface->_matchConfig(def, config, "exponential");
    EXPECT_EQ(def->prior(), MCMCPrior::exponential);

    // parameter that has a fixed number as prior parameter -> ok, can be changed!
    def->setPrior("uniform"); // reset prior
    def->setPriorParams("2");
    interface->_matchConfig(def, config, "exponential");
    EXPECT_EQ(def->prior(), MCMCPrior::exponential);

    // parameter that has a word (= parameter) as prior parameter -> can not be changed!
    def->setPrior("uniform"); // reset prior
    def->setPriorParams("other");
    EXPECT_THROW({try {interface->_matchConfig(def, config, "exponential");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);

    // -------- same applies to observation ---------
    // nothing happens if val is empty:
    interface->_matchConfig(obsDef, config, "");
    EXPECT_EQ(obsDef->prior(), MCMCPrior::uniform);

    obsDef->setPriorParams("");
    interface->_matchConfig(obsDef, config, "exponential");
    EXPECT_EQ(obsDef->prior(), MCMCPrior::exponential);

    // parameter that has a fixed number as prior parameter -> ok, can be changed!
    obsDef->setPrior("uniform"); // reset prior
    obsDef->setPriorParams("2");
    interface->_matchConfig(obsDef, config, "exponential");
    EXPECT_EQ(obsDef->prior(), MCMCPrior::exponential);

    // parameter that has a word (= parameter) as prior parameter -> can not be changed!
    obsDef->setPrior("uniform"); // reset prior
    obsDef->setPriorParams("other");
    EXPECT_THROW({try {interface->_matchConfig(obsDef, config, "exponential");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);

}

TEST_F(TMCMCUserInterfaceTest, _matchConfig_parameters){
    std::string config = "parameters";

    // nothing happens if val is empty:
    interface->_matchConfig(def, config, "");
    EXPECT_EQ(def->parameters(), "");

    // parameter that has a no prior parameter -> ok, can be changed!
    def->setPriorParams("");
    interface->_matchConfig(def, config, "2");
    EXPECT_EQ(def->parameters(), "2");

    // parameter that has a fixed number as prior parameter -> ok, can be changed!
    def->setPriorParams("2");
    interface->_matchConfig(def, config, "5");
    EXPECT_EQ(def->parameters(), "5");

    // parameter that has a word (= parameter) as prior parameter -> can not be changed!
    def->setPriorParams("other");
    EXPECT_THROW({try {interface->_matchConfig(def, config, "some");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);

    // -------- same applies to observation ---------
    // nothing happens if val is empty:
    interface->_matchConfig(obsDef, config, "");
    EXPECT_EQ(obsDef->parameters(), "");

    // parameter that has a no prior parameter -> ok, can be changed!
    obsDef->setPriorParams("");
    interface->_matchConfig(obsDef, config, "2");
    EXPECT_EQ(obsDef->parameters(), "2");

    // parameter that has a fixed number as prior parameter -> ok, can be changed!
    obsDef->setPriorParams("2");
    interface->_matchConfig(obsDef, config, "5");
    EXPECT_EQ(obsDef->parameters(), "5");

    // parameter that has a word (= parameter) as prior parameter -> can not be changed!
    obsDef->setPriorParams("other");
    EXPECT_THROW({try {interface->_matchConfig(obsDef, config, "some");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TMCMCUserInterfaceTest, _matchConfig_trace){
    std::string config = "traceFile";

    // nothing happens if val is empty:
    std::string val = "";
    interface->_matchConfig(def, config, val);
    EXPECT_EQ(def->traceFile(), "-");

    // trace can be changed
    val = "outer.txt";
    interface->_matchConfig(def, config, val);
    EXPECT_EQ(def->traceFile(), val);

    // observation can not have trace file -> throw
    EXPECT_THROW({try {interface->_matchConfig(obsDef, config, val);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TMCMCUserInterfaceTest, _matchConfig_meanVar){
    std::string config = "meanVarFile";

    // nothing happens if val is empty:
    std::string val = "";
    interface->_matchConfig(def, config, val);
    EXPECT_EQ(def->meanVarFile(), "-");

    // meanVar can be changed
    val = "outer.txt";
    interface->_matchConfig(def, config, val);
    EXPECT_EQ(def->meanVarFile(), val);

    // observation can not have meanVar file -> throw
    EXPECT_THROW({try {interface->_matchConfig(obsDef, config, val);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TMCMCUserInterfaceTest, _matchConfig_simulation){
    std::string config = "simulationFile";

    // nothing happens if val is empty:
    std::string val = "";
    interface->_matchConfig(def, config, val);
    EXPECT_EQ(def->simulationFile(), "-");

    // simulationFile can be changed
    val = "outer.txt";
    interface->_matchConfig(def, config, val);
    EXPECT_EQ(def->simulationFile(), val);

    // same applies to observation:
    // nothing happens if val is empty:
    val = "";
    interface->_matchConfig(obsDef, config, val);
    EXPECT_EQ(obsDef->simulationFile(), "-");

    // simulationFile can be changed
    val = "outer.txt";
    interface->_matchConfig(obsDef, config, val);
    EXPECT_EQ(obsDef->simulationFile(), val);
}

TEST_F(TMCMCUserInterfaceTest, _matchConfig_update){
    std::string config = "update";

    // nothing happens if val is empty:
    std::string val = "";
    interface->_matchConfig(def, config, val);
    EXPECT_EQ(def->isUpdated(), true);

    // update can be changed
    val = "false";
    interface->_matchConfig(def, config, val);
    EXPECT_EQ(def->isUpdated(), false);

    // observation can not be updated -> throw
    EXPECT_THROW({try {interface->_matchConfig(obsDef, config, val);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TMCMCUserInterfaceTest, _matchConfig_propKernel){
    std::string config = "propKernel";

    // nothing happens if val is empty:
    std::string val = "";
    interface->_matchConfig(def, config, val);
    EXPECT_EQ(def->propKernel(), ProposalKernel::normal);

    // propKernel can be changed
    val = "uniform";
    interface->_matchConfig(def, config, val);
    EXPECT_EQ(def->propKernel(), ProposalKernel::uniform);

    // observation can not have proposal kernel -> throw
    EXPECT_THROW({try {interface->_matchConfig(obsDef, config, val);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TMCMCUserInterfaceTest, _matchConfig_min){
    std::string config = "min";

    // nothing happens if val is empty:
    std::string val = "";
    interface->_matchConfig(def, config, val);
    EXPECT_EQ(def->min(), toString(std::numeric_limits<double>::lowest()));

    // nothing happens yet -> min is just stored, but not accepted yet!
    val = "-13.94";
    interface->_matchConfig(def, config, val);
    EXPECT_EQ(def->min(), toString(std::numeric_limits<double>::lowest()));

    // same applies to observation:
    // nothing happens if val is empty:
    val = "";
    interface->_matchConfig(obsDef, config, val);
    EXPECT_EQ(obsDef->min(), toString(std::numeric_limits<double>::lowest()));

    // nothing happens yet -> min is just stored, but not accepted yet!
    val = "-13.94";
    interface->_matchConfig(obsDef, config, val);
    EXPECT_EQ(obsDef->min(), toString(std::numeric_limits<double>::lowest()));
}

TEST_F(TMCMCUserInterfaceTest, _matchConfig_minIncluded){
    std::string config = "minIncluded";

    // nothing happens if val is empty:
    std::string val = "";
    interface->_matchConfig(def, config, val);
    EXPECT_EQ(def->minIncluded(), true);

    // nothing happens yet -> minIncluded is just stored, but not accepted yet!
    val = "false";
    interface->_matchConfig(def, config, val);
    EXPECT_EQ(def->minIncluded(), true);

    // same applies to observation:
    // nothing happens if val is empty:
    val = "";
    interface->_matchConfig(obsDef, config, val);
    EXPECT_EQ(obsDef->minIncluded(), true);

    // nothing happens yet -> minIncluded is just stored, but not accepted yet!
    val = "false";
    interface->_matchConfig(obsDef, config, val);
    EXPECT_EQ(obsDef->minIncluded(), true);
}

TEST_F(TMCMCUserInterfaceTest, _matchConfig_max){
    std::string config = "max";

    // nothing happens if val is empty:
    std::string val = "";
    interface->_matchConfig(def, config, val);
    EXPECT_EQ(def->max(), toString(std::numeric_limits<double>::max()));

    // nothing happens yet -> max is just stored, but not accepted yet!
    val = "13.94";
    interface->_matchConfig(def, config, val);
    EXPECT_EQ(def->max(), toString(std::numeric_limits<double>::max()));

    // same applies to observation:
    // nothing happens if val is empty:
    val = "";
    interface->_matchConfig(obsDef, config, val);
    EXPECT_EQ(obsDef->max(), toString(std::numeric_limits<double>::max()));

    // nothing happens yet -> max is just stored, but not accepted yet!
    val = "13.94";
    interface->_matchConfig(obsDef, config, val);
    EXPECT_EQ(obsDef->max(), toString(std::numeric_limits<double>::max()));
}

TEST_F(TMCMCUserInterfaceTest, _matchConfig_maxIncluded){
    std::string config = "maxIncluded";

    // nothing happens if val is empty:
    std::string val = "";
    interface->_matchConfig(def, config, val);
    EXPECT_EQ(def->maxIncluded(), true);

    // nothing happens yet -> maxIncluded is just stored, but not accepted yet!
    val = "false";
    interface->_matchConfig(def, config, val);
    EXPECT_EQ(def->maxIncluded(), true);

    // same applies to observation:
    // nothing happens if val is empty:
    val = "";
    interface->_matchConfig(obsDef, config, val);
    EXPECT_EQ(obsDef->maxIncluded(), true);

    // nothing happens yet -> maxIncluded is just stored, but not accepted yet!
    val = "false";
    interface->_matchConfig(obsDef, config, val);
    EXPECT_EQ(obsDef->maxIncluded(), true);
}

TEST_F(TMCMCUserInterfaceTest, _matchConfig_sharedJumpSize){
    std::string config = "sharedJumpSize";

    // nothing happens if val is empty:
    std::string val = "";
    interface->_matchConfig(def, config, val);
    EXPECT_EQ(def->oneJumpSizeForAll(), true);

    // sharedJumpSize can be changed
    val = "true";
    interface->_matchConfig(def, config, val);
    EXPECT_EQ(def->oneJumpSizeForAll(), true);

    // observation can not have proposal kernel -> throw
    EXPECT_THROW({try {interface->_matchConfig(obsDef, config, val);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TMCMCUserInterfaceTest, _parseHeaderConfigFile){
    // all names are given -> ok!
    std::vector<std::string> header =  {"name", "observation", "prior", "parameters", "traceFile", "meanVarFile", "simulationFile", "update", "propKernel", "min", "minIncluded", "max", "maxIncluded", "sharedJumpSize"};
    EXPECT_NO_THROW(interface->_checkHeaderConfigFile("file.txt", header));

    // only some names are given, and not in right order -> still ok!
    header = {"minIncluded", "meanVarFile", "traceFile", "min", "max", "update", "name"};
    EXPECT_NO_THROW(interface->_checkHeaderConfigFile("file.txt", header));

    // name is not given -> throw
    header = {"minIncluded", "meanVarFile", "traceFile", "min", "max", "update"};
    EXPECT_THROW({try {interface->_checkHeaderConfigFile("file.txt", header);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);

    // duplicate colnames -> throw
    header = {"name", "min", "traceFile", "min", "max", "update"};
    EXPECT_THROW({try {interface->_checkHeaderConfigFile("file.txt", header);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);

    // invalid colnames -> throw
    header = {"name", "mini", "TraceFile", "max", "update"};
    EXPECT_THROW({try {interface->_checkHeaderConfigFile("file.txt", header);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TMCMCUserInterfaceTest, _parseInitVals){
    // single parameter
    std::vector<std::string> line = {"def", "0.1", "jumpSizes.txt"};
    interface->_parseInitVals(defs, observationDefs, line);
    EXPECT_EQ(defs.at(0)->initVal(), "0.1");
    EXPECT_EQ(defs.at(0)->initJumpSizeProposal(), "jumpSizes.txt");

    // array
    line = {"def2", "1", "jumpSizes1.txt"};
    interface->_parseInitVals(defs, observationDefs, line);
    EXPECT_EQ(defs.at(1)->initVal(), "1");
    EXPECT_EQ(defs.at(1)->initJumpSizeProposal(), "jumpSizes1.txt");

    // invalid parameter name
    line = {"whatever", "2", "jumpSizes2.txt"};
    EXPECT_THROW({try {interface->_parseInitVals(defs, observationDefs, line);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);

    // observation can not have initial values -> throw
    line = {"obs", "2", "jumpSizes.txt"};
    EXPECT_THROW({try {interface->_parseInitVals(defs, observationDefs, line);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

struct WriteConfigFile {
public:
    TOutputFile file;
    explicit WriteConfigFile(const std::string &name) {
        std::vector<std::string> header = {"name", "observation", "min", "update", "traceFile"};
        file.open(name, header);

        file << "def" << "false" << "" << "false" << "out.txt" << std::endl;
        file << "def2" << "false" << "-1" << "" << "out1.txt" << std::endl;
        file.close();
    }
};

TEST_F(TMCMCUserInterfaceTest, _readParamConfigFile){
    // write config file
    WriteConfigFile write("config.txt");
    parameters.addParameter("config", "config.txt");

    // now call _readParamConfigFile
    interface->_readParamConfigFile(defs, observationDefs, parameters);

    // check if all configurations have been accepted
    EXPECT_EQ(defs.at(0)->min(),  toString(std::numeric_limits<double>::lowest()));
    EXPECT_EQ(defs.at(0)->isUpdated(), false);
    EXPECT_EQ(defs.at(0)->traceFile(), "out.txt");

    EXPECT_EQ(defs.at(1)->min(), "-1");
    EXPECT_EQ(defs.at(1)->isUpdated(), true);
    EXPECT_EQ(defs.at(1)->traceFile(), "out1.txt");

    remove("config.txt");
}

TEST_F(TMCMCUserInterfaceTest, _readParamConfigFile_paramNotGiven){
    // write config file
    WriteConfigFile write("config.txt");

    // now call _readParamConfigFile -> parameter config is not given -> will do nothing
    interface->_readParamConfigFile(defs, observationDefs, parameters);

    // check if nothing has changed
    EXPECT_EQ(defs.at(0)->min(),  toString(std::numeric_limits<double>::lowest()));
    EXPECT_EQ(defs.at(0)->isUpdated(), true);
    EXPECT_EQ(defs.at(0)->traceFile(), "-");

    EXPECT_EQ(defs.at(1)->min(),  toString(std::numeric_limits<double>::lowest()));
    EXPECT_EQ(defs.at(1)->isUpdated(), true);
    EXPECT_EQ(defs.at(1)->traceFile(), "-");

    remove("config.txt");
}

struct WriteConfigFile_invalid {
public:
    TOutputFile file;
    explicit WriteConfigFile_invalid(const std::string &name) {
        std::vector<std::string> header = {"name", "min", "update", "traceFile"};
        file.open(name, header);

        file << "whatever" << "" << "false" << "out.txt" << std::endl;
        file.close();
    }
};

TEST_F(TMCMCUserInterfaceTest, _readParamConfigFile_invalidParamName){
    // write config file
    WriteConfigFile_invalid write("config.txt");
    parameters.addParameter("config", "config.txt");

    // now call _readParamConfigFile -> throws because of invalid param name
    EXPECT_THROW({try {interface->_readParamConfigFile(defs, observationDefs, parameters);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    remove("config.txt");
}


struct WriteConfigFile_MinMaxChallenge {
public:
    TOutputFile file;
    explicit WriteConfigFile_MinMaxChallenge(std::vector<std::string>  line, const std::string &name) {
        std::vector<std::string> header = {"name", "min", "maxIncluded", "minIncluded", "max"};
        file.open(name, header);
        file.writeLine(line);
        file.close();
    }
};

TEST_F(TMCMCUserInterfaceTest, _readParamConfigFile_minSmallerThanOldMin){
    // write config file
    defs.at(0)->setMin("-1.");
    std::vector<std::string> line = {"def", "-2.", "", "", ""};
    WriteConfigFile_MinMaxChallenge write(line, "config.txt");
    parameters.addParameter("config", "config.txt");

    // now call _readParamConfigFile -> throws because of min is smaller than oldMin
    EXPECT_THROW({try {interface->_readParamConfigFile(defs, observationDefs, parameters);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    remove("config.txt");
}

TEST_F(TMCMCUserInterfaceTest, _readParamConfigFile_changeMinIncludedAndDontChangeMin_Throw){
    // write config file
    defs.at(0)->setMin("-1.", false);
    std::vector<std::string> line = {"def", "", "", "true", ""};
    WriteConfigFile_MinMaxChallenge write(line, "config.txt");
    parameters.addParameter("config", "config.txt");

    // now call _readParamConfigFile -> throws because of cannot change from minIncluded=F -> minIncluded=T without changing min
    EXPECT_THROW({try {interface->_readParamConfigFile(defs, observationDefs, parameters);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    remove("config.txt");
}

TEST_F(TMCMCUserInterfaceTest, _readParamConfigFile_changeMinIncludedAndChangeMinToSameValue_Throw){
    // write config file
    defs.at(0)->setMin("-1.", false);
    std::vector<std::string> line = {"def", "-1.", "", "true", ""};
    WriteConfigFile_MinMaxChallenge write(line, "config.txt");
    parameters.addParameter("config", "config.txt");

    // now call _readParamConfigFile -> throws because of cannot change from minIncluded=F -> minIncluded=T without changing min
    EXPECT_THROW({try {interface->_readParamConfigFile(defs, observationDefs, parameters);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    remove("config.txt");
}

TEST_F(TMCMCUserInterfaceTest, _readParamConfigFile_changeMinIncludedTrueFalse_Ok){
    // write config file
    defs.at(0)->setMin("-1.", true);
    std::vector<std::string> line = {"def", "", "", "false", ""};
    WriteConfigFile_MinMaxChallenge write(line, "config.txt");
    parameters.addParameter("config", "config.txt");

    interface->_readParamConfigFile(defs, observationDefs, parameters);
    EXPECT_EQ(defs.at(0)->min(), "-1.");
    EXPECT_FALSE(defs.at(0)->minIncluded());
    remove("config.txt");
}

TEST_F(TMCMCUserInterfaceTest, _readParamConfigFile_changeMinIncludedTrueTrue_Ok){
    // write config file
    defs.at(0)->setMin("-1.", true);
    std::vector<std::string> line = {"def", "-1.", "", "true", ""};
    WriteConfigFile_MinMaxChallenge write(line, "config.txt");
    parameters.addParameter("config", "config.txt");

    interface->_readParamConfigFile(defs, observationDefs, parameters);
    EXPECT_EQ(defs.at(0)->min(), "-1.");
    EXPECT_TRUE(defs.at(0)->minIncluded());
    remove("config.txt");
}

TEST_F(TMCMCUserInterfaceTest, _readParamConfigFile_changeMinIncludedFalseFalse_Ok){
    // write config file
    defs.at(0)->setMin("-1.", false);
    std::vector<std::string> line = {"def", "-1.", "", "false", ""};
    WriteConfigFile_MinMaxChallenge write(line, "config.txt");
    parameters.addParameter("config", "config.txt");

    interface->_readParamConfigFile(defs, observationDefs, parameters);
    EXPECT_EQ(defs.at(0)->min(), "-1.");
    EXPECT_FALSE(defs.at(0)->minIncluded());
    remove("config.txt");
}

TEST_F(TMCMCUserInterfaceTest, _readParamConfigFile_changeMinToLargerVal_Ok){
    // write config file
    defs.at(0)->setMin("-1.", false);
    std::vector<std::string> line = {"def", "0.", "", "true", ""};
    WriteConfigFile_MinMaxChallenge write(line, "config.txt");
    parameters.addParameter("config", "config.txt");

    interface->_readParamConfigFile(defs, observationDefs, parameters);
    EXPECT_EQ(defs.at(0)->min(), "0.");
    EXPECT_TRUE(defs.at(0)->minIncluded());
    remove("config.txt");
}

TEST_F(TMCMCUserInterfaceTest, _readParamConfigFile_maxLargerThanOldMax){
    // write config file
    defs.at(0)->setMax("1.");
    std::vector<std::string> line = {"def", "", "", "", "2."};
    WriteConfigFile_MinMaxChallenge write(line, "config.txt");
    parameters.addParameter("config", "config.txt");

    // now call _readParamConfigFile -> throws because of max is larger than oldMax
    EXPECT_THROW({try {interface->_readParamConfigFile(defs, observationDefs, parameters);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    remove("config.txt");
}

TEST_F(TMCMCUserInterfaceTest, _readParamConfigFile_changeMaxIncludedAndDontChangeMax_Throw){
    // write config file
    defs.at(0)->setMax("1.", false);
    std::vector<std::string> line = {"def", "", "true", "", ""};
    WriteConfigFile_MinMaxChallenge write(line, "config.txt");
    parameters.addParameter("config", "config.txt");

    // now call _readParamConfigFile -> throws because of cannot change from maxIncluded=F -> maxIncluded=T without changing max
    EXPECT_THROW({try {interface->_readParamConfigFile(defs, observationDefs, parameters);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    remove("config.txt");
}

TEST_F(TMCMCUserInterfaceTest, _readParamConfigFile_changeMaxIncludedAndChangeMaxToSameValue_Throw){
    // write config file
    defs.at(0)->setMax("1.", false);
    std::vector<std::string> line = {"def", "", "true", "", "1."};
    WriteConfigFile_MinMaxChallenge write(line, "config.txt");
    parameters.addParameter("config", "config.txt");

    // now call _readParamConfigFile -> throws because of cannot change from maxIncluded=F -> maxIncluded=T without changing max
    EXPECT_THROW({try {interface->_readParamConfigFile(defs, observationDefs, parameters);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    remove("config.txt");
}

TEST_F(TMCMCUserInterfaceTest, _readParamConfigFile_changeMaxIncludedTrueFalse_Ok){
    // write config file
    defs.at(0)->setMax("1.", true);
    std::vector<std::string> line = {"def", "", "false", "", ""};
    WriteConfigFile_MinMaxChallenge write(line, "config.txt");
    parameters.addParameter("config", "config.txt");

    interface->_readParamConfigFile(defs, observationDefs, parameters);
    EXPECT_EQ(defs.at(0)->max(), "1.");
    EXPECT_FALSE(defs.at(0)->maxIncluded());
    remove("config.txt");
}

TEST_F(TMCMCUserInterfaceTest, _readParamConfigFile_changeMaxIncludedTrueTrue_Ok){
    // write config file
    defs.at(0)->setMax("1.", true);
    std::vector<std::string> line = {"def", "", "true", "", ""};
    WriteConfigFile_MinMaxChallenge write(line, "config.txt");
    parameters.addParameter("config", "config.txt");

    interface->_readParamConfigFile(defs, observationDefs, parameters);
    EXPECT_EQ(defs.at(0)->max(), "1.");
    EXPECT_TRUE(defs.at(0)->maxIncluded());
    remove("config.txt");
}

TEST_F(TMCMCUserInterfaceTest, _readParamConfigFile_changeMaxIncludedFalseFalse_Ok){
    // write config file
    defs.at(0)->setMax("1.", false);
    std::vector<std::string> line = {"def", "", "false", "", ""};
    WriteConfigFile_MinMaxChallenge write(line, "config.txt");
    parameters.addParameter("config", "config.txt");

    interface->_readParamConfigFile(defs, observationDefs, parameters);
    EXPECT_EQ(defs.at(0)->max(), "1.");
    EXPECT_FALSE(defs.at(0)->maxIncluded());
    remove("config.txt");
}

TEST_F(TMCMCUserInterfaceTest, _readParamConfigFile_complexMultipleParameter){
    // write config file - complex with multiple parameters
    TOutputFile file;
    std::vector<std::string> header = {"name", "min", "maxIncluded", "minIncluded", "max"};
    file.open("config.txt", header);
    file << "def" << "" << "true" << "true" << 1. << std::endl;
    file << "def2" << 1 << "" << "false" << 2. << std::endl;
    file << "obs" << 5 << "" << "" << 10. << std::endl;
    file.close();

    // add developer conditions
    defs.at(0)->setMin("0.");
    defs.at(0)->setMax("1.", true);

    defs.at(1)->setMin("0.", false);
    defs.at(1)->setMax("2.", true);

    observationDefs.at(0)->setMin("0.", false);
    observationDefs.at(0)->setMax("20");

    parameters.addParameter("config", "config.txt");

    interface->_readParamConfigFile(defs, observationDefs, parameters);

    // now check
    EXPECT_EQ(defs.at(0)->min(), "0.");
    EXPECT_TRUE(defs.at(0)->minIncluded());
    EXPECT_EQ(defs.at(0)->max(), "1");
    EXPECT_TRUE(defs.at(0)->maxIncluded());

    EXPECT_EQ(defs.at(1)->min(), "1");
    EXPECT_FALSE(defs.at(1)->minIncluded());
    EXPECT_EQ(defs.at(1)->max(), "2");
    EXPECT_TRUE(defs.at(1)->maxIncluded());

    EXPECT_EQ(observationDefs.at(0)->min(), "5");
    EXPECT_FALSE(observationDefs.at(0)->minIncluded());
    EXPECT_EQ(observationDefs.at(0)->max(), "10");
    EXPECT_TRUE(observationDefs.at(0)->maxIncluded());

    remove("config.txt");
}

TEST_F(TMCMCUserInterfaceTest, _readParamConfigFile_minLargerThanMax){
    // write config file
    std::vector<std::string> line = {"def", "10.", "true", "false", "0."};
    WriteConfigFile_MinMaxChallenge write(line, "config.txt");
    parameters.addParameter("config", "config.txt");

    // min > max
    EXPECT_THROW({try {interface->_readParamConfigFile(defs, observationDefs, parameters);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    remove("config.txt");
}

struct WriteInitFile {
public:
    TOutputFile file;
    explicit WriteInitFile(const std::string &name) {
        std::vector<std::string> header = {"name", "value", "jumpSize"};
        file.open(name, header);
        file << "def" << 0.01 << 1 << std::endl;
        file << "def2" << "" << 2 << std::endl;
        file.close();
    }
};

TEST_F(TMCMCUserInterfaceTest, _readInitValFile){
    // write config file
    WriteInitFile write("init.txt");
    parameters.addParameter("initVals", "init.txt");

    // now call _readInitValFile
    interface->_readInitValFile(defs, observationDefs, parameters);

    // check if all configurations have been accepted
    EXPECT_EQ(defs.at(0)->initVal(), "0.01");
    EXPECT_EQ(defs.at(0)->initJumpSizeProposal(), "1");

    EXPECT_EQ(defs.at(1)->initVal(), "0");
    EXPECT_EQ(defs.at(1)->initJumpSizeProposal(), "2");

    remove("init.txt");
}

TEST_F(TMCMCUserInterfaceTest, _readInitValFile_paramNotGiven){
    // write config file
    WriteInitFile write("init.txt");

    // now call _readInitValFile -> parameter config is not given -> will do nothing
    interface->_readInitValFile(defs, observationDefs, parameters);

    // check if nothing has changed
    EXPECT_EQ(defs.at(0)->initVal(), "0");
    EXPECT_EQ(defs.at(0)->initJumpSizeProposal(), "0.1");

    EXPECT_EQ(defs.at(1)->initVal(), "0");
    EXPECT_EQ(defs.at(1)->initJumpSizeProposal(), "0.1");

    remove("init.txt");
}

struct WriteInitFile_invalid {
public:
    TOutputFile file;
    explicit WriteInitFile_invalid(const std::string &name) {
        std::vector<std::string> header = {"name", "value", "jumpSize"};
        file.open(name, header);

        file << "whatever" << 0.01 << 1 << std::endl;
        file.close();
    }
};

TEST_F(TMCMCUserInterfaceTest, _readInitValFile_invalidParamName){
    // write config file
    WriteInitFile_invalid write("init.txt");
    parameters.addParameter("initVals", "init.txt");

    // now call _readInitValFile -> throws because of invalid param name
    EXPECT_THROW({try {interface->_readInitValFile(defs, observationDefs, parameters);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TMCMCUserInterfaceTest, _parseCommandLineParamConfigs){
    // add parameters
    parameters.addParameter("def.update", "false");
    parameters.addParameter("def.traceFile", "out.txt");

    parameters.addParameter("def2.min", "-1");
    parameters.addParameter("def2.traceFile", "out1.txt");

    parameters.addParameter("obs.max", "1");
    parameters.addParameter("obs.simulationFile", "sim.txt");

    // invalid parameter names will not throw, but simply be ignored (and listed in the end as unused parameters)
    parameters.addParameter("whatever.min", "-10");
    parameters.addParameter("whatever.update", "false");

    interface->_parseCommandLineParamConfigs(defs, observationDefs, parameters);
    // check if all configurations have been accepted
    EXPECT_EQ(defs.at(0)->min(),  toString(std::numeric_limits<double>::lowest()));
    EXPECT_EQ(defs.at(0)->isUpdated(), false);
    EXPECT_EQ(defs.at(0)->traceFile(), "out.txt");

    EXPECT_EQ(defs.at(1)->min(), "-1");
    EXPECT_EQ(defs.at(1)->isUpdated(), true);
    EXPECT_EQ(defs.at(1)->traceFile(), "out1.txt");

    EXPECT_EQ(observationDefs.at(0)->max(), "1");
    EXPECT_EQ(observationDefs.at(0)->simulationFile(), "sim.txt");

    remove("init.txt");
}

TEST_F(TMCMCUserInterfaceTest, _parseCommandLineParamInitVals){
    // add parameters
    parameters.addParameter("def.value", "0.01");
    parameters.addParameter("def.jumpSize", "1");

    parameters.addParameter("def2.jumpSize", "2");

    // invalid parameter names will not throw, but simply be ignored (and listed in the end as unused parameters)
    parameters.addParameter("whatever.value", "-10");
    parameters.addParameter("whatever.jumpSize", "0.472");

    interface->_parseCommandLineParamInitVals(defs, observationDefs, parameters);
    // check if all configurations have been accepted
    EXPECT_EQ(defs.at(0)->initVal(), "0.01");
    EXPECT_EQ(defs.at(0)->initJumpSizeProposal(), "1");

    EXPECT_EQ(defs.at(1)->initVal(), "0");
    EXPECT_EQ(defs.at(1)->initJumpSizeProposal(), "2");
}