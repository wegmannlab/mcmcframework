//
// Created by madleina on 04.02.21.
//

#include "../../core/TDataTranslator.h"
#include "../TestCase.h"
using namespace testing;

//--------------------------------------------
// TDataTranslatorPhredScores
//--------------------------------------------

class TDataTranslatorPhredScoresTest : public Test {
public:
    // use uint8_t as StoredType, should not make a difference to other uints
    std::unique_ptr<TDataTranslatorPhredScores<double, uint8_t>> translator;

    void SetUp() override{
    }

};

// test: invalid types -> should throw upon construction

TEST_F(TDataTranslatorPhredScoresTest, constructor_StoredType_double){
    EXPECT_THROW((TDataTranslatorPhredScores<double, double>{10.}), std::runtime_error);
}

TEST_F(TDataTranslatorPhredScoresTest, constructor_StoredType_float){
    EXPECT_THROW((TDataTranslatorPhredScores<double, float>{10.}), std::runtime_error);
}

TEST_F(TDataTranslatorPhredScoresTest, constructor_StoredType_int){
    EXPECT_THROW((TDataTranslatorPhredScores<double, int>{10}), std::runtime_error);
}

TEST_F(TDataTranslatorPhredScoresTest, constructor_StoredType_bool){
    EXPECT_THROW((TDataTranslatorPhredScores<double, bool>{true}), std::runtime_error);
}

TEST_F(TDataTranslatorPhredScoresTest, translate_max_is_numeric_limit){
    uint8_t Max = 255; // numeric limit of uint8_t
    translator = std::make_unique<TDataTranslatorPhredScores<double, uint8_t>>(Max);

    EXPECT_FLOAT_EQ(translator->translate(0), 1.);
    EXPECT_FLOAT_EQ(translator->translate(1), 0.7943282);
    EXPECT_FLOAT_EQ(translator->translate(2), 0.6309573);
    EXPECT_FLOAT_EQ(translator->translate(3), 0.5011872);
    EXPECT_FLOAT_EQ(translator->translate(4), 0.3981072);
    EXPECT_FLOAT_EQ(translator->translate(5), 0.3162278);
    // ...
    EXPECT_FLOAT_EQ(translator->translate(254), 3.981072e-26);
    EXPECT_NEAR(translator->translate(255), 3.162278e-26, 0.00000000000001); // number is so small that it gets different from R due to numerical imprecision
    // can not go higher with uint8_t
}

TEST_F(TDataTranslatorPhredScoresTest, translate_max_below_numeric_limit){
    uint8_t Max = 100;
    translator = std::make_unique<TDataTranslatorPhredScores<double, uint8_t>>(Max);

    EXPECT_FLOAT_EQ(translator->translate(0), 1.);
    EXPECT_FLOAT_EQ(translator->translate(1), 0.7943282);
    EXPECT_FLOAT_EQ(translator->translate(2), 0.6309573);
    EXPECT_FLOAT_EQ(translator->translate(3), 0.5011872);
    EXPECT_FLOAT_EQ(translator->translate(4), 0.3981072);
    EXPECT_FLOAT_EQ(translator->translate(5), 0.3162278);
    // ...
    EXPECT_FLOAT_EQ(translator->translate(99), 1.258925e-10);
    EXPECT_FLOAT_EQ(translator->translate(100), 1e-10);
    EXPECT_FLOAT_EQ(translator->translate(101), 1e-10); // cut at 100
    EXPECT_FLOAT_EQ(translator->translate(255), 1e-10); // cut at 100
}

TEST_F(TDataTranslatorPhredScoresTest, translate_forth_and_back){
    uint8_t Max = 100;
    translator = std::make_unique<TDataTranslatorPhredScores<double, uint8_t>>(Max);

    uint8_t value = 0;
    EXPECT_FLOAT_EQ(translator->translateToStoredType(translator->translate(value)), value);
    value = 1;
    EXPECT_FLOAT_EQ(translator->translateToStoredType(translator->translate(value)), value);
    value = 2;
    EXPECT_FLOAT_EQ(translator->translateToStoredType(translator->translate(value)), value);
    value = 3;
    EXPECT_FLOAT_EQ(translator->translateToStoredType(translator->translate(value)), value);
    value = 4;
    EXPECT_FLOAT_EQ(translator->translateToStoredType(translator->translate(value)), value);
    value = 5;
    EXPECT_FLOAT_EQ(translator->translateToStoredType(translator->translate(value)), value);
    // ...
    value = 99;
    EXPECT_FLOAT_EQ(translator->translateToStoredType(translator->translate(value)), value);
    value = 100;
    EXPECT_FLOAT_EQ(translator->translateToStoredType(translator->translate(value)), value);
    value = 101;
    EXPECT_FLOAT_EQ(translator->translateToStoredType(translator->translate(value)), 100); // cap at 100
    value = 255;
    EXPECT_FLOAT_EQ(translator->translateToStoredType(translator->translate(value)), 100); // cap at 100
}