//
// Created by madleina on 07.04.21.
//

#include "../../core/TTransitionMatrixExponential.h"
#include "../../core/TPriorWithHyperPrior_MixedModels.h"
#include "../../core/TMCMCParameter.h"
#include "../TestCase.h"
using namespace testing;

//-------------------------------------------
// TGeneratingMatrixBool
//-------------------------------------------

TEST(TGeneratingMatrixBoolTest, constructor){
    TGeneratingMatrixBool<double, size_t> mat;

    const TBandMatrix& bandMatrix = mat.getGeneratingMatrix();
    EXPECT_EQ(bandMatrix.size(), 2);
    EXPECT_EQ(bandMatrix.bandwidth(), 1);
}

TEST(TGeneratingMatrixBoolTest, resize){
    TGeneratingMatrixBool<double, size_t> mat;

    mat.resize(2, 1);
    const TBandMatrix& bandMatrix = mat.getGeneratingMatrix();
    EXPECT_EQ(bandMatrix.size(), 2);
    EXPECT_EQ(bandMatrix.bandwidth(), 1);

    // throw if invalid numStates/bandwidth
    EXPECT_THROW(mat.resize(3, 1), std::runtime_error);
    EXPECT_THROW(mat.resize(2, 2), std::runtime_error);
}

TEST(TGeneratingMatrixBoolTest, fillGeneratingMatrix){
    TGeneratingMatrixBool<double, size_t> mat;

    mat.fillGeneratingMatrix({0.1, 0.7});
    EXPECT_FLOAT_EQ(mat(0,0), -0.1); EXPECT_FLOAT_EQ(mat(0,1), 0.1);
    EXPECT_FLOAT_EQ(mat(1,0), 0.7); EXPECT_FLOAT_EQ(mat(1,1), -0.7);
}

TEST(TGeneratingMatrixBoolTest, stationary){
    TGeneratingMatrixBool<double, size_t> mat;

    mat.fillGeneratingMatrix({0.1, 0.7});
    std::vector<double> values;
    EXPECT_TRUE(mat.fillStationary(values));

    EXPECT_FLOAT_EQ(values[0], 0.875);
    EXPECT_FLOAT_EQ(values[1], 0.125);
}

//-------------------------------------------
// TGeneratingMatrixLadder
//-------------------------------------------

TEST(TGeneratingMatrixLadderTest, constructor){
    TGeneratingMatrixLadder<double, size_t> mat(5);

    const TBandMatrix& bandMatrix = mat.getGeneratingMatrix();
    EXPECT_EQ(bandMatrix.size(), 5);
    EXPECT_EQ(bandMatrix.bandwidth(), 1);
}

TEST(TGeneratingMatrixLadderTest, resize){
    TGeneratingMatrixLadder<double, size_t> mat(10);

    mat.resize(5, 1);
    const TBandMatrix& bandMatrix = mat.getGeneratingMatrix();
    EXPECT_EQ(bandMatrix.size(), 5);
    EXPECT_EQ(bandMatrix.bandwidth(), 1);

    // throw if invalid bandwidth
    EXPECT_THROW(mat.resize(7, 2), std::runtime_error);
}

TEST(TGeneratingMatrixLadderTest, fillGeneratingMatrix_2States){
    TGeneratingMatrixLadder<double, size_t> mat(2);

    mat.fillGeneratingMatrix({0.2});
    EXPECT_FLOAT_EQ(mat(0,0), -0.2); EXPECT_FLOAT_EQ(mat(0,1), 0.2);
    EXPECT_FLOAT_EQ(mat(1,0), 0.2); EXPECT_FLOAT_EQ(mat(1,1), -0.2);
}

TEST(TGeneratingMatrixLadderTest, fillGeneratingMatrix_3States){
    TGeneratingMatrixLadder<double, size_t> mat(3);

    mat.fillGeneratingMatrix({0.2});
    EXPECT_FLOAT_EQ(mat(0,0), -0.2); EXPECT_FLOAT_EQ(mat(0,1), 0.2); EXPECT_FLOAT_EQ(mat(0,2), 0.);
    EXPECT_FLOAT_EQ(mat(1,0), 0.2); EXPECT_FLOAT_EQ(mat(1,1), -0.4); EXPECT_FLOAT_EQ(mat(1,2), 0.2);
    EXPECT_FLOAT_EQ(mat(2,0), 0.); EXPECT_FLOAT_EQ(mat(2,1), 0.2); EXPECT_FLOAT_EQ(mat(2,2), -0.2);
}

TEST(TGeneratingMatrixLadderTest, fillGeneratingMatrix_5States){
    TGeneratingMatrixLadder<double, size_t> mat(5);

    mat.fillGeneratingMatrix({0.2});
    EXPECT_FLOAT_EQ(mat(0,0), -0.2); EXPECT_FLOAT_EQ(mat(0,1), 0.2); EXPECT_FLOAT_EQ(mat(0,2), 0.); EXPECT_FLOAT_EQ(mat(0,3), 0.); EXPECT_FLOAT_EQ(mat(0,4), 0.);
    EXPECT_FLOAT_EQ(mat(1,0), 0.2); EXPECT_FLOAT_EQ(mat(1,1), -0.4); EXPECT_FLOAT_EQ(mat(1,2), 0.2); EXPECT_FLOAT_EQ(mat(1,3), 0.); EXPECT_FLOAT_EQ(mat(1,4), 0.);
    EXPECT_FLOAT_EQ(mat(2,0), 0.); EXPECT_FLOAT_EQ(mat(2,1), 0.2); EXPECT_FLOAT_EQ(mat(2,2), -0.4); EXPECT_FLOAT_EQ(mat(2,3), 0.2); EXPECT_FLOAT_EQ(mat(2,4), 0.);
    EXPECT_FLOAT_EQ(mat(3,0), 0.); EXPECT_FLOAT_EQ(mat(3,1), 0.); EXPECT_FLOAT_EQ(mat(3,2), 0.2); EXPECT_FLOAT_EQ(mat(3,3), -0.4); EXPECT_FLOAT_EQ(mat(3,4), 0.2);
    EXPECT_FLOAT_EQ(mat(4,0), 0.); EXPECT_FLOAT_EQ(mat(4,1), 0.); EXPECT_FLOAT_EQ(mat(4,2), 0.); EXPECT_FLOAT_EQ(mat(4,3), 0.2); EXPECT_FLOAT_EQ(mat(4,4), -0.2);
}

TEST(TGeneratingMatrixLadderTest, fillStationary){
    TGeneratingMatrixLadder<double, size_t> mat(3);

    mat.fillGeneratingMatrix({0.2});
    std::vector<double> values;
    EXPECT_TRUE(mat.fillStationary(values));

    EXPECT_FLOAT_EQ(values[0], 0.3333333);
    EXPECT_FLOAT_EQ(values[1], 0.3333333);
    EXPECT_FLOAT_EQ(values[2], 0.3333333);
}

//-------------------------------------------
// TTransitionMatrixExponentialNelderMead
//-------------------------------------------

class TTransitionMatrixExponentialTest : public TTransitionMatrixExponentialNelderMead<double, size_t, size_t>, public ::testing::Test {
    // allows testing of protected member functions
};

TEST_F(TTransitionMatrixExponentialTest, initializeWithDistances){
    uint16_t numStates = 2;
    std::shared_ptr<TDistancesBinnedBase> distances = std::make_shared<TDistancesBinned<uint8_t>>(100);
    distances->add(0, "junk_1"); // pro forma, to avoid error that distances are not filled

    initializeWithDistances(numStates, distances);

    EXPECT_EQ(this->numStates(), numStates);
    EXPECT_EQ(numDistanceGroups(), 8);

    for (int i = 0; i < numDistanceGroups(); i++) {
        TMatrix mat = (*this)[0];
        EXPECT_EQ(mat.size(), numStates);

        EXPECT_EQ(mat(0, 0), 0.);
        EXPECT_EQ(mat(0, 1), 0.);
        EXPECT_EQ(mat(1, 0), 0.);
        EXPECT_EQ(mat(1, 1), 0.);
    }
}

TEST_F(TTransitionMatrixExponentialTest, operator_square_brackets){
    uint16_t numStates = 2;
    std::shared_ptr<TDistancesBinnedBase> distances = std::make_shared<TDistancesBinned<uint8_t>>(100);
    distances->add(0, "junk_1"); // pro forma, to avoid error that distances are not filled

    // non-const
    TTransitionMatrixExponentialNelderMead base;
    base.initializeWithDistances(numStates, distances);

    // this is ok
    EXPECT_EQ(base[0].size(), numStates);
    // this is outside range
    EXPECT_THROW(base[8], std::runtime_error);

    // const
    const TTransitionMatrixExponentialNelderMead base2(numStates, distances);
    // this is ok
    EXPECT_EQ(base2[0].size(), numStates);
    // this is outside range
    EXPECT_THROW(base2[8], std::runtime_error);
}

TEST_F(TTransitionMatrixExponentialTest, _fillTransitionProbabilities_BoolGeneratingMatrix){
    uint16_t numStates = 2;
    std::shared_ptr<TDistancesBinnedBase> distances = std::make_shared<TDistancesBinned<uint8_t>>(16); // results in 4 distance groups
    distances->add(0, "junk_1"); // pro forma, to avoid error that distances are not filled
    initializeWithDistances(numStates, distances);
    std::unique_ptr<TGeneratingMatrixBase<double, size_t>> generatingMatrix = std::make_unique<TGeneratingMatrixBool<double, size_t>>();
    addGeneratingMatrix(generatingMatrix);

    // fill generating matrix
    double lambda1 = 0.1; double lambda2 = 0.2;
    _fillGeneratingMatrix({lambda1, lambda2});

    // fill transition probabilities
    _fillTransitionProbabilities();

    // [1]
    EXPECT_NEAR((*this)[1](0, 0), 0.9136061, 0.01);
    EXPECT_NEAR((*this)[1](0, 1), 0.08639393, 0.01);
    EXPECT_NEAR((*this)[1](1, 0), 0.1727879, 0.01);
    EXPECT_NEAR((*this)[1](1, 1), 0.82721215, 0.01);

    // [2]
    EXPECT_NEAR((*this)[2](0, 0), 0.8496039, 0.01);
    EXPECT_NEAR((*this)[2](0, 1), 0.1503961, 0.01);
    EXPECT_NEAR((*this)[2](1, 0), 0.3007922, 0.01);
    EXPECT_NEAR((*this)[2](1, 1), 0.6992078, 0.01);

    // [3]
    EXPECT_NEAR((*this)[3](0, 0), 0.7670647, 0.01);
    EXPECT_NEAR((*this)[3](0, 1), 0.2329353, 0.01);
    EXPECT_NEAR((*this)[3](1, 0), 0.4658705, 0.01);
    EXPECT_NEAR((*this)[3](1, 1), 0.5341295, 0.01);

    // [4]
    EXPECT_NEAR((*this)[4](0, 0), 0.696906, 0.01);
    EXPECT_NEAR((*this)[4](0, 1), 0.303094, 0.01);
    EXPECT_NEAR((*this)[4](1, 0), 0.606188, 0.01);
    EXPECT_NEAR((*this)[4](1, 1), 0.393812, 0.01);
}


TEST_F(TTransitionMatrixExponentialTest, _fillTransitionProbabilities_LadderGeneratingMatrix){
    uint16_t numStates = 3;
    std::shared_ptr<TDistancesBinnedBase> distances = std::make_shared<TDistancesBinned<uint8_t>>(16); // results in 4 distance groups
    distances->add(0, "junk_1"); // pro forma, to avoid error that distances are not filled
    initializeWithDistances(numStates, distances);
    std::unique_ptr<TGeneratingMatrixBase<double, size_t>> generatingMatrix = std::make_unique<TGeneratingMatrixLadder<double, size_t>>(numStates);
    addGeneratingMatrix(generatingMatrix);

    // fill generating matrix
    double kappa = 0.2;
    _fillGeneratingMatrix({kappa});

    // fill transition probabilities
    _fillTransitionProbabilities();

    // [1]
    EXPECT_NEAR((*this)[1](0, 0), 0.83416732, 0.01); EXPECT_NEAR((*this)[1](0, 1), 0.1503961, 0.01); EXPECT_NEAR((*this)[1](0, 2), 0.01543656, 0.01);
    EXPECT_NEAR((*this)[1](1, 0), 0.15039612, 0.01); EXPECT_NEAR((*this)[1](1, 1), 0.6992078, 0.01); EXPECT_NEAR((*this)[1](1, 2), 0.15039612, 0.01);
    EXPECT_NEAR((*this)[1](2, 0), 0.01543656, 0.01); EXPECT_NEAR((*this)[1](2, 1), 0.1503961, 0.01); EXPECT_NEAR((*this)[1](2, 2), 0.83416732, 0.01);

    // [2]
    EXPECT_NEAR((*this)[2](0, 0), 0.71869239, 0.01); EXPECT_NEAR((*this)[2](0, 1), 0.2329353, 0.01); EXPECT_NEAR((*this)[2](0, 2), 0.04837235, 0.01);
    EXPECT_NEAR((*this)[2](1, 0), 0.23293526, 0.01); EXPECT_NEAR((*this)[2](1, 1), 0.5341295, 0.01); EXPECT_NEAR((*this)[2](1, 2), 0.23293526, 0.01);
    EXPECT_NEAR((*this)[2](2, 0), 0.04837235, 0.01); EXPECT_NEAR((*this)[2](2, 1), 0.2329353, 0.01); EXPECT_NEAR((*this)[2](2, 2), 0.71869239, 0.01);

    // [3]
    EXPECT_NEAR((*this)[3](0, 0), 0.5731175, 0.01); EXPECT_NEAR((*this)[3](0, 1), 0.303094, 0.01); EXPECT_NEAR((*this)[3](0, 2), 0.1237885, 0.01);
    EXPECT_NEAR((*this)[3](1, 0), 0.3030940, 0.01); EXPECT_NEAR((*this)[3](1, 1), 0.393812, 0.01); EXPECT_NEAR((*this)[3](1, 2), 0.3030940, 0.01);
    EXPECT_NEAR((*this)[3](2, 0), 0.1237885, 0.01); EXPECT_NEAR((*this)[3](2, 1), 0.303094, 0.01); EXPECT_NEAR((*this)[3](2, 2), 0.5731175, 0.01);

    // [4]
    EXPECT_NEAR((*this)[4](0, 0), 0.4356532, 0.01); EXPECT_NEAR((*this)[4](0, 1), 0.3305901, 0.01); EXPECT_NEAR((*this)[4](0, 2), 0.2337567, 0.01);
    EXPECT_NEAR((*this)[4](1, 0), 0.3305901, 0.01); EXPECT_NEAR((*this)[4](1, 1), 0.3388198, 0.01); EXPECT_NEAR((*this)[4](1, 2), 0.3305901, 0.01);
    EXPECT_NEAR((*this)[4](2, 0), 0.2337567, 0.01); EXPECT_NEAR((*this)[4](2, 1), 0.3305901, 0.01); EXPECT_NEAR((*this)[4](2, 2), 0.4356532, 0.01);
}

TEST_F(TTransitionMatrixExponentialTest, _fillStationaryDistribution_BoolGeneratingMatrix) {
    uint16_t numStates = 2;
    std::shared_ptr<TDistancesBinnedBase> distances = std::make_shared<TDistancesBinned<uint8_t>>(16); // results in 4 distance groups
    distances->add(0, "junk_1"); // pro forma, to avoid error that distances are not filled
    initializeWithDistances(numStates, distances);
    std::unique_ptr<TGeneratingMatrixBase<double, size_t>> generatingMatrix = std::make_unique<TGeneratingMatrixBool<double, size_t>>();
    addGeneratingMatrix(generatingMatrix);

    // fill generating matrix
    double lambda1 = 0.1;
    double lambda2 = 0.2;
    _fillGeneratingMatrix({lambda1, lambda2});

    // fill transition probabilities
    _fillTransitionProbabilities();
    _fillStationaryDistribution();

    // [0]
    EXPECT_NEAR((*this)[0](0, 0), 0.66666, 0.001);
    EXPECT_NEAR((*this)[0](0, 1), 0.33333, 0.001);
    EXPECT_NEAR((*this)[0](1, 0), 0.66666, 0.001);
    EXPECT_NEAR((*this)[0](1, 1), 0.33333, 0.001);

    EXPECT_NEAR(stationary(0), 0.66666, 0.001);
    EXPECT_NEAR(stationary(1), 0.33333, 0.001);
}

TEST_F(TTransitionMatrixExponentialTest, _fillStationaryDistribution_LadderGeneratingMatrix) {
    uint16_t numStates = 3;
    std::shared_ptr<TDistancesBinnedBase> distances = std::make_shared<TDistancesBinned<uint8_t>>(16); // results in 4 distance groups
    distances->add(0, "junk_1"); // pro forma, to avoid error that distances are not filled
    initializeWithDistances(numStates, distances);
    std::unique_ptr<TGeneratingMatrixBase<double, size_t>> generatingMatrix = std::make_unique<TGeneratingMatrixLadder<double, size_t>>(numStates);
    addGeneratingMatrix(generatingMatrix);

    // fill generating matrix
    double kappa = 0.2;
    _fillGeneratingMatrix({kappa});

    // fill transition probabilities
    _fillTransitionProbabilities();
    _fillStationaryDistribution();

    // [0]
    EXPECT_NEAR((*this)[0](0, 0), 0.33333, 0.001); EXPECT_NEAR((*this)[0](0, 1), 0.33333, 0.001); EXPECT_NEAR((*this)[0](0, 2), 0.33333, 0.001);
    EXPECT_NEAR((*this)[0](1, 0), 0.33333, 0.001); EXPECT_NEAR((*this)[0](1, 1), 0.33333, 0.001); EXPECT_NEAR((*this)[0](1, 2), 0.33333, 0.001);
    EXPECT_NEAR((*this)[0](2, 0), 0.33333, 0.001); EXPECT_NEAR((*this)[0](2, 1), 0.33333, 0.001); EXPECT_NEAR((*this)[0](2, 2), 0.33333, 0.001);

    EXPECT_NEAR(stationary(0), 0.33333, 0.001);
    EXPECT_NEAR(stationary(1), 0.33333, 0.001);
    EXPECT_NEAR(stationary(2), 0.33333, 0.001);
}

class TGeneratingMatrixDummy : public TGeneratingMatrixBase<double, size_t> {
    // some other generating matrix, to specifically test the function _fillStationaryDistribution_SolveNormalEquations
    // (all other existing generating matrices do not use this function as they can caluclate the stationary distribution directly from the parameters of the generating matrix)
public:
    TGeneratingMatrixDummy() {
        resize(3, 2);
    }

    void fillGeneratingMatrix(const std::vector<double> & Values) override {
        // fill generating matrix
        double lambda12 = 0.73;
        double lambda13 = 0.62;
        double lambda21 = 0.123;
        double lambda23 = 0.99;
        double lambda31 = 0.28;
        double lambda32 = 0.92;
        _generatingMatrix.resize(3, 2);
        _generatingMatrix(0, 0) = -lambda12-lambda13; _generatingMatrix(0, 1) = lambda12; _generatingMatrix(0, 2) = lambda13;
        _generatingMatrix(1, 0) = lambda21; _generatingMatrix(1, 1) = -lambda21-lambda23; _generatingMatrix(1, 2) = lambda23;
        _generatingMatrix(2, 0) = lambda31; _generatingMatrix(2, 1) = lambda32; _generatingMatrix(2, 2) = -lambda31-lambda32;
    }
};

TEST_F(TTransitionMatrixExponentialTest, _fillStationaryDistribution_SolveNormalEquations) {
    uint16_t numStates = 3;
    std::shared_ptr<TDistancesBinnedBase> distances = std::make_shared<TDistancesBinned<uint8_t>>(16); // results in 4 distance groups (but doesn't matter for this test)
    distances->add(0, "junk_1"); // pro forma, to avoid error that distances are not filled
    initializeWithDistances(numStates, distances);
    std::unique_ptr<TGeneratingMatrixBase<double, size_t>> generatingMatrix = std::make_unique<TGeneratingMatrixDummy>();
    generatingMatrix->fillGeneratingMatrix({});
    addGeneratingMatrix(generatingMatrix);

    // fill transition probabilities
    _fillTransitionProbabilities();
    _fillStationaryDistribution();

    // [0]
    EXPECT_NEAR((*this)[0](0, 0), 0.1293560, 0.001); EXPECT_NEAR((*this)[0](0, 1), 0.4404439, 0.001); EXPECT_NEAR((*this)[0](0, 2), 0.4302001, 0.001);
    EXPECT_NEAR((*this)[0](1, 0), 0.1293560, 0.001); EXPECT_NEAR((*this)[0](1, 1), 0.4404439, 0.001); EXPECT_NEAR((*this)[0](1, 2), 0.4302001, 0.001);
    EXPECT_NEAR((*this)[0](2, 0), 0.1293560, 0.001); EXPECT_NEAR((*this)[0](2, 1), 0.4404439, 0.001); EXPECT_NEAR((*this)[0](2, 2), 0.4302001, 0.001);

    EXPECT_NEAR(stationary(0), 0.1293560, 0.001);
    EXPECT_NEAR(stationary(1), 0.4404439, 0.001);
    EXPECT_NEAR(stationary(2), 0.4302001, 0.001);
}