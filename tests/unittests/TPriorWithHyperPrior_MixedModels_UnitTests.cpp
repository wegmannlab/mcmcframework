//
// Created by madleina on 30.11.20.
//

#include "../TestCase.h"
#include "../../core/TPriorWithHyperPrior_MixedModels.h"
#include "../../core/TMCMCParameter.h"
using namespace testing;

template<class T>
class BridgeTMCMCArrayParameter : public TMCMCParameter<T> {
public:
    BridgeTMCMCArrayParameter(std::shared_ptr<TPriorWithHyperPrior<T>> Prior, const std::shared_ptr<TParameterDefinition> & def, TRandomGenerator* RandomGenerator)
            : TMCMCParameter<T>(Prior, def, RandomGenerator){};
    ~BridgeTMCMCArrayParameter() override = default;

    TBoundary<T> getBoundary(){
        return this->_boundary;
    }
};

//--------------------------------------------
// TPriorNormalMixedModelWithHyperPrior
class BridgeTPriorNormalMixedModelWithHyperPrior : public TPriorNormalMixedModelWithHyperPrior<double> {
public:
    BridgeTPriorNormalMixedModelWithHyperPrior() : TPriorNormalMixedModelWithHyperPrior() {};
    // update mean
    double _calcLLUpdateMean(const size_t& k) {
        return TPriorNormalMixedModelWithHyperPrior::_calcLLUpdateMean(k);
    }
    double _calcLogHUpdateMean(const size_t& k) {
        return TPriorNormalMixedModelWithHyperPrior::_calcLogHUpdateMean(k);
    }
    // update var
    double _calcLLUpdateVar(const size_t& k) {
        return TPriorNormalMixedModelWithHyperPrior::_calcLLUpdateVar(k);
    }
    double _calcLogHUpdateVar(const size_t& k) {
        return TPriorNormalMixedModelWithHyperPrior::_calcLogHUpdateVar(k);
    }
    // update z
    double _calcLLUpdateZ(const size_t & i){
        return TPriorNormalMixedModelWithHyperPrior::_calcLLUpdateZ(i);
    }
    double  _calcLogHUpdateZ(const size_t & i){
        return TPriorNormalMixedModelWithHyperPrior::_calcLogHUpdateZ(i);
    }
    void _fillMeanParameterValues(std::vector<double> & meanParamValues){
        return TPriorNormalMixedModelWithHyperPrior::_fillMeanParameterValues(meanParamValues);
    }
    void _findMostDistantPoints(std::vector<double> & minValues, const std::vector<double> & meanParameterValues){
        return TPriorNormalMixedModelWithHyperPrior::_findMostDistantPoints(minValues, meanParameterValues);
    }
    void _setInitialZ(std::vector<double> & minValues, const std::vector<double> & meanParameterValues){
        return TPriorNormalMixedModelWithHyperPrior::_setInitialZ(minValues, meanParameterValues);
    }
    void _setInitialMeanAndVar(){
        return TPriorNormalMixedModelWithHyperPrior::_setInitialMeanAndVar();
    }
    void reSetK(size_t k){
        _K = k;
    }
};

class TPriorNormalMixedModelWithHyperPriorTest : public Test {
protected:
    std::shared_ptr<BridgeTPriorNormalMixedModelWithHyperPrior> priorWithHyperPrior;

    std::shared_ptr<TParameterDefinition> defMean0;
    std::shared_ptr<TParameterDefinition> defMean1;
    std::shared_ptr<TParameterDefinition> defMean2;
    std::shared_ptr<TParameterDefinition> defVar0;
    std::shared_ptr<TParameterDefinition> defVar1;
    std::shared_ptr<TParameterDefinition> defVar2;
    std::shared_ptr<TParameterDefinition> defZ;

    std::unique_ptr<TPrior<double>> priorMean0;
    std::unique_ptr<TPrior<double>> priorMean1;
    std::unique_ptr<TPrior<double>> priorMean2;
    std::unique_ptr<TPrior<double>> priorVar0;
    std::unique_ptr<TPrior<double>> priorVar1;
    std::unique_ptr<TPrior<double>> priorVar2;
    std::unique_ptr<TPrior<uint8_t>> priorZ;

    std::shared_ptr<TMCMCParameter<double>> mean0;
    std::shared_ptr<TMCMCParameter<double>> mean1;
    std::shared_ptr<TMCMCParameter<double>> mean2;
    std::shared_ptr<TMCMCParameter<double>> var0;
    std::shared_ptr<TMCMCParameter<double>> var1;
    std::shared_ptr<TMCMCParameter<double>> var2;
    std::shared_ptr<TMCMCParameter<uint8_t>> z;

    std::shared_ptr<BridgeTMCMCArrayParameter<double>> param; // parameter on which prior is defined
    std::shared_ptr<TParameterDefinition> def;
    std::shared_ptr<TNamesEmpty> dimNames;

    TRandomGenerator randomGenerator;
    std::vector<std::shared_ptr<TObservationsBase>> observations = {};

    void SetUp() override {
        defMean0 = std::make_shared<TParameterDefinition>("defMean0");
        defMean1 = std::make_shared<TParameterDefinition>("defMean1");
        defMean2 = std::make_shared<TParameterDefinition>("defMean2");
        defVar0 = std::make_shared<TParameterDefinition>("defVar0");
        defVar1 = std::make_shared<TParameterDefinition>("defVar1");
        defVar2 = std::make_shared<TParameterDefinition>("defVar2");
        defZ = std::make_shared<TParameterDefinition>("defZ");

        // initialize param
        priorWithHyperPrior = std::make_shared<BridgeTPriorNormalMixedModelWithHyperPrior>();
        def = std::make_shared<TParameterDefinition>("defParam");
    }

    void setInitialValues(){
        // values were generated in R with in script normal_mixedModel.R
        defMean0->setInitVal("-0.626453810742332");
        defMean1->setInitVal("0.183643324222082");
        defMean2->setInitVal("-0.835628612410047");
        defVar0->setInitVal("0.218034312889587");
        defVar1->setInitVal("1.44748426873204");
        defVar2->setInitVal("0.614781026707447");

        defZ->setType("uint8_t");
        defZ->setInitVal("1,1,1,2,0,1,2,0,1,0");

        def->setInitVal("0.129583051999254,0.164164585485749,1.3191862641652,-0.115077080828884,-0.916536785000671,1.17166637278492,-0.222371391058945,-1.66058897590131,0.898174558239648,-0.101176942503107");
    }

    void setPriors(){
        // mean0
        priorMean0 = std::make_unique<TPriorNormal<double>>();
        priorMean0->initialize("0,1");

        // mean1
        priorMean1 = std::make_unique<TPriorNormal<double>>();
        priorMean1->initialize("0,1");

        // mean2
        priorMean2 = std::make_unique<TPriorNormal<double>>();
        priorMean2->initialize("0,1");

        // var0
        priorVar0 = std::make_unique<TPriorExponential<double>>();
        priorVar0->initialize("10");

        // var1
        priorVar1 = std::make_unique<TPriorExponential<double>>();
        priorVar1->initialize("2");

        // var2
        priorVar2 = std::make_unique<TPriorExponential<double>>();
        priorVar2->initialize("5");

        // z
        priorZ = std::make_unique<TPriorUniform<uint8_t>>();
        priorZ->initialize("");
    }

    void initializeDefinitions(std::vector<std::shared_ptr<TMCMCParameterBase> > & params){
        // mean0
        mean0 = std::make_shared<TMCMCParameter<double>>(std::move(priorMean0), defMean0, &randomGenerator);
        // mean1
        mean1 = std::make_shared<TMCMCParameter<double>>(std::move(priorMean1), defMean1, &randomGenerator);
        // mean2
        mean2 = std::make_shared<TMCMCParameter<double>>(std::move(priorMean2), defMean2, &randomGenerator);
        // var0
        var0 = std::make_shared<TMCMCParameter<double>>(std::move(priorVar0), defVar0, &randomGenerator);
        // var1
        var1 = std::make_shared<TMCMCParameter<double>>(std::move(priorVar1), defVar1, &randomGenerator);
        // var2
        var2 = std::make_shared<TMCMCParameter<double>>(std::move(priorVar2), defVar2, &randomGenerator);
        // z
        defZ->setDimensions({10});
        z = std::make_shared<TMCMCParameter<uint8_t>>(std::move(priorZ), defZ, &randomGenerator);

        params = {mean0, mean1, mean2, var0, var1, var2, z};

        // parameter itself
        def->setDimensions({10});
        param = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
        dimNames = std::make_shared<TNamesIndices>();
        param->initializeStorageBasedOnPrior({10}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)
    }

    void TearDown() override {}
};

TEST_F(TPriorNormalMixedModelWithHyperPriorTest, initParameter_wrongDimensions){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    // this is ok, dimensions match!
    param->handPointerToPrior(param);
    param->initializeStorage();

    // this is also ok, as total size still matches
    auto otherDef = std::make_shared<TParameterDefinition>("otherDefParam");
    otherDef->setDimensions({2, 5});
    std::shared_ptr<BridgeTMCMCArrayParameter<double>> otherParam;
    otherParam = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, otherDef, &randomGenerator);
    otherParam->handPointerToPrior(otherParam);
    otherParam->initializeStorageBasedOnPrior({2,5}, {dimNames, dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    // this throws - wrong size of parameter compared to z
    auto otherDef2 = std::make_shared<TParameterDefinition>("otherDef2Param");
    otherDef2->setDimensions({11});
    std::shared_ptr<BridgeTMCMCArrayParameter<double>> otherParam2;
    otherParam2 = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, otherDef2, &randomGenerator);
    otherParam2->handPointerToPrior(otherParam2);
    otherParam2->initializeStorageBasedOnPrior({11}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)
    EXPECT_THROW(otherParam2->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorNormalMixedModelWithHyperPriorTest, initPriorWithHyperPrior_notEnoughParams){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);
    EXPECT_NO_THROW(priorWithHyperPrior->initPriorWithHyperPrior(params, observations));

    // remove some
    std::vector<std::shared_ptr<TMCMCParameterBase> > params_tooFew;
    params_tooFew.push_back(params[0]); params_tooFew.push_back(params[1]);

    // now initialize priorWithHyperPrior - throws because only 2 instead of minimal 3 parameters were given
    EXPECT_THROW(priorWithHyperPrior->initPriorWithHyperPrior(params_tooFew, observations), std::runtime_error);
}

TEST_F(TPriorNormalMixedModelWithHyperPriorTest, initPriorWithHyperPrior_NotTheSameNumberOfMuAndVar){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);
    EXPECT_NO_THROW(priorWithHyperPrior->initPriorWithHyperPrior(params, observations));

    // add one more
    params.push_back(mean0);

    // now initialize priorWithHyperPrior - throws because 8 instead of 7 parameters were given
    EXPECT_THROW(priorWithHyperPrior->initPriorWithHyperPrior(params, observations), std::runtime_error);
}

TEST_F(TPriorNormalMixedModelWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_mean0) {
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace mean0 by another mean0 with wrong dimensions
    defMean0->setDimensions({2});
    mean0 = std::make_shared<TMCMCParameter<double>>(std::move(priorMean0), defMean0, &randomGenerator);
    params[0] = mean0;
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorNormalMixedModelWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_mean1) {
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace mean1 by another mean1 with wrong dimensions
    defMean1->setDimensions({2});
    mean1 = std::make_shared<TMCMCParameter<double>>(std::move(priorMean1), defMean1, &randomGenerator);
    params[1] = mean1;
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorNormalMixedModelWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_mean2) {
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace mean1 by another mean1 with wrong dimensions
    defMean2->setDimensions({2});
    mean2 = std::make_shared<TMCMCParameter<double>>(std::move(priorMean2), defMean2, &randomGenerator);
    params[2] = mean2;
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorNormalMixedModelWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_var0) {
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace var0 by another var0 with wrong dimensions
    defVar0->setDimensions({2});
    var0 = std::make_shared<TMCMCParameter<double>>(std::move(priorVar0), defVar0, &randomGenerator);
    params[3] = var0;
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorNormalMixedModelWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_var1) {
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace var1 by another var1 with wrong dimensions
    defVar1->setDimensions({2});
    var1 = std::make_shared<TMCMCParameter<double>>(std::move(priorVar1), defVar1, &randomGenerator);
    params[4] = var1;
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorNormalMixedModelWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_var2) {
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace var2 by another var2 with wrong dimensions
    defVar2->setDimensions({2});
    var2 = std::make_shared<TMCMCParameter<double>>(std::move(priorVar2), defVar2, &randomGenerator);
    params[5] = var2;
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorNormalMixedModelWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_z) {
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace z by another z with wrong dimensions
    defZ->setDimensions({2, 1});
    z = std::make_shared<TMCMCParameter<uint8_t>>(std::move(priorZ), defZ, &randomGenerator);
    params[6] = z;
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorNormalMixedModelWithHyperPriorTest, _getLogPriorDensity_vec){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // we want prior density of first element (z = 1)
    size_t linearIndex = param->getIndex({0});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(param, linearIndex), -1.104861579);

    // we want prior density of 4th element (z = 2)
    linearIndex = param->getIndex({3});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(param, linearIndex), -1.097953674);

    // we want prior density of last element (z = 0)
    linearIndex = param->getIndex({9});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(param, linearIndex),  -0.7901219035);
}

TEST_F(TPriorNormalMixedModelWithHyperPriorTest, _getLogPriorDensityOld_vec){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // we want prior density of first element
    size_t linearIndex = param->getIndex({0});
    double val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.8);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityOld(param, linearIndex), -1.104861579);

    // reset
    param->set(linearIndex, val);

    // we want prior density of 4th element
    linearIndex = param->getIndex({3});
    val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.5);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityOld(param, linearIndex), -1.097953674);

    // reset
    param->set(linearIndex, val);

    // we want prior density of last element
    linearIndex = param->getIndex({9});
    param->set(linearIndex, -0.5274);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityOld(param, linearIndex),  -0.7901219035);
}

TEST_F(TPriorNormalMixedModelWithHyperPriorTest, _getLogPriorRatio_vec){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // we want prior ratio of first element
    size_t linearIndex = param->getIndex({0});
    double val = param->value<double>(linearIndex);
    param->set(linearIndex, -1.1);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, linearIndex),  -0.5681642648);

    // reset
    param->set(linearIndex, val);

    // we want prior ratio of 4th element
    linearIndex = param->getIndex({3});
    val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.125);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, linearIndex), -0.3282573825);

    // reset
    param->set(linearIndex, val);

    // we want prior ratio of last element
    linearIndex = param->getIndex({9});
    param->set(linearIndex, 0.48);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, linearIndex), -2.174713316);
}

TEST_F(TPriorNormalMixedModelWithHyperPriorTest, _calcLLUpdateMean0){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // change mean0
    mean0->set(-0.8);

    // calculate LL ratio
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMean(0), 0.4287206);
    // calculate logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMean(0), 0.4287206 - 0.1237778);
}

TEST_F(TPriorNormalMixedModelWithHyperPriorTest, _calcLLUpdateMean1){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // change mean1
    mean1->set(0.5);

    // calculate LL ratio
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMean(1), 0.4313570748);
    // calculate logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMean(1), 0.4313570748 - 0.1081375647);
}

TEST_F(TPriorNormalMixedModelWithHyperPriorTest, _calcLLUpdateMean2){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // change mean1
    mean2->set(0.26);

    // calculate LL ratio
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMean(2), 0.4244714224);
    // calculate logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMean(2), 0.4244714224 + 0.3153376);
}

TEST_F(TPriorNormalMixedModelWithHyperPriorTest, _calcLLUpdateVar0){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // change mean1
    var0->set(0.5);

    // calculate LL ratio
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateVar(0), 0.6037196);
    // calculate logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateVar(0), 0.6037196 - 2.819657);
}

TEST_F(TPriorNormalMixedModelWithHyperPriorTest, _calcLLUpdateVar1){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // change var1
    var1->set(0.77);

    // calculate LL ratio
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateVar(1), 0.7332222424);
    // calculate logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateVar(1), 0.7332222424 + 1.354968537);
}

TEST_F(TPriorNormalMixedModelWithHyperPriorTest, _calcLLUpdateVar2){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // change var2
    var2->set(0.063);

    // calculate LL ratio
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateVar(2), -4.099128602);
    // calculate logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateVar(2), -4.099128602 + 2.758905134);
}

TEST_F(TPriorNormalMixedModelWithHyperPriorTest, _calcLLUpdateZ){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // change n=0 of z (1->0)
    size_t n = 0;
    z->set(n, 0);

    // calculate LL ratio and logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateZ(n), -0.363309447976558);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateZ(n), -0.363309447976558 + 0.);

    // reset previous change
    z->set(n, 1);

    // change n=0 of z (1->2)
    z->set(n, 2);

    // calculate LL ratio and logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateZ(n), -0.3285278266);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateZ(n), -0.3285278266 + 0.);

    // reset previous change
    z->set(n, 1);

    // change n=3 of z (2->0)
    n = 3;
    z->set(n, 0);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateZ(n),  0.3408761754);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateZ(n),  0.3408761754 + 0.);

    // reset previous change
    z->set(n, 2);

    // change n=3 of z (2->1)
    z->set(n, 1);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateZ(n), -0.0367221732994836);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateZ(n), -0.0367221732994836 + 0.);

    // reset previous change
    z->set(n, 2);

    // change n=9 of z (0->1)
    n = 9;
    z->set(n, 1);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateZ(n), -0.3417520836);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateZ(n), -0.3417520836 + 0.);

    // reset previous change
    z->set(n, 0);

    // change n=9 of z (0->2)
    n = 9;
    z->set(n, 2);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateZ(n), -0.3242805095);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateZ(n), -0.3242805095 + 0.);
}

TEST_F(TPriorNormalMixedModelWithHyperPriorTest, getLogPriorDensityFull){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // calculate sum of all log prior densities
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityFull(param), -19.17229034);
}

TEST_F(TPriorNormalMixedModelWithHyperPriorTest, _fillMeanParameterValues){
    // first construct hyperpriors and first parameter
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // create a second parameter
    std::shared_ptr<BridgeTMCMCArrayParameter<double>> param2;
    std::shared_ptr<TParameterDefinition> def2;
    def2 = std::make_shared<TParameterDefinition>("defParam");
    def2->setDimensions({10});
    def2->setInitVal("0.727593017742038,0.540824712254107,0.233559724874794,0.685175339691341,0.740851946175098,1.68942432384938,0.307546445168555,-1.55074018798769,-0.820806634612381,-1.3951880345121");
    param2 = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def2, &randomGenerator);
    param2->handPointerToPrior(param2);
    param2->initializeStorageBasedOnPrior({10}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)
    param2->initializeStorage();

    // calculate mean
    std::vector<double> meanParam;
    priorWithHyperPrior->_fillMeanParameterValues(meanParam);

    EXPECT_EQ(meanParam.size(), 10);
    std::vector<double> expected = {0.428588034870646,0.352494648869928,0.776372994519997,0.285049129431228,-0.0878424194127863,1.43054534831715,0.0425875270548049,-1.6056645819445,0.0386839618136333,-0.748182488507605};

    for (int i = 0; i < 10; i++){
        EXPECT_FLOAT_EQ(meanParam[i], expected[i]);
    }
}

TEST_F(TPriorNormalMixedModelWithHyperPriorTest, _findMostDistantPoints){
    // simulations and true values based on R script 'simulateHMM.R'

    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setPriors();
    initializeDefinitions(params);

    // re-set z and param to have more data points (clearer pattern)
    size_t K = 3;
    defZ->setDimensions({35});
    defZ->setType("uint8_t");
    z = std::make_shared<TMCMCParameter<uint8_t>>(std::move(priorZ), defZ, &randomGenerator);
    params[6] = z;
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    def->setDimensions({35});
    def->setInitVal("-8.02583477180451,18.887226106599,0.898389684967697,0.0617862704675645,-7.60173669829965,-6.69050672557205,18.8492070999928,-9.25512959435582,16.9119397853501,16.9305704627186,15.882783762645,18.2583688304294,16.7017449834384,0.660797792486846,16.3361033436377,15.6277754798066,0.944675268605351,-8.07167953811586,15.0669516657945,16.0607126064133,16.9205185910687,0.37212389963679,18.4351142332889,19.3484542286023,18.5880925413221,16.0298728744965,0.2655086631421,0.572853363351896,17.4884962104261,19.9595304741524,0.908207789994776,19.6735261555295,16.9001758971717,0.62911404389888,0.201681931037456");
    param = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
    param->handPointerToPrior(param);
    param->initializeStorageBasedOnPrior({35}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)
    param->initializeStorage();

    // calculate mean
    std::vector<double> meanParam;
    priorWithHyperPrior->_fillMeanParameterValues(meanParam);

    // find points that are most far apart
    std::vector<double> minValues;
    priorWithHyperPrior->_findMostDistantPoints(minValues, meanParam);

    EXPECT_EQ(minValues.size(), K);
    std::vector<double> expected = {-8.0258348, 19.9595305,  0.9446753};
    for (size_t k = 0; k < K; k++){
        EXPECT_FLOAT_EQ(minValues[k], expected[k]);
    }

    // set initial z
    priorWithHyperPrior->_setInitialZ(minValues, meanParam);
    std::vector<size_t > expectedZ = {0, 1, 2, 2, 0, 0, 1, 0, 1, 1, 1, 1, 1, 2, 1, 1, 2, 0, 1, 1, 1, 2, 1, 1, 1, 1, 2, 2, 1, 1, 2, 1, 1, 2, 2};
    for (size_t i = 0; i < z->totalSize(); i++){
        EXPECT_EQ(z->value<size_t>(i), expectedZ[i]);
    }
}

TEST_F(TPriorNormalMixedModelWithHyperPriorTest, _setInitialMeanAndVar) {
    // simulations and true values based on R script 'simulateHMM.R'

    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setPriors();
    initializeDefinitions(params);

    // re-set z to have more data points (clearer pattern)
    size_t K = 3;
    defZ->setDimensions({35});
    defZ->setType("uint8_t");
    defZ->setInitVal("0,1,2,2,0,0,1,0,1,1,1,1,1,2,1,1,2,0,1,1,1,2,1,1,1,1,2,2,1,1,2,1,1,2,2");
    z = std::make_shared<TMCMCParameter<uint8_t>>(std::move(priorZ), defZ, &randomGenerator);
    params[6] = z;
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    def->setDimensions({35});
    def->setInitVal("-8.02583477180451,18.887226106599,0.898389684967697,0.0617862704675645,-7.60173669829965,-6.69050672557205,18.8492070999928,-9.25512959435582,16.9119397853501,16.9305704627186,15.882783762645,18.2583688304294,16.7017449834384,0.660797792486846,16.3361033436377,15.6277754798066,0.944675268605351,-8.07167953811586,15.0669516657945,16.0607126064133,16.9205185910687,0.37212389963679,18.4351142332889,19.3484542286023,18.5880925413221,16.0298728744965,0.2655086631421,0.572853363351896,17.4884962104261,19.9595304741524,0.908207789994776,19.6735261555295,16.9001758971717,0.62911404389888,0.201681931037456");
    param = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
    param->handPointerToPrior(param);
    param->initializeStorageBasedOnPrior({35}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)
    param->initializeStorage();

    // calculate mean and variance based on z and param
    priorWithHyperPrior->_setInitialMeanAndVar();

    std::vector<double> expectedMeans = {-7.928977, 17.44286, 0.5515139};
    std::vector<double> expectedVars = {0.6858642, 2.006703, 0.08967447};

    EXPECT_FLOAT_EQ(mean0->value<double>(), expectedMeans[0]);
    EXPECT_FLOAT_EQ(mean1->value<double>(), expectedMeans[1]);
    EXPECT_FLOAT_EQ(mean2->value<double>(), expectedMeans[2]);
    EXPECT_FLOAT_EQ(var0->value<double>(), expectedVars[0]);
    EXPECT_FLOAT_EQ(var1->value<double>(), expectedVars[1]);
    EXPECT_FLOAT_EQ(var2->value<double>(), expectedVars[2]);
}

//--------------------------------------------
// TPriorNormalTwoMixedModelsWithHyperPrior
class BridgeTPriorNormalTwoMixedModelsWithHyperPrior : public TPriorNormalTwoMixedModelsWithHyperPrior<double> {
public:
    BridgeTPriorNormalTwoMixedModelsWithHyperPrior() : TPriorNormalTwoMixedModelsWithHyperPrior() {};
    // update mean
    double _calcLLUpdateMean(const size_t & k) {
        return TPriorNormalTwoMixedModelsWithHyperPrior::_calcLLUpdateMean(k);
    }
    double _calcLogHUpdateMean(const size_t & k) {
        return TPriorNormalTwoMixedModelsWithHyperPrior::_calcLogHUpdateMean(k);
    }
    // update var
    double _calcLLUpdateVar(const size_t & k) {
        return TPriorNormalTwoMixedModelsWithHyperPrior::_calcLLUpdateVar(k);
    }
    double _calcLogHUpdateVar(const size_t & k) {
        return TPriorNormalTwoMixedModelsWithHyperPrior::_calcLogHUpdateVar(k);
    }
    // update z
    double _calcLLUpdateZ(const size_t & i){
        return TPriorNormalTwoMixedModelsWithHyperPrior::_calcLLUpdateZ(i);
    }
    double  _calcLogHUpdateZ(const size_t & i){
        return TPriorNormalTwoMixedModelsWithHyperPrior::_calcLogHUpdateZ(i);
    }
    void _setInitialVar(){
        TPriorNormalTwoMixedModelsWithHyperPrior::_setInitialVar();
    }
    double getVariance1(){
        return _variance1;
    }
};

class TPriorNormalTwoMixedModelsWithHyperPriorTest : public Test {
protected:
    std::shared_ptr<BridgeTPriorNormalTwoMixedModelsWithHyperPrior> priorWithHyperPrior;

    std::shared_ptr<TParameterDefinition> defMean;
    std::shared_ptr<TParameterDefinition> defVar0;
    std::shared_ptr<TParameterDefinition> defVar1;
    std::shared_ptr<TParameterDefinition> defZ;

    std::unique_ptr<TPrior<double>> priorMean;
    std::unique_ptr<TPrior<double>> priorVar0;
    std::unique_ptr<TPrior<double>> priorVar1;
    std::unique_ptr<TPrior<uint8_t>> priorZ;

    std::shared_ptr<TMCMCParameter<double>> mean;
    std::shared_ptr<TMCMCParameter<double>> var0;
    std::shared_ptr<TMCMCParameter<double>> var1;
    std::shared_ptr<TMCMCParameter<uint8_t>> z;

    std::shared_ptr<BridgeTMCMCArrayParameter<double>> param; // parameter on which prior is defined
    std::shared_ptr<TParameterDefinition> def;
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();

    TRandomGenerator randomGenerator;
    std::vector<std::shared_ptr<TObservationsBase>> observations = {};

    void SetUp() override {
        defMean = std::make_shared<TParameterDefinition>("defMean");
        defVar0 = std::make_shared<TParameterDefinition>("defVar0");
        defVar1 = std::make_shared<TParameterDefinition>("defVar1");
        defZ = std::make_shared<TParameterDefinition>("defZ");

        // initialize param
        priorWithHyperPrior = std::make_shared<BridgeTPriorNormalTwoMixedModelsWithHyperPrior>();
        def = std::make_shared<TParameterDefinition>("defParam");
    }

    void setInitialValues(){
        // values were generated in R with in script normal_mixedModel.R
        defMean->setInitVal("-0.626453810742332");
        defVar0->setInitVal("0.0145706726703793");
        defVar1->setInitVal("0.0698976309342489");

        defZ->setType("uint8_t");
        defZ->setInitVal("1,0,0,0,0,0,0,0,1,0");

        def->setInitVal("-0.631159256727352,-0.556951795740949,-0.663316950203929,-0.443968155125421,-0.579396207387438,-0.701443165684603,-0.893788111063342,-0.490664477996881,-0.352142622533502,-0.631877703612212");
    }

    void setPriors(){
        // mean
        priorMean = std::make_unique<TPriorNormal<double>>();
        priorMean->initialize("0,1");

        // var0
        priorVar0 = std::make_unique<TPriorExponential<double>>();
        priorVar0->initialize("10");

        // var1
        priorVar1 = std::make_unique<TPriorExponential<double>>();
        priorVar1->initialize("2");

        // z
        auto priorPiOnZ = std::make_unique<TPriorUniform<double>>();
        auto defPiOnZ = std::make_shared<TParameterDefinition>("defPiOnZ");
        defPiOnZ->setInitVal("0.3"); defPiOnZ->update(false);
        auto piOnZ = std::make_shared<TMCMCParameter<double>>(std::move(priorPiOnZ), defPiOnZ, &randomGenerator);
        std::vector<std::shared_ptr<TMCMCParameterBase> > params = {piOnZ};
        priorZ = std::make_unique<TPriorBernouilliWithHyperPrior<uint8_t>>();
        priorZ->initPriorWithHyperPrior(params, observations);
    }

    void initializeDefinitions(std::vector<std::shared_ptr<TMCMCParameterBase> > & params){
        // mean
        mean = std::make_shared<TMCMCParameter<double>>(std::move(priorMean), defMean, &randomGenerator);
        // var0
        var0 = std::make_shared<TMCMCParameter<double>>(std::move(priorVar0), defVar0, &randomGenerator);
        // var1
        var1 = std::make_shared<TMCMCParameter<double>>(std::move(priorVar1), defVar1, &randomGenerator);
        // z
        defZ->setDimensions({10});
        z = std::make_shared<TMCMCParameter<uint8_t>>(std::move(priorZ), defZ, &randomGenerator);

        params = {mean, var0, var1, z};

        // parameter itself
        def->setDimensions({10});
        param = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
        dimNames = std::make_shared<TNamesIndices>();
        param->initializeStorageBasedOnPrior({10}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)
    }

    void TearDown() override {}
};

TEST_F(TPriorNormalTwoMixedModelsWithHyperPriorTest, initParameter_wrongDimensions){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    // this is ok, dimensions match!
    param->handPointerToPrior(param);
    param->initializeStorage();

    // this is also ok, as total size still matches
    auto otherDef = std::make_shared<TParameterDefinition>("otherDefParam");
    otherDef->setDimensions({2, 5});
    std::shared_ptr<BridgeTMCMCArrayParameter<double>> otherParam;
    otherParam = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, otherDef, &randomGenerator);
    otherParam->handPointerToPrior(otherParam);
    otherParam->initializeStorageBasedOnPrior({2,5}, {dimNames, dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)
    otherParam->initializeStorage();

    // this throws - wrong size of parameter compared to z
    auto otherDef2 = std::make_shared<TParameterDefinition>("otherDef2Param");
    otherDef2->setDimensions({11});
    std::shared_ptr<BridgeTMCMCArrayParameter<double>> otherParam2;
    otherParam2 = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, otherDef2, &randomGenerator);
    otherParam2->handPointerToPrior(otherParam2);
    otherParam2->initializeStorageBasedOnPrior({11}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)
    EXPECT_THROW(otherParam2->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorNormalTwoMixedModelsWithHyperPriorTest, initPriorWithHyperPrior_wrongNumParams){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);
    EXPECT_NO_THROW(priorWithHyperPrior->initPriorWithHyperPrior(params, observations));

    // add one more
    params.push_back(mean);

    // now initialize priorWithHyperPrior - throws because 5 instead of 4 parameters were given
    EXPECT_THROW(priorWithHyperPrior->initPriorWithHyperPrior(params, observations), std::runtime_error);
}

TEST_F(TPriorNormalTwoMixedModelsWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_mean) {
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace mean by another mean with wrong dimensions
    defMean->setDimensions({2});
    mean = std::make_shared<TMCMCParameter<double>>(std::move(priorMean), defMean, &randomGenerator);
    params[0] = mean;
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorNormalTwoMixedModelsWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_var0) {
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace var0 by another var0 with wrong dimensions
    defVar0->setDimensions({2});
    var0 = std::make_shared<TMCMCParameter<double>>(std::move(priorVar0), defVar0, &randomGenerator);
    params[1] = var0;
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorNormalTwoMixedModelsWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_var1) {
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace var1 by another var1 with wrong dimensions
    defVar1->setDimensions({2});
    var1 = std::make_shared<TMCMCParameter<double>>(std::move(priorVar1), defVar1, &randomGenerator);
    params[2] = var1;
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorNormalTwoMixedModelsWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_z) {
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace z by another z with wrong dimensions
    defZ->setDimensions({2, 1});
    z = std::make_shared<TMCMCParameter<uint8_t>>(std::move(priorZ), defZ, &randomGenerator);
    params[3] = z;
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorNormalTwoMixedModelsWithHyperPriorTest, _getLogPriorDensity_vec){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // we want prior density of first element
    size_t linearIndex = param->getIndex({0});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(param, linearIndex), 0.3166198646);

    // we want prior density of 5th element
    linearIndex = param->getIndex({4});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(param, linearIndex), 1.119444838);

    // we want prior density of last element
    linearIndex = param->getIndex({9});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(param, linearIndex), 1.194424198);
}

TEST_F(TPriorNormalTwoMixedModelsWithHyperPriorTest, _getLogPriorDensityOld_vec){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // we want prior density of first element
    size_t linearIndex = param->getIndex({0});
    double val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.8);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityOld(param, linearIndex), 0.3166198646);

    // reset
    param->set(linearIndex, val);

    // we want prior density of 5th element
    linearIndex = param->getIndex({4});
    val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.5);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityOld(param, linearIndex), 1.119444838);

    // reset
    param->set(linearIndex, val);

    // we want prior density of last element
    linearIndex = param->getIndex({9});
    param->set(linearIndex, -0.5274);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityOld(param, linearIndex), 1.194424198);
}

TEST_F(TPriorNormalTwoMixedModelsWithHyperPriorTest, _getLogPriorRatio_vec){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // we want prior ratio of first element
    size_t linearIndex = param->getIndex({0});
    double val = param->value<double>(linearIndex);
    param->set(linearIndex, -1.1);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, linearIndex),  -1.32726622);

    // reset
    param->set(linearIndex, val);

    // we want prior ratio of 5th element
    linearIndex = param->getIndex({4});
    val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.125);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, linearIndex), -19.30138794);

    // reset
    param->set(linearIndex, val);

    // we want prior ratio of last element
    linearIndex = param->getIndex({9});
    param->set(linearIndex, 0.48);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, linearIndex), -42.0094063);
}

TEST_F(TPriorNormalTwoMixedModelsWithHyperPriorTest, _calcLLUpdateMean){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // change mean
    mean->set(-0.8);

    // calculate LL ratio
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMean(0), -9.77687898);
    // calculate logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMean(0), -9.77687898 - 0.1237778);
}

TEST_F(TPriorNormalTwoMixedModelsWithHyperPriorTest, _calcLLUpdateVar0){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // change var0
    var0->set(0.5);

    // calculate LL ratio
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateVar(0), -11.09894);
    // calculate logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateVar(0), -11.09894 - 4.854293);
}

TEST_F(TPriorNormalTwoMixedModelsWithHyperPriorTest, _calcLLUpdateVar1){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // change var1
    var1->set(0.5);

    // calculate LL ratio
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateVar(1), -1.43455);
    // calculate logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateVar(1), -1.43455 - 0.8602047);
}

TEST_F(TPriorNormalTwoMixedModelsWithHyperPriorTest, _calcLLUpdateZ){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    z->initializeStorage();

    // change n=0 of z (1->0)
    size_t n = 0;
    bool val = z->value<bool>(n);
    z->set(n, !val);

    // calculate LL ratio and logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateZ(n), 0.8780540608);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateZ(n), 0.8780540608 + 0.8472979);

    // reset previous change
    z->set(n, val);

    // change n=4 of z (0->1)
    n = 4;
    val = z->value<bool>(n);
    z->set(n, !val);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateZ(n),  -0.8158018934);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateZ(n), -0.8158018934 - 0.8472979);

    // reset previous change
    z->set(n, val);

    // change n=9 of z (0->1)
    n = 9;
    val = z->value<bool>(n);
    z->set(n, !val);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateZ(n), -0.8778474111);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateZ(n), -0.8778474111 - 0.8472979);
}

TEST_F(TPriorNormalTwoMixedModelsWithHyperPriorTest, getLogPriorDensityFull){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    z->initializeStorage();

    // calculate sum of all log prior densities
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityFull(param), 1.374866334);
}

TEST_F(TPriorNormalTwoMixedModelsWithHyperPriorTest, estimateInitialEMParameters) {
    // simulations and true values based on R script 'simulateHMM.R'

    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setPriors();
    initializeDefinitions(params);

    // re-set z to have more data points (clearer pattern)
    size_t K = 2;
    defZ->setDimensions({50});
    defZ->setType("uint8_t");
    z = std::make_shared<TMCMCParameter<uint8_t>>(std::move(priorZ), defZ, &randomGenerator);
    params[3] = z;
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    def->setDimensions({50});
    def->setInitVal("-0.465092740724406,0.556663198673657,0.768532924515416,0.387671611559369,-0.68875569454952,0.290606161243692,1.35867955152904,-0.0593133967111857,-0.00511981073100457,0.182078050547539,1.10002537198388,0.0235795181126569,0.259692943931546,-0.0177494659105969,0.132164725907544,-0.102787727342996,-0.151204323747827,-0.0965722874379397,-0.198102089083783,0.478067181605537,-0.0492668650408876,-0.394289953710349,0.355734391167665,-0.0538050405829051,-0.629088242604682,0.36458196213683,-0.0142092547979605,-0.70749515696212,0.123279255747161,0.154138600341164,0.233478772098053,-1.37705955682861,-0.264248969322179,-0.112346212150228,0.696963375404737,-0.196453520943738,-0.700349597719884,-0.259454884197085,0.881107726454215,0.504472084229583,-0.164523596253587,0.104199506566357,0.19600611157646,0.247333215100859,-0.41499456329968,0.29846721639081,0.0580731181626549,0.187808088043061,0.763175748457544,-0.253361680136508");
    param = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
    param->handPointerToPrior(param);
    param->initializeStorageBasedOnPrior({50}, {dimNames});
    param->initializeStorage();

    priorWithHyperPrior->initializeEMParameters();

    // mean
    EXPECT_FLOAT_EQ(mean->value<double>(), 0.06661872);
    // variances
    EXPECT_FLOAT_EQ(var0->value<double>(), 0.06572253);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getVariance1(), 0.8230378);
    // z
    std::vector<uint8_t > expectedZ = {0,0,1,0,1,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,0,0,1,0,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,1,0};
    for (size_t i = 0; i < 50; i++){
        EXPECT_EQ(z->value<uint8_t>(i), expectedZ[i]);
    }
}

//--------------------------------------------
// TPriorMultivariateNormalMixedModelWithHyperPrior

#ifdef WITH_ARMADILLO
class BridgeTPriorMultivariateNormalMixedModelWithHyperPrior : public TPriorMultivariateNormalMixedModelWithHyperPrior<double> {
public:
    BridgeTPriorMultivariateNormalMixedModelWithHyperPrior() : TPriorMultivariateNormalMixedModelWithHyperPrior() {};
    size_t _linearizeIndex_Mrs(const size_t & r, const size_t & s) const{
        return TPriorMultivariateNormalMixedModelWithHyperPrior::_linearizeIndex_Mrs(r, s);
    }
    // update z
    double _calcLLUpdateZ(const size_t & n){
        return TPriorMultivariateNormalMixedModelWithHyperPrior::_calcLLUpdateZ(n);
    }
    double  _calcLogHUpdateZ(const size_t & n){
        return TPriorMultivariateNormalMixedModelWithHyperPrior::_calcLogHUpdateZ(n);
    }
    // update mus
    double _calcLLUpdateMu(const size_t &k, size_t r){
        return TPriorMultivariateNormalMixedModelWithHyperPrior::_calcLLUpdateMu(k, r);
    }
    double _calcLogHUpdateMu(const size_t &k, size_t r){
        return TPriorMultivariateNormalMixedModelWithHyperPrior::_calcLogHUpdateMu(k, r);
    }
    // update m
    virtual double _calcLLUpdateM(const size_t &k){
        return TPriorMultivariateNormalMixedModelWithHyperPrior::_calcLLUpdateM(k);
    }
    double _calcLogHUpdateM(const size_t &k){
        return TPriorMultivariateNormalMixedModelWithHyperPrior::_calcLogHUpdateM(k);
    }
    // update mrr
    double _calcLLUpdateMrr(const size_t &k, size_t r){
        return TPriorMultivariateNormalMixedModelWithHyperPrior::_calcLLUpdateMrr(k, r);
    }
    double _calcLogHUpdateMrr(const size_t &k, size_t r){
        return TPriorMultivariateNormalMixedModelWithHyperPrior::_calcLogHUpdateMrr(k, r);
    }
    // update mrs
    double _calcLLUpdateMrs(const size_t &k, size_t r, size_t s){
        return TPriorMultivariateNormalMixedModelWithHyperPrior::_calcLLUpdateMrs(k, r, s);
    }
    double _calcLogHUpdateMrs(const size_t &k, size_t r, size_t s){
        return TPriorMultivariateNormalMixedModelWithHyperPrior::_calcLogHUpdateMrs(k, r, s);
    }
    // EM
    void _fillMeanParameterValues(std::vector<std::vector<double>> & meanParamValues){
        return TPriorMultivariateNormalMixedModelWithHyperPrior::_fillMeanParameterValues(meanParamValues);
    }
    void _findMostDistantPoints(std::vector<std::vector<double>> & minValues, const std::vector<std::vector<double>> & meanParameterValues){
        return TPriorMultivariateNormalMixedModelWithHyperPrior::_findMostDistantPoints(minValues, meanParameterValues);
    }
    void _setInitialZ(std::vector<std::vector<double>> & minValues, const std::vector<std::vector<double>> & meanParameterValues){
        return TPriorMultivariateNormalMixedModelWithHyperPrior::_setInitialZ(minValues, meanParameterValues);
    }
    void _setInitialMeanAndM(){
        return TPriorMultivariateNormalMixedModelWithHyperPrior::_setInitialMeanAndM();
    }
};

class TPriorMultivariateNormalMixedModelWithHyperPriorTest : public Test {
protected:
    std::shared_ptr<BridgeTPriorMultivariateNormalMixedModelWithHyperPrior> priorWithHyperPrior;

    std::shared_ptr<TParameterDefinition> defMu0;
    std::shared_ptr<TParameterDefinition> defM0;
    std::shared_ptr<TParameterDefinition> defMrr0;
    std::shared_ptr<TParameterDefinition> defMrs0;
    std::shared_ptr<TParameterDefinition> defMu1;
    std::shared_ptr<TParameterDefinition> defM1;
    std::shared_ptr<TParameterDefinition> defMrr1;
    std::shared_ptr<TParameterDefinition> defMrs1;
    std::shared_ptr<TParameterDefinition> defMu2;
    std::shared_ptr<TParameterDefinition> defM2;
    std::shared_ptr<TParameterDefinition> defMrr2;
    std::shared_ptr<TParameterDefinition> defMrs2;
    std::shared_ptr<TParameterDefinition> defZ;
    std::shared_ptr<TPriorWithHyperPrior<double>> priorMu;
    std::shared_ptr<TPrior<double>> priorM;
    std::shared_ptr<TPrior<double>> priorMrr;
    std::shared_ptr<TPriorWithHyperPrior<double>> priorMrs;
    std::unique_ptr<TPrior<uint8_t>> priorZ;

    std::shared_ptr<TMCMCParameter<double>> mu0;
    std::shared_ptr<TMCMCParameter<double>> m0;
    std::shared_ptr<TMCMCParameter<double>> mrr0;
    std::shared_ptr<TMCMCParameter<double>> mrs0;
    std::shared_ptr<TMCMCParameter<double>> mu1;
    std::shared_ptr<TMCMCParameter<double>> m1;
    std::shared_ptr<TMCMCParameter<double>> mrr1;
    std::shared_ptr<TMCMCParameter<double>> mrs1;
    std::shared_ptr<TMCMCParameter<double>> mu2;
    std::shared_ptr<TMCMCParameter<double>> m2;
    std::shared_ptr<TMCMCParameter<double>> mrr2;
    std::shared_ptr<TMCMCParameter<double>> mrs2;
    std::shared_ptr<TMCMCParameter<uint8_t>> z;

    std::shared_ptr<BridgeTMCMCArrayParameter<double>> param; // parameter on which prior is defined
    std::shared_ptr<TParameterDefinition> def;
    std::shared_ptr<TNamesEmpty> dimNames;

    TRandomGenerator randomGenerator;
    std::vector<std::shared_ptr<TObservationsBase>> observations = {};
    TLog logfile;

    void SetUp() override {
        defMu0 = std::make_shared<TParameterDefinition>("defMu0");
        defM0 = std::make_shared<TParameterDefinition>("defM0");
        defMrr0 = std::make_shared<TParameterDefinition>("defMrr0");
        defMrs0 = std::make_shared<TParameterDefinition>("defMrs0");
        defMu1 = std::make_shared<TParameterDefinition>("defMu1");
        defM1 = std::make_shared<TParameterDefinition>("defM1");
        defMrr1 = std::make_shared<TParameterDefinition>("defMrr1");
        defMrs1 = std::make_shared<TParameterDefinition>("defMrs1");
        defMu2 = std::make_shared<TParameterDefinition>("defMu2");
        defM2 = std::make_shared<TParameterDefinition>("defM2");
        defMrr2 = std::make_shared<TParameterDefinition>("defMrr2");
        defMrs2 = std::make_shared<TParameterDefinition>("defMrs2");
        defZ = std::make_shared<TParameterDefinition>("defZ");

        // initialize param
        priorWithHyperPrior = std::make_shared<BridgeTPriorMultivariateNormalMixedModelWithHyperPrior>();
        def = std::make_shared<TParameterDefinition>("defParam");
    }

    void setInitialValues_5D(bool setInitValsOfParam = true){
        // values were generated with R in script multivariate_normal.R
        // mus
        defMu0->setInitVal("-0.820468384118015,0.487429052428485,0.738324705129217,0.575781351653492,-0.305388387156356");
        defMu1->setInitVal("0.503607972233726,1.08576936214569,-0.69095383969683,-1.28459935387219,0.046726172188352");
        defMu2->setInitVal("0.556663198673657,-0.68875569454952,-0.70749515696212,0.36458196213683,0.768532924515416");
        // m
        defM0->setInitVal("0.2");
        defM1->setInitVal("0.3");
        defM2->setInitVal("0.4");
        // mrr
        defMrr0->setInitVal("0.147045990503953,1.39073512881043,0.762029855470073,1.23760355073483,4.42393421768284");
        defMrr1->setInitVal("1.32046792931379,0.203510350031566,1.02272587731005,0.30174093414098,0.725214303429139");
        defMrr2->setInitVal("0.977395805823006,0.209866580553353,0.309447856154293,1.10593626830251,0.774187764110875");
        // mrs
        defMrs0->setInitVal("-0.411510832795067,0.252223448156132,-0.891921127284569,0.435683299355719,-1.23753842192996,-0.224267885278309,0.377395645981701,0.133336360814841,0.804189509744908,-0.0571067743838088");
        defMrs1->setInitVal("-0.0538050405829051,-1.37705955682861,-0.41499456329968,-0.394289953710349,-0.0593133967111857,1.10002537198388,0.763175748457544,-0.164523596253587,-0.253361680136508,0.696963375404737");
        defMrs2->setInitVal("2.44136462889459,-0.795339117255372,-0.0548774737115786,0.250141322854153,0.618243293566247,-0.172623502645857,-2.22390027400994,-1.26361438497058,0.358728895971352,-0.0110454784656636");
        // z
        defZ->setType("uint8_t");
        defZ->setInitVal("0,0,1,1,0,1,2,1,1,0");

        // parameter itself
        if (setInitValsOfParam) {
            def->setInitVal("-2.16457295409263,0.361439854607573,0.52964984639412,0.60952230645136,-0.35930690956568,"
                            "-0.291655279902113,0.568985644832297,0.780844638931894,0.593429066894042,-0.296432894990763,"
                            "-0.108401781156768,0.00624008810329268,-1.28252217296438,-0.494139504530673,0.0387529644667214,"
                            "-0.726678930549294,-2.28624631101354,-2.21455941123792,-0.310616200682877,0.0904273283086975,"
                            "0.272389714054252,0.378358870785744,0.605285791934401,0.493920177370878,-0.26142196592605,"
                            "1.07882766410243,4.11225197986768,-0.0232194297031681,-1.5030881319683,0.0147537423812551,"
                            "3.88909248376303,-2.2436412859183,0.0459508831181846,0.408099896811535,0.387999342401306,"
                            "1.87870819962303,3.06413042961675,0.612956137659212,-2.03701992654969,-0.0435783059986362,"
                            "-0.779213970307051,-1.0959492637063,-2.56202651072127,0.259489520694582,-0.56822803571598,"
                            "1.49430854907625,0.568464581792549,1.1116208525791,0.463288587121934,-0.364931471355877");
        }
    }

    void setInitialValues_1D(){
        // values were generated with R in script multivariate_normal.R
        // mu
        defMu0->setInitVal("-0.820468384118015");
        defMu1->setInitVal("0.503607972233726");
        defMu2->setInitVal("0.556663198673657");
        // m
        defM0->setInitVal("0.2");
        defM1->setInitVal("0.4");
        defM2->setInitVal("0.3");
        // mrr
        defMrr0->setInitVal("0.820468384118015"); // should be changed at initialization to 1
        defMrr1->setInitVal("0.747393"); // should be changed at initialization to 1
        defMrr2->setInitVal("0.0928572161"); // should be changed at initialization to 1
        // z
        defZ->setType("uint8_t");
        defZ->setInitVal("0,0,1,1,0,1,2,1,1,0");

        // parameter itself
        def->setInitVal("-2.16457295409263,-0.291655279902113,-0.108401781156768,-0.726678930549294,"
                        "0.272389714054252,1.07882766410243,3.88909248376303,1.87870819962303,-0.779213970307051,1.49430854907625");
    }

    void setPriors(size_t numDim = 5){
        // this looks horrible, as I dont' want to use MCMCManager to construct the parameters
        // as a real-life user, it won't be necessary to define all these parameters!

        std::vector<std::shared_ptr<TMCMCParameterBase> > params;
        // mu
        auto priorMuOnMu = std::make_unique<TPriorUniform<double>>();
        auto defMuOnMu = std::make_shared<TParameterDefinition>("defMuOnMu");
        defMuOnMu->setInitVal("0"); defMuOnMu->update(false);
        auto muOnMu = std::make_shared<TMCMCParameter<double>>(std::move(priorMuOnMu), defMuOnMu, &randomGenerator);

        auto priorSdOnMu = std::make_unique<TPriorUniform<double>>();
        auto defSdOnMu = std::make_shared<TParameterDefinition>("defSdOnMu");
        defSdOnMu->setInitVal("1"); defSdOnMu->update(false);
        auto sdOnMu = std::make_shared<TMCMCParameter<double>>(std::move(priorSdOnMu), defSdOnMu, &randomGenerator);
        params = {muOnMu, sdOnMu};
        priorMu = std::make_shared<TPriorNormalWithHyperPrior<double>>();
        priorMu->initPriorWithHyperPrior(params, observations);

        // m
        priorM = std::make_shared<TPriorExponential<double>>();
        priorM->initialize("5");

        // mrr0
        priorMrr = std::make_shared<TPriorMultivariateChisq<double>>();
        if (numDim == 5)
            priorMrr->initialize("5,4,3,2,1");
        else priorMrr->initialize("1");

        // mrs
        auto priorMuOnMrs = std::make_unique<TPriorUniform<double>>();
        auto defMuOnMrs = std::make_shared<TParameterDefinition>("defMuOnMrs");
        defMuOnMrs->setInitVal("0"); defMuOnMrs->update(false);
        auto muOnMrs = std::make_shared<TMCMCParameter<double>>(std::move(priorMuOnMrs), defMuOnMrs, &randomGenerator);

        auto priorSdOnMrs = std::make_unique<TPriorUniform<double>>();
        auto defSdOnMrs = std::make_shared<TParameterDefinition>("defSdOnMrs");
        defSdOnMrs->setInitVal("1"); defSdOnMrs->update(false);
        auto sdOnMrs = std::make_shared<TMCMCParameter<double>>(std::move(priorSdOnMrs), defSdOnMrs, &randomGenerator);
        params = {muOnMrs, sdOnMrs};
        priorMrs = std::make_shared<TPriorNormalWithHyperPrior<double>>();
        priorMrs->initPriorWithHyperPrior(params, observations);

        // z
        priorZ = std::make_unique<TPriorUniform<uint8_t>>();
        priorZ->initialize("");
    }

    void initializeDefinitions(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, size_t numDim = 5){
        // mu
        defMu0->setDimensions({numDim});
        mu0 = std::make_shared<TMCMCParameter<double>>(priorMu, defMu0, &randomGenerator);
        defMu1->setDimensions({numDim});
        mu1 = std::make_shared<TMCMCParameter<double>>(priorMu, defMu1, &randomGenerator);
        defMu2->setDimensions({numDim});
        mu2 = std::make_shared<TMCMCParameter<double>>(priorMu, defMu2, &randomGenerator);
        // m
        m0 = std::make_shared<TMCMCParameter<double>>(priorM, defM0, &randomGenerator);
        m1 = std::make_shared<TMCMCParameter<double>>(priorM, defM1, &randomGenerator);
        m2 = std::make_shared<TMCMCParameter<double>>(priorM, defM2, &randomGenerator);
        // mrr
        defMrr0->setDimensions({numDim});
        mrr0 = std::make_shared<TMCMCParameter<double>>(priorMrr, defMrr0, &randomGenerator);
        defMrr1->setDimensions({numDim});
        mrr1 = std::make_shared<TMCMCParameter<double>>(priorMrr, defMrr1, &randomGenerator);
        defMrr2->setDimensions({numDim});
        mrr2 = std::make_shared<TMCMCParameter<double>>(priorMrr, defMrr2, &randomGenerator);
        // mrs0
        defMrs0->setDimensions({static_cast<size_t>((numDim - 1.) * numDim / 2.)});
        mrs0 = std::make_shared<TMCMCParameter<double>>(priorMrs, defMrs0, &randomGenerator);
        defMrs1->setDimensions({static_cast<size_t>((numDim - 1.) * numDim / 2.)});
        mrs1 = std::make_shared<TMCMCParameter<double>>(priorMrs, defMrs1, &randomGenerator);
        defMrs2->setDimensions({static_cast<size_t>((numDim - 1.) * numDim / 2.)});
        mrs2 = std::make_shared<TMCMCParameter<double>>(priorMrs, defMrs2, &randomGenerator);
        // z
        defZ->setDimensions({10});
        z = std::make_shared<TMCMCParameter<uint8_t>>(std::move(priorZ), defZ, &randomGenerator);

        params = {mu0, mu1, mu2, m0, m1, m2, mrr0, mrr1, mrr2, mrs0, mrs1, mrs2, z};

        // parameter itself
        def->setDimensions({10, numDim});
        param = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
        dimNames = std::make_shared<TNamesIndices>();
        param->initializeStorageBasedOnPrior({10, numDim}, {dimNames, dimNames});
    }

    void TearDown() override {}
};

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, initParameter_wrongDimensions){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    // this is ok, dimensions match!
    param->handPointerToPrior(param);
    param->initializeStorage();

    // this should throw, wrong number of columns
    auto otherDef = std::make_shared<TParameterDefinition>("otherDefParam");
    otherDef->setDimensions({10, 9});
    std::shared_ptr<BridgeTMCMCArrayParameter<double>> otherParam;
    otherParam = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, otherDef, &randomGenerator);
    otherParam->handPointerToPrior(otherParam);
    otherParam->initializeStorageBasedOnPrior({10, 9}, {dimNames, dimNames});
    EXPECT_THROW(otherParam->initializeStorage(), std::runtime_error);

    // this should also throw, rows - cols switched
    auto otherDef2 = std::make_shared<TParameterDefinition>("otherDef2Param");
    otherDef2->setDimensions({5, 10});
    std::shared_ptr<BridgeTMCMCArrayParameter<double>> otherParam2;
    otherParam2 = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, otherDef2, &randomGenerator);
    otherParam2->handPointerToPrior(otherParam);
    otherParam2->initializeStorageBasedOnPrior({5, 10}, {dimNames, dimNames});
    EXPECT_THROW(otherParam2->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, initParameter_wrongDimensionsOfZ){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace z by another z with wrong dimensions that don't match the number of rows of the parameter
    defZ->setDimensions({5});
    z = std::make_shared<TMCMCParameter<uint8_t>>(std::move(priorZ), defZ, &randomGenerator);
    params[12] = z;

    // this is ok, we don't know dimensions of parameter yet
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);

    // this throws, as size of z and number of rows of parameter don't match
    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, initPriorWithHyperPrior_notEnoughParams){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);
    EXPECT_NO_THROW(priorWithHyperPrior->initPriorWithHyperPrior(params, observations));

    // remove some
    std::vector<std::shared_ptr<TMCMCParameterBase> > params_tooFew;
    params_tooFew.push_back(params[0]); params_tooFew.push_back(params[1]); params_tooFew.push_back(params[2]); params_tooFew.push_back(params[3]);

    // now initialize priorWithHyperPrior - throws because only 4 instead of minimal 5 parameters were given
    EXPECT_THROW(priorWithHyperPrior->initPriorWithHyperPrior(params_tooFew, observations), std::runtime_error);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, initPriorWithHyperPrior_wrongNumParams){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);
    EXPECT_NO_THROW(priorWithHyperPrior->initPriorWithHyperPrior(params, observations));

    // add one more
    params.push_back(m0);

    // now initialize priorWithHyperPrior - throws because not divisible by 4 anymore
    EXPECT_THROW(priorWithHyperPrior->initPriorWithHyperPrior(params, observations), std::runtime_error);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_mu0){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace mu by another mu with wrong dimensions
    defMu0->setDimensions({2, 5});
    mu0 = std::make_shared<TMCMCParameter<double>>(std::move(priorMu), defMu0, &randomGenerator);
    params[0] = mu0;
    param->handPointerToPrior(param);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_mu1){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace mu by another mu with wrong dimensions
    defMu1->setDimensions({6});
    mu1 = std::make_shared<TMCMCParameter<double>>(std::move(priorMu), defMu1, &randomGenerator);
    params[1] = mu1;
    param->handPointerToPrior(param);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_mu2){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace mu by another mu with wrong dimensions
    defMu2->setDimensions({6});
    mu2 = std::make_shared<TMCMCParameter<double>>(std::move(priorMu), defMu2, &randomGenerator);
    params[2] = mu2;
    param->handPointerToPrior(param);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_m0){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace m by another m with wrong dimensions
    defM0->setDimensions({2});
    m0 = std::make_shared<TMCMCParameter<double>>(std::move(priorM), defM0, &randomGenerator);
    params[3] = m0;
    param->handPointerToPrior(param);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_m1){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace m by another m with wrong dimensions
    defM1->setDimensions({2});
    m1 = std::make_shared<TMCMCParameter<double>>(std::move(priorM), defM1, &randomGenerator);
    params[4] = m1;
    param->handPointerToPrior(param);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_m2){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace m by another m with wrong dimensions
    defM2->setDimensions({2});
    m2 = std::make_shared<TMCMCParameter<double>>(std::move(priorM), defM2, &randomGenerator);
    params[5] = m2;
    param->handPointerToPrior(param);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_mrr0){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace mrr by another mrr with wrong size
    defMrr0->setDimensions({6});
    mrr0 = std::make_shared<TMCMCParameter<double>>(std::move(priorMrr), defMrr0, &randomGenerator);
    params[6] = mrr0;
    param->handPointerToPrior(param);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_mrr1){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace mrr by another mrr with wrong size
    defMrr1->setDimensions({6});
    mrr1 = std::make_shared<TMCMCParameter<double>>(std::move(priorMrr), defMrr1, &randomGenerator);
    params[7] = mrr1;
    param->handPointerToPrior(param);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_mrr2){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace mrr by another mrr with wrong size
    defMrr2->setDimensions({6});
    mrr2 = std::make_shared<TMCMCParameter<double>>(std::move(priorMrr), defMrr2, &randomGenerator);
    params[8] = mrr2;
    param->handPointerToPrior(param);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_mrs0){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace mrs by another mrs with wrong size
    defMrs0->setDimensions({6});
    mrs0 = std::make_shared<TMCMCParameter<double>>(std::move(priorMrs), defMrs0, &randomGenerator);
    params[9] = mrs0;
    param->handPointerToPrior(param);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_mrs1){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace mrs by another mrs with wrong size
    defMrs1->setDimensions({6});
    mrs1 = std::make_shared<TMCMCParameter<double>>(std::move(priorMrs), defMrs1, &randomGenerator);
    params[10] = mrs1;
    param->handPointerToPrior(param);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_mrs2){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace mrs by another mrs with wrong size
    defMrs2->setDimensions({6});
    mrs2 = std::make_shared<TMCMCParameter<double>>(std::move(priorMrs), defMrs2, &randomGenerator);
    params[11] = mrs2;
    param->handPointerToPrior(param);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_z){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);
    initializeDefinitions(params);

    // replace z by another z with wrong dimensions
    defZ->setDimensions({2, 1});
    z = std::make_shared<TMCMCParameter<uint8_t>>(std::move(priorZ), defZ, &randomGenerator);
    params[12] = z;
    param->handPointerToPrior(param);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, _getLogPriorDensity_vec){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // we want prior density of first element of first row -> z = 0
    size_t linearIndex = param->getIndex({0, 0});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(param, linearIndex), 1.46775254410025);

    // we want prior density of third element of 7th row -> z = 2
    linearIndex = param->getIndex({6, 2});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(param, linearIndex), -3.53834226494083);

    // we want prior density of last element of 9th row -> z = 1
    linearIndex = param->getIndex({8, 4});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(param, linearIndex),  -3.20203842196268);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, _getLogPriorDensity_vec_1D){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_1D();
    initializeDefinitions(params, 1);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // we want prior density of first element (z = 0)
    size_t linearIndex = param->getIndex({0, 0});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(param, linearIndex),  -21.8922143086036);

    // we want prior density of 7th element (z = 2)
    linearIndex = param->getIndex({6, 0});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(param, linearIndex), -61.4098820628858);

    // we want prior density of 9th element (z = 1)
    linearIndex = param->getIndex({8, 0});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(param, linearIndex), -5.14524822715581);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, _getLogPriorDensityOld_vec){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // we want prior density of first element of first row
    size_t linearIndex = param->getIndex({0, 0});
    double val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.8);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityOld(param, linearIndex), 1.46775254410025);

    // reset
    param->set(linearIndex, val);

    // we want prior density of third element of 7th row
    linearIndex = param->getIndex({6, 2});
    val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.5);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityOld(param, linearIndex), -3.53834226494083);

    // reset
    param->set(linearIndex, val);

    // we want prior density of last element of 9th row
    linearIndex = param->getIndex({8, 4});
    param->set(linearIndex, -0.5274);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityOld(param, linearIndex), -3.20203842196268);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, _getLogPriorDensityOld_vec_1D){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_1D();
    initializeDefinitions(params, 1);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // we want prior density of first element
    size_t linearIndex = param->getIndex({0, 0});
    double val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.8);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityOld(param, linearIndex), -21.8922143086036);

    // reset
    param->set(linearIndex, val);

    // we want prior density of 7th element
    linearIndex = param->getIndex({6, 0});
    val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.5);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityOld(param, linearIndex), -61.4098820628858);

    // reset
    param->set(linearIndex, val);

    // we want prior density of 9th element
    linearIndex = param->getIndex({8, 0});
    param->set(linearIndex, -0.5274);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityOld(param, linearIndex), -5.14524822715581);
}


TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, _getLogPriorRatio_vec){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // we want prior ratio of first element of first row
    size_t linearIndex = param->getIndex({0, 0});
    double val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.8);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, linearIndex), -0.151311651277237);

    // reset
    param->set(linearIndex, val);

    // we want prior ratio of third element of 7th row
    linearIndex = param->getIndex({6, 2});
    val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.5);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, linearIndex), -1.16191082678025);

    // reset
    param->set(linearIndex, val);

    // we want prior ratio of last element of 9th row
    linearIndex = param->getIndex({8, 4});
    param->set(linearIndex, -0.5274);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, linearIndex), 0.165073210306163);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, _getLogPriorRatio_vec_1D){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_1D();
    initializeDefinitions(params, 1);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // we want prior ratio of first element
    size_t linearIndex = param->getIndex({0, 0});
    double val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.8);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, linearIndex), -10.2412586112426);

    // reset
    param->set(linearIndex, val);

    // we want prior ratio of 7th element
    linearIndex = param->getIndex({6, 0});
    val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.5);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, linearIndex), 61.6770790113185);

    // reset
    param->set(linearIndex, val);

    // we want prior ratio of 9th element
    linearIndex = param->getIndex({8, 0});
    param->set(linearIndex, -0.5274);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, linearIndex), 1.8207959295456);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, _calcLLUpdateMrr){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // change r=0 of Mrr
    size_t r = 0;
    double val0 = mrr0->value<double>(r);
    double val1 = mrr1->value<double>(r);
    double val2 = mrr2->value<double>(r);
    mrr0->set(r, 0.62);
    mrr1->set(r, 0.65);
    mrr2->set(r, 0.79);

    // calculate LL ratio
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMrr(0, r), -33.0550168865815);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMrr(1, r), -18.4747599040689);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMrr(2, r), -2.52841403480806);
    // calculate logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMrr(0, r), -33.0550168865815 + 1.92198411468191);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMrr(1, r), -18.4747599040689 - 0.727919658009671);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMrr(2, r), -2.52841403480806 - 0.225590219401449);

    // reset previous change
    mrr0->set(r, val0);
    mrr1->set(r, val1);
    mrr2->set(r, val2);

    // change r=4 of Mrr
    r = 4;
    mrr0->set(r, 0.01);
    mrr1->set(r, 0.1);
    mrr2->set(r, 0.2);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMrr(0, r), -22.2977281660075);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMrr(1, r), -8.79057566389597);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMrr(2, r), -1.10037348686971);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMrr(0, r), -22.2977281660075 + 5.25306689917986);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMrr(1, r), -8.79057566389597 + 1.30325565977704);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMrr(2, r), -1.10037348686971 + 0.963842415511539);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, _calcLLUpdateMrs){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    mrs0->initializeStorage();
    mrs1->initializeStorage();
    mrs2->initializeStorage();

    // change r=1, s=0 of Mrs
    size_t r = 1;
    size_t s = 0;
    size_t linearIndex = priorWithHyperPrior->_linearizeIndex_Mrs(r, s);
    double val0 = mrs0->value<double>(linearIndex);
    double val1 = mrs1->value<double>(linearIndex);
    double val2 = mrs2->value<double>(linearIndex);
    mrs0->set(linearIndex, 0.62);
    mrs1->set(linearIndex, 0.77);
    mrs2->set(linearIndex, 0.88);

    // calculate LL ratio
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMrs(0, r, s), -1.60576348134709);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMrs(1, r, s), -104.501921796656);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMrs(2, r, s), -14.1543608666133);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMrs(0, r, s), -1.60576348134709 - 0.107529417246155);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMrs(1, r, s), -104.501921796656 - 0.295002508803936);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMrs(2, r, s), -14.1543608666133 + 2.59293062560881);

    // reset previous change
    mrs0->set(linearIndex, val0);
    mrs1->set(linearIndex, val1);
    mrs2->set(linearIndex, val2);

    // change r=4, s=3 of Mrs
    r = 4; s = 3;
    linearIndex = priorWithHyperPrior->_linearizeIndex_Mrs(r, s);
    mrs0->set(linearIndex, 0.01);
    mrs1->set(linearIndex, 0.87);
    mrs2->set(linearIndex, 0.2);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMrs(0, r, s), -0.00265077348486553);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMrs(1, r, s), -0.100092120820062);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMrs(2, r, s), 0.00611172924459069);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMrs(0, r, s), -0.00265077348486553 + 0.0015805918402616);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMrs(1, r, s), -0.100092120820062 - 0.135571026672218);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMrs(2, r, s), 0.00611172924459069 - 0.0199389987027323);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, _calcLLUpdateMus){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    mu0->initializeStorage();
    mu1->initializeStorage();
    mu2->initializeStorage();

    // change r=0 of mu
    size_t r = 0;
    double val0 = mu0->value<double>(r);
    double val1 = mu1->value<double>(r);
    double val2 = mu2->value<double>(r);
    mu0->set(r, -0.62);
    mu1->set(r, 0.77);
    mu2->set(r, 0.001);

    // calculate LL ratio and logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMu(0, r), 0.210483743353389);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMu(1, r), -4.00976070689726);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMu(2, r), 0.0321774047667285);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMu(0, r), 0.210483743353389 + 0.144384184668613);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMu(1, r), -4.00976070689726 - 0.169639505151317);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMu(2, r), 0.0321774047667285 + 0.154936458378794);

    // reset previous change
    mu0->set(r, val0);
    mu1->set(r, val1);
    mu2->set(r, val2);

    // change r=4 of mu
    r = 4;
    mu0->set(r, 0.1);
    mu1->set(r, 0.5);
    mu2->set(r, 0.3);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMu(0, r), -177.735773265188);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMu(1, r), -12.5581449248626);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMu(2, r), -5.72357149340797);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMu(0, r), -177.735773265188 + 0.0416310335049802);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMu(1, r), -12.5581449248626 - 0.123908332416312);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMu(2, r), -5.72357149340797 + 0.250321428032109);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, _calcLLUpdateMu_1D){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_1D();
    setPriors();
    initializeDefinitions(params, 1);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    mu0->initializeStorage();
    mu1->initializeStorage();
    mu2->initializeStorage();

    // change r=0 of mu
    size_t r = 0;
    mu0->set(r, -0.62);
    mu1->set(r, 0.88);
    mu2->set(r, 0.01);

    // calculate LL ratio and logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMu(0, r), 10.9826944903589);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMu(1, r), -4.97726408495443);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMu(2, r), -21.9015197636976);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMu(0, r), 10.9826944903589 + 0.1443842);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMu(1, r), -4.97726408495443 - 0.2603895);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMu(2, r), -21.9015197636976 + 0.154887);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, _calcLLUpdateM){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // change m
    m0->set(0.62);
    m1->set(0.44);
    m2->set(0.33);

    // calculate LL ratio
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateM(0), -16.9218632242873);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateM(1), -4.28648917144376);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateM(2), 0.674337666686083);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateM(0), -16.9218632242873 - 2.1);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateM(1), -4.28648917144376 - 0.7);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateM(2), 0.674337666686083 + 0.35);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, _calcLLUpdateM_1D){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_1D();
    setPriors();
    initializeDefinitions(params, 1);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // change m
    m0->set(0.62);
    m1->set(0.44);
    m2->set(0.33);

    // calculate LL ratio
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateM(0),   92.2225634249593);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateM(1),  2.64501463832118);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateM(2),   10.6120719938663);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateM(0),   92.2225634249593 - 2.1);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateM(1),  2.64501463832118 - 0.2);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateM(2),   10.6120719938663 -0.15);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, _calcLLUpdateZ){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // change n=0 of z (0->1)
    size_t n = 0;
    z->set(n, 1);
    // calculate LL ratio and logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateZ(n), -287.121546387208);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateZ(n), -287.121546387208 + 0.);
    // reset previous change
    z->set(n, 0);
    // change n=0 of z (0->2)
    z->set(n, 2);
    // calculate LL ratio and logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateZ(n), -23.3177341205053);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateZ(n), -23.3177341205053 + 0.);
    // reset previous change
    z->set(n, 0);

    // change n=6 of z (2->0)
    n = 6;
    z->set(n, 0);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateZ(n),  -257.216042504266);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateZ(n), -257.216042504266 - 0.);
    // reset previous change
    z->set(n, 2);
    // change n=6 of z (2->1)
    z->set(n, 1);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateZ(n),  -101.941388859767);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateZ(n), -101.941388859767 - 0.);
    // reset previous change
    z->set(n, 2);

    // change n=8 of z (1->0)
    n = 8;
    z->set(n, 0);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateZ(n), -117.50482102395);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateZ(n), -117.50482102395 + 0.);
    // reset previous change
    z->set(n, 1);
    // change n=8 of z (1->2)
    z->set(n, 2);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateZ(n), -28.9299988164634);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateZ(n), -28.9299988164634 + 0.);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, _calcLLUpdateZ_1D){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_1D();
    setPriors();
    initializeDefinitions(params, 1);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // change n=0 of z (0->1)
    size_t n = 0;
    z->set(n, 1);
    // calculate LL ratio and logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateZ(n), -0.357900541513676);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateZ(n), -0.357900541513676 + 0.);
    // reset previous change
    z->set(n, 0);
    // change n=0 of z (0->2)
    z->set(n, 2);
    // calculate LL ratio and logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateZ(n), -18.9623414153988);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateZ(n), -18.9623414153988 + 0.);
    // reset previous change
    z->set(n, 0);

    // change n=6 of z (2->0)
    n = 6;
    z->set(n, 0);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateZ(n),  -215.149163161341);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateZ(n), -215.149163161341 - 0.);
    // reset previous change
    z->set(n, 2);
    // change n=6 of z (2->1)
    z->set(n, 1);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateZ(n),  25.5900299559152);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateZ(n), 25.5900299559152 - 0.);
    // reset previous change
    z->set(n, 2);

    // change n=8 of z (1->0)
    n = 8;
    z->set(n, 0);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateZ(n), 5.81447352314915);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateZ(n), 5.81447352314915 + 0.);
    // reset previous change
    z->set(n, 1);
    // change n=8 of z (1->2)
    z->set(n, 2);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateZ(n), -4.4839831161891);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateZ(n), -4.4839831161891 + 0.);
}


TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, getLogPriorDensityFull){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    mrs0->initializeStorage();
    mrs1->initializeStorage();
    mrs2->initializeStorage();
    mu0->initializeStorage();
    mu1->initializeStorage();
    mu2->initializeStorage();

    // calculate sum of all log prior densities
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityFull(param), -101.01248675098);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, getLogPriorDensityFull_1D){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_1D();
    setPriors();
    initializeDefinitions(params, 1);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    mrs0->initializeStorage();
    mrs1->initializeStorage();
    mrs2->initializeStorage();
    mu0->initializeStorage();
    mu1->initializeStorage();
    mu2->initializeStorage();

    // calculate sum of all log prior densities
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityFull(param), -187.679013729617);
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, simulate){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D(false);
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // create mock random generator and define return values
    MockRandomGenerator mockRandomGenerator;
    EXPECT_CALL(mockRandomGenerator, getNormalRandom)
            .Times(10*5)
                    // first row
            .WillOnce(Return(-1.))
            .WillOnce(Return(-0.5))
            .WillOnce(Return(0.))
            .WillOnce(Return(0.5))
            .WillOnce(Return(1.))
                    // second row
            .WillOnce(Return(-2.))
            .WillOnce(Return(-1.5))
            .WillOnce(Return(-1.))
            .WillOnce(Return(-0.5))
            .WillOnce(Return(-0.))
                    // all other rows
            .WillRepeatedly(Return(0.1));

    TParameters parameters;
    priorWithHyperPrior->simulateUnderPrior(&mockRandomGenerator, &logfile, parameters);

    // go over storage and check if this is expected
    std::vector<double> expected = {-2.24709236596471,0.309619865623647,0.636878217101605,0.588727372613197,-0.25384821083023,-3.6737163478114,-0.0087939731732144,0.248111004431766,0.447002794905211,-0.284307159722277,0.627266852067547,1.54653607884676,-0.489252379551305,-1.43768007968525,0.132228918234897,0.627266852067547,1.54653607884676,-0.489252379551305,-1.43768007968525,0.132228918234897,-0.677805985933346,0.519270436308171,0.777201426396201,0.589953809424291,-0.302342492267151,0.627266852067547,1.54653607884676,-0.489252379551305,-1.43768007968525,0.132228918234897,1.41641288979343,-1.00202782681911,-0.464751276966915,0.409250773924664,0.738634078605361,0.627266852067547,1.54653607884676,-0.489252379551305,-1.43768007968525,0.132228918234897,0.627266852067547,1.54653607884676,-0.489252379551305,-1.43768007968525,0.132228918234897,-0.677805985933346,0.519270436308171,0.777201426396201,0.589953809424291,-0.302342492267151};
    size_t linearIndex = 0;
    for (size_t n = 0; n < 10; n++){
        for (size_t d = 0; d < 5; d++, linearIndex++) {
            EXPECT_FLOAT_EQ(param->value<double>(linearIndex), expected[linearIndex]);
        }
    }
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, _fillMeanParameterValues){
    // first construct hyperpriors and first parameter
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // create a second parameter
    std::shared_ptr<BridgeTMCMCArrayParameter<double>> param2;
    std::shared_ptr<TParameterDefinition> def2;
    def2 = std::make_shared<TParameterDefinition>("defParam");
    def2->setDimensions({10, 5});
    def2->setInitVal("0.572107254061848,0.19080298487097,0.655678969807923,0.766966716852039,-0.920544867869467,-0.743469690904021,0.445130201522261,-0.790729211177677,-0.0675785224884748,-0.303332074545324,-0.744616952724755,0.0778065952472389,-0.798433765769005,-0.899159126449376,0.0229985057376325,0.555464670527726,-0.84236484579742,-0.833747797179967,-0.433329480700195,-0.579726171679795,-0.159699272830039,-0.80774840246886,-0.154940249398351,0.753418369218707,-0.252314264886081,0.431803866289556,0.892676684074104,-0.0156213254667819,0.904606130905449,-0.484872106928378,-0.693391778506339,-0.227578979916871,0.160191596485674,-0.803356627002358,0.770603664685041,-0.82352674100548,0.93429670156911,0.1291553103365,-0.435859040822834,-0.932180262636393,-0.589946123305708,0.445400434080511,0.268847276922315,0.963484708685428,-0.226984581910074,-0.957631814293563,0.828781148418784,-0.107157536316663,0.237238423433155,-0.0795218143612146");
    param2 = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def2, &randomGenerator);
    param2->handPointerToPrior(param2);
    param2->initializeStorageBasedOnPrior({10, 5}, {dimNames, dimNames});
    param2->initializeStorage();

    // calculate mean
    std::vector<std::vector<double>> meanParam;
    priorWithHyperPrior->_fillMeanParameterValues(meanParam);

    EXPECT_EQ(meanParam.size(), 10);
    std::vector<double> expected = {-0.796232850015391,0.276121419739272,0.592664408101022,0.6882445116517,-0.639925888717573,-0.517562485403067,0.507057923177279,-0.00494228612289144,0.262925272202784,-0.299882484768043,-0.426509366940761,0.0420233416752658,-1.04047796936669,-0.696649315490025,0.030875735102177,-0.0856071300107838,-1.56430557840548,-1.52415360420894,-0.371972840691536,-0.244649421685549,0.0563452206121064,-0.214694765841558,0.225172771268025,0.623669273294793,-0.256868115406066,0.755315765195993,2.50246433197089,-0.019420377584975,-0.299241000531425,-0.235059182273562,1.59785035262835,-1.23561013291759,0.103071239801929,-0.197628365095412,0.579301503543173,0.527590729308775,1.99921356559293,0.371055723997856,-1.23643948368626,-0.487879284317515,-0.68458004680638,-0.325274414812894,-1.14658961689948,0.611487114690005,-0.397606308813027,0.268338367391343,0.698622865105667,0.502231658131219,0.350263505277544,-0.222226642858546};

    int c = 0;
    for (int i = 0; i < 10; i++){
        for (int d = 0; d < 5; d++, c++){
            EXPECT_EQ(meanParam[i].size(), 5);
            EXPECT_FLOAT_EQ(meanParam[i][d], expected[c]);
        }
    }
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, _findMostDistantPoints){
    // simulations and true values based on R script 'simulateHMM.R'

    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setPriors();
    initializeDefinitions(params);

    // re-set z and param to have more data points (clearer pattern)
    size_t K = 3;
    defZ->setDimensions({50});
    defZ->setType("uint8_t");
    z = std::make_shared<TMCMCParameter<uint8_t>>(std::move(priorZ), defZ, &randomGenerator);
    params[12] = z;
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    def->setDimensions({50, 5});
    def->setInitVal("1.91714536276596,3.0828460206628,5.61078100970474,5.74152079791429,7.3039648225257,-5.54214237953273,-6.52653214649499,-4.02167199368079,-2.27771739405969,-3.17368163188458,0.295667307425141,0.0221284733892106,-0.938626500416387,0.392262137726352,0.60036849165861,0.0917985539091022,-0.813753260213032,-0.682023897202607,0.552080686486983,0.290703532160413,4.05351567882921,3.18971650280693,2.87693394190199,5.29192141099235,5.68721342047964,-2.85616451407826,2.18392291802792,0.278246735587241,1.12506961668223,-0.151432502085334,1.04255931610422,-0.734491194840375,-0.891424766586292,-0.857470305521913,0.415972011945491,2.82112063000613,4.99559328314573,4.82229569387376,6.27473393571959,7.24794637649306,-4.02230321365538,-1.36909553958601,-0.413891554934754,2.48921826495574,0.921010232489222,3.90552809216589,3.94602330604539,4.75424042419282,4.84521439119284,5.01619475728603,-5.37838066148848,-5.27874487632671,-2.73591467082369,-3.19198688295824,-3.41850168952339,3.58378324250254,4.07028206432596,4.95347477534063,4.84140845940536,7.60258407676699,3.73841444053325,6.28798171613724,5.8673005155776,6.2025809379042,7.62353223954902,3.5135285044922,4.14696055516133,4.93769947560116,7.11961510955958,7.24621140625639,-1.35470831434632,1.89503334179611,0.551009936822662,1.74878589516192,1.04947015244577,3.57349959788138,2.7969047856018,3.95850264397015,6.26873496201223,5.25467962256364,-1.45554881527043,0.157889402027386,-1.48546741238514,1.96918454895137,0.778238833895827,-0.111299942235672,-1.56500611114445,-0.777271197344697,0.90786263410391,1.75027167348149,-3.54040478510828,-4.89684253416621,-4.48308382877741,-3.81621913231605,-1.78999605217456,-1.04991861387281,-1.36241030562427,-0.497998098950626,-0.895918217076378,0.575622790891925,3.95429277356773,6.68799111569512,6.42209579552564,5.68324702147649,5.34706716161904,-5.60677917848851,-4.41309117949462,-4.23429702939719,-2.23747310432147,-4.56379809166681,-0.190726923056152,-1.21162227867747,-2.68958517903653,-0.0670470254026386,2.29919883255821,-4.33598680827274,-5.0478381979128,-4.91744200544194,-3.06289424696695,-3.38496856155157,3.16259535297757,3.78424738461006,5.37482095804266,7.88332796196972,5.53608703843818,0.668095116963238,0.585743355850373,0.557528328618703,-1.13683281866016,0.728461970367885,-1.01722712359236,-1.04263905055956,1.9786926597401,-0.866112645231459,1.42489636147032,-0.0938508251234479,-0.0988571690435028,0.0756974415287978,1.55948916812251,1.91693803731112,4.31808070003799,3.85125099154918,3.13109565647019,4.29300342135454,7.89725805765655,-6.49347152278348,-5.59713482195183,-4.02910674788401,-1.32631508043571,-1.63021295126421,0.00154009440800351,-0.69352784002616,-0.9331768198576,0.810329445057101,2.58332171358656,4.19640746067212,3.28347368776555,4.71495713558065,5.19965393518509,5.3613794993824,0.286701362727813,-0.663649099670905,1.08522983690742,1.22983194527456,0.700824406037245,-2.48382894682663,-0.786871051313951,-0.901023769016841,-1.074017112396,0.331685068476428,5.74284789606923,2.63059940434558,5.03118449189908,4.29088587044379,4.65894905206225,2.40984129075226,3.89000878547744,4.68671886626466,4.57787023191352,5.74351939204366,-0.0707565132032982,-0.788020655264366,1.40937245611348,0.553768001220129,2.60676777835353,6.00858995573927,2.64558048717593,4.53728561751052,6.54310383332729,5.6961624786282,3.37489502208362,3.62485324545076,5.14335694280806,6.92518963325776,4.2652505739696,1.32195540765037,5.10918047095619,3.96635982281761,5.99739149641744,5.02610766346699,-4.15762469622184,-3.54698369494362,-4.41063181575339,-3.3991917232368,-2.07924430681596,3.66547142215042,4.11546010451152,4.5788317880187,4.54164476708552,6.24911630844292,2.94176429031876,4.91722359556646,4.68454846578415,6.32554913408199,7.29127203757818,-5.19029154455482,-5.13804833369078,-6.03538388162579,-4.10437235544708,-3.80816188907634,3.93632795124526,5.36882763411433,5.37837548052549,6.91568816870243,5.79097324342121,-2.71500317257465,-0.307745313132091,-1.16173323791098,-0.14182866447016,2.31243918737546,-5.75298064796168,-5.14926724362705,-4.25424445327965,-4.79636955777938,-3.42465709071545,-1.97630902147991,0.692971683882685,0.440959630244447,0.0709024405101132,0.622335733898549,-5.03764292957633,-3.31270143362489,-4.1056576938449,-3.2975583477008,-2.21959152572253,-1.36488733045942,-0.718080698235093,2.49935178789941,0.645173889834543,-0.477803443245903");
    param = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
    param->handPointerToPrior(param);
    param->initializeStorageBasedOnPrior({50, 5}, {dimNames, dimNames});
    param->initializeStorage();

    // calculate mean
    std::vector<std::vector<double>> meanParam;
    priorWithHyperPrior->_fillMeanParameterValues(meanParam);

    // find points that are most far apart
    std::vector<std::vector<double>> minValues;
    priorWithHyperPrior->_findMostDistantPoints(minValues, meanParam);

    EXPECT_EQ(minValues.size(), K);
    std::vector<std::vector<double>> expected = {{1.91714536276596,3.0828460206628,5.61078100970474,5.74152079791429,7.3039648225257},
                                                 {-5.19029154455482,-5.13804833369078,-6.03538388162579,-4.10437235544708,-3.80816188907634},
                                                 {-2.85616451407826,2.18392291802792,0.278246735587241,1.12506961668223,-0.151432502085334}};
    for (size_t k = 0; k < K; k++){
        EXPECT_EQ(minValues[k].size(), 5);
        for (size_t d = 0; d < 5; d++) {
            EXPECT_FLOAT_EQ(minValues[k][d], expected[k][d]);
        }
    }

    // set initial z
    priorWithHyperPrior->_setInitialZ(minValues, meanParam);
    std::vector<size_t > expectedZ = {0,1,2,2,0,2,2,0,2,0,1,0,0,0,2,0,2,2,1,2,0,1,2,1,0,2,2,2,0,1,2,0,2,2,0,0,2,0,0,0,1,0,0,1,0,2,1,2,1,2};
    for (size_t i = 0; i < z->totalSize(); i++){
        EXPECT_EQ(z->value<size_t>(i), expectedZ[i]);
    }
}

TEST_F(TPriorMultivariateNormalMixedModelWithHyperPriorTest, _setInitialMeanAndM) {
    // simulations and true values based on R script 'simulateHMM.R'

    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setPriors();
    initializeDefinitions(params);

    // re-set z to have more data points (clearer pattern)
    size_t K = 3;
    defZ->setDimensions({50});
    defZ->setType("uint8_t");
    defZ->setInitVal("1,0,1,1,0,1,2,0,2,1,0,0,1,1,1,2,0,2,0,2,0,1,1,2,0,0,2,1,0,1,1,0,0,0,0,1,1,1,2,1,0,2,0,1,0,0,2,1,1,0");
    z = std::make_shared<TMCMCParameter<uint8_t>>(std::move(priorZ), defZ, &randomGenerator);
    params[12] = z;
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    def->setDimensions({50, 5});
    def->setInitVal("-0.549812898727345,-0.518559832714638,-0.318068374543844,-0.429362147453702,-0.487460310141485,3.4313312671815,4.36482138487617,6.1780869965732,3.97643319957024,6.59394618762842,-2.23132342155804,0.483895570053379,0.219924803660651,-0.967250029092243,1.52102274264814,-1.50595746211426,0.843038825170412,-0.214579408546869,0.320443469956613,0.899809258786438,3.83547640374641,4.24663831986349,5.69696337540474,6.05666319867366,5.31124430545048,-1.62036667722412,-0.457884126855765,-0.910921648552446,0.658028772404075,0.345415356081182,-6.73321840682484,-4.49786814031973,-4.63030033392815,-3.84096857986041,-4.15657236263585,4.47550952889966,3.79005356907819,5.61072635348905,4.56590236835575,4.7463665997609,-5.09817874400523,-3.93917927137988,-5.18645863857947,-2.40322295572576,-3.00534402827817,-1.39280792944198,-0.819992868548507,-0.279113302976559,0.994188331267827,0.822669517730394,4.18879229951434,2.69504137110896,6.46555486156289,5.6532533382119,8.17261167036215,5.35867955152904,4.397212272657,5.38767161155937,5.4461949594171,4.62294044317139,-0.939839559565485,-1.08889448625966,0.531496192632572,-1.01839408178679,1.30655786078977,-0.287333692948595,-0.573564404126326,-0.0376341714670479,-0.181660478755657,0.675729727753681,-1.75081900119345,1.58716654562835,0.0173956196932517,-0.786300530434326,-0.640605534418578,-5.16437583176967,-4.07930535674549,-4.40024674397764,-4.87020787754746,-2.01216173254512,3.943871260471,4.34420449329467,3.52924761610073,5.02184994489138,6.4179415601997,-3.48025497450045,-4.80874056922561,-5.25328975560769,-2.85775869432218,-3.04470913689398,3.37354618925767,4.68364332422208,4.16437138758995,7.09528080213779,6.32950777181536,-4.29268933260192,-3.46589226526254,-3.7765195850847,-4.37870761286602,-1.83703544403267,6.40161776050478,4.46075999726683,5.68973936245078,5.52800215878067,5.25672679111759,-0.574899622627552,-0.738647100913033,1.05848304870902,1.38642265137494,0.380756951768853,-1.03472602831128,0.287639605630162,2.07524500865228,1.52739243876377,2.2079083983867,-2.50233841016584,-3.83293383323451,-3.4586726640363,-3.51339952314591,-2.48989157704707,3.58500543670032,4.10571004628965,4.94068660328881,6.60002537198388,6.76317574845754,3.17953161588198,4.98742905242849,5.73832470512922,6.07578135165349,5.69461161284364,-3.19685809208253,-4.83113203639122,-5.60551341225308,-3.30280656126052,-2.73682435359453,-1.15875460471602,0.964587311969797,-0.766081999604665,0.0697882460714535,0.0738905026225628,4.5584864255653,3.22340779154196,4.42673458576311,4.27538738510164,5.52659936356069,-2.04798441280774,0.941157706844281,-1.01584746530465,0.911974712317515,0.61892394889108,0.767287269372646,0.216707476017206,0.910174229495227,0.884185357826345,2.68217608051942,4.91897737160822,5.28213630073107,5.07456498336519,3.51064830413663,6.61982574789471,3.29250484303788,4.86458196213683,5.76853292451542,5.38765378784977,6.88110772645422,5.98039989850586,4.13277852353349,3.95586537368347,6.06971962744241,5.86494539611918,3.95506639098477,4.48380973690105,5.9438362106853,6.32122119509809,6.59390132121751,-1.17710396143654,-0.0979882205136621,-0.731748173119606,1.33037316798167,-0.208082786304465,-1.63573645394898,-0.961644730360566,1.43228223854166,-0.150696353310367,0.792619256398035,-2.53644982353759,-0.800976126836611,-0.528279904445006,-0.152094780680999,0.943103222152607,-5.98582670040929,-7.38892067167954,-4.64048170256511,-2.92949236407952,-3.05972327604261,-2.07519229661568,0.500028803713914,-0.621266694796823,-0.884426847384491,2.86929062242358,3.45747996900835,5.70786780598317,6.16040261569495,6.200213649515,7.58683345454085,-4.59059816034907,-2.81112671379595,-2.41341156655803,-3.83090780068277,-5.28523553529247,4.33295037121352,5.56309983727636,4.6958160763657,5.87001880991629,6.26709879077223,-2.91435942568001,0.676583312018562,-1.664972436212,0.0364695985276144,-0.115920105042845,4.39810588036707,3.88797360674923,5.34111969142442,4.37063690391921,7.43302370170104,4.29144623551746,4.05670812678157,5.00110535163162,5.57434132415166,5.41047905381193,-7.00016494478548,-5.04479074000172,-4.25567070915699,-3.66612103676501,-1.97953609121589,1.30797839905936,-0.394197632106289,0.456998805423414,0.422847064643469,0.665999157633456,1.20610246454047,-0.755027030141015,-1.42449465021281,0.355600398045781,1.20753833923234,5.51178116845085,4.88984323641143,4.3787594194582,3.2853001128225,7.12493091814311");
    param = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
    param->handPointerToPrior(param);
    param->initializeStorageBasedOnPrior({50, 5}, {dimNames, dimNames});
    param->initializeStorage();

    // calculate mean and variance based on z and param
    priorWithHyperPrior->_setInitialMeanAndM();

    std::vector<std::vector<double>> expectedMeans = {{4.323528, 4.408386, 5.207406, 5.344226, 6.260891},
                                                      {-1.10760496, -0.03532857, -0.09055041,  0.21637645,  0.82806711},
                                                      {-4.804450, -4.469989, -4.362057, -3.559359, -2.960703}};
    std::vector<std::vector<double>> expectedMrr = {{1.3139124, 1.4277875, 1.2930445, 0.9863651, 1.0790508},
                                                      {0.9920687, 1.3452873, 1.1737095, 1.2817348, 1.0743238},
                                                      {0.8255203, 1.0288827, 1.4561026, 1.4652312, 0.9877661}};
    std::vector<std::vector<double>> expectedMrs = {{0.1573176,  0.2993756,  0.09015391, 0.3761421, -0.08936336, -0.07192644, 0.3827986, -0.08068906, -0.20038523, 0.06971016},
                                                    {0.4383422, -0.1329276, 0.2177355024, -0.3212492, 0.0858837914, -0.2262790, -0.1141799, 0.00090212241, -0.4357094, -0.000684821},
                                                    {-0.54646253, -0.07741808, -0.4668938, -0.70797852,  0.4506182, 1.0506617, -0.35595644,  0.1541158, 0.5971107, 0.1993824}};
    // m's
    EXPECT_FLOAT_EQ(m0->value<double>(), 1.);
    EXPECT_FLOAT_EQ(m1->value<double>(), 1.);
    EXPECT_FLOAT_EQ(m2->value<double>(), 1.);

    for (size_t d = 0; d < 5; d++){
        // means
        EXPECT_FLOAT_EQ(mu0->value<double>(d), expectedMeans[0][d]);
        EXPECT_FLOAT_EQ(mu1->value<double>(d), expectedMeans[1][d]);
        EXPECT_FLOAT_EQ(mu2->value<double>(d), expectedMeans[2][d]);

        // Mrr
        EXPECT_FLOAT_EQ(mrr0->value<double>(d), expectedMrr[0][d]);
        EXPECT_FLOAT_EQ(mrr1->value<double>(d), expectedMrr[1][d]);
        EXPECT_FLOAT_EQ(mrr2->value<double>(d), expectedMrr[2][d]);
    }

    for (size_t d = 0; d < 10; d++){
        // Mrs
        EXPECT_FLOAT_EQ(mrs0->value<double>(d), expectedMrs[0][d]);
        EXPECT_FLOAT_EQ(mrs1->value<double>(d), expectedMrs[1][d]);
        EXPECT_FLOAT_EQ(mrs2->value<double>(d), expectedMrs[2][d]);
    }
}

#endif

//--------------------------------------------
// TPriorMultivariateNormalTwoMixedModelsWithHyperPrior

#ifdef WITH_ARMADILLO
class BridgeTPriorMultivariateTwoNormalMixedModelsWithHyperPrior : public TPriorMultivariateNormalTwoMixedModelsWithHyperPrior<double> {
public:
    BridgeTPriorMultivariateTwoNormalMixedModelsWithHyperPrior() : TPriorMultivariateNormalTwoMixedModelsWithHyperPrior() {};
    size_t _linearizeIndex_Mrs(size_t r, size_t s) const{
        return TPriorMultivariateNormalTwoMixedModelsWithHyperPrior::_linearizeIndex_Mrs(r, s);
    }
    arma::vec _stretchEigenVals(const arma::vec& Lambda_0){
        return TPriorMultivariateNormalTwoMixedModelsWithHyperPrior::_stretchEigenVals(Lambda_0);
    }
    void _stretchSigma1(){
        return TPriorMultivariateNormalTwoMixedModelsWithHyperPrior::_stretchSigma1();
    }
    void _fill_M0(){
        return TPriorMultivariateNormalTwoMixedModelsWithHyperPrior::_fill_M0();
    }
    void _fill_M1(){
        return TPriorMultivariateNormalTwoMixedModelsWithHyperPrior::_fill_M1();
    }
    void _fill_M0_afterUpdateMrr(const double & newVal, const size_t & r){
        TPriorMultivariateNormalTwoMixedModelsWithHyperPrior::_fill_M0_afterUpdateMrr(newVal, r);
    }
    void _fill_M0_afterUpdateMrs(const double & newVal, const size_t & r, const size_t & s){
        TPriorMultivariateNormalTwoMixedModelsWithHyperPrior::_fill_M0_afterUpdateMrs(newVal, r, s);
    }
    void _fill_M0_afterUpdate_m(const double & newVal, const double & oldVal){
        TPriorMultivariateNormalTwoMixedModelsWithHyperPrior::_fill_M0_afterUpdate_m(newVal, oldVal);
    }
    void calculateSigma0(){
        return _zeroModel.calculateSigma();
    }
    // acess to private armadillo matrices
    arma::mat getM0(){
        return _zeroModel.M;
    }
    arma::mat getM1(){
        return _oneModel.M;
    }
    arma::mat getSigma0(){
        return _zeroModel.Sigma;
    }
    arma::mat getSigma1(){
        return _oneModel.Sigma;
    }
    // set armadillo matrices
    void setM0(const arma::mat & M){
        _zeroModel.M = M;
    }
    void setM1(const arma::mat & M){
        _oneModel.M = M;
    }
    void setSigma0(const arma::mat & Sigma){
        _zeroModel.Sigma = Sigma;
    }
    void setSigma1(const arma::mat & Sigma){
        _oneModel.Sigma = Sigma;
    }
    // reset changes in M0 and M1
    void reset(){
        _zeroModel.reset();
        _oneModel.reset();
    }
    void resetM1Only(){
        _oneModel.reset();
    }
    // update z
    double _calcLLUpdateZ(const size_t & n){
        return TPriorMultivariateNormalTwoMixedModelsWithHyperPrior::_calcLLUpdateZ(n);
    }
    double  _calcLogHUpdateZ(const size_t & n){
        return TPriorMultivariateNormalTwoMixedModelsWithHyperPrior::_calcLogHUpdateZ(n);
    }
    // update rhos
    double _calcLLUpdateRho(const size_t & d){
        return TPriorMultivariateNormalTwoMixedModelsWithHyperPrior::_calcLLUpdateRho(d);
    }
    double  _calcLogHUpdateRho(const size_t & d){
        return TPriorMultivariateNormalTwoMixedModelsWithHyperPrior::_calcLogHUpdateRho(d);
    }
    // update mus_0
    double _calcLLUpdateMu(size_t k, size_t r){
        return TPriorMultivariateNormalTwoMixedModelsWithHyperPrior::_calcLLUpdateMu(k, r);
    }
    double _calcLogHUpdateMu(size_t k, size_t r){
        return TPriorMultivariateNormalTwoMixedModelsWithHyperPrior::_calcLogHUpdateMu(k, r);
    }
    // update m0
    virtual double _calcLLUpdateM(size_t k){
        return TPriorMultivariateNormalTwoMixedModelsWithHyperPrior::_calcLLUpdateM(k);
    }
    double _calcLogHUpdateM(size_t k){
        return TPriorMultivariateNormalTwoMixedModelsWithHyperPrior::_calcLogHUpdateM(k);
    }

    // update mrr0
    double _calcLLUpdateMrr(size_t k, size_t r){
        return TPriorMultivariateNormalTwoMixedModelsWithHyperPrior::_calcLLUpdateMrr(k, r);
    }
    double _calcLogHUpdateMrr(size_t k, size_t r){
        return TPriorMultivariateNormalTwoMixedModelsWithHyperPrior::_calcLogHUpdateMrr(k, r);
    }

    // update mrs0
    double _calcLLUpdateMrs(size_t k, size_t r, size_t s){
        return TPriorMultivariateNormalTwoMixedModelsWithHyperPrior::_calcLLUpdateMrs(k, r, s);
    }
    double _calcLogHUpdateMrs(size_t k, size_t r, size_t s){
        return TPriorMultivariateNormalTwoMixedModelsWithHyperPrior::_calcLogHUpdateMrs(k, r, s);
    }
    // EM
    void _setMusToMLE() override{
        TPriorMultivariateNormalTwoMixedModelsWithHyperPrior::_setMusToMLE();
    }
    void _setMToMLE() override{
        TPriorMultivariateNormalTwoMixedModelsWithHyperPrior::_setMToMLE();
    }
    void _setAllParametersAfterEM(TLog * Logfile){
        TPriorMultivariateNormalTwoMixedModelsWithHyperPrior::_setAllParametersAfterEM(Logfile);
    }
};

class TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest : public Test {
protected:
    std::shared_ptr<BridgeTPriorMultivariateTwoNormalMixedModelsWithHyperPrior> priorWithHyperPrior;

    std::shared_ptr<TParameterDefinition> defMu;
    std::shared_ptr<TParameterDefinition> defM0;
    std::shared_ptr<TParameterDefinition> defMrr0;
    std::shared_ptr<TParameterDefinition> defMrs0;
    std::shared_ptr<TParameterDefinition> defRho;
    std::shared_ptr<TParameterDefinition> defZ;
    std::unique_ptr<TPriorWithHyperPrior<double>> priorMu;
    std::unique_ptr<TPrior<double>> priorM0;
    std::unique_ptr<TPrior<double>> priorMrr0;
    std::unique_ptr<TPriorWithHyperPrior<double>> priorMrs0;
    std::unique_ptr<TPriorWithHyperPrior<double>> priorRho;
    std::unique_ptr<TPriorWithHyperPrior<uint8_t>> priorZ;

    std::shared_ptr<TMCMCParameter<double>> mu;
    std::shared_ptr<TMCMCParameter<double>> m0;
    std::shared_ptr<TMCMCParameter<double>> mrr0;
    std::shared_ptr<TMCMCParameter<double>> mrs0;
    std::shared_ptr<TMCMCParameter<double>> rho;
    std::shared_ptr<TMCMCParameter<uint8_t>> z;

    std::shared_ptr<BridgeTMCMCArrayParameter<double>> param; // parameter on which prior is defined
    std::shared_ptr<TParameterDefinition> def;
    std::shared_ptr<TNamesEmpty> dimNames;

    TRandomGenerator randomGenerator;
    std::vector<std::shared_ptr<TObservationsBase>> observations = {};
    TLog logfile;

    void SetUp() override {
        defMu = std::make_shared<TParameterDefinition>("defMu0");
        defM0 = std::make_shared<TParameterDefinition>("defM0");
        defMrr0 = std::make_shared<TParameterDefinition>("defMrr0");
        defMrs0 = std::make_shared<TParameterDefinition>("defMrs0");
        defRho = std::make_shared<TParameterDefinition>("defRho");
        defZ = std::make_shared<TParameterDefinition>("defZ");

        // initialize param
        priorWithHyperPrior = std::make_shared<BridgeTPriorMultivariateTwoNormalMixedModelsWithHyperPrior>();
        def = std::make_shared<TParameterDefinition>("defParam");
    }

    void setInitialValues_5D(bool setInitValsOfParam = true){
        // values were generated in R with rnorm() in script multivariate_normal.R
        // mu0
        defMu->setInitVal("-0.626453810742332,0.183643324222082,-0.835628612410047,1.59528080213779,0.329507771815361");
        // m0
        defM0->setInitVal("0.2");
        // mrr0
        defMrr0->setInitVal("0.820468384118015,0.487429052428485,0.738324705129217,0.575781351653492,0.305388387156356");
        // mrs0
        defMrs0->setInitVal("1.51178116845085,0.389843236411431,-0.621240580541804,-2.2146998871775,1.12493091814311,"
                            "-0.0449336090152309,-0.0161902630989461,0.943836210685299,0.821221195098089,0.593901321217509");
        // rho
        defRho->setInitVal("0.996812955243844,1.43528534337308,0.0372685263864696,0.324010152835399,1.32046792931379");
        // z
        defZ->setType("uint8_t");
        defZ->setInitVal("1,0,0,0,0,0,0,1,0,1");

        // parameter itself
        if (setInitValsOfParam) {
            def->setInitVal("-0.41499456329968,-0.70749515696212,1.98039989850586,0.188792299514343,0.291446235517463,"
                            "-0.394289953710349,0.36458196213683,-0.367221476466509,-1.80495862889104,-0.443291873218433,"
                            "-0.0593133967111857,0.768532924515416,-1.04413462631653,1.46555486156289,0.00110535163162413,"
                            "1.10002537198388,-0.112346212150228,0.569719627442413,0.153253338211898,0.0743413241516641,"
                            "0.763175748457544,0.881107726454215,-0.135054603880824,2.17261167036215,-0.589520946188072,"
                            "-0.164523596253587,0.398105880367068,2.40161776050478,0.475509528899663,-0.568668732818502,"
                            "-0.253361680136508,-0.612026393250771,-0.0392400027331692,-0.709946430921815,-0.135178615123832,"
                            "0.696963375404737,0.341119691424425,0.689739362450777,0.610726353489055,1.1780869965732,"
                            "0.556663198673657,-1.12936309608079,0.0280021587806661,-0.934097631644252,-1.52356680042976,"
                            "-0.68875569454952,1.43302370170104,-0.743273208882405,-1.2536334002391,0.593946187628422");
        }
    }

    void setInitialValues_1D(){
        // values were generated in R with rnorm() in script multivariate_normal.R
        // mu
        defMu->setInitVal("-0.626453810742332");
        // m
        defM0->setInitVal("0.2");
        // mrr
        defMrr0->setInitVal("0.820468384118015"); // should be changed at initialization to 1
        // rho
        defRho->setInitVal("0.996812955243844");
        // z
        defZ->setType("uint8_t");
        defZ->setInitVal("1,0,0,0,0,0,0,1,0,1");

        // parameter itself
        def->setInitVal("-0.41499456329968,-0.394289953710349,-0.0593133967111857,1.10002537198388,0.763175748457544,"
                        "-0.164523596253587,-0.253361680136508,0.696963375404737,0.556663198673657,-0.68875569454952");
    }

    void setPriors(size_t numDim = 5){
        // this looks horrible, as I dont' want to use MCMCManager to construct the parameters
        // as a real-life user, it won't be necessary to define all these parameters!

        std::vector<std::shared_ptr<TMCMCParameterBase> > params;
        // mu0
        auto priorMuOnMu = std::make_unique<TPriorUniform<double>>();
        auto defMuOnMu = std::make_shared<TParameterDefinition>("defMuOnMu");
        defMuOnMu->setInitVal("0"); defMuOnMu->update(false);
        auto muOnMu = std::make_shared<TMCMCParameter<double>>(std::move(priorMuOnMu), defMuOnMu, &randomGenerator);

        auto priorSdOnMu = std::make_unique<TPriorUniform<double>>();
        auto defSdOnMu = std::make_shared<TParameterDefinition>("defSdOnMu");
        defSdOnMu->setInitVal("1"); defSdOnMu->update(false);
        auto sdOnMu = std::make_shared<TMCMCParameter<double>>(std::move(priorSdOnMu), defSdOnMu, &randomGenerator);
        params = {muOnMu, sdOnMu};
        priorMu = std::make_unique<TPriorNormalWithHyperPrior<double>>();
        priorMu->initPriorWithHyperPrior(params, observations);

        // m0
        priorM0 = std::make_unique<TPriorExponential<double>>();
        priorM0->initialize("5");

        // mrr0
        priorMrr0 = std::make_unique<TPriorMultivariateChisq<double>>();
        if (numDim == 5)
            priorMrr0->initialize("5,4,3,2,1");
        else priorMrr0->initialize("1");

        // mrs0
        auto priorMuOnMrs0 = std::make_unique<TPriorUniform<double>>();
        auto defMuOnMrs0 = std::make_shared<TParameterDefinition>("defMuOnMrs0");
        defMuOnMrs0->setInitVal("0"); defMuOnMrs0->update(false);
        auto muOnMrs0 = std::make_shared<TMCMCParameter<double>>(std::move(priorMuOnMrs0), defMuOnMrs0, &randomGenerator);

        auto priorSdOnMrs0 = std::make_unique<TPriorUniform<double>>();
        auto defSdOnMrs0 = std::make_shared<TParameterDefinition>("defSdOnMrs");
        defSdOnMrs0->setInitVal("1"); defSdOnMrs0->update(false);
        auto sdOnMrs0 = std::make_shared<TMCMCParameter<double>>(std::move(priorSdOnMrs0), defSdOnMrs0, &randomGenerator);
        params.clear();
        params = {muOnMrs0, sdOnMrs0};
        priorMrs0 = std::make_unique<TPriorNormalWithHyperPrior<double>>();
        priorMrs0->initPriorWithHyperPrior(params, observations);

        // rho
        auto priorLambdaOnRho = std::make_unique<TPriorUniform<double>>();
        auto defLambdaOnRho = std::make_shared<TParameterDefinition>("defLambdaOnRho");
        defLambdaOnRho->setInitVal("1"); defLambdaOnRho->update(false);
        auto lambdaOnRho = std::make_shared<TMCMCParameter<double>>(std::move(priorLambdaOnRho), defLambdaOnRho, &randomGenerator);
        params.clear();
        params = {lambdaOnRho};
        priorRho = std::make_unique<TPriorExponentialWithHyperPrior<double>>();
        priorRho->initPriorWithHyperPrior(params, observations);

        // z
        auto priorPiOnZ = std::make_unique<TPriorUniform<double>>();
        auto defPiOnZ = std::make_shared<TParameterDefinition>("defPiOnZ");
        defPiOnZ->setInitVal("0.3"); defPiOnZ->update(false);
        auto piOnZ = std::make_shared<TMCMCParameter<double>>(std::move(priorPiOnZ), defPiOnZ, &randomGenerator);
        params.clear();
        params = {piOnZ};
        priorZ = std::make_unique<TPriorBernouilliWithHyperPrior<uint8_t>>();
        priorZ->initPriorWithHyperPrior(params, observations);
    }

    void initializeDefinitions(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, size_t numDim = 5){
        // mu
        defMu->setDimensions({numDim});
        mu = std::make_shared<TMCMCParameter<double>>(std::move(priorMu), defMu, &randomGenerator);
        // m0
        m0 = std::make_shared<TMCMCParameter<double>>(std::move(priorM0), defM0, &randomGenerator);
        // mrr0
        defMrr0->setDimensions({numDim});
        mrr0 = std::make_shared<TMCMCParameter<double>>(std::move(priorMrr0), defMrr0, &randomGenerator);
        // mrs0
        defMrs0->setDimensions({static_cast<size_t>((numDim - 1.) * numDim / 2.)});
        mrs0 = std::make_shared<TMCMCParameter<double>>(std::move(priorMrs0), defMrs0, &randomGenerator);
        // rho
        defRho->setDimensions({numDim});
        rho = std::make_shared<TMCMCParameter<double>>(std::move(priorRho), defRho, &randomGenerator);
        // z
        defZ->setDimensions({10});
        z = std::make_shared<TMCMCParameter<uint8_t>>(std::move(priorZ), defZ, &randomGenerator);

        params = {mu, m0, mrr0, mrs0, z, rho};

        // parameter itself
        def->setDimensions({10, numDim});
        param = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
        dimNames = std::make_shared<TNamesIndices>();
        param->initializeStorageBasedOnPrior({10, numDim}, {dimNames, dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)
    }

    void TearDown() override {}
};

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, initParameter_wrongDimensions){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    // this is ok, dimensions match!
    param->handPointerToPrior(param);

    // this should throw, wrong number of columns
    auto otherDef = std::make_shared<TParameterDefinition>("otherDefParam");
    otherDef->setDimensions({10, 9});
    std::shared_ptr<BridgeTMCMCArrayParameter<double>> otherParam;
    otherParam = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, otherDef, &randomGenerator);
    otherParam->handPointerToPrior(otherParam);
    otherParam->initializeStorageBasedOnPrior({10, 9}, {dimNames, dimNames});
    EXPECT_THROW(otherParam->initializeStorage(), std::runtime_error);

    // this should also throw, rows - cols switched
    auto otherDef2 = std::make_shared<TParameterDefinition>("otherDef2Param");
    otherDef2->setDimensions({5, 10});
    std::shared_ptr<BridgeTMCMCArrayParameter<double>> otherParam2;
    otherParam2 = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, otherDef2, &randomGenerator);
    otherParam2->handPointerToPrior(otherParam);
    otherParam2->initializeStorageBasedOnPrior({5, 10}, {dimNames, dimNames});
    EXPECT_THROW(otherParam2->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, initParameter_wrongDimensionsOfZ){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace z by another z with wrong dimensions that don't match the number of rows of the parameter
    defZ->setDimensions({5});
    z = std::make_shared<TMCMCParameter<uint8_t>>(std::move(priorZ), defZ, &randomGenerator);
    params[4] = z;

    // this is ok, we don't know dimensions of parameter yet
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);

    // this throws, as size of z and number of rows of parameter don't match
    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, initPriorWithHyperPrior_wrongNumParams){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);
    EXPECT_NO_THROW(priorWithHyperPrior->initPriorWithHyperPrior(params, observations));

    // add one more
    params.push_back(m0);

    // now initialize priorWithHyperPrior - throws because 5 instead of 4 parameters were given
    EXPECT_THROW(priorWithHyperPrior->initPriorWithHyperPrior(params, observations), std::runtime_error);
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_mu0){
    // will just test mu0 here (not m0, mrr0, mrs0), because if mu works the other should also work as they are checked inside the same function of the base class
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace mu by another mu with wrong dimensions
    defMu->setDimensions({2, 5});
    mu = std::make_shared<TMCMCParameter<double>>(std::move(priorMu), defMu, &randomGenerator);
    params[0] = mu;
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);

    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_z){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace z by another z with wrong dimensions
    defZ->setDimensions({2, 1});
    z = std::make_shared<TMCMCParameter<uint8_t>>(std::move(priorZ), defZ, &randomGenerator);
    params[4] = z;
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);

    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_rho){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace rho by another rho with wrong dimensions
    defRho->setDimensions({2, 1});
    rho = std::make_shared<TMCMCParameter<double>>(std::move(priorRho), defRho, &randomGenerator);
    params[5] = rho;
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);

    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, initPriorWithHyperPrior_wrongSize_rho){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace mu by another mu with wrong dimensions
    defRho->setDimensions({11});
    rho = std::make_shared<TMCMCParameter<double>>(std::move(priorRho), defRho, &randomGenerator);
    params[5] = rho;
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);

    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, _stretchEigenVals){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    arma::vec Lambda_0 = {0.1, 0.2, 0.3, 0.4, 0.5};
    arma::vec Lambda_1 = priorWithHyperPrior->_stretchEigenVals(Lambda_0);

    EXPECT_EQ(Lambda_1.size(), 5);

    EXPECT_FLOAT_EQ(Lambda_1[0], 0.1996813);
    EXPECT_FLOAT_EQ(Lambda_1[1], 0.4870571);
    EXPECT_FLOAT_EQ(Lambda_1[2], 0.3111806);
    EXPECT_FLOAT_EQ(Lambda_1[3], 0.5296041);
    EXPECT_FLOAT_EQ(Lambda_1[4], 1.1602340);
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, _fillM0){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    priorWithHyperPrior->_fill_M0();

    // get filled matrix and check if elements are correct
    arma::mat M0 = priorWithHyperPrior->getM0();
    EXPECT_EQ(M0.n_rows, 5);
    EXPECT_EQ(M0.n_cols, 5);

    EXPECT_FLOAT_EQ(M0(0,0), 4.10234192);
    EXPECT_FLOAT_EQ(M0(1,0), 7.55890584); EXPECT_FLOAT_EQ(M0(1,1), 2.437145);
    EXPECT_FLOAT_EQ(M0(2,0), 1.94921618); EXPECT_FLOAT_EQ(M0(2,1),  -3.106203); EXPECT_FLOAT_EQ(M0(2,2), 3.691624);
    EXPECT_FLOAT_EQ(M0(3,0),  -11.07349944); EXPECT_FLOAT_EQ(M0(3,1), 5.624655); EXPECT_FLOAT_EQ(M0(3,2), -0.224668); EXPECT_FLOAT_EQ(M0(3,3), 2.878907);
    EXPECT_FLOAT_EQ(M0(4,0), -0.08095132); EXPECT_FLOAT_EQ(M0(4,1), 4.719181); EXPECT_FLOAT_EQ(M0(4,2), 4.106106); EXPECT_FLOAT_EQ(M0(4,3), 2.969507); EXPECT_FLOAT_EQ(M0(4,4), 1.526942);

    // all other elements must be 0
    for (size_t row = 0; row < 5; row++){
        for (size_t col = row+1; col < 5; col++){
            EXPECT_EQ(M0(row, col), 0);
        }
    }
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, calculateSigma0){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // first fill M0 and then Sigma0 gets filled
    priorWithHyperPrior->calculateSigma0();

    // get filled matrix and check if elements are correct
    arma::mat Sigma0 = priorWithHyperPrior->getSigma0();
    EXPECT_EQ(Sigma0.n_rows, 5);
    EXPECT_EQ(Sigma0.n_cols, 5);

    EXPECT_FLOAT_EQ(Sigma0(0,0), 6.7923885);
    EXPECT_FLOAT_EQ(Sigma0(1,0), -2.2789786); EXPECT_FLOAT_EQ(Sigma0(1,1), 1.3637252);
    EXPECT_FLOAT_EQ(Sigma0(2,0), -0.0238001); EXPECT_FLOAT_EQ(Sigma0(2,1),  0.6080712); EXPECT_FLOAT_EQ(Sigma0(2,2), 0.6660243);
    EXPECT_FLOAT_EQ(Sigma0(3,0),  0.9353109); EXPECT_FLOAT_EQ(Sigma0(3,1), 0.1969531); EXPECT_FLOAT_EQ(Sigma0(3,2), 0.5271811); EXPECT_FLOAT_EQ(Sigma0(3,3), 0.5769736);
    EXPECT_FLOAT_EQ(Sigma0(4,0), -0.11369944); EXPECT_FLOAT_EQ(Sigma0(4,1), -0.4518327); EXPECT_FLOAT_EQ(Sigma0(4,2), -0.5039779); EXPECT_FLOAT_EQ(Sigma0(4,3), -0.4423965); EXPECT_FLOAT_EQ(Sigma0(4,4), 0.4288989);

    // all elements must be same as the ones mirrored along the diagonal
    for (size_t row = 0; row < 5; row++){
        for (size_t col = 0; col < 5; col++){
            EXPECT_EQ(Sigma0(row, col), Sigma0(col, row));
        }
    }
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, _stretchSigma1){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    priorWithHyperPrior->_stretchSigma1();

    // get filled matrix and check if elements are correct
    arma::mat Sigma1 = priorWithHyperPrior->getSigma1();
    EXPECT_EQ(Sigma1.n_rows, 5);
    EXPECT_EQ(Sigma1.n_cols, 5);

    EXPECT_FLOAT_EQ(Sigma1(0,0), 15.7254193);
    EXPECT_FLOAT_EQ(Sigma1(1,0), -5.4219346); EXPECT_FLOAT_EQ(Sigma1(1,1), 2.66939296);
    EXPECT_FLOAT_EQ(Sigma1(2,0), -0.2012354); EXPECT_FLOAT_EQ(Sigma1(2,1),  0.86943394); EXPECT_FLOAT_EQ(Sigma1(2,2), 0.8796913);
    EXPECT_FLOAT_EQ(Sigma1(3,0),  2.0402969); EXPECT_FLOAT_EQ(Sigma1(3,1), -0.02582492); EXPECT_FLOAT_EQ(Sigma1(3,2), 0.6780219); EXPECT_FLOAT_EQ(Sigma1(3,3), 0.86202003);
    EXPECT_FLOAT_EQ(Sigma1(4,0), -0.1405949); EXPECT_FLOAT_EQ(Sigma1(4,1), -0.59192888); EXPECT_FLOAT_EQ(Sigma1(4,2), -0.6711749); EXPECT_FLOAT_EQ(Sigma1(4,3), -0.58166372); EXPECT_FLOAT_EQ(Sigma1(4,4), 0.5771569);

    // all elements must be same as the ones mirrored along the diagonal
    for (size_t row = 0; row < 5; row++){
        for (size_t col = 0; col < 5; col++){
            EXPECT_EQ(Sigma1(row, col), Sigma1(col, row));
        }
    }
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, _fillM1){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    priorWithHyperPrior->_fill_M1();

    // get filled matrix and check if elements are correct
    arma::mat M1 = priorWithHyperPrior->getM1();
    EXPECT_EQ(M1.n_rows, 5);
    EXPECT_EQ(M1.n_cols, 5);

    EXPECT_FLOAT_EQ(M1(0,0), 2.9014867);
    EXPECT_FLOAT_EQ(M1(1,0), 5.3796527); EXPECT_FLOAT_EQ(M1(1,1), 1.784806);
    EXPECT_FLOAT_EQ(M1(2,0), 0.9750320); EXPECT_FLOAT_EQ(M1(2,1),  -3.324089); EXPECT_FLOAT_EQ(M1(2,2), 3.17542581);
    EXPECT_FLOAT_EQ(M1(3,0),  -7.8392382); EXPECT_FLOAT_EQ(M1(3,1), 4.046776); EXPECT_FLOAT_EQ(M1(3,2), -0.01848968); EXPECT_FLOAT_EQ(M1(3,3), 1.904108);
    EXPECT_FLOAT_EQ(M1(4,0), -0.5424477); EXPECT_FLOAT_EQ(M1(4,1), 2.043284); EXPECT_FLOAT_EQ(M1(4,2), 3.67406403); EXPECT_FLOAT_EQ(M1(4,3), 1.918977); EXPECT_FLOAT_EQ(M1(4,4), 1.316294);

    // all other elements must be 0
    for (size_t row = 0; row < 5; row++){
        for (size_t col = row+1; col < 5; col++){
            EXPECT_EQ(M1(row, col), 0);
        }
    }
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, _fill_M0_afterUpdateMrr){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    priorWithHyperPrior->_fill_M0_afterUpdateMrr(60.67, 4);

    // get filled matrix and check if elements are correct
    arma::mat M0 = priorWithHyperPrior->getM0();
    EXPECT_EQ(M0.n_rows, 5);
    EXPECT_EQ(M0.n_cols, 5);

    // should stay the same, except for the element we've changed
    EXPECT_FLOAT_EQ(M0(0,0), 4.10234192);
    EXPECT_FLOAT_EQ(M0(1,0), 7.55890584); EXPECT_FLOAT_EQ(M0(1,1), 2.437145);
    EXPECT_FLOAT_EQ(M0(2,0), 1.94921618); EXPECT_FLOAT_EQ(M0(2,1),  -3.106203); EXPECT_FLOAT_EQ(M0(2,2), 3.691624);
    EXPECT_FLOAT_EQ(M0(3,0),  -11.07349944); EXPECT_FLOAT_EQ(M0(3,1), 5.624655); EXPECT_FLOAT_EQ(M0(3,2), -0.224668); EXPECT_FLOAT_EQ(M0(3,3), 2.878907);
    EXPECT_FLOAT_EQ(M0(4,0), -0.08095132); EXPECT_FLOAT_EQ(M0(4,1), 4.719181); EXPECT_FLOAT_EQ(M0(4,2), 4.106106); EXPECT_FLOAT_EQ(M0(4,3), 2.969507); EXPECT_FLOAT_EQ(M0(4,4), 60.67/0.2);

    // all other elements must be 0
    for (size_t row = 0; row < 5; row++){
        for (size_t col = row+1; col < 5; col++){
            EXPECT_EQ(M0(row, col), 0);
        }
    }
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, _fill_M0_afterUpdateMrs){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    priorWithHyperPrior->_fill_M0_afterUpdateMrs(60.67, 4, 3);

    // get filled matrix and check if elements are correct
    arma::mat M0 = priorWithHyperPrior->getM0();
    EXPECT_EQ(M0.n_rows, 5);
    EXPECT_EQ(M0.n_cols, 5);

    // should stay the same, except for the element we've changed
    EXPECT_FLOAT_EQ(M0(0,0), 4.10234192);
    EXPECT_FLOAT_EQ(M0(1,0), 7.55890584); EXPECT_FLOAT_EQ(M0(1,1), 2.437145);
    EXPECT_FLOAT_EQ(M0(2,0), 1.94921618); EXPECT_FLOAT_EQ(M0(2,1),  -3.106203); EXPECT_FLOAT_EQ(M0(2,2), 3.691624);
    EXPECT_FLOAT_EQ(M0(3,0),  -11.07349944); EXPECT_FLOAT_EQ(M0(3,1), 5.624655); EXPECT_FLOAT_EQ(M0(3,2), -0.224668); EXPECT_FLOAT_EQ(M0(3,3), 2.878907);
    EXPECT_FLOAT_EQ(M0(4,0), -0.08095132); EXPECT_FLOAT_EQ(M0(4,1), 4.719181); EXPECT_FLOAT_EQ(M0(4,2), 4.106106); EXPECT_FLOAT_EQ(M0(4,3), 60.67/0.2); EXPECT_FLOAT_EQ(M0(4,4), 1.526942);

    // all other elements must be 0
    for (size_t row = 0; row < 5; row++){
        for (size_t col = row+1; col < 5; col++){
            EXPECT_EQ(M0(row, col), 0);
        }
    }
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, _fill_M0_afterUpdate_m){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    priorWithHyperPrior->_fill_M0_afterUpdate_m(0.55, 0.2);

    // get filled matrix and check if elements are correct
    arma::mat M0 = priorWithHyperPrior->getM0();
    EXPECT_EQ(M0.n_rows, 5);
    EXPECT_EQ(M0.n_cols, 5);

    // should stay the same, except for the element we've changed
    EXPECT_FLOAT_EQ(M0(0,0), 1.49176070);
    EXPECT_FLOAT_EQ(M0(1,0), 2.74869303); EXPECT_FLOAT_EQ(M0(1,1), 0.8862346);
    EXPECT_FLOAT_EQ(M0(2,0), 0.70880588); EXPECT_FLOAT_EQ(M0(2,1),  -1.1295283); EXPECT_FLOAT_EQ(M0(2,2), 1.34240855);
    EXPECT_FLOAT_EQ(M0(3,0),  -4.02672707); EXPECT_FLOAT_EQ(M0(3,1), 2.0453289); EXPECT_FLOAT_EQ(M0(3,2), -0.08169747); EXPECT_FLOAT_EQ(M0(3,3), 1.046875);
    EXPECT_FLOAT_EQ(M0(4,0),  -0.02943684); EXPECT_FLOAT_EQ(M0(4,1), 1.7160658); EXPECT_FLOAT_EQ(M0(4,2), 1.49312945); EXPECT_FLOAT_EQ(M0(4,3), 1.079821); EXPECT_FLOAT_EQ(M0(4,4), 0.5552516);

    // all other elements must be 0
    for (size_t row = 0; row < 5; row++){
        for (size_t col = row+1; col < 5; col++){
            EXPECT_EQ(M0(row, col), 0);
        }
    }
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, _getLogPriorDensity_vec){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // we want prior density of first element of first row -> z = 1
    size_t linearIndex = param->getIndex({0, 0});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(param, linearIndex), -229.614677302288);

    // we want prior density of third element of 6th row -> z = 0
    linearIndex = param->getIndex({5, 2});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(param, linearIndex), -504.058645509965);

    // we want prior density of last element of last row -> z = 1
    linearIndex = param->getIndex({9, 4});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(param, linearIndex),  -470.294479533199);
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, _getLogPriorDensity_vec_1D){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_1D();
    initializeDefinitions(params, 1);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // we want prior density of first element
    size_t linearIndex = param->getIndex({0, 0});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(param, linearIndex),  0.0648083011800458);

    // we want prior density of third element
    linearIndex = param->getIndex({2, 0});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(param, linearIndex), -3.33010373611332);

    // we want prior density of last element
    linearIndex = param->getIndex({9, 0});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(param, linearIndex), 0.32042493516794);
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, _getLogPriorDensityOld_vec){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // we want prior density of first element of first row
    size_t linearIndex = param->getIndex({0, 0});
    double val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.8);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityOld(param, linearIndex), -229.614677302288);

    // reset
    param->set(linearIndex, val);

    // we want prior density of third element of 6th row
    linearIndex = param->getIndex({5, 2});
    val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.5);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityOld(param, linearIndex), -504.058645509965);

    // reset
    param->set(linearIndex, val);

    // we want prior density of last element of last row
    linearIndex = param->getIndex({9, 4});
    param->set(linearIndex, -0.5274);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityOld(param, linearIndex), -470.294479533199);
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, _getLogPriorDensityOld_vec_1D){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_1D();
    initializeDefinitions(params, 1);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // we want prior density of first element
    size_t linearIndex = param->getIndex({0, 0});
    double val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.8);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityOld(param, linearIndex), 0.0648083011800458);

    // reset
    param->set(linearIndex, val);

    // we want prior density of third element
    linearIndex = param->getIndex({2, 0});
    val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.5);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityOld(param, linearIndex), -3.33010373611332);

    // reset
    param->set(linearIndex, val);

    // we want prior density of last element
    linearIndex = param->getIndex({9, 0});
    param->set(linearIndex, -0.5274);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityOld(param, linearIndex), 0.32042493516794);
}


TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, _getLogPriorRatio_vec){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // we want prior ratio of first element of first row
    size_t linearIndex = param->getIndex({0, 0});
    double val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.8);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, linearIndex), -40.097848569596);

    // reset
    param->set(linearIndex, val);

    // we want prior ratio of third element of 6th row
    linearIndex = param->getIndex({5, 2});
    val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.5);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, linearIndex), 212.017684803439);

    // reset
    param->set(linearIndex, val);

    // we want prior ratio of last element of last row
    linearIndex = param->getIndex({9, 4});
    param->set(linearIndex, -0.5274);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, linearIndex), -57.6425846064016);
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, _getLogPriorRatio_vec_1D){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_1D();
    initializeDefinitions(params, 1);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // we want prior ratio of first element
    size_t linearIndex = param->getIndex({0, 0});
    double val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.8);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, linearIndex), -12.457698221597);

    // reset
    param->set(linearIndex, val);

    // we want prior ratio of third element
    linearIndex = param->getIndex({2, 0});
    val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.5);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, linearIndex), -11.8406242313563);

    // reset
    param->set(linearIndex, val);

    // we want prior ratio of last element
    linearIndex = param->getIndex({9, 0});
    param->set(linearIndex, -0.5274);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, linearIndex), -0.0371224847760238);
}


TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, _calcLLUpdateMrr){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // change r=0 of Mrr
    size_t r = 0;
    double val = mrr0->value<double>(r);
    mrr0->set(r, 0.62);

    // calculate LL ratio
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMrr(0, r), 118.273990121255);
    // calculate logH
    priorWithHyperPrior->reset();
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMrr(0, r), 118.273990121255 - 0.3199997);

    // reset previous change
    mrr0->set(r, val);
    priorWithHyperPrior->reset();

    // change r=2 of Mrr
    r = 2;
    val = mrr0->value<double>(r);
    mrr0->set(r, 0.9467);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMrr(0, r), -85.5747445153654);
    priorWithHyperPrior->reset();
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMrr(0, r), -85.5747445153654 + 0.02011163);

    // reset previous change
    mrr0->set(r, val);
    priorWithHyperPrior->reset();

    // change r=4 of Mrr
    r = 4;
    val = mrr0->value<double>(r);
    mrr0->set(r, 0.01);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMrr(0, r), -25.7886875003375);
    priorWithHyperPrior->reset();
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMrr(0, r), -25.7886875003375 + 1.857194);
}


TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, _calcLLUpdateMrs){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    mrs0->initializeStorage();

    // change r=1, s=0 of Mrs
    size_t r = 1;
    size_t s = 0;
    size_t linearIndex = priorWithHyperPrior->_linearizeIndex_Mrs(r, s); // = 0
    double val = mrs0->value<double>(linearIndex);
    mrs0->set(linearIndex, 0.62);

    // calculate LL ratio
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMrs(0, r, s), -94.5057244464356);
    priorWithHyperPrior->reset();
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMrs(0, r, s), -94.5057244464356 + 0.9505412);

    // reset previous change
    mrs0->set(linearIndex, val);
    priorWithHyperPrior->reset();

    // change r=2, s=1 of Mrs
    r = 2; s = 1;
    linearIndex = priorWithHyperPrior->_linearizeIndex_Mrs(r, s); // = 2
    val = mrs0->value<double>(linearIndex);
    mrs0->set(linearIndex, 0.9467);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMrs(0, r, s), 789.576061482575);
    priorWithHyperPrior->reset();
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMrs(0, r, s), 789.576061482575 - 0.2551505);

    // reset previous change
    mrs0->set(linearIndex, val);
    priorWithHyperPrior->reset();

    // change r=4, s=3 of Mrs
    r = 4; s = 3;
    linearIndex = priorWithHyperPrior->_linearizeIndex_Mrs(r, s); // = 9
    val = mrs0->value<double>(linearIndex);
    mrs0->set(linearIndex, 0.01);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMrs(0, r, s), 105.390970158065);
    priorWithHyperPrior->reset();
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMrs(0, r, s), 105.390970158065 + 0.1763094);
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, _calcLLUpdateMu){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    mu->initializeStorage();

    // change r=0 of mu
    size_t r = 0;
    double val = mu->value<double>(r);
    mu->set(r, -0.62);

    // calculate LL ratio and logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMu(0, r), 4.88791233064387);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMu(0, r), 4.88791233064387 + 0.004022188);

    // reset previous change
    mu->set(r, val);

    // change r=2 of mu
    r = 2;
    val = mu->value<double>(r);
    mu->set(r, 0.9467);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMu(0, r),  1139.0713108375);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMu(0, r), 1139.0713108375 - 0.09898286);

    // reset previous change
    mu->set(r, val);

    // change r=4 of mu
    r = 4;
    val = mu->value<double>(r);
    mu->set(r, 0.1);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMu(0, r), 146.872678877209);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMu(0, r), 146.872678877209 + 0.04928769);
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, _calcLLUpdateMu_1D){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_1D();
    setPriors();
    initializeDefinitions(params, 1);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    mu->initializeStorage();

    // change r=0 of mu
    size_t r = 0;
    mu->set(r, -0.62);

    // calculate LL ratio and logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMu(0, r),  1.07190993828984);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMu(0, r),  1.07190993828984 + 0.004022188);
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, _calcLLUpdateM){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // change m
    m0->set(0.62);

    // calculate LL ratio
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateM(0), 3797.76514550288);
    priorWithHyperPrior->reset();
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateM(0), 3797.76514550288 - 2.1);
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, _calcLLUpdateM_1D){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_1D();
    setPriors();
    initializeDefinitions(params, 1);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // change m
    m0->set(0.62);

    // calculate LL ratio
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateM(0),  77.6210003363649);
    priorWithHyperPrior->reset();
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateM(0),  77.6210003363649 - 2.1);
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, _calcLLUpdateRho){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    rho->initializeStorage();

    // change r=0 of rho
    size_t r = 0;
    double val = rho->value<double>(r);
    rho->set(r, 0.62);

    // calculate LL ratio and logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateRho(r), -160.86301719483);
    priorWithHyperPrior->resetM1Only();
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateRho(r), -160.86301719483 + 0.376813);

    // reset previous change
    rho->set(r, val);
    priorWithHyperPrior->resetM1Only();

    // change r=2 of rho
    r = 2;
    val = rho->value<double>(r);
    rho->set(r, 0.9467);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateRho(r),  61.2890047941106);
    priorWithHyperPrior->resetM1Only();
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateRho(r), 61.2890047941106 - 0.9094315);

    // reset previous change
    rho->set(r, val);
    priorWithHyperPrior->resetM1Only();

    // change r=4 of rho
    r = 4;
    val = rho->value<double>(r);
    rho->set(r, 0.1);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateRho(r), 1.06426909825359);
    priorWithHyperPrior->resetM1Only();
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateRho(r), 1.06426909825359 + 1.220468);
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, _calcLLUpdateRho_1D){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_1D();
    setPriors();
    initializeDefinitions(params, 1);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    rho->initializeStorage();

    // change r=0 of rho
    size_t r = 0;
    rho->set(r, 0.62);

    // calculate LL ratio and logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateRho(r),  -2.30728689597631);
    priorWithHyperPrior->resetM1Only();
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateRho(r),  -2.30728689597631 + 0.376813);
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, _calcLLUpdateZ){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    z->initializeStorage();

    // change n=0 of z (1->0)
    size_t n = 0;
    bool val = z->value<bool>(n);
    z->set(n, !val);

    // calculate LL ratio and logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateZ(n), -130.440316180821);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateZ(n), -130.440316180821 + 0.8472979);

    // reset previous change
    z->set(n, val);

    // change n=2 of z (0->1)
    n = 2;
    val = z->value<bool>(n);
    z->set(n, !val);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateZ(n),  13.2301368372884);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateZ(n), 13.2301368372884 - 0.8472979);

    // reset previous change
    z->set(n, val);

    // change n=9 of z
    n = 9;
    val = z->value<bool>(n);
    z->set(n, !val);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateZ(n), -467.326213535581);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateZ(n), -467.326213535581 + 0.8472979);
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, _calcLLUpdateZ_1D){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_1D();
    setPriors();
    initializeDefinitions(params, 1);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    z->initializeStorage();

    // change n=0 of z (1->0)
    size_t n = 0;
    bool val = z->value<bool>(n);
    z->set(n, !val);

    // calculate LL ratio and logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateZ(n), 0.0667534114367228);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateZ(n), 0.0667534114367228 + 0.8472979);

    // reset previous change
    z->set(n, val);

    // change n=2 of z (0->1)
    n = 2;
    val = z->value<bool>(n);
    z->set(n, !val);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateZ(n),  1.66131679423661);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateZ(n), 1.66131679423661 - 0.8472979);

    // reset previous change
    z->set(n, val);

    // change n=9 of z
    n = 9;
    val = z->value<bool>(n);
    z->set(n, !val);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateZ(n), 0.321555384987433);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateZ(n), 0.321555384987433 + 0.8472979);
}


TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, getLogPriorDensityFull){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    mrs0->initializeStorage();
    mu->initializeStorage();
    rho->initializeStorage();
    z->initializeStorage();

    // calculate sum of all log prior densities
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityFull(param), -4339.8755233589);
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, getLogPriorDensityFull_1D){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_1D();
    setPriors();
    initializeDefinitions(params, 1);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    mrs0->initializeStorage();
    mu->initializeStorage();
    rho->initializeStorage();
    z->initializeStorage();

    // calculate sum of all log prior densities
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityFull(param), -101.007805817253);
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, simulate){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D(false);
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // create mock random generator and define return values
    MockRandomGenerator mockRandomGenerator;
    EXPECT_CALL(mockRandomGenerator, getNormalRandom)
            .Times(10*5)
                    // first row
            .WillOnce(Return(-1.))
            .WillOnce(Return(-0.5))
            .WillOnce(Return(0.))
            .WillOnce(Return(0.5))
            .WillOnce(Return(1.))
                    // second row
            .WillOnce(Return(-2.))
            .WillOnce(Return(-1.5))
            .WillOnce(Return(-1.))
            .WillOnce(Return(-0.5))
            .WillOnce(Return(-0.))
                    // all other rows
            .WillRepeatedly(Return(0.1));

    TParameters parameters;
    priorWithHyperPrior->simulateUnderPrior(&mockRandomGenerator, &logfile, parameters);

    // go over storage and check if this is expected
    std::vector<double> expected = {-4.59198269197647,1.10370305161226,-1.23213106974757,0.761559856519043,0.888703592588936,-5.83889604790332,0.771511956138138,-2.2349730484247,-0.228831372597752,1.44930301575601,-0.365831698884283,0.173600013489068,-0.733545958220933,1.71306329926365,0.264678451396183,-0.365831698884283,0.173600013489068,-0.733545958220933,1.71306329926365,0.264678451396183,-0.365831698884283,0.173600013489068,-0.733545958220933,1.71306329926365,0.264678451396183,-0.365831698884283,0.173600013489068,-0.733545958220933,1.71306329926365,0.264678451396183,-0.365831698884283,0.173600013489068,-0.733545958220933,1.71306329926365,0.264678451396183,-0.229900922618919,0.13635802385691,-0.72350629934221,1.74394009229107,0.255113373465907,-0.365831698884283,0.173600013489068,-0.733545958220933,1.71306329926365,0.264678451396183,-0.229900922618919,0.13635802385691,-0.72350629934221,1.74394009229107,0.255113373465907};
    size_t linearIndex = 0;
    for (size_t n = 0; n < 10; n++){
        for (size_t d = 0; d < 5; d++, linearIndex++) {
            EXPECT_FLOAT_EQ(param->value<double>(linearIndex), expected[linearIndex]);
        }
    }
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, _setMusToMLE){
    // simulations and true values based on R script 'simulateHMM.R'

    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setPriors();
    initializeDefinitions(params);

    // re-set z and param to have more data points (clearer pattern)
    size_t K = 2;
    defZ->setDimensions({50});
    defZ->setType("uint8_t");
    z = std::make_shared<TMCMCParameter<uint8_t>>(std::move(priorZ), defZ, &randomGenerator);
    params[4] = z;
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    def->setDimensions({50, 5});
    def->setInitVal("3.50187101272656,-0.68559832714638,-3.18068374543844,-8.79362147453702,-13.8746031014148,-1.5686687328185,-0.635178615123832,1.1780869965732,-1.02356680042976,1.59394618762842,-13.3132342155804,9.33895570053379,2.19924803660651,-14.1725002909224,6.21022742648139,-6.05957462114257,12.9303882517041,-2.14579408546869,-1.29556530043387,-0.00190741213561951,-1.16452359625359,-0.753361680136508,0.696963375404737,1.05666319867366,0.31124430545048,-7.20366677224124,-0.0788412685576476,-9.10921648552445,2.08028772404075,-5.54584643918818,-18.3321840682484,-0.478681403197304,-6.30300333928146,-2.90968579860405,-10.5657236263585,-0.524490471100338,-1.20994643092181,0.610726353489055,-0.434097631644252,-0.253633400239102,-1.98178744005234,5.10820728620116,-11.8645863857947,11.4677704427424,0.946559717218343,-4.92807929441984,-3.69992868548507,-2.79113302976559,5.44188331267827,-0.773304822696064,-0.811207700485657,-2.30495862889104,1.46555486156289,0.653253338211898,3.17261167036215,0.358679551529044,-0.602787727342996,0.387671611559369,0.446194959417095,-0.377059556828607,-0.398395595654848,-6.38894486259664,5.31496192632572,-14.6839408178679,4.06557860789766,6.12666307051405,-1.23564404126326,-0.376341714670479,-6.31660478755657,-2.24270272246319,-8.50819001193448,20.3716654562835,0.173956196932517,-12.3630053043433,-15.4060553441858,-2.64375831769667,3.70694643254513,-4.00246743977644,-13.2020787754746,10.8783826745488,-1.056128739529,-0.655795506705329,-1.47075238389927,0.0218499448913796,1.4179415601997,14.1974502549955,-3.58740569225614,-12.5328975560769,6.92241305677824,0.552908631060209,-1.62645381074233,-0.316356675777918,-0.835628612410047,2.09528080213779,1.32950777181536,6.07310667398079,9.84107734737462,2.23480414915304,-8.28707612866019,12.6296455596733,1.40161776050478,-0.539240002733169,0.689739362450777,0.528002158780666,0.256726791117595,3.25100377372448,-2.88647100913033,10.5848304870902,9.36422651374936,-5.19243048231147,-1.34726028311276,7.37639605630162,20.7524500865228,10.7739243876377,13.079083983867,23.9766158983416,6.17066166765493,5.413273359637,0.366004768540913,6.10108422952927,-1.41499456329968,-0.894289953710349,-0.0593133967111857,1.60002537198388,1.76317574845754,-1.82046838411802,-0.0125709475715147,0.738324705129217,1.07578135165349,0.694611612843644,17.0314190791747,-3.81132036391221,-16.0551341225308,2.47193438739481,3.63175646405474,-2.58754604716016,14.145873119698,-7.66081999604665,-3.80211753928547,-8.26109497377437,-0.441513574434696,-1.77659220845804,-0.573265414236886,-0.724612614898356,0.526599363560688,-11.4798441280774,13.9115770684428,-10.1584746530465,4.61974712317515,-2.8107605110892,16.6728726937265,6.66707476017206,9.10174229495227,4.34185357826345,17.8217608051942,-0.0810226283917818,0.282136300731067,0.0745649833651906,-1.48935169586337,1.61982574789471,-1.70749515696212,-0.13541803786317,0.768532924515416,0.387653787849772,1.88110772645421,0.98039989850586,-0.867221476466509,-1.04413462631653,1.06971962744241,0.864945396119176,-1.04493360901523,-0.516190263098946,0.943836210685299,1.32122119509809,1.59390132121751,-2.7710396143654,3.52011779486338,-7.31748173119606,8.80373167981674,-11.0808278630447,-7.35736453948977,-5.11644730360566,14.3228223854166,-6.00696353310367,-1.07380743601965,-16.3644982353759,-3.50976126836611,-5.28279904445006,-6.02094780680999,0.431032221526075,-10.8582670040929,-29.3892067167954,-6.40481702565115,6.20507635920485,0.402767239573899,-11.7519229661568,9.50028803713914,-6.21266694796823,-13.3442684738449,19.6929062242358,-1.54252003099165,0.707867805983172,1.16040261569495,1.200213649515,2.58683345454085,3.09401839650934,16.3887328620405,15.8658843344197,-2.80907800682766,-21.8523553529247,-0.667049628786482,0.563099837276363,-0.304183923634301,0.870018809916288,1.26709879077223,-20.1435942568001,11.2658331201856,-16.64972436212,-4.13530401472386,-10.1592010504285,-0.601894119632932,-1.11202639325077,0.341119691424425,-0.629363096080793,2.43302370170104,-0.708553764482537,-0.943291873218433,0.00110535163162413,0.574341324151664,0.410479053811928,-21.0016494478548,-5.94790740001725,-2.55670709156989,-1.16121036765006,11.2046390878411,22.0797839905936,0.558023678937115,4.56998805423414,-0.27152935356531,-2.34000842366545,21.0610246454047,-3.05027030141015,-14.2449465021281,-0.943996019542194,3.07538339232345,0.511781168450848,-0.110156763588569,-0.621240580541804,-1.7146998871775,2.12493091814311");
    param = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
    param->handPointerToPrior(param);
    param->initializeStorageBasedOnPrior({50, 5}, {dimNames, dimNames});
    param->initializeStorage();
    mrs0->initializeStorage();
    mu->initializeStorage();
    rho->initializeStorage();

    // calculate MLE mu
    priorWithHyperPrior->_setMusToMLE();

    std::vector<double> expected = {-0.9099094,  1.3820622, -1.0033525, -0.8155223,  0.4952181};
    for (size_t d = 0; d < 5; d++){
        EXPECT_FLOAT_EQ(mu->value<double>(d), expected[d]);
    }
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, estimateInitialEMParameters){
    // simulations and true values based on R script 'simulateHMM.R'

    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setPriors();
    initializeDefinitions(params);

    // re-set z and param to have more data points (clearer pattern)
    size_t K = 2;
    size_t N = 100;
    defZ->setDimensions({N});
    defZ->setType("uint8_t");
    z = std::make_shared<TMCMCParameter<uint8_t>>(std::move(priorZ), defZ, &randomGenerator);
    params[4] = z;
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    def->setDimensions({N, 5});
    def->setInitVal("-4.50668131363331,0.472611065421362,-3.5003076616815,1.59937736714571,-1.76150493064403,-0.476379409501983,0.51775422653797,-0.251164588087978,-0.92999344738861,2.70912103210088,-0.549812898727345,-0.518559832714638,-0.318068374543844,-0.429362147453702,-0.487460310141485,-0.237413487581682,0.611431080730634,-0.923206952829831,0.664341838427956,2.15482518709727,0.511781168450848,-0.110156763588569,-0.621240580541804,-1.7146998871775,2.12493091814311,-2.04798441280774,0.941157706844281,-1.01584746530465,0.911974712317515,0.61892394889108,-1.39280792944198,-0.819992868548507,-0.279113302976559,0.994188331267827,0.822669517730394,-2.43262905201625,6.347496439329,3.93939687346042,2.38313012981247,1.01544598565683,-1.70749515696212,-0.13541803786317,0.768532924515416,0.387653787849772,1.88110772645421,-2.12987818560255,-4.12654472301241,-1.63622723618163,-0.645135916240166,8.43310543345503,0.51221269395063,-0.417034266414086,0.5672209148515,-0.52454847953446,1.32300650302244,1.49766158983416,0.167066166765493,0.5413273359637,0.486600476854091,1.51010842295293,-2.40885045607319,0.416019328792574,-0.1912789505352,1.30328321613365,2.8874744633086,-0.387781826010872,0.1783401772227,0.567951972471672,-0.0725426039261257,-0.36329125627834,-1.54252003099165,0.707867805983172,1.16040261569495,1.200213649515,2.58683345454085,-1.37565742282039,-0.866630945702358,-0.295677452700886,1.94182041019987,0.302461708086959,-1.25893258311878,-0.105620831778428,-0.851857092023863,3.14916688109488,1.15601167566508,0.803141907917467,-0.831132036391221,-1.60551341225308,0.697193438739481,1.26317564640547,-1.63573645394898,-0.961644730360566,1.43228223854166,-0.150696353310367,0.792619256398035,0.324004321299602,0.115636849302674,1.09166895553536,0.806604861513632,0.889841237517143,-1.43383274002959,1.2726111849785,-0.0182597111630101,1.35281499360053,1.20516290324403,-2.91435942568001,0.676583312018562,-1.664972436212,0.0364695985276144,-0.115920105042845,-0.260410774425204,-1.56345741548281,0.2462108435364,0.210500633435688,-1.26488935648794,0.307901520114146,0.997041009402783,0.814702730897356,-1.36978879020261,1.48202950412376,1.40161776050478,-0.539240002733169,0.689739362450777,0.528002158780666,0.256726791117595,0.767287269372646,0.216707476017206,0.910174229495227,0.884185357826345,2.68217608051942,-2.00107218102542,-1.16817860675339,0.945184953373082,0.933702149545162,2.00515921767704,-1.19819542148464,0.392008392473526,-0.0257150709181537,-0.147660450585665,1.64635941503464,-0.590598160349066,1.18887328620405,1.58658843344197,0.169092199317234,-1.28523553529247,-0.116583639779486,-2.73227117010984,1.98596461299926,5.18084753824839,4.4261062010146,-1.3887222443379,-0.222085867549457,-0.823081121572025,0.431159065521535,-0.167662326129799,-0.574899622627552,-0.738647100913033,1.05848304870902,1.38642265137494,0.380756951768853,-1.41499456329968,-0.894289953710349,-0.0593133967111857,1.60002537198388,1.76317574845754,0.98039989850586,-0.867221476466509,-1.04413462631653,1.06971962744241,0.864945396119176,-0.708553764482537,-0.943291873218433,0.00110535163162413,0.574341324151664,0.410479053811928,-1.16437583176967,-0.0793053567454872,-0.400246743977644,-0.87020787754746,1.98783826745488,-1.62036667722412,-0.457884126855765,-0.910921648552446,0.658028772404075,0.345415356081182,-1.056128739529,-0.655795506705329,-1.47075238389927,0.0218499448913796,1.4179415601997,0.473881181109138,0.177268492312998,0.379962686566745,0.307201573542666,2.5778917949044,-0.287333692948595,-0.573564404126326,-0.0376341714670479,-0.181660478755657,0.675729727753681,-0.865552339066324,0.265598999157864,0.955136676908982,0.44943429855773,0.694184580233029,-0.267249957790898,0.446585640197786,0.00439870432637403,0.147677694450151,0.470304490866496,0.0691614606767963,-0.983974930301277,-0.121010111327444,-0.794140003820841,1.49431283601486,0.358679551529044,-0.602787727342996,0.387671611559369,0.446194959417095,-0.377059556828607,1.30797839905936,-0.394197632106289,0.456998805423414,0.422847064643469,0.665999157633456,-1.92431277312728,1.0929137537192,0.0450105981218803,-0.215128400667883,1.86522309971714,-1.05652142452649,-2.62936064823465,0.344845762099456,-1.40495544558553,0.188829846859783,-1.75081900119345,1.58716654562835,0.0173956196932517,-0.786300530434326,-0.640605534418578,-0.0810226283917818,0.282136300731067,0.0745649833651906,-1.48935169586337,1.61982574789471,0.146228357215801,-2.90309621489187,0.572739555245841,0.874724406778655,0.574732278443924,-1.04493360901523,-0.516190263098946,0.943836210685299,1.32122119509809,1.59390132121751,-0.601894119632932,-1.11202639325077,0.341119691424425,-0.629363096080793,2.43302370170104,-0.403765890681546,-1.67357694087136,-0.155642534890318,-1.41890982026984,0.804741153889363,-1.82046838411802,-0.0125709475715147,0.738324705129217,1.07578135165349,0.694611612843644,-0.821863093288871,1.11076749549807,-6.63408841737381,-2.67607145033512,2.69425894822931,1.20610246454047,-0.755027030141015,-1.42449465021281,0.355600398045781,1.20753833923234,-1.98582670040929,-3.38892067167955,-0.640481702565115,1.07050763592048,0.94027672395739,-1.3271010146531,-2.06908218514395,-0.367450756170482,1.86443492906985,0.665718635268836,0.0436124583561828,-0.400921513102802,-0.45413690915436,-0.15578185245044,0.964077577377489,-1.16452359625359,-0.753361680136508,0.696963375404737,1.05666319867366,0.31124430545048,-1.17710396143654,-0.0979882205136621,-0.731748173119606,1.33037316798167,-0.208082786304465,0.612346798201784,-2.13728064710472,-0.779568513201747,-0.141176933750564,0.318868606428844,-0.863778106897222,-0.0928323965761635,-0.0696548130129049,0.252335658380669,1.69555080661964,-1.5686687328185,-0.635178615123832,1.1780869965732,-1.02356680042976,1.59394618762842,-1.09817874400523,0.0608207286201161,-1.18645863857947,1.59677704427424,0.994655971721834,-1.50595746211426,0.843038825170412,-0.214579408546869,0.320443469956613,0.899809258786438,-1.00830901421561,-0.371144598402595,-0.145875628461003,0.336089043263932,2.76355200278492,2.04115177959897,-2.41136199539877,-2.38080700137769,-4.41927578700162,-3.59761568793944,-5.16980931542663,0.652285256912088,0.738391312153421,4.27561804992,0.911740918638715,-1.39011866405368,-0.123629708225352,0.244164924486494,-0.926257342382538,2.77842928747545,0.519745025499545,-0.808740569225614,-1.25328975560769,1.14224130567782,0.955290863106021,0.435069572311625,-1.21037114584988,-0.0650675736555838,-1.25946873536593,1.5697229718191,-0.106326297574487,-1.5472981490613,1.97133738622415,0.116367893711638,2.65414530227499,-3.59232766994599,0.814002167198053,-0.635543001032135,0.070021161305812,0.830681667698037,-1.03472602831128,0.287639605630162,2.07524500865228,1.52739243876377,2.2079083983867,-0.543864396698798,-0.853400285829911,0.170489470947982,-0.364035954126904,1.67923077401566,-0.601869844548044,-0.907528579269772,1.32425863017727,-0.20123166924692,0.419385695759464,-1.15875460471602,0.964587311969797,-0.766081999604665,0.0697882460714535,0.0738905026225628,-4.00804859892048,-1.86611193132898,-0.424102260144812,0.736803663745558,-1.34272312035896,-0.939839559565485,-1.08889448625966,0.531496192632572,-1.01839408178679,1.30655786078977,-0.729945099062771,-0.92218400978764,-1.18911329485959,0.16896702112099,0.0601706734899786,-0.441513574434696,-1.77659220845804,-0.573265414236886,-0.724612614898356,0.526599363560688,-0.0489871924231836,-0.889237181718379,-0.284330661799574,1.3574097780798,2.7196272991206,-2.53644982353759,-0.800976126836611,-0.528279904445006,-0.152094780680999,0.943103222152607,-3.00016494478548,-1.04479074000172,-0.255670709156989,0.333878963234994,2.02046390878411,6.73661151427699,-1.02716205658993,-3.30036622719055,-5.73896806699011,2.62753460874345,-2.23132342155804,0.483895570053379,0.219924803660651,-0.967250029092243,1.52102274264814,-2.07519229661568,0.500028803713914,-0.621266694796823,-0.884426847384491,2.86929062242358,-4.44869648061451,6.7247271462643,-2.80056866546881,0.851349380685964,13.0491528264719,0.0744409582779002,1.39565477419858,-0.602997303605094,0.109132179276863,0.583777968469808,-0.292689332601921,0.534107734737462,0.223480414915304,-0.378707612866019,2.16296455596733,-3.03328559561795,0.000963559247906698,-1.53179813996574,0.475002360721737,1.59298472102529,-1.62645381074233,-0.316356675777918,-0.835628612410047,2.09528080213779,1.32950777181536,-0.667049628786482,0.563099837276363,-0.304183923634301,0.870018809916288,1.26709879077223,-2.73321840682484,-0.49786814031973,-0.630300333928146,0.159031420139595,-0.156572362635853,-0.811207700485657,-2.30495862889104,1.46555486156289,0.653253338211898,3.17261167036215,-3.57170414510909,-5.61933809486353,-0.346761768801802,1.89421460825645,5.2722180813707,-0.524490471100338,-1.20994643092181,0.610726353489055,-0.434097631644252,-0.253633400239102,0.130207267454943,-2.78912397984011,0.741001157195439,-0.816245160451562,1.91980367760914,-1.38816750592136,0.152536452171517,1.12477244653513,-0.272110803023928,0.491913783861713");
    param = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
    param->handPointerToPrior(param);
    param->initializeStorageBasedOnPrior({N, 5}, {dimNames, dimNames});
    param->initializeStorage();
    mrs0->initializeStorage();
    mu->initializeStorage();
    rho->initializeStorage();

    priorWithHyperPrior->initializeEMParameters();


    std::vector<size_t > expectedZ = {1,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,1,0,0,0,0,0,0,0,1,0,0,0};
    for (size_t i = 0; i < z->totalSize(); i++){
        EXPECT_EQ(z->value<size_t>(i), expectedZ[i]);
    }

    // define expectations
    std::vector<double> mus = {-0.906038381437916,-0.377143277693602,-0.133143115400586,0.253685694198566,1.32243317540003};
    std::vector<std::vector<double>> M0 = {{0.965802522632954, 0, 0, 0, 0},
                                           {0.180166027458101, 1.03702524582431, 0, 0, 0},
                                           {-0.158163183928821, 0.155344922297222, 1.23593883937097, 0, 0},
                                           {0.126158793282841, 0.00047403522, 0.0998037300808922, 1.08524008140326, 0},
                                           {0.027006035849962, -0.114923013965249, -0.169824609130338, 0.049755568574062, 1.03801048996879}};
    std::vector<std::vector<double>> Sigma0 = {{1.14681861840939, -0.195004178062831, 0.133469539722875, -0.123319901496212, -0.014477337648415},
                                               {-0.195004178062831, 0.952141227668795, -0.0871152402464683, 0.00606573420950888, 0.0832537980406304},
                                               {0.133469539722875, -0.0871152402464683, 0.678661741520376, -0.074568620708783, 0.130962488601776},
                                               {-0.123319901496212, 0.00606573420950888, -0.074568620708783, 0.851030357829063, -0.0425512559451544},
                                               {-0.014477337648415, 0.0832537980406304, 0.130962488601776, -0.0425512559451544, 0.928103723646395}};
    std::vector<std::vector<double>> M1 = {{0.520860997818386, 0, 0, 0, 0},
                                           {0.0723031778613533, 0.283128703661081, 0, 0, 0},
                                           {-0.289230792791949, -0.0274446742287846, 0.457571308810008, 0, 0},
                                           {0.547973328600461, -0.0126362748572851, -0.275314686642822, 0.323492763847089, 0},
                                           {-0.0169582702999429, -0.0391761044101289, 0.0843593173120065, -0.0317872854725262, 0.2128865152413}};
    std::vector<std::vector<double>> Sigma1 = {{11.0064297400177, -2.65799256693268, -1.18416162390536, -7.34076444801027, -3.49720097035805},
                                               {-2.65799256693268, 12.9973564738482, 0.693985411313888, 1.26701451606762, 2.88199954591638},
                                               {-1.18416162390536, 0.693985411313888, 8.5817784634488, 5.47810988453714, -2.76341479576466},
                                               {-7.34076444801027, 1.26701451606762, 5.47810988453714, 9.76893372882951, 2.16816617941564},
                                               {-3.49720097035805, 2.88199954591638, -2.76341479576466, 2.16816617941564, 22.0649879167933}};

    // m's
    EXPECT_FLOAT_EQ(m0->value<double>(), 1.);

    for (size_t d = 0; d < 5; d++){
        for (size_t e = 0; e < 5; e++){
            if (d == e){
                EXPECT_FLOAT_EQ(mrr0->value<double>(d), M0[d][d]);
                EXPECT_FLOAT_EQ(mu->value<double>(d), mus[d]);
            } else if (d > e) {
                EXPECT_FLOAT_EQ(mrs0->value<double>(d * (d-1) / 2 + e), M0[d][e]);
            }
            EXPECT_FLOAT_EQ(priorWithHyperPrior->getM1()(d, e), M1[d][e]);
            EXPECT_FLOAT_EQ(priorWithHyperPrior->getSigma1()(d, e), Sigma1[d][e]);
        }
    }
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, _setMToMLE) {
    // simulations and true values based on R script 'simulateHMM.R'

    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setPriors();
    initializeDefinitions(params);

    // re-set z to have more data points (clearer pattern)
    defZ->setDimensions({50});
    defZ->setType("uint8_t");
    defZ->setInitVal("1,0,1,1,0,1,1,0,1,1,0,0,1,1,1,1,0,1,0,1,0,1,1,1,0,0,1,1,0,1,1,0,0,0,0,1,1,1,1,1,0,1,0,1,0,0,1,1,1,0");
    z = std::make_shared<TMCMCParameter<uint8_t>>(std::move(priorZ), defZ, &randomGenerator);
    params[4] = z;
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    def->setDimensions({50, 5});
    def->setInitVal("3.50187101272656,-0.68559832714638,-3.18068374543844,-8.79362147453702,-13.8746031014148,-1.5686687328185,-0.635178615123832,1.1780869965732,-1.02356680042976,1.59394618762842,-13.3132342155804,9.33895570053379,2.19924803660651,-14.1725002909224,6.21022742648139,-6.05957462114257,12.9303882517041,-2.14579408546869,-1.29556530043387,-0.00190741213561951,-1.16452359625359,-0.753361680136508,0.696963375404737,1.05666319867366,0.31124430545048,-7.20366677224124,-0.0788412685576476,-9.10921648552445,2.08028772404075,-5.54584643918818,-18.3321840682484,-0.478681403197304,-6.30300333928146,-2.90968579860405,-10.5657236263585,-0.524490471100338,-1.20994643092181,0.610726353489055,-0.434097631644252,-0.253633400239102,-1.98178744005234,5.10820728620116,-11.8645863857947,11.4677704427424,0.946559717218343,-4.92807929441984,-3.69992868548507,-2.79113302976559,5.44188331267827,-0.773304822696064,-0.811207700485657,-2.30495862889104,1.46555486156289,0.653253338211898,3.17261167036215,0.358679551529044,-0.602787727342996,0.387671611559369,0.446194959417095,-0.377059556828607,-0.398395595654848,-6.38894486259664,5.31496192632572,-14.6839408178679,4.06557860789766,6.12666307051405,-1.23564404126326,-0.376341714670479,-6.31660478755657,-2.24270272246319,-8.50819001193448,20.3716654562835,0.173956196932517,-12.3630053043433,-15.4060553441858,-2.64375831769667,3.70694643254513,-4.00246743977644,-13.2020787754746,10.8783826745488,-1.056128739529,-0.655795506705329,-1.47075238389927,0.0218499448913796,1.4179415601997,14.1974502549955,-3.58740569225614,-12.5328975560769,6.92241305677824,0.552908631060209,-1.62645381074233,-0.316356675777918,-0.835628612410047,2.09528080213779,1.32950777181536,6.07310667398079,9.84107734737462,2.23480414915304,-8.28707612866019,12.6296455596733,1.40161776050478,-0.539240002733169,0.689739362450777,0.528002158780666,0.256726791117595,3.25100377372448,-2.88647100913033,10.5848304870902,9.36422651374936,-5.19243048231147,-1.34726028311276,7.37639605630162,20.7524500865228,10.7739243876377,13.079083983867,23.9766158983416,6.17066166765493,5.413273359637,0.366004768540913,6.10108422952927,-1.41499456329968,-0.894289953710349,-0.0593133967111857,1.60002537198388,1.76317574845754,-1.82046838411802,-0.0125709475715147,0.738324705129217,1.07578135165349,0.694611612843644,17.0314190791747,-3.81132036391221,-16.0551341225308,2.47193438739481,3.63175646405474,-2.58754604716016,14.145873119698,-7.66081999604665,-3.80211753928547,-8.26109497377437,-0.441513574434696,-1.77659220845804,-0.573265414236886,-0.724612614898356,0.526599363560688,-11.4798441280774,13.9115770684428,-10.1584746530465,4.61974712317515,-2.8107605110892,16.6728726937265,6.66707476017206,9.10174229495227,4.34185357826345,17.8217608051942,-0.0810226283917818,0.282136300731067,0.0745649833651906,-1.48935169586337,1.61982574789471,-1.70749515696212,-0.13541803786317,0.768532924515416,0.387653787849772,1.88110772645421,0.98039989850586,-0.867221476466509,-1.04413462631653,1.06971962744241,0.864945396119176,-1.04493360901523,-0.516190263098946,0.943836210685299,1.32122119509809,1.59390132121751,-2.7710396143654,3.52011779486338,-7.31748173119606,8.80373167981674,-11.0808278630447,-7.35736453948977,-5.11644730360566,14.3228223854166,-6.00696353310367,-1.07380743601965,-16.3644982353759,-3.50976126836611,-5.28279904445006,-6.02094780680999,0.431032221526075,-10.8582670040929,-29.3892067167954,-6.40481702565115,6.20507635920485,0.402767239573899,-11.7519229661568,9.50028803713914,-6.21266694796823,-13.3442684738449,19.6929062242358,-1.54252003099165,0.707867805983172,1.16040261569495,1.200213649515,2.58683345454085,3.09401839650934,16.3887328620405,15.8658843344197,-2.80907800682766,-21.8523553529247,-0.667049628786482,0.563099837276363,-0.304183923634301,0.870018809916288,1.26709879077223,-20.1435942568001,11.2658331201856,-16.64972436212,-4.13530401472386,-10.1592010504285,-0.601894119632932,-1.11202639325077,0.341119691424425,-0.629363096080793,2.43302370170104,-0.708553764482537,-0.943291873218433,0.00110535163162413,0.574341324151664,0.410479053811928,-21.0016494478548,-5.94790740001725,-2.55670709156989,-1.16121036765006,11.2046390878411,22.0797839905936,0.558023678937115,4.56998805423414,-0.27152935356531,-2.34000842366545,21.0610246454047,-3.05027030141015,-14.2449465021281,-0.943996019542194,3.07538339232345,0.511781168450848,-0.110156763588569,-0.621240580541804,-1.7146998871775,2.12493091814311");
    param = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
    param->handPointerToPrior(param);
    param->initializeStorageBasedOnPrior({50, 5}, {dimNames, dimNames});
    param->initializeStorage();
    mrs0->initializeStorage();
    mu->initializeStorage();
    rho->initializeStorage();

    // calculate mean and variance based on z and param
    priorWithHyperPrior->_setMusToMLE();
    priorWithHyperPrior->_setMToMLE();
    std::vector<double> expectedMu = {-0.9099094,  1.3820622, -1.0033525, -0.8155223,  0.4952181};
    std::vector<double> expectedMrr0 = {1.25845933372183,0.870161135987401,1.03309426781994,0.724801552486324,0.831860778845982};
    std::vector<double> expectedMrs0 = {0.4760688,  0.1710598, 0.6714555, 0.2538392, 0.3350229, -0.4597982, 0.2920597, 0.2051382, -0.4922499, -0.4149332};

    // cannot check rho yet, because EM ignores rho
    // instead: check M1 and Sigma1
    std::vector<std::vector<double>> M1 = {{0.084553685,  0.00000000,  0.000000000, 0.000000000, 0.0000000},
                                           {-0.003680969,  0.11107604,  0.000000000, 0.000000000, 0.0000000},
                                           {-0.015334993, -0.01372092,  0.107748514, 0.000000000, 0.0000000},
                                           {-0.033495583,  0.03435236,  0.004264703, 0.130916020, 0.0000000},
                                           {-0.015572832,  0.01948826, -0.011338385, 0.004454872, 0.10359625}};
    std::vector<std::vector<double>> Sigma1 = {{154.383097,  -3.957343, 17.039686,  21.315315,  17.090567},
                                           {-3.957343,  90.270003,  9.858398, -17.848821, -14.140706},
                                           {17.039686,   9.858398, 87.284334,  -2.647284,   9.930593},
                                           {21.315315, -17.848821, -2.647284,  58.454341,  -3.170694},
                                           {17.090567, -14.140706,  9.930593,  -3.170694,  93.177696}};

    // m's
    EXPECT_FLOAT_EQ(m0->value<double>(), 1.);

    for (size_t d = 0; d < 5; d++){
        // mean
        EXPECT_FLOAT_EQ(mu->value<double>(d), expectedMu[d]);
        // Mrr0
        EXPECT_FLOAT_EQ(mrr0->value<double>(d), expectedMrr0[d]);

        // M1 and Sigma1
        for (size_t e = 0; e < 5; e++){
            EXPECT_FLOAT_EQ(priorWithHyperPrior->getM1()(d, e), M1[d][e]);
            EXPECT_FLOAT_EQ(priorWithHyperPrior->getSigma1()(d, e), Sigma1[d][e]);
        }
    }

    for (size_t d = 0; d < 10; d++){
        // Mrs0
        EXPECT_FLOAT_EQ(mrs0->value<double>(d), expectedMrs0[d]);
    }
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, estimateInitialPriorParameters_dontSwitch) {
    // simulations and true values based on R script 'simulateHMM.R'

    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    mrs0->initializeStorage();
    mu->initializeStorage();
    rho->initializeStorage();
    z->initializeStorage();

    // set values for mu, m, Mrr, Mrs as well as M1 and Sigma1 (pretend that EM finished)
    std::vector<double> valsMu = {-0.9099094,  1.3820622, -1.0033525, -0.8155223,  0.4952181};
    std::vector<double> valsMrr0 = {1.25845933372183,0.870161135987401,1.03309426781994,0.724801552486324,0.831860778845982};
    std::vector<double> valsMrs0 = {0.476068806134040,  0.171059770886838, 0.671455454926952, 0.253839232896260, 0.335022947604300, -0.459798169844298, 0.292059674177830, 0.205138213619484, -0.492249879652779, -0.414933186478483};
    std::vector<uint8_t > valsZ = {1,0,1,1,0,1,1,0,1,1};

    arma::mat M0 = {{1.2584593, 0.0000000,  0.0000000,  0.0000000, 0.0000000},
                    {0.4760688, 0.8701611,  0.0000000,  0.0000000, 0.0000000},
                    {0.1710598, 0.6714555,  1.0330943,  0.0000000, 0.0000000},
                    {0.2538392, 0.3350229, -0.4597982,  0.7248016, 0.0000000},
                    {0.2920597, 0.2051382, -0.4922499, -0.4149332, 0.8318608}};
    arma::mat M1 = {{0.084553685,  0.00000000,  0.000000000, 0.000000000, 0.0000000},
                    {-0.003680969,  0.11107604,  0.000000000, 0.000000000, 0.0000000},
                    {-0.015334993, -0.01372092,  0.107748514, 0.000000000, 0.0000000},
                    {-0.033495583,  0.03435236,  0.004264703, 0.130916020, 0.0000000},
                    {-0.015572832,  0.01948826, -0.011338385, 0.004454872, 0.10359625}};
    arma::mat Sigma0 = {{0.84892462, -0.5410777 , 0.09309022, -0.0249604346814075, -0.0880386075435555},
                        {-0.54107771,  4.3934628, -2.41851195, -2.23083023, -1.47464370},
                        { 0.09309022, -2.4185119,  2.08680731,  1.45218123,  1.05676516},
                        {-0.0249604346814075, -2.2308302,  1.45218123,  2.37714455,  0.82728997},
                        {-0.0880386075435555, -1.4746437,  1.05676516,  0.82728997,  1.44510267}};
    arma::mat Sigma1 = {{154.383097,  -3.957343, 17.039686,  21.315315,  17.090567},
                        {-3.957343,  90.270003,  9.858398, -17.848821, -14.140706},
                        {17.039686,   9.858398, 87.284334,  -2.647284,   9.930593},
                        {21.315315, -17.848821, -2.647284,  58.454341,  -3.170694},
                        {17.090567, -14.140706,  9.930593,  -3.170694,  93.177696}};


    m0->set(1.);
    for (size_t d = 0; d < 5; d++){
        mu->set(d, valsMu[d]);
        mrr0->set(d, valsMrr0[d]);
    }
    for (size_t d = 0; d < 10; d++){
        mrs0->set(d, valsMrs0[d]);
    }
    for (size_t i = 0; i < 10; i++){
        z->set(i, valsZ[i]);
    }
    priorWithHyperPrior->setM1(M1);
    priorWithHyperPrior->setSigma1(Sigma1);

    // initialize prior parameters - Sigma1 is larger than Sigma0, so no labels should be switched
    priorWithHyperPrior->_setAllParametersAfterEM(&logfile);

    // check if mu is correctly initialized (i.e. still the same)
    for (size_t d = 0; d < 5; d++) {
        EXPECT_FLOAT_EQ(mu->value<double>(d), valsMu[d]);
    }

    // check if z are correclty initialized (i.e. still the same)
    for (size_t i = 0; i < 10; i++) {
        EXPECT_FLOAT_EQ(z->value<uint8_t>(i), valsZ[i]);
    }

    // check if M0 is correctly initialized (i.e. still the same)
    EXPECT_FLOAT_EQ(m0->value<double>(), 1.);
    for (size_t d = 0; d < 5; d++){
        // Mrr0
        EXPECT_FLOAT_EQ(mrr0->value<double>(d), valsMrr0[d]);
        // M0 and Sigma0
        for (size_t e = 0; e < 5; e++){
            EXPECT_FLOAT_EQ(priorWithHyperPrior->getM0()(d, e), M0(d, e));
            EXPECT_FLOAT_EQ(priorWithHyperPrior->getSigma0()(d, e), Sigma0(d, e));
        }
    }
    for (size_t d = 0; d < 10; d++){
        // Mrs0
        EXPECT_FLOAT_EQ(mrs0->value<double>(d), valsMrs0[d]);
    }

    arma::mat newSigma1 = {{77.898558, -23.47372,  -7.328659, -11.198468, -11.985875},
                        {-23.473723, 118.24410, -36.076071, -26.705525, -19.304496},
                        {-7.328659, -36.07607,  87.875695,  15.149112,  14.761750},
                        {-11.198468, -26.70553,  15.149112, 107.587491,   4.828746},
                        {-11.985875, -19.30450,  14.761750,   4.828746,  91.963623}};
    arma::mat newM1 = {{0.12315897, 0.00000000,  0.00000000,  0.000000000, 0.0000000},
                        {0.03776554, 0.10121991,  0.00000000,  0.000000000, 0.0000000},
                        {0.0192243, 0.03577007,  0.10938194,  0.000000000 ,0.0000000},
                        {0.01859263, 0.01943814, -0.01464828,  0.096523142, 0.0000000},
                        {0.01991711, 0.01448517, -0.01678855, -0.005068154, 0.1042778}};

    // check if M1 is correctly initialized (not the same anymore, as it is adjusted to match rho)
    for (size_t d = 0; d < 5; d++){
        for (size_t e = 0; e < 5; e++){
            EXPECT_FLOAT_EQ(priorWithHyperPrior->getM1()(d, e), newM1(d, e));
            EXPECT_FLOAT_EQ(priorWithHyperPrior->getSigma1()(d, e), newSigma1(d, e));
        }
    }

    // check if rho is correctly initialized
    std::vector<double> expectedRho = {109.98731, 129.57167,  92.42899,  85.77360 , 20.02343};
    for (size_t d = 0; d < 5; d++) {
        EXPECT_FLOAT_EQ(rho->value<double>(d), expectedRho[d]);
    }
}


TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, estimateInitialPriorParameters_doSwitch) {
    // simulations and true values based on R script 'simulateHMM.R'

    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    mrs0->initializeStorage();
    mu->initializeStorage();
    rho->initializeStorage();
    z->initializeStorage();

    // set values for mu, m, Mrr, Mrs as well as M1 and Sigma1 (pretend that EM finished) - choose other solution than above
    std::vector<double> valsMu = {-0.9099094,  1.3820622, -1.0033525, -0.8155223,  0.4952181};
    std::vector<double> valsMrr0 = {0.0845536848148294,0.111076042660194,0.107748513669999,0.130916019591296,0.103596245528932};
    std::vector<double> valsMrs0 = {-0.00368096942135846, -0.01533499298712761, -0.0137209198395793, -0.03349558251678762,  0.0343523604389143,  0.00426470319765408, -0.01557283179137137 , 0.0194882623985414, -0.01133838480014017, 0.00445487150593236};
    std::vector<uint8_t > valsZ = {0, 1, 0, 0, 1, 0, 0, 1, 0, 0};

    arma::mat M0 = {{0.084553685,  0.00000000,  0.000000000, 0.000000000, 0.0000000},
                    {-0.003680969,  0.11107604,  0.000000000, 0.000000000, 0.0000000},
                    {-0.015334993, -0.01372092,  0.107748514, 0.000000000, 0.0000000},
                    {-0.033495583,  0.03435236,  0.004264703, 0.130916020, 0.0000000},
                    {-0.015572832,  0.01948826, -0.011338385, 0.004454872, 0.10359625}};
    arma::mat M1 = {{1.2584593, 0.0000000,  0.0000000,  0.0000000, 0.0000000},
                    {0.4760688, 0.8701611,  0.0000000,  0.0000000, 0.0000000},
                    {0.171059770886838, 0.6714555,  1.0330943,  0.0000000, 0.0000000},
                    {0.2538392, 0.3350229, -0.4597982,  0.7248016, 0.0000000},
                    {0.2920597, 0.2051382, -0.4922499, -0.4149332, 0.8318608}};
    arma::mat Sigma0 = {{154.383097,  -3.957343, 17.039686,  21.315315,  17.090567},
                        {-3.957343,  90.270003,  9.858398, -17.848821, -14.140706},
                        {17.039686,   9.858398, 87.284334,  -2.647284,   9.930593},
                        {21.315315, -17.848821, -2.647284,  58.454341,  -3.170694},
                        {17.090567, -14.140706,  9.930593,  -3.170694,  93.177696}};
    arma::mat Sigma1 = {{0.84892462, -0.5410777 , 0.09309022, -0.0249604346814075, -0.0880386075435555},
                        {-0.54107771,  4.3934628, -2.41851195, -2.23083023, -1.47464370},
                        { 0.09309022, -2.4185119,  2.08680731,  1.45218123,  1.05676516},
                        {-0.0249604346814075, -2.2308302,  1.45218123,  2.37714455,  0.82728997},
                        {-0.0880386075435555, -1.4746437,  1.05676516,  0.82728997,  1.44510267}};



    m0->set(1.);
    for (size_t d = 0; d < 5; d++){
        mu->set(d, valsMu[d]);
        mrr0->set(d, valsMrr0[d]);
    }
    for (size_t d = 0; d < 10; d++){
        mrs0->set(d, valsMrs0[d]);
    }
    for (size_t i = 0; i < 10; i++){
        z->set(i, valsZ[i]);
    }
    priorWithHyperPrior->setM1(M1);
    priorWithHyperPrior->setSigma1(Sigma1);

    // initialize prior parameters
    priorWithHyperPrior->_setAllParametersAfterEM(&logfile);

    // check if mu is correctly initialized (i.e. still the same)
    for (size_t d = 0; d < 5; d++) {
        EXPECT_FLOAT_EQ(mu->value<double>(d), valsMu[d]);
    }

    // check if z are correctly initialized (i.e. always the opposite)
    for (size_t i = 0; i < 10; i++) {
        EXPECT_FLOAT_EQ(z->value<uint8_t>(i), 1-valsZ[i]);
    }

    // check if M0 is correctly initialized (i.e. what was M1 before)
    EXPECT_FLOAT_EQ(m0->value<double>(), 1.);
    for (size_t d = 0; d < 5; d++){
        // Mrr0
        EXPECT_FLOAT_EQ(mrr0->value<double>(d), M1(d, d));
        // M0 and Sigma0
        for (size_t e = 0; e < 5; e++){
            EXPECT_FLOAT_EQ(priorWithHyperPrior->getM0()(d, e), M1(d, e));
            EXPECT_FLOAT_EQ(priorWithHyperPrior->getSigma0()(d, e), Sigma1(d, e));
        }
    }
    std::vector<double> valsMrs1 = {0.4760688, 0.171059770886838, 0.6714555, 0.2538392, 0.3350229, -0.4597982, 0.2920597, 0.2051382, -0.4922499, -0.4149332};
    for (size_t d = 0; d < 10; d++){
        // Mrs0
        EXPECT_FLOAT_EQ(mrs0->value<double>(d), valsMrs1[d]);
    }

    arma::mat newSigma1 = {{77.898558, -23.47372,  -7.328659, -11.198468, -11.985875},
                           {-23.473723, 118.24410, -36.076071, -26.705525, -19.304496},
                           {-7.328659, -36.07607,  87.875695,  15.149112,  14.761750},
                           {-11.198468, -26.70553,  15.149112, 107.587491,   4.828746},
                           {-11.985875, -19.30450,  14.761750,   4.828746,  91.963623}};
    arma::mat newM1 = {{0.12315897, 0.00000000,  0.00000000,  0.000000000, 0.0000000},
                       {0.03776554, 0.10121991,  0.00000000,  0.000000000, 0.0000000},
                       {0.0192243, 0.03577007,  0.10938194,  0.000000000 ,0.0000000},
                       {0.01859263, 0.01943814, -0.01464828,  0.096523142, 0.0000000},
                       {0.01991711, 0.01448517, -0.01678855, -0.005068154, 0.1042778}};

    // check if M1 is correctly initialized (not the same as M0, because it now matches rho)
    for (size_t d = 0; d < 5; d++){
        for (size_t e = 0; e < 5; e++){
            EXPECT_FLOAT_EQ(priorWithHyperPrior->getM1()(d, e), newM1(d, e));
            EXPECT_FLOAT_EQ(priorWithHyperPrior->getSigma1()(d, e), newSigma1(d, e));
        }
    }

    // check if rho is correctly initialized
    std::vector<double> expectedRho = {109.98731, 129.57167,  92.42899,  85.77360 , 20.02343};
    for (size_t d = 0; d < 5; d++) {
        EXPECT_FLOAT_EQ(rho->value<double>(d), expectedRho[d]);
    }
}

TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, estimateInitialPriorParameters_doSwitch_M0Fix) {
    // simulations and true values based on R script 'simulateHMM.R'

    // set values for mu, m, Mrr, Mrs as well as M1 and Sigma1 (pretend that EM finished) - choose other solution than above
    std::vector<double> valsMu = {-0.9099094,  1.3820622, -1.0033525, -0.8155223,  0.4952181};
    std::vector<double> valsMrr0 = {0.0845536848148294,0.111076042660194,0.107748513669999,0.130916019591296,0.103596245528932};
    std::vector<double> valsMrs0 = {-0.00368096942135846, -0.01533499298712761, -0.0137209198395793, -0.03349558251678762,  0.0343523604389143,  0.00426470319765408, -0.01557283179137137 , 0.0194882623985414, -0.01133838480014017, 0.00445487150593236};
    std::vector<uint8_t > valsZ = {0, 1, 0, 0, 1, 0, 0, 1, 0, 0};

    arma::mat M0 = {{0.084553685,  0.00000000,  0.000000000, 0.000000000, 0.0000000},
                    {-0.003680969,  0.11107604,  0.000000000, 0.000000000, 0.0000000},
                    {-0.015334993, -0.01372092,  0.107748514, 0.000000000, 0.0000000},
                    {-0.033495583,  0.03435236,  0.004264703, 0.130916020, 0.0000000},
                    {-0.015572832,  0.01948826, -0.011338385, 0.004454872, 0.10359625}};
    arma::mat M1 = {{1.2584593, 0.0000000,  0.0000000,  0.0000000, 0.0000000},
                    {0.4760688, 0.8701611,  0.0000000,  0.0000000, 0.0000000},
                    {0.171059770886838, 0.6714555,  1.0330943,  0.0000000, 0.0000000},
                    {0.2538392, 0.3350229, -0.4597982,  0.7248016, 0.0000000},
                    {0.2920597, 0.2051382, -0.4922499, -0.4149332, 0.8318608}};
    arma::mat Sigma0 = {{154.383097,  -3.957343, 17.039686,  21.315315,  17.090567},
                        {-3.957343,  90.270003,  9.858398, -17.848821, -14.140706},
                        {17.039686,   9.858398, 87.284334,  -2.647284,   9.930593},
                        {21.315315, -17.848821, -2.647284,  58.454341,  -3.170694},
                        {17.090567, -14.140706,  9.930593,  -3.170694,  93.177696}};
    arma::mat Sigma1 = {{0.84892462, -0.5410777 , 0.09309022, -0.0249604346814075, -0.0880386075435555},
                        {-0.54107771,  4.3934628, -2.41851195, -2.23083023, -1.47464370},
                        { 0.09309022, -2.4185119,  2.08680731,  1.45218123,  1.05676516},
                        {-0.0249604346814075, -2.2308302,  1.45218123,  2.37714455,  0.82728997},
                        {-0.0880386075435555, -1.4746437,  1.05676516,  0.82728997,  1.44510267}};

    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setPriors();
    // set initial values for M0 parameters
    defM0->setInitVal("1");
    defMrr0->setInitVal("0.0845536848148294,0.111076042660194,0.107748513669999,0.130916019591296,0.103596245528932");
    defMrs0->setInitVal("-0.00368096942135846, -0.01533499298712761, -0.0137209198395793, -0.03349558251678762,  0.0343523604389143,  0.00426470319765408, -0.01557283179137137 , 0.0194882623985414, -0.01133838480014017, 0.00445487150593236");
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    mrs0->initializeStorage();
    mu->initializeStorage();
    rho->initializeStorage();
    z->initializeStorage();

    for (size_t d = 0; d < 5; d++){
        mu->set(d, valsMu[d]);
    }
    for (size_t i = 0; i < 10; i++){
        z->set(i, valsZ[i]);
    }
    priorWithHyperPrior->setM1(M1);
    priorWithHyperPrior->setSigma1(Sigma1);

    // initialize prior parameters
    priorWithHyperPrior->_setAllParametersAfterEM(&logfile);

    // check if mu is correctly initialized (i.e. still the same)
    for (size_t d = 0; d < 5; d++) {
        EXPECT_FLOAT_EQ(mu->value<double>(d), valsMu[d]);
    }

    // check if z are correctly initialized (i.e. still the same)
    for (size_t i = 0; i < 10; i++) {
        EXPECT_FLOAT_EQ(z->value<uint8_t>(i), valsZ[i]);
    }

    // check if M0 is correctly initialized (i.e. still the same, as we fixed it)
    EXPECT_FLOAT_EQ(m0->value<double>(), 1.);
    for (size_t d = 0; d < 5; d++){
        // Mrr0
        EXPECT_FLOAT_EQ(mrr0->value<double>(d), valsMrr0[d]);
        // M0 and Sigma0
        for (size_t e = 0; e < 5; e++){
            EXPECT_FLOAT_EQ(priorWithHyperPrior->getM0()(d, e), M0(d, e));
            EXPECT_FLOAT_EQ(priorWithHyperPrior->getSigma0()(d, e), Sigma0(d, e));
        }
    }
    for (size_t d = 0; d < 10; d++){
        // Mrs0
        EXPECT_FLOAT_EQ(mrs0->value<double>(d), valsMrs0[d]);
    }

    arma::mat newSigma1 = {{154.384640723053, -3.95738251974007, 17.0398562885519, 21.3155285235148, 17.0907377855032},
                           {-3.95738251974006, 90.2709053608522, 9.85849639611634, -17.8489998705873, -14.1408476363398},
                           {17.0398562885519, 9.85849639611633, 87.2852067940119, -2.64731092878332, 9.93069236256174},
                           {21.3155285235148, -17.8489998705874, -2.64731092878332, 58.4549254453216, -3.17072589854317},
                           {17.0907377855032, -14.1408476363399, 9.93069236256174, -3.17072589854317, 93.1786277784979}};
    arma::mat newM1 = {{0.084553262049576, 0, 0, 0, 0},
                       {-0.00368095101664936, 0.111075487284146, 0, 0, 0},
                       {-0.0153349163127377, -0.0137208512354947, 0.107747974931471, 0, 0},
                       {-0.0334954150401311, 0.0343521886784003, 0.00426468187429804, 0.130915365016108, 0},
                       {-0.0155727539277964, 0.0194881649579601, -0.0113383281086413, 0.00445484923174189, 0.103595727551589}};

    // check if M1 is correctly initialized (not the same as M0, because it now matches rho)
    for (size_t d = 0; d < 5; d++){
        for (size_t e = 0; e < 5; e++){
            EXPECT_FLOAT_EQ(priorWithHyperPrior->getM1()(d, e), newM1(d, e));
            EXPECT_FLOAT_EQ(priorWithHyperPrior->getSigma1()(d, e), newSigma1(d, e));
        }
    }

    // check if rho is correctly initialized
    std::vector<double> expectedRho = {0.00001, 0.00001,  0.00001,  0.00001 , 0.00001};
    for (size_t d = 0; d < 5; d++) {
        EXPECT_FLOAT_EQ(rho->value<double>(d), expectedRho[d]);
    }
}


TEST_F(TPriorMultivariateNormalTwoMixedModelsWithHyperPriorTest, estimateInitialPriorParameters_doSwitch_rhoFix) {
    // simulations and true values based on R script 'simulateHMM.R'

    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setPriors();
    defRho->setInitVal("54.9936548516528,64.7858352806619,46.2144931413699,42.8868015161778,10.0117143682513");
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    mrs0->initializeStorage();
    mu->initializeStorage();
    rho->initializeStorage();
    z->initializeStorage();

    // set values for mu, m, Mrr, Mrs as well as M1 and Sigma1 (pretend that EM finished) - choose other solution than above
    std::vector<double> valsMu = {-0.9099094,  1.3820622, -1.0033525, -0.8155223,  0.4952181};
    std::vector<double> valsMrr0 = {0.0845536848148294,0.111076042660194,0.107748513669999,0.130916019591296,0.103596245528932};
    std::vector<double> valsMrs0 = {-0.00368096942135846, -0.01533499298712761, -0.0137209198395793, -0.03349558251678762,  0.0343523604389143,  0.00426470319765408, -0.01557283179137137 , 0.0194882623985414, -0.01133838480014017, 0.00445487150593236};
    std::vector<uint8_t > valsZ = {0, 1, 0, 0, 1, 0, 0, 1, 0, 0};

    arma::mat M0 = {{0.084553685,  0.00000000,  0.000000000, 0.000000000, 0.0000000},
                    {-0.003680969,  0.11107604,  0.000000000, 0.000000000, 0.0000000},
                    {-0.015334993, -0.01372092,  0.107748514, 0.000000000, 0.0000000},
                    {-0.033495583,  0.03435236,  0.004264703, 0.130916020, 0.0000000},
                    {-0.015572832,  0.01948826, -0.011338385, 0.004454872, 0.10359625}};
    arma::mat M1 = {{1.2584593, 0.0000000,  0.0000000,  0.0000000, 0.0000000},
                    {0.4760688, 0.8701611,  0.0000000,  0.0000000, 0.0000000},
                    {0.171059770886838, 0.6714555,  1.0330943,  0.0000000, 0.0000000},
                    {0.2538392, 0.3350229, -0.4597982,  0.7248016, 0.0000000},
                    {0.2920597, 0.2051382, -0.4922499, -0.4149332, 0.8318608}};
    arma::mat Sigma0 = {{154.383097,  -3.957343, 17.039686,  21.315315,  17.090567},
                        {-3.957343,  90.270003,  9.858398, -17.848821, -14.140706},
                        {17.039686,   9.858398, 87.284334,  -2.647284,   9.930593},
                        {21.315315, -17.848821, -2.647284,  58.454341,  -3.170694},
                        {17.090567, -14.140706,  9.930593,  -3.170694,  93.177696}};
    arma::mat Sigma1 = {{0.84892462, -0.5410777 , 0.09309022, -0.0249604346814075, -0.0880386075435555},
                        {-0.54107771,  4.3934628, -2.41851195, -2.23083023, -1.47464370},
                        { 0.09309022, -2.4185119,  2.08680731,  1.45218123,  1.05676516},
                        {-0.0249604346814075, -2.2308302,  1.45218123,  2.37714455,  0.82728997},
                        {-0.0880386075435555, -1.4746437,  1.05676516,  0.82728997,  1.44510267}};

    m0->set(1.);
    for (size_t d = 0; d < 5; d++){
        mu->set(d, valsMu[d]);
        mrr0->set(d, valsMrr0[d]);
    }
    for (size_t d = 0; d < 10; d++){
        mrs0->set(d, valsMrs0[d]);
    }
    for (size_t i = 0; i < 10; i++){
        z->set(i, valsZ[i]);
    }
    priorWithHyperPrior->setM1(M1);
    priorWithHyperPrior->setSigma1(Sigma1);

    // initialize prior parameters
    priorWithHyperPrior->_setAllParametersAfterEM(&logfile);
    // check if mu is correctly initialized (i.e. still the same)
    for (size_t d = 0; d < 5; d++) {
        EXPECT_FLOAT_EQ(mu->value<double>(d), valsMu[d]);
    }

    // check if z are correctly initialized (i.e. always the opposite)
    for (size_t i = 0; i < 10; i++) {
        EXPECT_FLOAT_EQ(z->value<uint8_t>(i), 1-valsZ[i]);
    }

    // check if M0 is correctly initialized (i.e. what was M1 before)
    EXPECT_FLOAT_EQ(m0->value<double>(), 1.);
    for (size_t d = 0; d < 5; d++){
        // Mrr0
        EXPECT_FLOAT_EQ(mrr0->value<double>(d), M1(d, d));
        // M0 and Sigma0
        for (size_t e = 0; e < 5; e++){
            EXPECT_FLOAT_EQ(priorWithHyperPrior->getM0()(d, e), M1(d, e));
            EXPECT_FLOAT_EQ(priorWithHyperPrior->getSigma0()(d, e), Sigma1(d, e));
        }
    }
    std::vector<double> valsMrs1 = {0.4760688, 0.171059770886838, 0.6714555, 0.2538392, 0.3350229, -0.4597982, 0.2920597, 0.2051382, -0.4922499, -0.4149332};
    for (size_t d = 0; d < 10; d++){
        // Mrs0
        EXPECT_FLOAT_EQ(mrs0->value<double>(d), valsMrs1[d]);
    }

    arma::mat newM1 = {{0.173328936745686, 0, 0, 0, 0},
                       {0.053362602006052, 0.141627696697659, 0, 0, 0},
                       {0.026993476766172, 0.0516644671904022, 0.153346726538172, 0, 0},
                       {0.0262112341583052, 0.0283864278428662, -0.0218831061340459, 0.135072155666649, 0},
                       {0.0281166357824236, 0.0210375617040339, -0.0246437993140327, -0.00817881862093901, 0.146325923376515}};
    arma::mat newSigma1 = {{39.3737411285589, -12.007400388162, -3.61778431277451, -5.61171427673387, -6.03695676776014},
                           {-12.007400388162, 61.3187829117041, -19.2472916291611, -14.46817783632, -10.3895697692343},
                           {-3.6177843127745, -19.2472916291611, 44.9812511469365, 8.30064678396295, 7.90925766022018},
                           {-5.61171427673385, -14.4681778363199, 8.30064678396294, 54.9823179688798, 2.82801819530256},
                           {-6.03695676776014, -10.3895697692343, 7.90925766022018, 2.82801819530256, 46.7043630147369}};

    // check if M1 is correctly initialized (what was M0 before)
    for (size_t d = 0; d < 5; d++){
        for (size_t e = 0; e < 5; e++){
            EXPECT_FLOAT_EQ(priorWithHyperPrior->getM1()(d, e), newM1(d, e));
            EXPECT_FLOAT_EQ(priorWithHyperPrior->getSigma1()(d, e), newSigma1(d, e));
        }
    }

    // check if rho is correctly initialized
    std::vector<double> expectedRho = {54.9936548516528,64.7858352806619,46.2144931413699,42.8868015161778,10.0117143682513};
    for (size_t d = 0; d < 5; d++) {
        EXPECT_FLOAT_EQ(rho->value<double>(d), expectedRho[d]);
    }
}

#endif
