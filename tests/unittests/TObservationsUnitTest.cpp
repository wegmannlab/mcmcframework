//
// Created by madleina on 22.01.21.
//

#include "../../core/TObservations.h"
#include "../TestCase.h"
using namespace testing;

// can not test TObservations class directly, because it is pure virtual and cannot be instatiated

//-------------------------
// TObservationsRaw
//-------------------------

class TObservationsRawTest : public Test {
public:
    // prior
    std::shared_ptr<TPrior<double>> prior;
    TRandomGenerator randomGenerator;

    std::shared_ptr<TObservationDefinition> def;
    std::shared_ptr<TObservationsRaw<double>> observations;

    void SetUp() override {
        def = std::make_shared<TObservationDefinition>("def");
        def->setDimensions({5});
        prior = std::make_shared<TPriorNormal<double>>();
    }
    void TearDown() override {}
};

TEST_F(TObservationsRawTest, constructor){
    observations = std::make_unique<TObservationsRaw<double>>(prior, def, &randomGenerator);

    EXPECT_EQ(observations->name(), "def");
}

// fill 1D data

TEST_F(TObservationsRawTest, prepareFillData_1D){
    observations = std::make_unique<TObservationsRaw<double>>(prior, def, &randomGenerator);

    // before filling data: dimensions should be as in definition (5)
    EXPECT_EQ(observations->numDim(), 1);
    EXPECT_EQ(observations->dimensions()[0], 5);

    observations->prepareFillData(100, {});

    // dimensions should now have changed to 100
    EXPECT_EQ(observations->numDim(), 1);
    EXPECT_EQ(observations->dimensions()[0], 100);
}

TEST_F(TObservationsRawTest, prepareFillData_invalid){
    observations = std::make_unique<TObservationsRaw<double>>(prior, def, &randomGenerator);

    // unknown dimension = 0
    EXPECT_THROW(observations->prepareFillData(0, {100}), std::runtime_error);
    // total size = 0
    EXPECT_THROW(observations->prepareFillData(100, {0}), std::runtime_error);
    EXPECT_THROW(observations->prepareFillData(100, {10, 8, 0, 5}), std::runtime_error);
}

TEST_F(TObservationsRawTest, fillData_1D_dimensionsExactlySame){
    observations = std::make_unique<TObservationsRaw<double>>(prior, def, &randomGenerator);
    observations->prepareFillData(100, {});

    for (size_t i = 0; i < 100; i++) {
        observations->fillDataRaw(i+0.5);
    }
    observations->finalizeFillData();

    // dimensions should be the same as we set initially (100)
    EXPECT_EQ(observations->numDim(), 1);
    EXPECT_EQ(observations->dimensions()[0], 100);
    for (size_t i = 0; i < 100; i++) {
        EXPECT_EQ(observations->value<double>(i), i+0.5);
    }
}

TEST_F(TObservationsRawTest, fillData_1D_dimensionsLessThanExpected){
    observations = std::make_unique<TObservationsRaw<double>>(prior, def, &randomGenerator);
    observations->prepareFillData(100, {});

    for (size_t i = 0; i < 18; i++) { // only fill 18 values
        observations->fillDataRaw(i+0.5);
    }
    observations->finalizeFillData();

    // dimensions should be the smaller than we set initially
    EXPECT_EQ(observations->numDim(), 1);
    EXPECT_EQ(observations->dimensions()[0], 18);
    for (size_t i = 0; i < 18; i++) {
        EXPECT_EQ(observations->value<double>(i), i+0.5);
    }
}

TEST_F(TObservationsRawTest, fillData_1D_dimensionsMoreThanExpected){
    observations = std::make_unique<TObservationsRaw<double>>(prior, def, &randomGenerator);
    observations->prepareFillData(10, {});

    for (size_t i = 0; i < 100; i++) { // fill much more values
        observations->fillDataRaw(i+0.5);
    }
    observations->finalizeFillData();

    // dimensions should be the higher than we set initially
    EXPECT_EQ(observations->numDim(), 1);
    EXPECT_EQ(observations->dimensions()[0], 100);
    for (size_t i = 0; i < 100; i++) {
        EXPECT_EQ(observations->value<double>(i), i+0.5);
    }
}

// fill 2D data

TEST_F(TObservationsRawTest, prepareFillData_2D){
    observations = std::make_unique<TObservationsRaw<double>>(prior, def, &randomGenerator);

    // before filling data: dimensions should be as in definition (5)
    EXPECT_EQ(observations->numDim(), 1);
    EXPECT_EQ(observations->dimensions()[0], 5);

    observations->prepareFillData(100, {50});

    // dimensions should now have changed to 100x50
    EXPECT_EQ(observations->numDim(), 2);
    EXPECT_EQ(observations->dimensions()[0], 100);
    EXPECT_EQ(observations->dimensions()[1], 50);
}

TEST_F(TObservationsRawTest, fillData_2D_dimensionsExactlySame){
    observations = std::make_unique<TObservationsRaw<double>>(prior, def, &randomGenerator);
    observations->prepareFillData(100, {50});

    for (size_t i = 0; i < 100*50; i++) {
        observations->fillDataRaw(i+0.5);
    }
    observations->finalizeFillData();

    // dimensions should be the same as we set initially (100x50)
    EXPECT_EQ(observations->numDim(), 2);
    EXPECT_EQ(observations->dimensions()[0], 100);
    EXPECT_EQ(observations->dimensions()[1], 50);
    for (size_t i = 0; i < 100*50; i++) {
        EXPECT_EQ(observations->value<double>(i), i+0.5);
    }
}

TEST_F(TObservationsRawTest, fillData_2D_dimensionsLessThanExpected){
    observations = std::make_unique<TObservationsRaw<double>>(prior, def, &randomGenerator);
    observations->prepareFillData(100, {50});

    for (size_t i = 0; i < 18*50; i++) { // only fill 18*50 values
        observations->fillDataRaw(i+0.5);
    }
    observations->finalizeFillData();

    // dimensions should be the smaller than we set initially
    EXPECT_EQ(observations->numDim(), 2);
    EXPECT_EQ(observations->dimensions()[0], 18);
    EXPECT_EQ(observations->dimensions()[1], 50);
    for (size_t i = 0; i < 18*50; i++) {
        EXPECT_EQ(observations->value<double>(i), i+0.5);
    }
}

TEST_F(TObservationsRawTest, fillData_2D_dimensionsMoreThanExpected){
    observations = std::make_unique<TObservationsRaw<double>>(prior, def, &randomGenerator);
    observations->prepareFillData(10, {50});

    for (size_t i = 0; i < 100*50; i++) { // fill much more values
        observations->fillDataRaw(i+0.5);
    }
    observations->finalizeFillData();

    // dimensions should be the higher than we set initially
    EXPECT_EQ(observations->numDim(), 2);
    EXPECT_EQ(observations->dimensions()[0], 100);
    EXPECT_EQ(observations->dimensions()[1], 50);
    for (size_t i = 0; i < 100*50; i++) {
        EXPECT_FLOAT_EQ(observations->value<double>(i), i+0.5);
    }
}

// fill 3D data

TEST_F(TObservationsRawTest, prepareFillData_3D){
    observations = std::make_unique<TObservationsRaw<double>>(prior, def, &randomGenerator);

    // before filling data: dimensions should be as in definition (5)
    EXPECT_EQ(observations->numDim(), 1);
    EXPECT_EQ(observations->dimensions()[0], 5);

    observations->prepareFillData(100, {50, 15});

    // dimensions should now have changed to 100x50x15
    EXPECT_EQ(observations->numDim(), 3);
    EXPECT_EQ(observations->dimensions()[0], 100);
    EXPECT_EQ(observations->dimensions()[1], 50);
    EXPECT_EQ(observations->dimensions()[2], 15);
}

TEST_F(TObservationsRawTest, fillData_3D_dimensionsExactlySame){
    observations = std::make_unique<TObservationsRaw<double>>(prior, def, &randomGenerator);
    observations->prepareFillData(100, {50, 15});

    for (size_t i = 0; i < 100*50*15; i++) {
        observations->fillDataRaw(i+0.5);
    }
    observations->finalizeFillData();

    // dimensions should be the same as we set initially (100x50x15)
    EXPECT_EQ(observations->numDim(), 3);
    EXPECT_EQ(observations->dimensions()[0], 100);
    EXPECT_EQ(observations->dimensions()[1], 50);
    EXPECT_EQ(observations->dimensions()[2], 15);
    for (size_t i = 0; i < 100*50*15; i++) {
        EXPECT_EQ(observations->value<double>(i), i+0.5);
    }
}

TEST_F(TObservationsRawTest, fillData_3D_dimensionsLessThanExpected){
    observations = std::make_unique<TObservationsRaw<double>>(prior, def, &randomGenerator);
    observations->prepareFillData(100, {50, 15});

    for (size_t i = 0; i < 18*50*15; i++) { // only fill 18*50*15 values
        observations->fillDataRaw(i+0.5);
    }
    observations->finalizeFillData();

    // dimensions should be the smaller than we set initially
    EXPECT_EQ(observations->numDim(), 3);
    EXPECT_EQ(observations->dimensions()[0], 18);
    EXPECT_EQ(observations->dimensions()[1], 50);
    EXPECT_EQ(observations->dimensions()[2], 15);
    for (size_t i = 0; i < 18*50*15; i++) {
        EXPECT_EQ(observations->value<double>(i), i+0.5);
    }
}

TEST_F(TObservationsRawTest, fillData_3D_dimensionsMoreThanExpected){
    observations = std::make_unique<TObservationsRaw<double>>(prior, def, &randomGenerator);
    observations->prepareFillData(10, {50, 15});

    for (size_t i = 0; i < 100*50*15; i++) { // fill much more values
        observations->fillDataRaw(i+0.5);
    }
    observations->finalizeFillData();

    // dimensions should be the higher than we set initially
    EXPECT_EQ(observations->numDim(), 3);
    EXPECT_EQ(observations->dimensions()[0], 100);
    EXPECT_EQ(observations->dimensions()[1], 50);
    EXPECT_EQ(observations->dimensions()[2], 15);
    for (size_t i = 0; i < 100*50*15; i++) {
        EXPECT_EQ(observations->value<double>(i), i+0.5);
    }
}

TEST_F(TObservationsRawTest, fillDataAndTranslate){
    // should not make any difference to fillDataRaw
    observations = std::make_unique<TObservationsRaw<double>>(prior, def, &randomGenerator);
    observations->prepareFillData(10, {50, 15});

    for (size_t i = 0; i < 100*50*15; i++) { // fill much more values
        observations->fillDataAndTranslate(i+0.5);
    }
    observations->finalizeFillData();

    // dimensions should be the higher than we set initially
    EXPECT_EQ(observations->numDim(), 3);
    EXPECT_EQ(observations->dimensions()[0], 100);
    EXPECT_EQ(observations->dimensions()[1], 50);
    EXPECT_EQ(observations->dimensions()[2], 15);
    for (size_t i = 0; i < 100*50*15; i++) {
        EXPECT_EQ(observations->value<double>(i), i+0.5);
    }
}

TEST_F(TObservationsRawTest, getLogPriorDensity){
    // define a standard normal prior
    prior->initialize("0,1");
    observations = std::make_shared<TObservationsRaw<double>>(prior, def, &randomGenerator);
    observations->handPointerToPrior(observations);

    // fill observations
    observations->prepareFillData(10, {5, 3});
    for (size_t i = 0; i < 8*5*3; i++) { // fill less
        observations->fillDataRaw(i+0.5);
    }
    observations->finalizeFillData();

    // get log prior density
    for (size_t i = 0; i < 8*5*3; i++) {
        EXPECT_EQ(observations->getLogPriorDensity(i), TNormalDistr::density(i+0.5, 0, 1, true));
    }
}

TEST_F(TObservationsRawTest, set){
    observations = std::make_shared<TObservationsRaw<double>>(prior, def, &randomGenerator);

    // fill observations
    observations->prepareFillData(10, {5, 3});
    for (size_t i = 0; i < 8*5*3; i++) { // fill less
        observations->fillDataRaw(i+0.5);
    }
    observations->finalizeFillData();

    // set
    for (size_t i = 0; i < 8*5*3; i++) {
        observations->set(i, i - 1);
    }

    // check if ok
    for (size_t i = 0; i < 8*5*3; i++) {
        EXPECT_EQ(observations->value<double>(i), i - 1);
    }
}

//-------------------------
// TObservationsTranslated
//-------------------------

class TObservationsTranslatedTest : public Test {
public:
    // prior
    std::shared_ptr<TPrior<double>> prior;
    TRandomGenerator randomGenerator;

    std::shared_ptr<TObservationDefinition> def;
    std::shared_ptr<TObservationsTranslated<double, uint8_t>> observations;
    std::unique_ptr<TDataTranslator<double, uint8_t>> translator;
    uint8_t Max = 255;

    void SetUp() override {
        def = std::make_shared<TObservationDefinition>("def");
        def->setDimensions({5});
        prior = std::make_shared<TPriorNormal<double>>();
        translator = std::make_unique<TDataTranslatorPhredScores<double, uint8_t>>(Max);
    }

    void TearDown() override {}
};

TEST_F(TObservationsTranslatedTest, constructor){
    observations = std::make_shared<TObservationsTranslated<double, uint8_t>>(prior, def, &randomGenerator, translator);

    EXPECT_EQ(observations->name(), "def");
}

// fillDataRaw works exactly the same as for raw observations -> not tested here

TEST_F(TObservationsTranslatedTest, fillDataAndTranslate){
    // principle of resizing etc. is the same for raw and translated -> will only check if correctly translated
    observations = std::make_shared<TObservationsTranslated<double, uint8_t>>(prior, def, &randomGenerator, translator);
    observations->prepareFillData(10, {50, 15});

    for (size_t i = 0; i < 100*50*15; i++) { // fill much more values
        observations->fillDataAndTranslate((double) i/(100*50*15)); // back-translates: assumes this is translated value and sets it to stored type (where 0 <= translated_val <= 1)
    }
    observations->finalizeFillData();

    // dimensions should be the higher than we set initially
    EXPECT_EQ(observations->numDim(), 3);
    EXPECT_EQ(observations->dimensions()[0], 100);
    EXPECT_EQ(observations->dimensions()[1], 50);
    EXPECT_EQ(observations->dimensions()[2], 15);
    for (size_t i = 0; i < 100*50*15; i++) {
        // calculate expected value
        double storedVal = std::round(-10. * log10((double) i/(100*50*15))); // translated -> stored
        if (storedVal > Max){
            storedVal = Max;
        }
        double translatedVal = pow(10., -static_cast<double>(storedVal)/10.); // stored -> translated

        EXPECT_NEAR(observations->value<double>(i), translatedVal, 0.00000000000001);
    }
}

TEST_F(TObservationsTranslatedTest, value){
    observations = std::make_shared<TObservationsTranslated<double, uint8_t>>(prior, def, &randomGenerator, translator);
    observations->prepareFillData(50, {3});

    for (size_t i = 0; i < 50*3; i++) {
        observations->fillDataRaw(i+1); // fill stored values
    }
    observations->finalizeFillData();

    // dimensions should be the same as we set initially (50x3)
    EXPECT_EQ(observations->numDim(), 2);
    EXPECT_EQ(observations->dimensions()[0], 50);
    EXPECT_EQ(observations->dimensions()[1], 3);
    for (size_t i = 0; i < 50*3; i++) {
        double expectedValue;
        if (i >= Max){
            expectedValue = pow(10., -static_cast<double>(Max)/10.);
        } else {
            expectedValue = pow(10., -static_cast<double>(i+1)/10.);
        }
        EXPECT_FLOAT_EQ(observations->value<double>(i), expectedValue); // expect translated value
    }
}

TEST_F(TObservationsTranslatedTest, getLogPriorDensity){
    // define a standard normal prior
    prior->initialize("0,1");
    observations = std::make_shared<TObservationsTranslated<double, uint8_t>>(prior, def, &randomGenerator, translator);
    observations->handPointerToPrior(observations);

    // fill observations
    observations->prepareFillData(10, {5, 3});
    for (size_t i = 0; i < 8*5*3; i++) { // fill less
        observations->fillDataRaw(i+1);
    }
    observations->finalizeFillData();

    // get log prior density
    for (size_t i = 0; i < 8*5*3; i++) {
        EXPECT_FLOAT_EQ(observations->getLogPriorDensity(i), TNormalDistr::density(pow(10., -static_cast<double>(i+1)/10.), 0, 1, true));
    }
}

TEST_F(TObservationsTranslatedTest, set){
    observations = std::make_shared<TObservationsTranslated<double, uint8_t>>(prior, def, &randomGenerator, translator);

    // fill observations
    observations->prepareFillData(10, {5, 3});
    for (size_t i = 0; i < 8*5*3; i++) { // fill less
        observations->fillDataRaw(i+1);
    }
    observations->finalizeFillData();

    // set
    for (size_t i = 0; i < 8*5*3; i++) {
        observations->set(i, (double) i/(8*5*3)); // back-translates: assumes this is translated value and sets it to stored type (where 0 <= translated_val <= 1)
    }

    // check if ok
    for (size_t i = 0; i < 8*5*3; i++) {
        // calculate expected value
        double storedVal = std::round(-10. * log10((double) i/(8*5*3))); // translated -> stored
        if (storedVal > Max){
            storedVal = Max;
        }
        double translatedVal = pow(10., -static_cast<double>(storedVal)/10.); // stored -> translated

        EXPECT_NEAR(observations->value<double>(i), translatedVal, 0.00000000000001); // translates
    }
}
