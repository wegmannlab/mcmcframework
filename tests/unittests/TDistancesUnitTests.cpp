//
// Created by madleina on 07.01.21.
//

#include "../../core/TDistances.h"
#include "../TestCase.h"
using namespace testing;

typedef uint8_t numDistGroupsType;

//-------------------------------------------
// TPositionsRaw
//-------------------------------------------

TEST(TPositionsRawTest, add_single){
    // single element at position 0
    TPositionsRaw positions;
    positions.add(0, "junk_1");
    positions.finalizeFilling();

    EXPECT_EQ(positions.getPosition(0), 0);
    EXPECT_EQ(positions.getJunkName(0), "junk_1");
    EXPECT_EQ(positions.getJunkPositionAsString(0), "junk_1:0");
    EXPECT_EQ(positions.getJunkEnds<size_t>().size(), 1);
    EXPECT_EQ(positions.getJunkEnds<size_t>()[0], 1);
}

TEST(TPositionsRawTest, add_single_offset){
    // single element at position 10
    TPositionsRaw positions;
    positions.add(10, "junk_1");
    positions.finalizeFilling();

    EXPECT_EQ(positions.getPosition(0), 10);
    EXPECT_EQ(positions.getJunkName(0), "junk_1");
    EXPECT_EQ(positions.getJunkPositionAsString(0), "junk_1:10");
    EXPECT_EQ(positions.getJunkEnds<size_t>().size(), 1);
    EXPECT_EQ(positions.getJunkEnds<size_t>()[0], 1);
}

TEST(TPositionsRawTest, add_one_junk){
    // multiple elements on junk 1
    TPositionsRaw positions;
    positions.add(10, "junk_1");
    positions.add(15, "junk_1");
    positions.add(100, "junk_1");
    positions.add(400, "junk_1");
    positions.finalizeFilling();

    EXPECT_EQ(positions.getPosition(0), 10);
    EXPECT_EQ(positions.getPosition(1), 15);
    EXPECT_EQ(positions.getPosition(2), 100);
    EXPECT_EQ(positions.getPosition(3), 400);

    EXPECT_EQ(positions.getJunkName(0), "junk_1");
    EXPECT_EQ(positions.getJunkName(1), "junk_1");
    EXPECT_EQ(positions.getJunkName(2), "junk_1");
    EXPECT_EQ(positions.getJunkName(3), "junk_1");

    EXPECT_EQ(positions.getJunkPositionAsString(0), "junk_1:10");
    EXPECT_EQ(positions.getJunkPositionAsString(1), "junk_1:15");
    EXPECT_EQ(positions.getJunkPositionAsString(2), "junk_1:100");
    EXPECT_EQ(positions.getJunkPositionAsString(3), "junk_1:400");

    EXPECT_EQ(positions.getJunkEnds<size_t>().size(), 1);
    EXPECT_EQ(positions.getJunkEnds<size_t>()[0], 4);
}

TEST(TPositionsRawTest, add_multiple_junks){
    // multiple elements on multiple junks
    TPositionsRaw positions;
    positions.add(10, "junk_1");
    positions.add(15, "junk_1");
    positions.add(100, "junk_1");
    positions.add(5, "junk_2");
    positions.add(20, "junk_2");
    positions.add(80, "junk_10");
    positions.finalizeFilling();

    EXPECT_EQ(positions.getPosition(0), 10);
    EXPECT_EQ(positions.getPosition(1), 15);
    EXPECT_EQ(positions.getPosition(2), 100);
    EXPECT_EQ(positions.getPosition(3), 5);
    EXPECT_EQ(positions.getPosition(4), 20);
    EXPECT_EQ(positions.getPosition(5), 80);

    EXPECT_EQ(positions.getJunkName(0), "junk_1");
    EXPECT_EQ(positions.getJunkName(1), "junk_1");
    EXPECT_EQ(positions.getJunkName(2), "junk_1");
    EXPECT_EQ(positions.getJunkName(3), "junk_2");
    EXPECT_EQ(positions.getJunkName(4), "junk_2");
    EXPECT_EQ(positions.getJunkName(5), "junk_10");

    EXPECT_EQ(positions.getJunkPositionAsString(0), "junk_1:10");
    EXPECT_EQ(positions.getJunkPositionAsString(1), "junk_1:15");
    EXPECT_EQ(positions.getJunkPositionAsString(2), "junk_1:100");
    EXPECT_EQ(positions.getJunkPositionAsString(3), "junk_2:5");
    EXPECT_EQ(positions.getJunkPositionAsString(4), "junk_2:20");
    EXPECT_EQ(positions.getJunkPositionAsString(5), "junk_10:80");

    EXPECT_EQ(positions.getJunkEnds<size_t>().size(), 3);
    EXPECT_EQ(positions.getJunkEnds<size_t>()[0], 3);
    EXPECT_EQ(positions.getJunkEnds<size_t>()[1], 5);
    EXPECT_EQ(positions.getJunkEnds<size_t>()[2], 6);
}

TEST(TPositionsRawTest, findJunk){
    // multiple elements on multiple junks
    std::vector<uint32_t> pos = {100,2,5,10,12,17,22,25,28,29,31,33,37,39,43,46,50,57,59,63,68,70,73,74,76,78,79,81,86,88,91,94,97,99,103,106,110,111,115,118,122,125,129,132,135,139,140,143,147,151,154,159,162,164,165,166,168,171,174,177,182,184,187,189,192,194,197,201,202,207,209,213,215,217,220,225,230,232,236,242,245,249,251,253,257,259,263,264,266,268,270,271,274,279,283,287,290,293,297,300,303,305,307,314,317,319,320,323,328,331,337,341,343,346,348,349,353,354,357,360,367,370,373,375,379,382,385,387,389,392,395,396,397,5,8,11,14,21,24,28,31,33,35,39,42,44,48,49,54,57,60,62,65,68,70,73,74,76,78,80,85,88,92,97,100,101,103,107,109,112,116,120,122,124,129,132,136,139,144,146,148,153,156,161,163,167,171,176,179,183,185,186,191,193,196,197,201,203,207,209,211,214,216,218,221,224,225,227,231,237,238,242,248,252,254,257,263,269,271,273,275,277,280,285,288,290,291,294,298,300,301,303,306,308,312,316,319,322,325,327,330,335,337,340,342,345,346,349,354,358,363,366,369,372,373,375,378,380,385,388,390,392,396,400,402,403,407,410,412,413,414,416,418,420,422,424,426,429,433,434,437,442,444,445,447,449,451,452,454,456,457,463,465,468,472,473,474,475,480,483,484,487,490,492,499,501,505,506,508,510,512,516,520,522,525,526,528,534,537,540,542,544,552,556,562,566,570,572,576,583,585,587,591,592,594,597,599,602,608,615,617,620,622,625,627,630,632,634,638,641,642,646,649,655,657,661,662,667,670,671,673,677,679,682,686,687,691,695,698,701,704,709,711,713,716,718,723,725,728,731,734,737,743,745,747,751,755,756,762,764,766,771,774,776,778,781,783,785,787,790,792,798,803,808,812,814,818,819,824,831,833,837,841,845,848,851,853,857,861,864,867,870,872,874,876,877,882,886,891,896,897,899,902,903,907,911,912,915,920,921,923,926,929,931,934,936,941,942,943,949,952,954,956,957,3,10,13,14,16,19,20,23,25,30,34,36,39,43,46,52,56,58,59,61,66,69,72,76,80,82,86,91,94,97,99,100,105,106,107,112,114,116,118,121,124,127,131,132,135,139,142,145,148,154,156,160,164,167,172,175,176,180,184,186,189,192,194,197,201,205,207,208,210,215,219,223,229,236,239,240,242,244,245,246,248,250,252,255,257,263,266,268,270,271,279,281,284,288,293,296,297,302,306,308,309,310,314,318,321,322,324,328,333,337,339,342,344,350,352,355,357,358,362,365,368,371,375,379,381,387,390,391,394,397,398,401,407,411,414,416,420,423,427,430,433,436,438,441,445,450,452,456,462,468,470,472,478,479,484,486,489,492,496,498,500,502,505,506,510,512,515,517,519,522,524,527,530,535,540,542,544,548,551,553,554,555,559,562,567,569,574,576,580,582,583,588,592,597,600,604,606,608,611,612,614,617,621,626,628,632,633,636,638,641,646,648,650,653,657,660,664,667,670,673,675,676,678,680,684,688,690,693,696,699,701,704,705,710,711,714,718,721,724,727,731,733,739,742,747,752,756,759,762,763,766,769,774,778,782,785,786,790,793,796,797,799,802,806,807,810,815,819,821,825,827,830,833,834,838,841,845,846,850,852,854,858,859,860,862,865,868,870,872,874,877,881,882,885,888,893,896,897,900,902,905,907,913,916,920,922,927,929,931,933,936,940,945,949,952,956,957,965,968,971,972,976,978,987,992,996,1000,1003,2,5,7,10,11,13,14,19,21,24,27,29,32,36,38,43,44,50,54,56,60,62,67,69,70,72,74,76,82,84,87,92,97,99,103,105,108,112,116,121,122,124,126,128,132,136,141,145,149,151,154,155,160,166,169,174,175,177,180,183,186,187,190,194,196,201,205,206,212,214,215,217,221,223,225,226,228,232,233,234,236,239,242,246,247,250,256,260,262,265,266,268,271,275,282,284,289,295,300,303,307,311,315,318,320,321,322,323,324,326,327,330,331,332,334,338,342,344,346,348,349,350,352,353,354,357,361,365,366,367,370,372,376,380,385,387,393,400,401,406,410,414,416,419,422,425,430,432,434,438,442,447,450,455,457,459,464,465,468,471,474,477,481,486,490,491,493,496,501,504,506,511,513,516,517,519,522,523,526,528,530,532,537,544,548,555,558,561,567,569,570,571,574,578,582,584,586,593,595,597,600,603,605,615,618,619,622,625,627,628,632,635,637,639,641,644,648,650,656,657,658,661,664,668,671,676,679,680,687,689,694,699,701,702,704,705,709,711,715,717,721,724,726,728,732,735,738,740,746,749,751,753,756,761,763,765,767,770,773,775,778,781,783,786,791,792,795,798,803,808,810,814,816,819,820,821,826,831,833,836,841,843,845,851,854,856,857,860,862,867,870,874,875,877,879,881,883,886,887,891,895,898,899,904,907,910,914,916,918,922,926,927,930,933,938,942,944,947,948,949,952,954,957,962,964,966,970,971,975,979,982,984,988,990,994,995,999,1002,1006,1010,1015,1017,1020,1022,1024,1029,1030,1032,1035,1038,1040,1042,1045,1049,1052,1054,1056,1060,1062,1066,1069,1071,1073,1077,1078,1082,1083,1087,1092,1098,1100,1101,1103,1104,1106,1107,1109,1112,1114,1119,1121,1127,1129,1133,1135,1139,1143,1146,1149,1153,1154,1157,1161,1163,1164,1166,1168,1171,1173,1178,1180,1182,1185,1186,1187,1191,1196,1198,1201,1203,1209,1213,1217,1219,1220,1222,1226,1231,1234,1238,1242,1246,1251,1254,1257,1258,1262,1263,1265,1268,1273, 3};
    std::vector<uint32_t> junk = {0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4, 5};
    size_t length = pos.size();
    uint32_t maxPos = *std::max_element(pos.begin(), pos.end())+10;
    uint32_t maxChr = *std::max_element(junk.begin(), junk.end())+2;

    TPositionsRaw positions;
    for (size_t i = 0; i < length; i++){
        positions.add(pos[i], toString(junk[i]));
    }
    positions.finalizeFilling();
    positions.setMaxSearchDistance(20); // decrease, because we don't have that many loci


    size_t numRep = 100000;
    TRandomGenerator randomGenerator;
    //randomGenerator.setSeed(80102871, true);
    std::cout << randomGenerator.usedSeed << std::endl;
    uint32_t previousPos = 0;
    uint32_t previousJunk = 0;
    for (size_t i = 0; i < numRep; i++){
        double random = randomGenerator.getRand();
        uint32_t newPos = previousPos;
        uint32_t newJunk = previousJunk;
        if (random > 0.5){
            // in 50% of cases: pick a locus that is close (+-20)
            int diff = randomGenerator.getRand(0, 41) - 20;
            newPos += diff;
        } else {
            // pick random locus
            newPos = randomGenerator.getRand(0, maxPos);
            newJunk = randomGenerator.getRand(1, 6);
        }

        bool exists = positions.exists(newPos, toString(newJunk));
        size_t index = 0;
        if (exists){
            index = positions.getIndex(newPos, toString(newJunk));
        }

        // evaluate
        bool reallyExists = false;
        size_t realIndex = 0;
        // does pos-junk exist?
        for (size_t j = 0; j < length; j++){
            if (pos[j] == newPos){
                if (junk[j] == newJunk){
                    reallyExists = true;
                    realIndex = j;
                    break;
                }
            }
        }

        if (exists != reallyExists){
            std::cout << i << std::endl;
        }

        EXPECT_EQ(exists, reallyExists);
        EXPECT_EQ(index, realIndex);

        // reset
        previousPos = newPos;
        previousJunk = newJunk;
    }
}


//-------------------------------------------
// TDistances
//-------------------------------------------
TEST(TDistancesBinnedTest, initialize_maxDist0){
    uint32_t maxDist = 0;
    EXPECT_THROW(TDistancesBinned<numDistGroupsType> distances(maxDist), std::runtime_error);
}

TEST(TDistancesBinnedTest, initialize_maxDist_1){
    uint32_t maxDist = 1;
    TDistancesBinned<numDistGroupsType> distances(maxDist);

    // numDistanceGroups
    EXPECT_EQ(distances.numDistanceGroups(), 1);

    // distanceGroups
    TDistanceGroup group = distances.distanceGroup(0);
    EXPECT_EQ(group.min, 0);
    EXPECT_EQ(group.maxPlusOne, 0);
    EXPECT_EQ(group.distance, 0);
    EXPECT_EQ(group.hasSites, false);
}

TEST(TDistancesBinnedTest, initialize_maxDist_2){
    uint32_t maxDist = 2;
    TDistancesBinned<numDistGroupsType> distances(maxDist);

    // numDistanceGroups
    EXPECT_EQ(distances.numDistanceGroups(), 2);

    // distanceGroups
    TDistanceGroup group = distances.distanceGroup(0);
    EXPECT_EQ(group.min, 0);
    EXPECT_EQ(group.maxPlusOne, 0);
    EXPECT_EQ(group.distance, 0);
    EXPECT_EQ(group.hasSites, false);

    group = distances.distanceGroup(1);
    EXPECT_EQ(group.min, 1);
    EXPECT_EQ(group.maxPlusOne, 2);
    EXPECT_EQ(group.distance, 1);
    EXPECT_EQ(group.hasSites, false);
}

TEST(TDistancesBinnedTest, initialize_maxDist_3_4){
    std::vector<uint32_t> maxDists = {3, 4};
    for (auto maxDist : maxDists) {
        TDistancesBinned<numDistGroupsType> distances(maxDist);

        // numDistanceGroups
        EXPECT_EQ(distances.numDistanceGroups(), 3);

        // distanceGroups
        TDistanceGroup group = distances.distanceGroup(0);
        EXPECT_EQ(group.min, 0);
        EXPECT_EQ(group.maxPlusOne, 0);
        EXPECT_EQ(group.distance, 0);
        EXPECT_EQ(group.hasSites, false);

        group = distances.distanceGroup(1);
        EXPECT_EQ(group.min, 1);
        EXPECT_EQ(group.maxPlusOne, 2);
        EXPECT_EQ(group.distance, 1);
        EXPECT_EQ(group.hasSites, false);

        group = distances.distanceGroup(2);
        EXPECT_EQ(group.min, 2);
        EXPECT_EQ(group.maxPlusOne, 4);
        EXPECT_EQ(group.distance, 2);
        EXPECT_EQ(group.hasSites, false);
    }
}

TEST(TDistancesBinnedTest, initialize_maxDist_5_6_7_8){
    std::vector<uint32_t> maxDists = {5, 6, 7, 8};
    for (auto maxDist : maxDists) {
        TDistancesBinned<numDistGroupsType> distances(maxDist);

        // numDistanceGroups
        EXPECT_EQ(distances.numDistanceGroups(), 4);

        // distanceGroups
        TDistanceGroup group = distances.distanceGroup(0);
        EXPECT_EQ(group.min, 0);
        EXPECT_EQ(group.maxPlusOne, 0);
        EXPECT_EQ(group.distance, 0);
        EXPECT_EQ(group.hasSites, false);

        group = distances.distanceGroup(1);
        EXPECT_EQ(group.min, 1);
        EXPECT_EQ(group.maxPlusOne, 2);
        EXPECT_EQ(group.distance, 1);
        EXPECT_EQ(group.hasSites, false);

        group = distances.distanceGroup(2);
        EXPECT_EQ(group.min, 2);
        EXPECT_EQ(group.maxPlusOne, 4);
        EXPECT_EQ(group.distance, 2);
        EXPECT_EQ(group.hasSites, false);

        group = distances.distanceGroup(3);
        EXPECT_EQ(group.min, 4);
        EXPECT_EQ(group.maxPlusOne, 8);
        EXPECT_EQ(group.distance, 4);
        EXPECT_EQ(group.hasSites, false);
    }
}

TEST(TDistancesBinnedTest, initialize_maxDist_9_10_11_12_13_14_15_16){
    std::vector<uint32_t> maxDists = {9,10,11,12,13,14,15,16};
    for (auto maxDist : maxDists) {
        TDistancesBinned<numDistGroupsType> distances(maxDist);

        // numDistanceGroups
        EXPECT_EQ(distances.numDistanceGroups(), 5);

        // distanceGroups
        TDistanceGroup group = distances.distanceGroup(0);
        EXPECT_EQ(group.min, 0);
        EXPECT_EQ(group.maxPlusOne, 0);
        EXPECT_EQ(group.distance, 0);
        EXPECT_EQ(group.hasSites, false);

        group = distances.distanceGroup(1);
        EXPECT_EQ(group.min, 1);
        EXPECT_EQ(group.maxPlusOne, 2);
        EXPECT_EQ(group.distance, 1);
        EXPECT_EQ(group.hasSites, false);

        group = distances.distanceGroup(2);
        EXPECT_EQ(group.min, 2);
        EXPECT_EQ(group.maxPlusOne, 4);
        EXPECT_EQ(group.distance, 2);
        EXPECT_EQ(group.hasSites, false);

        group = distances.distanceGroup(3);
        EXPECT_EQ(group.min, 4);
        EXPECT_EQ(group.maxPlusOne, 8);
        EXPECT_EQ(group.distance, 4);
        EXPECT_EQ(group.hasSites, false);

        group = distances.distanceGroup(4);
        EXPECT_EQ(group.min, 8);
        EXPECT_EQ(group.maxPlusOne, 16);
        EXPECT_EQ(group.distance, 8);
        EXPECT_EQ(group.hasSites, false);
    }
}

TEST(TDistancesBinnedTest, initialize_maxDist_17_18__32){
    std::vector<uint32_t> maxDists = {17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32};
    for (auto maxDist : maxDists) {
        TDistancesBinned<numDistGroupsType> distances(maxDist);

        // numDistanceGroups
        EXPECT_EQ(distances.numDistanceGroups(), 6);

        // distanceGroups
        TDistanceGroup group = distances.distanceGroup(0);
        EXPECT_EQ(group.min, 0);
        EXPECT_EQ(group.maxPlusOne, 0);
        EXPECT_EQ(group.distance, 0);
        EXPECT_EQ(group.hasSites, false);

        group = distances.distanceGroup(1);
        EXPECT_EQ(group.min, 1);
        EXPECT_EQ(group.maxPlusOne, 2);
        EXPECT_EQ(group.distance, 1);
        EXPECT_EQ(group.hasSites, false);

        group = distances.distanceGroup(2);
        EXPECT_EQ(group.min, 2);
        EXPECT_EQ(group.maxPlusOne, 4);
        EXPECT_EQ(group.distance, 2);
        EXPECT_EQ(group.hasSites, false);

        group = distances.distanceGroup(3);
        EXPECT_EQ(group.min, 4);
        EXPECT_EQ(group.maxPlusOne, 8);
        EXPECT_EQ(group.distance, 4);
        EXPECT_EQ(group.hasSites, false);

        group = distances.distanceGroup(4);
        EXPECT_EQ(group.min, 8);
        EXPECT_EQ(group.maxPlusOne, 16);
        EXPECT_EQ(group.distance, 8);
        EXPECT_EQ(group.hasSites, false);

        group = distances.distanceGroup(5);
        EXPECT_EQ(group.min, 16);
        EXPECT_EQ(group.maxPlusOne, 32);
        EXPECT_EQ(group.distance, 16);
        EXPECT_EQ(group.hasSites, false);
    }
}

TEST(TDistancesBinnedTest, addInitial){
    TDistancesBinned<numDistGroupsType> distances(5);

    // first one
    distances.add(10000, "junk_1");
    EXPECT_TRUE(distances.groupHasSites(0));
    EXPECT_EQ(distances[0], 0);

    // another first one
    distances.add(10000, "junk_2");
    EXPECT_TRUE(distances.groupHasSites(0));
    EXPECT_EQ(distances[1], 0);
}

TEST(TDistancesBinnedTest, add_maxDist_1){
    TDistancesBinned<numDistGroupsType> distances(1);

    // first locus
    distances.add(0, "junk_1");
    EXPECT_TRUE(distances.groupHasSites(0));
    EXPECT_EQ(distances[0], 0);

    // second locus, distance = 1
    distances.add(1, "junk_1");
    EXPECT_TRUE(distances.groupHasSites(0));
    EXPECT_EQ(distances[1], 0);

    // another locus, distance = large
    distances.add(100000, "junk_1");
    EXPECT_TRUE(distances.groupHasSites(0));
    EXPECT_EQ(distances[2], 0);
}

TEST(TDistancesBinnedTest, add_maxDist_2){
    TDistancesBinned<numDistGroupsType> distances(2);

    // first locus
    distances.add(0, "junk_1");
    EXPECT_TRUE(distances.groupHasSites(0));
    EXPECT_EQ(distances[0], 0);

    // second locus, distance = 1
    distances.add(1, "junk_1");
    EXPECT_TRUE(distances.groupHasSites(1));
    EXPECT_EQ(distances[1], 1);

    // second locus, distance = 2
    distances.add(3, "junk_1");
    EXPECT_TRUE(distances.groupHasSites(1));
    EXPECT_EQ(distances[2], 1);

    // another locus, distance = large
    distances.add(100000, "junk_1");
    EXPECT_TRUE(distances.groupHasSites(1));
    EXPECT_EQ(distances[3], 1);
}

TEST(TDistancesBinnedTest, add_maxDist_3_4){
    std::vector<uint32_t> maxDists = {3, 4};
    for (auto maxDist : maxDists) {
        TDistancesBinned<numDistGroupsType> distances(maxDist);

        // first locus
        distances.add(0, "junk_1");
        EXPECT_TRUE(distances.groupHasSites(0));
        EXPECT_EQ(distances[0], 0);

        // second locus, distance = 1
        distances.add(1, "junk_1");
        EXPECT_TRUE(distances.groupHasSites(1));
        EXPECT_EQ(distances[1], 1);

        // second locus, distance = 2
        distances.add(3, "junk_1");
        EXPECT_TRUE(distances.groupHasSites(2));
        EXPECT_EQ(distances[2], 2);

        // third locus, distance = 3
        distances.add(6, "junk_1");
        EXPECT_TRUE(distances.groupHasSites(2));
        EXPECT_EQ(distances[3], 2);

        // another locus, distance = large
        distances.add(100000, "junk_1");
        EXPECT_TRUE(distances.groupHasSites(2));
        EXPECT_EQ(distances[4], 2);
    }
}

TEST(TDistancesBinnedTest, add_maxDist_5_6_7_8){
    std::vector<uint32_t> maxDists = {5, 6, 7, 8};
    for (auto maxDist : maxDists) {
        TDistancesBinned<numDistGroupsType> distances(maxDist);

        // first locus
        distances.add(0, "junk_1");
        EXPECT_TRUE(distances.groupHasSites(0));
        EXPECT_EQ(distances[0], 0);

        // second locus, distance = 1
        distances.add(1, "junk_1");
        EXPECT_TRUE(distances.groupHasSites(1));
        EXPECT_EQ(distances[1], 1);

        // third locus, distance = 2
        distances.add(3, "junk_1");
        EXPECT_TRUE(distances.groupHasSites(2));
        EXPECT_EQ(distances[2], 2);

        // fourth locus, distance = 3
        distances.add(6, "junk_1");
        EXPECT_TRUE(distances.groupHasSites(2));
        EXPECT_EQ(distances[3], 2);

        // fifth locus, distance = 4
        distances.add(10, "junk_1");
        EXPECT_TRUE(distances.groupHasSites(3));
        EXPECT_EQ(distances[4], 3);

        // sixth locus, distance = 8
        distances.add(18, "junk_1");
        EXPECT_TRUE(distances.groupHasSites(3));
        EXPECT_EQ(distances[5], 3);

        // another locus, distance = large
        distances.add(100000, "junk_1");
        EXPECT_TRUE(distances.groupHasSites(3));
        EXPECT_EQ(distances[6], 3);
    }
}

TEST(TDistancesBinnedTest, add_maxDist_9_10_11_12_13_14_15_16){
    std::vector<uint32_t> maxDists = {9,10,11,12,13,14,15,16};
    for (auto maxDist : maxDists) {
        TDistancesBinned<numDistGroupsType> distances(maxDist);

        // first locus
        distances.add(0, "junk_1");
        EXPECT_TRUE(distances.groupHasSites(0));
        EXPECT_EQ(distances[0], 0);

        // second locus, distance = 1
        distances.add(1, "junk_1");
        EXPECT_TRUE(distances.groupHasSites(1));
        EXPECT_EQ(distances[1], 1);

        // third locus, distance = 2
        distances.add(3, "junk_1");
        EXPECT_TRUE(distances.groupHasSites(2));
        EXPECT_EQ(distances[2], 2);

        // fourth locus, distance = 3
        distances.add(6, "junk_1");
        EXPECT_TRUE(distances.groupHasSites(2));
        EXPECT_EQ(distances[3], 2);

        // fifth locus, distance = 4
        distances.add(10, "junk_1");
        EXPECT_TRUE(distances.groupHasSites(3));
        EXPECT_EQ(distances[4], 3);

        // sixth locus, distance = 8
        distances.add(18, "junk_1");
        EXPECT_TRUE(distances.groupHasSites(4));
        EXPECT_EQ(distances[5], 4);

        // seventh locus, distance = 16
        distances.add(34, "junk_1");
        EXPECT_TRUE(distances.groupHasSites(4));
        EXPECT_EQ(distances[6], 4);

        // another locus, distance = large
        distances.add(100000, "junk_1");
        EXPECT_TRUE(distances.groupHasSites(4));
        EXPECT_EQ(distances[7], 4);
    }
}

TEST(TDistancesBinnedTest, add_maxDist_100){
    uint32_t maxDist = 100;
    TDistancesBinned<numDistGroupsType> distances(maxDist);

    // add loci
    distances.add(0, "junk_1");
    distances.add(1, "junk_1");
    distances.add(3, "junk_1");
    distances.add(7, "junk_1");
    distances.add(15, "junk_1");
    distances.add(31, "junk_1");
    distances.add(63, "junk_1");
    distances.add(127, "junk_1");
    distances.add(255, "junk_1");
    distances.finalizeFilling();

    // numDistanceGroups
    EXPECT_EQ(distances.numDistanceGroups(), 8);

    // groupHasSites
    EXPECT_TRUE(distances.groupHasSites(0));
    EXPECT_TRUE(distances.groupHasSites(1));
    EXPECT_TRUE(distances.groupHasSites(2));
    EXPECT_TRUE(distances.groupHasSites(3));
    EXPECT_TRUE(distances.groupHasSites(4));
    EXPECT_TRUE(distances.groupHasSites(5));
    EXPECT_TRUE(distances.groupHasSites(6));
    EXPECT_TRUE(distances.groupHasSites(7));

    // groupMembership
    EXPECT_EQ(distances[0], 0);
    EXPECT_EQ(distances[1], 1);
    EXPECT_EQ(distances[2], 2);
    EXPECT_EQ(distances[3], 3);
    EXPECT_EQ(distances[4], 4);
    EXPECT_EQ(distances[5], 5);
    EXPECT_EQ(distances[6], 6);
    EXPECT_EQ(distances[7], 7);
}

TEST(TDistancesBinnedTest, fillJunkEnds){
    uint32_t maxDist = 100;
    TDistancesBinned<numDistGroupsType> distances(maxDist);

    // add loci
    // junk 1
    distances.add(0, "junk_1");
    distances.add(1, "junk_1");
    distances.add(3, "junk_1");
    distances.add(7, "junk_1");
    distances.add(15, "junk_1");
    // junk 2
    distances.add(31, "junk_2");
    distances.add(63, "junk_2");
    distances.add(127, "junk_2");
    // junk 3
    distances.add(255, "junk_3");
    distances.add(722, "junk_3");
    distances.add(1000, "junk_3");
    // junk 4
    distances.add(1001, "junk_4");
    distances.add(1082, "junk_4");
    distances.add(1900, "junk_4");
    // junk 5
    distances.add(1999, "junk_5");
    distances.finalizeFilling();

    std::vector<size_t> junkEnds = distances.getJunkEnds<size_t>();

    EXPECT_EQ(junkEnds.size(), 5);
    std::vector<size_t> expected = {5, 8, 11, 14, 15};
    for (size_t i = 0; i < 5; i++){
        EXPECT_EQ(junkEnds[i], expected[i]);
    }
}
