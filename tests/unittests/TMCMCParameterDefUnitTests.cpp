//
// Created by madleina on 11.12.20.
//

#include "../../core/TMCMCParameterDef.h"
#include "../TestCase.h"
using namespace testing;

//-------------------------------------------
// TDefinitionBase
//-------------------------------------------

// use TParameterDefinition to test TDefinitionBase,
// because TDefinitionBase is a pure virtual class that can not be instatiated
// functionalities should be exactly the same for TObservationDefinition

// Test: isObserved

TEST(TDefTest, reSetObserved){
    // not observed
    TParameterDefinition def("def");
    // ok: not observed -> not observed
    EXPECT_NO_THROW(def.reSetObserved("false"));
    EXPECT_FALSE(def.isObserved());
    // throw: not observed -> observed
    EXPECT_THROW(try{def.reSetObserved("true");} catch(...){throw std::runtime_error("");}, std::runtime_error);
    EXPECT_FALSE(def.isObserved());

    // observed
    TObservationDefinition obs("obs");
    // ok: observed -> observed
    EXPECT_NO_THROW(obs.reSetObserved("true"));
    EXPECT_TRUE(obs.isObserved());
    // throw: observed -> not observed
    EXPECT_THROW(try{obs.reSetObserved("false");} catch(...){throw std::runtime_error("");}, std::runtime_error);
    EXPECT_TRUE(obs.isObserved());
}

// Test: set priors and parameters

TEST(TDefTest, setPrior_ok) {
    TParameterDefinition def("array");
    EXPECT_NO_THROW(def.setPrior("normal"));
    EXPECT_NO_THROW(def.setPrior(" normal "));
    EXPECT_NO_THROW(def.setPrior(""
                                 "normal"));
    EXPECT_NO_THROW(def.setPrior("normal"
                                 ""));
}

TEST(TDefTest, reSetPrior){
    // no prior parameters -> ok!
    TParameterDefinition def("def");
    EXPECT_NO_THROW(def.reSetPrior("normal"));
    EXPECT_EQ(def.prior(), MCMCPrior::normal);

    // fixed prior parameters -> ok!
    TParameterDefinition def2("def2");
    def2.setPriorParams("0,1");
    EXPECT_NO_THROW(def2.reSetPrior("normal"));
    EXPECT_EQ(def2.prior(), MCMCPrior::normal);

    // "word" prior parameters -> not ok, throw
    TParameterDefinition def3("def3");
    def3.setPriorParams("tux");
    EXPECT_THROW({try {def3.reSetPrior("normal");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST(TDefTest, setPriorParams){
    TParameterDefinition def("def");

    // default
    EXPECT_TRUE(def.priorParametersAreFix());

    // empty parameters
    def.setPriorParams("");
    EXPECT_TRUE(def.priorParametersAreFix());

    // number(s)
    def.setPriorParams("1");
    EXPECT_TRUE(def.priorParametersAreFix());
    def.setPriorParams("1,2,3");
    EXPECT_TRUE(def.priorParametersAreFix());
    def.setPriorParams("1.1,-8.46239,100.274,-37");
    EXPECT_TRUE(def.priorParametersAreFix());

    // letter(s)
    def.setPriorParams("and");
    EXPECT_FALSE(def.priorParametersAreFix());
    def.setPriorParams("and,b,cnd");
    EXPECT_FALSE(def.priorParametersAreFix());
    def.setPriorParams("k,b100,K20,f1");
    EXPECT_FALSE(def.priorParametersAreFix());

    // numbers and letters mixed -> throw
    EXPECT_THROW(try{def.setPriorParams("1,a");} catch (...) {throw std::runtime_error("Mixed!");}, std::runtime_error);
    EXPECT_THROW(try{def.setPriorParams("1,bfzd,g3");} catch (...) {throw std::runtime_error("Mixed!");}, std::runtime_error);
    EXPECT_THROW(try{def.setPriorParams("beta,-8.46239,alpha,-37");} catch (...) {throw std::runtime_error("Mixed!");}, std::runtime_error);
}

TEST(TDefTest, reSetParameters){
    // no prior parameters -> ok!
    TParameterDefinition def("def");
    EXPECT_NO_THROW(def.reSetPriorParams("5"));
    EXPECT_EQ(def.parameters(), "5");

    // fixed prior parameters -> ok!
    TParameterDefinition def2("def2");
    def2.setPriorParams("0,1");
    EXPECT_NO_THROW(def2.reSetPriorParams("9"));
    EXPECT_EQ(def2.parameters(), "9");

    // "word" prior parameters -> not ok, throw
    TParameterDefinition def3("def3");
    def3.setPriorParams("tux");
    EXPECT_THROW({try {def3.reSetPriorParams("alpha");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST(TDefTest, reSet_okIfDoesntChange){
    TParameterDefinition array("array");
    array.setDimensions({5});
    array.setPrior("normal");
    array.setPriorParams("mean,sd");
    EXPECT_NO_THROW(array.reSetPrior("normal"));
    EXPECT_NO_THROW(array.reSetPriorParams("mean,sd"));
    EXPECT_EQ(array.prior(), MCMCPrior::normal);
    EXPECT_EQ(array.parameters(), "mean,sd");
}

// Test: setMin(), setMax(), hasDefaultMin(), hasDefaultMax(), min(), max()
TEST(TDefTest, testMinMaxDefault){
    // by default: has default min and max
    TParameterDefinition def("def");
    EXPECT_THAT(def.hasDefaultMin(), true);
    EXPECT_THAT(def.hasDefaultMax(), true);
}

TEST(TDefTest, testMin){
    TParameterDefinition def("def");
    def.setMin("-10.", false);
    EXPECT_THAT(def.hasDefaultMin(), false);
    EXPECT_THAT(def.hasDefaultMax(), true);
    EXPECT_THAT(def.minIncluded(), false);
    EXPECT_EQ(def.min(), "-10.");
}

TEST(TDefTest, testMax){
    TParameterDefinition def("def");
    def.setMax("-10.", false);
    EXPECT_THAT(def.hasDefaultMin(), true);
    EXPECT_THAT(def.hasDefaultMax(), false);
    EXPECT_THAT(def.maxIncluded(), false);
    EXPECT_EQ(def.max(), "-10.");
}

TEST(TDefTest, testMinMax){
    TParameterDefinition def("def");
    def.setMin("-10.", false);
    def.setMax("10.", false);
    EXPECT_THAT(def.hasDefaultMin(), false);
    EXPECT_THAT(def.hasDefaultMax(), false);
    EXPECT_THAT(def.minIncluded(), false);
    EXPECT_THAT(def.maxIncluded(), false);
    EXPECT_EQ(def.min(), "-10.");
    EXPECT_EQ(def.max(), "10.");
}

// Test: set dimensions

TEST(TDefTest, setDimensions_single){
    TParameterDefinition single("single");
    EXPECT_NO_THROW(single.setDimensions({1}));
    EXPECT_EQ(single.dimensions().size(), 1);
    EXPECT_EQ(single.dimensions()[0], 1);
    EXPECT_FALSE(single.hasDefaultDimensions());
}

TEST(TDefTest, setDimensions_linear){
    TParameterDefinition array("array");
    EXPECT_NO_THROW(array.setDimensions({5}));
    EXPECT_EQ(array.dimensions().size(), 1);
    EXPECT_EQ(array.dimensions()[0], 5);
    EXPECT_FALSE(array.hasDefaultDimensions());
}

TEST(TDefTest, setDimensions_2D){
    TParameterDefinition array("array");
    EXPECT_NO_THROW(array.setDimensions({5, 5}));
    EXPECT_EQ(array.dimensions().size(), 2);
    EXPECT_EQ(array.dimensions()[0], 5);
    EXPECT_FALSE(array.hasDefaultDimensions());
}

//--------------------------------------------
// TMCMCParameterDef
//--------------------------------------------

TEST(TDefTest, setPropKernel_throw) {
    TParameterDefinition def("array");
    // don't give valid proposal kernel -> throw
    EXPECT_THROW(try {def.setPropKernel("whatever");} catch (...){throw std::runtime_error("caught throw");}, std::runtime_error);
    EXPECT_THROW(try {def.setPropKernel("");} catch (...){throw std::runtime_error("caught throw");}, std::runtime_error);
    EXPECT_THROW(try {def.setPropKernel("chisq");} catch (...){throw std::runtime_error("caught throw");}, std::runtime_error);
    EXPECT_THROW(try {def.setPropKernel("normal.");} catch (...){throw std::runtime_error("caught throw");}, std::runtime_error);
    EXPECT_THROW(try {def.setPropKernel("normal,");} catch (...){throw std::runtime_error("caught throw");}, std::runtime_error);
}

TEST(TDefTest, setPropKernel) {
    TParameterDefinition def("array");
    EXPECT_NO_THROW(def.setPropKernel("normal"));
    EXPECT_NO_THROW(
            def.setPropKernel("           normal             "));
    EXPECT_NO_THROW(def.setPropKernel("normal"
                                          ""));
    EXPECT_NO_THROW(def.setPropKernel("normal "));
    EXPECT_NO_THROW(def.setPropKernel("uniform"));
}


// Test: initVal()
TEST(TDefTest, testInitValDefault){
    TParameterDefinition def("def");
    EXPECT_THAT(def.hasDefaultInitVal(), true);
}

TEST(TDefTest, testInitVal){
    TParameterDefinition def("def");
    def.setInitVal("1.");
    EXPECT_THAT(def.hasDefaultInitVal(), false);
}

// Test: storeMeanVar()
TEST(TDefTest, testMeanVarDefault){
    TParameterDefinition def("def");
    EXPECT_THAT(def.storeMeanVar(), false);
}

TEST(TDefTest, testMeanVarWithFilename){
    TParameterDefinition def("def");
    def.setMeanVarFile("cheggie");
    EXPECT_THAT(def.storeMeanVar(), true);
}

//--------------------------------------------
// TObservationDefinition
//--------------------------------------------

TEST(TDefTest, default){
    TObservationDefinition def("def");

    EXPECT_THAT(def.isTranslated(), false);
    EXPECT_THROW(def.translator(), std::runtime_error);
    EXPECT_EQ(def.maxStoredValue(), std::numeric_limits<double>::max());
    EXPECT_EQ(def.storedType(), "double");
}

TEST(TDefTest, translate){
    TObservationDefinition def("def");
    def.translate(Translator::phred, "uint8_t", 255);

    EXPECT_THAT(def.isTranslated(), true);
    EXPECT_EQ(def.translator(), Translator::phred);
    EXPECT_EQ(def.maxStoredValue(), 255);
    EXPECT_EQ(def.storedType(), "uint8_t");
}