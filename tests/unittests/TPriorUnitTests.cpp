//
// Created by madleina on 15.07.20.
//

#include "../../core/TPrior.h"
#include "../../core/TMCMCParameter.h"
#include "../TestCase.h"
using namespace testing;

//-------------------------------------------
// TPrior
//-------------------------------------------
// idea: test all functionalities for doubles, then specifically test special cases with ints and bools
class TPriorTest : public TPrior<double>, public Test
{
    // Empty - bridge to protected members for unit-testing
};

TEST_F(TPriorTest, testProcessingStringsTwoValidValues){
    std::vector<double> vec = _processPriorParameterString("0,1", 2);
    EXPECT_THAT(vec, ElementsAre(0,1));
}

TEST_F(TPriorTest, testProcessingStringsTwoSignedValidValues){
    std::vector<double> vec = _processPriorParameterString("-1,+1", 2);
    EXPECT_THAT(vec, ElementsAre(-1,1));
}

TEST_F(TPriorTest, testProcessingStringsTwoInvalidValues){
    EXPECT_THROW({try { _processPriorParameterString("1", 2);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try { _processPriorParameterString("1/2", 2);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try { _processPriorParameterString("1.2", 2);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try { _processPriorParameterString("1,b", 2);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try { _processPriorParameterString("a,1", 2);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try { _processPriorParameterString("a,b", 2);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try { _processPriorParameterString("inf,b", 2);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try { _processPriorParameterString("true,b", 2);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TPriorTest, testProcessingStringsOneValidValue){
    std::vector<double> vec = _processPriorParameterString("1", 1);
    EXPECT_THAT(vec, ElementsAre(1));
}

TEST_F(TPriorTest, testProcessingStringsOneSignedValidValue){
    std::vector<double> vec = _processPriorParameterString("-1", 1);
    EXPECT_THAT(vec, ElementsAre(-1));
    vec = _processPriorParameterString("+1", 1);
    EXPECT_THAT(vec, ElementsAre(1));
}

TEST_F(TPriorTest, testProcessingStringsOneInvalidValue){
    EXPECT_THROW({try { _processPriorParameterString("1", 2);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try { _processPriorParameterString("1/2", 1);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try { _processPriorParameterString("a", 1);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try { _processPriorParameterString("inf", 1);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try { _processPriorParameterString("true", 1);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    EXPECT_THROW({try { _processPriorParameterString("0,1", 1);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

//-------------------------------------------
struct TPriorUniformTest : Test {
    std::string params;
    TPriorUniform<double> prior;
};

TEST_F(TPriorUniformTest, initValid){
    EXPECT_NO_THROW(prior.initialize(params));
}

TEST_F(TPriorUniformTest, initUniformThrowIfParametersGiven){
    params = "1.";
    EXPECT_THROW({try {prior.initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    params = "-10,10";
    EXPECT_THROW({try {prior.initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TPriorUniformTest, checkMinMaxDefault){
    prior.initialize(params);
    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIsIncluded = true;
    bool maxIsIncluded = true;
    bool hasDefaultMin = true;
    bool hasDefaultMax = true;

    EXPECT_TRUE(prior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(prior.getLogPriorDensity(5), 0.);
}

TEST_F(TPriorUniformTest, checkMinMax){
    prior.initialize(params);
    std::string min = "-1.";
    std::string max = "1.";
    bool minIsIncluded = true;
    bool maxIsIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;

    EXPECT_TRUE(prior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_FLOAT_EQ(prior.getLogPriorDensity(0.), -0.6931472);
}

TEST_F(TPriorUniformTest, hastings){
    prior.initialize(params);
    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIsIncluded = true;
    bool maxIsIncluded = true;
    bool hasDefaultMin = true;
    bool hasDefaultMax = true;

    EXPECT_TRUE(prior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(prior.getLogPriorDensity(-5.) - prior.getLogPriorDensity(5.), prior.getLogPriorRatio(5., 5.));
}

struct TPriorUniformTest_Int : Test {
    std::string params;
    TPriorUniform<int> prior;
};

TEST_F(TPriorUniformTest_Int, checkMinMaxDefault){
    prior.initialize(params);
    std::string min = toString(std::numeric_limits<int>::lowest());
    std::string max = toString(std::numeric_limits<int>::max());
    bool minIsIncluded = true;
    bool maxIsIncluded = true;
    bool hasDefaultMin = true;
    bool hasDefaultMax = true;

    EXPECT_TRUE(prior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(prior.getLogPriorDensity(5), 0.);
}

TEST_F(TPriorUniformTest_Int, checkMinMax){
    prior.initialize(params);
    std::string min = "-1";
    std::string max = "1";
    bool minIsIncluded = true;
    bool maxIsIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;

    EXPECT_TRUE(prior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_FLOAT_EQ(prior.getLogPriorDensity(0.), -0.6931472);
}

TEST_F(TPriorUniformTest_Int, hastings){
    prior.initialize(params);
    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIsIncluded = true;
    bool maxIsIncluded = true;
    bool hasDefaultMin = true;
    bool hasDefaultMax = true;

    EXPECT_TRUE(prior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(prior.getLogPriorDensity(-5.) - prior.getLogPriorDensity(5.), prior.getLogPriorRatio(5., 5.));
}

struct TPriorUniformTest_Bool : Test {
    std::string params;
    TPriorUniform<bool> prior;
};

TEST_F(TPriorUniformTest_Bool, checkMinMaxDefault){
    prior.initialize(params);
    std::string min = toString(std::numeric_limits<int>::lowest());
    std::string max = toString(std::numeric_limits<int>::max());
    bool minIsIncluded = true;
    bool maxIsIncluded = true;
    bool hasDefaultMin = true;
    bool hasDefaultMax = true;

    EXPECT_TRUE(prior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(prior.getLogPriorDensity(5), 0.);
}

TEST_F(TPriorUniformTest_Bool, hastings){
    prior.initialize(params);
    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIsIncluded = true;
    bool maxIsIncluded = true;
    bool hasDefaultMin = true;
    bool hasDefaultMax = true;

    EXPECT_TRUE(prior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(prior.getLogPriorDensity(-5.) - prior.getLogPriorDensity(5.), prior.getLogPriorRatio(5., 5.));
}

//-------------------------------------------

struct TPriorNormalTest : Test {
    std::string params = "0,1";
    TPriorNormal<double> prior;
};

TEST_F(TPriorNormalTest, initValid){
    prior.initialize(params);
    EXPECT_EQ(prior.mean(), 0);
    EXPECT_EQ(prior.var(), 1);
}

TEST_F(TPriorNormalTest, checkSdVar){
    params = "0,5";
    prior.initialize(params);
    EXPECT_EQ(prior.mean(), 0);
    EXPECT_EQ(prior.var(), 5.);
}

TEST_F(TPriorNormalTest, initNegativeVar){
    params = "0,-1";
    EXPECT_THROW({try {prior.initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TPriorNormalTest, checkMinMax){
    params = "0,-1";
    std::string min = "0.1";
    std::string max = "1.";
    bool minIsIncluded = true;
    bool maxIsIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;

    EXPECT_TRUE(prior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
}

TEST_F(TPriorNormalTest, hastings){
    params = "0,1";
    prior.initialize(params);
    EXPECT_EQ(prior.getLogPriorRatio(5., -10.), 37.5);
    EXPECT_EQ(prior.getLogPriorRatio(5., -10.), prior.getLogPriorDensity(5.) - prior.getLogPriorDensity(-10.));
}

//-------------------------------------------

struct TPriorExponentialTest : Test {
    std::string params = "1";
    TPriorExponential<double> prior;
};

TEST_F(TPriorExponentialTest, initValid){
    prior.initialize(params);
    EXPECT_EQ(prior.lambda(), 1);
    params = "1.5";
    TPriorExponential<double> prior2;
    prior2.initialize(params);
    EXPECT_EQ(prior2.lambda(), 1.5);
}

TEST_F(TPriorExponentialTest, initZeroLambda){
    params = "0";
    EXPECT_THROW({try {prior.initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TPriorExponentialTest, initNegativeLambda){
    params = "-1";
    EXPECT_THROW({try {prior.initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TPriorExponentialTest, noLambda){
    params = "";
    EXPECT_THROW({try {prior.initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TPriorExponentialTest, checkMinDefault){
    prior.initialize(params);
    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIsIncluded = true;
    bool maxIsIncluded = true;
    bool hasDefaultMin = true;
    bool hasDefaultMax = true;

    EXPECT_FALSE(prior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(min, "0.");
    EXPECT_TRUE(minIsIncluded);
    EXPECT_FALSE(hasDefaultMin);
}

TEST_F(TPriorExponentialTest, checkMinDontOverwriteValidMin){
    prior.initialize(params);

    std::string min = "0.289";
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIsIncluded = false;
    bool maxIsIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = true;

    EXPECT_TRUE(prior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(min, "0.289");
    EXPECT_FALSE(minIsIncluded);
    EXPECT_FALSE(hasDefaultMin);
}

TEST_F(TPriorExponentialTest, checkMinDontOverwriteValidMinExcluded){
    prior.initialize(params);

    std::string min = "0";
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIsIncluded = false;
    bool maxIsIncluded = false;
    bool hasDefaultMin = false;
    bool hasDefaultMax = true;

    EXPECT_TRUE(prior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(min, "0");
    EXPECT_FALSE(minIsIncluded);
    EXPECT_FALSE(hasDefaultMin);
}

TEST_F(TPriorExponentialTest, checkMinOverwriteInvalidMin){
    prior.initialize(params);

    std::string min = "-0.289";
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIsIncluded = false;
    bool maxIsIncluded = false;
    bool hasDefaultMin = false;
    bool hasDefaultMax = true;

    EXPECT_FALSE(prior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(min, "0.");
    EXPECT_TRUE(minIsIncluded);
    EXPECT_FALSE(hasDefaultMin);
}

TEST_F(TPriorExponentialTest, hastings){
    std::string params = "1";
    TPriorExponential<double> prior;
    prior.initialize(params);
    EXPECT_EQ(prior.getLogPriorRatio(5., 10.), 5.);
    EXPECT_EQ(prior.getLogPriorRatio(5., 10.), prior.getLogPriorDensity(5.) - prior.getLogPriorDensity(10.));
}

//-------------------------------------------

struct TPriorChisqTest : Test {
    std::string params = "1";
    TPriorChisq<double> prior;
};

TEST_F(TPriorChisqTest, initValid){
    prior.initialize(params);
    EXPECT_EQ(prior.k(), 1);
}

TEST_F(TPriorChisqTest, initZero){
    params = "0";
    EXPECT_THROW({try {prior.initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TPriorChisqTest, initNegative){
    params = "-1";
    EXPECT_THROW({try {prior.initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TPriorChisqTest, initFloat){
    params = "1.5";
    EXPECT_THROW({try {prior.initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TPriorChisqTest, initNoParams){
    params = "";
    EXPECT_THROW({try {prior.initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TPriorChisqTest, checkMinDefaultK1){
    // test if min is open if k = 1 and closed otherwise
    prior.initialize(params);

    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIsIncluded = true;
    bool maxIsIncluded = true;
    bool hasDefaultMin = true;
    bool hasDefaultMax = true;

    EXPECT_FALSE(prior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(min, "0.");
    EXPECT_FALSE(minIsIncluded);
    EXPECT_FALSE(hasDefaultMin);
}

TEST_F(TPriorChisqTest, checkMinDefaultKBiggerThan1){
    // test if min is open if k = 1 and closed otherwise
    params = "2";
    prior.initialize(params);

    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIsIncluded = true;
    bool maxIsIncluded = true;
    bool hasDefaultMin = true;
    bool hasDefaultMax = true;

    EXPECT_FALSE(prior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(min, "0.");
    EXPECT_TRUE(minIsIncluded);
    EXPECT_FALSE(hasDefaultMin);
}

TEST_F(TPriorChisqTest, checkMin0K1MinIncluded){
    // test if min is open if k = 1 and closed otherwise
    prior.initialize(params);

    std::string min = "0";
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIsIncluded = true;
    bool maxIsIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = true;

    EXPECT_FALSE(prior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(min, "0");
    EXPECT_FALSE(minIsIncluded);
    EXPECT_FALSE(hasDefaultMin);
}

TEST_F(TPriorChisqTest, checkMinDontOverwriteValidMin){
    prior.initialize(params);

    std::string min = "1";
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIsIncluded = true;
    bool maxIsIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = true;

    EXPECT_TRUE(prior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(min, "1");
    EXPECT_TRUE(minIsIncluded);
    EXPECT_FALSE(hasDefaultMin);
}

TEST_F(TPriorChisqTest, hastings){
    std::string params = "1";
    TPriorChisq<double> prior;
    prior.initialize(params);
    EXPECT_FLOAT_EQ(prior.getLogPriorRatio(5., 10.), 2.846574);
    EXPECT_FLOAT_EQ(prior.getLogPriorRatio(5., 10.), prior.getLogPriorDensity(5.) - prior.getLogPriorDensity(10.));
}

//--------------------------------------------
// TPriorMultivariateChisq
template<class T>
class MockTMCMCArrayParameter : public TMCMCParameter<T> {
public:
    MockTMCMCArrayParameter(std::shared_ptr<TPrior<T>> Prior, const std::shared_ptr<TParameterDefinition> & def, TRandomGenerator* RandomGenerator)
            : TMCMCParameter<T>(Prior, def, RandomGenerator){};
    ~MockTMCMCArrayParameter() override = default;
};

class TPriorMultivariateChisqTest : public Test {
protected:
    std::shared_ptr<TPriorMultivariateChisq<double>> prior;

    std::shared_ptr<MockTMCMCArrayParameter<double>> param; // parameter on which prior is defined
    std::shared_ptr<TParameterDefinition> def;

    TRandomGenerator randomGenerator;

    void SetUp() override {
        // initialize param
        prior = std::make_shared<TPriorMultivariateChisq<double>>();
        def = std::make_shared<TParameterDefinition>("defParam");
    }

    void initializeDefinitions(){
        // parameter itself
        def->setDimensions({10});
        def->setPrior("multivariateChisq");
        def->setPriorParams("10,9,8,7,6,5,4,3,2,1");
        def->setInitVal("16.9913843841421,10.3898992460316,3.03827316667281,6.81957212048465,8.64789850337061,"
                        "6.5170836177272,1.39898459261386,0.593045827795861,0.0402791057044047,4.78553130391896");
        prior->initialize(def->parameters());
        param = std::make_shared<MockTMCMCArrayParameter<double>>(prior, def, &randomGenerator);
        // initialize storage
        std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
        param->initializeStorageBasedOnPrior({10}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)
    }

    void TearDown() override {}
};

TEST_F(TPriorMultivariateChisqTest, _getLogPriorDensity_vec){
    // first construct hyperpriors
    initializeDefinitions();

    // prior density of first element
    size_t index = 0;
    EXPECT_FLOAT_EQ(prior->getLogPriorDensity(param, index), -3.808656);

    // prior density of fifth element
    index = 4;
    EXPECT_FLOAT_EQ(prior->getLogPriorDensity(param, index), -2.781905);

    // prior density of last element
    index = 9;
    EXPECT_FLOAT_EQ(prior->getLogPriorDensity(param, index), -4.094503);
}

TEST_F(TPriorMultivariateChisqTest, _getLogPriorDensityOld_vec){
    // first construct hyperpriors
    initializeDefinitions();

    // prior density of first element
    size_t index = 0;
    double val = param->value<double>(index);
    param->set(index, 6.34);
    EXPECT_FLOAT_EQ(prior->getLogPriorDensityOld(param, index), -3.808656);

    // reset
    param->set(index, val);

    // prior density of fifth element
    index = 4;
    val = param->value<double>(index);
    param->set(index, 5.72);
    EXPECT_FLOAT_EQ(prior->getLogPriorDensityOld(param, index), -2.781905);

    // reset
    param->set(index, val);

    // prior density of last element
    index = 9;
    val = param->value<double>(index);
    param->set(index, 1.7327);
    EXPECT_FLOAT_EQ(prior->getLogPriorDensityOld(param, index), -4.094503);
}

TEST_F(TPriorMultivariateChisqTest, _getLogPriorRatio_vec){
    // first construct hyperpriors
    initializeDefinitions();

    // prior ratio of first element
    size_t index = 0;
    double val = param->value<double>(index);
    param->set(index, 6.34);
    EXPECT_FLOAT_EQ(prior->getLogPriorRatio(param, index), 1.382382);

    // reset
    param->set(index, val);

    // prior ratio of fifth element
    index = 4;
    val = param->value<double>(index);
    param->set(index, 5.72);
    EXPECT_FLOAT_EQ(prior->getLogPriorRatio(param, index), 0.6372542);

    // reset
    param->set(index, val);

    // prior ratio of last element
    index = 9;
    val = param->value<double>(index);
    param->set(index, 1.7327);
    EXPECT_FLOAT_EQ(prior->getLogPriorRatio(param, index), 2.034374);
}

TEST_F(TPriorMultivariateChisqTest, getLogPriorDensityFull){
    // first construct hyperpriors
    initializeDefinitions();

    // log prior density full
    EXPECT_FLOAT_EQ(prior->getLogPriorDensityFull(param), -24.65138);
}

//--------------------------------------------
// TPriorMultivariateChi

class TPriorMultivariateChiTest : public Test {
protected:
    std::shared_ptr<TPriorMultivariateChi<double>> prior;

    std::shared_ptr<MockTMCMCArrayParameter<double>> param; // parameter on which prior is defined
    std::shared_ptr<TParameterDefinition> def;

    TRandomGenerator randomGenerator;

    void SetUp() override {
        // initialize param
        prior = std::make_shared<TPriorMultivariateChi<double>>();
        def = std::make_shared<TParameterDefinition>("defParam");
    }

    void initializeDefinitions(){
        // parameter itself
        def->setDimensions({10});
        def->setPrior("multivariateChi");
        def->setPriorParams("10,9,8,7,6,5,4,3,2,1");
        def->setInitVal("16.9913843841421,10.3898992460316,3.03827316667281,6.81957212048465,8.64789850337061,"
                        "6.5170836177272,1.39898459261386,0.593045827795861,0.0402791057044047,4.78553130391896");
        prior->initialize(def->parameters());
        param = std::make_shared<MockTMCMCArrayParameter<double>>(prior, def, &randomGenerator);
        // initialize storage
        std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
        param->initializeStorageBasedOnPrior({10}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)
    }

    void TearDown() override {}
};

TEST_F(TPriorMultivariateChiTest, _getLogPriorDensity_vec){
    // first construct hyperpriors
    initializeDefinitions();

    // prior density of first element
    size_t index = 0;
    EXPECT_FLOAT_EQ(prior->getLogPriorDensity(param, index), -124.8098565);

    // prior density of fifth element
    index = 4;
    EXPECT_FLOAT_EQ(prior->getLogPriorDensity(param, index), -28.68593);

    // prior density of last element
    index = 9;
    EXPECT_FLOAT_EQ(prior->getLogPriorDensity(param, index), -11.67645);
}

TEST_F(TPriorMultivariateChiTest, _getLogPriorDensityOld_vec){
    // first construct hyperpriors
    initializeDefinitions();

    // prior density of first element
    size_t index = 0;
    double val = param->value<double>(index);
    param->set(index, 6.34);
    EXPECT_FLOAT_EQ(prior->getLogPriorDensityOld(param, index), -124.8098565);

    // reset
    param->set(index, val);

    // prior density of fifth element
    index = 4;
    val = param->value<double>(index);
    param->set(index, 5.72);
    EXPECT_FLOAT_EQ(prior->getLogPriorDensityOld(param, index), -28.68593);

    // reset
    param->set(index, val);

    // prior density of last element
    index = 9;
    val = param->value<double>(index);
    param->set(index, 1.7327);
    EXPECT_FLOAT_EQ(prior->getLogPriorDensityOld(param, index), -11.67645);
}

TEST_F(TPriorMultivariateChiTest, _getLogPriorRatio_vec){
    // first construct hyperpriors
    initializeDefinitions();

    // prior ratio of first element
    size_t index = 0;
    double val = param->value<double>(index);
    param->set(index, 6.34);
    EXPECT_FLOAT_EQ(prior->getLogPriorRatio(param, index), 115.3833);

    // reset
    param->set(index, val);

    // prior ratio of fifth element
    index = 4;
    val = param->value<double>(index);
    param->set(index, 5.72);
    EXPECT_FLOAT_EQ(prior->getLogPriorRatio(param, index), 18.96714);

    // reset
    param->set(index, val);

    // prior ratio of last element
    index = 9;
    val = param->value<double>(index);
    param->set(index, 1.7327);
    EXPECT_FLOAT_EQ(prior->getLogPriorRatio(param, index), 9.94953);
}

TEST_F(TPriorMultivariateChiTest, getLogPriorDensityFull){
    // first construct hyperpriors
    initializeDefinitions();

    // log prior density full
    EXPECT_FLOAT_EQ(prior->getLogPriorDensityFull(param), -241.0631);
}

//-------------------------------------------

struct TPriorBetaTest : Test {
    std::string params = "1,1";
    TPriorBeta<double> prior;
};

TEST_F(TPriorBetaTest, initValid){
    prior.initialize(params);
    EXPECT_EQ(prior.alpha(), 1);
    EXPECT_EQ(prior.beta(), 1);
}

TEST_F(TPriorBetaTest, initNegative){
    params = "-1,1";
    EXPECT_THROW({try {prior.initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    params = "1,-1";
    EXPECT_THROW({try {prior.initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    params = "-1,-1";
    EXPECT_THROW({try {prior.initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TPriorBetaTest, initZero){
    params = "0,1";
    EXPECT_THROW({try {prior.initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    params = "1,0";
    EXPECT_THROW({try {prior.initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    params = "0,0";
    EXPECT_THROW({try {prior.initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TPriorBetaTest, noParams){
    params = "";
    EXPECT_THROW({try {prior.initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TPriorBetaTest, checkMinMaxDefault){
    prior.initialize(params);

    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIsIncluded = true;
    bool maxIsIncluded = true;
    bool hasDefaultMin = true;
    bool hasDefaultMax = true;

    EXPECT_FALSE(prior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(min, "0.");
    EXPECT_EQ(max, "1.");
    EXPECT_FALSE(minIsIncluded);
    EXPECT_FALSE(maxIsIncluded);
    EXPECT_FALSE(hasDefaultMin);
    EXPECT_FALSE(hasDefaultMax);
}

TEST_F(TPriorBetaTest, checkMinIsOkMaxIsnt){
    prior.initialize(params);

    std::string min = "0";
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIsIncluded = false;
    bool maxIsIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = true;

    EXPECT_FALSE(prior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(min, "0");
    EXPECT_EQ(max, "1.");
    EXPECT_FALSE(minIsIncluded);
    EXPECT_FALSE(maxIsIncluded);
    EXPECT_FALSE(hasDefaultMin);
    EXPECT_FALSE(hasDefaultMax);
}

TEST_F(TPriorBetaTest, checkMaxIsOkMinIsnt){
    prior.initialize(params);

    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = "1";
    bool minIsIncluded = true;
    bool maxIsIncluded = true;
    bool hasDefaultMin = true;
    bool hasDefaultMax = false;

    EXPECT_FALSE(prior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(min, "0.");
    EXPECT_EQ(max, "1.");
    EXPECT_FALSE(minIsIncluded);
    EXPECT_FALSE(maxIsIncluded);
    EXPECT_FALSE(hasDefaultMin);
    EXPECT_FALSE(hasDefaultMax);
}

TEST_F(TPriorBetaTest, checkMaxIsOkMinIsntBecauseOfIncluded){
    prior.initialize(params);

    std::string min = "0";
    std::string max = "1";
    bool minIsIncluded = true;
    bool maxIsIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;

    EXPECT_FALSE(prior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(min, "0.");
    EXPECT_EQ(max, "1.");
    EXPECT_FALSE(minIsIncluded);
    EXPECT_FALSE(maxIsIncluded);
    EXPECT_FALSE(hasDefaultMin);
    EXPECT_FALSE(hasDefaultMax);
}

TEST_F(TPriorBetaTest, checkMinMaxDontOverwriteValid){
    prior.initialize(params);

    std::string min = "0.1";
    std::string max = "0.9";
    bool minIsIncluded = true;
    bool maxIsIncluded = false;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;

    EXPECT_TRUE(prior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(min, "0.1");
    EXPECT_EQ(max, "0.9");
    EXPECT_TRUE(minIsIncluded);
    EXPECT_FALSE(maxIsIncluded);
    EXPECT_FALSE(hasDefaultMin);
    EXPECT_FALSE(hasDefaultMax);
}

TEST_F(TPriorBetaTest, hastings){
    std::string params = "1,2";
    TPriorBeta<double> prior;
    prior.initialize(params);
    EXPECT_FLOAT_EQ(prior.getLogPriorRatio(0.1, 0.8), 1.504077);
    EXPECT_FLOAT_EQ(prior.getLogPriorRatio(0.1, 0.8), prior.getLogPriorDensity(0.1) - prior.getLogPriorDensity(0.8));
}


//-------------------------------------------

class TPriorBinomialTest : public Test {
protected:
    std::shared_ptr<TPriorBinomial<uint8_t>> prior;

    std::shared_ptr<MockTMCMCArrayParameter<uint8_t>> param; // parameter on which prior is defined
    std::shared_ptr<TParameterDefinition> def;

    TRandomGenerator randomGenerator;

    void SetUp() override {
        // initialize param
        prior = std::make_shared<TPriorBinomial<uint8_t>>();
        def = std::make_shared<TParameterDefinition>("defParam");
    }

    void initializeDefinitions(){
        // parameter itself
        def->setDimensions({10, 2});
        def->setPrior("binomial");
        def->setPriorParams("0.3");
        def->setInitVal("2,0,3,0,5,2,9,2,2,1,8,2,9,3,6,5,6,1,0,0");
        prior->initialize(def->parameters());
        param = std::make_shared<MockTMCMCArrayParameter<uint8_t>>(prior, def, &randomGenerator);
        // initialize storage
        std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
        param->initializeStorageBasedOnPrior({10, 2}, {dimNames, dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)
    }

    void TearDown() override {}
};

TEST_F(TPriorBinomialTest, initValid){
    // first construct hyperpriors
    initializeDefinitions();

    EXPECT_EQ(prior->p(), 0.3);
}

TEST_F(TPriorBinomialTest, invalidP){
    // first construct hyperpriors
    initializeDefinitions();

    // p can't be negative
    std::string params = "-1";
    EXPECT_THROW({try {prior->initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    // p can't be 0
    params = "0.";
    EXPECT_THROW({try {prior->initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    // p can't be 1
    params = "1.";
    EXPECT_THROW({try {prior->initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    // p can't be >1
    params = "1.2";
    EXPECT_THROW({try {prior->initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TPriorBinomialTest, noParams){
    // first construct hyperpriors
    initializeDefinitions();

    EXPECT_THROW({try {prior->initialize("");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TPriorBinomialTest, checkMinMaxDefault){
    // first construct hyperpriors
    initializeDefinitions();

    std::string min = toString(std::numeric_limits<uint8_t>::lowest());
    std::string max = toString(std::numeric_limits<uint8_t>::max());
    bool minIsIncluded = true;
    bool maxIsIncluded = true;
    bool hasDefaultMin = true;
    bool hasDefaultMax = true;

    EXPECT_TRUE(prior->checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(min, "0");
    EXPECT_TRUE(minIsIncluded);
    EXPECT_TRUE(maxIsIncluded);
    EXPECT_TRUE(hasDefaultMin);
    EXPECT_TRUE(hasDefaultMax);
}

TEST_F(TPriorBinomialTest, checkMinMaxDontOverwriteValid){
    // first construct hyperpriors
    initializeDefinitions();

    std::string min = "1";
    std::string max = "9";
    bool minIsIncluded = true;
    bool maxIsIncluded = false;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;

    EXPECT_TRUE(prior->checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(min, "1");
    EXPECT_EQ(max, "9");
    EXPECT_TRUE(minIsIncluded);
    EXPECT_FALSE(maxIsIncluded);
    EXPECT_FALSE(hasDefaultMin);
    EXPECT_FALSE(hasDefaultMax);
}

TEST_F(TPriorBinomialTest, _getLogPriorDensity_vec){
    // first construct hyperpriors
    initializeDefinitions();

    EXPECT_FLOAT_EQ(prior->getLogPriorDensity(param, 0), -0.7133499);
    EXPECT_FLOAT_EQ(prior->getLogPriorDensity(param, 4*2), -0.8675006);
    EXPECT_NEAR(prior->getLogPriorDensity(param, 9*2), 0., 10e-15); // gammaLog does not return exactly 0, but 10e-16 -> float_eq doesn't work
}

TEST_F(TPriorBinomialTest, _getLogPriorDensityOld_vec){
    // first construct hyperpriors
    initializeDefinitions();

    // modify values
    std::vector<double> newK = {1,2,1,3,0,2,2,0,1,0};
    for (size_t k = 0; k < 10; k++){
        param->set(k*2+1, newK[k]);
    }

    // still the same as before - take oldValue() for every k!
    EXPECT_FLOAT_EQ(prior->getLogPriorDensityOld(param, 0), -0.7133499);
    EXPECT_FLOAT_EQ(prior->getLogPriorDensityOld(param, 4*2), -0.8675006);
    EXPECT_NEAR(prior->getLogPriorDensityOld(param, 9*2), 0., 10e-15); // gammaLog does not return exactly 0, but 10e-16 -> float_eq doesn't work
}

TEST_F(TPriorBinomialTest, hastings){
    // first construct hyperpriors
    initializeDefinitions();

    // modify values
    std::vector<double> newK = {1,2,1,3,0,2,2,0,1,0};
    for (size_t k = 0; k < 10; k++){
        param->set(k*2+1, newK[k]);
    }

    uint32_t indexN = 0;
    EXPECT_FLOAT_EQ(prior->getLogPriorRatio(param, indexN), -0.1541507);
    EXPECT_FLOAT_EQ(prior->getLogPriorRatio(param, indexN), prior->getLogPriorDensity(param, indexN) - prior->getLogPriorDensityOld(param, indexN));

    indexN = 4*2;
    EXPECT_FLOAT_EQ(prior->getLogPriorRatio(param, indexN), 0.1541507);
    EXPECT_FLOAT_EQ(prior->getLogPriorRatio(param, indexN), prior->getLogPriorDensity(param, indexN) - prior->getLogPriorDensityOld(param, indexN));

    indexN = 9*2;
    EXPECT_FLOAT_EQ(prior->getLogPriorRatio(param, indexN), 0.);
    EXPECT_FLOAT_EQ(prior->getLogPriorRatio(param, indexN), prior->getLogPriorDensity(param, indexN) - prior->getLogPriorDensityOld(param, indexN));
}

//-------------------------------------------

struct TPriorBernouilliTest : Test {
    std::string params = "0.44";
    TPriorBernouilli<bool> prior;
};

TEST_F(TPriorBernouilliTest, initValid){
    prior.initialize(params);
    EXPECT_EQ(prior.pi(), 0.44);
}

TEST_F(TPriorBernouilliTest, initInvalidPi){
    params = "-0.1";
    EXPECT_THROW({try {prior.initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    params = "0.";
    EXPECT_THROW({try {prior.initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    params = "1.";
    EXPECT_THROW({try {prior.initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    params = "1.1";
    EXPECT_THROW({try {prior.initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TPriorBernouilliTest, noPi){
    params = "";
    EXPECT_THROW({try {prior.initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TPriorBernouilliTest, checkMinDefault){
    prior.initialize(params);

    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIsIncluded = true;
    bool maxIsIncluded = true;
    bool hasDefaultMin = true;
    bool hasDefaultMax = true;

    EXPECT_FALSE(prior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(min, "0");
    EXPECT_TRUE(minIsIncluded);
    EXPECT_FALSE(hasDefaultMin);
}

TEST_F(TPriorBernouilliTest, checkMinDontOverwriteValidMinMax){
    prior.initialize(params);

    std::string min = "0.001";
    std::string max = "0.999";
    bool minIsIncluded = true;
    bool maxIsIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;

    EXPECT_TRUE(prior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(min, "0.001");
    EXPECT_TRUE(minIsIncluded);
    EXPECT_FALSE(hasDefaultMin);
    EXPECT_EQ(max, "0.999");
    EXPECT_TRUE(maxIsIncluded);
    EXPECT_FALSE(hasDefaultMax);
}

TEST_F(TPriorBernouilliTest, checkMinOverwriteInvalidMinAndMax){
    prior.initialize(params);

    std::string min = "-0.289";
    std::string max = "1.93";
    bool minIsIncluded = false;
    bool maxIsIncluded = false;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;

    EXPECT_FALSE(prior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(min, "0");
    EXPECT_EQ(max, "1");
    EXPECT_TRUE(minIsIncluded);
    EXPECT_TRUE(maxIsIncluded);
    EXPECT_FALSE(hasDefaultMin);
    EXPECT_FALSE(hasDefaultMax);
}

TEST_F(TPriorBernouilliTest, hastings){
    std::string params = "0.4";
    TPriorBernouilli<bool> prior;
    prior.initialize(params);
    EXPECT_FLOAT_EQ(prior.getLogPriorRatio(false, true), 0.4054651);
    EXPECT_FLOAT_EQ(prior.getLogPriorRatio(false, true), prior.getLogPriorDensity(false) - prior.getLogPriorDensity(true));
    EXPECT_FLOAT_EQ(prior.getLogPriorRatio(true, false), -0.4054651);
    EXPECT_FLOAT_EQ(prior.getLogPriorRatio(true, false), prior.getLogPriorDensity(true) - prior.getLogPriorDensity(false));
}


//-------------------------------------------

class TPriorDirichletTest : public Test {
protected:
    std::shared_ptr<TPriorDirichlet<double>> prior;

    std::shared_ptr<MockTMCMCArrayParameter<double>> param; // parameter on which prior is defined
    std::shared_ptr<TParameterDefinition> def;

    TRandomGenerator randomGenerator;

    void SetUp() override {
        // initialize param
        prior = std::make_shared<TPriorDirichlet<double>>();
        def = std::make_shared<TParameterDefinition>("defParam");
    }

    void initializeDefinitions(){
        // parameter itself
        def->setDimensions({10});
        def->setPrior("dirichlet");
        def->setPriorParams("0.1,0.188888888888889,0.277777777777778,0.366666666666667,0.455555555555556,0.544444444444445,0.633333333333333,0.722222222222222,0.811111111111111,0.9");
        def->setInitVal("0.181818181818182,0.163636363636364,0.145454545454545,0.127272727272727,0.109090909090909,0.0909090909090909,0.0727272727272727,0.0545454545454545,0.0363636363636364,0.0181818181818182");
        prior->initialize(def->parameters());
        param = std::make_shared<MockTMCMCArrayParameter<double>>(prior, def, &randomGenerator);
        // initialize storage
        std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
        param->initializeStorageBasedOnPrior({10}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)
    }

    void TearDown() override {}
};

TEST_F(TPriorDirichletTest, initValid){
    // first construct hyperpriors
    initializeDefinitions();

    EXPECT_THAT(prior->alpha(), ElementsAre(0.1,0.188888888888889,0.277777777777778,0.366666666666667,0.455555555555556,0.544444444444445,0.633333333333333,0.722222222222222,0.811111111111111,0.9));
}

TEST_F(TPriorDirichletTest, initInvalidAlpha){
    EXPECT_THROW(prior->initialize("0.1, 0.2, 0."), std::runtime_error);
    EXPECT_THROW(prior->initialize("0.1, 0.2, -0.3"), std::runtime_error);
}

TEST_F(TPriorDirichletTest, checkMinDefault){
    // first construct hyperpriors
    initializeDefinitions();

    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIsIncluded = true;
    bool maxIsIncluded = true;
    bool hasDefaultMin = true;
    bool hasDefaultMax = true;

    EXPECT_FALSE(prior->checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(min, "0");
    EXPECT_FALSE(minIsIncluded);
    EXPECT_FALSE(hasDefaultMin);
    EXPECT_EQ(max, "1");
    EXPECT_TRUE(maxIsIncluded);
    EXPECT_FALSE(hasDefaultMax);
}

TEST_F(TPriorDirichletTest, checkMinDontOverwriteValidMinMax){
    // first construct hyperpriors
    initializeDefinitions();

    std::string min = "0.001";
    std::string max = "0.999";
    bool minIsIncluded = true;
    bool maxIsIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;

    EXPECT_TRUE(prior->checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(min, "0.001");
    EXPECT_TRUE(minIsIncluded);
    EXPECT_FALSE(hasDefaultMin);
    EXPECT_EQ(max, "0.999");
    EXPECT_TRUE(maxIsIncluded);
    EXPECT_FALSE(hasDefaultMax);
}

TEST_F(TPriorDirichletTest, checkMinOverwriteInvalidMinAndMax){
    // first construct hyperpriors
    initializeDefinitions();

    std::string min = "-0.289";
    std::string max = "1.93";
    bool minIsIncluded = false;
    bool maxIsIncluded = false;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;

    EXPECT_FALSE(prior->checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(min, "0");
    EXPECT_EQ(max, "1");
    EXPECT_FALSE(minIsIncluded);
    EXPECT_TRUE(maxIsIncluded);
    EXPECT_FALSE(hasDefaultMin);
    EXPECT_FALSE(hasDefaultMax);
}

TEST_F(TPriorDirichletTest, _getLogPriorDensity_vec){
    // first construct hyperpriors
    initializeDefinitions();

    EXPECT_FLOAT_EQ(prior->getLogPriorDensity(param, 0), 6.126878);
    // index doesn't matter
    EXPECT_FLOAT_EQ(prior->getLogPriorDensity(param, 9), 6.126878);
}

TEST_F(TPriorDirichletTest, _getLogPriorDensityOld_vec){
    // first construct hyperpriors
    initializeDefinitions();

    // modify values
    std::vector<double> newVals = {0.139495918701045,0.143278688150917,0.0490180575439956,0.0479300393857425,0.127627647333716,0.0912743516047731,0.0711770008483544,0.01044347693951,0.169024252259974,0.150730567231972};
    for (size_t k = 0; k < 10; k++){
        param->set(k, newVals[k]);
    }

    // still the same as before - take oldValue() for every x!
    EXPECT_FLOAT_EQ(prior->getLogPriorDensityOld(param, 0), 6.126878);
    // index doesn't matter
    EXPECT_FLOAT_EQ(prior->getLogPriorDensityOld(param, 9), 6.126878);
}

TEST_F(TPriorDirichletTest, hastings){
    // first construct hyperpriors
    initializeDefinitions();

    // modify values
    std::vector<double> newVals = {0.139495918701045,0.143278688150917,0.0490180575439956,0.0479300393857425,0.127627647333716,0.0912743516047731,0.0711770008483544,0.01044347693951,0.169024252259974,0.150730567231972};
    for (size_t k = 0; k < 10; k++){
        param->set(k, newVals[k]);
    }

    EXPECT_FLOAT_EQ(prior->getLogPriorRatio(param, 0), 1.628368);
    EXPECT_FLOAT_EQ(prior->getLogPriorRatio(param, 0), prior->getLogPriorDensity(param, 0) - prior->getLogPriorDensityOld(param, 0));

    // index doesn't matter
    EXPECT_FLOAT_EQ(prior->getLogPriorRatio(param, 9), 1.628368);
    EXPECT_FLOAT_EQ(prior->getLogPriorRatio(param, 9), prior->getLogPriorDensity(param, 0) - prior->getLogPriorDensityOld(param, 0));
}
