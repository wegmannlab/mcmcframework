//
// Created by madleina on 28.07.20.
//

#include "../TestCase.h"
#include "../../core/TMultiDimensionalStorage.h"
using namespace testing;

//-------------------------------------------
// TDimension
//-------------------------------------------

class BridgeTDimension : public TDimension {
public:
    BridgeTDimension(const std::vector<size_t> & dimensions) : TDimension(dimensions){};

    inline size_t _linearizeIndex(const std::vector<size_t> & coord) const {
        return TDimension::_linearizeIndex(coord);
    }
    void _validateCoordinates(const std::vector<size_t> & coord) {
        TDimension::_validateCoordinates(coord);
    }
};

TEST(TDimensionTest, init){
    // this is ok
    std::vector<size_t> dimensions = {2,3,4};
    EXPECT_NO_THROW(TDimension dim(dimensions));
}

TEST(TDimensionTest, _linearizeIndex_single){
    // create array consisting of single element
    std::vector<size_t> dimensions = {1};
    BridgeTDimension dim(dimensions);

    // just test valid case here
    std::vector<size_t> coord = {0};
    EXPECT_EQ(dim._linearizeIndex(coord), 0);
}

TEST(TDimensionTest, _linearizeIndex_1D_array){
    // create 1D array consisting of 5 elements
    std::vector<size_t> dimensions = {5};
    BridgeTDimension dim(dimensions);

    // just test valid cases here
    for (size_t i = 0; i < 5; i++){
        std::vector<size_t> coord = {i};
        EXPECT_EQ(dim._linearizeIndex(coord), i);
    }
}

TEST(TDimensionTest, _linearizeIndex_1D_array_fake2DRow){
    // create 2D array consisting of 1x5 elements
    std::vector<size_t> dimensions = {1, 5};
    BridgeTDimension dim(dimensions);

    // just test valid cases here
    for (size_t i = 0; i < 1; i++){
        for (size_t j = 0; j < 5; j++) {
            std::vector<size_t> coord = {i, j};
            EXPECT_EQ(dim._linearizeIndex(coord), i*5 + j);
        }
    }
}

TEST(TDimensionTest, _linearizeIndex_1D_array_fake2DCol){
    // create 2D array consisting of 1x5 elements
    std::vector<size_t> dimensions = {5, 1};
    BridgeTDimension dim(dimensions);

    // just test valid cases here
    for (size_t i = 0; i < 5; i++){
        for (size_t j = 0; j < 1; j++) {
            std::vector<size_t> coord = {i, j};
            EXPECT_EQ(dim._linearizeIndex(coord), i*1 + j);
        }
    }
}

TEST(TDimensionTest, _linearizeIndex_2D_array){
    // create 2D array consisting of 5x3 elements
    std::vector<size_t> dimensions = {5, 3};
    BridgeTDimension dim( dimensions);

    // just test valid cases here
    for (size_t i = 0; i < 5; i++){
        for (size_t j = 0; j < 3; j++) {
            std::vector<size_t> coord = {i, j};
            EXPECT_EQ(dim._linearizeIndex(coord), i*3 + j);
        }
    }
}

TEST(TDimensionTest, _linearizeIndex_3D_array){
    // create 2D array consisting of 2x3x4 elements
    std::vector<size_t> dimensions = {2, 3, 4};
    BridgeTDimension dim(dimensions);

    // just test valid cases here
    for (size_t i = 0; i < 2; i++){
        for (size_t j = 0; j < 3; j++) {
            for (size_t k = 0; k < 4; k++) {
                std::vector<size_t> coord = {i, j, k};
                EXPECT_EQ(dim._linearizeIndex(coord), k + 4 * (j + 3*i));
            }
        }
    }
}

TEST(TDimensionTest, _validateCoordinates){
    // create 2D array consisting of 2x3x4 elements
    std::vector<size_t> dimensions = {2, 3, 4};
    BridgeTDimension dim(dimensions);

    // coordinates vector does not match number of dimensions
    std::vector<size_t> coord = {0,1}; // too few
    EXPECT_THROW(dim._validateCoordinates(coord), std::runtime_error);
    coord = {0,1,2,3}; // too many
    EXPECT_THROW(dim._validateCoordinates(coord), std::runtime_error);

    // are coordinates within array?
    coord = {2, 0, 0}; // first dimension is outside array
    EXPECT_THROW(dim._validateCoordinates(coord), std::runtime_error);
    coord = {0, 3, 0}; // second dimension is outside array
    EXPECT_THROW(dim._validateCoordinates(coord), std::runtime_error);
    coord = {0, 0, 4}; // third dimension is outside array
    EXPECT_THROW(dim._validateCoordinates(coord), std::runtime_error);
}

class TDimensionTestSingle : public Test {
protected:
    std::vector<size_t> dimensions = {1};
    TDimension dim;

    double * linearArray = nullptr;

    void SetUp() override {
        dim.init(dimensions);

        // create some 1D array and fill it
        linearArray = new double [1];
        linearArray[0] = 0.1;
    }
    void TearDown() override {
        delete [] linearArray;
    }
};

TEST_F(TDimensionTestSingle, getElement){
    // go over all possible coordinates and check if value in multi-array is same as in linear array
    std::vector<size_t> coord = {0};
    size_t i = dim.getIndex(coord);
    EXPECT_EQ(linearArray[0], linearArray[i]);
}

TEST_F(TDimensionTestSingle, getRange){
    // take last coordinate as end-coordinate
    std::vector<size_t> endCoord = {0};
    std::vector<size_t> coord = {0};
    TRange range = dim.getRange(coord, endCoord);
    // now check if correct
    size_t indexInLinear = range.first;
    int c = 0;
    for (size_t i = 0; i < dimensions.at(0); i++, c++) {
        EXPECT_EQ(linearArray[c], linearArray[indexInLinear]);
        indexInLinear += range.increment;
    }
    EXPECT_EQ(indexInLinear, range.last);
}

TEST_F(TDimensionTestSingle, getFull){
    TRange range = dim.getFull();
    // now check if these values from 1D array corrspond to values in 3D array
    size_t indexInLinear = range.first;
    for (size_t i = 0; i < dimensions.at(0); i++) {
        EXPECT_EQ(linearArray[i], linearArray[indexInLinear]);
        indexInLinear += range.increment;
    }
    EXPECT_EQ(indexInLinear, range.last);
}

TEST_F(TDimensionTestSingle, get1DSlice_invalidDimension){
    std::vector<size_t> startCoord = {0};
    // this is ok
    EXPECT_NO_THROW(TRange range = dim.get1DSlice(0, startCoord));
    // this is too much
    EXPECT_THROW(TRange range = dim.get1DSlice(1, startCoord), std::runtime_error);
}

TEST_F(TDimensionTestSingle, get1DSlice_startToEndOfDim0){
    std::vector<size_t> startCoord = {0};
    // get index in linear array
    TRange range = dim.get1DSlice(0, startCoord);
    // now check if these values from 1D array corrspond to values in 2D array
    size_t indexInLinear = range.first;

    EXPECT_EQ(linearArray[0], linearArray[indexInLinear]);
    indexInLinear += range.increment;

    EXPECT_EQ(indexInLinear, range.last);
}

TEST_F(TDimensionTestSingle, getSubscripts){
    std::vector<size_t> coords = dim.getSubscripts(0);
    EXPECT_EQ(coords.size(), 1);
    EXPECT_EQ(coords[0], 0);

    // throw for all other cases
    EXPECT_THROW(dim.getSubscripts(1), std::runtime_error);
    EXPECT_THROW(dim.getSubscripts(2), std::runtime_error);
}

class TDimensionTest1D : public Test {
protected:
    std::vector<size_t> dimensions = {3};
    TDimension dim;

    double * linearArray = nullptr;

    void SetUp() override {
        dim.init(dimensions);

        // create some 1D array and fill it
        double val = 0.1;
        linearArray = new double [3];
        for (size_t i = 0; i < 3; i++){
            linearArray[i] = val;
            val += 0.1;
        }
    }
    void TearDown() override {
        delete [] linearArray;
    }
};

TEST_F(TDimensionTest1D, getElement){
    // go over all possible coordinates and check if value in multi-array is same as in linear array
    for (size_t i = 0; i < dimensions.at(0); i++){
        // get index in linear array
        std::vector<size_t> coord = {i};
        size_t z = dim.getIndex(coord);
        EXPECT_EQ(linearArray[i], linearArray[z]);
    }
}

TEST_F(TDimensionTest1D, getRange_startToEnd){
    // take last coordinate as end-coordinate
    std::vector<size_t> endCoord = {2};
    // get index in linear array
    std::vector<size_t> coord = {0};
    TRange range = dim.getRange(coord, endCoord);
    // now check if correct
    size_t indexInLinear = range.first;
    int c = 0;
    for (size_t i = 0; i < dimensions.at(0); i++, c++) {
        EXPECT_EQ(linearArray[c], linearArray[indexInLinear]);
        indexInLinear += range.increment;
    }
    EXPECT_EQ(indexInLinear, range.last);
}

TEST_F(TDimensionTest1D, getRange_endToEnd){
    // take last coordinate as end-coordinate
    std::vector<size_t> endCoord = {2};
    // get index in linear array
    std::vector<size_t> coord = {2};
    TRange range = dim.getRange(coord, endCoord);
    // now check if these values from 1D array corrspond to values in 3D array
    for (size_t r = range.first; r < range.last; r += range.increment) {
        EXPECT_EQ(linearArray[2], linearArray[r]);
    }
}

TEST_F(TDimensionTest1D, getRange_somewhereToSomewhere){
    std::vector<size_t> startCoord = {1};
    std::vector<size_t> endCoord = {2};
    // get index in linear array
    TRange range = dim.getRange(startCoord, endCoord);
    // now check if these values from 1D array corrspond to values in 3D array
    size_t indexInLinear = range.first;

    EXPECT_EQ(linearArray[1], linearArray[indexInLinear]);
    indexInLinear++;
    EXPECT_EQ(linearArray[2], linearArray[indexInLinear]);
    indexInLinear++;

    EXPECT_EQ(indexInLinear, range.last);
}

TEST_F(TDimensionTest1D, getRange_startIsLargerThanEnd){
    std::vector<size_t> startCoord = {1};
    std::vector<size_t> endCoord = {0};
    // get index in linear array
    EXPECT_THROW(TRange range = dim.getRange(startCoord, endCoord), std::runtime_error);
}

TEST_F(TDimensionTest1D, getFull){
    TRange range = dim.getFull();
    // now check if these values from 1D array corrspond to values in 3D array
    size_t indexInLinear = range.first;
    for (size_t i = 0; i < dimensions.at(0); i++) {
        EXPECT_EQ(linearArray[i], linearArray[indexInLinear]);
        indexInLinear += range.increment;
    }
    EXPECT_EQ(indexInLinear, range.last);
}


TEST_F(TDimensionTest1D, get1DSlice_invalidDimension){
    std::vector<size_t> startCoord = {0};
    // this is ok
    EXPECT_NO_THROW(TRange range = dim.get1DSlice(0, startCoord));
    // this is too much
    EXPECT_THROW(TRange range = dim.get1DSlice(1, startCoord), std::runtime_error);
}

TEST_F(TDimensionTest1D, get1DSlice_startToEndOfDim0){
    std::vector<size_t> startCoord = {0};
    // get index in linear array
    TRange range = dim.get1DSlice(0, startCoord);
    // now check if these values from 1D array corrspond to values in 2D array
    size_t indexInLinear = range.first;

    EXPECT_EQ(linearArray[0], linearArray[indexInLinear]);
    indexInLinear += range.increment;
    EXPECT_EQ(linearArray[1], linearArray[indexInLinear]);
    indexInLinear += range.increment;
    EXPECT_EQ(linearArray[2], linearArray[indexInLinear]);
    indexInLinear += range.increment;

    EXPECT_EQ(indexInLinear, range.last);
}

TEST_F(TDimensionTest1D, get1DSlice_endToEndOfDim0){
    std::vector<size_t> startCoord = {2};
    // get index in linear array
    TRange range = dim.get1DSlice(0, startCoord);
    // now check if these values from 1D array corrspond to values in 3D array

    for (size_t r = range.first; r < range.last; r += range.increment){
        EXPECT_EQ(linearArray[2], linearArray[r]);
    }

    size_t indexInLinear = range.first;
    EXPECT_EQ(linearArray[2], linearArray[indexInLinear]);
    indexInLinear += range.increment;

    EXPECT_EQ(indexInLinear, range.last);
}

TEST_F(TDimensionTest1D, getSubscripts){
    // first element
    std::vector<size_t> coords = dim.getSubscripts(0);
    EXPECT_EQ(coords.size(), 1);
    EXPECT_EQ(coords[0], 0);

    // second element
    coords = dim.getSubscripts(1);
    EXPECT_EQ(coords.size(), 1);
    EXPECT_EQ(coords[0], 1);

    // third element
    coords = dim.getSubscripts(2);
    EXPECT_EQ(coords.size(), 1);
    EXPECT_EQ(coords[0], 2);

    // throw for all other cases
    EXPECT_THROW(dim.getSubscripts(3), std::runtime_error);
    EXPECT_THROW(dim.getSubscripts(4), std::runtime_error);
}

class TDimensionTest2D : public Test {
protected:
    std::vector<size_t> dimensions = {2, 3};
    TDimension dim;

    double ** multiDimArray = nullptr;
    double * linearArray = nullptr;

    void SetUp() override {
        dim.init(dimensions);

        // create some 3D array and fill it
        // create a 1D array and fill it with the same values as the 3D array
        double val = 0.1;
        int c = 0;
        multiDimArray = new double * [2];
        linearArray = new double [2*3];
        for (size_t i = 0; i < 2; i++){
            multiDimArray[i] = new double [3];
            for (size_t j = 0; j < 3; j++){
                multiDimArray[i][j] = val;
                linearArray[c] = val;
                val += 0.1;
                c++;
            }
        }
    }
    void TearDown() override {
        for (size_t i = 0; i < 2; i++){
            delete [] multiDimArray[i];
        }
        delete [] multiDimArray;

        delete [] linearArray;
    }
};

TEST_F(TDimensionTest2D, getElement){
    // go over all possible coordinates and check if value in multi-array is same as in linear array
    for (size_t i = 0; i < dimensions.at(0); i++){
        for (size_t j = 0; j < dimensions.at(1); j++){
            // get index in linear array
            std::vector<size_t> coord = {i, j};
            size_t k = dim.getIndex(coord);
            EXPECT_EQ(multiDimArray[i][j], linearArray[k]);
        }
    }
}

TEST_F(TDimensionTest2D, getRange_startToEnd){
    // take last coordinate as end-coordinate
    std::vector<size_t> endCoord = {1,2};
    // get index in linear array
    std::vector<size_t> coord = {0, 0};
    TRange range = dim.getRange(coord, endCoord);
    // now check if these values from 1D array corrspond to values in 3D array
    size_t indexInLinear = range.first;
    for (size_t i = 0; i < dimensions.at(0); i++) {
        for (size_t j = 0; j < dimensions.at(1); j++) {
            EXPECT_EQ(multiDimArray[i][j], linearArray[indexInLinear]);
            indexInLinear += range.increment;
        }
    }
    EXPECT_EQ(indexInLinear, range.last);
}

TEST_F(TDimensionTest2D, getRange_endToEnd){
    // take last coordinate as end-coordinate
    std::vector<size_t> endCoord = {1,2};
    // get index in linear array
    std::vector<size_t> coord = {1,2};
    TRange range = dim.getRange(coord, endCoord);
    // now check if these values from 1D array corrspond to values in 3D array
    for (size_t r = range.first; r < range.last; r += range.increment) {
        EXPECT_EQ(multiDimArray[1][2], linearArray[r]);
    }
}

TEST_F(TDimensionTest2D, getRange_somewhereToSomewhere){
    std::vector<size_t> startCoord = {0, 1};
    std::vector<size_t> endCoord = {1,1};
    // get index in linear array
    TRange range = dim.getRange(startCoord, endCoord);
    // now check if these values from 1D array corrspond to values in 3D array
    size_t indexInLinear = range.first;

    EXPECT_EQ(multiDimArray[0][1], linearArray[indexInLinear]);
    indexInLinear++;
    EXPECT_EQ(multiDimArray[0][2], linearArray[indexInLinear]);
    indexInLinear++;
    EXPECT_EQ(multiDimArray[1][0], linearArray[indexInLinear]);
    indexInLinear++;
    EXPECT_EQ(multiDimArray[1][1], linearArray[indexInLinear]);
    indexInLinear++;

    EXPECT_EQ(indexInLinear, range.last);
}

TEST_F(TDimensionTest2D, getRange_startIsLargerThanEnd){
    std::vector<size_t> startCoord = {1,0};
    std::vector<size_t> endCoord = {0,2};
    // get index in linear array
    EXPECT_THROW(TRange range = dim.getRange(startCoord, endCoord), std::runtime_error);
}

TEST_F(TDimensionTest2D, getFull){
    TRange range = dim.getFull();
    // now check if these values from 1D array corrspond to values in 3D array
    size_t indexInLinear = range.first;
    for (size_t i = 0; i < dimensions.at(0); i++) {
        for (size_t j = 0; j < dimensions.at(1); j++) {
            EXPECT_EQ(multiDimArray[i][j], linearArray[indexInLinear]);
            indexInLinear += range.increment;
        }
    }
    EXPECT_EQ(indexInLinear, range.last);
}


TEST_F(TDimensionTest2D, get1DSlice_invalidDimension){
    std::vector<size_t> startCoord = {0, 0};
    // this is ok
    EXPECT_NO_THROW(TRange range = dim.get1DSlice(0, startCoord));
    EXPECT_NO_THROW(TRange range = dim.get1DSlice(1, startCoord));
    // this is too much
    EXPECT_THROW(TRange range = dim.get1DSlice(2, startCoord), std::runtime_error);
}

TEST_F(TDimensionTest2D, get1DSlice_startToEndOfDim0){
    std::vector<size_t> startCoord = {0, 0};
    // get index in linear array
    TRange range = dim.get1DSlice(0, startCoord);
    // now check if these values from 1D array corrspond to values in 2D array
    size_t indexInLinear = range.first;

    EXPECT_EQ(multiDimArray[0][0], linearArray[indexInLinear]);
    indexInLinear += range.increment;
    EXPECT_EQ(multiDimArray[1][0], linearArray[indexInLinear]);
    indexInLinear += range.increment;

    EXPECT_EQ(indexInLinear, range.last);
}

TEST_F(TDimensionTest2D, get1DSlice_startToEndOfDim1){
    std::vector<size_t> startCoord = {0, 0};
    // get index in linear array
    TRange range = dim.get1DSlice(1, startCoord);
    // now check if these values from 1D array corrspond to values in 3D array
    size_t indexInLinear = range.first;

    EXPECT_EQ(multiDimArray[0][0], linearArray[indexInLinear]);
    indexInLinear += range.increment;
    EXPECT_EQ(multiDimArray[0][1], linearArray[indexInLinear]);
    indexInLinear += range.increment;
    EXPECT_EQ(multiDimArray[0][2], linearArray[indexInLinear]);
    indexInLinear += range.increment;

    EXPECT_EQ(indexInLinear, range.last);
}

TEST_F(TDimensionTest2D, get1DSlice_endToEndOfDim0){
    std::vector<size_t> startCoord = {1, 0};
    // get index in linear array
    TRange range = dim.get1DSlice(0, startCoord);
    // now check if these values from 1D array corrspond to values in 3D array

    for (size_t r = range.first; r < range.last; r += range.increment){
        EXPECT_EQ(multiDimArray[1][0], linearArray[r]);
    }

    size_t indexInLinear = range.first;
    EXPECT_EQ(multiDimArray[1][0], linearArray[indexInLinear]);
    indexInLinear += range.increment;

    EXPECT_EQ(indexInLinear, range.last);
}

TEST_F(TDimensionTest2D, get1DSlice_endToEndOfDim1){
    std::vector<size_t> startCoord = {0, 2};
    // get index in linear array
    TRange range = dim.get1DSlice(1, startCoord);
    // now check if these values from 1D array corrspond to values in 3D array

    for (size_t r = range.first; r < range.last; r += range.increment){
        EXPECT_EQ(multiDimArray[0][2], linearArray[r]);
    }

    size_t indexInLinear = range.first;
    EXPECT_EQ(multiDimArray[0][2], linearArray[indexInLinear]);
    indexInLinear += range.increment;

    EXPECT_EQ(indexInLinear, range.last);
}

TEST_F(TDimensionTest2D, getSubscripts){
    // compute forth and back
    for (size_t linearIndex = 0; linearIndex < dim.totalSize(); linearIndex++){
        std::vector<size_t> coords = dim.getSubscripts(linearIndex);
        EXPECT_EQ(linearIndex, dim.getIndex(coords));
    }

    // throw for all other cases
    EXPECT_THROW(dim.getSubscripts(6), std::runtime_error);
    EXPECT_THROW(dim.getSubscripts(7), std::runtime_error);
}

class TDimensionTest3D : public Test {
protected:
    std::vector<size_t> dimensions = {2, 3, 4};
    TDimension dim;

    double *** multiDimArray = nullptr;
    double * linearArray = nullptr;

    void SetUp() override {
        dim.init(dimensions);

        // create some 3D array and fill it
        // create a 1D array and fill it with the same values as the 3D array
        double val = 0.1;
        int c = 0;
        multiDimArray = new double ** [2];
        linearArray = new double [2*3*4];
        for (size_t i = 0; i < 2; i++){
            multiDimArray[i] = new double * [3];
            for (size_t j = 0; j < 3; j++){
                multiDimArray[i][j] = new double [4];
                for (size_t k = 0; k < 4; k++) {
                    multiDimArray[i][j][k] = val;
                    linearArray[c] = val;
                    val += 0.1;
                    c++;
                }
            }
        }
    }
    void TearDown() override {
        for (size_t i = 0; i < 2; i++){
            for (size_t j = 0; j < 3; j++)
                delete [] multiDimArray[i][j];
            delete [] multiDimArray[i];
        }
        delete [] multiDimArray;

        delete [] linearArray;
    }
};

TEST_F(TDimensionTest3D, getElement){
    // go over all possible coordinates and check if value in multi-array is same as in linear array
    for (size_t i = 0; i < dimensions.at(0); i++){
        for (size_t j = 0; j < dimensions.at(1); j++){
            for (size_t k = 0; k < dimensions.at(2); k++){
                // get index in linear array
                std::vector<size_t> coord = {i, j, k};
                size_t z = dim.getIndex(coord);
                EXPECT_EQ(multiDimArray[i][j][k], linearArray[z]);
            }
        }
    }
}

TEST_F(TDimensionTest3D, getRange_startToEnd){
    // take last coordinate as end-coordinate
    std::vector<size_t> endCoord = {1,2,3};
    // get index in linear array
    std::vector<size_t> coord = {0, 0, 0};
    TRange range = dim.getRange(coord, endCoord);
    // now check if these values from 1D array corrspond to values in 3D array
    size_t indexInLinear = range.first;
    for (size_t i = 0; i < dimensions.at(0); i++) {
        for (size_t j = 0; j < dimensions.at(1); j++) {
            for (size_t k = 0; k < dimensions.at(2); k++) {
                EXPECT_EQ(multiDimArray[i][j][k], linearArray[indexInLinear]);
                indexInLinear += range.increment;
            }
        }
    }
    EXPECT_EQ(indexInLinear, range.last);
}

TEST_F(TDimensionTest3D, getRange_endToEnd){
    // take last coordinate as end-coordinate
    std::vector<size_t> endCoord = {1,2,3};
    // get index in linear array
    std::vector<size_t> coord = {1,2,3};
    TRange range = dim.getRange(coord, endCoord);
    // now check if these values from 1D array corrspond to values in 3D array
    for (size_t r = range.first; r < range.last; r += range.increment) {
        EXPECT_EQ(multiDimArray[1][2][3], linearArray[r]);
    }
}

TEST_F(TDimensionTest3D, getRange_somewhereToSomewhere){
    std::vector<size_t> startCoord = {0, 0, 3};
    std::vector<size_t> endCoord = {0,2,0};
    // get index in linear array
    TRange range = dim.getRange(startCoord, endCoord);
    // now check if these values from 1D array corrspond to values in 3D array
    size_t indexInLinear = range.first;

    EXPECT_EQ(multiDimArray[0][0][3], linearArray[indexInLinear]);
    indexInLinear++;
    EXPECT_EQ(multiDimArray[0][1][0], linearArray[indexInLinear]);
    indexInLinear++;
    EXPECT_EQ(multiDimArray[0][1][1], linearArray[indexInLinear]);
    indexInLinear++;
    EXPECT_EQ(multiDimArray[0][1][2], linearArray[indexInLinear]);
    indexInLinear++;
    EXPECT_EQ(multiDimArray[0][1][3], linearArray[indexInLinear]);
    indexInLinear++;
    EXPECT_EQ(multiDimArray[0][2][0], linearArray[indexInLinear]);
    indexInLinear++;

    EXPECT_EQ(indexInLinear, range.last);
}

TEST_F(TDimensionTest3D, getRange_startIsLargerThanEnd){
    std::vector<size_t> startCoord = {0, 2, 0};
    std::vector<size_t> endCoord = {0,0,1};
    // get index in linear array
    EXPECT_THROW(TRange range = dim.getRange(startCoord, endCoord), std::runtime_error);
}

TEST_F(TDimensionTest3D, getFull){
    TRange range = dim.getFull();
    // now check if these values from 1D array corrspond to values in 3D array
    size_t indexInLinear = range.first;
    for (size_t i = 0; i < dimensions.at(0); i++) {
        for (size_t j = 0; j < dimensions.at(1); j++) {
            for (size_t k = 0; k < dimensions.at(2); k++) {
                EXPECT_EQ(multiDimArray[i][j][k], linearArray[indexInLinear]);
                indexInLinear += range.increment;
            }
        }
    }
    EXPECT_EQ(indexInLinear, range.last);
}

TEST_F(TDimensionTest3D, get1DSlice_invalidDimension){
    std::vector<size_t> startCoord = {0, 0, 3};
    // this is ok
    EXPECT_NO_THROW(TRange range = dim.get1DSlice(0, startCoord));
    EXPECT_NO_THROW(TRange range = dim.get1DSlice(2, startCoord));
    // this is too much
    EXPECT_THROW(TRange range = dim.get1DSlice(3, startCoord), std::runtime_error);
}

TEST_F(TDimensionTest3D, get1DSlice_startToEndOfDim0){
    std::vector<size_t> startCoord = {0, 0, 0};
    // get index in linear array
    TRange range = dim.get1DSlice(0, startCoord);
    // now check if these values from 1D array corrspond to values in 3D array
    size_t indexInLinear = range.first;

    EXPECT_EQ(multiDimArray[0][0][0], linearArray[indexInLinear]);
    indexInLinear += range.increment;
    EXPECT_EQ(multiDimArray[1][0][0], linearArray[indexInLinear]);
    indexInLinear += range.increment;

    EXPECT_EQ(indexInLinear, range.last);
}

TEST_F(TDimensionTest3D, get1DSlice_startToEndOfDim1){
    std::vector<size_t> startCoord = {0, 0, 0};
    // get index in linear array
    TRange range = dim.get1DSlice(1, startCoord);
    // now check if these values from 1D array corrspond to values in 3D array
    size_t indexInLinear = range.first;

    EXPECT_EQ(multiDimArray[0][0][0], linearArray[indexInLinear]);
    indexInLinear += range.increment;
    EXPECT_EQ(multiDimArray[0][1][0], linearArray[indexInLinear]);
    indexInLinear += range.increment;
    EXPECT_EQ(multiDimArray[0][2][0], linearArray[indexInLinear]);
    indexInLinear += range.increment;

    EXPECT_EQ(indexInLinear, range.last);
}

TEST_F(TDimensionTest3D, get1DSlice_startToEndOfDim2){
    std::vector<size_t> startCoord = {0, 0, 0};
    // get index in linear array
    TRange range = dim.get1DSlice(2, startCoord);
    // now check if these values from 1D array corrspond to values in 3D array
    size_t indexInLinear = range.first;

    EXPECT_EQ(multiDimArray[0][0][0], linearArray[indexInLinear]);
    indexInLinear += range.increment;
    EXPECT_EQ(multiDimArray[0][0][1], linearArray[indexInLinear]);
    indexInLinear += range.increment;
    EXPECT_EQ(multiDimArray[0][0][2], linearArray[indexInLinear]);
    indexInLinear += range.increment;
    EXPECT_EQ(multiDimArray[0][0][3], linearArray[indexInLinear]);
    indexInLinear += range.increment;

    EXPECT_EQ(indexInLinear, range.last);
}

TEST_F(TDimensionTest3D, get1DSlice_endToEndOfDim0){
    std::vector<size_t> startCoord = {1, 0, 0};
    // get index in linear array
    TRange range = dim.get1DSlice(0, startCoord);
    // now check if these values from 1D array corrspond to values in 3D array

    for (size_t r = range.first; r < range.last; r += range.increment){
        EXPECT_EQ(multiDimArray[1][0][0], linearArray[r]);
    }

    size_t indexInLinear = range.first;
    EXPECT_EQ(multiDimArray[1][0][0], linearArray[indexInLinear]);
    indexInLinear += range.increment;

    EXPECT_EQ(indexInLinear, range.last);
}

TEST_F(TDimensionTest3D, get1DSlice_endToEndOfDim1){
    std::vector<size_t> startCoord = {0, 2, 0};
    // get index in linear array
    TRange range = dim.get1DSlice(1, startCoord);
    // now check if these values from 1D array corrspond to values in 3D array

    for (size_t r = range.first; r < range.last; r += range.increment){
        EXPECT_EQ(multiDimArray[0][2][0], linearArray[r]);
    }

    size_t indexInLinear = range.first;
    EXPECT_EQ(multiDimArray[0][2][0], linearArray[indexInLinear]);
    indexInLinear += range.increment;

    EXPECT_EQ(indexInLinear, range.last);
}

TEST_F(TDimensionTest3D, get1DSlice_endToEndOfDim2){
    std::vector<size_t> startCoord = {0, 0, 3};
    // get index in linear array
    TRange range = dim.get1DSlice(2, startCoord);
    // now check if these values from 1D array corrspond to values in 3D array

    for (size_t r = range.first; r < range.last; r += range.increment){
        EXPECT_EQ(multiDimArray[0][0][3], linearArray[r]);
    }

    size_t indexInLinear = range.first;
    EXPECT_EQ(multiDimArray[0][0][3], linearArray[indexInLinear]);
    indexInLinear += range.increment;

    EXPECT_EQ(indexInLinear, range.last);
}

TEST_F(TDimensionTest3D, getSubscripts){
    // compute forth and back
    for (size_t linearIndex = 0; linearIndex < dim.totalSize(); linearIndex++){
        std::vector<size_t> coords = dim.getSubscripts(linearIndex);
        EXPECT_EQ(linearIndex, dim.getIndex(coords));
    }

    // throw for all other cases
    EXPECT_THROW(dim.getSubscripts(25), std::runtime_error);
    EXPECT_THROW(dim.getSubscripts(26), std::runtime_error);
}

//-------------------------------------------
// TMultiDimensionalStorage
//-------------------------------------------
class TMultiDimensionalStorageTest : public Test {
public:
    TMultiDimensionalStorage<double> storage;
    TMultiDimensionalStorage<double> storage_1Dim;

    void SetUp() override {
        // multidimensional
        storage.init({10, 5, 2}, false, false, true);
        // multidimensional, but size 1 for each dimension
        storage_1Dim.init({1, 1, 1}, false, false, true);

        // create name pointers and add them to storage
        std::shared_ptr<TNamesEmpty> namesDim0 = std::make_shared<TNamesIndices>();
        std::shared_ptr<TNamesEmpty> namesDim1 = std::make_shared<TNamesIndicesAlphabetLowerCase>();
        std::shared_ptr<TNamesEmpty> namesDim2 = std::make_shared<TNamesIndicesAlphabetUpperCase>();
        storage.setDimensionName(namesDim0, 0);
        storage.setDimensionName(namesDim1, 1);
        storage.setDimensionName(namesDim2, 2);
        storage_1Dim.setDimensionNames({namesDim0, namesDim1, namesDim2});
    }
};

TEST_F(TMultiDimensionalStorageTest, getDimensionName){
    EXPECT_EQ((*storage.getDimensionName(0))[0], "1");
    EXPECT_EQ((*storage.getDimensionName(1))[0], "a");
    EXPECT_EQ((*storage.getDimensionName(2))[0], "A");
}

TEST_F(TMultiDimensionalStorageTest, getDimensionName_1D){
    EXPECT_EQ((*storage_1Dim.getDimensionName(0))[0], "1");
    EXPECT_EQ((*storage_1Dim.getDimensionName(1))[0], "a");
    EXPECT_EQ((*storage_1Dim.getDimensionName(2))[0], "A");
}

TEST_F(TMultiDimensionalStorageTest, fillFullDimensionName){
    std::vector<std::string> fullName;
    // linear index 0
    storage.fillFullDimensionName(0, fullName);
    EXPECT_EQ(fullName.size(), 3);
    EXPECT_EQ(fullName[0], "1");
    EXPECT_EQ(fullName[1], "a");
    EXPECT_EQ(fullName[2], "A");

    // linear index 1
    storage.fillFullDimensionName(1, fullName);
    EXPECT_EQ(fullName.size(), 3);
    EXPECT_EQ(fullName[0], "1");
    EXPECT_EQ(fullName[1], "a");
    EXPECT_EQ(fullName[2], "B");

    // linear index 2
    storage.fillFullDimensionName(2, fullName);
    EXPECT_EQ(fullName.size(), 3);
    EXPECT_EQ(fullName[0], "1");
    EXPECT_EQ(fullName[1], "b");
    EXPECT_EQ(fullName[2], "A");

    // linear index 99 (last)
    storage.fillFullDimensionName(99, fullName);
    EXPECT_EQ(fullName.size(), 3);
    EXPECT_EQ(fullName[0], "10");
    EXPECT_EQ(fullName[1], "e");
    EXPECT_EQ(fullName[2], "B");
}

TEST_F(TMultiDimensionalStorageTest, fillFullDimensionName_1D){
    std::vector<std::string> fullName;
    // linear index 0
    storage_1Dim.fillFullDimensionName(0, fullName);
    EXPECT_EQ(fullName.size(), 3);
    EXPECT_EQ(fullName[0], "1");
    EXPECT_EQ(fullName[1], "a");
    EXPECT_EQ(fullName[2], "A");
}

TEST_F(TMultiDimensionalStorageTest, fillFullDimensionName_coords){
    std::vector<std::string> fullName;
    // linear index 0
    storage.fillFullDimensionName({0,0,0}, fullName);
    EXPECT_EQ(fullName.size(), 3);
    EXPECT_EQ(fullName[0], "1");
    EXPECT_EQ(fullName[1], "a");
    EXPECT_EQ(fullName[2], "A");

    // linear index 1
    storage.fillFullDimensionName({0,0,1}, fullName);
    EXPECT_EQ(fullName.size(), 3);
    EXPECT_EQ(fullName[0], "1");
    EXPECT_EQ(fullName[1], "a");
    EXPECT_EQ(fullName[2], "B");

    // linear index 2
    storage.fillFullDimensionName({0,1,0}, fullName);
    EXPECT_EQ(fullName.size(), 3);
    EXPECT_EQ(fullName[0], "1");
    EXPECT_EQ(fullName[1], "b");
    EXPECT_EQ(fullName[2], "A");

    // linear index 99 (last)
    storage.fillFullDimensionName({9,4,1}, fullName);
    EXPECT_EQ(fullName.size(), 3);
    EXPECT_EQ(fullName[0], "10");
    EXPECT_EQ(fullName[1], "e");
    EXPECT_EQ(fullName[2], "B");
}

TEST_F(TMultiDimensionalStorageTest, fillFullDimensionName_coords_1D){
    std::vector<std::string> fullName;
    // linear index 0
    storage_1Dim.fillFullDimensionName({0,0,0}, fullName);
    EXPECT_EQ(fullName.size(), 3);
    EXPECT_EQ(fullName[0], "1");
    EXPECT_EQ(fullName[1], "a");
    EXPECT_EQ(fullName[2], "A");
}

TEST_F(TMultiDimensionalStorageTest, getFullDimensionNameAsString){
    // linear index 0
    std::string result = storage.getFullDimensionNameAsString(0);
    EXPECT_EQ(result, "1_a_A");

    // linear index 1
    result = storage.getFullDimensionNameAsString(1);
    EXPECT_EQ(result, "1_a_B");

    // linear index 2
    result = storage.getFullDimensionNameAsString(2);
    EXPECT_EQ(result, "1_b_A");

    // linear index 99 (last)
    result = storage.getFullDimensionNameAsString(99);
    EXPECT_EQ(result, "10_e_B");
}

TEST_F(TMultiDimensionalStorageTest, getFullDimensionNameAsString_1D){
    // linear index 0
    std::string result = storage_1Dim.getFullDimensionNameAsString(0);
    EXPECT_EQ(result, "1_a_A");
}

TEST_F(TMultiDimensionalStorageTest, getFullDimensionNameAsString_coords){
    // linear index 0
    std::string result = storage.getFullDimensionNameAsString({0,0,0});
    EXPECT_EQ(result, "1_a_A");

    // linear index 1
    result = storage.getFullDimensionNameAsString({0,0,1});
    EXPECT_EQ(result, "1_a_B");

    // linear index 2
    result = storage.getFullDimensionNameAsString({0,1,0});
    EXPECT_EQ(result, "1_b_A");

    // linear index 99 (last)
    result = storage.getFullDimensionNameAsString({9,4,1});
    EXPECT_EQ(result, "10_e_B");
}

TEST_F(TMultiDimensionalStorageTest, getFullDimensionNameAsString_coords_1D){
    // linear index 0
    std::string result = storage_1Dim.getFullDimensionNameAsString({0,0,0});
    EXPECT_EQ(result, "1_a_A");
}

TEST_F(TMultiDimensionalStorageTest, appendToVectorOfAllFullDimensionNames){
    std::vector<std::string> fullName;
    storage.appendToVectorOfAllFullDimensionNames(fullName);

    EXPECT_EQ(fullName.size(), 100);
    EXPECT_EQ(fullName[0], "1_a_A");
    EXPECT_EQ(fullName[1], "1_a_B");
    EXPECT_EQ(fullName[2], "1_b_A");
    EXPECT_EQ(fullName[99], "10_e_B");
}

TEST_F(TMultiDimensionalStorageTest, appendToVectorOfAllFullDimensionNames_1D){
    std::vector<std::string> fullName;
    storage_1Dim.appendToVectorOfAllFullDimensionNames(fullName);
    EXPECT_EQ(fullName.size(), 1);
    EXPECT_EQ(fullName[0], "1_a_A");
}

TEST_F(TMultiDimensionalStorageTest, appendToVectorOfAllFullDimensionNamesWithPrefix){
    std::vector<std::string> fullName;
    storage.appendToVectorOfAllFullDimensionNamesWithPrefix(fullName, "param");

    EXPECT_EQ(fullName.size(), 100);
    EXPECT_EQ(fullName[0], "param_1_a_A");
    EXPECT_EQ(fullName[1], "param_1_a_B");
    EXPECT_EQ(fullName[2], "param_1_b_A");
    EXPECT_EQ(fullName[99], "param_10_e_B");
}

TEST_F(TMultiDimensionalStorageTest, appendToVectorOfAllFullDimensionNamesWithPrefix_1D){
    std::vector<std::string> fullName;
    storage_1Dim.appendToVectorOfAllFullDimensionNamesWithPrefix(fullName, "param");

    EXPECT_EQ(fullName.size(), 1);
    EXPECT_EQ(fullName[0], "param_1_a_A");
}

//-------------------------------------------
// TSingleStorage
//-------------------------------------------
class TSingleStorageTest : public Test {
public:
    TSingleStorage<double> storage;

    void SetUp() override {
        storage.init({1}, false, false, true);
    }
};

TEST_F(TSingleStorageTest, getDimensionName){
    EXPECT_EQ((*storage.getDimensionName(0))[0], "");
}

TEST_F(TSingleStorageTest, fillFullDimensionName){
    std::vector<std::string> fullName;
    // linear index 0
    storage.fillFullDimensionName(0, fullName);
    EXPECT_EQ(fullName.size(), 1);
    EXPECT_EQ(fullName[0], "");
}

TEST_F(TSingleStorageTest, fillFullDimensionName_coords){
    std::vector<std::string> fullName;
    // linear index 0
    storage.fillFullDimensionName({0}, fullName);
    EXPECT_EQ(fullName.size(), 1);
    EXPECT_EQ(fullName[0], "");
}

TEST_F(TSingleStorageTest, getFullDimensionNameAsString){
    // linear index 0
    std::string result = storage.getFullDimensionNameAsString(0);
    EXPECT_EQ(result, "");
}

TEST_F(TSingleStorageTest, getFullDimensionNameAsString_coords){
    // linear index 0
    std::string result = storage.getFullDimensionNameAsString({0});
    EXPECT_EQ(result, "");
}

TEST_F(TSingleStorageTest, appendToVectorOfAllFullDimensionNames){
    std::vector<std::string> fullName;
    storage.appendToVectorOfAllFullDimensionNames(fullName);

    EXPECT_EQ(fullName.size(), 1);
    EXPECT_EQ(fullName[0], "");
}

TEST_F(TSingleStorageTest, appendToVectorOfAllFullDimensionNamesWithPrefix){
    std::vector<std::string> fullName;
    storage.appendToVectorOfAllFullDimensionNamesWithPrefix(fullName, "param");

    EXPECT_EQ(fullName.size(), 1);
    EXPECT_EQ(fullName[0], "param");
}
