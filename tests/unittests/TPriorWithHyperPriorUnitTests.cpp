#include "../TestCase.h"
#include "../../core/TPriorWithHyperPrior.h"
#include "../../core/TMCMCParameter.h"
using namespace testing;

//-------------------------------------------
// TPriorWithHyperPrior
//-------------------------------------------

class BridgeTPriorWithHyperPrior : public TPriorWithHyperPrior<double> {
public:
    BridgeTPriorWithHyperPrior() : TPriorWithHyperPrior(){};
    void _checkSizeParams(int actualSize, int expectedSize) override {
        TPriorWithHyperPrior::_checkSizeParams(actualSize, expectedSize);
    }
};

class TPriorWithHyperPriorTest : public Test {
public:
    std::shared_ptr<BridgeTPriorWithHyperPrior> priorWithHyperPrior;
    std::shared_ptr<TParameterDefinition> def;
    TRandomGenerator randomGenerator;
    std::shared_ptr<TMCMCParameter<double>> param;

    void SetUp() override{
        priorWithHyperPrior = std::make_shared<BridgeTPriorWithHyperPrior>();
        def = std::make_shared<TParameterDefinition>("def");
        def->setDimensions({3});
        def->setInitVal("2.1, -0.11, 1.29");
        param = std::make_shared<TMCMCParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
        std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
        param->initializeStorageBasedOnPrior({3}, {dimNames});
    }
};

TEST_F(TPriorWithHyperPriorTest, checkSize){
    EXPECT_THROW(priorWithHyperPrior->_checkSizeParams(1, 2), std::runtime_error);
    EXPECT_THROW(priorWithHyperPrior->_checkSizeParams(-1, 1), std::runtime_error);
    EXPECT_NO_THROW(priorWithHyperPrior->_checkSizeParams(2, 2));
}

TEST_F(TPriorWithHyperPriorTest, getSumLogPriorDensity){
    double sum = priorWithHyperPrior->getSumLogPriorDensity(param);
    EXPECT_EQ(sum, 0.); // just zero for base class
}

TEST_F(TPriorWithHyperPriorTest, getLogPriorDensityFull){
    double sum = priorWithHyperPrior->getLogPriorDensityFull(param);
    EXPECT_EQ(sum, 0.); // just zero for base class
}

TEST_F(TPriorWithHyperPriorTest, getLogPriorDensity){
    double dens = priorWithHyperPrior->getLogPriorDensity(param, 0);
    EXPECT_EQ(dens, 0.); // just zero for base class
}

TEST_F(TPriorWithHyperPriorTest, getLogPriorRatio){
    double dens = priorWithHyperPrior->getLogPriorRatio(param, 0);
    EXPECT_EQ(dens, 0.); // just zero for base class
}

//-------------------------------------------
// uniform

class TPriorUniform_onArray_Test : public Test {
public:
    std::string params;
    std::shared_ptr<TPriorUniform<double>> priorWithHyperPrior;
    std::shared_ptr<TParameterDefinition> def;
    TRandomGenerator randomGenerator;
    std::shared_ptr<TMCMCParameter<double>> param;

    void SetUp() override{
        priorWithHyperPrior = std::make_shared<TPriorUniform<double>>();
        def = std::make_shared<TParameterDefinition>("def");
        def->setDimensions({3});
        def->setInitVal("2.1, -0.11, 1.29");
        param = std::make_shared<TMCMCParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
        std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
        param->initializeStorageBasedOnPrior({3}, {dimNames});
    }
};

TEST_F(TPriorUniform_onArray_Test, initValid){
    EXPECT_NO_THROW(priorWithHyperPrior->initialize(params));
}

TEST_F(TPriorUniform_onArray_Test, initUniformThrowIfParametersGiven){
    params = "1.";
    EXPECT_THROW({try {priorWithHyperPrior->initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error); // too many cols
    params = "-10,10";
    EXPECT_THROW({try {priorWithHyperPrior->initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error); // too many cols
}

TEST_F(TPriorUniform_onArray_Test, checkMinMaxDefault){
    priorWithHyperPrior->initialize(params);
    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIncluded = true;
    bool maxIncluded = true;
    bool hasDefaultMin = true;
    bool hasDefaultMax = true;

    EXPECT_TRUE(priorWithHyperPrior->checkMinMax(min, max, minIncluded, maxIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(priorWithHyperPrior->getLogPriorDensity(5), 0.);
}

TEST_F(TPriorUniform_onArray_Test, checkMinMax){
    priorWithHyperPrior->initialize(params);
    std::string min = "-1.";
    std::string max = "1.";
    bool minIncluded = true;
    bool maxIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;

    EXPECT_TRUE(priorWithHyperPrior->checkMinMax(min, max, minIncluded, maxIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(0.), -0.6931472);
}

TEST_F(TPriorUniform_onArray_Test, hastings){
    priorWithHyperPrior->initialize(params);
    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIncluded = true;
    bool maxIncluded = true;
    bool hasDefaultMin = true;
    bool hasDefaultMax = true;

    priorWithHyperPrior->checkMinMax(min, max, minIncluded, maxIncluded, hasDefaultMin, hasDefaultMax);
    EXPECT_EQ(priorWithHyperPrior->getLogPriorDensity(-5.) - priorWithHyperPrior->getLogPriorDensity(5.), priorWithHyperPrior->getLogPriorRatio(-5., 5.));
}

TEST_F(TPriorUniform_onArray_Test, getSumLogPriorDensity){
    priorWithHyperPrior->initialize(params);

    double sum = priorWithHyperPrior->getSumLogPriorDensity(param);
    EXPECT_EQ(sum, 0.); // just zero for uniform
}

TEST_F(TPriorUniform_onArray_Test, getLogPriorDensityFull){
    priorWithHyperPrior->initialize(params);

    double sum = priorWithHyperPrior->getLogPriorDensityFull(param);
    EXPECT_EQ(sum, 0.); // just zero for uniform
}

TEST_F(TPriorUniform_onArray_Test, getLogPriorDensity){
    priorWithHyperPrior->initialize(params);

    double sum = priorWithHyperPrior->getLogPriorDensity(param, 0);
    EXPECT_EQ(sum, 0.); // just zero for uniform
}

TEST_F(TPriorUniform_onArray_Test, getLogPriorRatio){
    priorWithHyperPrior->initialize(params);

    double sum = priorWithHyperPrior->getLogPriorRatio(param, 0);
    EXPECT_EQ(sum, 0.); // just zero for uniform
}

struct TPriorUniform_onArray_Test_int : Test {
    std::string params;
    TPriorUniform<int> priorWithHyperPrior;
};

TEST_F(TPriorUniform_onArray_Test_int, checkMinMaxDefault){
    priorWithHyperPrior.initialize(params);
    std::string min = toString(std::numeric_limits<int>::lowest());
    std::string max = toString(std::numeric_limits<int>::max());
    bool minIsIncluded = true;
    bool maxIsIncluded = true;
    bool hasDefaultMin = true;
    bool hasDefaultMax = true;

    EXPECT_TRUE(priorWithHyperPrior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(priorWithHyperPrior.getLogPriorDensity(5), 0.);
}

TEST_F(TPriorUniform_onArray_Test_int, checkMinMax){
    priorWithHyperPrior.initialize(params);
    std::string min = "-1";
    std::string max = "1";
    bool minIsIncluded = true;
    bool maxIsIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;

    EXPECT_TRUE(priorWithHyperPrior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_FLOAT_EQ(priorWithHyperPrior.getLogPriorDensity(0.), -0.6931472);
}

TEST_F(TPriorUniform_onArray_Test_int, hastings){
    priorWithHyperPrior.initialize(params);
    std::string min = toString(std::numeric_limits<int>::lowest());
    std::string max = toString(std::numeric_limits<int>::max());
    bool minIsIncluded = true;
    bool maxIsIncluded = true;
    bool hasDefaultMin = true;
    bool hasDefaultMax = true;

    EXPECT_TRUE(priorWithHyperPrior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(priorWithHyperPrior.getLogPriorDensity(-5.) - priorWithHyperPrior.getLogPriorDensity(5.), priorWithHyperPrior.getLogPriorRatio(5., 5.));
}

struct TPriorUniform_onArray_Test_Bool : Test {
    std::string params;
    TPriorUniform<bool> priorWithHyperPrior;
};

TEST_F(TPriorUniform_onArray_Test_Bool, checkMinMaxDefault){
    priorWithHyperPrior.initialize(params);
    std::string min =  toString(std::numeric_limits<int>::lowest()); // can give any possible min and max, it will automatically set min = 0 and max = 1
    std::string max =  toString(std::numeric_limits<int>::max());
    bool minIsIncluded = true;
    bool maxIsIncluded = true;
    bool hasDefaultMin = true;
    bool hasDefaultMax = true;

    EXPECT_TRUE(priorWithHyperPrior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(priorWithHyperPrior.getLogPriorDensity(5), 0.);
}

TEST_F(TPriorUniform_onArray_Test_Bool, hastings){
    priorWithHyperPrior.initialize(params);
    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIsIncluded = true;
    bool maxIsIncluded = true;
    bool hasDefaultMin = true;
    bool hasDefaultMax = true;

    EXPECT_TRUE(priorWithHyperPrior.checkMinMax(min, max, minIsIncluded, maxIsIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_EQ(priorWithHyperPrior.getLogPriorDensity(-5.) - priorWithHyperPrior.getLogPriorDensity(5.), priorWithHyperPrior.getLogPriorRatio(5., 5.));
}

//--------------------------------------------
// TPriorNormalWithHyperPrior

class MockTPriorNormalWithHyperPrior : public TPriorNormalWithHyperPrior<double> {
public:
    MockTPriorNormalWithHyperPrior()
            : TPriorNormalWithHyperPrior() {};
    MOCK_METHOD(void, _updateMean, (), (override));
    MOCK_METHOD(void, _updateVar, (), (override));
    MOCK_METHOD(void, _updateTempVals, (), (override));
};

template<class T>
class BridgeTMCMCArrayParameter : public TMCMCParameter<T> {
public:
    BridgeTMCMCArrayParameter(std::shared_ptr<TPriorWithHyperPrior<T>> Prior, const std::shared_ptr<TParameterDefinition> & def, TRandomGenerator* RandomGenerator)
            : TMCMCParameter<T>(Prior, def, RandomGenerator){};
    ~BridgeTMCMCArrayParameter() override = default;

    TBoundary<T> getBoundary(){
        return this->_boundary;
    }
};

class BridgeTPriorNormalWithHyperPrior : public TPriorNormalWithHyperPrior<double> {
public:
    BridgeTPriorNormalWithHyperPrior() : TPriorNormalWithHyperPrior() {};
    // update mean
    double _calcLLUpdateMean() override{
        return TPriorNormalWithHyperPrior::_calcLLUpdateMean();
    }
    double _calcLogHUpdateMean() override{
        return TPriorNormalWithHyperPrior::_calcLogHUpdateMean();
    }
    // update var
    double _calcLLUpdateVar() override{
        return TPriorNormalWithHyperPrior::_calcLLUpdateVar();
    }
    double _calcLogHUpdateVar() override {
        return TPriorNormalWithHyperPrior::_calcLogHUpdateVar();
    }
    // initialize MLE
    void _setMeanToMLE(){
        TPriorNormalWithHyperPrior::_setMeanToMLE();
    }
    void _setVarToMLE(){
        TPriorNormalWithHyperPrior::_setVarToMLE();
    }

    // functions to tweak value of mean and var (for controlling updates, we can't directly access them from outside)
    void setMeanTo(double x){
        _mean->set(x);
    }
    void setVarTo(double x){
        _var->set(x);
    }
};

class TPriorNormalWithHyperPriorTest : public Test {
protected:
    std::shared_ptr<TPriorNormalWithHyperPrior<double>> priorWithHyperPrior;

    std::shared_ptr<TParameterDefinition> defMean;
    std::shared_ptr<TParameterDefinition> defVar;
    std::unique_ptr<TPrior<double>> priorMean;
    std::unique_ptr<TPrior<double>> priorVar;
    std::shared_ptr<TMCMCParameterBase> mean;
    std::shared_ptr<TMCMCParameterBase> var;

    std::shared_ptr<BridgeTMCMCArrayParameter<double>> param; // parameter on which prior is defined
    std::shared_ptr<TParameterDefinition> def;

    TRandomGenerator randomGenerator;
    std::vector<std::shared_ptr<TObservationsBase>> observations = {};

    void SetUp() override {
        defMean = std::make_shared<TParameterDefinition>("defMean");
        defVar = std::make_shared<TParameterDefinition>("defVar");
        priorWithHyperPrior = std::make_shared<TPriorNormalWithHyperPrior<double>>();

        // initialize param
        def = std::make_shared<TParameterDefinition>("defParam");
        def->setDimensions({21});
        def->setInitVal("-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10");
        param = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
        std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
        param->initializeStorageBasedOnPrior({21}, {dimNames});
    }

    void TearDown() override {}
};

TEST_F(TPriorNormalWithHyperPriorTest, initPriorWithHyperPrior_wrongNumParams){
    // first construct mean and var
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    mean = std::make_shared<TMCMCParameter<double>>(std::move(priorMean), defMean, &randomGenerator);
    var = std::make_shared<TMCMCParameter<double>>(std::move(priorVar), defVar, &randomGenerator);
    params.push_back(mean);
    params.push_back(var);
    params.push_back(var); // one too much

    // now initialize priorWithHyperPrior - throws because 3 instead of 2 parameters were given
    EXPECT_THROW(priorWithHyperPrior->initPriorWithHyperPrior(params, observations), std::runtime_error);
}

TEST_F(TPriorNormalWithHyperPriorTest, initPriorWithHyperPrior){
    // first construct mean and var
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    mean = std::make_shared<TMCMCParameter<double>>(std::move(priorMean), defMean, &randomGenerator);
    var = std::make_shared<TMCMCParameter<double>>(std::move(priorVar), defVar, &randomGenerator);
    params.push_back(mean);
    params.push_back(var);

    // now initialize priorWithHyperPrior - check if mean and var are correctly initialized
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->initializeStorage();
    EXPECT_EQ(priorWithHyperPrior->mean(), 0.);
    EXPECT_EQ(priorWithHyperPrior->var(), cutoffFloat);
}

TEST_F(TPriorNormalWithHyperPriorTest, initPriorWithHyperPrior_reSetInitValOfVar){
    // first construct mean and var
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defVar->setMin("-10.", false); // set some complete bullshit
    defVar->setMax("5.", false);
    defVar->setInitVal("-1.");
    mean = std::make_shared<TMCMCParameter<double>>(std::move(priorMean), defMean, &randomGenerator);
    var = std::make_shared<TMCMCParameter<double>>(std::move(priorVar), defVar, &randomGenerator);
    params.push_back(mean);
    params.push_back(var);

    // now initialize priorWithHyperPrior - should re-assign min and throw
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorNormalWithHyperPriorTest, getLogPriorRatio){
    // first construct mean and var
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defVar->setInitVal("1.");
    mean = std::make_shared<TMCMCParameter<double>>(std::move(priorMean), defMean, &randomGenerator);
    var = std::make_shared<TMCMCParameter<double>>(std::move(priorVar), defVar, &randomGenerator);
    params.push_back(mean);
    params.push_back(var);

    // now initialize priorWithHyperPrior
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->initializeStorage();
    // correct logPriorRatio and logPriorDensity?
    EXPECT_EQ(priorWithHyperPrior->getLogPriorRatio(5., -10.), 37.5);
    EXPECT_EQ(priorWithHyperPrior->getLogPriorRatio(5., -10.), priorWithHyperPrior->getLogPriorDensity(5.) - priorWithHyperPrior->getLogPriorDensity(-10.));
}

TEST_F(TPriorNormalWithHyperPriorTest, update_bothFixed){
    // first construct mean and var
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defMean->update(false);
    defVar->setInitVal("1.");
    defVar->update(false);
    mean = std::make_shared<TMCMCParameter<double>>(std::move(priorMean), defMean, &randomGenerator);
    var = std::make_shared<TMCMCParameter<double>>(std::move(priorVar), defVar, &randomGenerator);
    params.push_back(mean);
    params.push_back(var);

    // now initialize priorWithHyperPrior
    MockTPriorNormalWithHyperPrior mockPriorWithHyperPrior;
    EXPECT_CALL(mockPriorWithHyperPrior, _updateTempVals()).Times(1);
    mockPriorWithHyperPrior.initPriorWithHyperPrior(params, observations);
    mockPriorWithHyperPrior.initializeStorageOfPriorParameters();

    // should never update
    EXPECT_CALL(mockPriorWithHyperPrior, _updateMean).Times(0);
    EXPECT_CALL(mockPriorWithHyperPrior, _updateVar).Times(0);
    EXPECT_CALL(mockPriorWithHyperPrior, _updateTempVals()).Times(0);
    mockPriorWithHyperPrior.updateParams();
}

TEST_F(TPriorNormalWithHyperPriorTest, update_onlyVarFixed){
    // first construct mean and var
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defVar->setInitVal("1.");
    defVar->update(false);
    mean = std::make_shared<TMCMCParameter<double>>(std::move(priorMean), defMean, &randomGenerator);
    var = std::make_shared<TMCMCParameter<double>>(std::move(priorVar), defVar, &randomGenerator);
    params.push_back(mean);
    params.push_back(var);

    // now initialize priorWithHyperPrior
    MockTPriorNormalWithHyperPrior mockPriorWithHyperPrior;
    EXPECT_CALL(mockPriorWithHyperPrior, _updateTempVals()).Times(1);
    mockPriorWithHyperPrior.initPriorWithHyperPrior(params, observations);
    mockPriorWithHyperPrior.initializeStorageOfPriorParameters();

    // should never update var and tempVals
    EXPECT_CALL(mockPriorWithHyperPrior, _updateMean).Times(1);
    EXPECT_CALL(mockPriorWithHyperPrior, _updateVar).Times(0);
    EXPECT_CALL(mockPriorWithHyperPrior, _updateTempVals()).Times(0);
    mockPriorWithHyperPrior.updateParams();
}

TEST_F(TPriorNormalWithHyperPriorTest, _updateMean){
    // first construct mean and var
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defMean->setInitVal("2.");
    priorMean = std::make_unique<TPriorNormal<double>>(); // set some prior on mean, such that prior does not cancel out in hastings ratio
    priorMean->initialize("0,1");
    defVar->setInitVal("1.");
    mean = std::make_shared<TMCMCParameter<double>>(std::move(priorMean), defMean, &randomGenerator);
    var = std::make_shared<TMCMCParameter<double>>(std::move(priorVar), defVar, &randomGenerator);
    params.push_back(mean);
    params.push_back(var);

    // now initialize priorWithHyperPrior
    auto bridgePriorNormalWithHyperPrior = std::make_shared<BridgeTPriorNormalWithHyperPrior>();
    bridgePriorNormalWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgePriorNormalWithHyperPrior->initializeStorageOfPriorParameters();
    // change value of mean, such that oldValue and newValue are not the same
    bridgePriorNormalWithHyperPrior->setMeanTo(2.19);
    bridgePriorNormalWithHyperPrior->initParameter(param);

    // calculate LL
    // equivalent to: sum(dnorm(vals, mean = 2.19, sd = 1, log = T)) - sum(dnorm(vals, mean = 2, sd = 1, log = T)) in R
    EXPECT_FLOAT_EQ(bridgePriorNormalWithHyperPrior->_calcLLUpdateMean(), -8.35905);

    // calculate logH
    // equivalent to: sum(dnorm(vals, mean = 2.19, sd = 1, log = T)) - sum(dnorm(vals, mean = 2, sd = 1, log = T))
    //                 + dnorm(2.19, 0, 1, log = T) - dnorm(2, 0, 1, log = T)
    EXPECT_FLOAT_EQ(bridgePriorNormalWithHyperPrior->_calcLogHUpdateMean(), -8.35905 - 0.39805);
}

TEST_F(TPriorNormalWithHyperPriorTest, _updateMean_3params){
    // first construct mean and var
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defMean->setInitVal("2.");
    priorMean = std::make_unique<TPriorNormal<double>>(); // set some prior on mean, such that prior does not cancel out in hastings ratio
    priorMean->initialize("0,1");
    defVar->setInitVal("1.");
    mean = std::make_shared<TMCMCParameter<double>>(std::move(priorMean), defMean, &randomGenerator);
    var = std::make_shared<TMCMCParameter<double>>(std::move(priorVar), defVar, &randomGenerator);
    params.push_back(mean);
    params.push_back(var);

    // create a second parameter
    auto def2 = std::make_shared<TParameterDefinition>("defParam2");
    def2->setDimensions({10});
    def2->setInitVal("-0.626453810742332,0.183643324222082,-0.835628612410047,1.59528080213779,0.329507771815361,-0.820468384118015,0.487429052428485,0.738324705129217,0.575781351653492,-0.305388387156356");
    auto param2 = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def2, &randomGenerator);; // 2nd parameter on which prior is defined
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param2->initializeStorageBasedOnPrior({10}, {dimNames});

    // create a third parameter
    auto def3 = std::make_shared<TParameterDefinition>("defParam3");
    def3->setDimensions({50});
    def3->setInitVal("151.178116845085,38.9843236411431,-62.1240580541804,-221.46998871775,112.493091814311,-4.49336090152308,-1.61902630989461,94.3836210685299,82.1221195098089,59.3901321217509,91.8977371608218,78.2136300731067,7.45649833651906,-198.935169586337,61.982574789471,-5.61287395290008,-15.5795506705329,-147.075238389927,-47.815005510862,41.7941560199702,135.867955152904,-10.2787727342996,38.7671611559369,-5.38050405829051,-137.705955682861,-41.499456329968,-39.4289953710349,-5.93133967111857,110.002537198388,76.3175748457544,-16.4523596253587,-25.3361680136508,69.6963375404737,55.6663198673657,-68.875569454952,-70.749515696212,36.458196213683,76.8532924515416,-11.2346212150228,88.1107726454215,39.8105880367068,-61.2026393250771,34.1119691424425,-112.936309608079,143.302370170104,198.039989850586,-36.7221476466509,-104.413462631653,56.9719627442413,-13.5054603880824");
    auto param3 = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def3, &randomGenerator);; // 3rd parameter on which prior is defined
    param3->initializeStorageBasedOnPrior({50}, {dimNames});

    // now initialize priorWithHyperPrior
    auto bridgePriorNormalWithHyperPrior = std::make_shared<BridgeTPriorNormalWithHyperPrior>();
    bridgePriorNormalWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgePriorNormalWithHyperPrior->initParameter(param);
    bridgePriorNormalWithHyperPrior->initParameter(param2);
    bridgePriorNormalWithHyperPrior->initParameter(param3);
    bridgePriorNormalWithHyperPrior->initializeStorageOfPriorParameters();
    // change value of mean, such that oldValue and newValue are not the same
    bridgePriorNormalWithHyperPrior->setMeanTo(2.19);

    // calculate LL
    // equivalent to: sum(dnorm(c(vals, vals2, vals3), mean = 2.19, sd = 1, log = T)) - sum(dnorm(c(vals, vals2, vals3), mean = 2, sd = 1, log = T)) in R
    EXPECT_FLOAT_EQ(bridgePriorNormalWithHyperPrior->_calcLLUpdateMean(),  65.57328);

    // calculate logH
    // equivalent to: sum(dnorm(c(vals, vals2, vals3), mean = 2.19, sd = 1, log = T)) - sum(dnorm(c(vals, vals2, vals3), mean = 2, sd = 1, log = T))
    //                 + dnorm(2.19, 0, 1, log = T) - dnorm(2, 0, 1, log = T)
    EXPECT_FLOAT_EQ(bridgePriorNormalWithHyperPrior->_calcLogHUpdateMean(), 65.57328 - 0.39805);
}

TEST_F(TPriorNormalWithHyperPriorTest, _updateVar){
    // first construct mean and var
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defMean->setInitVal("0.");
    defVar->setInitVal("1.");
    priorVar = std::make_unique<TPriorExponential<double>>(); // set some prior on var, such that prior does not cancel out in hastings ratio
    priorVar->initialize("1");
    mean = std::make_shared<TMCMCParameter<double>>(std::move(priorMean), defMean, &randomGenerator);
    var = std::make_shared<TMCMCParameter<double>>(std::move(priorVar), defVar, &randomGenerator);
    params.push_back(mean);
    params.push_back(var);

    // now initialize priorWithHyperPrior
    auto bridgePriorNormalWithHyperPrior = std::make_shared<BridgeTPriorNormalWithHyperPrior>();
    bridgePriorNormalWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgePriorNormalWithHyperPrior->initParameter(param);
    bridgePriorNormalWithHyperPrior->initializeStorageOfPriorParameters();
    // change value of var, such that oldValue and newValue are not the same
    bridgePriorNormalWithHyperPrior->setVarTo(0.7396);

    // calculate LL
    // equivalent to: sum(dnorm(vals, mean = 0, sd = 0.86^2, log = T)) - sum(dnorm(vals, mean = 0, sd = 1, log = T)) in R
    EXPECT_FLOAT_EQ(bridgePriorNormalWithHyperPrior->_calcLLUpdateVar(), -132.3844);

    // calculate logH
    // equivalent to: sum(dnorm(vals, mean = 0, sd = 0.86^2, log = T)) - sum(dnorm(vals, mean = 0, sd = 1, log = T)) in R
    //                 + dexp(0.86^2, 1, log = T) - dexp(1, 1, log = T)
    EXPECT_FLOAT_EQ(bridgePriorNormalWithHyperPrior->_calcLogHUpdateVar(), -132.3844 + 0.2604);
}

TEST_F(TPriorNormalWithHyperPriorTest, _updateVar_3params){
    // first construct mean and var
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defMean->setInitVal("0.");
    defVar->setInitVal("1.");
    priorVar = std::make_unique<TPriorExponential<double>>(); // set some prior on var, such that prior does not cancel out in hastings ratio
    priorVar->initialize("1");
    mean = std::make_shared<TMCMCParameter<double>>(std::move(priorMean), defMean, &randomGenerator);
    var = std::make_shared<TMCMCParameter<double>>(std::move(priorVar), defVar, &randomGenerator);
    params.push_back(mean);
    params.push_back(var);

    // create a second parameter
    auto def2 = std::make_shared<TParameterDefinition>("defParam2");
    def2->setDimensions({10});
    def2->setInitVal("-0.626453810742332,0.183643324222082,-0.835628612410047,1.59528080213779,0.329507771815361,-0.820468384118015,0.487429052428485,0.738324705129217,0.575781351653492,-0.305388387156356");
    auto param2 = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def2, &randomGenerator);; // 2nd parameter on which prior is defined
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param2->initializeStorageBasedOnPrior({10}, {dimNames});

    // create a third parameter
    auto def3 = std::make_shared<TParameterDefinition>("defParam3");
    def3->setDimensions({50});
    def3->setInitVal("151.178116845085,38.9843236411431,-62.1240580541804,-221.46998871775,112.493091814311,-4.49336090152308,-1.61902630989461,94.3836210685299,82.1221195098089,59.3901321217509,91.8977371608218,78.2136300731067,7.45649833651906,-198.935169586337,61.982574789471,-5.61287395290008,-15.5795506705329,-147.075238389927,-47.815005510862,41.7941560199702,135.867955152904,-10.2787727342996,38.7671611559369,-5.38050405829051,-137.705955682861,-41.499456329968,-39.4289953710349,-5.93133967111857,110.002537198388,76.3175748457544,-16.4523596253587,-25.3361680136508,69.6963375404737,55.6663198673657,-68.875569454952,-70.749515696212,36.458196213683,76.8532924515416,-11.2346212150228,88.1107726454215,39.8105880367068,-61.2026393250771,34.1119691424425,-112.936309608079,143.302370170104,198.039989850586,-36.7221476466509,-104.413462631653,56.9719627442413,-13.5054603880824");
    auto param3 = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def3, &randomGenerator);; // 3rd parameter on which prior is defined
    param3->initializeStorageBasedOnPrior({50}, {dimNames});

    // now initialize priorWithHyperPrior
    auto bridgePriorNormalWithHyperPrior = std::make_shared<BridgeTPriorNormalWithHyperPrior>();
    bridgePriorNormalWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgePriorNormalWithHyperPrior->initParameter(param);
    bridgePriorNormalWithHyperPrior->initParameter(param2);
    bridgePriorNormalWithHyperPrior->initParameter(param3);
    bridgePriorNormalWithHyperPrior->initializeStorageOfPriorParameters();
    // change value of mean, such that oldValue and newValue are not the same
    bridgePriorNormalWithHyperPrior->setVarTo(0.7396);

    // calculate LL
    // equivalent to: sum(dnorm(c(vals, vals2, vals3), mean = 0, sd = 0.86^2, log = T)) - sum(dnorm(c(vals, vals2, vals3), mean = 0, sd = 1, log = T)) in R
    EXPECT_FLOAT_EQ(bridgePriorNormalWithHyperPrior->_calcLLUpdateVar(), -67343.48);

    // calculate logH
    // equivalent to: sum(dnorm(c(vals, vals2, vals3), mean = 0, sd = 0.86^2, log = T)) - sum(dnorm(c(vals, vals2, vals3), mean = 0, sd = 1, log = T)) in R
    //                 + dexp(0.86^2, 1, log = T) - dexp(1, 1, log = T)
    EXPECT_FLOAT_EQ(bridgePriorNormalWithHyperPrior->_calcLogHUpdateVar(), -67343.48 + 0.2604);
}

TEST_F(TPriorNormalWithHyperPriorTest, getSumLogPriorDensity){
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defMean->setInitVal("0.");
    defVar->setInitVal("1.");
    mean = std::make_shared<TMCMCParameter<double>>(std::move(priorMean), defMean, &randomGenerator);
    var = std::make_shared<TMCMCParameter<double>>(std::move(priorVar), defVar, &randomGenerator);
    params.push_back(mean);
    params.push_back(var);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    // resize param
    def = std::make_shared<TParameterDefinition>("defParam");
    def->setDimensions({3});
    param = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param->initializeStorageBasedOnPrior({3}, {dimNames});
    param->initializeStorage();
    param->set(0, 2.1);
    param->set(1, -0.11);
    param->set(2, 1.29);

    double sum = priorWithHyperPrior->getSumLogPriorDensity(param);
    EXPECT_FLOAT_EQ(sum, -5.799916);
}

TEST_F(TPriorNormalWithHyperPriorTest, getLogPriorDensityFull){
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defMean->setInitVal("0.");
    defVar->setInitVal("1.");
    priorMean = std::make_unique<TPriorNormal<double>>(); // set some prior on mean
    priorMean->initialize("0,1");
    priorVar = std::make_unique<TPriorExponential<double>>(); // set some prior on var
    priorVar->initialize("1");
    mean = std::make_shared<TMCMCParameter<double>>(std::move(priorMean), defMean, &randomGenerator);
    var = std::make_shared<TMCMCParameter<double>>(std::move(priorVar), defVar, &randomGenerator);
    params.push_back(mean);
    params.push_back(var);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    // resize param
    def = std::make_shared<TParameterDefinition>("defParam");
    def->setDimensions({3});
    param = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param->initializeStorageBasedOnPrior({3}, {dimNames});
    param->initializeStorage();
    param->set(0, 2.1);
    param->set(1, -0.11);
    param->set(2, 1.29);

    double sum = priorWithHyperPrior->getLogPriorDensityFull(param);
    EXPECT_FLOAT_EQ(sum, -7.718854);
}

TEST_F(TPriorNormalWithHyperPriorTest, getLogPriorDensity){
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defMean->setInitVal("0.");
    defVar->setInitVal("1.");
    mean = std::make_shared<TMCMCParameter<double>>(std::move(priorMean), defMean, &randomGenerator);
    var = std::make_shared<TMCMCParameter<double>>(std::move(priorVar), defVar, &randomGenerator);
    params.push_back(mean);
    params.push_back(var);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    // resize param
    def = std::make_shared<TParameterDefinition>("defParam");
    def->setDimensions({3});
    param = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param->initializeStorageBasedOnPrior({3}, {dimNames});
    param->initializeStorage();
    param->set(0, 2.1);
    param->set(1, -0.11);
    param->set(2, 1.29);

    double val = priorWithHyperPrior->getLogPriorDensity(param, 0);
    EXPECT_FLOAT_EQ(val,  -3.123939);
    val = priorWithHyperPrior->getLogPriorRatio(param, 0);
    EXPECT_FLOAT_EQ(val,  -2.205);
}

TEST_F(TPriorNormalWithHyperPriorTest, _setMeanToMLE_1param){
    // first construct mean and var
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    mean = std::make_shared<TMCMCParameter<double>>(std::move(priorMean), defMean, &randomGenerator);
    var = std::make_shared<TMCMCParameter<double>>(std::move(priorVar), defVar, &randomGenerator);
    params.push_back(mean);
    params.push_back(var);

    // now initialize priorWithHyperPrior
    auto bridgePriorNormalWithHyperPrior = std::make_shared<BridgeTPriorNormalWithHyperPrior>();
    bridgePriorNormalWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgePriorNormalWithHyperPrior->initParameter(param);
    bridgePriorNormalWithHyperPrior->initializeStorageOfPriorParameters();

    bridgePriorNormalWithHyperPrior->_setMeanToMLE();

    // calculate MLE
    // equivalent to: mean(vals) in R
    EXPECT_FLOAT_EQ(bridgePriorNormalWithHyperPrior->mean(), 0.);
}

TEST_F(TPriorNormalWithHyperPriorTest, _setMeanToMLE_3params){
    // first construct mean and var
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    mean = std::make_shared<TMCMCParameter<double>>(std::move(priorMean), defMean, &randomGenerator);
    var = std::make_shared<TMCMCParameter<double>>(std::move(priorVar), defVar, &randomGenerator);
    params.push_back(mean);
    params.push_back(var);

    // create a second parameter
    auto def2 = std::make_shared<TParameterDefinition>("defParam2");
    def2->setDimensions({10});
    def2->setInitVal("-0.626453810742332,0.183643324222082,-0.835628612410047,1.59528080213779,0.329507771815361,-0.820468384118015,0.487429052428485,0.738324705129217,0.575781351653492,-0.305388387156356");
    auto param2 = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def2, &randomGenerator);; // 2nd parameter on which prior is defined
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param2->initializeStorageBasedOnPrior({10}, {dimNames});

    // create a third parameter
    auto def3 = std::make_shared<TParameterDefinition>("defParam3");
    def3->setDimensions({50});
    def3->setInitVal("151.178116845085,38.9843236411431,-62.1240580541804,-221.46998871775,112.493091814311,-4.49336090152308,-1.61902630989461,94.3836210685299,82.1221195098089,59.3901321217509,91.8977371608218,78.2136300731067,7.45649833651906,-198.935169586337,61.982574789471,-5.61287395290008,-15.5795506705329,-147.075238389927,-47.815005510862,41.7941560199702,135.867955152904,-10.2787727342996,38.7671611559369,-5.38050405829051,-137.705955682861,-41.499456329968,-39.4289953710349,-5.93133967111857,110.002537198388,76.3175748457544,-16.4523596253587,-25.3361680136508,69.6963375404737,55.6663198673657,-68.875569454952,-70.749515696212,36.458196213683,76.8532924515416,-11.2346212150228,88.1107726454215,39.8105880367068,-61.2026393250771,34.1119691424425,-112.936309608079,143.302370170104,198.039989850586,-36.7221476466509,-104.413462631653,56.9719627442413,-13.5054603880824");
    auto param3 = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def3, &randomGenerator);; // 3rd parameter on which prior is defined
    param3->initializeStorageBasedOnPrior({50}, {dimNames});

    // now initialize priorWithHyperPrior
    auto bridgePriorNormalWithHyperPrior = std::make_shared<BridgeTPriorNormalWithHyperPrior>();
    bridgePriorNormalWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgePriorNormalWithHyperPrior->initParameter(param);
    bridgePriorNormalWithHyperPrior->initParameter(param2);
    bridgePriorNormalWithHyperPrior->initParameter(param3);
    bridgePriorNormalWithHyperPrior->initializeStorageOfPriorParameters();

    bridgePriorNormalWithHyperPrior->_setMeanToMLE();

    // calculate MLE
    // equivalent to: mean(vals) in R
    EXPECT_FLOAT_EQ(bridgePriorNormalWithHyperPrior->mean(),  6.355772);
}

TEST_F(TPriorNormalWithHyperPriorTest, _setVarToMLE_1param){
    // first construct mean and var
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    mean = std::make_shared<TMCMCParameter<double>>(std::move(priorMean), defMean, &randomGenerator);
    var = std::make_shared<TMCMCParameter<double>>(std::move(priorVar), defVar, &randomGenerator);
    params.push_back(mean);
    params.push_back(var);

    // now initialize priorWithHyperPrior
    auto bridgePriorNormalWithHyperPrior = std::make_shared<BridgeTPriorNormalWithHyperPrior>();
    bridgePriorNormalWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgePriorNormalWithHyperPrior->initParameter(param);
    bridgePriorNormalWithHyperPrior->initializeStorageOfPriorParameters();

    bridgePriorNormalWithHyperPrior->_setMeanToMLE();
    bridgePriorNormalWithHyperPrior->_setVarToMLE();

    // calculate MLE
    // equivalent to: 1/length(vals) * sum((vals - mean(vals))^2) in R (not exactly the same as var(vals), because var divides by N-1)
    EXPECT_FLOAT_EQ(bridgePriorNormalWithHyperPrior->var(), 36.66667);
}

TEST_F(TPriorNormalWithHyperPriorTest, _setVarToMLE_3params){
    // first construct mean and var
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    mean = std::make_shared<TMCMCParameter<double>>(std::move(priorMean), defMean, &randomGenerator);
    var = std::make_shared<TMCMCParameter<double>>(std::move(priorVar), defVar, &randomGenerator);
    params.push_back(mean);
    params.push_back(var);

    // create a second parameter
    auto def2 = std::make_shared<TParameterDefinition>("defParam2");
    def2->setDimensions({10});
    def2->setInitVal("-0.626453810742332,0.183643324222082,-0.835628612410047,1.59528080213779,0.329507771815361,-0.820468384118015,0.487429052428485,0.738324705129217,0.575781351653492,-0.305388387156356");
    auto param2 = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def2, &randomGenerator);; // 2nd parameter on which prior is defined
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param2->initializeStorageBasedOnPrior({10}, {dimNames});

    // create a third parameter
    auto def3 = std::make_shared<TParameterDefinition>("defParam3");
    def3->setDimensions({50});
    def3->setInitVal("151.178116845085,38.9843236411431,-62.1240580541804,-221.46998871775,112.493091814311,-4.49336090152308,-1.61902630989461,94.3836210685299,82.1221195098089,59.3901321217509,91.8977371608218,78.2136300731067,7.45649833651906,-198.935169586337,61.982574789471,-5.61287395290008,-15.5795506705329,-147.075238389927,-47.815005510862,41.7941560199702,135.867955152904,-10.2787727342996,38.7671611559369,-5.38050405829051,-137.705955682861,-41.499456329968,-39.4289953710349,-5.93133967111857,110.002537198388,76.3175748457544,-16.4523596253587,-25.3361680136508,69.6963375404737,55.6663198673657,-68.875569454952,-70.749515696212,36.458196213683,76.8532924515416,-11.2346212150228,88.1107726454215,39.8105880367068,-61.2026393250771,34.1119691424425,-112.936309608079,143.302370170104,198.039989850586,-36.7221476466509,-104.413462631653,56.9719627442413,-13.5054603880824");
    auto param3 = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def3, &randomGenerator);; // 3rd parameter on which prior is defined
    param3->initializeStorageBasedOnPrior({50}, {dimNames});

    // now initialize priorWithHyperPrior
    auto bridgePriorNormalWithHyperPrior = std::make_shared<BridgeTPriorNormalWithHyperPrior>();
    bridgePriorNormalWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgePriorNormalWithHyperPrior->initParameter(param);
    bridgePriorNormalWithHyperPrior->initParameter(param2);
    bridgePriorNormalWithHyperPrior->initParameter(param3);
    bridgePriorNormalWithHyperPrior->initializeStorageOfPriorParameters();

    bridgePriorNormalWithHyperPrior->_setMeanToMLE();
    bridgePriorNormalWithHyperPrior->_setVarToMLE();

    // calculate MLE
    // equivalent to: 1/(length(vals) + length(vals2) + length(vals3)) * sum((c(vals, vals2, vals3) - mean(c(vals, vals2, vals3)))^2) in R
    // (not exactly the same as var(c(vals, vals2, vals3)), because var divides by N-1)
    EXPECT_FLOAT_EQ(bridgePriorNormalWithHyperPrior->var(),  4683.227);
}

//--------------------------------------------
// TPriorExponentialWithHyperPrior

class MockTPriorExponentialWithHyperPrior : public TPriorExponentialWithHyperPrior<double> {
public:
    MockTPriorExponentialWithHyperPrior()
            : TPriorExponentialWithHyperPrior() {};
    MOCK_METHOD(void, _updateLambda, (), (override));
    MOCK_METHOD(void, _updateTempVals, (), (override));
};

class BridgeTPriorExponentialWithHyperPrior : public TPriorExponentialWithHyperPrior<double> {
public:
    BridgeTPriorExponentialWithHyperPrior() : TPriorExponentialWithHyperPrior() {};
    // update lambda
    double _calcLLUpdateLambda() override{
        return TPriorExponentialWithHyperPrior::_calcLLUpdateLambda();
    }
    double _calcLogHUpdateLambda() override{
        return TPriorExponentialWithHyperPrior::_calcLogHUpdateLambda();
    }
    void _setLambdaToMLE(){
        TPriorExponentialWithHyperPrior::_setLambdaToMLE();
    }

    // functions to tweak value of lambda (for controlling updates, we can't directly access them from outside)
    void setLambdaTo(double x){
        _lambda->set(x);
    }
};

class TPriorExponentialWithHyperPriorTest : public Test {
protected:
    std::shared_ptr<TPriorExponentialWithHyperPrior<double>> priorWithHyperPrior;

    std::shared_ptr<TParameterDefinition> defLambda;
    std::unique_ptr<TPrior<double>> priorLambda;
    std::shared_ptr<TMCMCParameter<double>> lambda;

    std::shared_ptr<BridgeTMCMCArrayParameter<double>> param; // parameter on which prior is defined
    std::shared_ptr<TParameterDefinition> def;

    TRandomGenerator randomGenerator;
    std::vector<std::shared_ptr<TObservationsBase>> observations = {};

    void SetUp() override {
        defLambda = std::make_shared<TParameterDefinition>("defLambda");

        // initialize param
        priorWithHyperPrior = std::make_shared<TPriorExponentialWithHyperPrior<double>>();
        def = std::make_shared<TParameterDefinition>("defParam");
        def->setDimensions({21});
        def->setInitVal("0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20");
        param = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
        std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
        param->initializeStorageBasedOnPrior({21}, {dimNames});
    }
    void TearDown() override {}
};

TEST_F(TPriorExponentialWithHyperPriorTest, initPriorWithHyperPrior_wrongNumParams){
    // first construct lambda
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    lambda = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda), defLambda, &randomGenerator);
    // leave parama empty

    // now initialize priorWithHyperPrior - throws because 0 instead of 1 parameter were given
    EXPECT_THROW(priorWithHyperPrior->initPriorWithHyperPrior(params, observations), std::runtime_error);
}

TEST_F(TPriorExponentialWithHyperPriorTest, initPriorWithHyperPrior){
    // first construct lambda
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    lambda = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda), defLambda, &randomGenerator);
    params.push_back(lambda);

    // now initialize priorWithHyperPrior - check if lambda is correctly initialized
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->initializeStorage();
    EXPECT_EQ(priorWithHyperPrior->lambda(), cutoffFloat);
}

TEST_F(TPriorExponentialWithHyperPriorTest, initPriorWithHyperPrior_reSetInitValOfLambda){
    // first construct lambda
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defLambda->setMin("-10.", false); // set some complete bullshit
    defLambda->setMax("5.", false);
    defLambda->setInitVal("-1.");
    lambda = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda), defLambda, &randomGenerator); // this works, because everything is ok (we don't know yet that lambda must be > 0)
    params.push_back(lambda);

    // now initialize priorWithHyperPrior - should re-assign min throw because of invalid initVal of lambda
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorExponentialWithHyperPriorTest, getLogPriorRatio){
    // first construct lambda
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defLambda->setInitVal("1.");
    lambda = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda), defLambda, &randomGenerator);
    params.push_back(lambda);

    // now initialize priorWithHyperPrior
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->initializeStorage();
    // correct logPriorRatio and logPriorDensity?
    EXPECT_EQ(priorWithHyperPrior->getLogPriorRatio(5., 10.), 5.);
    EXPECT_EQ(priorWithHyperPrior->getLogPriorRatio(5., 10.), priorWithHyperPrior->getLogPriorDensity(5.) - priorWithHyperPrior->getLogPriorDensity(10.));
}

TEST_F(TPriorExponentialWithHyperPriorTest, update_Fixed){
    // first construct lambda
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defLambda->update(false);
    defLambda->setInitVal("1.");
    lambda = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda), defLambda, &randomGenerator);
    params.push_back(lambda);

    // now initialize priorWithHyperPrior
    MockTPriorExponentialWithHyperPrior mockPriorWithHyperPrior;
    EXPECT_CALL(mockPriorWithHyperPrior, _updateTempVals()).Times(1);
    mockPriorWithHyperPrior.initPriorWithHyperPrior(params, observations);
    mockPriorWithHyperPrior.initializeStorageOfPriorParameters();

    // should never update
    EXPECT_CALL(mockPriorWithHyperPrior, _updateLambda).Times(0);
    EXPECT_CALL(mockPriorWithHyperPrior, _updateTempVals()).Times(0);
    mockPriorWithHyperPrior.updateParams();
}

TEST_F(TPriorExponentialWithHyperPriorTest, update_NotFixed){
    // first construct lambda
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defLambda->setInitVal("1.");
    lambda = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda), defLambda, &randomGenerator);
    params.push_back(lambda);

    // now initialize priorWithHyperPrior
    MockTPriorExponentialWithHyperPrior mockPriorWithHyperPrior;
    EXPECT_CALL(mockPriorWithHyperPrior, _updateTempVals()).Times(1);
    mockPriorWithHyperPrior.initPriorWithHyperPrior(params, observations);
    mockPriorWithHyperPrior.initializeStorageOfPriorParameters();

    // should update
    EXPECT_CALL(mockPriorWithHyperPrior, _updateLambda).Times(1);
    EXPECT_CALL(mockPriorWithHyperPrior, _updateTempVals()).Times(1);
    mockPriorWithHyperPrior.updateParams();
}

TEST_F(TPriorExponentialWithHyperPriorTest, _updateLambda){
    // first construct lambda
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defLambda->setInitVal("1.");
    priorLambda = std::make_unique<TPriorExponential<double>>(); // set some prior on lambda, such that prior does not cancel out in hastings ratio
    priorLambda->initialize("1");
    lambda = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda), defLambda, &randomGenerator);
    params.push_back(lambda);

    // now initialize priorWithHyperPrior
    auto bridgePriorExponentialWithHyperPrior = std::make_shared<BridgeTPriorExponentialWithHyperPrior>();
    bridgePriorExponentialWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgePriorExponentialWithHyperPrior->initializeStorageOfPriorParameters();
    // change value of lambda, such that oldValue and newValue are not the same
    bridgePriorExponentialWithHyperPrior->setLambdaTo(0.86);
    bridgePriorExponentialWithHyperPrior->initParameter(param);

    // calculate LL
    // equivalent to: sum(dexp(vals, 0.86, log = T)) - sum(dexp(vals, 1, log = T)) in R
    EXPECT_FLOAT_EQ(bridgePriorExponentialWithHyperPrior->_calcLLUpdateLambda(), 26.23272);

    // calculate logH
    // equivalent to: sum(dexp(vals, 0.86, log = T)) - sum(dexp(vals, 1, log = T)) in R
    //                 + dexp(0.86, 1, log = T) - dexp(1, 1, log = T)
    EXPECT_FLOAT_EQ(bridgePriorExponentialWithHyperPrior->_calcLogHUpdateLambda(), 26.23272 + 0.14);
}

TEST_F(TPriorExponentialWithHyperPriorTest, _updateLambda_3params){
    // first construct lambda
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defLambda->setInitVal("1.");
    priorLambda = std::make_unique<TPriorExponential<double>>(); // set some prior on lambda, such that prior does not cancel out in hastings ratio
    priorLambda->initialize("1");
    lambda = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda), defLambda, &randomGenerator);
    params.push_back(lambda);

    // create a second parameter
    auto def2 = std::make_shared<TParameterDefinition>("defParam2");
    def2->setDimensions({10});
    def2->setInitVal("0.755181833128345,1.18164277910711,0.145706726703793,0.139795261868498,0.436068625779175,2.89496853746407,1.22956205341489,0.539682839997113,0.956567493698454,0.147045990503953");
    auto param2 = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def2, &randomGenerator);; // 2nd parameter on which prior is defined
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param2->initializeStorageBasedOnPrior({10}, {dimNames});

    // create a third parameter
    auto def3 = std::make_shared<TParameterDefinition>("defParam3");
    def3->setDimensions({50});
    def3->setInitVal("0.695367564405215,0.381014927735037,0.618801775367415,2.21196710884142,0.527271583655334,0.517621973015371,0.938017586204124,0.327373318606988,0.168466738192365,0.29423986072652,1.18225762651182,0.320946294115856,0.147060193819925,0.28293276228942,0.0530363116413355,0.0297195801977068,0.289356231689453,1.97946642608124,0.586656052910385,0.498406477621922,0.717642671686538,0.0186342631932348,0.162005076417699,0.660233964656896,0.101755175015783,0.511362938655025,0.15087046707049,0.362607151714569,0.375771345862542,0.117513725435484,0.539940568597472,0.514123451875007,0.64613082383708,0.626552677226669,0.277320698834956,0.150641498187325,0.646562328075475,0.497277894036329,0.257087148027495,1.0039162011397,0.211121222469956,1.08938628411976,1.60889450887402,0.278914677444845,0.29730882588774,0.488697902911503,0.104933290276676,0.154723928077146,0.552968134151256,0.387093882055438");
    auto param3 = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def3, &randomGenerator);; // 3rd parameter on which prior is defined
    param3->initializeStorageBasedOnPrior({50}, {dimNames});

    // now initialize priorWithHyperPrior
    auto bridgePriorExponentialWithHyperPrior = std::make_shared<BridgeTPriorExponentialWithHyperPrior>();
    bridgePriorExponentialWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgePriorExponentialWithHyperPrior->initParameter(param);
    bridgePriorExponentialWithHyperPrior->initParameter(param2);
    bridgePriorExponentialWithHyperPrior->initParameter(param3);
    bridgePriorExponentialWithHyperPrior->initializeStorageOfPriorParameters();
    // change value of lambda, such that oldValue and newValue are not the same
    bridgePriorExponentialWithHyperPrior->setLambdaTo(0.86);
    // calculate LL
    // equivalent to: sum(dexp(c(vals, vals2, vals3), 0.86, log = T)) - sum(dexp(c(vals, vals2, vals3), 1, log = T)) in R
    EXPECT_FLOAT_EQ(bridgePriorExponentialWithHyperPrior->_calcLLUpdateLambda(), 21.94169);

    // calculate logH
    // equivalent to: sum(dexp(c(vals, vals2, vals3), 0.86, log = T)) - sum(dexp(c(vals, vals2, vals3), 1, log = T)) in R
    //                 + dexp(0.86, 1, log = T) - dexp(1, 1, log = T)
    EXPECT_FLOAT_EQ(bridgePriorExponentialWithHyperPrior->_calcLogHUpdateLambda(), 21.94169 + 0.14);
}

TEST_F(TPriorExponentialWithHyperPriorTest, getSumLogPriorDensity){
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defLambda->setInitVal("1.");
    lambda = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda), defLambda, &randomGenerator);
    params.push_back(lambda);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    // resize param
    def = std::make_shared<TParameterDefinition>("defParam");
    def->setDimensions({3});
    param = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param->initializeStorageBasedOnPrior({3}, {dimNames});
    param->initializeStorage();
    param->set(0, 2.1);
    param->set(1, 0.11);
    param->set(2, 0.29);

    double sum = priorWithHyperPrior->getSumLogPriorDensity(param);
    EXPECT_FLOAT_EQ(sum, -2.5);
}

TEST_F(TPriorExponentialWithHyperPriorTest, getLogPriorDensityFull){
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defLambda->setInitVal("1.");
    priorLambda = std::make_unique<TPriorExponential<double>>(); // set some prior on lambda
    priorLambda->initialize("1");
    lambda = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda), defLambda, &randomGenerator);
    params.push_back(lambda);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    // resize param
    def->setDimensions({3});
    def->setInitVal("2.1, 0.11, 0.29");
    param = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param->initializeStorageBasedOnPrior({3}, {dimNames});
    param->initializeStorage();

    double sum = priorWithHyperPrior->getLogPriorDensityFull(param);
    EXPECT_FLOAT_EQ(sum, -3.5);
}

TEST_F(TPriorExponentialWithHyperPriorTest, getLogPriorDensityAndRatio){
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defLambda->setInitVal("1.");
    lambda = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda), defLambda, &randomGenerator);
    params.push_back(lambda);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    // resize param
    def = std::make_shared<TParameterDefinition>("defParam");
    def->setDimensions({3});
    param = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param->initializeStorageBasedOnPrior({3}, {dimNames});
    param->initializeStorage();
    param->set(0, 2.1);
    param->set(1, 0.11);
    param->set(2, 0.29);

    double val = priorWithHyperPrior->getLogPriorDensity(param, 0);
    EXPECT_FLOAT_EQ(val, -2.1);
    val = priorWithHyperPrior->getLogPriorRatio(param, 0);
    EXPECT_FLOAT_EQ(val, -2.1);
}

TEST_F(TPriorExponentialWithHyperPriorTest, _setLambdaToMLE_1param){
    // first construct lambda
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defLambda->setInitVal("1.");
    lambda = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda), defLambda, &randomGenerator);
    params.push_back(lambda);

    // now initialize priorWithHyperPrior
    auto bridgePriorExponentialWithHyperPrior = std::make_shared<BridgeTPriorExponentialWithHyperPrior>();
    bridgePriorExponentialWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgePriorExponentialWithHyperPrior->initParameter(param);
    bridgePriorExponentialWithHyperPrior->initializeStorageOfPriorParameters();

    bridgePriorExponentialWithHyperPrior->_setLambdaToMLE();

    // calculate LL
    // equivalent to: 1/mean(vals) in R
    EXPECT_FLOAT_EQ(bridgePriorExponentialWithHyperPrior->lambda(), 0.1);
}

TEST_F(TPriorExponentialWithHyperPriorTest, _setLambdaToMLE_3params){
    // first construct lambda
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defLambda->setInitVal("1.");
    lambda = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda), defLambda, &randomGenerator);
    params.push_back(lambda);

    // create a second parameter
    auto def2 = std::make_shared<TParameterDefinition>("defParam2");
    def2->setDimensions({10});
    def2->setInitVal("0.755181833128345,1.18164277910711,0.145706726703793,0.139795261868498,0.436068625779175,2.89496853746407,1.22956205341489,0.539682839997113,0.956567493698454,0.147045990503953");
    auto param2 = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def2, &randomGenerator);; // 2nd parameter on which prior is defined
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param2->initializeStorageBasedOnPrior({10}, {dimNames});

    // create a third parameter
    auto def3 = std::make_shared<TParameterDefinition>("defParam3");
    def3->setDimensions({50});
    def3->setInitVal("0.695367564405215,0.381014927735037,0.618801775367415,2.21196710884142,0.527271583655334,0.517621973015371,0.938017586204124,0.327373318606988,0.168466738192365,0.29423986072652,1.18225762651182,0.320946294115856,0.147060193819925,0.28293276228942,0.0530363116413355,0.0297195801977068,0.289356231689453,1.97946642608124,0.586656052910385,0.498406477621922,0.717642671686538,0.0186342631932348,0.162005076417699,0.660233964656896,0.101755175015783,0.511362938655025,0.15087046707049,0.362607151714569,0.375771345862542,0.117513725435484,0.539940568597472,0.514123451875007,0.64613082383708,0.626552677226669,0.277320698834956,0.150641498187325,0.646562328075475,0.497277894036329,0.257087148027495,1.0039162011397,0.211121222469956,1.08938628411976,1.60889450887402,0.278914677444845,0.29730882588774,0.488697902911503,0.104933290276676,0.154723928077146,0.552968134151256,0.387093882055438");
    auto param3 = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def3, &randomGenerator);; // 3rd parameter on which prior is defined
    param3->initializeStorageBasedOnPrior({50}, {dimNames});

    // now initialize priorWithHyperPrior
    auto bridgePriorExponentialWithHyperPrior = std::make_shared<BridgeTPriorExponentialWithHyperPrior>();
    bridgePriorExponentialWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgePriorExponentialWithHyperPrior->initParameter(param);
    bridgePriorExponentialWithHyperPrior->initParameter(param2);
    bridgePriorExponentialWithHyperPrior->initParameter(param3);
    bridgePriorExponentialWithHyperPrior->initializeStorageOfPriorParameters();

    bridgePriorExponentialWithHyperPrior->_setLambdaToMLE();

    // calculate LL
    // equivalent to: 1/mean(c(vals, vals2, vals3)) in R
    EXPECT_FLOAT_EQ(bridgePriorExponentialWithHyperPrior->lambda(), 0.3319833);
}


//-------------------------------------------
// chisq

class TPriorChisq_onArray_Test : public Test {
public:
    std::string params;
    std::shared_ptr<TPriorChisq<double>> priorWithHyperPrior;
    std::shared_ptr<TParameterDefinition> def;
    TRandomGenerator randomGenerator;
    std::shared_ptr<TMCMCParameter<double>> param;

    void SetUp() override{
        params = "1";

        priorWithHyperPrior = std::make_shared<TPriorChisq<double>>();
        def = std::make_shared<TParameterDefinition>("def");
        param = std::make_shared<TMCMCParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
        param->initializeStorageSingleElementBasedOnPrior();
    }
};

TEST_F(TPriorChisq_onArray_Test, initValid){
    params = "1";
    EXPECT_NO_THROW(priorWithHyperPrior->initialize(params));
}

TEST_F(TPriorChisq_onArray_Test, initThrowIfWrongParametersGiven){
    params = "";

    EXPECT_THROW({try {priorWithHyperPrior->initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error); // too many cols
    params = "-10";
    EXPECT_THROW({try {priorWithHyperPrior->initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error); // too many cols
    params = "10,10";
    EXPECT_THROW({try {priorWithHyperPrior->initialize(params);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error); // too many cols
}

TEST_F(TPriorChisq_onArray_Test, checkMinMaxDefault){
    priorWithHyperPrior->initialize(params);
    std::string min = "0."; // will be 0 when I call checkMinMaxPrior() when initializing the priorWithHyperPrior
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIncluded = false;
    bool maxIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = true;

    EXPECT_TRUE(priorWithHyperPrior->checkMinMax(min, max, minIncluded, maxIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(5.), -4.223657);
}

TEST_F(TPriorChisq_onArray_Test, checkMinMax){
    priorWithHyperPrior->initialize(params);
    std::string min =  "0."; // will be 0 when I call checkMinMaxPrior() when initializing the priorWithHyperPrior
    std::string max = "1.";
    bool minIncluded = false;
    bool maxIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;

    EXPECT_TRUE(priorWithHyperPrior->checkMinMax(min, max, minIncluded, maxIncluded, hasDefaultMin, hasDefaultMax));
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(0.1), 0.182354);
}

TEST_F(TPriorChisq_onArray_Test, hastings){
    priorWithHyperPrior->initialize(params);
    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIncluded = true;
    bool maxIncluded = true;
    bool hasDefaultMin = true;
    bool hasDefaultMax = true;

    priorWithHyperPrior->checkMinMax(min, max, minIncluded, maxIncluded, hasDefaultMin, hasDefaultMax);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(0.1) - priorWithHyperPrior->getLogPriorDensity(5.), priorWithHyperPrior->getLogPriorRatio(0.1, 5.));
}

TEST_F(TPriorChisq_onArray_Test, getSumLogPriorDensity){
    priorWithHyperPrior->initialize(params);

    // resize param
    def = std::make_shared<TParameterDefinition>("defParam");
    def->setDimensions({3});
    param = std::make_shared<TMCMCParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param->initializeStorageBasedOnPrior({3}, {dimNames});
    param->set(0, 2.1);
    param->set(1, 0.11);
    param->set(2, 0.29);

    double sum = priorWithHyperPrior->getSumLogPriorDensity(param);
    EXPECT_FLOAT_EQ(sum, -2.65521);
}

TEST_F(TPriorChisq_onArray_Test, getLogPriorDensityFull){
    priorWithHyperPrior->initialize(params);

    // resize param
    def = std::make_shared<TParameterDefinition>("defParam");
    def->setDimensions({3});
    param = std::make_shared<TMCMCParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param->initializeStorageBasedOnPrior({3}, {dimNames});
    param->set(0, 2.1);
    param->set(1, 0.11);
    param->set(2, 0.29);

    double sum = priorWithHyperPrior->getLogPriorDensityFull(param);
    EXPECT_FLOAT_EQ(sum, -2.65521); // same as getSumLogPriorDensity, because there is no prior on k
}

TEST_F(TPriorChisq_onArray_Test, getLogPriorRatio){
    priorWithHyperPrior->initialize(params);

    // resize param
    def = std::make_shared<TParameterDefinition>("defParam");
    def->setDimensions({3});
    param = std::make_shared<TMCMCParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param->initializeStorageBasedOnPrior({3}, {dimNames});
    param->set(0, 2.1);
    param->set(0, 3.1); // set again for ratio
    param->set(1, 0.11);
    param->set(2, 0.29);

    double val = priorWithHyperPrior->getLogPriorDensity(param, 0);
    EXPECT_FLOAT_EQ(val, -3.03464);
    val = priorWithHyperPrior->getLogPriorRatio(param, 0);
    EXPECT_FLOAT_EQ(val, -0.6947324);
}

//--------------------------------------------
// TPriorBetaWithHyperPrior

class MockTPriorBetaWithHyperPrior : public TPriorBetaWithHyperPrior<double> {
public:
    MockTPriorBetaWithHyperPrior()
            : TPriorBetaWithHyperPrior() {};
    MOCK_METHOD(void, _updateAlpha, (), (override));
    MOCK_METHOD(void, _updateBeta, (), (override));
    MOCK_METHOD(void, _updateTempVals, (), (override));
};

class BridgeTPriorBetaWithHyperPrior : public TPriorBetaWithHyperPrior<double> {
public:
    BridgeTPriorBetaWithHyperPrior() : TPriorBetaWithHyperPrior() {};
    // update alpha
    double _calcLLUpdateAlpha() override{
        return TPriorBetaWithHyperPrior::_calcLLUpdateAlpha();
    }
    double _calcLogHUpdateAlpha() override{
        return TPriorBetaWithHyperPrior::_calcLogHUpdateAlpha();
    }
    // update beta
    double _calcLLUpdateBeta() override{
        return TPriorBetaWithHyperPrior::_calcLLUpdateBeta();
    }
    double _calcLogHUpdateBeta() override {
        return TPriorBetaWithHyperPrior::_calcLogHUpdateBeta();
    }

    // functions to tweak value of alpha and beta (for controlling updates, we can't directly access them from outside)
    void setAlphaTo(double x){
        _alpha->set(x);
    }
    void setBetaTo(double x){
        _beta->set(x);
    }
};

class TPriorBetaWithHyperPriorTest : public Test {
protected:
    std::shared_ptr<TPriorBetaWithHyperPrior<double>> priorWithHyperPrior;

    std::shared_ptr<TParameterDefinition> defAlpha;
    std::shared_ptr<TParameterDefinition> defBeta;
    std::unique_ptr<TPrior<double>> prioralpha;
    std::unique_ptr<TPrior<double>> priorbeta;
    std::shared_ptr<TMCMCParameter<double>> alpha;
    std::shared_ptr<TMCMCParameter<double>> beta;

    std::shared_ptr<BridgeTMCMCArrayParameter<double>> param; // parameter on which prior is defined
    std::shared_ptr<TParameterDefinition> def;

    TRandomGenerator randomGenerator;
    TLog logfile;
    std::vector<std::shared_ptr<TObservationsBase>> observations = {};

    void SetUp() override {
        defAlpha = std::make_shared<TParameterDefinition>("defAlpha");
        defBeta = std::make_shared<TParameterDefinition>("defBeta");

        prioralpha = std::make_unique<TPriorExponential<double>>();
        priorbeta = std::make_unique<TPriorExponential<double>>();

        // initialize param
        priorWithHyperPrior = std::make_shared<TPriorBetaWithHyperPrior<double>>();
        def = std::make_shared<TParameterDefinition>("defParam");
        def->setDimensions({21});
        def->setInitVal("0.001,0.0509,0.1008,0.1507,0.2006,0.2505,0.3004,0.3503,0.4002,0.4501,0.5,0.5499,0.5998,0.6497,0.6996,0.7495,0.7994,0.8493,0.8992,0.9491,0.999");
        param = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
        std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
        param->initializeStorageBasedOnPrior({21}, {dimNames});
    }
    void TearDown() override {}
};

TEST_F(TPriorBetaWithHyperPriorTest, initPriorWithHyperPrior_wrongNumParams){
    // first construct alpha and beta
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    alpha = std::make_shared<TMCMCParameter<double>>(std::move(prioralpha), defAlpha, &randomGenerator);
    beta = std::make_shared<TMCMCParameter<double>>(std::move(priorbeta), defBeta, &randomGenerator);
    params.push_back(alpha);
    params.push_back(alpha);
    params.push_back(beta); // one too much

    // now initialize priorWithHyperPrior - throws because 3 instead of 2 parameters were given
    EXPECT_THROW(priorWithHyperPrior->initPriorWithHyperPrior(params, observations), std::runtime_error);
}

TEST_F(TPriorBetaWithHyperPriorTest, initPriorWithHyperPrior){
    // first construct alpha and beta
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    alpha = std::make_shared<TMCMCParameter<double>>(std::move(prioralpha), defAlpha, &randomGenerator);
    beta = std::make_shared<TMCMCParameter<double>>(std::move(priorbeta), defBeta, &randomGenerator);
    params.push_back(alpha);
    params.push_back(beta);

    // now initialize priorWithHyperPrior - check if alpha and beta are correctly initialized
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->initializeStorage();
    EXPECT_EQ(priorWithHyperPrior->alpha(), cutoffFloat);
    EXPECT_EQ(priorWithHyperPrior->beta(), cutoffFloat);
}

TEST_F(TPriorBetaWithHyperPriorTest, initPriorWithHyperPrior_reSetInitValOfAlpha){
    // first construct alpha and beta
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defAlpha->setMin("-10.", false); // set some complete bullshit
    defAlpha->setMax("5.", false);
    defAlpha->setInitVal("-1.");

    alpha = std::make_shared<TMCMCParameter<double>>(std::move(prioralpha), defAlpha, &randomGenerator); // this works, because everything is ok (we don't know yet that alpha must be > 0)
    beta = std::make_shared<TMCMCParameter<double>>(std::move(priorbeta), defBeta, &randomGenerator);
    params.push_back(alpha);
    params.push_back(beta);

    // now initialize priorWithHyperPrior - should re-assign min and throws because initial value is invalid
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorBetaWithHyperPriorTest, getLogPriorRatio){
    // first construct alpha and beta
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defAlpha->setInitVal("1.");
    defBeta->setInitVal("0.7");
    alpha = std::make_shared<TMCMCParameter<double>>(std::move(prioralpha), defAlpha, &randomGenerator);
    beta = std::make_shared<TMCMCParameter<double>>(std::move(priorbeta), defBeta, &randomGenerator);
    params.push_back(alpha);
    params.push_back(beta);

    // now initialize priorWithHyperPrior
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->initializeStorage();
    // correct logPriorRatio and logPriorDensity?
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(0.83, 0.8), 0.04875568);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(0.83, 0.8), priorWithHyperPrior->getLogPriorDensity(0.83) - priorWithHyperPrior->getLogPriorDensity(0.8));
}

TEST_F(TPriorBetaWithHyperPriorTest, update_bothFixed){
    // first construct alpha and beta
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defAlpha->update(false);
    defAlpha->setInitVal("1.");
    defBeta->update(false);
    defBeta->setInitVal("0.7");
    alpha = std::make_shared<TMCMCParameter<double>>(std::move(prioralpha), defAlpha, &randomGenerator);
    beta = std::make_shared<TMCMCParameter<double>>(std::move(priorbeta), defBeta, &randomGenerator);
    params.push_back(alpha);
    params.push_back(beta);

    // now initialize priorWithHyperPrior
    MockTPriorBetaWithHyperPrior mockPriorWithHyperPrior;
    EXPECT_CALL(mockPriorWithHyperPrior, _updateTempVals()).Times(1);
    mockPriorWithHyperPrior.initPriorWithHyperPrior(params, observations);
    mockPriorWithHyperPrior.initializeStorageOfPriorParameters();

    // should never update both
    EXPECT_CALL(mockPriorWithHyperPrior, _updateAlpha).Times(0);
    EXPECT_CALL(mockPriorWithHyperPrior, _updateBeta).Times(0);
    EXPECT_CALL(mockPriorWithHyperPrior, _updateTempVals()).Times(0);
    mockPriorWithHyperPrior.updateParams();
}

TEST_F(TPriorBetaWithHyperPriorTest, update_onlyAlphaFixed){
    // first construct alpha and beta
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defAlpha->update(false);
    defAlpha->setInitVal("1.");
    defBeta->setInitVal("0.7");
    alpha = std::make_shared<TMCMCParameter<double>>(std::move(prioralpha), defAlpha, &randomGenerator);
    beta = std::make_shared<TMCMCParameter<double>>(std::move(priorbeta), defBeta, &randomGenerator);
    params.push_back(alpha);
    params.push_back(beta);

    // now initialize priorWithHyperPrior
    MockTPriorBetaWithHyperPrior mockPriorWithHyperPrior;
    EXPECT_CALL(mockPriorWithHyperPrior, _updateTempVals()).Times(1);
    mockPriorWithHyperPrior.initPriorWithHyperPrior(params, observations);
    mockPriorWithHyperPrior.initializeStorageOfPriorParameters();

    // should never update alpha
    EXPECT_CALL(mockPriorWithHyperPrior, _updateAlpha).Times(0);
    EXPECT_CALL(mockPriorWithHyperPrior, _updateBeta).Times(1);
    EXPECT_CALL(mockPriorWithHyperPrior, _updateTempVals()).Times(1);
    mockPriorWithHyperPrior.updateParams();
}

TEST_F(TPriorBetaWithHyperPriorTest, _updateAlpha){
    // first construct alpha and beta
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defAlpha->setInitVal("1.");
    prioralpha = std::make_unique<TPriorExponential<double>>(); // set some prior on alpha, such that prior does not cancel out in hastings ratio
    prioralpha->initialize("1");
    defBeta->setInitVal("0.7");
    alpha = std::make_shared<TMCMCParameter<double>>(std::move(prioralpha), defAlpha, &randomGenerator);
    beta = std::make_shared<TMCMCParameter<double>>(std::move(priorbeta), defBeta, &randomGenerator);
    params.push_back(alpha);
    params.push_back(beta);

    // now initialize priorWithHyperPrior
    auto bridgePriorBetaWithHyperPrior = std::make_shared<BridgeTPriorBetaWithHyperPrior>();
    bridgePriorBetaWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgePriorBetaWithHyperPrior->initializeStorageOfPriorParameters();
    // change value of alpha, such that oldValue and newValue are not the same
    bridgePriorBetaWithHyperPrior->setAlphaTo(2.19);
    bridgePriorBetaWithHyperPrior->initParameter(param);

    // calculate LL
    // equivalent to: sum(dbeta(vals, 2.19, 0.7, log = T)) - sum(dbeta(vals, 1, 0.7, log = T)) in R
    EXPECT_FLOAT_EQ(bridgePriorBetaWithHyperPrior->_calcLLUpdateAlpha(), -16.53637);

    // calculate logH
    // equivalent to: sum(dbeta(vals, alpha = 2.19, beta = 0.7, log = T)) - sum(dbeta(vals, alpha = 1, beta = 1, log = T)) in R
    //                 + dexp(2.19, 1, log = T) - dexp(1, 1, log = T)
    EXPECT_FLOAT_EQ(bridgePriorBetaWithHyperPrior->_calcLogHUpdateAlpha(), -16.53637 - 1.19);
}

TEST_F(TPriorBetaWithHyperPriorTest, _updateAlpha_3params){
    // first construct alpha and beta
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defAlpha->setInitVal("1.");
    prioralpha = std::make_unique<TPriorExponential<double>>(); // set some prior on alpha, such that prior does not cancel out in hastings ratio
    prioralpha->initialize("1");
    defBeta->setInitVal("0.7");
    alpha = std::make_shared<TMCMCParameter<double>>(std::move(prioralpha), defAlpha, &randomGenerator);
    beta = std::make_shared<TMCMCParameter<double>>(std::move(priorbeta), defBeta, &randomGenerator);
    params.push_back(alpha);
    params.push_back(beta);

    // create a second parameter
    auto def2 = std::make_shared<TParameterDefinition>("defParam2");
    def2->setDimensions({10});
    def2->setInitVal("0.234143806961374,0.731057851890315,0.80083513852956,0.149552868462035,0.860712598089965,0.925751318016275,0.424374922630389,0.992940929440277,0.825610709278389,0.23677231646186");
    auto param2 = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def2, &randomGenerator);; // 2nd parameter on which prior is defined
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param2->initializeStorageBasedOnPrior({10}, {dimNames});

    // create a third parameter
    auto def3 = std::make_shared<TParameterDefinition>("defParam3");
    def3->setDimensions({50});
    def3->setInitVal("0.997855041432728,0.0622833100395043,0.525588429923676,0.50922617010304,0.0963243372014664,0.126802075030701,0.201710963868274,0.101982490809532,0.137925490323595,0.457595457794797,0.995202227676167,0.191901016480627,0.53194975426909,0.587963984897121,0.975408025165289,0.750516695313652,0.276808882983362,0.0336961317387989,0.558341750310423,0.291148849856178,0.530630074350011,0.721812271885351,0.712019709554695,0.533757727869112,0.0662704480006508,0.592792577078564,0.640899275034239,0.164658749400032,0.216376583535156,0.832580748720302,0.83883549484883,0.30234483129109,0.141963647965732,0.563716573228606,0.111101850090936,0.286161771168555,0.31394054573018,0.938404923676034,0.0273833015625331,0.698946036507677,0.924011270816947,0.211157677156089,0.576430141034066,0.00105058911869594,0.522350224222634,0.167086684461037,0.484045928540885,0.850306264805687,0.39386781271289,0.991124635568981");
    auto param3 = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def3, &randomGenerator); // 3rd parameter on which prior is defined
    param3->initializeStorageBasedOnPrior({50}, {dimNames});

    // now initialize priorWithHyperPrior
    auto bridgePriorBetaWithHyperPrior = std::make_shared<BridgeTPriorBetaWithHyperPrior>();
    bridgePriorBetaWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgePriorBetaWithHyperPrior->initParameter(param);
    bridgePriorBetaWithHyperPrior->initParameter(param2);
    bridgePriorBetaWithHyperPrior->initParameter(param3);
    bridgePriorBetaWithHyperPrior->initializeStorageOfPriorParameters();
    // change value of alpha, such that oldValue and newValue are not the same
    bridgePriorBetaWithHyperPrior->setAlphaTo(2.19);

    // calculate LL
    // equivalent to: sum(dbeta(c(vals, vals2, vals3), 2.19, 0.7, log = T)) - sum(dbeta(c(vals, vals2, vals3), 1, 0.7, log = T)) in R
    EXPECT_FLOAT_EQ(bridgePriorBetaWithHyperPrior->_calcLLUpdateAlpha(), -59.58255);

    // calculate logH
    // equivalent to: sum(dbeta(c(vals, vals2, vals3), alpha = 2.19, beta = 0.7, log = T)) - sum(dbeta(c(vals, vals2, vals3), alpha = 1, beta = 1, log = T)) in R
    //                 + dexp(2.19, 1, log = T) - dexp(1, 1, log = T)
    EXPECT_FLOAT_EQ(bridgePriorBetaWithHyperPrior->_calcLogHUpdateAlpha(), -59.58255 - 1.19);
}

TEST_F(TPriorBetaWithHyperPriorTest, _updateBeta){
    // first construct alpha and beta
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defAlpha->setInitVal("1.");
    defBeta->setInitVal("0.7");
    priorbeta = std::make_unique<TPriorExponential<double>>(); // set some prior on beta, such that prior does not cancel out in hastings ratio
    priorbeta->initialize("2.4");
    alpha = std::make_shared<TMCMCParameter<double>>(std::move(prioralpha), defAlpha, &randomGenerator);
    beta = std::make_shared<TMCMCParameter<double>>(std::move(priorbeta), defBeta, &randomGenerator);
    params.push_back(alpha);
    params.push_back(beta);

    // now initialize priorWithHyperPrior
    auto bridgePriorBetaWithHyperPrior = std::make_shared<BridgeTPriorBetaWithHyperPrior>();
    bridgePriorBetaWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgePriorBetaWithHyperPrior->initParameter(param);
    bridgePriorBetaWithHyperPrior->initializeStorageOfPriorParameters();
    // change value of beta, such that oldValue and newValue are not the same
    bridgePriorBetaWithHyperPrior->setBetaTo(1.);

    // calculate LL
    // equivalent to: sum(dbeta(vals, 1, 1, log = T)) - sum(dbeta(vals, 1, 0.7, log = T)) in R
    EXPECT_FLOAT_EQ(bridgePriorBetaWithHyperPrior->_calcLLUpdateBeta(), 0.153661);

    // calculate logH
    // equivalent to: sum(dbeta(vals, 1, 1, log = T)) - sum(dbeta(vals, 1, 0.7, log = T))  in R
    //                 + dexp(0.7, 2.4, log = T) - dexp(1, 2.4, log = T)
    EXPECT_FLOAT_EQ(bridgePriorBetaWithHyperPrior->_calcLogHUpdateBeta(), 0.153661 - 0.72);
}

TEST_F(TPriorBetaWithHyperPriorTest, _updateBeta_3params){
    // first construct alpha and beta
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defAlpha->setInitVal("1.");
    defBeta->setInitVal("0.7");
    priorbeta = std::make_unique<TPriorExponential<double>>(); // set some prior on beta, such that prior does not cancel out in hastings ratio
    priorbeta->initialize("2.4");
    alpha = std::make_shared<TMCMCParameter<double>>(std::move(prioralpha), defAlpha, &randomGenerator);
    beta = std::make_shared<TMCMCParameter<double>>(std::move(priorbeta), defBeta, &randomGenerator);
    params.push_back(alpha);
    params.push_back(beta);

    // create a second parameter
    auto def2 = std::make_shared<TParameterDefinition>("defParam2");
    def2->setDimensions({10});
    def2->setInitVal("0.234143806961374,0.731057851890315,0.80083513852956,0.149552868462035,0.860712598089965,0.925751318016275,0.424374922630389,0.992940929440277,0.825610709278389,0.23677231646186");
    auto param2 = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def2, &randomGenerator);; // 2nd parameter on which prior is defined
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param2->initializeStorageBasedOnPrior({10}, {dimNames});

    // create a third parameter
    auto def3 = std::make_shared<TParameterDefinition>("defParam3");
    def3->setDimensions({50});
    def3->setInitVal("0.997855041432728,0.0622833100395043,0.525588429923676,0.50922617010304,0.0963243372014664,0.126802075030701,0.201710963868274,0.101982490809532,0.137925490323595,0.457595457794797,0.995202227676167,0.191901016480627,0.53194975426909,0.587963984897121,0.975408025165289,0.750516695313652,0.276808882983362,0.0336961317387989,0.558341750310423,0.291148849856178,0.530630074350011,0.721812271885351,0.712019709554695,0.533757727869112,0.0662704480006508,0.592792577078564,0.640899275034239,0.164658749400032,0.216376583535156,0.832580748720302,0.83883549484883,0.30234483129109,0.141963647965732,0.563716573228606,0.111101850090936,0.286161771168555,0.31394054573018,0.938404923676034,0.0273833015625331,0.698946036507677,0.924011270816947,0.211157677156089,0.576430141034066,0.00105058911869594,0.522350224222634,0.167086684461037,0.484045928540885,0.850306264805687,0.39386781271289,0.991124635568981");
    auto param3 = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def3, &randomGenerator);; // 3rd parameter on which prior is defined
    param3->initializeStorageBasedOnPrior({50}, {dimNames});

    // now initialize priorWithHyperPrior
    auto bridgePriorBetaWithHyperPrior = std::make_shared<BridgeTPriorBetaWithHyperPrior>();
    bridgePriorBetaWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgePriorBetaWithHyperPrior->initParameter(param);
    bridgePriorBetaWithHyperPrior->initParameter(param2);
    bridgePriorBetaWithHyperPrior->initParameter(param3);
    bridgePriorBetaWithHyperPrior->initializeStorageOfPriorParameters();
    // change value of beta, such that oldValue and newValue are not the same
    bridgePriorBetaWithHyperPrior->setBetaTo(1.);

    // calculate LL
    // equivalent to: sum(dbeta(c(vals, vals2, vals3), 1, 1, log = T)) - sum(dbeta(c(vals, vals2, vals3), 1, 0.7, log = T)) in R
    EXPECT_FLOAT_EQ(bridgePriorBetaWithHyperPrior->_calcLLUpdateBeta(), 1.483910673);

    // calculate logH
    // equivalent to: sum(dbeta(c(vals, vals2, vals3), alpha = 1, beta = 1, log = T)) - sum(dbeta(c(vals, vals2, vals3), alpha = 1, beta = 0.7, log = T)) in R
    //                 + dexp(1, 2.4, log = T) - dexp(0.7, 2.4, log = T)
    EXPECT_FLOAT_EQ(bridgePriorBetaWithHyperPrior->_calcLogHUpdateBeta(), 1.483910673 - 0.72);
}

TEST_F(TPriorBetaWithHyperPriorTest, getSumLogPriorDensity){
    // first construct alpha and beta
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defAlpha->setInitVal("1.");
    defBeta->setInitVal("0.7");
    alpha = std::make_shared<TMCMCParameter<double>>(std::move(prioralpha), defAlpha, &randomGenerator);
    beta = std::make_shared<TMCMCParameter<double>>(std::move(priorbeta), defBeta, &randomGenerator);
    params.push_back(alpha);
    params.push_back(beta);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    // resize param
    def = std::make_shared<TParameterDefinition>("defParam");
    def->setDimensions({3});
    param = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param->initializeStorageBasedOnPrior({3}, {dimNames});
    param->initializeStorage();
    param->set(0, 0.81);
    param->set(1, 0.11);
    param->set(2, 0.29);

    double sum = priorWithHyperPrior->getSumLogPriorDensity(param);
    EXPECT_FLOAT_EQ(sum, -0.4340982);
}

TEST_F(TPriorBetaWithHyperPriorTest, getLogPriorDensityFull){
    // first construct alpha and beta
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defAlpha->setInitVal("1.");
    defBeta->setInitVal("0.7");
    prioralpha = std::make_unique<TPriorExponential<double>>(); // set some prior on alpha
    prioralpha->initialize("1");
    priorbeta = std::make_unique<TPriorExponential<double>>(); // set some prior on beta
    priorbeta->initialize("2.4");
    alpha = std::make_shared<TMCMCParameter<double>>(std::move(prioralpha), defAlpha, &randomGenerator);
    beta = std::make_shared<TMCMCParameter<double>>(std::move(priorbeta), defBeta, &randomGenerator);
    params.push_back(alpha);
    params.push_back(beta);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    // resize param
    def = std::make_shared<TParameterDefinition>("defParam");
    def->setDimensions({3});
    param = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param->initializeStorageBasedOnPrior({3}, {dimNames});
    param->initializeStorage();
    param->set(0, 0.81);
    param->set(1, 0.11);
    param->set(2, 0.29);

    double sum = priorWithHyperPrior->getLogPriorDensityFull(param);
    EXPECT_FLOAT_EQ(sum, -2.238629);
}

TEST_F(TPriorBetaWithHyperPriorTest, getLogPriorDensity){
    // first construct alpha and beta
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defAlpha->setInitVal("1.");
    defBeta->setInitVal("0.7");
    alpha = std::make_shared<TMCMCParameter<double>>(std::move(prioralpha), defAlpha, &randomGenerator);
    beta = std::make_shared<TMCMCParameter<double>>(std::move(priorbeta), defBeta, &randomGenerator);
    params.push_back(alpha);
    params.push_back(beta);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    // resize param
    def = std::make_shared<TParameterDefinition>("defParam");
    def->setDimensions({3});
    param = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param->initializeStorageBasedOnPrior({3}, {dimNames});
    param->initializeStorage();
    param->set(0, 0.81);
    param->set(0, 0.91); // set again for ratio
    param->set(1, 0.11);
    param->set(2, 0.29);

    double val = priorWithHyperPrior->getLogPriorDensity(param, 0);
    EXPECT_FLOAT_EQ(val, 0.3657087);
    val = priorWithHyperPrior->getLogPriorRatio(param, 0);
    EXPECT_FLOAT_EQ(val, 0.2241643);
}

TEST_F(TPriorBetaWithHyperPriorTest, _setAlphaBetaToMOM_1param){
    // first construct alpha and beta
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    alpha = std::make_shared<TMCMCParameter<double>>(std::move(prioralpha), defAlpha, &randomGenerator);
    beta = std::make_shared<TMCMCParameter<double>>(std::move(priorbeta), defBeta, &randomGenerator);
    params.push_back(alpha);
    params.push_back(beta);

    // now initialize priorWithHyperPrior
    auto bridgePriorBetaWithHyperPrior = std::make_shared<BridgeTPriorBetaWithHyperPrior>();
    bridgePriorBetaWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgePriorBetaWithHyperPrior->initParameter(param);
    bridgePriorBetaWithHyperPrior->initializeStorageOfPriorParameters();

    bridgePriorBetaWithHyperPrior->estimateInitialPriorParameters(&logfile);

    // equivalent to: ebeta(vals, "mme") in R
    EXPECT_FLOAT_EQ(bridgePriorBetaWithHyperPrior->alpha(), 0.8691073);
    EXPECT_FLOAT_EQ(bridgePriorBetaWithHyperPrior->beta(), 0.8691073);
}

TEST_F(TPriorBetaWithHyperPriorTest, _setAlphaBetaToMOM_3params){
    // first construct alpha and beta
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    alpha = std::make_shared<TMCMCParameter<double>>(std::move(prioralpha), defAlpha, &randomGenerator);
    beta = std::make_shared<TMCMCParameter<double>>(std::move(priorbeta), defBeta, &randomGenerator);
    params.push_back(alpha);
    params.push_back(beta);

    // create a second parameter
    auto def2 = std::make_shared<TParameterDefinition>("defParam2");
    def2->setDimensions({10});
    def2->setInitVal("0.234143806961374,0.731057851890315,0.80083513852956,0.149552868462035,0.860712598089965,0.925751318016275,0.424374922630389,0.992940929440277,0.825610709278389,0.23677231646186");
    auto param2 = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def2, &randomGenerator);; // 2nd parameter on which prior is defined
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param2->initializeStorageBasedOnPrior({10}, {dimNames});

    // create a third parameter
    auto def3 = std::make_shared<TParameterDefinition>("defParam3");
    def3->setDimensions({50});
    def3->setInitVal("0.997855041432728,0.0622833100395043,0.525588429923676,0.50922617010304,0.0963243372014664,0.126802075030701,0.201710963868274,0.101982490809532,0.137925490323595,0.457595457794797,0.995202227676167,0.191901016480627,0.53194975426909,0.587963984897121,0.975408025165289,0.750516695313652,0.276808882983362,0.0336961317387989,0.558341750310423,0.291148849856178,0.530630074350011,0.721812271885351,0.712019709554695,0.533757727869112,0.0662704480006508,0.592792577078564,0.640899275034239,0.164658749400032,0.216376583535156,0.832580748720302,0.83883549484883,0.30234483129109,0.141963647965732,0.563716573228606,0.111101850090936,0.286161771168555,0.31394054573018,0.938404923676034,0.0273833015625331,0.698946036507677,0.924011270816947,0.211157677156089,0.576430141034066,0.00105058911869594,0.522350224222634,0.167086684461037,0.484045928540885,0.850306264805687,0.39386781271289,0.991124635568981");
    auto param3 = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def3, &randomGenerator);; // 3rd parameter on which prior is defined
    param3->initializeStorageBasedOnPrior({50}, {dimNames});

    // now initialize priorWithHyperPrior
    auto bridgePriorBetaWithHyperPrior = std::make_shared<BridgeTPriorBetaWithHyperPrior>();
    bridgePriorBetaWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgePriorBetaWithHyperPrior->initParameter(param);
    bridgePriorBetaWithHyperPrior->initParameter(param2);
    bridgePriorBetaWithHyperPrior->initParameter(param3);
    bridgePriorBetaWithHyperPrior->initializeStorageOfPriorParameters();

    bridgePriorBetaWithHyperPrior->estimateInitialPriorParameters(&logfile);

    // equivalent to: ebeta(c(vals, vals2, vals3), "mme") in R
    EXPECT_FLOAT_EQ(bridgePriorBetaWithHyperPrior->alpha(), 0.8021020);
    EXPECT_FLOAT_EQ(bridgePriorBetaWithHyperPrior->beta(), 0.8448825);
}

//--------------------------------------------
// TPriorBinomialWithHyperPrior

class MockTPriorBinomialWithHyperPrior : public TPriorBinomialWithHyperPrior<uint32_t> {
public:
    MockTPriorBinomialWithHyperPrior()
            : TPriorBinomialWithHyperPrior() {};
    MOCK_METHOD(void, _updateP, (), (override));
    MOCK_METHOD(void, _updateTempVals, (), (override));
};

class BridgeTPriorBinomialWithHyperPrior : public TPriorBinomialWithHyperPrior<uint32_t> {
public:
    BridgeTPriorBinomialWithHyperPrior() : TPriorBinomialWithHyperPrior() {};
    // update lambda
    double _calcLLUpdateP() override{
        return TPriorBinomialWithHyperPrior::_calcLLUpdateP();
    }
    double _calcLogHUpdateP() override{
        return TPriorBinomialWithHyperPrior::_calcLogHUpdateP();
    }

    // functions to tweak value of p (for controlling updates, we can't directly access them from outside)
    void setPTo(double x){
        _paramP->set(x);
    }
};

class TPriorBinomialWithHyperPriorTest : public Test {
protected:
    std::shared_ptr<BridgeTPriorBinomialWithHyperPrior> priorWithHyperPrior;

    std::shared_ptr<TParameterDefinition> defP;
    std::unique_ptr<TPrior<double>> priorP;
    std::shared_ptr<TMCMCParameter<double>> p;

    std::shared_ptr<BridgeTMCMCArrayParameter<uint32_t>> param; // parameter on which prior is defined
    std::shared_ptr<TParameterDefinition> def;

    TRandomGenerator randomGenerator;
    TLog logfile;
    std::vector<std::shared_ptr<TObservationsBase>> observations = {};

    void SetUp() override {
        defP = std::make_shared<TParameterDefinition>("defP");
        priorP = std::make_unique<TPriorBeta<double>>();

        // initialize param
        priorWithHyperPrior = std::make_shared<BridgeTPriorBinomialWithHyperPrior>();
        def = std::make_shared<TParameterDefinition>("defParam");
        def->setDimensions({21, 2});
        def->setMax("10");
        def->setInitVal("2,0,3,1,5,0,9,2,2,0,8,0,9,2,6,3,6,1,0,0,2,0,1,0,6,2,3,0,7,3,4,2,7,3,9,1,3,1,7,2,9,4");
        param = std::make_shared<BridgeTMCMCArrayParameter<uint32_t>>(priorWithHyperPrior, def, &randomGenerator);
        std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
        param->initializeStorageBasedOnPrior({21, 2}, {dimNames, dimNames});
    }
    void TearDown() override {}
};

TEST_F(TPriorBinomialWithHyperPriorTest, initPriorWithHyperPrior_wrongNumParams){
    // first construct p
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    p = std::make_shared<TMCMCParameter<double>>(std::move(priorP), defP, &randomGenerator);
    params = {}; // forget to add p

    // now initialize priorWithHyperPrior - throws because 0 instead of 1 parameter were given
    EXPECT_THROW(priorWithHyperPrior->initPriorWithHyperPrior(params, observations), std::runtime_error);
}

TEST_F(TPriorBinomialWithHyperPriorTest, initPriorWithHyperPrior){
    // first construct p
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    p = std::make_shared<TMCMCParameter<double>>(std::move(priorP), defP, &randomGenerator);
    params.push_back(p);

    // now initialize priorWithHyperPrior - check if p is correctly initialized
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    EXPECT_EQ(priorWithHyperPrior->p(), cutoffFloat);
}

TEST_F(TPriorBinomialWithHyperPriorTest, initPriorWithHyperPrior_reSetInitValOfP){
    // first construct lambda
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defP->setMin("-10.", false); // set some complete bullshit
    defP->setMax("5.", false);
    defP->setInitVal("-0.5");
    p = std::make_shared<TMCMCParameter<double>>(std::move(priorP), defP, &randomGenerator); // this works, because everything is ok (we don't know yet that lambda must be > 0)
    params.push_back(p);

    // now initialize priorWithHyperPrior - should re-assign min and throw, because initVal is outside min
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorBinomialWithHyperPriorTest, getLogPriorDensity){
    // first construct lambda
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defP->setInitVal("0.3");
    p = std::make_shared<TMCMCParameter<double>>(std::move(priorP), defP, &randomGenerator);
    params.push_back(p);

    // now initialize priorWithHyperPrior
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    // correct logPriorRatio and logPriorDensity?
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(param, 0), -0.7133499);
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorDensity(param, 9*2), 0., 10e-15); // gammaLog does not return exactly 0, but 10e-16 -> float_eq doesn't work
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(param, 19*2), -1.146798);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(param, 20*2), -1.762984);
}

TEST_F(TPriorBinomialWithHyperPriorTest, getLogPriorRatio){
    // first construct p
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defP->setInitVal("0.3");
    p = std::make_shared<TMCMCParameter<double>>(std::move(priorP), defP, &randomGenerator);
    params.push_back(p);

    // now initialize priorWithHyperPrior
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // update k
    std::vector<uint32_t> newK = {1,1,2,3,1,0,3,2,2,0,0,1,2,0,0,0,1,3,1,2,5};

    // correct logPriorRatio and logPriorDensity?
    uint32_t indexN = 0;
    param->set(indexN+1, newK[0]);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, indexN), -0.1541507);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, indexN), priorWithHyperPrior->getLogPriorDensity(param, indexN) - priorWithHyperPrior->getLogPriorDensityOld(param, indexN));

    indexN = 9*2;
    param->set(indexN+1, newK[9]);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, indexN), 0);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, indexN), priorWithHyperPrior->getLogPriorDensity(param, indexN) - priorWithHyperPrior->getLogPriorDensityOld(param, indexN));

    indexN = 19*2;
    param->set(indexN+1, newK[19]);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, indexN), 0);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, indexN), priorWithHyperPrior->getLogPriorDensity(param, indexN) - priorWithHyperPrior->getLogPriorDensityOld(param, indexN));

    indexN = 20*2;
    param->set(indexN+1, newK[20]);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, indexN), -0.8472979);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, indexN), priorWithHyperPrior->getLogPriorDensity(param, indexN) - priorWithHyperPrior->getLogPriorDensityOld(param, indexN));
}

TEST_F(TPriorBinomialWithHyperPriorTest, update_Fixed){
    // first construct lambda
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defP->setInitVal("0.4");
    defP->update(false);
    p = std::make_shared<TMCMCParameter<double>>(std::move(priorP), defP, &randomGenerator);
    params.push_back(p);

    // now initialize priorWithHyperPrior
    MockTPriorBinomialWithHyperPrior mockPriorWithHyperPrior;
    EXPECT_CALL(mockPriorWithHyperPrior, _updateTempVals()).Times(1);
    mockPriorWithHyperPrior.initPriorWithHyperPrior(params, observations);
    mockPriorWithHyperPrior.initParameter(param);
    mockPriorWithHyperPrior.initializeStorageOfPriorParameters();

    // should never update
    EXPECT_CALL(mockPriorWithHyperPrior, _updateP).Times(0);
    EXPECT_CALL(mockPriorWithHyperPrior, _updateTempVals()).Times(0);
    mockPriorWithHyperPrior.updateParams();
}

TEST_F(TPriorBinomialWithHyperPriorTest, updateP){
    // first construct lambda
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defP->setInitVal("0.4");
    priorP = std::make_unique<TPriorBeta<double>>(); // set some prior on p, such that prior does not cancel out in hastings ratio
    priorP->initialize("0.1,0.2");
    p = std::make_shared<TMCMCParameter<double>>(std::move(priorP), defP, &randomGenerator); // this works, because everything is ok (we don't know yet that lambda must be > 0)
    params.push_back(p);

    // now initialize priorWithHyperPrior
    auto bridgePriorWithHyperPrior = std::make_shared<BridgeTPriorBinomialWithHyperPrior>();
    bridgePriorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgePriorWithHyperPrior->initParameter(param);
    bridgePriorWithHyperPrior->initializeStorageOfPriorParameters();
    // change value of pi, such that oldValue and newValue are not the same
    bridgePriorWithHyperPrior->setPTo(0.86);

    // calculate LL
    // equivalent to: sum(dbinom(k, n, 0.86, log = T)) - sum(dbinom(k, n, 0.4, log = T)) in R
    EXPECT_FLOAT_EQ(bridgePriorWithHyperPrior->_calcLLUpdateP(), -97.21063);

    // calculate logH
    // equivalent to: sum(dbinom(vals, 10, 0.86, log = T)) - sum(dbinom(vals, 10, 0.4, log = T)) in R
    //                 + dbeta(0.86, 0.1, 0.2, log = T) - dbeta(0.4, 0.1, 0.2, log = T)
    EXPECT_FLOAT_EQ(bridgePriorWithHyperPrior->_calcLogHUpdateP(), -97.21063 + 0.4753087);
}

TEST_F(TPriorBinomialWithHyperPriorTest, getSumLogPriorDensity){
    // first construct p
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defP->setInitVal("0.4");
    p = std::make_shared<TMCMCParameter<double>>(std::move(priorP), defP, &randomGenerator);
    params.push_back(p);

    // now initialize priorWithHyperPrior
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    double sum = priorWithHyperPrior->getSumLogPriorDensity(param);
    EXPECT_FLOAT_EQ(sum, -30.27644);
}

TEST_F(TPriorBinomialWithHyperPriorTest, getLogPriorDensityFull){
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defP->setInitVal("0.4");
    priorP = std::make_unique<TPriorBeta<double>>(); // set some prior on pi, such that prior does not cancel out in hastings ratio
    priorP->initialize("0.1,0.2");
    p = std::make_shared<TMCMCParameter<double>>(std::move(priorP), defP, &randomGenerator);
    params.push_back(p);

    // now initialize priorWithHyperPrior
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    double sum = priorWithHyperPrior->getLogPriorDensityFull(param);
    EXPECT_FLOAT_EQ(sum, -30.27644 - 1.447656);
}

TEST_F(TPriorBinomialWithHyperPriorTest, _setPToMLE_1param){
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    p = std::make_shared<TMCMCParameter<double>>(std::move(priorP), defP, &randomGenerator); // this works, because everything is ok (we don't know yet that lambda must be > 0)
    params.push_back(p);

    // now initialize priorWithHyperPrior
    auto bridgePriorWithHyperPrior = std::make_shared<BridgeTPriorBinomialWithHyperPrior>();
    bridgePriorWithHyperPrior->initParameter(param);
    bridgePriorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgePriorWithHyperPrior->initParameter(param);
    bridgePriorWithHyperPrior->initializeStorageOfPriorParameters();

    bridgePriorWithHyperPrior->estimateInitialPriorParameters(&logfile);

    // equivalent to: sum(k) / (sum(n))
    EXPECT_FLOAT_EQ(p->value<double>(), 0.25);
}

TEST_F(TPriorBinomialWithHyperPriorTest, _setPiToMLE_3params){
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    p = std::make_shared<TMCMCParameter<double>>(std::move(priorP), defP, &randomGenerator); // this works, because everything is ok (we don't know yet that lambda must be > 0)
    params.push_back(p);

    // create a second parameter
    auto def2 = std::make_shared<TParameterDefinition>("defParam2");
    def2->setDimensions({10, 2});
    def2->setMax("10");
    def2->setInitVal("2,1,4,1,3,0,6,2,2,1,4,2,7,2,0,0,8,3,3,2");
    auto param2 = std::make_shared<BridgeTMCMCArrayParameter<uint32_t>>(priorWithHyperPrior, def2, &randomGenerator);; // 2nd parameter on which prior is defined
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param2->initializeStorageBasedOnPrior({10, 2}, {dimNames, dimNames});

    // create a third parameter
    auto def3 = std::make_shared<TParameterDefinition>("defParam3");
    def3->setDimensions({50, 2});
    def3->setMax("10");
    def3->setInitVal("4,1,7,0,3,0,3,1,7,4,2,1,7,2,1,0,2,2,1,0,2,1,0,0,6,2,8,1,7,1,7,3,4,1,4,0,8,3,6,0,6,3,3,1,2,1,9,2,6,2,2,1,1,0,4,1,9,1,5,1,9,2,7,1,3,2,4,1,1,1,0,0,7,4,1,0,4,0,6,1,9,3,4,1,4,1,1,1,7,3,4,1,5,1,2,1,2,1,5,2");
    auto param3 = std::make_shared<BridgeTMCMCArrayParameter<uint32_t>>(priorWithHyperPrior, def3, &randomGenerator);; // 3rd parameter on which prior is defined
    param3->initializeStorageBasedOnPrior({50, 2}, {dimNames, dimNames});

    // now initialize priorWithHyperPrior
    auto bridgePriorWithHyperPrior = std::make_shared<BridgeTPriorBinomialWithHyperPrior>();
    bridgePriorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgePriorWithHyperPrior->initParameter(param);
    bridgePriorWithHyperPrior->initParameter(param2);
    bridgePriorWithHyperPrior->initParameter(param3);
    bridgePriorWithHyperPrior->initializeStorageOfPriorParameters();

    bridgePriorWithHyperPrior->estimateInitialPriorParameters(&logfile);

    // equivalent to: (sum(k) + sum(k2) + sum(k3)) / (sum(n) + sum(n2) + sum(n3)) in R
    EXPECT_FLOAT_EQ(p->value<double>(),  0.2826087);
}

//--------------------------------------------
// TPriorBernouillilWithHyperPrior

class MockTPriorBernouilliWithHyperPrior : public TPriorBernouilliWithHyperPrior<bool> {
public:
    MockTPriorBernouilliWithHyperPrior()
            : TPriorBernouilliWithHyperPrior() {};
    MOCK_METHOD(void, _updatePi, (), (override));
    MOCK_METHOD(void, _updateTempVals, (), (override));
};

class BridgeTPriorBernouilliWithHyperPrior : public TPriorBernouilliWithHyperPrior<bool> {
public:
    BridgeTPriorBernouilliWithHyperPrior() : TPriorBernouilliWithHyperPrior() {};
    // update lambda
    double _calcLLUpdatePi() override{
        return TPriorBernouilliWithHyperPrior::_calcLLUpdatePi();
    }
    double _calcLogHUpdatePi() override{
        return TPriorBernouilliWithHyperPrior::_calcLogHUpdatePi();
    }

    // functions to tweak value of pi (for controlling updates, we can't directly access them from outside)
    void setPiTo(double x){
        _pi->set(x);
    }
};

class TPriorBernouilliWithHyperPriorTest : public Test {
protected:
    std::shared_ptr<TPriorBernouilliWithHyperPrior<bool>> priorWithHyperPrior;

    std::shared_ptr<TParameterDefinition> defPi;
    std::unique_ptr<TPrior<double>> priorPi;
    std::shared_ptr<TMCMCParameter<double>> pi;

    std::shared_ptr<BridgeTMCMCArrayParameter<bool>> param; // parameter on which prior is defined
    std::shared_ptr<TParameterDefinition> def;

    TRandomGenerator randomGenerator;
    TLog logfile;
    std::vector<std::shared_ptr<TObservationsBase>> observations = {};

    void SetUp() override {
        defPi = std::make_shared<TParameterDefinition>("defPi");

        priorPi = std::make_unique<TPriorBeta<double>>();

        // initialize param
        priorWithHyperPrior = std::make_shared<TPriorBernouilliWithHyperPrior<bool>>();
        def = std::make_shared<TParameterDefinition>("defParam");
        def->setDimensions({21});
        def->setInitVal("0,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0");
        param = std::make_shared<BridgeTMCMCArrayParameter<bool>>(priorWithHyperPrior, def, &randomGenerator);
        std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
        param->initializeStorageBasedOnPrior({21}, {dimNames});
    }
    void TearDown() override {}
};

TEST_F(TPriorBernouilliWithHyperPriorTest, initPriorWithHyperPrior_wrongNumParams){
    // first construct pi
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    pi = std::make_shared<TMCMCParameter<double>>(std::move(priorPi), defPi, &randomGenerator);
    // leave params empty

    // now initialize priorWithHyperPrior - throws because 0 instead of 1 parameter were given
    EXPECT_THROW(priorWithHyperPrior->initPriorWithHyperPrior(params, observations), std::runtime_error);
}

TEST_F(TPriorBernouilliWithHyperPriorTest, initPriorWithHyperPrior){
    // first construct lambda
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    pi = std::make_shared<TMCMCParameter<double>>(std::move(priorPi), defPi, &randomGenerator);
    params.push_back(pi);

    // now initialize priorWithHyperPrior - check if pi is correctly initialized
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->initializeStorage();
    EXPECT_EQ(priorWithHyperPrior->pi(), cutoffFloat);
}

TEST_F(TPriorBernouilliWithHyperPriorTest, initPriorWithHyperPrior_reSetInitValOfPi){
    // first construct lambda
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defPi->setMin("-10.", false); // set some complete bullshit
    defPi->setMax("5.", false);
    defPi->setInitVal("-0.5");
    pi = std::make_shared<TMCMCParameter<double>>(std::move(priorPi), defPi, &randomGenerator); // this works, because everything is ok (we don't know yet that lambda must be > 0)
    params.push_back(pi);

    // now initialize priorWithHyperPrior - should re-assign min and throw, because initVal is outside min
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorBernouilliWithHyperPriorTest, getLogPriorRatio){
    // first construct lambda
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defPi->setInitVal("0.4");
    pi = std::make_shared<TMCMCParameter<double>>(std::move(priorPi), defPi, &randomGenerator); // this works, because everything is ok (we don't know yet that lambda must be > 0)
    params.push_back(pi);

    // now initialize priorWithHyperPrior
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->initializeStorage();
    // correct logPriorRatio and logPriorDensity?
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(true, false), -0.4054651);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(true, false), priorWithHyperPrior->getLogPriorDensity(true) - priorWithHyperPrior->getLogPriorDensity(false));
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(false, true), 0.4054651);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(false, true), priorWithHyperPrior->getLogPriorDensity(false) - priorWithHyperPrior->getLogPriorDensity(true));
}

TEST_F(TPriorBernouilliWithHyperPriorTest, update_Fixed){
    // first construct lambda
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defPi->setInitVal("0.4");
    defPi->update(false);
    pi = std::make_shared<TMCMCParameter<double>>(std::move(priorPi), defPi, &randomGenerator); // this works, because everything is ok (we don't know yet that lambda must be > 0)
    params.push_back(pi);

    // now initialize priorWithHyperPrior
    MockTPriorBernouilliWithHyperPrior mockPriorWithHyperPrior;
    EXPECT_CALL(mockPriorWithHyperPrior, _updateTempVals()).Times(1);
    mockPriorWithHyperPrior.initPriorWithHyperPrior(params, observations);
    mockPriorWithHyperPrior.initializeStorageOfPriorParameters();

    // should never update
    EXPECT_CALL(mockPriorWithHyperPrior, _updatePi).Times(0);
    EXPECT_CALL(mockPriorWithHyperPrior, _updateTempVals()).Times(0);
    mockPriorWithHyperPrior.updateParams();
}

TEST_F(TPriorBernouilliWithHyperPriorTest, updatePi){
    // first construct lambda
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defPi->setInitVal("0.4");
    priorPi = std::make_unique<TPriorBeta<double>>(); // set some prior on pi, such that prior does not cancel out in hastings ratio
    priorPi->initialize("0.1,0.2");
    pi = std::make_shared<TMCMCParameter<double>>(std::move(priorPi), defPi, &randomGenerator); // this works, because everything is ok (we don't know yet that lambda must be > 0)
    params.push_back(pi);

    // now initialize priorWithHyperPrior
    auto bridgePriorBernouilliWithHyperPrior = std::make_shared<BridgeTPriorBernouilliWithHyperPrior>();
    bridgePriorBernouilliWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgePriorBernouilliWithHyperPrior->initParameter(param);
    bridgePriorBernouilliWithHyperPrior->initializeStorageOfPriorParameters();
    // change value of pi, such that oldValue and newValue are not the same
    bridgePriorBernouilliWithHyperPrior->setPiTo(0.86);

    // calculate LL
    // equivalent to: sum(dbinom(vals, 1, 0.86, log = T)) - sum(dbinom(vals, 1, 0.4, log = T)) in R
    EXPECT_FLOAT_EQ(bridgePriorBernouilliWithHyperPrior->_calcLLUpdatePi(), -23.89877);

    // calculate logH
    // equivalent to: sum(dbinom(vals, 1, 0.86, log = T)) - sum(dbinom(vals, 1, 0.4, log = T)) in R
    //                 + dbeta(0.86, 0.1, 0.2, log = T) - dbeta(0.4, 0.1, 0.2, log = T)
    EXPECT_FLOAT_EQ(bridgePriorBernouilliWithHyperPrior->_calcLogHUpdatePi(), -23.89877 + 0.4753087);
}

TEST_F(TPriorBernouilliWithHyperPriorTest, _updatePi_3params){
    // first construct alpha and pi
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defPi->setInitVal("0.4");
    priorPi = std::make_unique<TPriorBeta<double>>(); // set some prior on pi, such that prior does not cancel out in hastings ratio
    priorPi->initialize("0.1,0.2");
    pi = std::make_shared<TMCMCParameter<double>>(std::move(priorPi), defPi, &randomGenerator); // this works, because everything is ok (we don't know yet that lambda must be > 0)
    params.push_back(pi);

    // create a second parameter
    auto def2 = std::make_shared<TParameterDefinition>("defParam2");
    def2->setDimensions({10});
    def2->setInitVal("0,0,1,1,0,1,1,1,1,0");
    auto param2 = std::make_shared<BridgeTMCMCArrayParameter<bool>>(priorWithHyperPrior, def2, &randomGenerator);; // 2nd parameter on which prior is defined
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param2->initializeStorageBasedOnPrior({10}, {dimNames});

    // create a third parameter
    auto def3 = std::make_shared<TParameterDefinition>("defParam3");
    def3->setDimensions({50});
    def3->setInitVal("0,0,0,0,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0");
    auto param3 = std::make_shared<BridgeTMCMCArrayParameter<bool>>(priorWithHyperPrior, def3, &randomGenerator);; // 3rd parameter on which prior is defined
    param3->initializeStorageBasedOnPrior({50}, {dimNames});

    // now initialize priorWithHyperPrior
    auto bridgePriorBernouilliWithHyperPrior = std::make_shared<BridgeTPriorBernouilliWithHyperPrior>();
    bridgePriorBernouilliWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgePriorBernouilliWithHyperPrior->initParameter(param);
    bridgePriorBernouilliWithHyperPrior->initParameter(param2);
    bridgePriorBernouilliWithHyperPrior->initParameter(param3);
    bridgePriorBernouilliWithHyperPrior->initializeStorageOfPriorParameters();
    // change value of pi, such that oldValue and newValue are not the same
    bridgePriorBernouilliWithHyperPrior->setPiTo(0.86);

    // calculate LL
    // equivalent to: sum(dbinom(c(vals, vals2, vals3), 1, 0.86, log = T)) - sum(dbinom(c(vals, vals2, vals3), 1, 0.4, log = T)) in R
    EXPECT_FLOAT_EQ(bridgePriorBernouilliWithHyperPrior->_calcLLUpdatePi(), -93.44996);

    // calculate logH
    // equivalent to: sum(dbinom(c(vals, vals2, vals3), 1, 0.86, log = T)) - sum(dbinom(c(vals, vals2, vals3), 1, 0.4, log = T)) in R
    //                 + dbeta(0.86, 0.1, 0.2, log = T) - dbeta(0.4, 0.1, 0.2, log = T)
    EXPECT_FLOAT_EQ(bridgePriorBernouilliWithHyperPrior->_calcLogHUpdatePi(), -93.44996 + 0.4753087);
}

TEST_F(TPriorBernouilliWithHyperPriorTest, getSumLogPriorDensity){
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defPi->setInitVal("0.4");
    pi = std::make_shared<TMCMCParameter<double>>(std::move(priorPi), defPi, &randomGenerator); // this works, because everything is ok (we don't know yet that lambda must be > 0)
    params.push_back(pi);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    // resize param
    def = std::make_shared<TParameterDefinition>("defParam");
    def->setDimensions({3});
    param = std::make_shared<BridgeTMCMCArrayParameter<bool>>(priorWithHyperPrior, def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param->initializeStorageBasedOnPrior({3}, {dimNames});
    param->initializeStorage();
    param->set(0, true);
    param->set(1, false);
    param->set(2, false);

    double sum = priorWithHyperPrior->getSumLogPriorDensity(param);
    EXPECT_FLOAT_EQ(sum, -1.937942);
}

TEST_F(TPriorBernouilliWithHyperPriorTest, getLogPriorDensityFull){
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defPi->setInitVal("0.4");
    priorPi = std::make_unique<TPriorBeta<double>>(); // set some prior on pi, such that prior does not cancel out in hastings ratio
    priorPi->initialize("0.1,0.2");
    pi = std::make_shared<TMCMCParameter<double>>(std::move(priorPi), defPi, &randomGenerator); // this works, because everything is ok (we don't know yet that lambda must be > 0)
    params.push_back(pi);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    // resize param
    def = std::make_shared<TParameterDefinition>("defParam");
    def->setDimensions({3});
    param = std::make_shared<BridgeTMCMCArrayParameter<bool>>(priorWithHyperPrior, def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param->initializeStorageBasedOnPrior({3}, {dimNames});
    param->initializeStorage();
    param->set(0, true);
    param->set(1, false);
    param->set(2, false);

    double sum = priorWithHyperPrior->getLogPriorDensityFull(param);
    EXPECT_FLOAT_EQ(sum, -1.937942 - 1.447656);
}

TEST_F(TPriorBernouilliWithHyperPriorTest, getLogPriorDensityAndRatio){
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defPi->setInitVal("0.4");
    pi = std::make_shared<TMCMCParameter<double>>(std::move(priorPi), defPi, &randomGenerator); // this works, because everything is ok (we don't know yet that lambda must be > 0)
    params.push_back(pi);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    // resize param
    def = std::make_shared<TParameterDefinition>("defParam");
    def->setDimensions({3});
    param = std::make_shared<BridgeTMCMCArrayParameter<bool>>(priorWithHyperPrior, def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param->initializeStorageBasedOnPrior({3}, {dimNames});
    param->initializeStorage();
    param->set(0, true);
    param->set(1, false);
    param->set(2, false);

    double val = priorWithHyperPrior->getLogPriorDensity(param, 0);
    EXPECT_FLOAT_EQ(val, log(0.4));
    val = priorWithHyperPrior->getLogPriorRatio(param, 0);
    EXPECT_FLOAT_EQ(val, log(0.4) - log(0.6));
}

TEST_F(TPriorBernouilliWithHyperPriorTest, _setPiToMLE_1param){
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    pi = std::make_shared<TMCMCParameter<double>>(std::move(priorPi), defPi, &randomGenerator); // this works, because everything is ok (we don't know yet that lambda must be > 0)
    params.push_back(pi);

    // now initialize priorWithHyperPrior
    auto bridgePriorBernouilliWithHyperPrior = std::make_shared<BridgeTPriorBernouilliWithHyperPrior>();
    bridgePriorBernouilliWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgePriorBernouilliWithHyperPrior->initParameter(param);
    bridgePriorBernouilliWithHyperPrior->initializeStorageOfPriorParameters();

    bridgePriorBernouilliWithHyperPrior->estimateInitialPriorParameters(&logfile);

    // equivalent to: mean(vals) in R
    EXPECT_FLOAT_EQ(pi->value<double>(), 0.1428571);
}

TEST_F(TPriorBernouilliWithHyperPriorTest, _setPiToMLE_3params){
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    pi = std::make_shared<TMCMCParameter<double>>(std::move(priorPi), defPi, &randomGenerator); // this works, because everything is ok (we don't know yet that lambda must be > 0)
    params.push_back(pi);


    // create a second parameter
    auto def2 = std::make_shared<TParameterDefinition>("defParam2");
    def2->setDimensions({10});
    def2->setInitVal("0,0,1,1,0,1,1,1,1,0");
    auto param2 = std::make_shared<BridgeTMCMCArrayParameter<bool>>(priorWithHyperPrior, def2, &randomGenerator);; // 2nd parameter on which prior is defined
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param2->initializeStorageBasedOnPrior({10}, {dimNames});

    // create a third parameter
    auto def3 = std::make_shared<TParameterDefinition>("defParam3");
    def3->setDimensions({50});
    def3->setInitVal("0,0,0,0,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0");
    auto param3 = std::make_shared<BridgeTMCMCArrayParameter<bool>>(priorWithHyperPrior, def3, &randomGenerator);; // 3rd parameter on which prior is defined
    param3->initializeStorageBasedOnPrior({50}, {dimNames});

    // now initialize priorWithHyperPrior
    auto bridgePriorBernouilliWithHyperPrior = std::make_shared<BridgeTPriorBernouilliWithHyperPrior>();
    bridgePriorBernouilliWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgePriorBernouilliWithHyperPrior->initParameter(param);
    bridgePriorBernouilliWithHyperPrior->initParameter(param2);
    bridgePriorBernouilliWithHyperPrior->initParameter(param3);
    bridgePriorBernouilliWithHyperPrior->initializeStorageOfPriorParameters();

    bridgePriorBernouilliWithHyperPrior->estimateInitialPriorParameters(&logfile);

    // equivalent to: mean(c(vals, vals2, vals3)) in R
    EXPECT_FLOAT_EQ(pi->value<double>(), 0.1358025);
}


//--------------------------------------------
// TPriorCategoricalWithHyperPrior

class MockTPriorCategoricalWithHyperPrior : public TPriorCategoricalWithHyperPrior<uint8_t> {
public:
    MockTPriorCategoricalWithHyperPrior()
            : TPriorCategoricalWithHyperPrior() {};
    MOCK_METHOD(void, _updatePi, (const size_t& k), (override));
};

class BridgeTPriorCategoricalWithHyperPrior : public TPriorCategoricalWithHyperPrior<uint8_t> {
public:
    BridgeTPriorCategoricalWithHyperPrior() : TPriorCategoricalWithHyperPrior() {};
    // update lambda
    double _calcLLUpdatePi(){
        return TPriorCategoricalWithHyperPrior::_calcLLUpdatePi();
    }
    double _calcLogHUpdatePi(){
        return TPriorCategoricalWithHyperPrior::_calcLogHUpdatePi();
    }
    void _normalizePis(bool onlyReSetNewValue, const size_t & k = 0){
        return TPriorCategoricalWithHyperPrior::_normalizePis(onlyReSetNewValue, k);
    }
    void setPiTo(uint8_t k, double val){
        _pis->set(k, val);
    }
};

class TPriorCategoricalWithHyperPriorTest : public Test {
protected:
    std::shared_ptr<TPriorCategoricalWithHyperPrior<uint8_t>> priorWithHyperPrior;

    std::shared_ptr<TParameterDefinition> defPis;
    std::unique_ptr<TPrior<double>> priorPis;
    std::shared_ptr<TMCMCParameter<double>> pis;
    uint8_t K = 10;

    std::shared_ptr<BridgeTMCMCArrayParameter<uint8_t>> param; // parameter on which prior is defined
    std::shared_ptr<TParameterDefinition> def;

    TRandomGenerator randomGenerator;
    TLog logfile;
    std::vector<std::shared_ptr<TObservationsBase>> observations = {};

    void SetUp() override {
        defPis = std::make_shared<TParameterDefinition>("defPis");
        defPis->setDimensions({K});

        priorPis = std::make_unique<TPriorBeta<double>>();

        // initialize param
        priorWithHyperPrior = std::make_shared<TPriorCategoricalWithHyperPrior<uint8_t>>();
        def = std::make_shared<TParameterDefinition>("defParam");
        def->setDimensions({21});
        def->setInitVal("7,8,6,5,7,6,1,7,2,5,6,3,2,3,4,4,7,6,5,7,9");
        def->setMax("9");
        param = std::make_shared<BridgeTMCMCArrayParameter<uint8_t>>(priorWithHyperPrior, def, &randomGenerator);
        std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
        param->initializeStorageBasedOnPrior({21}, {dimNames});
    }
    void TearDown() override {}
};

TEST_F(TPriorCategoricalWithHyperPriorTest, initPriorWithHyperPrior_wrongNumParams){
    // first construct pi
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    pis = std::make_shared<TMCMCParameter<double>>(std::move(priorPis), defPis, &randomGenerator);
    // leave params empty

    // now initialize priorWithHyperPrior - throws because 0 instead of 1 parameter were given
    EXPECT_THROW(priorWithHyperPrior->initPriorWithHyperPrior(params, observations), std::runtime_error);
}

TEST_F(TPriorCategoricalWithHyperPriorTest, initPriorWithHyperPrior){
    // first construct pi
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    pis = std::make_shared<TMCMCParameter<double>>(std::move(priorPis), defPis, &randomGenerator);
    params.push_back(pis);

    // now initialize priorWithHyperPrior - check if pi is correctly initialized
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    for (uint8_t k = 0; k < K; k++){
        EXPECT_EQ(priorWithHyperPrior->pi(k), 0.1); // initialized to cutoffFloat (small value), but then normalized such that they sum to 1
    }
}

TEST_F(TPriorCategoricalWithHyperPriorTest, initPriorWithHyperPrior_reSetInitValOfPi){
    // first construct pis
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defPis->setMin("-10.", false); // set some complete bullshit
    defPis->setMax("5.", false);
    defPis->setInitVal("-0.5");
    pis = std::make_shared<TMCMCParameter<double>>(std::move(priorPis), defPis, &randomGenerator);
    params.push_back(pis);

    // now initialize priorWithHyperPrior - should re-assign min and throw, because initVal is outside min
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorCategoricalWithHyperPriorTest, getLogPriorRatio){
    // first construct pis
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defPis->setInitVal("0.181818181818182,0.163636363636364,0.145454545454545,0.127272727272727,0.109090909090909,0.0909090909090909,0.0727272727272727,0.0545454545454545,0.0363636363636364,0.0181818181818182");
    pis = std::make_shared<TMCMCParameter<double>>(std::move(priorPis), defPis, &randomGenerator);
    params.push_back(pis);

    // now initialize priorWithHyperPrior
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    // correct logPriorRatio and logPriorDensity?
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(0, 1),  0.1053605157);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(0, 1), priorWithHyperPrior->getLogPriorDensity(0) - priorWithHyperPrior->getLogPriorDensity(1));
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(9, 8), -0.6931471806);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(9, 8), priorWithHyperPrior->getLogPriorDensity(9) - priorWithHyperPrior->getLogPriorDensity(8));
}

TEST_F(TPriorCategoricalWithHyperPriorTest, update_Fixed){
    // first construct lambda
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defPis->update(false);
    defPis->setInitVal("0.181818181818182,0.163636363636364,0.145454545454545,0.127272727272727,0.109090909090909,0.0909090909090909,0.0727272727272727,0.0545454545454545,0.0363636363636364,0.0181818181818182");
    pis = std::make_shared<TMCMCParameter<double>>(std::move(priorPis), defPis, &randomGenerator);
    params.push_back(pis);

    // now initialize priorWithHyperPrior
    MockTPriorCategoricalWithHyperPrior mockPriorWithHyperPrior;
    mockPriorWithHyperPrior.initPriorWithHyperPrior(params, observations);
    mockPriorWithHyperPrior.initParameter(param);
    mockPriorWithHyperPrior.initializeStorageOfPriorParameters();

    // should never update
    EXPECT_CALL(mockPriorWithHyperPrior, _updatePi).Times(0);
    mockPriorWithHyperPrior.updateParams();
}

TEST_F(TPriorCategoricalWithHyperPriorTest, updatePi){
    // first construct lambda
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    priorPis = std::make_unique<TPriorBeta<double>>(); // set some prior on pi, such that prior does not cancel out in hastings ratio
    priorPis->initialize("0.1,0.2");
    defPis->setInitVal("0.181818181818182,0.163636363636364,0.145454545454545,0.127272727272727,0.109090909090909,0.0909090909090909,0.0727272727272727,0.0545454545454545,0.0363636363636364,0.0181818181818182");
    pis = std::make_shared<TMCMCParameter<double>>(std::move(priorPis), defPis, &randomGenerator);
    params.push_back(pis);

    // now initialize priorWithHyperPrior
    auto bridgePriorCategoricalWithHyperPrior = std::make_shared<BridgeTPriorCategoricalWithHyperPrior>();
    bridgePriorCategoricalWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgePriorCategoricalWithHyperPrior->initParameter(param);
    bridgePriorCategoricalWithHyperPrior->initializeStorageOfPriorParameters();
    // change value of pi, such that oldValue and newValue are not the same
    bridgePriorCategoricalWithHyperPrior->setPiTo(0, 0.86);
    bridgePriorCategoricalWithHyperPrior->_normalizePis(true, 0);

    // other pis will be modified too (normalized to sum to 1) -> pis <- pis / sum(pis)
    std::vector<double> expected = {0.512459371614301,0.0975081256771398,0.0866738894907909,0.075839653304442,0.0650054171180932,0.0541711809317443,0.0433369447453954,0.0325027085590466,0.0216684723726977,0.0108342361863489};
    for (uint8_t k = 0; k < K; k++){
        EXPECT_FLOAT_EQ(bridgePriorCategoricalWithHyperPrior->pi(k), expected[k]);
    }

    // calculate LL
    EXPECT_FLOAT_EQ(bridgePriorCategoricalWithHyperPrior->_calcLLUpdatePi(), -10.87193);

    // calculate logH
    EXPECT_FLOAT_EQ(bridgePriorCategoricalWithHyperPrior->_calcLogHUpdatePi(), -10.87193 - 0.5184241);
}

TEST_F(TPriorCategoricalWithHyperPriorTest, getSumLogPriorDensity){
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defPis->setInitVal("0.181818181818182,0.163636363636364,0.145454545454545,0.127272727272727,0.109090909090909,0.0909090909090909,0.0727272727272727,0.0545454545454545,0.0363636363636364,0.0181818181818182");
    pis = std::make_shared<TMCMCParameter<double>>(std::move(priorPis), defPis, &randomGenerator);
    params.push_back(pis);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    // resize param
    def = std::make_shared<TParameterDefinition>("defParam");
    def->setDimensions({3});
    def->setMax("9");
    param = std::make_shared<BridgeTMCMCArrayParameter<uint8_t>>(priorWithHyperPrior, def, &randomGenerator);
    param->handPointerToPrior(param);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param->initializeStorageBasedOnPrior({3}, {dimNames});
    param->initializeStorage();
    param->set(0, 0);
    param->set(1, 4);
    param->set(2, 2);

    double sum = priorWithHyperPrior->getSumLogPriorDensity(param);
    EXPECT_FLOAT_EQ(sum, -5.848213);
}

TEST_F(TPriorCategoricalWithHyperPriorTest, getLogPriorDensityFull){
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defPis->setInitVal("0.181818181818182,0.163636363636364,0.145454545454545,0.127272727272727,0.109090909090909,0.0909090909090909,0.0727272727272727,0.0545454545454545,0.0363636363636364,0.0181818181818182");
    priorPis = std::make_unique<TPriorBeta<double>>(); // set some prior on pi, such that prior does not cancel out in hastings ratio
    priorPis->initialize("0.1,0.2");
    pis = std::make_shared<TMCMCParameter<double>>(std::move(priorPis), defPis, &randomGenerator);
    params.push_back(pis);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    // resize param
    def = std::make_shared<TParameterDefinition>("defParam");
    def->setDimensions({3});
    def->setMax("9");
    param = std::make_shared<BridgeTMCMCArrayParameter<uint8_t>>(priorWithHyperPrior, def, &randomGenerator);
    param->handPointerToPrior(param);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param->initializeStorageBasedOnPrior({3}, {dimNames});
    param->initializeStorage();
    param->set(0, 0);
    param->set(1, 4);
    param->set(2, 2);

    double sum = priorWithHyperPrior->getLogPriorDensityFull(param);
    EXPECT_FLOAT_EQ(sum, -5.848213 - 3.481365);
}

TEST_F(TPriorCategoricalWithHyperPriorTest, getLogPriorDensityAndRatio){
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defPis->setInitVal("0.181818181818182,0.163636363636364,0.145454545454545,0.127272727272727,0.109090909090909,0.0909090909090909,0.0727272727272727,0.0545454545454545,0.0363636363636364,0.0181818181818182");
    pis = std::make_shared<TMCMCParameter<double>>(std::move(priorPis), defPis, &randomGenerator);
    params.push_back(pis);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    // resize param
    def = std::make_shared<TParameterDefinition>("defParam");
    def->setDimensions({3});
    def->setMax("9");
    param = std::make_shared<BridgeTMCMCArrayParameter<uint8_t>>(priorWithHyperPrior, def, &randomGenerator);
    param->handPointerToPrior(param);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param->initializeStorageBasedOnPrior({3}, {dimNames});
    param->initializeStorage();
    param->set(0, 0);
    param->set(1, 4);
    param->set(2, 2);

    double val = priorWithHyperPrior->getLogPriorDensity(param, 1);
    EXPECT_FLOAT_EQ(val, log(0.109090909090909));
    val = priorWithHyperPrior->getLogPriorRatio(param, 1);
    EXPECT_FLOAT_EQ(val, log(0.109090909090909) - log(0.181818181818182)); // 0 -> 4
}

TEST_F(TPriorCategoricalWithHyperPriorTest, _setPiToMLE_1param){
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    pis = std::make_shared<TMCMCParameter<double>>(std::move(priorPis), defPis, &randomGenerator);
    params.push_back(pis);

    // now initialize priorWithHyperPrior
    auto bridgePriorCategoricalWithHyperPrior = std::make_shared<BridgeTPriorCategoricalWithHyperPrior>();
    bridgePriorCategoricalWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgePriorCategoricalWithHyperPrior->initParameter(param);
    bridgePriorCategoricalWithHyperPrior->initializeStorageOfPriorParameters();
    bridgePriorCategoricalWithHyperPrior->estimateInitialPriorParameters(&logfile);

    // equivalent to: mean(vals) in R
    std::vector<double> expected ={9.99990000099999e-06,0.0476185714333333,0.0952371428666666,0.0952371428666666,0.0952371428666666,0.1428557143,0.190474285733333,0.238092857166666,0.0476185714333333,0.0476185714333333};
    for (uint8_t k = 0; k < K; k++){
        EXPECT_FLOAT_EQ(pis->value<double>(k), expected[k]);
    }
}

TEST_F(TPriorCategoricalWithHyperPriorTest, simulateCategorical){
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defPis->update(false);
    defPis->setInitVal("0.584453535943892,0.175336060783168,0.116890707188778,0.0584453535943892,0.0292226767971946,0.0146113383985973,0.0116890707188779,0.00584453535943893,0.00292226767971946,0.000584453535943893");
    pis = std::make_shared<TMCMCParameter<double>>(std::move(priorPis), defPis, &randomGenerator);
    params.push_back(pis);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    size_t n = 100000;
    // reset dimensions of parameter
    def = std::make_shared<TParameterDefinition>("defParam");
    def->setDimensions({n});
    def->setMax("9");
    param = std::make_shared<BridgeTMCMCArrayParameter<uint8_t>>(priorWithHyperPrior, def, &randomGenerator);
    priorWithHyperPrior->initParameter(param);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param->initializeStorageBasedOnPrior({n}, {dimNames});
    param->initializeStorage();
    TBoundary boundary = param->getBoundary();

    TParameters parameters;
    priorWithHyperPrior->simulateUnderPrior(&randomGenerator, &logfile, parameters);

    // check if this matches expectations: count categories
    std::vector<size_t > counterPerCategory(K);
    for (size_t i = 0; i < n; i++){
        uint8_t cat = param->value<uint8_t>(i);
        EXPECT_TRUE(cat >= 0);
        EXPECT_TRUE(cat < K);
        counterPerCategory[cat]++;
    }

    std::vector<double> expected = {0.584453535943892,0.175336060783168,0.116890707188778,0.0584453535943892,0.0292226767971946,0.0146113383985973,0.0116890707188779,0.00584453535943893,0.00292226767971946,0.000584453535943893}; // pis from above
    for (uint8_t k = 0; k < K; k++){
        double percentWithThisCategory = static_cast<double>(counterPerCategory[k])/static_cast<double>(n); // percentage of values with this category
        EXPECT_NEAR(percentWithThisCategory, expected[k], 0.01); // should be very close to pi
    }
}

//--------------------------------------------
// TPriorHMMBoolWithHyperPrior

class MockTPriorHMMBoolWithHyperPrior : public TPriorHMMBoolWithHyperPrior<bool> {
public:
    MockTPriorHMMBoolWithHyperPrior()
            : TPriorHMMBoolWithHyperPrior() {};
    ~MockTPriorHMMBoolWithHyperPrior() override = default;
    MOCK_METHOD(void, _updateGeneratingMatrixParameter, (std::shared_ptr<TMCMCParameterBase> & lambda), (override));
};

class BridgeTPriorHMMBoolWithHyperPrior : public TPriorHMMBoolWithHyperPrior<bool> {
public:
    BridgeTPriorHMMBoolWithHyperPrior() : TPriorHMMBoolWithHyperPrior() {};
    // update alpha
    double _calcLLUpdateGeneratingMatrixParameter(){
        return TPriorHMMBoolWithHyperPrior::_calcLLUpdateGeneratingMatrixParameter();
    }
    double _calcLogHUpdateGeneratingMatrixParameter(std::shared_ptr<TMCMCParameterBase> & lambda){
        return TPriorHMMBoolWithHyperPrior::_calcLogHUpdateGeneratingMatrixParameter(lambda);
    }
    double _calcLogPOfValueGivenPrevious(const std::shared_ptr<TMCMCObservationsBase> & param, const size_t & i) {
        return TPriorHMMBoolWithHyperPrior::_calcLogPOfValueGivenPrevious(param, i);
    }
    double _calcLogPOfNextGivenValue(const std::shared_ptr<TMCMCObservationsBase> & param, const size_t & i) {
        return TPriorHMMBoolWithHyperPrior::_calcLogPOfNextGivenValue(param, i);
    }
    double _calcLogPOfOldValueGivenPrevious(const std::shared_ptr<TMCMCParameterBase> & param, const size_t & i) {
        return TPriorHMMBoolWithHyperPrior::_calcLogPOfOldValueGivenPrevious(param, i);
    }
    double _calcLogPOfNextGivenOldValue(const std::shared_ptr<TMCMCParameterBase> & param, const size_t & i) {
        return TPriorHMMBoolWithHyperPrior::_calcLogPOfNextGivenOldValue(param, i);
    }
    void _updateTransitionMatrices() {
        TPriorHMMBoolWithHyperPrior::_updateTransitionMatrices();
    }
    void _fillCurrentGeneratingMatrixParameters(std::vector<double> & Values) override{
        TPriorHMMBoolWithHyperPrior::_fillCurrentGeneratingMatrixParameters(Values);
    }

    // functions to tweak value of lambda1 and lambda2 (for controlling updates, we can't directly access them from outside)
    void setLambda1To(double x){
        _lambda1->set(x);
    }
    void setLambda2To(double x){
        _lambda2->set(x);
    }
};

class TTPriorHMMBoolWithHyperPriorTest : public Test {
protected:
    std::shared_ptr<BridgeTPriorHMMBoolWithHyperPrior> priorWithHyperPrior;

    std::shared_ptr<TParameterDefinition> defLambda1;
    std::shared_ptr<TParameterDefinition> defLambda2;
    std::unique_ptr<TPrior<double>> priorLambda1;
    std::unique_ptr<TPrior<double>> priorLambda2;
    std::shared_ptr<TMCMCParameter<double>> lambda1;
    std::shared_ptr<TMCMCParameter<double>> lambda2;

    std::shared_ptr<BridgeTMCMCArrayParameter<bool>> param; // parameter on which prior is defined
    std::shared_ptr<TParameterDefinition> def;

    std::shared_ptr<TDistancesBinnedBase> distances;
    std::vector<std::shared_ptr<TObservationsBase>> observations = {};

    TRandomGenerator randomGenerator;

    void SetUp() override {
        defLambda1 = std::make_shared<TParameterDefinition>("defLambda1");
        defLambda2 = std::make_shared<TParameterDefinition>("defLambda2");

        priorLambda1 = std::make_unique<TPriorBeta<double>>();
        priorLambda2 = std::make_unique<TPriorBeta<double>>();

        // initialize param
        priorWithHyperPrior = std::make_shared<BridgeTPriorHMMBoolWithHyperPrior>();
        def = std::make_shared<TParameterDefinition>("defParam");
        def->setDimensions({21});
        def->setInitVal("0,1,1,0,0,1,1,0,0,1,1,0,0,1,1,0,0,1,1,0,0");
        param = std::make_shared<BridgeTMCMCArrayParameter<bool>>(priorWithHyperPrior, def, &randomGenerator);
        std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
        param->initializeStorageBasedOnPrior({21}, {dimNames});

        // fill distances
        distances = std::make_shared<TDistancesBinned<uint8_t>>(16);
        for (size_t i = 0; i < 21; i++){
            distances->add(i*i, "junk_1");
        }
        distances->finalizeFilling();
    }

    void prepareParamForRatios(){
        bool val = true;
        for (size_t i = 0; i < 21; i++) {
            param->set(i, val);
            param->set(i, !param->value<bool>(i)); // value is always opposite of oldValue
            if (i % 2 == 0) // jump every 2nd element
                val = !val;
        }
    }

    void TearDown() override {}
};

TEST_F(TTPriorHMMBoolWithHyperPriorTest, initPriorWithHyperPrior_wrongNumParams){
    // first construct lambda1 and lambda2
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    lambda1 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda1), defLambda1, &randomGenerator);
    lambda2 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda2), defLambda2, &randomGenerator);
    params.push_back(lambda1);
    params.push_back(lambda2);
    params.push_back(lambda2); // one too much

    // now initialize priorWithHyperPrior - throws because 3 instead of 2 parameters were given
    EXPECT_THROW(priorWithHyperPrior->initPriorWithHyperPrior(params, observations), std::runtime_error);
}


TEST_F(TTPriorHMMBoolWithHyperPriorTest, initPriorWithHyperPrior){
    // first construct lambda1 and lambda2
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    lambda1 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda1), defLambda1, &randomGenerator);
    lambda2 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda2), defLambda2, &randomGenerator);
    params.push_back(lambda1);
    params.push_back(lambda2);

    // now initialize priorWithHyperPrior - check if lambda1 and lambda2 are correctly initialized
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->initializeStorage();
    EXPECT_EQ(priorWithHyperPrior->lambda1(), 1e-05);
    EXPECT_EQ(priorWithHyperPrior->lambda2(), 1e-05);
}

TEST_F(TTPriorHMMBoolWithHyperPriorTest, initPriorWithHyperPrior_reSetInitValOfLambda1){
    // first construct alpha and beta
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defLambda1->setMin("-10.", false); // set some complete bullshit
    defLambda1->setMax("5.", false);
    defLambda1->setInitVal("-1.");

    lambda1 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda1), defLambda1, &randomGenerator); // this works, because everything is ok (we don't know yet that lambda1 must be > 0)
    lambda2 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda2), defLambda2, &randomGenerator);
    params.push_back(lambda1);
    params.push_back(lambda2);

    // now initialize priorWithHyperPrior - should re-assign min and throw because initVal is smaller than min
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}
/*
TEST_F(TTPriorHMMBoolWithHyperPriorTest, _resetTransitionMatrices){
    uint16_t numStates = 2;
    std::shared_ptr<TDistances> distances = std::make_shared<TDistances>(100);
    distances->addInitial(0); // pro forma, to avoid error that distances are not filled

    initializeWithDistances(numStates, distances);

    // change some values
    (*this)[1](0,0) = 1.; (*this)[1](0,1) = 2.;
    (*this)[1](1,0) = 3.; (*this)[1](1,1) = 4.;

    EXPECT_EQ((*this)[1](0, 0), 1.);
    EXPECT_EQ((*this)[1](0, 1), 2.);
    EXPECT_EQ((*this)[1](1, 0), 3.);
    EXPECT_EQ((*this)[1](1, 1), 4.);

    // now swap pointers -> all values should be zero
    _resetTransitionMatrices();

    EXPECT_EQ((*this)[1](0, 0), 0.);
    EXPECT_EQ((*this)[1](0, 1), 0.);
    EXPECT_EQ((*this)[1](1, 0), 0.);
    EXPECT_EQ((*this)[1](1, 1), 0.);

    // now swap again -> all values should be back to previous
    _resetTransitionMatrices();

    EXPECT_EQ((*this)[1](0, 0), 1.);
    EXPECT_EQ((*this)[1](0, 1), 2.);
    EXPECT_EQ((*this)[1](1, 0), 3.);
    EXPECT_EQ((*this)[1](1, 1), 4.);
}*/

TEST_F(TTPriorHMMBoolWithHyperPriorTest, _fillStationaryDistribution){
    // first construct alpha and beta
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defLambda1->setInitVal("0.1");
    defLambda2->setInitVal("0.2");
    lambda1 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda1), defLambda1, &randomGenerator);
    lambda2 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda2), defLambda2, &randomGenerator);
    params.push_back(lambda1);
    params.push_back(lambda2);

    // now initialize priorWithHyperPrior
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    priorWithHyperPrior->initDistances(distances);
    param->initializeStorage();

    // check stationary distribution
    EXPECT_FLOAT_EQ((*priorWithHyperPrior)[0](0,0),0.6666667);
    EXPECT_FLOAT_EQ((*priorWithHyperPrior)[0](0,1), 0.3333333);
    EXPECT_FLOAT_EQ((*priorWithHyperPrior)[0](1,0), 0.6666667);
    EXPECT_FLOAT_EQ((*priorWithHyperPrior)[0](1,1), 0.3333333);
}

TEST_F(TTPriorHMMBoolWithHyperPriorTest, _calcLogPOfValueGivenPrevious){
    // first construct alpha and beta
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defLambda1->setInitVal("0.1");
    defLambda2->setInitVal("0.2");
    lambda1 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda1), defLambda1, &randomGenerator);
    lambda2 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda2), defLambda2, &randomGenerator);
    params.push_back(lambda1);
    params.push_back(lambda2);

    // now initialize priorWithHyperPrior
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    priorWithHyperPrior->initDistances(distances);
    param->initializeStorage();
    prepareParamForRatios();

    // check _calcLogPOfValueGivenPrevious
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfValueGivenPrevious(param, 0),log(0.6666667), 0.01); // value = 0; stationary distr
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfValueGivenPrevious(param, 1), log(0.08639393), 0.01); // value previous = 0; value this = 1; distGroup = 1
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfValueGivenPrevious(param, 2), log(0.6992078), 0.01); // value previous = 1; value this = 1; distGroup = 2
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfValueGivenPrevious(param, 3), log(0.4658705), 0.01); // value previous = 1; value this = 0; distGroup = 3
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfValueGivenPrevious(param, 4), log(0.7670647), 0.01); // value previous = 0; value this = 0; distGroup = 3
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfValueGivenPrevious(param, 5), log(0.303094), 0.01); // value previous = 0; value this = 1; distGroup = 4
    // ...
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfValueGivenPrevious(param, 20), log(0.696906), 0.01); // value previous = 0; value this = 0; distGroup = 4

    // check _calcLogPOfOldValueGivenPrevious
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfOldValueGivenPrevious(param, 0),log(0.3333333), 0.01); // oldValue = 1; stationary distr
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfOldValueGivenPrevious(param, 1), log(0.9136061), 0.01); // value previous = 0; oldValue this = 0; distGroup = 1
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfOldValueGivenPrevious(param, 2), log(0.3007922), 0.01); // value previous = 1; oldValue this = 0; distGroup = 2
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfOldValueGivenPrevious(param, 3), log(0.5341295), 0.01); // value previous = 1; oldValue this = 1; distGroup = 3
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfOldValueGivenPrevious(param, 4), log(0.2329353), 0.01); // value previous = 0; oldValue this = 1; distGroup = 3
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfOldValueGivenPrevious(param, 5), log(0.696906), 0.01); // value previous = 0; oldValue this = 0; distGroup = 4
    // ...
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfOldValueGivenPrevious(param, 20), log(0.303094), 0.01); // value previous = 0; oldValue this = 1; distGroup = 4
}

TEST_F(TTPriorHMMBoolWithHyperPriorTest, _calcLogPOfNextGivenValue){
    // first construct alpha and beta
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defLambda1->setInitVal("0.1");
    defLambda2->setInitVal("0.2");
    lambda1 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda1), defLambda1, &randomGenerator);
    lambda2 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda2), defLambda2, &randomGenerator);
    params.push_back(lambda1);
    params.push_back(lambda2);

    // now initialize priorWithHyperPrior
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    priorWithHyperPrior->initDistances(distances);
    param->initializeStorage();
    prepareParamForRatios();

    // check _calcLogPOfNextGivenValue
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenValue(param, 0), log(0.08639393), 0.01); // value this = 0; value next = 1, distGroup next = 1
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenValue(param, 1), log(0.6992078), 0.01); // value this = 1; value next = 1, distGroup next = 2
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenValue(param, 2), log(0.4658705), 0.01); // value this = 1; value next = 0,  distGroup next = 3
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenValue(param, 3), log(0.7670647), 0.01); // value this = 0; value next = 0,  distGroup next = 3
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenValue(param, 4), log(0.303094), 0.01); // value this = 0; value next = 1,  distGroup next = 4
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenValue(param, 5), log(0.393812), 0.01); // value this = 1; value next = 1,  distGroup next = 4
    // ...
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenValue(param, 20), 0., 0.01); // value this = 1; there is no next

    // check _calcLogPOfNextGivenOldValue
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenOldValue(param, 0), log(0.82721215), 0.01); // oldValue this = 1; value next = 1; distGroup next = 1
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenOldValue(param, 1), log(0.1503961), 0.01); // oldValue this = 0; value next = 1; distGroup next = 2
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenOldValue(param, 2), log(0.7670647), 0.01); // oldValue this = 0; value next = 0; distGroup next = 3
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenOldValue(param, 3), log(0.4658705), 0.01); // oldValue this = 1; value next = 0; distGroup next = 3
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenOldValue(param, 4), log(0.393812), 0.01); // oldValue this = 1; value next = 1; distGroup next = 4
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenOldValue(param, 5), log(0.303094), 0.01); // oldValue this = 0; value next = 1; distGroup next = 4
    // ...
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenOldValue(param, 20), 0., 0.01); // oldValue this = 0; there is no next
}

TEST_F(TTPriorHMMBoolWithHyperPriorTest, getLogPriorRatio){
    // first construct alpha and beta
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defLambda1->setInitVal("0.1");
    defLambda2->setInitVal("0.2");
    lambda1 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda1), defLambda1, &randomGenerator);
    lambda2 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda2), defLambda2, &randomGenerator);
    params.push_back(lambda1);
    params.push_back(lambda2);

    // now initialize priorWithHyperPrior
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    priorWithHyperPrior->initDistances(distances);
    param->initializeStorage();
    prepareParamForRatios();

    // correct logPriorDensity?
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorDensity(param, 0), log(0.6666667) + log(0.08639393), 0.01);
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorDensity(param, 1), log(0.08639393) + log(0.6992078), 0.01);
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorDensity(param, 2), log(0.6992078) + log(0.4658705), 0.01);
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorDensity(param, 3), log(0.4658705) + log(0.7670647), 0.01);
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorDensity(param, 4), log(0.7670647) + log(0.303094), 0.01);
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorDensity(param, 5), log(0.303094) + log(0.393812), 0.01);
    // ...
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorDensity(param, 20), log(0.696906) + 0., 0.01);

    // correct logPriorRatio?
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorRatio(param, 0), log(0.6666667) - log(0.3333333) + log(0.08639393) -  log(0.82721215), 0.01);
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorRatio(param, 1), log(0.08639393) - log(0.9136061) + log(0.6992078) -  log(0.1503961), 0.01);
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorRatio(param, 2), log(0.6992078) - log(0.3007922) + log(0.4658705) -  log(0.7670647), 0.01);
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorRatio(param, 3), log(0.4658705) - log(0.5341295) + log(0.7670647) -  log(0.4658705), 0.01);
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorRatio(param, 4), log(0.7670647) - log(0.2329353) + log(0.303094) -  log(0.393812), 0.01);
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorRatio(param, 5), log(0.303094) - log(0.696906) + log(0.393812) -  log(0.303094), 0.01);
    // ...
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorRatio(param, 20), log(0.696906) - log(0.303094) + 0. -  0., 0.01);
}


TEST_F(TTPriorHMMBoolWithHyperPriorTest, update_bothFixed){
    // first construct lambda1 and lambda2
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defLambda1->setInitVal("0.1");
    defLambda1->update(false);
    defLambda2->setInitVal("0.2");
    defLambda2->update(false);
    lambda1 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda1), defLambda1, &randomGenerator);
    lambda2 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda2), defLambda2, &randomGenerator);
    params.push_back(lambda1);
    params.push_back(lambda2);

    // now initialize priorWithHyperPrior
    MockTPriorHMMBoolWithHyperPrior mockPriorWithHyperPrior;
    mockPriorWithHyperPrior.initPriorWithHyperPrior(params, observations);
    mockPriorWithHyperPrior.initializeStorageOfPriorParameters();

    // should never update both
    EXPECT_CALL(mockPriorWithHyperPrior, _updateGeneratingMatrixParameter).Times(0);
    mockPriorWithHyperPrior.updateParams();
}

TEST_F(TTPriorHMMBoolWithHyperPriorTest, update_onlyLambda1Fixed){
    // first construct lambda1 and lambda2
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defLambda1->setInitVal("0.1");
    defLambda1->update(false);
    defLambda2->setInitVal("0.2");
    lambda1 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda1), defLambda1, &randomGenerator);
    lambda2 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda2), defLambda2, &randomGenerator);
    params.push_back(lambda1);
    params.push_back(lambda2);

    // now initialize priorWithHyperPrior
    MockTPriorHMMBoolWithHyperPrior mockPriorWithHyperPrior;
    mockPriorWithHyperPrior.initPriorWithHyperPrior(params, observations);
    mockPriorWithHyperPrior.initializeStorageOfPriorParameters();

    // should only update lambda2
    EXPECT_CALL(mockPriorWithHyperPrior, _updateGeneratingMatrixParameter).Times(1);
    mockPriorWithHyperPrior.updateParams();
}

TEST_F(TTPriorHMMBoolWithHyperPriorTest, _updateLambda1){
    // first construct lambda1 and lambda2
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defLambda1->setInitVal("0.1");
    priorLambda1 = std::make_unique<TPriorExponential<double>>(); // set some prior on lambda1, such that prior does not cancel out in hastings ratio
    priorLambda1->initialize("1");
    defLambda2->setInitVal("0.2");
    lambda1 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda1), defLambda1, &randomGenerator);
    lambda2 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda2), defLambda2, &randomGenerator);
    params.push_back(lambda1);
    params.push_back(lambda2);

    // now initialize bridgeTPriorWithHyperPrior
    auto bridgeTPriorWithHyperPrior = std::make_shared<BridgeTPriorHMMBoolWithHyperPrior>();
    bridgeTPriorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgeTPriorWithHyperPrior->initDistances(distances);
    bridgeTPriorWithHyperPrior->initParameter(param);
    bridgeTPriorWithHyperPrior->initializeStorageOfPriorParameters();

    // change value of lambda1, such that oldValue and newValue are not the same
    bridgeTPriorWithHyperPrior->setLambda1To(0.3);
    bridgeTPriorWithHyperPrior->_updateTransitionMatrices();

    // calculate LL
    EXPECT_NEAR(bridgeTPriorWithHyperPrior->_calcLLUpdateGeneratingMatrixParameter(), 0.33781, 0.01);

    // calculate logH
    std::shared_ptr<TMCMCParameterBase> lambda = lambda1;
    EXPECT_NEAR(bridgeTPriorWithHyperPrior->_calcLogHUpdateGeneratingMatrixParameter(lambda), 0.33781 - 0.2, 0.01);
}

TEST_F(TTPriorHMMBoolWithHyperPriorTest, _updateLambda2){
    // first construct lambda1 and lambda2
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defLambda1->setInitVal("0.1");
    defLambda2->setInitVal("0.2");
    priorLambda2 = std::make_unique<TPriorExponential<double>>(); // set some prior on lambda2, such that prior does not cancel out in hastings ratio
    priorLambda2->initialize("1");
    lambda1 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda1), defLambda1, &randomGenerator);
    lambda2 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda2), defLambda2, &randomGenerator);
    params.push_back(lambda1);
    params.push_back(lambda2);

    // now initialize priorWithHyperPrior
    auto bridgeTPriorWithHyperPrior = std::make_shared<BridgeTPriorHMMBoolWithHyperPrior>();
    bridgeTPriorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgeTPriorWithHyperPrior->initDistances(distances);
    bridgeTPriorWithHyperPrior->initParameter(param);
    bridgeTPriorWithHyperPrior->initializeStorageOfPriorParameters();

    // change value of lambda1, such that oldValue and newValue are not the same
    bridgeTPriorWithHyperPrior->setLambda2To(0.3);
    bridgeTPriorWithHyperPrior->_updateTransitionMatrices();

    // calculate LL
    EXPECT_NEAR(bridgeTPriorWithHyperPrior->_calcLLUpdateGeneratingMatrixParameter(), -1.07118, 0.01);

    // calculate logH
    std::shared_ptr<TMCMCParameterBase> lambda = lambda2;
    EXPECT_NEAR(bridgeTPriorWithHyperPrior->_calcLogHUpdateGeneratingMatrixParameter(lambda), -1.07118 - 0.1, 0.01);
}

TEST_F(TTPriorHMMBoolWithHyperPriorTest, getSumLogPriorDensity){
    // first construct lambda1 and lambda2
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defLambda1->setInitVal("0.1");
    defLambda2->setInitVal("0.2");
    lambda1 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda1), defLambda1, &randomGenerator);
    lambda2 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda2), defLambda2, &randomGenerator);
    params.push_back(lambda1);
    params.push_back(lambda2);

    // now initialize priorWithHyperPrior
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    priorWithHyperPrior->initDistances(distances);
    priorWithHyperPrior->initializeStorageOfPriorParameters();
    prepareParamForRatios();

    double sum = priorWithHyperPrior->getSumLogPriorDensity(param);
    EXPECT_NEAR(sum, -16.1902, 0.01);
}

TEST_F(TTPriorHMMBoolWithHyperPriorTest, getLogPriorDensityFull){
    // first construct lambda1 and lambda2
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defLambda1->setInitVal("0.1");
    defLambda2->setInitVal("0.2");
    priorLambda1 = std::make_unique<TPriorExponential<double>>(); // set some prior on lambda1
    priorLambda1->initialize("2");
    priorLambda2 = std::make_unique<TPriorExponential<double>>(); // set some prior on lambda2
    priorLambda2->initialize("1");
    lambda1 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda1), defLambda1, &randomGenerator);
    lambda2 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda2), defLambda2, &randomGenerator);
    params.push_back(lambda1);
    params.push_back(lambda2);

    // now initialize priorWithHyperPrior
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    priorWithHyperPrior->initDistances(distances);
    priorWithHyperPrior->initializeStorageOfPriorParameters();
    prepareParamForRatios();

    double sum = priorWithHyperPrior->getLogPriorDensityFull(param);
    EXPECT_NEAR(sum, -16.1902 + 0.2931472, 0.01);
}


TEST_F(TTPriorHMMBoolWithHyperPriorTest, _updateLambda2_twoParameterArrays){
    // first construct lambda1 and lambda2
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    defLambda1->setInitVal("0.1");
    defLambda2->setInitVal("0.2");
    priorLambda2 = std::make_unique<TPriorExponential<double>>(); // set some prior on lambda2, such that prior does not cancel out in hastings ratio
    priorLambda2->initialize("1");
    lambda1 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda1), defLambda1, &randomGenerator);
    lambda2 = std::make_shared<TMCMCParameter<double>>(std::move(priorLambda2), defLambda2, &randomGenerator);
    params.push_back(lambda1);
    params.push_back(lambda2);

    // now initialize priorWithHyperPrior
    auto bridgeTPriorWithHyperPrior = std::make_shared<BridgeTPriorHMMBoolWithHyperPrior>();
    bridgeTPriorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgeTPriorWithHyperPrior->initDistances(distances);

    // HMM prior is on two parameter arrays together
    auto def2 = std::make_shared<TParameterDefinition>("defParam2");
    def2->setDimensions({21});
    def2->setInitVal("1,0,0,1,1,0,0,1,1,0,0,1,1,0,0,1,1,0,0,1,1");
    std::shared_ptr<BridgeTMCMCArrayParameter<bool>> param2(new BridgeTMCMCArrayParameter<bool>(bridgeTPriorWithHyperPrior, def2, &randomGenerator));
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param2->initializeStorageBasedOnPrior({21}, {dimNames});
    bridgeTPriorWithHyperPrior->initParameter(param);
    bridgeTPriorWithHyperPrior->initParameter(param2);
    bridgeTPriorWithHyperPrior->initializeStorageOfPriorParameters();

    // change value of lambda1, such that oldValue and newValue are not the same
    bridgeTPriorWithHyperPrior->setLambda2To(0.3);
    bridgeTPriorWithHyperPrior->_updateTransitionMatrices();

    // calculate LL
    EXPECT_NEAR(bridgeTPriorWithHyperPrior->_calcLLUpdateGeneratingMatrixParameter(), -2.68629, 0.05);

    // calculate logH
    std::shared_ptr<TMCMCParameterBase> lambda = lambda2;
    EXPECT_NEAR(bridgeTPriorWithHyperPrior->_calcLogHUpdateGeneratingMatrixParameter(lambda), -2.68629 - 0.1, 0.05);
}

//--------------------------------------------
// TPriorHMMLadderWithHyperPrior

class BridgeTPriorHMMLadderWithHyperPrior : public TPriorHMMLadderWithHyperPrior<uint8_t> {
public:
    BridgeTPriorHMMLadderWithHyperPrior() : TPriorHMMLadderWithHyperPrior() {};
    double _calcLLUpdateGeneratingMatrixParameter(){
        return TPriorHMMLadderWithHyperPrior::_calcLLUpdateGeneratingMatrixParameter();
    }
    double _calcLogHUpdateGeneratingMatrixParameter(std::shared_ptr<TMCMCParameterBase> & lambda){
        return TPriorHMMLadderWithHyperPrior::_calcLogHUpdateGeneratingMatrixParameter(lambda);
    }
    double _calcLogPOfValueGivenPrevious(const std::shared_ptr<TMCMCObservationsBase> & param, const size_t & i) {
        return TPriorHMMLadderWithHyperPrior::_calcLogPOfValueGivenPrevious(param, i);
    }
    double _calcLogPOfNextGivenValue(const std::shared_ptr<TMCMCObservationsBase> & param, const size_t & i) {
        return TPriorHMMLadderWithHyperPrior::_calcLogPOfNextGivenValue(param, i);
    }
    double _calcLogPOfOldValueGivenPrevious(const std::shared_ptr<TMCMCParameterBase> & param, const size_t & i) {
        return TPriorHMMLadderWithHyperPrior::_calcLogPOfOldValueGivenPrevious(param, i);
    }
    double _calcLogPOfNextGivenOldValue(const std::shared_ptr<TMCMCParameterBase> & param, const size_t & i) {
        return TPriorHMMLadderWithHyperPrior::_calcLogPOfNextGivenOldValue(param, i);
    }
    void _updateTransitionMatrices() {
        TPriorHMMLadderWithHyperPrior::_updateTransitionMatrices();
    }
    void _fillCurrentGeneratingMatrixParameters(std::vector<double> & Values) override{
        TPriorHMMLadderWithHyperPrior::_fillCurrentGeneratingMatrixParameters(Values);
    }

    // functions to tweak value of lambda1 and lambda2 (for controlling updates, we can't directly access them from outside)
    void setKappaTo(double x){
        _kappa->set(x);
    }
};

class TPriorHMMLadderWithHyperPriorTest : public Test {
protected:
    std::shared_ptr<BridgeTPriorHMMLadderWithHyperPrior> priorWithHyperPrior;

    std::shared_ptr<TParameterDefinition> defKappa;
    std::unique_ptr<TPrior<double>> priorKappa;
    std::shared_ptr<TMCMCParameter<double>> kappa;

    std::shared_ptr<TParameterDefinition> defNumStates;
    std::unique_ptr<TPrior<uint8_t>> priorNumStates;
    std::shared_ptr<TMCMCParameter<uint8_t>> numStates;

    std::shared_ptr<BridgeTMCMCArrayParameter<uint8_t>> param; // parameter on which prior is defined
    std::shared_ptr<TParameterDefinition> def;

    std::shared_ptr<TDistancesBinnedBase> distances;
    std::vector<std::shared_ptr<TObservationsBase>> observations = {};

    TRandomGenerator randomGenerator;

    void SetUp() override {
        defKappa = std::make_shared<TParameterDefinition>("defKappa");
        priorKappa = std::make_unique<TPriorBeta<double>>();

        defNumStates = std::make_shared<TParameterDefinition>("defNumStates");
        defNumStates->setType("uint8_t");
        defNumStates->setInitVal("3");
        defNumStates->update(false);
        priorNumStates = std::make_unique<TPriorUniform<uint8_t>>();
        numStates = std::make_shared<TMCMCParameter<uint8_t>>(std::move(priorNumStates), defNumStates, &randomGenerator);

        // initialize param
        priorWithHyperPrior = std::make_shared<BridgeTPriorHMMLadderWithHyperPrior>();
        priorWithHyperPrior->setNumStates(3);
        def = std::make_shared<TParameterDefinition>("defParam");
        def->setDimensions({21});
        def->setInitVal("0,0,0,0,2,0,2,2,2,2,0,0,0,1,1,2,1,2,2,1,2");
        param = std::make_shared<BridgeTMCMCArrayParameter<uint8_t>>(priorWithHyperPrior, def, &randomGenerator);
        std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
        param->initializeStorageBasedOnPrior({21}, {dimNames});

        // fill distances
        distances = std::make_shared<TDistancesBinned<uint8_t>>(16);
        for (size_t i = 0; i < 21; i++){
            distances->add(i*i, "junk_1");
        }
        distances->finalizeFilling();
    }

    void prepareParamForRatios(){
        std::vector<uint8_t> oldZ = {1,1,1,1,0,1,0,0,0,0,1,1,1,2,2,0,2,0,0,2,0};
        std::vector<uint8_t> curZ = {0,0,0,0,2,0,2,2,2,2,0,0,0,1,1,2,1,2,2,1,2};
        for (size_t i = 0; i < 21; i++) {
            param->set(i, oldZ[i]);
            param->set(i, curZ[i]);
        }
    }

    void TearDown() override {}
};

TEST_F(TPriorHMMLadderWithHyperPriorTest, initPriorWithHyperPrior_wrongNumParams){
    // first construct kappa
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    kappa = std::make_shared<TMCMCParameter<double>>(std::move(priorKappa), defKappa, &randomGenerator);
    params.push_back(kappa);
    params.push_back(numStates);
    params.push_back(kappa); // one too much

    // now initialize priorWithHyperPrior - throws because 2 instead of 1 parameters were given
    EXPECT_THROW(priorWithHyperPrior->initPriorWithHyperPrior(params, observations), std::runtime_error);
}


TEST_F(TPriorHMMLadderWithHyperPriorTest, initPriorWithHyperPrior){
    // first construct kappa
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    kappa = std::make_shared<TMCMCParameter<double>>(std::move(priorKappa), defKappa, &randomGenerator);
    params.push_back(kappa);
    params.push_back(numStates);

    // now initialize priorWithHyperPrior - check if lambda1 and lambda2 are correctly initialized
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->initializeStorage();
    EXPECT_EQ(priorWithHyperPrior->kappa(), 1e-05);
}

TEST_F(TPriorHMMLadderWithHyperPriorTest, initPriorWithHyperPrior_reSetInitValOfKappa){
    // first construct kappa
    defKappa->setMin("-10.", false); // set some complete bullshit
    defKappa->setMax("5.", false);
    defKappa->setInitVal("-1.");

    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    kappa = std::make_shared<TMCMCParameter<double>>(std::move(priorKappa), defKappa, &randomGenerator);
    params.push_back(kappa);
    params.push_back(numStates);

    // now initialize priorWithHyperPrior - should re-assign min and throw because initVal is smaller than min
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TPriorHMMLadderWithHyperPriorTest, _fillStationaryDistribution){
    // first construct kappa
    defKappa->setInitVal("0.2");
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    kappa = std::make_shared<TMCMCParameter<double>>(std::move(priorKappa), defKappa, &randomGenerator);
    params.push_back(kappa);
    params.push_back(numStates);

    // now initialize priorWithHyperPrior
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    priorWithHyperPrior->initDistances(distances);
    param->initializeStorage();

    // check stationary distribution
    EXPECT_FLOAT_EQ((*priorWithHyperPrior)[0](0,0),0.3333333); EXPECT_FLOAT_EQ((*priorWithHyperPrior)[0](0,1),0.3333333); EXPECT_FLOAT_EQ((*priorWithHyperPrior)[0](0,2),0.3333333);
    EXPECT_FLOAT_EQ((*priorWithHyperPrior)[0](1,0),0.3333333); EXPECT_FLOAT_EQ((*priorWithHyperPrior)[0](1,1),0.3333333); EXPECT_FLOAT_EQ((*priorWithHyperPrior)[0](1,2),0.3333333);
    EXPECT_FLOAT_EQ((*priorWithHyperPrior)[0](2,0),0.3333333); EXPECT_FLOAT_EQ((*priorWithHyperPrior)[0](2,1),0.3333333); EXPECT_FLOAT_EQ((*priorWithHyperPrior)[0](2,2),0.3333333);
}

TEST_F(TPriorHMMLadderWithHyperPriorTest, _calcLogPOfValueGivenPrevious){
    // first construct kappa
    defKappa->setInitVal("0.2");
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    kappa = std::make_shared<TMCMCParameter<double>>(std::move(priorKappa), defKappa, &randomGenerator);
    params.push_back(kappa);
    params.push_back(numStates);

    // now initialize priorWithHyperPrior
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    priorWithHyperPrior->initDistances(distances);
    param->initializeStorage();
    prepareParamForRatios();

    // check _calcLogPOfValueGivenPrevious
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfValueGivenPrevious(param, 0), log(0.3333333), 0.01); // value = 0; stationary distr
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfValueGivenPrevious(param, 1), log(0.8341673), 0.01); // value previous = 0; value this = 0; distGroup = 1
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfValueGivenPrevious(param, 2), log(0.7186924), 0.01); // value previous = 0; value this = 0; distGroup = 2
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfValueGivenPrevious(param, 3), log(0.5731175), 0.01); // value previous = 0; value this = 0; distGroup = 3
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfValueGivenPrevious(param, 4), log(0.1237885), 0.01); // value previous = 0; value this = 2; distGroup = 3
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfValueGivenPrevious(param, 5), log(0.2337567), 0.01); // value previous = 2; value this = 0; distGroup = 4
    // ...
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfValueGivenPrevious(param, 20), log(0.3305901), 0.01); // value previous = 1; value this = 2; distGroup = 4

    // check _calcLogPOfOldValueGivenPrevious
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfOldValueGivenPrevious(param, 0),log(0.3333333), 0.01); // oldValue = 0; stationary distr
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfOldValueGivenPrevious(param, 1), log(0.1503961), 0.01); // value previous = 0; oldValue this = 1; distGroup = 1
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfOldValueGivenPrevious(param, 2), log(0.2329353), 0.01); // value previous = 0; oldValue this = 1; distGroup = 2
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfOldValueGivenPrevious(param, 3), log(0.303094), 0.01); // value previous = 0; oldValue this = 1; distGroup = 3
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfOldValueGivenPrevious(param, 4), log(0.5731175), 0.01); // value previous = 0; oldValue this = 0; distGroup = 3
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfOldValueGivenPrevious(param, 5), log(0.3305901), 0.01); // value previous = 2; oldValue this = 1; distGroup = 4
    // ...
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfOldValueGivenPrevious(param, 20), log(0.3305901), 0.01); // value previous = 1; oldValue this = 0; distGroup = 4
}


TEST_F(TPriorHMMLadderWithHyperPriorTest, _calcLogPOfNextGivenValue){
    // first construct kappa
    defKappa->setInitVal("0.2");
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    kappa = std::make_shared<TMCMCParameter<double>>(std::move(priorKappa), defKappa, &randomGenerator);
    params.push_back(kappa);
    params.push_back(numStates);

    // now initialize priorWithHyperPrior
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    priorWithHyperPrior->initDistances(distances);
    param->initializeStorage();
    prepareParamForRatios();

    // check _calcLogPOfNextGivenValue
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenValue(param, 0), log(0.8341673), 0.01); // value this = 0; value next = 0, distGroup next = 1
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenValue(param, 1), log(0.7186924), 0.01); // value this = 0; value next = 0, distGroup next = 2
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenValue(param, 2), log(0.5731175), 0.01); // value this = 0; value next = 0,  distGroup next = 3
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenValue(param, 3), log(0.1237885), 0.01); // value this = 0; value next = 2,  distGroup next = 3
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenValue(param, 4), log(0.2337567), 0.01); // value this = 2; value next = 0,  distGroup next = 4
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenValue(param, 5), log(0.2337567), 0.01); // value this = 0; value next = 2,  distGroup next = 4
    // ...
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenValue(param, 20), 0., 0.01); // value this = 1; there is no next

    // check _calcLogPOfNextGivenOldValue
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenOldValue(param, 0), log(0.1503961), 0.01); // oldValue this = 0; value next = 0; distGroup next = 1
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenOldValue(param, 1), log(0.2329353), 0.01); // oldValue this = 0; value next = 0; distGroup next = 2
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenOldValue(param, 2), log(0.303094), 0.01); // oldValue this = 0; value next = 2; distGroup next = 3
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenOldValue(param, 3), log(0.303094), 0.01); // oldValue this = 0; value next = 2; distGroup next = 3
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenOldValue(param, 4), log(0.4356532), 0.01); // oldValue this = 2; value next = 0; distGroup next = 4
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenOldValue(param, 5), log(0.3305901), 0.01); // oldValue this = 0; value next = 2; distGroup next = 4
    // ...
    EXPECT_NEAR(priorWithHyperPrior->_calcLogPOfNextGivenOldValue(param, 20), 0., 0.01); // oldValue this = 0; there is no next
}

TEST_F(TPriorHMMLadderWithHyperPriorTest, getLogPriorRatio){
    // first construct kappa
    defKappa->setInitVal("0.2");
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    kappa = std::make_shared<TMCMCParameter<double>>(std::move(priorKappa), defKappa, &randomGenerator);
    params.push_back(kappa);
    params.push_back(numStates);

    // now initialize priorWithHyperPrior
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    priorWithHyperPrior->initDistances(distances);
    param->initializeStorage();
    prepareParamForRatios();

    // correct logPriorDensity?
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorDensity(param, 0), log(0.333333) + log(0.8341673), 0.01);
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorDensity(param, 1), log(0.8341673) + log(0.7186924), 0.01);
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorDensity(param, 2), log(0.7186924) + log(0.5731175), 0.01);
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorDensity(param, 3), log(0.5731175) + log(0.1237885), 0.01);
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorDensity(param, 4), log(0.1237885) + log(0.2337567), 0.01);
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorDensity(param, 5), log(0.2337567) + log(0.2337567), 0.01);
    // ...
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorDensity(param, 20), log(0.3305901) + 0., 0.01);

    // correct logPriorRatio?
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorRatio(param, 0), log(0.3333333) - log(0.3333333) + log(0.8341673) -  log(0.1503961), 0.01);
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorRatio(param, 1), log(0.8341673) - log(0.1503961) + log(0.7186924) -  log(0.2329353), 0.01);
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorRatio(param, 2), log(0.7186924) - log(0.2329353) + log(0.5731175) -  log(0.303094), 0.01);
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorRatio(param, 3), log(0.5731175) - log(0.303094) + log(0.1237885) -  log(0.303094), 0.01);
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorRatio(param, 4), log(0.1237885) - log(0.5731175) + log(0.2337567) -  log(0.4356532), 0.01);
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorRatio(param, 5), log(0.2337567) - log(0.3305901) + log(0.2337567) -  log(0.3305901), 0.01);
    // ...
    EXPECT_NEAR(priorWithHyperPrior->getLogPriorRatio(param, 20), log(0.3305901) - log(0.3305901) + 0. -  0., 0.01);
}

TEST_F(TPriorHMMLadderWithHyperPriorTest, _updateKappa){
    // first construct kappa
    defKappa->setInitVal("0.2");
    priorKappa = std::make_unique<TPriorExponential<double>>(); // set some prior on kappa, such that prior does not cancel out in hastings ratio
    priorKappa->initialize("1");
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    kappa = std::make_shared<TMCMCParameter<double>>(std::move(priorKappa), defKappa, &randomGenerator);
    params.push_back(kappa);
    params.push_back(numStates);

    // now initialize bridgeTPriorWithHyperPrior
    auto bridgeTPriorWithHyperPrior = std::make_shared<BridgeTPriorHMMLadderWithHyperPrior>();
    bridgeTPriorWithHyperPrior->setNumStates(3);
    bridgeTPriorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    bridgeTPriorWithHyperPrior->initDistances(distances);
    bridgeTPriorWithHyperPrior->initParameter(param);
    bridgeTPriorWithHyperPrior->initializeStorageOfPriorParameters();

    // change value of kappa, such that oldValue and newValue are not the same
    bridgeTPriorWithHyperPrior->setKappaTo(0.3);
    bridgeTPriorWithHyperPrior->_updateTransitionMatrices();

    // calculate LL
    EXPECT_NEAR(bridgeTPriorWithHyperPrior->_calcLLUpdateGeneratingMatrixParameter(), -20.02215 + 19.89357, 0.01);

    // calculate logH
    std::shared_ptr<TMCMCParameterBase> param = kappa;
    EXPECT_NEAR(bridgeTPriorWithHyperPrior->_calcLogHUpdateGeneratingMatrixParameter(param), -20.02215 + 19.89357 - 0.1, 0.01);
}

TEST_F(TPriorHMMLadderWithHyperPriorTest, getSumLogPriorDensity){
    // first construct kappa
    defKappa->setInitVal("0.2");
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    kappa = std::make_shared<TMCMCParameter<double>>(std::move(priorKappa), defKappa, &randomGenerator);
    params.push_back(kappa);
    params.push_back(numStates);

    // now initialize priorWithHyperPrior
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    priorWithHyperPrior->initDistances(distances);
    priorWithHyperPrior->initializeStorageOfPriorParameters();

    double sum = priorWithHyperPrior->getSumLogPriorDensity(param);
    EXPECT_NEAR(sum, -21.32552, 0.01);
}

TEST_F(TPriorHMMLadderWithHyperPriorTest, getLogPriorDensityFull){
    // first construct kappa
    defKappa->setInitVal("0.2");
    priorKappa = std::make_unique<TPriorExponential<double>>(); // set some prior on kappa, such that prior does not cancel out in hastings ratio
    priorKappa->initialize("1");
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    kappa = std::make_shared<TMCMCParameter<double>>(std::move(priorKappa), defKappa, &randomGenerator);
    params.push_back(kappa);
    params.push_back(numStates);

    // now initialize priorWithHyperPrior
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    priorWithHyperPrior->initDistances(distances);
    priorWithHyperPrior->initializeStorageOfPriorParameters();

    double sum = priorWithHyperPrior->getLogPriorDensityFull(param);
    EXPECT_NEAR(sum, -21.32552 -0.2 , 0.01);
}

//--------------------------------------------
// TPriorMultivariateNormalWithHyperPrior
class BridgeTPriorMultivariateNormalWithHyperPrior : public TPriorMultivariateNormalWithHyperPrior<double> {
public:
    BridgeTPriorMultivariateNormalWithHyperPrior() : TPriorMultivariateNormalWithHyperPrior() {};
    double _numberOfElementsInTriangularMatrix_Diagonal0(size_t dimensions){
        return TPriorMultivariateNormalWithHyperPrior::_numberOfElementsInTriangularMatrix_Diagonal0(dimensions);
    }
    double _calc_Dimensions_from_numberOfElementsInTriangularMatrix_Diagonal0(size_t totalSize){
        return TPriorMultivariateNormalWithHyperPrior::_calc_Dimensions_from_numberOfElementsInTriangularMatrix_Diagonal0(totalSize);
    }
    size_t _linearizeIndex_Mrs(size_t r, size_t s) const{
        return TPriorMultivariateNormalWithHyperPrior::_linearizeIndex_Mrs(r, s);
    }
    template<typename C> double _calcDoubleSum(const std::shared_ptr<C> & paramOrStorage, TRange row, const std::shared_ptr<TMCMCParameterBase> & mus, const std::shared_ptr<TMCMCParameterBase> & Mrr, const std::shared_ptr<TMCMCParameterBase> & Mrs){
        return TPriorMultivariateNormalWithHyperPrior::_calcDoubleSum(paramOrStorage, row, mus, Mrr, Mrs);
    }
    double _calcDoubleSum_OldParam(const std::shared_ptr<TMCMCParameterBase> & param, TRange row, size_t indexInRowThatChanged, const std::shared_ptr<TMCMCParameterBase> & mus, const std::shared_ptr<TMCMCParameterBase> & Mrr, const std::shared_ptr<TMCMCParameterBase> & Mrs){
        return TPriorMultivariateNormalWithHyperPrior::_calcDoubleSum_OldParam(param, row, indexInRowThatChanged, mus, Mrr, Mrs);
    }
    double _calcSumLogMrr(const std::shared_ptr<TMCMCParameterBase> & Mrr){
        return TPriorMultivariateNormalWithHyperPrior::_calcSumLogMrr(Mrr);
    }

    // update mus
    virtual double _calcLLUpdateMu(size_t r){
        return TPriorMultivariateNormalWithHyperPrior::_calcLLUpdateMu(r);
    }
    double _calcLogHUpdateMu(size_t r){
        return TPriorMultivariateNormalWithHyperPrior::_calcLogHUpdateMu(r);
    }

    // update m
    virtual double _calcLLUpdateM(){
        return TPriorMultivariateNormalWithHyperPrior::_calcLLUpdateM();
    }
    double _calcLogHUpdateM(){
        return TPriorMultivariateNormalWithHyperPrior::_calcLogHUpdateM();
    }

    // update mrr
    double _calcLLUpdateMrr(size_t r){
        return TPriorMultivariateNormalWithHyperPrior::_calcLLUpdateMrr(r);
    }
    double _calcLogHUpdateMrr(size_t r){
        return TPriorMultivariateNormalWithHyperPrior::_calcLogHUpdateMrr(r);
    }

    // update mrs
    double _calcLLUpdateMrs(size_t r, size_t s){
        return TPriorMultivariateNormalWithHyperPrior::_calcLLUpdateMrs(r, s);
    }
    double _calcLogHUpdateMrs(size_t r, size_t s){
        return TPriorMultivariateNormalWithHyperPrior::_calcLogHUpdateMrs(r, s);
    }
};

class TTPriorMultivariateNormalWithHyperPriorTest : public Test {
protected:
    std::shared_ptr<BridgeTPriorMultivariateNormalWithHyperPrior> priorWithHyperPrior;

    std::shared_ptr<TParameterDefinition> defMu;
    std::shared_ptr<TParameterDefinition> defM;
    std::shared_ptr<TParameterDefinition> defMrr;
    std::shared_ptr<TParameterDefinition> defMrs;
    std::unique_ptr<TPriorWithHyperPrior<double>> priorMu;
    std::unique_ptr<TPrior<double>> priorM;
    std::unique_ptr<TPrior<double>> priorMrr;
    std::unique_ptr<TPriorWithHyperPrior<double>> priorMrs;
    std::shared_ptr<TMCMCParameter<double>> mu;
    std::shared_ptr<TMCMCParameter<double>> m;
    std::shared_ptr<TMCMCParameter<double>> mrr;
    std::shared_ptr<TMCMCParameter<double>> mrs;

    std::shared_ptr<BridgeTMCMCArrayParameter<double>> param; // parameter on which prior is defined
    std::shared_ptr<TParameterDefinition> def;

    TRandomGenerator randomGenerator;
    TLog logfile;
    std::vector<std::shared_ptr<TObservationsBase>> observations = {};

    void SetUp() override {
        defMu = std::make_shared<TParameterDefinition>("defMu");
        defM = std::make_shared<TParameterDefinition>("defM");
        defMrr = std::make_shared<TParameterDefinition>("defMrr");
        defMrs = std::make_shared<TParameterDefinition>("defMrs");

        // initialize param
        priorWithHyperPrior = std::make_shared<BridgeTPriorMultivariateNormalWithHyperPrior>();
        def = std::make_shared<TParameterDefinition>("defParam");
    }

    void setInitialValues_5D(bool setPriorVals = true, bool setParamVals = true){
        // values were generated in R with rnorm() in script multivariate_normal.R
        // mu
        if (setPriorVals) {
            defMu->setInitVal(
                    "-0.626453810742332,0.183643324222082,-0.835628612410047,1.59528080213779,0.329507771815361");
            // m
            defM->setInitVal("0.2");
            // mrr
            defMrr->setInitVal(
                    "0.820468384118015,0.487429052428485,0.738324705129217,0.575781351653492,0.305388387156356");
            // mrs
            defMrs->setInitVal(
                    "1.51178116845085,0.389843236411431,-0.621240580541804,-2.2146998871775,1.12493091814311,"
                    "-0.0449336090152309,-0.0161902630989461,0.943836210685299,0.821221195098089,0.593901321217509");
        }
        // parameter itself
        if (setParamVals) {
            def->setInitVal("0.918977371608218,1.35867955152904,-0.164523596253587,0.398105880367068,2.40161776050478,"
                            "0.782136300731067,-0.102787727342996,-0.253361680136508,-0.612026393250771,-0.0392400027331692,"
                            "0.0745649833651906,0.387671611559369,0.696963375404737,0.341119691424425,0.689739362450777,"
                            "-1.98935169586337,-0.0538050405829051,0.556663198673657,-1.12936309608079,0.0280021587806661,"
                            "0.61982574789471,-1.37705955682861,-0.68875569454952,1.43302370170104,-0.743273208882405,"
                            "-0.0561287395290008,-0.41499456329968,-0.70749515696212,1.98039989850586,0.188792299514343,"
                            "-0.155795506705329,-0.394289953710349,0.36458196213683,-0.367221476466509,-1.80495862889104,"
                            "-1.47075238389927,-0.0593133967111857,0.768532924515416,-1.04413462631653,1.46555486156289,"
                            "-0.47815005510862,1.10002537198388,-0.112346212150228,0.569719627442413,0.153253338211898,"
                            "0.417941560199702,0.763175748457544,0.881107726454215,-0.135054603880824,2.17261167036215");
        }
    }

    void setInitialValues_1D(bool setPriorVals = true){
        // values were generated in R with rnorm() in script multivariate_normal.R
        // mu
        if (setPriorVals) {
            defMu->setInitVal("-0.626453810742332");
            // m
            defM->setInitVal("0.2");
            // mrr
            defMrr->setInitVal("0.820468384118015"); // should be changed at initialization to 1
        }
        // parameter itself
        def->setInitVal("0.918977371608218,1.35867955152904,-0.164523596253587,0.398105880367068,2.40161776050478,"
                        "0.782136300731067,-0.102787727342996,-0.253361680136508,-0.612026393250771,-0.0392400027331692");
    }

    void setPriors(size_t numDim = 5){
        // this looks horrible, as I dont' want to use MCMCManager to construct the parameters
        // as a real-life user, it won't be necessary to define all these parameters!

        std::vector<std::shared_ptr<TMCMCParameterBase> > params;
        // mu
        auto priorMuOnMu = std::make_unique<TPriorUniform<double>>();
        auto defMuOnMu = std::make_shared<TParameterDefinition>("defMuOnMu");
        defMuOnMu->setInitVal("0"); defMuOnMu->update(false);
        auto muOnMu = std::make_shared<TMCMCParameter<double>>(std::move(priorMuOnMu), defMuOnMu, &randomGenerator);

        auto priorSdOnMu = std::make_unique<TPriorUniform<double>>();
        auto defSdOnMu = std::make_shared<TParameterDefinition>("defSdOnMu");
        defSdOnMu->setInitVal("1"); defSdOnMu->update(false);
        auto sdOnMu = std::make_shared<TMCMCParameter<double>>(std::move(priorSdOnMu), defSdOnMu, &randomGenerator);
        params = {muOnMu, sdOnMu};
        priorMu = std::make_unique<TPriorNormalWithHyperPrior<double>>();
        priorMu->initPriorWithHyperPrior(params, observations);

        // m
        priorM = std::make_unique<TPriorExponential<double>>();
        priorM->initialize("5");

        // mrr
        priorMrr = std::make_unique<TPriorMultivariateChisq<double>>();
        if (numDim == 5)
            priorMrr->initialize("5,4,3,2,1");
        else priorMrr->initialize("1");

        // mrs
        auto priorMuOnMrs = std::make_unique<TPriorUniform<double>>();
        auto defMuOnMrs = std::make_shared<TParameterDefinition>("defMuOnMrs");
        defMuOnMrs->setInitVal("0"); defMuOnMrs->update(false);
        auto muOnMrs = std::make_shared<TMCMCParameter<double>>(std::move(priorMuOnMrs), defMuOnMrs, &randomGenerator);

        auto priorSdOnMrs = std::make_unique<TPriorUniform<double>>();
        auto defSdOnMrs = std::make_shared<TParameterDefinition>("defSdOnMrs");
        defSdOnMrs->setInitVal("1"); defSdOnMrs->update(false);
        auto sdOnMrs = std::make_shared<TMCMCParameter<double>>(std::move(priorSdOnMrs), defSdOnMrs, &randomGenerator);
        params.clear();
        params = {muOnMrs, sdOnMrs};
        priorMrs = std::make_unique<TPriorNormalWithHyperPrior<double>>();
        priorMrs->initPriorWithHyperPrior(params, observations);
    }

    void initializeDefinitions(std::vector<std::shared_ptr<TMCMCParameterBase> > & params, size_t numDim = 5){
        // mu
        defMu->setDimensions({numDim});
        mu = std::make_shared<TMCMCParameter<double>>(std::move(priorMu), defMu, &randomGenerator);
        // m
        m = std::make_shared<TMCMCParameter<double>>(std::move(priorM), defM, &randomGenerator);
        // mrr
        defMrr->setDimensions({numDim});
        mrr = std::make_shared<TMCMCParameter<double>>(std::move(priorMrr), defMrr, &randomGenerator);
        // mrs
        defMrs->setDimensions({static_cast<size_t>((numDim - 1.) * numDim / 2.)});
        mrs = std::make_shared<TMCMCParameter<double>>(std::move(priorMrs), defMrs, &randomGenerator);
        params = {mu, m, mrr, mrs};

        // parameter itself
        def->setDimensions({10, numDim});
        param = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def, &randomGenerator);
        std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
        param->initializeStorageBasedOnPrior({10, numDim}, {dimNames, dimNames});
    }

    void TearDown() override {}
};

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, _numberOfElementsInTriangularMatrix_Diagonal0){
    size_t dimensions = 1;
    EXPECT_EQ(priorWithHyperPrior->_numberOfElementsInTriangularMatrix_Diagonal0(dimensions), 0);
    dimensions = 2;
    EXPECT_EQ(priorWithHyperPrior->_numberOfElementsInTriangularMatrix_Diagonal0(dimensions), 1);
    dimensions = 3;
    EXPECT_EQ(priorWithHyperPrior->_numberOfElementsInTriangularMatrix_Diagonal0(dimensions), 3);
    dimensions = 4;
    EXPECT_EQ(priorWithHyperPrior->_numberOfElementsInTriangularMatrix_Diagonal0(dimensions), 6);
    dimensions = 5;
    EXPECT_EQ(priorWithHyperPrior->_numberOfElementsInTriangularMatrix_Diagonal0(dimensions), 10);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, _calc_Dimensions_from_numberOfElementsInTriangularMatrix_Diagonal0){
    size_t totalSize = 0;
    EXPECT_EQ(priorWithHyperPrior->_calc_Dimensions_from_numberOfElementsInTriangularMatrix_Diagonal0(totalSize), 1);
    totalSize = 1;
    EXPECT_EQ(priorWithHyperPrior->_calc_Dimensions_from_numberOfElementsInTriangularMatrix_Diagonal0(totalSize), 2);
    totalSize = 3;
    EXPECT_EQ(priorWithHyperPrior->_calc_Dimensions_from_numberOfElementsInTriangularMatrix_Diagonal0(totalSize), 3);
    totalSize = 6;
    EXPECT_EQ(priorWithHyperPrior->_calc_Dimensions_from_numberOfElementsInTriangularMatrix_Diagonal0(totalSize), 4);
    totalSize = 10;
    EXPECT_EQ(priorWithHyperPrior->_calc_Dimensions_from_numberOfElementsInTriangularMatrix_Diagonal0(totalSize), 5);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, _linearizeIndex_Mrs){
    // example for a 5 dimensions
    EXPECT_EQ(priorWithHyperPrior->_linearizeIndex_Mrs(1, 0), 0);
    EXPECT_EQ(priorWithHyperPrior->_linearizeIndex_Mrs(2, 0), 1);
    EXPECT_EQ(priorWithHyperPrior->_linearizeIndex_Mrs(2, 1), 2);
    EXPECT_EQ(priorWithHyperPrior->_linearizeIndex_Mrs(3, 0), 3);
    EXPECT_EQ(priorWithHyperPrior->_linearizeIndex_Mrs(3, 1), 4);
    EXPECT_EQ(priorWithHyperPrior->_linearizeIndex_Mrs(3, 2), 5);
    EXPECT_EQ(priorWithHyperPrior->_linearizeIndex_Mrs(4, 0), 6);
    EXPECT_EQ(priorWithHyperPrior->_linearizeIndex_Mrs(4, 1), 7);
    EXPECT_EQ(priorWithHyperPrior->_linearizeIndex_Mrs(4, 2), 8);
    EXPECT_EQ(priorWithHyperPrior->_linearizeIndex_Mrs(4, 3), 9);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, initParameter_wrongDimensions){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);

    // this is ok, dimensions match!
    param->handPointerToPrior(param);

    // this should throw, wrong number of columns
    auto otherDef = std::make_shared<TParameterDefinition>("otherDefParam");
    otherDef->setDimensions({10, 9});
    std::shared_ptr<BridgeTMCMCArrayParameter<double>> otherParam;
    otherParam = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, otherDef, &randomGenerator);
    otherParam->handPointerToPrior(otherParam);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    otherParam->initializeStorageBasedOnPrior({10, 9}, {dimNames, dimNames});
    EXPECT_THROW(otherParam->initializeStorage(), std::runtime_error);

    // this should also throw, rows - cols switched
    auto otherDef2 = std::make_shared<TParameterDefinition>("otherDef2Param");
    otherDef2->setDimensions({5, 10});
    std::shared_ptr<BridgeTMCMCArrayParameter<double>> otherParam2;
    otherParam2 = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, otherDef2, &randomGenerator);
    otherParam2->initializeStorageBasedOnPrior({5, 10}, {dimNames, dimNames});
    otherParam2->handPointerToPrior(otherParam2);
    EXPECT_THROW(otherParam2->initializeStorage(), std::runtime_error);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, initPriorWithHyperPrior_wrongNumParams){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);
    EXPECT_NO_THROW(priorWithHyperPrior->initPriorWithHyperPrior(params, observations));

    // add one more
    params.push_back(m);

    // now initialize priorWithHyperPrior - throws because 5 instead of 4 parameters were given
    EXPECT_THROW(priorWithHyperPrior->initPriorWithHyperPrior(params, observations), std::runtime_error);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_mu){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace mu by another mu with wrong dimensions
    defMu->setDimensions({2, 5});
    mu = std::make_shared<TMCMCParameter<double>>(std::move(priorMu), defMu, &randomGenerator);
    params[0] = mu;

    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    // now initialize priorWithHyperPrior - throws because mu is 2-dimensional
    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_m){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace mu by another mu with wrong dimensions
    defM->setDimensions({2, 1});
    m = std::make_shared<TMCMCParameter<double>>(std::move(priorM), defM, &randomGenerator);
    params[1] = m;

    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    // now initialize priorWithHyperPrior - throws because m is 2-dimensional/has wrong size
    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_mrr){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace mu by another mu with wrong dimensions
    defMrr->setDimensions({2, 5});
    mrr = std::make_shared<TMCMCParameter<double>>(std::move(priorMrr), defMrr, &randomGenerator);
    params[2] = mrr;

    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    // now initialize priorWithHyperPrior - throws because mrr is 2-dimensional/has wrong size
    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, initPriorWithHyperPrior_wrongDimensions_mrs){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace mu by another mu with wrong dimensions
    defMrs->setDimensions({2, 5});
    mrs = std::make_shared<TMCMCParameter<double>>(std::move(priorMrs), defMrs, &randomGenerator);
    params[3] = mrs;

    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    // now initialize priorWithHyperPrior - throws because mrr is 2-dimensional/has wrong size
    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, initPriorWithHyperPrior_wrongSize_mu_vs_mrr){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace mu by another mu with wrong dimensions
    defMrr->setDimensions({6});
    mrr = std::make_shared<TMCMCParameter<double>>(std::move(priorMrr), defMrr, &randomGenerator);
    params[2] = mrr;

    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    // now initialize priorWithHyperPrior - throws because dimensions of mu and mrr don't match
    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, initPriorWithHyperPrior_wrongSize_mrr_vs_mrs){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    initializeDefinitions(params);

    // replace mu by another mu with wrong dimensions
    defMrs->setDimensions({11});
    mrs = std::make_shared<TMCMCParameter<double>>(std::move(priorMrs), defMrs, &randomGenerator);
    params[3] = mrs;

    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    // now initialize priorWithHyperPrior - throws because dimensions of mu and mrr don't match
    EXPECT_THROW(param->initializeStorage(), std::runtime_error);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, _calcSumLogMrr){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcSumLogMrr(mrr), -2.95806);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, _calcSumLogMrr_1D){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_1D();
    initializeDefinitions(params, 1);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcSumLogMrr(mrr), 0);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, _calcDoubleSum){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    TRange row = param->get1DSlice(1, {0,0});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcDoubleSum(param, row, mu, mrr, mrs), 41.43701);

    row = param->get1DSlice(1, {4,0});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcDoubleSum(param, row, mu, mrr, mrs), 6.232287);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, _calcDoubleSum_1D){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_1D();
    initializeDefinitions(params, 1);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    TRange row = param->get1DSlice(1, {0,0});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcDoubleSum(param, row, mu, mrr, mrs), 2.388358);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, _calcDoubleSum_OldParam){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // change 3rd element in 2nd row of param
    size_t index = param->getIndex({1, 2});
    param->set(index, 0.8);

    TRange row = param->get1DSlice(1, {1,0});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcDoubleSum_OldParam(param, row, 2, mu, mrr, mrs), 47.54327);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, _calcDoubleSum_OldParam_1D){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_1D();
    initializeDefinitions(params, 1);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // change 3rd element of param
    size_t index = param->getIndex({2, 0});
    param->set(index, 0.8);

    TRange row = param->get1DSlice(1, {0,0});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcDoubleSum_OldParam(param, row, 2, mu, mrr, mrs), 2.388358);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, _getLogPriorDensity_vec){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // we want prior density of first element of first row
    size_t linearIndex = param->getIndex({0, 0});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(param, linearIndex), -517.4682);

    // we want prior density of third element of 6th row
    linearIndex = param->getIndex({5, 2});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(param, linearIndex), -18.99946);

    // we want prior density of last element of last row
    linearIndex = param->getIndex({9, 4});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(param, linearIndex), -599.2394);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, _getLogPriorDensity_vec_1D){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_1D();
    initializeDefinitions(params, 1);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // we want prior density of first element
    size_t linearIndex = param->getIndex({0, 0});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(param, linearIndex), -29.16397);

    // we want prior density of third element
    linearIndex = param->getIndex({2, 0});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(param, linearIndex), -1.976745);

    // we want prior density of last element
    linearIndex = param->getIndex({9, 0});
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensity(param, linearIndex), -3.619751);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, _getLogPriorDensityOld_vec){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // we want prior density of first element of first row
    size_t linearIndex = param->getIndex({0, 0});
    double val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.8);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityOld(param, linearIndex), -517.4682);

    // reset
    param->set(linearIndex, val);

    // we want prior density of third element of 6th row
    linearIndex = param->getIndex({5, 2});
    val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.5);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityOld(param, linearIndex), -18.99946);

    // reset
    param->set(linearIndex, val);

    // we want prior density of last element of last row
    linearIndex = param->getIndex({9, 4});
    param->set(linearIndex, -0.5274);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityOld(param, linearIndex), -599.2394);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, _getLogPriorDensityOld_vec_1D){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_1D();
    initializeDefinitions(params, 1);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // we want prior density of first element
    size_t linearIndex = param->getIndex({0, 0});
    double val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.8);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityOld(param, linearIndex), -29.16397);

    // reset
    param->set(linearIndex, val);

    // we want prior density of third element
    linearIndex = param->getIndex({2, 0});
    val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.5);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityOld(param, linearIndex), -1.976745);

    // reset
    param->set(linearIndex, val);

    // we want prior density of last element
    linearIndex = param->getIndex({9, 0});
    param->set(linearIndex, -0.5274);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityOld(param, linearIndex), -3.619751);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, _getLogPriorRatio_vec){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // we want prior ratio of first element of first row
    size_t linearIndex = param->getIndex({0, 0});
    double val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.8);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, linearIndex), 14.33759);

    // reset
    param->set(linearIndex, val);

    // we want prior ratio of third element of 6th row
    linearIndex = param->getIndex({5, 2});
    val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.5);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, linearIndex), -5.650851);

    // reset
    param->set(linearIndex, val);

    // we want prior ratio of last element of last row
    linearIndex = param->getIndex({9, 4});
    param->set(linearIndex, -0.5274);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, linearIndex), -79.19169);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, _getLogPriorRatio_vec_1D){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_1D();
    initializeDefinitions(params, 1);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // we want prior ratio of first element
    size_t linearIndex = param->getIndex({0, 0});
    double val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.8);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, linearIndex), 4.419838);

    // reset
    param->set(linearIndex, val);

    // we want prior ratio of third element
    linearIndex = param->getIndex({2, 0});
    val = param->value<double>(linearIndex);
    param->set(linearIndex, 0.5);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, linearIndex), -13.19398);

    // reset
    param->set(linearIndex, val);

    // we want prior ratio of last element
    linearIndex = param->getIndex({9, 0});
    param->set(linearIndex, -0.5274);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorRatio(param, linearIndex), 4.187605);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, _calcLLUpdateMrr){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    mrr->initializeStorage();

    // change r=0 of Mrr
    size_t r = 0;
    double val = mrr->value<double>(r);
    mrr->set(r, 0.62);

    // calculate LL ratio
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMrr(r), 72.85032);
    // calculate logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMrr(r), 72.85032 - 0.3199997);

    // reset previous change
    mrr->set(r, val);

    // change r=2 of Mrr
    r = 2;
    val = mrr->value<double>(r);
    mrr->set(r, 0.9467);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMrr(r), -71.71539);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMrr(r), -71.71539 + 0.02011163);

    // reset previous change
    mrr->set(r, val);

    // change r=4 of Mrr
    r = 4;
    val = mrr->value<double>(r);
    mrr->set(r, 0.01);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMrr(r), -16.61078);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMrr(r), -16.61078 + 1.857194);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, _calcLLUpdateMrs){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    mrs->initializeStorage();

    // change r=1, s=0 of Mrs
    size_t r = 1;
    size_t s = 0;
    size_t linearIndex = priorWithHyperPrior->_linearizeIndex_Mrs(r, s);
    double val = mrs->value<double>(linearIndex);
    mrs->set(linearIndex, 0.62);

    // calculate LL ratio
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMrs(r, s), 177.0145);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMrs(r, s), 177.0145 + 0.9505412);

    // reset previous change
    mrs->set(linearIndex, val);

    // change r=2, s=1 of Mrs
    r = 2; s = 1;
    linearIndex = priorWithHyperPrior->_linearizeIndex_Mrs(r, s);
    val = mrs->value<double>(linearIndex);
    mrs->set(linearIndex, 0.9467);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMrs(r, s), 573.3473);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMrs(r, s), 573.3473 - 0.2551505);

    // reset previous change
    mrs->set(linearIndex, val);

    // change r=4, s=3 of Mrs
    r = 4; s = 3;
    linearIndex = priorWithHyperPrior->_linearizeIndex_Mrs(r, s);
    val = mrs->value<double>(linearIndex);
    mrs->set(linearIndex, 0.01);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMrs(r, s), 41.36284);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMrs(r, s), 41.36284 + 0.1763094);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, _calcLLUpdateMu){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    mu->initializeStorage();

    // change r=0 of mu
    size_t r = 0;
    double val = mu->value<double>(r);
    mu->set(r, -0.62);

    // calculate LL ratio and logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMu(r), 5.160112);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMu(r), 5.160112 + 0.004022188);

    // reset previous change
    mu->set(r, val);

    // change r=2 of mu
    r = 2;
    val = mu->value<double>(r);
    mu->set(r, 0.9467);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMu(r),  1132.956);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMu(r), 1132.956 - 0.09898286);

    // reset previous change
    mu->set(r, val);

    // change r=4 of mu
    r = 4;
    val = mu->value<double>(r);
    mu->set(r, 0.1);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMu(r), 90.74116);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMu(r), 90.74116 + 0.04928769);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, _calcLLUpdateMu_1D){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_1D();
    setPriors();
    initializeDefinitions(params, 1);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    mu->initializeStorage();

    // change r=0 of mu
    size_t r = 0;
    mu->set(r, -0.62);

    // calculate LL ratio and logH
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateMu(r), 1.761866);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateMu(r), 1.761866 + 0.004022188);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, _calcLLUpdateM){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    m->initializeStorage();

    // change m
    m->set(0.62);

    // calculate LL ratio
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateM(), 3683.704);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateM(), 3683.704 - 2.1);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, _calcLLUpdateM_1D){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_1D();
    setPriors();
    initializeDefinitions(params, 1);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    m->initializeStorage();

    // change m
    m->set(0.62);

    // calculate LL ratio
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLLUpdateM(), 207.1167);
    EXPECT_FLOAT_EQ(priorWithHyperPrior->_calcLogHUpdateM(), 207.1167 - 2.1);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, getLogPriorDensityFull){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D();
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    mu->initializeStorage();
    m->initializeStorage();
    mrr->initializeStorage();
    mrs->initializeStorage();

    // calculate sum of all log prior densities
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityFull(param), -4198.234);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, getLogPriorDensityFull_1D){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_1D();
    setPriors();
    initializeDefinitions(params, 1);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    mu->initializeStorage();
    m->initializeStorage();
    mrr->initializeStorage();
    mrs->initializeStorage();

    // calculate sum of all log prior densities
    EXPECT_FLOAT_EQ(priorWithHyperPrior->getLogPriorDensityFull(param), -237.4009);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, estimateInitialPriorParameters_1param_1D){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_1D(false);
    setPriors(1);
    initializeDefinitions(params, 1);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    mu->initializeStorage();
    m->initializeStorage();
    mrr->initializeStorage();
    mrs->initializeStorage();

    priorWithHyperPrior->estimateInitialPriorParameters(&logfile);

    // check mus
    EXPECT_FLOAT_EQ(mu->value<double>(0), 0.4687577);

    // check mrr
    EXPECT_FLOAT_EQ(mrr->value<double>(), 1.);

    // check m
    EXPECT_FLOAT_EQ(m->value<double>(), 0.8665521445);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, estimateInitialPriorParameters_1param_5D){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D(false);
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    mu->initializeStorage();
    m->initializeStorage();
    mrr->initializeStorage();
    mrs->initializeStorage();

    priorWithHyperPrior->estimateInitialPriorParameters(&logfile);

    // check mus
    std::vector<double> valuesMu = {-0.1336732,  0.1207302,  0.1341367,  0.1434569,  0.4512100};
    for (size_t d = 0; d < 5; d++){
        EXPECT_FLOAT_EQ(mu->value<double>(d), valuesMu[d]);
    }

    // check mrr
    std::vector<double> valuesMrr = {1.3583827, 1.8108263, 2.8103980, 1.0447555, 0.8179298};
    for (size_t d = 0; d < 5; d++){
        EXPECT_FLOAT_EQ(mrr->value<double>(d), valuesMrr[d]);
    }

    // check mrs
    std::vector<double> valuesMrs = {-0.250928795376855,0.773588645448706,-0.199745612798616,-0.36948072030297,0.0979577172098371,1.17071283517945,-0.15450849326371,-0.735381241210143,-0.326766798424084,0.0858513827738757};
    for (size_t d = 0; d < 10; d++){
        EXPECT_FLOAT_EQ(mrs->value<double>(d), valuesMrs[d]);
    }

    // check m
    EXPECT_FLOAT_EQ(m->value<double>(), 1.);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, estimateInitialPriorParameters_3params_5D){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D(false);
    setPriors();
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();
    mu->initializeStorage();
    m->initializeStorage();
    mrr->initializeStorage();
    mrs->initializeStorage();

    // create a second parameter
    auto def2 = std::make_shared<TParameterDefinition>("defParam2");
    def2->setDimensions({20, 5});
    def2->setInitVal("0.475509528899663,-0.54252003099165,-0.635736453948977,0.0601604404345152,0.450187101272656,-0.709946430921815,1.20786780598317,-0.461644730360566,-0.588894486259664,-0.018559832714638,0.610726353489055,1.16040261569495,1.43228223854166,0.531496192632572,-0.318068374543844,-0.934097631644252,0.700213649514998,-0.650696353310367,-1.51839408178679,-0.929362147453702,-1.2536334002391,1.58683345454085,-0.207380743601965,0.306557860789766,-1.48746031014148,0.291446235517463,0.558486425565304,-0.392807929441984,-1.53644982353759,-1.07519229661568,-0.443291873218433,-1.27659220845804,-0.319992868548507,-0.300976126836611,1.00002880371391,0.00110535163162413,-0.573265414236886,-0.279113302976559,-0.528279904445006,-0.621266694796823,0.0743413241516641,-1.22461261489836,0.494188331267827,-0.652094780680999,-1.38442684738449,-0.589520946188072,-0.473400636439312,-0.177330482269606,-0.0568967778473925,1.86929062242358,-0.568668732818502,-0.620366677224124,-0.505957462114257,-1.91435942568001,0.425100377372448,-0.135178615123832,0.0421158731442352,1.34303882517041,1.17658331201856,-0.238647100913033,1.1780869965732,-0.910921648552446,-0.214579408546869,-1.664972436212,1.05848304870902,-1.52356680042976,0.158028772404075,-0.179556530043387,-0.463530401472386,0.886422651374936,0.593946187628422,-0.654584643918818,-0.100190741213562,-1.11592010504285,-0.619243048231147,0.332950371213518,1.76728726937265,0.712666307051405,-0.750819001193448,2.20610246454047,1.06309983727636,0.716707476017206,-0.0735644041263263,2.08716654562835,-0.255027030141015,-0.304183923634301,0.910174229495227,-0.0376341714670479,0.0173956196932517,-1.42449465021281,0.370018809916288,0.384185357826345,-0.681660478755657,-1.28630053043433,-0.144399601954219,0.267098790772231,1.68217608051942,-0.324270272246319,-1.64060553441858,0.207538339232345");
    auto param2 = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def2, &randomGenerator);; // 2nd parameter on which prior is defined
    param2->handPointerToPrior(param2);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param2->initializeStorageBasedOnPrior({20, 5}, {dimNames, dimNames});

    // create a third parameter
    auto def3 = std::make_shared<TParameterDefinition>("defParam3");
    def3->setDimensions({13, 5});
    def3->setInitVal("2.30797839905936,-1.46725002909224,1.44115770684428,0.510108422952926,-0.630300333928146,0.105802367893711,0.521022742648139,-1.01584746530465,-0.164375831769667,-0.340968579860405,0.456998805423414,-0.158754604716016,0.411974712317515,0.420694643254513,-1.15657236263585,-0.077152935356531,1.4645873119698,-0.38107605110892,-0.400246743977644,1.80314190791747,-0.334000842366544,-0.766081999604665,0.409401839650934,-1.37020787754746,-0.331132036391221,-0.0347260283112762,-0.430211753928547,1.68887328620405,0.987838267454879,-1.60551341225308,0.787639605630162,-0.926109497377437,1.58658843344197,1.51974502549955,0.197193438739481,2.07524500865228,-0.17710396143654,-0.330907800682766,-0.308740569225614,0.263175646405474,1.02739243876377,0.402011779486338,-2.28523553529247,-1.25328975560769,-0.985826700409291,1.2079083983867,-0.731748173119606,2.49766158983416,0.642241305677824,-2.88892067167955,-1.23132342155804,0.830373167981674,0.667066166765493,-0.0447091368939791,-0.640481702565115,0.983895570053379,-1.20808278630446,0.5413273359637,-1.73321840682484,0.570507635920485,0.219924803660651,-1.04798441280774,-0.0133995231459087,0.00213185968026965,-0.05972327604261");
    auto param3 = std::make_shared<BridgeTMCMCArrayParameter<double>>(priorWithHyperPrior, def3, &randomGenerator);; // 3rd parameter on which prior is defined
    param3->handPointerToPrior(param3);
    param3->initializeStorageBasedOnPrior({13, 5}, {dimNames, dimNames});

    priorWithHyperPrior->estimateInitialPriorParameters(&logfile);

    // check mus
    std::vector<double> valuesMu = {0.11523468,  0.04907407,  0.12323281, -0.22317660, -0.03968175};
    for (size_t d = 0; d < 5; d++){
        EXPECT_FLOAT_EQ(mu->value<double>(d), valuesMu[d]);
    }

    // check mrr
    std::vector<double> valuesMrr = {1.1911603, 1.1422352, 1.2554085, 1.0098860, 0.8781874};
    for (size_t d = 0; d < 5; d++){
        EXPECT_FLOAT_EQ(mrr->value<double>(d), valuesMrr[d]);
    }

    // check mrs
    std::vector<double> valuesMrs = {0.298477439029773,0.0161442097269054,0.189428554581099,-0.191076324114716,-0.0366835662693865,-0.335797691798708,-0.0166892047239951,-0.178660559742825,0.0978876330684502,0.119086121705966};
    for (size_t d = 0; d < 10; d++){
        EXPECT_FLOAT_EQ(mrs->value<double>(d), valuesMrs[d]);
    }

    // check m
    EXPECT_FLOAT_EQ(m->value<double>(), 1.);
}

TEST_F(TTPriorMultivariateNormalWithHyperPriorTest, simulate){
    // first construct hyperpriors
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    setInitialValues_5D(true, false);
    initializeDefinitions(params);
    priorWithHyperPrior->initPriorWithHyperPrior(params, observations);
    param->handPointerToPrior(param);
    param->initializeStorage();

    // create mock random generator and define return values
    MockRandomGenerator mockRandomGenerator;
    EXPECT_CALL(mockRandomGenerator, getNormalRandom)
                .Times(10*5)
                // first row
                .WillOnce(Return(-1.))
                .WillOnce(Return(-0.5))
                .WillOnce(Return(0.))
                .WillOnce(Return(0.5))
                .WillOnce(Return(1.))
                // second row
                .WillOnce(Return(-2.))
                .WillOnce(Return(-1.5))
                .WillOnce(Return(-1.))
                .WillOnce(Return(-0.5))
                .WillOnce(Return(-0.))
                // all other rows
                .WillRepeatedly(Return(0.1));

    TParameters parameters;
    priorWithHyperPrior->simulateUnderPrior(&mockRandomGenerator, &logfile, parameters);

    // go over storage and check if this is expected
    std::vector<double> expected_Row1 = {-3.2326749, 0.6710788, -1.214147,  0.9489936, 0.8010098};
    std::vector<double> expected_Row2 = {-5.8388960, 0.7715120, -2.234973, -0.2288314, 1.4493030};
    std::vector<double> expected_RowAllRest = {-0.3658317, 0.1736000, -0.733546,  1.7130633, 0.2646785};

    // first row
    size_t index = 0;
    for (size_t d = 0; d < 5; d++, index++) {
        EXPECT_FLOAT_EQ(param->value<double>(index), expected_Row1[d]);
    }

    // second row
    for (size_t d = 0; d < 5; d++, index++) {
        EXPECT_FLOAT_EQ(param->value<double>(index), expected_Row2[d]);
    }

    // all other rows
    for (size_t n = 2; n < 10; n++){
        for (size_t d = 0; d < 5; d++, index++) {
            EXPECT_FLOAT_EQ(param->value<double>(index), expected_RowAllRest[d]);
        }
    }
}
