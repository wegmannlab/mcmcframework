//
// Created by madleina on 01.07.20.
//

#include "../TestCase.h"
#include "../../core/TMCMCParameter.h"
using namespace testing;


//-------------------------------------------
// TMCMCParameterArray
//-------------------------------------------
class MockTMCMCParameterArray : public TMCMCParameter<double> {
public:
    MockTMCMCParameterArray(std::shared_ptr<TPrior<double>> Prior, std::shared_ptr<TParameterDefinition> & def, TRandomGenerator* RandomGenerator)
            : TMCMCParameter(std::move(Prior), def, RandomGenerator) {};
    MOCK_METHOD(void, _propose, (const size_t & i), (override));
};

template<class T>
class BridgeTMCMCParameterArray : public TMCMCParameter<T> {
    // provides access to private/protected functions of TMCMCParameterArray
public:
    BridgeTMCMCParameterArray(std::shared_ptr<TPrior<T>> Prior, const std::shared_ptr<TParameterDefinition> & def, TRandomGenerator* RandomGenerator)
            : TMCMCParameter<T>(std::move(Prior), def, RandomGenerator) {};
    void _readInitVals(const std::string& initVal, bool hasDefaultInitVal) {
        TMCMCParameter<T>::_readInitVals(initVal, hasDefaultInitVal);
    }
    void _readInitJumpSizesVals(bool oneJumpSizeForAll, const std::string& initJumpSize) {
        TMCMCParameter<T>::_readInitJumpSizesVals(oneJumpSizeForAll, initJumpSize);
    }
    void _readVals(const std::string& initVal, std::vector<T> & storage) {
        TMCMCParameter<T>::_readVals(initVal, storage);
    }
    void _readValsOnlyOneNumber(const std::string& initVal, std::vector<T> & storage) const {
        TMCMCParameter<T>::_readValsOnlyOneNumber(initVal, storage);
    }
    void _readValsFromVec(const std::string& initVal, std::vector<T> & storage) const {
        TMCMCParameter<T>::_readValsFromVec(initVal, storage);
    }
    void _readValsFromFile(const std::string& filename, std::vector<T> & storage) const {
        TMCMCParameter<T>::_readValsFromFile(filename, storage);
    }
    T _checkDefaultInitVal(T initVal, bool hasDefaultInitVal) {
        return TMCMCParameter<T>::_checkDefaultInitVal(initVal, hasDefaultInitVal);
    }
    void _initJumpSizeVal_shared(const std::string &initJumpSize) {
        TMCMCParameter<T>::_initJumpSizeVal_shared(initJumpSize);
    };
    void _initJumpSizeVal_unique(const std::string &initJumpSize) {
        TMCMCParameter<T>::_initJumpSizeVal_unique(initJumpSize);
    };
    void _propose(const size_t & i) override {
        TMCMCParameter<T>::_propose(i);
    }
    void _reject(const size_t& i) {
        TMCMCParameter<T>::_reject(i);
    }
    void _setPropKernel(T val)  {
        TMCMCParameter<T>::_setPropKernel(val);
    }
    void _setPropKernel(T val, size_t i) {
        TMCMCParameter<T>::_setPropKernel(val, i);
    }
    T _jumpSize() const{
        return TMCMCParameter<T>::_jumpSize();
    }
    T _jumpSize(const size_t i) const {
        return TMCMCParameter<T>::_jumpSize(i);
    }
    double _mean(const size_t & i){
        return TMCMCParameter<T>::_mean(i);
    }
    double _sd(const size_t & i){
        return TMCMCParameter<T>::_sd(i);
    }
    double _var(const size_t & i){
        return TMCMCParameter<T>::_var(i);
    }
   void _initBoundary(bool hasDefaultMin, bool hasDefaultMax, std::string min, std::string max, bool minIncluded, bool maxIncluded) {
        TMCMCParameter<T>::_initBoundary(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    }
    std::unique_ptr<TPropKernel<T>> _initPropKernel(ProposalKernel::MCMCProposalKernel propKernelDistr) {
        return TMCMCParameter<T>::_initPropKernel(propKernelDistr);
    }
};

class TMCMCParameterArrayTest : public Test {
protected:
    std::shared_ptr<TParameterDefinition> def;
    std::shared_ptr<TPrior<double>> prior;
    MockRandomGenerator randomGenerator;
    TLog logfile;

    void SetUp() override {
        def = std::make_shared<TParameterDefinition>("def");
    }
    void TearDown() override {}
};

TEST_F(TMCMCParameterArrayTest, initialize_2D){
    def->setInitVal("1,2,3,4,5,6");
    def->setJumpSizeForAll(false);
    def->setInitJumpSizeProposal("0.1, 0.2, 0.3, 0.4, 0.5, 0.6");
    def->setDimensions({2,3});
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({2,3}, {dimNames, dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    EXPECT_EQ(param.numDim(), 2);
    EXPECT_EQ(param.dimensions().at(0), 2);
    EXPECT_EQ(param.dimensions().at(1), 3);
    EXPECT_EQ(param.totalSize(), 3*2);
    double val = 1;
    double jumpSize = 0.1;

    TRange range = param.getFull();
    for (size_t i = range.first; i < range.last; i++){
        EXPECT_EQ(param.value<double>(i), val);
        val++;
        EXPECT_FLOAT_EQ(param._jumpSize(i), jumpSize);
        jumpSize += 0.1;
    }
}

TEST_F(TMCMCParameterArrayTest, _readValsOnlyOneNumber_1D){
    def->setDimensions({5});
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    std::vector<double> vec;
    param._readValsOnlyOneNumber("1.", vec);
    EXPECT_THAT(vec, ElementsAre(1., 1., 1., 1., 1.));
}

TEST_F(TMCMCParameterArrayTest, _readValsOnlyOneNumber_2D){
    def->setDimensions({2,3});
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({2,3}, {dimNames, dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    std::vector<double> vec;
    param._readValsOnlyOneNumber("1.", vec);
    EXPECT_THAT(vec, ElementsAre(1., 1., 1., 1., 1., 1.));
}

TEST_F(TMCMCParameterArrayTest, _readVals_bool_onlyOneBool_true_1D){
    def->setDimensions({5});
    def->setInitVal("true");
    std::unique_ptr<TPriorNormalWithHyperPrior<bool>> prior1;
    TMCMCParameter<bool> param(std::move(prior1), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    for (size_t i = 0; i < 5; i++)
        EXPECT_EQ(param.value<bool>(i), true);
}

TEST_F(TMCMCParameterArrayTest, _readVals_bool_onlyOneBool_false_1D){
    def->setDimensions({5});
    def->setInitVal("0");
    std::unique_ptr<TPriorNormalWithHyperPrior<bool>> prior1;
    TMCMCParameter<bool> param(std::move(prior1), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    for (size_t i = 0; i < 5; i++)
        EXPECT_EQ(param.value<bool>(i), false);
}

TEST_F(TMCMCParameterArrayTest, _readVals_bool_onlyOneBool_false_2D){
    def->setDimensions({2,3});
    def->setInitVal("0");
    std::unique_ptr<TPriorNormalWithHyperPrior<bool>> prior1;
    TMCMCParameter<bool> param(std::move(prior1), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({2,3}, {dimNames, dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    for (size_t i = 0; i < 6; i++)
        EXPECT_EQ(param.value<bool>(i), false);
}

TEST_F(TMCMCParameterArrayTest, _readValsFromVec_1D){
    def->setDimensions({5});
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    std::vector<double> vec;

    EXPECT_THROW(try{param._readValsFromVec("1.", vec);} catch(...){throw std::runtime_error("blah");}, std::runtime_error); // too few
    EXPECT_THROW(try{param._readValsFromVec("1.,1.,1.,1.,1.,1.,1.,1.", vec);} catch(...){throw std::runtime_error("blah");}, std::runtime_error); // too many

    param._readValsFromVec("1.,1.,1.,1.,1.", vec); // this works
    EXPECT_THAT(vec, ElementsAre(1., 1., 1., 1., 1.));
    vec.clear();
    param._readValsFromVec("1. ,1. ,1. ,1. ,1.", vec); // this works
    EXPECT_THAT(vec, ElementsAre(1., 1., 1., 1., 1.));
}

TEST_F(TMCMCParameterArrayTest, _readValsFromVec_2D){
    def->setDimensions({2,3});
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({2,3}, {dimNames, dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    std::vector<double> vec;

    EXPECT_THROW(try{param._readValsFromVec("1.", vec);} catch(...){throw std::runtime_error("blah");}, std::runtime_error); // too few
    EXPECT_THROW(try{param._readValsFromVec("1.,1.,1.,1.,1.,1.,1.,1.", vec);} catch(...){throw std::runtime_error("blah");}, std::runtime_error); // too many

    param._readValsFromVec("1.,1.,1.,1.,1.,1.", vec); // this works
    EXPECT_THAT(vec, ElementsAre(1., 1., 1., 1., 1., 1.));
    vec.clear();
    param._readValsFromVec("1. ,1. ,1. ,1. ,1., 1.", vec); // this works
    EXPECT_THAT(vec, ElementsAre(1., 1., 1., 1., 1., 1.));
}

TEST_F(TMCMCParameterArrayTest, _readValsFromFile_tooFewCols_1D){
    def->setDimensions({5});
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    // first write file
    TOutputFile file("vals.txt", 4);
    file << 1.1 << 2.4 << -3.5 << 4.9 << std::endl; // - one col
    file.close();

    // now read
    std::vector<double> vec;
    EXPECT_THROW({try {param._readValsFromFile("vals.txt", vec);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error); // too few cols

    // remove file
    remove("vals.txt");
}

TEST_F(TMCMCParameterArrayTest, _readValsFromFile_tooFewCols_2D){
    def->setDimensions({2,3});
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({2,3}, {dimNames, dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    // first write file
    TOutputFile file("vals.txt", 4);
    file << 1.1 << 2.4 << -3.5 << 4.9 << std::endl; // - two cols
    file.close();

    // now read
    std::vector<double> vec;
    EXPECT_THROW({try {param._readValsFromFile("vals.txt", vec);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error); // too few cols

    // remove file
    remove("vals.txt");
}

TEST_F(TMCMCParameterArrayTest, _readValsFromFile_tooManyCols_1D){
    def->setDimensions({5});
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    // write file
    TOutputFile file("vals.txt", 6);
    file << 1.1 << 2.4 << -3.5 << 4.98 << 5.189 << 6.4 << std::endl; // one col too much
    file.close();

    std::vector<double> vec;
    EXPECT_THROW({try {param._readValsFromFile("vals.txt", vec);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error); // too many cols

    // remove file
    remove("vals.txt");
}

TEST_F(TMCMCParameterArrayTest, _readValsFromFile_tooManyCols_2D){
    def->setDimensions({2,3});
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({2,3}, {dimNames, dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    // write file
    TOutputFile file("vals.txt", 7);
    file << 1.1 << 2.4 << -3.5 << 4.98 << 5.189 << 6.4 << 7.9 << std::endl; // one col too much
    file.close();

    std::vector<double> vec;
    EXPECT_THROW({try {param._readValsFromFile("vals.txt", vec);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error); // too many cols

    // remove file
    remove("vals.txt");
}

TEST_F(TMCMCParameterArrayTest, _readValsFromFile_tooManyLines_1D){
    def->setDimensions({5});
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    // write file
    TOutputFile file("vals.txt", 5);
    file << 1.1 << 2.4 << -3.5 << 4.98 << 5.189 << std::endl;
    file << 1.1 << 2.4 << -3.5 << 4.98 << 5.189 << std::endl; // one line too much
    file.close();

    std::vector<double> vec;
    EXPECT_THROW({try {param._readValsFromFile("vals.txt", vec);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error); // too many lines

    // remove file
    remove("vals.txt");
}

TEST_F(TMCMCParameterArrayTest, _readValsFromFile_tooManyLines){
    def->setDimensions({2,3});
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({2,3}, {dimNames, dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    // write file
    TOutputFile file("vals.txt", 1);
    file << 1.1 << std::endl;
    file << 2.4 << std::endl;
    file << -3.5 << std::endl;
    file << 4.98 << std::endl;
    file << 5.189 << std::endl;
    file << 6.4 << std::endl;
    file << 7.3 << std::endl; // one line too much
    file.close();

    std::vector<double> vec;
    EXPECT_THROW({try {param._readValsFromFile("vals.txt", vec);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error); // too many lines

    // remove file
    remove("vals.txt");
}

TEST_F(TMCMCParameterArrayTest, _readValsFromFile_ok_1D){
    def->setDimensions({5});
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    // write file
    TOutputFile file("vals.txt", 5);
    file << 1.1 << 2.4 << -3.5 << 4.98 << 5.189 << std::endl;
    file.close();

    std::vector<double> vec;
    param._readValsFromFile("vals.txt", vec);
    EXPECT_THAT(vec, ElementsAre(1.1, 2.4, -3.5, 4.98, 5.189));

    // remove file
    remove("vals.txt");
}

TEST_F(TMCMCParameterArrayTest, _readValsFromFile_ok_allInOneLine_2D){
    def->setDimensions({2,3});
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({2,3}, {dimNames, dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    // write file
    TOutputFile file("vals.txt", 6);
    file << 1.1 << 2.4 << -3.5 << 4.98 << 5.189 << 6.4 << std::endl;
    file.close();

    std::vector<double> vec;
    param._readValsFromFile("vals.txt", vec);
    EXPECT_THAT(vec, ElementsAre(1.1, 2.4, -3.5, 4.98, 5.189, 6.4));

    // remove file
    remove("vals.txt");
}

TEST_F(TMCMCParameterArrayTest, _readValsFromFile_ok_allInOneCol_2D){
    def->setDimensions({2,3});
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({2,3}, {dimNames, dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    // write file
    TOutputFile file("vals.txt", 1);
    file << 1.1 << std::endl;
    file << 2.4 << std::endl;
    file << -3.5 << std::endl;
    file << 4.98 << std::endl;
    file << 5.189 << std::endl;
    file << 6.4 << std::endl;
    file.close();

    std::vector<double> vec;
    param._readValsFromFile("vals.txt", vec);
    EXPECT_THAT(vec, ElementsAre(1.1, 2.4, -3.5, 4.98, 5.189, 6.4));

    // remove file
    remove("vals.txt");
}

void writeTraceFile(std::shared_ptr<TPrior<double>> prior){
    TRandomGenerator randomGenerator;
    std::shared_ptr<TParameterDefinition> def1 = std::make_shared<TParameterDefinition>("def1");
    def1->setDimensions({1});
    def1->setMeanVarFile("vals_meanVar.txt");
    std::shared_ptr<TMCMCParameterBase> param1 = std::make_shared<BridgeTMCMCParameterArray<double>>(std::move(prior), def1, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param1->initializeStorageBasedOnPrior({1}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    std::shared_ptr<TParameterDefinition> def2 = std::make_shared<TParameterDefinition>("def2");
    def2->setDimensions({3});
    def2->setMeanVarFile("vals_meanVar.txt");
    std::shared_ptr<TMCMCParameterBase> param2 = std::make_shared<BridgeTMCMCParameterArray<double>>(std::move(prior), def2, &randomGenerator);
    param2->initializeStorageBasedOnPrior({3}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    std::shared_ptr<TParameterDefinition> def3 = std::make_shared<TParameterDefinition>("def3");
    def3->setDimensions({2,2});
    def3->setMeanVarFile("vals_meanVar.txt");
    std::shared_ptr<TMCMCParameterBase> param3 = std::make_shared<BridgeTMCMCParameterArray<double>>(std::move(prior), def3, &randomGenerator);
    param3->initializeStorageBasedOnPrior({2,2}, {dimNames, dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    TMCMCTraceFile trace("vals_trace.txt");
    TMCMCTraceFile simulated("vals_simulated.txt");
    TMCMCMeanVarFile meanVar("vals_meanVar.txt");

    trace.addParam(param1); trace.addParam(param2); trace.addParam(param3);
    simulated.addParam(param1); simulated.addParam(param2); simulated.addParam(param3);
    meanVar.addParam(param1); meanVar.addParam(param2); meanVar.addParam(param3);
    trace.writeHeader();
    simulated.writeHeader();
    meanVar.writeHeader();

    // trace file: 3 iterations
    // 1st iteration
    trace.write();

    // change values, write 2nd iteration
    param1->set(8.3);
    param2->set(0, 1.1); param2->set(1, 2.4); param2->set(2, -3.5);
    param3->set(0, 4.98); param3->set(1, 5.189); param3->set(2, 6.4); param3->set(3, 82.3);
    trace.write();

    // change values, write 3rd iteration
    param1->set(1);
    param2->set(0, 2); param2->set(1, 3); param2->set(2, 4);
    param3->set(0, 5); param3->set(1, 6); param3->set(2, 7); param3->set(3, 8);
    trace.write();

    // simulated: just one line
    simulated.write();

    // meanVar
    param1->acceptOrReject(1.);
    param2->acceptOrReject(0, 1.); param2->acceptOrReject(1, 1.); param2->acceptOrReject(2, 1.);
    param3->acceptOrReject(0, 1.); param3->acceptOrReject(1, 1.); param3->acceptOrReject(2, 1.); param3->acceptOrReject(3, 1.);

    meanVar.write();
}

TEST_F(TMCMCParameterArrayTest, _readValsFromFile_trace){
    // write trace
    writeTraceFile(prior);

    // create 3 parameters (1 element, 1D and 2D)
    std::shared_ptr<TParameterDefinition> def1 = std::make_shared<TParameterDefinition>("def1");
    def1->setDimensions({1});
    BridgeTMCMCParameterArray<double> param1(std::move(prior), def1, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param1.initializeStorageBasedOnPrior({1}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    std::shared_ptr<TParameterDefinition> def2 = std::make_shared<TParameterDefinition>("def2");
    def2->setDimensions({3});
    BridgeTMCMCParameterArray<double> param2(std::move(prior), def2, &randomGenerator);
    param2.initializeStorageBasedOnPrior({3}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    std::shared_ptr<TParameterDefinition> def3 = std::make_shared<TParameterDefinition>("def3");
    def3->setDimensions({2,2});
    BridgeTMCMCParameterArray<double> param3(std::move(prior), def3, &randomGenerator);
    param3.initializeStorageBasedOnPrior({2,2}, {dimNames, dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    // now read
    // param1
    std::vector<double> vec;
    param1._readVals("vals_trace.txt", vec);
    EXPECT_THAT(vec, ElementsAre(1));

    // param2
    vec.clear();
    param2._readVals("vals_trace.txt", vec);
    EXPECT_THAT(vec, ElementsAre(2,3,4));

    // param3
    vec.clear();
    param3._readVals("vals_trace.txt", vec);
    EXPECT_THAT(vec, ElementsAre(5,6,7,8));

    // remove file
    remove("vals_trace.txt");
    remove("vals_simulated.txt");
    remove("vals_meanVar.txt");
}

TEST_F(TMCMCParameterArrayTest, _readValsFromFile_simulated){
    // write trace
    writeTraceFile(prior);

    // create 3 parameters (1 element, 1D and 2D)
    std::shared_ptr<TParameterDefinition> def1 = std::make_shared<TParameterDefinition>("def1");
    def1->setDimensions({1});
    BridgeTMCMCParameterArray<double> param1(std::move(prior), def1, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param1.initializeStorageBasedOnPrior({1}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    std::shared_ptr<TParameterDefinition> def2 = std::make_shared<TParameterDefinition>("def2");
    def2->setDimensions({3});
    BridgeTMCMCParameterArray<double> param2(std::move(prior), def2, &randomGenerator);
    param2.initializeStorageBasedOnPrior({3}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    std::shared_ptr<TParameterDefinition> def3 = std::make_shared<TParameterDefinition>("def3");
    def3->setDimensions({2,2});
    BridgeTMCMCParameterArray<double> param3(std::move(prior), def3, &randomGenerator);
    param3.initializeStorageBasedOnPrior({2,2}, {dimNames, dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    // now read
    // param1
    std::vector<double> vec;
    param1._readVals("vals_simulated.txt", vec);
    EXPECT_THAT(vec, ElementsAre(1));

    // param2
    vec.clear();
    param2._readVals("vals_simulated.txt", vec);
    EXPECT_THAT(vec, ElementsAre(2,3,4));

    // param3
    vec.clear();
    param3._readVals("vals_simulated.txt", vec);
    EXPECT_THAT(vec, ElementsAre(5,6,7,8));

    // remove file
    // remove file
    remove("vals_trace.txt");
    remove("vals_simulated.txt");
    remove("vals_meanVar.txt");
}

TEST_F(TMCMCParameterArrayTest, _readValsFromFile_meanVar){
    // write trace
    writeTraceFile(prior);

    // create 3 parameters (1 element, 1D and 2D)
    std::shared_ptr<TParameterDefinition> def1 = std::make_shared<TParameterDefinition>("def1");
    def1->setDimensions({1});
    BridgeTMCMCParameterArray<double> param1(std::move(prior), def1, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param1.initializeStorageBasedOnPrior({1}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    std::shared_ptr<TParameterDefinition> def2 = std::make_shared<TParameterDefinition>("def2");
    def2->setDimensions({3});
    BridgeTMCMCParameterArray<double> param2(std::move(prior), def2, &randomGenerator);
    param2.initializeStorageBasedOnPrior({3}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    std::shared_ptr<TParameterDefinition> def3 = std::make_shared<TParameterDefinition>("def3");
    def3->setDimensions({2,2});
    BridgeTMCMCParameterArray<double> param3(std::move(prior), def3, &randomGenerator);
    param3.initializeStorageBasedOnPrior({2,2}, {dimNames, dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    // now read
    // param1
    std::vector<double> vec;
    param1._readVals("vals_meanVar.txt", vec);
    EXPECT_THAT(vec, ElementsAre(1));

    // param2
    vec.clear();
    param2._readVals("vals_meanVar.txt", vec);
    EXPECT_THAT(vec, ElementsAre(2,3,4));

    // param3
    vec.clear();
    param3._readVals("vals_meanVar.txt", vec);
    EXPECT_THAT(vec, ElementsAre(5,6,7,8));

    // remove file
    // remove file
    remove("vals_trace.txt");
    remove("vals_simulated.txt");
    remove("vals_meanVar.txt");
}

TEST_F(TMCMCParameterArrayTest, checkDefaultInitValNoBoundary) {
    def->setDimensions({5});
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    double initVal = 0.;
    bool hasDefaultInitVal = true;
    EXPECT_EQ(param._checkDefaultInitVal(initVal, hasDefaultInitVal), initVal);
}

TEST_F(TMCMCParameterArrayTest, checkDefaultInitValNoDefaultInitVal) {
    def->setDimensions({5});
    def->setMin("2.", true);
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    double initVal = 1.;
    bool hasDefaultInitVal = false;
    EXPECT_EQ(param._checkDefaultInitVal(initVal, hasDefaultInitVal), initVal);
}

TEST_F(TMCMCParameterArrayTest, checkDefaultInitValMinAndMax) {
    def->setMin("2.", true);
    def->setMax("4.", true);
    def->setDimensions({5});
    def->setInitVal("2.1"); // just to prevent error in constructor
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    double initVal = 0.;
    bool hasDefaultInitVal = true;
    EXPECT_EQ(param._checkDefaultInitVal(initVal, hasDefaultInitVal), 4.);
}

TEST_F(TMCMCParameterArrayTest, _readInitVals_allOk){
    def->setDimensions({5});
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    std::vector<double> vec;
    // just take single number (other cases are covered with above tests)
    param._readInitVals("1", true);

    for (size_t i = 0; i < 5; i++) {
        EXPECT_EQ(param.value<double>(i), 1.);
        EXPECT_EQ(param.oldValue<double>(i), 1.);
    }
    EXPECT_DEATH(param.value<double>(5), ""); // outside array boundaries
    EXPECT_DEATH(param.oldValue<double>(5), ""); // outside array boundaries
}

TEST_F(TMCMCParameterArrayTest, _readInitVals_defaultInitValIsOutsideBoundaries){
    def->setDimensions({5});
    def->setMin("0.", true);
    def->setMax("1.", false);
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    std::vector<double> vec;
    // do as if this is the default initial value
    param._readInitVals("2", true); // will mirror

    for (size_t i = 0; i < 5; i++) {
        EXPECT_EQ(param.value<double>(i), 0.);
        EXPECT_EQ(param.oldValue<double>(i), 0.);
    }
    EXPECT_DEATH(param.value<double>(5), ""); // outside array boundaries
    EXPECT_DEATH(param.oldValue<double>(5), ""); // outside array boundaries
}

TEST_F(TMCMCParameterArrayTest, _readInitVals_initValIsOutsideBoundaries){
    def->setDimensions({5});
    def->setMin("0.", true);
    def->setMax("1.", false);
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    std::vector<double> vec;
    // will not mirror, as it is not the default value
    EXPECT_THROW({try {param._readInitVals("2", false);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error); // too many cols
}

TEST_F(TMCMCParameterArrayTest, _readInitJumpSizesVals_sharedJumpSize){
    def->setDimensions({5});
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    std::vector<double> vec;
    // must be a single number
    param._readInitJumpSizesVals(true, "1");
    EXPECT_EQ(param._jumpSize(), 1.);

    // throws if it is a vector/file
    EXPECT_THROW(param._readInitJumpSizesVals(true, "hundert.txt"), std::runtime_error);
    EXPECT_THROW(param._readInitJumpSizesVals(true, "1,1,1,1,1"), std::runtime_error);
}

TEST_F(TMCMCParameterArrayTest, _readInitJumpSizesVals_differentJumpSizes){
    def->setDimensions({5});
    def->setJumpSizeForAll(false);
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    std::vector<double> vec;
    // can be a number, a filename or a vector. Will just use vec number here, other cases are covered by tests above
    param._readInitJumpSizesVals(false, "1,1,1,1,1");
    for (size_t i = 0; i < 5; i++) {
        EXPECT_EQ(param._jumpSize(i), 1.);
    }
    EXPECT_THROW(param._jumpSize(5), std::runtime_error); // outside array boundaries
}

TEST_F(TMCMCParameterArrayTest, _initJumpSizeVal_shared){
    def->setDimensions({5});
    def->setJumpSizeForAll(true); // shared propKernel
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    param._initJumpSizeVal_shared("1"); // valid
    EXPECT_EQ(param._jumpSize(), 1.);

    EXPECT_THROW(param._initJumpSizeVal_shared("1,1,1,1,1"), std::runtime_error); // vectors/filenames are not valid
    EXPECT_THROW(param._initJumpSizeVal_shared("hundert.txt"), std::runtime_error);
}

TEST_F(TMCMCParameterArrayTest, _initJumpSizeVal_shared_withBoundaries){
    def->setDimensions({5});
    def->setMin("0.", false);
    def->setMax("1.", false);
    def->setPropKernel("normal");
    def->setJumpSizeForAll(true); // shared propKernel
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    param._initJumpSizeVal_shared("1"); // valid, but too big -> will be adjusted
    EXPECT_EQ(param._jumpSize(), 0.5);
}

TEST_F(TMCMCParameterArrayTest, _initJumpSizeVal_unique){
    def->setDimensions({5});
    def->setJumpSizeForAll(false); // unique propKernel
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    param._initJumpSizeVal_unique("1"); // valid
    for (size_t i = 0; i < 5; i++)
        EXPECT_EQ(param._jumpSize(i), 1.);
    EXPECT_THROW(param._jumpSize(5), std::runtime_error); // outside array boundaries

    EXPECT_NO_THROW(param._initJumpSizeVal_unique("1,1,1,1,1")); // vectors/filenames are ok
}

TEST_F(TMCMCParameterArrayTest, _initJumpSizeVal_unique_withBoundaries){
    def->setDimensions({5});
    def->setMin("0.", false);
    def->setMax("1.", false);
    def->setPropKernel("normal");
    def->setJumpSizeForAll(false); // unique propKernel
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    param._initJumpSizeVal_unique("1"); // valid, but too big -> will be adjusted
    for (size_t i = 0; i < 5; i++)
        EXPECT_EQ(param._jumpSize(i), 0.5);
    EXPECT_THROW(param._jumpSize(5), std::runtime_error); // outside array boundaries
}

TEST_F(TMCMCParameterArrayTest, _initJumpSizeVal_bool_shared){
    std::unique_ptr<TPrior<bool>> prior1;
    def->setDimensions({5});
    def->setJumpSizeForAll(true); // shared propKernel
    BridgeTMCMCParameterArray<bool> param(std::move(prior1), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    EXPECT_EQ(param._jumpSize(), true);
}

TEST_F(TMCMCParameterArrayTest, _initJumpSizeVal_bool_uniqueWillBeShared){
    std::unique_ptr<TPrior<bool>> prior1;
    def->setDimensions({5});
    def->setJumpSizeForAll(false); // unique propKernel, but will become shared
    BridgeTMCMCParameterArray<bool> param(std::move(prior1), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    EXPECT_EQ(param._jumpSize(), true);
}

TEST_F(TMCMCParameterArrayTest, constructor_complex){
    // one "integration" test to check constructor
    def->setDimensions({5});
    def->setMin("0.", false);
    def->setMax("1.", true);
    def->setPropKernel("normal");
    def->setJumpSizeForAll(false); // unique propKernel
    def->setInitVal("0.2, 0.4, 0.6, 0.8, 1.0");
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    EXPECT_EQ(param.totalSize(), 5);
    for (size_t i = 0; i < 5; i++) {
        EXPECT_FLOAT_EQ(param.value<double>(i), i/5. + 0.2);
        EXPECT_FLOAT_EQ(param.oldValue<double>(i), i/5. + 0.2);
        EXPECT_EQ(param._jumpSize(i), 0.1);
    }
    EXPECT_THROW(param._jumpSize(5), std::runtime_error); // outside array boundaries
    EXPECT_DEATH(param.value<double>(5), ""); // outside array boundaries
    EXPECT_DEATH(param.oldValue<double>(5), ""); // outside array boundaries
}

TEST_F(TMCMCParameterArrayTest, update){
    def->setDimensions({5});
    MockTMCMCParameterArray param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    // check if _propose is called from update() if _isUpdated is true
    EXPECT_CALL(param, _propose)
            .Times(5);
    for (size_t i = 0; i < 5; i++)
        EXPECT_TRUE(param.update(i));

    EXPECT_THROW(param.update(5), std::runtime_error); // outside array boundaries
}

TEST_F(TMCMCParameterArrayTest, update_isUpdatedFalse){
    def->update(false);
    def->setDimensions({5});
    MockTMCMCParameterArray param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    EXPECT_CALL(param, _propose)
            .Times(0);
    for (size_t i = 0; i < 5; i++)
        EXPECT_FALSE(param.update(i));
}

TEST_F(TMCMCParameterArrayTest, updateBool){
    std::unique_ptr<TPriorNormalWithHyperPrior<bool>> prior1;
    def->setDimensions({5});
    TMCMCParameter<bool> param(std::move(prior1), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    for (size_t i = 0; i < 5; i++)
        EXPECT_EQ(param.value<bool>(i), false);

    for (size_t i = 0; i < 5; i++)
        EXPECT_TRUE(param.update(i));

    for (size_t i = 0; i < 5; i++)
        EXPECT_EQ(param.value<bool>(i), true);
}

TEST_F(TMCMCParameterArrayTest, _propose_normalPropKernel_shared){
    def->setDimensions({5});
    def->setPropKernel("normal");
    def->setJumpSizeForAll(true);
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    // mock random number (normal prop kernel)
    EXPECT_CALL(randomGenerator, getNormalRandom)
            .Times(5)
            .WillOnce(Return(0.))
            .WillOnce(Return(1.))
            .WillOnce(Return(2.))
            .WillOnce(Return(3.))
            .WillOnce(Return(4.));
    for (size_t i = 0; i < 5; i++) {
        param._propose(i);
        EXPECT_EQ(param.oldValue<double>(i), 0.);
        EXPECT_EQ(param.value<double>(i), i);
    }
}

TEST_F(TMCMCParameterArrayTest, _propose_normalPropKernel_minMax_shared){
    def->setDimensions({5});
    def->setPropKernel("normal");
    def->setJumpSizeForAll(true);
    def->setMin("-1.", true);
    def->setMax("1.", true);
    def->setInitVal("-0.5,-0.5,0.,0.5,0.5");
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    // mock random number (normal prop kernel)
    EXPECT_CALL(randomGenerator, getNormalRandom)
            .Times(5)
            .WillOnce(Return(-1.))
            .WillOnce(Return(-0.5))
            .WillOnce(Return(0.))
            .WillOnce(Return(0.5))
            .WillOnce(Return(1.));
    std::vector<double> expected = {-0.5, -1., 0., 1., 0.5};
    std::vector<double> expectedOld = {-0.5,-0.5,0.,0.5,0.5};
    for (size_t i = 0; i < 5; i++) {
        // should mirror
        param._propose(i);
        EXPECT_EQ(param.oldValue<double>(i), expectedOld[i]);
        EXPECT_EQ(param.value<double>(i), expected[i]);
    }
}

// won't test only min boundary, only max boundary and uniform proposal kernel; as these functionalities should be
// covered by other tests


TEST_F(TMCMCParameterArrayTest, _propose_uniformPropKernel_unique){ // use uniform instead of normal, because proposal kernels do influence result from mocked randomGenerator here
    def->setDimensions({5});
    def->setPropKernel("uniform");
    def->setJumpSizeForAll(false);
    def->setInitJumpSizeProposal("1,2,3,4,5");
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    // mock random number (normal prop kernel)
    EXPECT_CALL(randomGenerator, getRand)
            .Times(5)
            .WillOnce(Return(0.))
            .WillOnce(Return(1.))
            .WillOnce(Return(2.))
            .WillOnce(Return(3.))
            .WillOnce(Return(4.));
    std::vector<double> expected = {0*1 -1./2., 1*2. - 2./2., 2*3. - 3./2., 3*4. - 4./2., 4*5. - 5./2.};
    for (size_t i = 0; i < 5; i++) {
        param._propose(i);
        EXPECT_EQ(param.oldValue<double>(i), 0.);
        EXPECT_EQ(param.value<double>(i), expected[i]);
    }
}

TEST_F(TMCMCParameterArrayTest, _propose_uniformPropKernel_minMax_unique){
    def->setDimensions({5});
    def->setPropKernel("uniform");
    def->setJumpSizeForAll(false);
    def->setInitJumpSizeProposal("0.01,0.05,0.1,0.5,2"); // will be automatically adjusted to: 0.01, 0.05, 0.1, 0.5, 1 (should not be bigger than range/2)
    def->setMin("-1.", true);
    def->setMax("1.", true);
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    // mock random number (uniform prop kernel)
    EXPECT_CALL(randomGenerator, getRand)
            .Times(5)
            .WillOnce(Return(-1.))
            .WillOnce(Return(-0.5))
            .WillOnce(Return(0.))
            .WillOnce(Return(0.5))
            .WillOnce(Return(1.));
    std::vector<double> expected = {-1.*0.01 - 0.01/2., -0.5*0.05 - 0.05/2., 0*0.1 - 0.1/2., 0.5*0.5 - 0.5/2., 1.*1. - 1./2.}; // mirror
    for (size_t i = 0; i < 5; i++) {
        // should mirror
        param._propose(i);
        EXPECT_EQ(param.oldValue<double>(i), 0.);
        EXPECT_EQ(param.value<double>(i), expected[i]);
    }
}

TEST_F(TMCMCParameterArrayTest, getLogPriorRatio){
    // first construct mean and sd
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    std::vector<std::shared_ptr<TObservationsBase>> observations = {};
    auto defSd = std::make_shared<TParameterDefinition>("defSd");
    defSd->setInitVal("1.");
    auto defMean = std::make_shared<TParameterDefinition>("defMean");
    std::shared_ptr<TPriorNormal<double>> priorMean;
    std::shared_ptr<TPriorNormal<double>> priorSd;
    std::shared_ptr<TMCMCParameter<double>> mean;
    mean = std::make_shared<TMCMCParameter<double>>(priorMean, defMean, &randomGenerator);
    std::shared_ptr<TMCMCParameter<double>> sd;
    sd = std::make_shared<TMCMCParameter<double>>(priorSd, defSd, &randomGenerator);
    params.push_back(mean);
    params.push_back(sd);
    prior = std::make_shared<TPriorNormalWithHyperPrior<double>>();

    prior->initPriorWithHyperPrior(params, observations);

    // now construct paramArray
    def->setDimensions({5});
    std::shared_ptr<BridgeTMCMCParameterArray<double>> param;
    param = std::make_shared<BridgeTMCMCParameterArray<double>>(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param->initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)
    param->initializeStorage();

    // set some values
    param->set(0, 0.);
    param->set(1, 1.);
    param->set(2, 2.);
    param->set(3, 3.);
    param->set(4, 4.);

    std::vector<double> expected = {0., -0.5, -2, -4.5, -8};
    for (size_t i = 0; i < 5; i++){
        EXPECT_FLOAT_EQ(param->getLogPriorRatio(i), expected[i]);
        EXPECT_FLOAT_EQ(param->getLogPriorRatio(i), param->getLogPriorDensity(i) - param->getLogPriorDensityOld(i));
    }
}

TEST_F(TMCMCParameterArrayTest, reject){
    def->setDimensions({5});
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    // mock random number (normal prop kernel)
    EXPECT_CALL(randomGenerator, getNormalRandom)
            .Times(5)
            .WillOnce(Return(0.))
            .WillOnce(Return(1.))
            .WillOnce(Return(2.))
            .WillOnce(Return(3.))
            .WillOnce(Return(4.));

    // update
    for (size_t i = 0; i < 5; i++) {
        param.update(i);
        EXPECT_EQ(param.oldValue<double>(i), 0.);
        EXPECT_EQ(param.value<double>(i), i);
    }
    // ...and reject
    for (size_t i = 0; i < 5; i++) {
        param._reject(i);
        EXPECT_EQ(param.oldValue<double>(i), 0.);
        EXPECT_EQ(param.value<double>(i), 0.);
    }
}

TEST_F(TMCMCParameterArrayTest, acceptOrRejectDoReject){
    def->setDimensions({5});
    def->setMeanVarFile("something");
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    // mock random number (normal prop kernel)
    EXPECT_CALL(randomGenerator, getNormalRandom)
        .Times(5)
        .WillOnce(Return(1.))
        .WillOnce(Return(2.))
        .WillOnce(Return(3.))
        .WillOnce(Return(4.))
        .WillOnce(Return(5.));

    // update
    for (size_t i = 0; i < 5; i++) {
        param.update(i);
        EXPECT_EQ(param.oldValue<double>(i), 0.);
        EXPECT_EQ(param.value<double>(i), i + 1);
    }
    // ...and reject
    // mock random number (for accepting/rejecting)
    EXPECT_CALL(randomGenerator, getRand)
        .Times(5)
        .WillRepeatedly(Return(0.5));
    double logH = log(0.1);
    for (size_t i = 0; i < 5; i++) {
        EXPECT_FALSE(param.acceptOrReject(i, logH));
        EXPECT_EQ(param.oldValue<double>(i), 0.);
        EXPECT_EQ(param.value<double>(i), 0.);
        EXPECT_FLOAT_EQ(param._mean(i), 0.);
    }
    EXPECT_THROW(param.acceptOrReject(5, logH), std::runtime_error); // outside array boundaries
}

TEST_F(TMCMCParameterArrayTest, acceptOrRejectDoAccept){
    def->setDimensions({5});
    def->setMeanVarFile("something");
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    // mock random number (normal prop kernel)
    EXPECT_CALL(randomGenerator, getNormalRandom)
        .Times(5)
        .WillOnce(Return(1.))
        .WillOnce(Return(2.))
        .WillOnce(Return(3.))
        .WillOnce(Return(4.))
        .WillOnce(Return(5.));

    // update
    for (size_t i = 0; i < 5; i++) {
        param.update(i);
        EXPECT_EQ(param.oldValue<double>(i), 0.);
        EXPECT_EQ(param.value<double>(i), i + 1);
    }
    // ...and reject
    // mock random number (for accepting/rejecting)
    EXPECT_CALL(randomGenerator, getRand)
        .Times(5)
        .WillRepeatedly(Return(0.5));
    double logH = log(0.8);
    for (size_t i = 0; i < 5; i++) {
        EXPECT_TRUE(param.acceptOrReject(i, logH));
        EXPECT_EQ(param.oldValue<double>(i), 0.);
        EXPECT_EQ(param.value<double>(i), i + 1.);
        EXPECT_FLOAT_EQ(param._mean(i), i + 1.);
    }
}

TEST_F(TMCMCParameterArrayTest, meanSdVarThrow){
    def->setDimensions({5});
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    for (size_t i = 0; i < 5; i++) {
        EXPECT_THROW(param._mean(i), std::runtime_error); // mean not stored
        EXPECT_THROW(param._sd(i), std::runtime_error);
        EXPECT_THROW(param._var(i), std::runtime_error);
    }

    EXPECT_THROW(param._mean(5), std::runtime_error); // outside array boundaries
    EXPECT_THROW(param._sd(5), std::runtime_error);
    EXPECT_THROW(param._var(5), std::runtime_error);
}


TEST_F(TMCMCParameterArrayTest, set){
    def->setDimensions({5});
    def->setInitVal("0,1,2,3,4");
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    for (size_t i = 0; i < 5; i++)
        param.set(i, i + 1);

    for (size_t i = 0; i < 5; i++) {
        EXPECT_EQ(param.oldValue<double>(i), i);
        EXPECT_EQ(param.value<double>(i), i + 1.);
    }
    EXPECT_THROW({try {param.set(5, 0.);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TMCMCParameterArrayTest, setAcrossBoundary){
    def->setDimensions({5});
    def->setInitVal("0,1,2,3,4");
    def->setMax("5.", true);
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    for (size_t i = 0; i < 5; i++)
        EXPECT_THROW({try {param.set(i, i + 6);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TMCMCParameterArrayTest, reset){
    def->setDimensions({5});
    def->setInitVal("0,1,2,3,4");
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    // set
    for (size_t i = 0; i < 5; i++)
        param.set(i, i + 1.);

    for (size_t i = 0; i < 5; i++) {
        EXPECT_EQ(param.oldValue<double>(i), i);
        EXPECT_EQ(param.value<double>(i), i + 1.);
    }

    // reset
    for (size_t i = 0; i < 5; i++)
        param.reset(i);

    for (size_t i = 0; i < 5; i++) {
        EXPECT_EQ(param.oldValue<double>(i), i);
        EXPECT_EQ(param.value<double>(i), i);
    }
    EXPECT_THROW(param.reset(5), std::runtime_error); // outside array boundaries
}

TEST_F(TMCMCParameterArrayTest, _setPropKernel_shared){
    def->setDimensions({5});
    def->setInitJumpSizeProposal("0.1");
    def->setMin("0.", true);
    def->setMax("1.", true);
    def->setJumpSizeForAll(true);
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    param._setPropKernel(1.); // will be set to something smaller because of min max
    EXPECT_EQ(param._jumpSize(), 0.5);

    // functions where index is used should not work
    EXPECT_THROW(param._setPropKernel(1., 0), std::runtime_error);
}

TEST_F(TMCMCParameterArrayTest, _setPropKernel_unique){
    def->setDimensions({5});
    def->setInitJumpSizeProposal("0.1");
    def->setMin("0.", true);
    def->setMax("1.", true);
    def->setJumpSizeForAll(false);
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    for (size_t i = 0; i < 5; i++) {
        param._setPropKernel(1., i);
        EXPECT_EQ(param._jumpSize(i), 0.5); // will be set to something smaller because of min max
    }

    // functions without index is used should not work
    EXPECT_THROW(param._setPropKernel(0), std::runtime_error);
}

TEST_F(TMCMCParameterArrayTest, acceptanceRate_shared){
    def->setDimensions({5});
    def->setJumpSizeForAll(true);
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    // nothing happened yet
    EXPECT_EQ(param.getAcceptanceRate(), 1.);

    // mock random number (normal prop kernel)
    EXPECT_CALL(randomGenerator, getNormalRandom)
            .Times(AtLeast(1))
            .WillRepeatedly(Return(2.));
    // mock random number (for accepting/rejecting)
    EXPECT_CALL(randomGenerator, getRand)
            .Times(AtLeast(1))
            .WillRepeatedly(Return(0.5));

    // accept
    double logH = std::log(0.8);
    for (size_t i = 0; i < 5; i++){
        param.update(i);
        EXPECT_TRUE(param.acceptOrReject(i, logH));
    }
    for (size_t i = 0; i < 5; i++)
        EXPECT_FLOAT_EQ(param.getAcceptanceRate(), 1.);

    // reject
    logH = std::log(0.1);
    for (size_t i = 0; i < 5; i++){
        param.update(i);
        EXPECT_FALSE(param.acceptOrReject(i, logH));
    }

    for (size_t i = 0; i< 5; i++)
        EXPECT_FLOAT_EQ(param.getAcceptanceRate(), 6./11.);
    // 6./11. because each parameter is 1x accepted and 1x rejected -> 5 accepted / 10 total; and then +1 in nom. and denom.
}

TEST_F(TMCMCParameterArrayTest, acceptanceRate_unique){
    def->setDimensions({5});
    def->setJumpSizeForAll(false);
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    // nothing happened yet
    for (size_t i = 0; i < 5; i++)
        EXPECT_EQ(param.getAcceptanceRate(i), 1.);

    // mock random number (normal prop kernel)
    EXPECT_CALL(randomGenerator, getNormalRandom)
            .Times(AtLeast(1))
            .WillRepeatedly(Return(2.));
    // mock random number (for accepting/rejecting)
    EXPECT_CALL(randomGenerator, getRand)
            .Times(AtLeast(1))
            .WillRepeatedly(Return(0.5));

    // accept
    double logH = std::log(0.8);
    for (size_t i = 0; i < 5; i++){
        param.update(i);
        EXPECT_TRUE(param.acceptOrReject(i, logH));
    }
    for (size_t i = 0; i < 5; i++)
        EXPECT_FLOAT_EQ(param.getAcceptanceRate(i), 1.);

    // reject
    logH = std::log(0.1);
    for (size_t i = 0; i < 5; i++){
        param.update(i);
        EXPECT_FALSE(param.acceptOrReject(i, logH));
    }

    for (size_t i = 0; i< 5; i++)
        EXPECT_FLOAT_EQ(param.getAcceptanceRate(i), 2./3.);
    // 2./3. because each parameter is 1x accepted and 1x rejected -> 1 accepted / 2 total; and then +1 in nom. and denom.

    // mean acceptance rate
    EXPECT_FLOAT_EQ(param.getMeanAcceptanceRate(), 6./11.);
}

TEST_F(TMCMCParameterArrayTest, adjustProposal_shared){
    def->setDimensions({5});
    def->setInitJumpSizeProposal("0.1");
    def->setJumpSizeForAll(true);
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    EXPECT_FLOAT_EQ(param._jumpSize(), 0.1);

    // mock random number (normal prop kernel)
    EXPECT_CALL(randomGenerator, getNormalRandom)
            .Times(AtLeast(1))
            .WillRepeatedly(Return(2.));
    // mock random number (for accepting/rejecting)
    EXPECT_CALL(randomGenerator, getRand)
            .Times(AtLeast(1))
            .WillRepeatedly(Return(0.5));

    // accept
    double logH = std::log(0.8);
    for (size_t i = 0; i < 5; i++){
        param.update(i);
        EXPECT_TRUE(param.acceptOrReject(i, logH));
    }
    for (size_t i = 0; i < 5; i++)
        EXPECT_FLOAT_EQ(param.getAcceptanceRate(), 1.);


    param.adjustProposalAndResetCounters();
    EXPECT_FLOAT_EQ(param._jumpSize(), 0.3);

    // now reject
    logH = std::log(0.1);
    for (size_t i = 0; i < 5; i++){
        param.update(i);
        EXPECT_FALSE(param.acceptOrReject(i, logH));
    }

    param.adjustProposalAndResetCounters();
    EXPECT_FLOAT_EQ(param._jumpSize(), 1./6. * 0.3 * 3.); // 1/6 because never accepted (0) + 1 divided by 5 updates + 1 = 6

}

TEST_F(TMCMCParameterArrayTest, adjustProposal_unique){
    def->setDimensions({5});
    def->setInitJumpSizeProposal("0.1");
    def->setJumpSizeForAll(false);
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)

    // nothing happened yet
    for (size_t i = 0; i < 5; i++)
        EXPECT_EQ(param._jumpSize(i), 0.1);

    // mock random number (normal prop kernel)
    EXPECT_CALL(randomGenerator, getNormalRandom)
            .Times(AtLeast(1))
            .WillRepeatedly(Return(2.));
    // mock random number (for accepting/rejecting)
    EXPECT_CALL(randomGenerator, getRand)
            .Times(AtLeast(1))
            .WillRepeatedly(Return(0.5));

    // accept
    double logH = std::log(0.8);
    for (size_t i = 0; i < 5; i++){
        param.update(i);
        EXPECT_TRUE(param.acceptOrReject(i, logH));
    }
    for (size_t i = 0; i < 5; i++)
        EXPECT_FLOAT_EQ(param.getAcceptanceRate(i), 1.);

    param.adjustProposalAndResetCounters();

    for (size_t i = 0; i< 5; i++)
        EXPECT_FLOAT_EQ(param._jumpSize(i), 0.3);

    // now reject
    logH = std::log(0.1);
    for (size_t i = 0; i < 5; i++){
        param.update(i);
        EXPECT_FALSE(param.acceptOrReject(i, logH));
    }
    for (size_t i = 0; i < 5; i++)
        EXPECT_FLOAT_EQ(param.getAcceptanceRate(i), 1./2.);

    param.adjustProposalAndResetCounters();

    for (size_t i = 0; i< 5; i++)
        EXPECT_FLOAT_EQ(param._jumpSize(i), 1./2. * 0.3 * 3.);
}

TEST_F(TMCMCParameterArrayTest, updatePrior){
    // first construct sd
    auto defSd = std::make_shared<TParameterDefinition>("defSd");
    defSd->setInitVal("1.");
    auto priorSd = std::make_shared<TPriorExponential<double>>();
    priorSd->initialize("1");
    auto sd = std::make_shared<TMCMCParameter<double>>(std::move(priorSd), defSd, &randomGenerator);
    // then construct mean
    auto defMean = std::make_shared<TParameterDefinition>("defMean");
    auto priorMean = std::make_shared<TPriorNormal<double>>();
    priorMean->initialize("0,1");
    auto mean = std::make_shared<TMCMCParameter<double>>(std::move(priorMean), defMean, &randomGenerator);
    // now construct priorWithHyperPrior (normal)
    std::vector<std::shared_ptr<TMCMCParameterBase> > params;
    std::vector<std::shared_ptr<TObservationsBase>> observations = {};
    params.push_back(mean);
    params.push_back(sd);
    prior = std::make_shared<TPriorNormalWithHyperPrior<double>>();
    prior->initPriorWithHyperPrior(params, observations);
    // now construct paramArray
    def->setDimensions({5});
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    std::shared_ptr<TNamesEmpty> dimNames = std::make_shared<TNamesIndices>();
    param.initializeStorageBasedOnPrior({5}, {dimNames}); // initialize storage of parameter (on bottom of the dag, normally this would be an observation that is filled by developer)
    param.initializeStorage();

    // set some values
    for (size_t i = 0; i < 5; i++)
        param.set(i, i);

    // will update mean and sd -> will need one normal random number for proposing and one uniform random number for accepting, each
    EXPECT_CALL(randomGenerator, getRand)
            .Times(2);
    EXPECT_CALL(randomGenerator, getNormalRandom)
            .Times(2);

    EXPECT_NO_THROW(param.updatePrior());
}

// idea: first test main functionalities with double, then test uint and bool specifically
TEST_F(TMCMCParameterArrayTest, initBoundaryMin) {
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    bool hasDefaultMin = false;
    bool hasDefaultMax = true;
    std::string min = "0.";
    std::string max;
    bool minIncluded = false;
    bool maxIncluded = false;
    param._initBoundary(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_EQ(param.min(), convertString<double>(min));
    EXPECT_EQ(param.max(), std::numeric_limits<double>::max());
    EXPECT_EQ(param.minIncluded(), false);
    EXPECT_EQ(param.maxIncluded(), true);
}

TEST_F(TMCMCParameterArrayTest, initBoundaryMax) {
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    bool hasDefaultMin = true;
    bool hasDefaultMax = false;
    std::string min;
    std::string max = "0.";
    bool minIncluded = false;
    bool maxIncluded = false;
    param._initBoundary(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_EQ(param.min(), std::numeric_limits<double>::lowest());
    EXPECT_EQ(param.max(), convertString<double>(max));
    EXPECT_EQ(param.minIncluded(), true);
    EXPECT_EQ(param.maxIncluded(), false);
}

TEST_F(TMCMCParameterArrayTest, initBoundaryMinMax) {
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;
    std::string min = "0.";
    std::string max = "1.";
    bool minIncluded = false;
    bool maxIncluded = false;
    param._initBoundary(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_EQ(param.min(), convertString<double>(min));
    EXPECT_EQ(param.max(), convertString<double>(max));
    EXPECT_EQ(param.minIncluded(), false);
    EXPECT_EQ(param.maxIncluded(), false);
}

TEST_F(TMCMCParameterArrayTest, boolparam){
    std::shared_ptr<TPrior<bool>> priorBool;
    BridgeTMCMCParameterArray<bool> param(std::move(priorBool), def, &randomGenerator);
    
    EXPECT_EQ(param.min(), false);
    EXPECT_EQ(param.max(), true);
    EXPECT_EQ(param.minIncluded(), true);
    EXPECT_EQ(param.maxIncluded(), true);
}

TEST_F(TMCMCParameterArrayTest, boolparamIgnoreBoundaries){
    def->setMin("0");

    // exactly the same as before
    std::shared_ptr<TPrior<bool>> priorBool;
    BridgeTMCMCParameterArray<bool> param(std::move(priorBool), def, &randomGenerator);
    EXPECT_EQ(param.min(), false);
    EXPECT_EQ(param.max(), true);
    EXPECT_EQ(param.minIncluded(), true);
    EXPECT_EQ(param.maxIncluded(), true);
}

TEST_F(TMCMCParameterArrayTest, intparam){
    def->setMin("-12.74");
    def->setMax("11");

    // exactly the same as before
    std::shared_ptr<TPrior<int>> priorInt;
    BridgeTMCMCParameterArray<int> param(std::move(priorInt), def, &randomGenerator);

    EXPECT_EQ(param.min(), -12); // just cuts off everything after .
    EXPECT_EQ(param.max(), 11);
    EXPECT_EQ(param.minIncluded(), true);
    EXPECT_EQ(param.maxIncluded(), true);
}

TEST_F(TMCMCParameterArrayTest, initPropKernelNormal) {
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    double range;
    std::unique_ptr<TPropKernel<double>> propKernel = param._initPropKernel(ProposalKernel::normal);
    EXPECT_TRUE(propKernel);
    std::string className = str_type(*propKernel);
    eraseAllNonAlphabeticCharacters(className); // get rid of gcc-numbers
    EXPECT_EQ( className, "TPropKernelNormalIdE"); // check if correct class is instantiated
}

TEST_F(TMCMCParameterArrayTest, initPropKernelUniform) {
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    double range;
    std::unique_ptr<TPropKernel<double>> propKernel = param._initPropKernel(ProposalKernel::uniform);
    EXPECT_TRUE(propKernel);
    std::string className = str_type(*propKernel);
    eraseAllNonAlphabeticCharacters(className); // get rid of gcc-numbers
    EXPECT_EQ( className, "TPropKernelUniformIdE"); // check if correct class is instantiated
}

TEST_F(TMCMCParameterArrayTest, initPropKernelInteger) {
    std::shared_ptr<TPrior<int>> priorInt;
    BridgeTMCMCParameterArray<int> param(std::move(priorInt), def, &randomGenerator);
    bool hasDefaultMin = true;
    bool hasDefaultMax = true;
    double range;
    std::unique_ptr<TPropKernel<int>> propKernel = param._initPropKernel(ProposalKernel::integer);
    EXPECT_TRUE(propKernel);
    std::string className = str_type(*propKernel);
    eraseAllNonAlphabeticCharacters(className); // get rid of gcc-numbers
    EXPECT_EQ( className, "TPropKernelIntegerIiE"); // check if correct class is instantiated
}

TEST_F(TMCMCParameterArrayTest, initPropKernelBool) {
    std::shared_ptr<TPrior<bool>> priorBool;
    BridgeTMCMCParameterArray<bool> param(std::move(priorBool), def, &randomGenerator);

    bool hasDefaultMin = true;
    bool hasDefaultMax = true;
    bool range;
    std::unique_ptr<TPropKernel<bool>> propKernel = param._initPropKernel(ProposalKernel::boolean);
    EXPECT_TRUE(propKernel);
    std::string className = str_type(*propKernel);
    eraseAllNonAlphabeticCharacters(className); // get rid of gcc-numbers
    EXPECT_EQ( className, "TPropKernelBoolIbE"); // check if correct class is instantiated
}

TEST_F(TMCMCParameterArrayTest, initPropKernelInt) {
    std::shared_ptr<TPrior<int>> priorInt;
    BridgeTMCMCParameterArray<int> param(std::move(priorInt), def, &randomGenerator);

    bool hasDefaultMin = true;
    bool hasDefaultMax = false;
    double range = 1.;
    // will ignore the distribution given and always initialize to propKernelInteger
    std::unique_ptr<TPropKernel<int>> propKernel = param._initPropKernel(ProposalKernel::uniform);
    EXPECT_TRUE(propKernel);
    std::string className = str_type(*propKernel);
    eraseAllNonAlphabeticCharacters(className); // get rid of gcc-numbers
    EXPECT_EQ( className, "TPropKernelIntegerIiE"); // check if correct class is instantiated
}

// reSet boundaries
TEST_F(TMCMCParameterArrayTest, resetBoundary_noBoundaryBefore) {
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);

    // now reSet
    bool hasDefaultMin = false;
    bool hasDefaultMax = true;
    std::string min = "0.";
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIncluded = false;
    bool maxIncluded = false;
    param.reSetBoundaries(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded); // initializes boundary
    EXPECT_EQ(param.min(), 0.);
    EXPECT_EQ(param.max(), std::numeric_limits<double>::max());
}

TEST_F(TMCMCParameterArrayTest, resetBoundary_minIsSmallerThanBefore) {
    def->setMin("0.", false);
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    EXPECT_EQ(param.min(), 0.);
    EXPECT_EQ(param.max(), std::numeric_limits<double>::max());

    // now reSet
    bool hasDefaultMin = false;
    bool hasDefaultMax = true;
    std::string min = "-10.";
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIncluded = false;
    bool maxIncluded = false;
    param.reSetBoundaries(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_EQ(param.min(), 0.); // min does not change, as previous min is ok with current min
    EXPECT_EQ(param.max(), std::numeric_limits<double>::max());
}

TEST_F(TMCMCParameterArrayTest, resetBoundary_minIncludedThanBefore) {
    def->setMin("0.", false);
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    EXPECT_EQ(param.min(), 0.);
    EXPECT_EQ(param.max(), std::numeric_limits<double>::max());

    // now reSet
    bool hasDefaultMin = false;
    bool hasDefaultMax = true;
    std::string min = "0.";
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIncluded = true; // minIncluded now false
    bool maxIncluded = false;
    param.reSetBoundaries(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_EQ(param.min(), 0.); // min does not change
    EXPECT_EQ(param.max(), std::numeric_limits<double>::max());
    EXPECT_FALSE(param.minIncluded());
}

TEST_F(TMCMCParameterArrayTest, resetBoundary_minNotIncludedThanBefore) {
    def->setMin("0.", true);
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    EXPECT_EQ(param.min(), 0.);
    EXPECT_EQ(param.max(), std::numeric_limits<double>::max());

    // now reSet
    bool hasDefaultMin = false;
    bool hasDefaultMax = true;
    std::string min = "0.";
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIncluded = false; // minIncluded now false
    bool maxIncluded = false;
    param.reSetBoundaries(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_EQ(param.min(), 0.); // min does not change
    EXPECT_EQ(param.max(), std::numeric_limits<double>::max());
    EXPECT_FALSE(param.minIncluded());
}

TEST_F(TMCMCParameterArrayTest, resetBoundary_minIsBiggerThanBefore) {
    def->setMin("0.", false);
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    EXPECT_EQ(param.min(), 0.);
    EXPECT_EQ(param.max(), std::numeric_limits<double>::max());

    // now reSet
    bool hasDefaultMin = false;
    bool hasDefaultMax = true;
    std::string min = "10.";
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIncluded = false;
    bool maxIncluded = false;
    param.reSetBoundaries(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_EQ(param.min(), 10.); // min changes
    EXPECT_EQ(param.max(), std::numeric_limits<double>::max());
}

TEST_F(TMCMCParameterArrayTest, resetBoundary_maxIsSmallerThanBefore) {
    def->setMax("0.", false);
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    EXPECT_EQ(param.min(), std::numeric_limits<double>::lowest());
    EXPECT_EQ(param.max(), 0.);

    // now reSet
    bool hasDefaultMin = true;
    bool hasDefaultMax = false;
    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = "-10.";
    bool minIncluded = false;
    bool maxIncluded = false;
    param.reSetBoundaries(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_EQ(param.min(), std::numeric_limits<double>::lowest());
    EXPECT_EQ(param.max(), -10.); // max changes
}

TEST_F(TMCMCParameterArrayTest, resetBoundary_maxIncludedThanBefore) {
    def->setMax("0.", false);
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    EXPECT_EQ(param.min(), std::numeric_limits<double>::lowest());
    EXPECT_EQ(param.max(), 0.);

    // now reSet
    bool hasDefaultMin = true;
    bool hasDefaultMax = false;
    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = "0.";
    bool minIncluded = false;
    bool maxIncluded = true; // max is now included
    param.reSetBoundaries(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_EQ(param.min(), std::numeric_limits<double>::lowest());
    EXPECT_EQ(param.max(), 0.);
    EXPECT_FALSE(param.maxIncluded()); // max still not included
}

TEST_F(TMCMCParameterArrayTest, resetBoundary_maxNotIncludedThanBefore) {
    def->setMax("0.", true);
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    EXPECT_EQ(param.min(), std::numeric_limits<double>::lowest());
    EXPECT_EQ(param.max(), 0.);

    // now reSet
    bool hasDefaultMin = true;
    bool hasDefaultMax = false;
    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = "0.";
    bool minIncluded = false;
    bool maxIncluded = false; // max is now not included
    param.reSetBoundaries(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_EQ(param.min(), std::numeric_limits<double>::lowest());
    EXPECT_EQ(param.max(), 0.);
    EXPECT_FALSE(param.maxIncluded()); // max now not included
}

TEST_F(TMCMCParameterArrayTest, resetBoundary_maxIsBiggerThanBefore) {
    def->setMax("0.", false);
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    EXPECT_EQ(param.min(), std::numeric_limits<double>::lowest());
    EXPECT_EQ(param.max(), 0.);

    // now reSet
    bool hasDefaultMin = true;
    bool hasDefaultMax = false;
    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = "10.";
    bool minIncluded = false;
    bool maxIncluded = false;
    param.reSetBoundaries(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_EQ(param.min(), std::numeric_limits<double>::lowest());
    EXPECT_EQ(param.max(), 0.); // max does not change - previous max is still mathematically valid
}

TEST_F(TMCMCParameterArrayTest, resetBoundary_minAndMax) {
    def->setMin("0.", false);
    def->setMax("1.", false);
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    EXPECT_EQ(param.min(), 0.);
    EXPECT_EQ(param.max(), 1.);

    // now reSet
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;
    std::string min = "0.1";
    std::string max = "0.9";
    bool minIncluded = false;
    bool maxIncluded = false;
    param.reSetBoundaries(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_EQ(param.min(), 0.1);
    EXPECT_EQ(param.max(), 0.9);
}

TEST_F(TMCMCParameterArrayTest, resetBoundary_minBiggerThanAndMax) {
    def->setMin("0.", false);
    def->setMax("1.", false);
    BridgeTMCMCParameterArray<double> param(std::move(prior), def, &randomGenerator);
    EXPECT_EQ(param.min(), 0.);
    EXPECT_EQ(param.max(), 1.);

    // now reSet
    bool hasDefaultMin = false;
    bool hasDefaultMax = true;
    std::string min = "2.";
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIncluded = false;
    bool maxIncluded = false;
    EXPECT_THROW({try {param.reSetBoundaries(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TMCMCParameterArrayTest, resetBoundary_minAndMax_integer) {
    def->setMin("-2", false);
    def->setMax("2", false);
    std::shared_ptr<TPrior<int>> priorInt;
    BridgeTMCMCParameterArray<int> param(std::move(priorInt), def, &randomGenerator);
    EXPECT_EQ(param.min(), -2);
    EXPECT_EQ(param.max(), 2);

    // now reSet
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;
    std::string min = "-1";
    std::string max = "2";
    bool minIncluded = false;
    bool maxIncluded = true;
    param.reSetBoundaries(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_EQ(param.min(), -1);
    EXPECT_EQ(param.max(), 2);
    EXPECT_EQ(param.minIncluded(), false);
    EXPECT_EQ(param.maxIncluded(), false);
}

TEST_F(TMCMCParameterArrayTest, resetBoundary_minAndMax_bool) {
    std::shared_ptr<TPrior<bool>> priorBool;
    BridgeTMCMCParameterArray<bool> param(std::move(priorBool), def, &randomGenerator);

    // now reSet
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;
    std::string min = "1";
    std::string max = "0";
    bool minIncluded = false;
    bool maxIncluded = true;
    // min > max
    EXPECT_THROW(try{param.reSetBoundaries(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);} catch(...){throw std::runtime_error("blah");}, std::runtime_error); // too few
}