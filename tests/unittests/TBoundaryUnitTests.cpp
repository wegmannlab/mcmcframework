//
// Created by madleina on 11.12.20.
//

#include "../../core/TBoundary.h"
#include "../TestCase.h"
using namespace testing;

//--------------------------------------------
// TBoundary
//--------------------------------------------

//--------------------------------------------
// Test: min and max are default (numeric limit)
struct TBoundaryDefaultDoubleTest : Test {
    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIncluded = true;
    bool maxIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;
    TBoundary<double> boundary;
};

TEST_F(TBoundaryDefaultDoubleTest, init){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_THAT(boundary.minIncluded(), true);
    EXPECT_THAT(boundary.maxIncluded(), true);
    EXPECT_EQ(boundary.min(), std::numeric_limits<double>::lowest());
    EXPECT_EQ(boundary.max(), std::numeric_limits<double>::max());
    EXPECT_EQ(boundary.range(), std::numeric_limits<double>::max());
}

TEST_F(TBoundaryDefaultDoubleTest, checkForValidJumpSizeMirroring){
    // jump size is restricted to range/2
    EXPECT_NO_FATAL_FAILURE(boundary.updateAndMirror(0., std::numeric_limits<double>::max()/2.));
    EXPECT_NO_FATAL_FAILURE(boundary.updateAndMirror(0., -std::numeric_limits<double>::max()/2.));

    // too large
    EXPECT_DEATH(boundary.updateAndMirror(0., std::numeric_limits<double>::max()*0.51), "");
    EXPECT_DEATH(boundary.updateAndMirror(0., -std::numeric_limits<double>::max()*0.51), "");
}

// don't mirror, min and max are included
TEST_F(TBoundaryDefaultDoubleTest, dontMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 (cannot jump directly on maximum because jump size is restricted to range/2)
    EXPECT_EQ(boundary.updateAndMirror(0., 1.), 1.);
    EXPECT_EQ(boundary.updateAndMirror(0., 1000.), 1000.);
    // value = 0, jump < 0 (cannot jump directly on minimum because jump size is restricted to range/2)
    EXPECT_EQ(boundary.updateAndMirror(0., -0.5), -0.5);
    EXPECT_EQ(boundary.updateAndMirror(0., -1000.), -1000.);
    // value < 0, jump > 0 (cannot jump directly on maximum because jump size is restricted to range/2)
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 1.), 0.5);
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 1000.), 999.5);
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, -0.2), -0.7);
    double value = std::nextafter(std::numeric_limits<double>::lowest(), 0.); // next-smaller value above numeric min
    EXPECT_EQ(boundary.updateAndMirror(value, std::numeric_limits<double>::lowest()-value), std::numeric_limits<double>::lowest()); // jump exactly on min -> ok because min is included
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(1., 1.), 2.);
    EXPECT_EQ(boundary.updateAndMirror(1000000., 1000.), 1001000.);
    value = std::nextafter(std::numeric_limits<double>::max(), 0.); // next-smaller value below numeric max
    EXPECT_EQ(boundary.updateAndMirror(value, std::numeric_limits<double>::max() - value), std::numeric_limits<double>::max()); // jump exactly on max -> ok because max is included
    // value > 0, jump < 0 (cannot jump directly on minimum because jump size is restricted to range/2)
    EXPECT_EQ(boundary.updateAndMirror(1., -2.), -1.); // ok because min is included
    EXPECT_EQ(boundary.updateAndMirror(1000000., -1000.), 999000.);
}

// don't mirror, min and max are excluded
TEST_F(TBoundaryDefaultDoubleTest, dontMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 (cannot jump directly on maximum because jump size is restricted to range/2)
    // value = 0, jump < 0 (cannot jump directly on minimum because jump size is restricted to range/2)
    // value < 0, jump > 0 (cannot jump directly on maximum because jump size is restricted to range/2)
    // value < 0, jump < 0
    double value = std::nextafter(std::numeric_limits<double>::lowest(), 0.); // next-smaller value above numeric min
    EXPECT_EQ(boundary.updateAndMirror(value, std::numeric_limits<double>::lowest()-value), value); // jump exactly on min -> jump back (min is excluded)
    // value > 0, jump > 0
    value = std::nextafter(std::numeric_limits<double>::max(), 0.); // next-smaller value below numeric max
    EXPECT_EQ(boundary.updateAndMirror(value, std::numeric_limits<double>::max() - value), value); // jump exactly on max -> jump back (max is excluded)
    // value > 0, jump < 0 (cannot jump directly on minimum because jump size is restricted to range/2)
}

// do mirror, min is included
TEST_F(TBoundaryDefaultDoubleTest, doMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
    // value = 0, jump < 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
    // value < 0, jump > 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, -0.2), -0.7);
    double value = std::nextafter(std::numeric_limits<double>::lowest(), 0.); // next-smaller value above numeric min
    EXPECT_EQ(boundary.updateAndMirror(value, -3.*(value-std::numeric_limits<double>::lowest())), std::numeric_limits<double>::lowest() + 2.*(value - std::numeric_limits<double>::lowest())); // jump exactly on min -> ok because min is included
    // value > 0, jump > 0
    value = std::nextafter(std::numeric_limits<double>::max(), 0.); // next-smaller value below numeric max
    EXPECT_EQ(boundary.updateAndMirror(value, 3.*(std::numeric_limits<double>::max() - value)), std::numeric_limits<double>::max() - 2.*(std::numeric_limits<double>::max() - value));
    // value > 0, jump < 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
}

TEST_F(TBoundaryDefaultDoubleTest, doMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    // exactly the same as doMirrorIncluded, as this should not make a difference
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
    // value = 0, jump < 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
    // value < 0, jump > 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, -0.2), -0.7);
    double value = std::nextafter(std::numeric_limits<double>::lowest(), 0.); // next-smaller value above numeric min
    EXPECT_EQ(boundary.updateAndMirror(value, -3.*(value-std::numeric_limits<double>::lowest())), std::numeric_limits<double>::lowest() + 2.*(value - std::numeric_limits<double>::lowest())); // jump exactly on min -> ok because min is included
    // value > 0, jump > 0
    value = std::nextafter(std::numeric_limits<double>::max(), 0.); // next-smaller value below numeric max
    EXPECT_EQ(boundary.updateAndMirror(value, 3.*(std::numeric_limits<double>::max() - value)), std::numeric_limits<double>::max() - 2.*(std::numeric_limits<double>::max() - value));
    // value > 0, jump < 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
}

//--------------------------------------------
// Test: min is negative, max is default (numeric limit)
struct TBoundaryMinIsNegativeDoubleTest : Test {
    std::string min = "-1.";
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIncluded = true;
    bool maxIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;
    TBoundary<double> boundary;
};

TEST_F(TBoundaryMinIsNegativeDoubleTest, init){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_THAT(boundary.minIncluded(), true);
    EXPECT_THAT(boundary.maxIncluded(), true);
    EXPECT_EQ(boundary.min(), -1.);
    EXPECT_EQ(boundary.max(), std::numeric_limits<double>::max());
    EXPECT_EQ(boundary.range(), std::numeric_limits<double>::max());
}

TEST_F(TBoundaryMinIsNegativeDoubleTest, init_minIsMax){
    // min = max -> throw
    min = toString(std::numeric_limits<double>::max());
    EXPECT_THROW({try {boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);

    // min = max, min is not included -> still throw
    minIncluded = false;
    EXPECT_THROW({try {boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TBoundaryMinIsNegativeDoubleTest, checkRangeMinIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_NO_THROW(boundary.checkRangeAndThrow(1., "boundary"));
    // value -1. is exactly min, but min is included -> ok
    EXPECT_NO_THROW(boundary.checkRangeAndThrow(-1., "boundary"));
    // value -1.01 is below min
    EXPECT_THROW({try { boundary.checkRangeAndThrow(-1.01, "boundary");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TBoundaryMinIsNegativeDoubleTest, checkRangeMinExcluded){
    minIncluded = false;
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_NO_THROW(boundary.checkRangeAndThrow(1., "boundary"));
    // value -1 is exactly min, but min is not included -> throw
    EXPECT_THROW({try { boundary.checkRangeAndThrow(-1., "boundary");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    // value -1.01 is below min
    EXPECT_THROW({try { boundary.checkRangeAndThrow(-1.01, "boundary");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TBoundaryMinIsNegativeDoubleTest, checkForValidJumpSizeMirroring){
    // jump size is restricted to range/2
    EXPECT_NO_FATAL_FAILURE(boundary.updateAndMirror(0., std::numeric_limits<double>::max()/2.));
    EXPECT_NO_FATAL_FAILURE(boundary.updateAndMirror(0., -std::numeric_limits<double>::max()/2.));

    // too large
    EXPECT_DEATH(boundary.updateAndMirror(0., std::numeric_limits<double>::max()*0.51), "");
    EXPECT_DEATH(boundary.updateAndMirror(0., -std::numeric_limits<double>::max()*0.51), "");
}

// don't mirror, min and max are included
TEST_F(TBoundaryMinIsNegativeDoubleTest, dontMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 (cannot jump directly on maximum because jump size is restricted to range/2)
    EXPECT_EQ(boundary.updateAndMirror(0., 1.), 1.);
    EXPECT_EQ(boundary.updateAndMirror(0., 1000.), 1000.);
    // value = 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(0., -0.5), -0.5);
    EXPECT_EQ(boundary.updateAndMirror(0., -1.), -1.); // ok because min is included
    // value < 0, jump > 0 (cannot jump directly on maximum because jump size is restricted to range/2)
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 1.), 0.5);
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 1000.), 999.5);
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, -0.2), -0.7);
    EXPECT_EQ(boundary.updateAndMirror(-0.5, -0.5), -1.);  // ok because min is included
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(1., 1.), 2.);
    EXPECT_EQ(boundary.updateAndMirror(1000000., 1000.), 1001000.);
    double value = std::nextafter(std::numeric_limits<double>::max(), 0.); // next-smaller value below numeric max
    EXPECT_EQ(boundary.updateAndMirror(value, std::numeric_limits<double>::max() - value), std::numeric_limits<double>::max()); // jump exactly on max -> ok because max is included
    // value > 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(1., -2.), -1.); // ok because min is included
    EXPECT_EQ(boundary.updateAndMirror(1000000., -1000.), 999000.);
}

// don't mirror, min and max are excluded
TEST_F(TBoundaryMinIsNegativeDoubleTest, dontMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(0., 1.), 1.);
    EXPECT_EQ(boundary.updateAndMirror(0., 1000.), 1000.);
    // value = 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(0., -0.5), -0.5);
    EXPECT_EQ(boundary.updateAndMirror(0., -1.), -1+cutoffFloat);
    // value < 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 1.), 0.5);
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 1000.), 999.5);
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, -0.2), -0.7);
    EXPECT_EQ(boundary.updateAndMirror(-0.5, -0.5), -1+cutoffFloat);
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(1., 1.), 2.);
    EXPECT_EQ(boundary.updateAndMirror(1000000., 1000.), 1001000.);
    double value = std::nextafter(std::numeric_limits<double>::max(), 0.); // next-smaller value below numeric max
    EXPECT_EQ(boundary.updateAndMirror(value, std::numeric_limits<double>::max() - value), value); // jump exactly on max -> jump back (max is excluded)
    // value > 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(1., -2.), -1+cutoffFloat);
    EXPECT_EQ(boundary.updateAndMirror(1000000., -1000.), 999000.);
}

// do mirror, min is included
TEST_F(TBoundaryMinIsNegativeDoubleTest, doMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
    EXPECT_EQ(boundary.updateAndMirror(0., 1000000000.), 1000000000.);
    // value = 0, jump < 0
    EXPECT_FLOAT_EQ(boundary.updateAndMirror(0., -3.), 1.);
    EXPECT_FLOAT_EQ(boundary.updateAndMirror(0., -100000.), 99998.);
    // value < 0, jump > 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 1000000000.), 999999999.5);
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, -1), -0.5);
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 1000), 999.5);
    // value > 0, jump > 0
    double value = std::nextafter(std::numeric_limits<double>::max(), 0.); // next-smaller value below numeric max
    EXPECT_EQ(boundary.updateAndMirror(value, 3.*(std::numeric_limits<double>::max() - value)), std::numeric_limits<double>::max() - 2.*(std::numeric_limits<double>::max() - value));
    // value > 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(1., -2.5), -0.5);
    EXPECT_FLOAT_EQ(boundary.updateAndMirror(1., -5.), 2.);
}

TEST_F(TBoundaryMinIsNegativeDoubleTest, doMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    // exactly the same as doMirrorIncluded, as this should not make a difference
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
    EXPECT_EQ(boundary.updateAndMirror(0., 1000000000.), 1000000000.);
    // value = 0, jump < 0
    EXPECT_FLOAT_EQ(boundary.updateAndMirror(0., -3.), 1.);
    EXPECT_FLOAT_EQ(boundary.updateAndMirror(0., -100000.), 99998.);
    // value < 0, jump > 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 1000000000.), 999999999.5);
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, -1), -0.5);
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 1000), 999.5);
    // value > 0, jump > 0
    double value = std::nextafter(std::numeric_limits<double>::max(), 0.); // next-smaller value below numeric max
    EXPECT_EQ(boundary.updateAndMirror(value, 3.*(std::numeric_limits<double>::max() - value)), std::numeric_limits<double>::max() - 2.*(std::numeric_limits<double>::max() - value));
    // value > 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(1., -2.5), -0.5);
    EXPECT_FLOAT_EQ(boundary.updateAndMirror(1., -5.), 2.);
}

//--------------------------------------------
// Test: min is 0, max is default (numeric limit)
struct TBoundaryMinIs0DoubleTest : Test {
    std::string min = "0.";
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIncluded = true;
    bool maxIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;
    TBoundary<double> boundary;
};

TEST_F(TBoundaryMinIs0DoubleTest, init){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_THAT(boundary.minIncluded(), true);
    EXPECT_THAT(boundary.maxIncluded(), true);
    EXPECT_EQ(boundary.min(), 0.);
    EXPECT_EQ(boundary.max(), std::numeric_limits<double>::max());
    EXPECT_EQ(boundary.range(), std::numeric_limits<double>::max());
}

TEST_F(TBoundaryMinIs0DoubleTest, checkForValidJumpSizeMirroring){
    // jump size is restricted to range/2
    EXPECT_NO_FATAL_FAILURE(boundary.updateAndMirror(0., std::numeric_limits<double>::max()/2.));
    EXPECT_NO_FATAL_FAILURE(boundary.updateAndMirror(0., -std::numeric_limits<double>::max()/2.));

    // too large
    EXPECT_DEATH(boundary.updateAndMirror(0., std::numeric_limits<double>::max()*0.51), "");
    EXPECT_DEATH(boundary.updateAndMirror(0., -std::numeric_limits<double>::max()*0.51), "");
}

// don't mirror, min and max are included
TEST_F(TBoundaryMinIs0DoubleTest, dontMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 (cannot jump directly on maximum because jump size is restricted to range/2)
    EXPECT_EQ(boundary.updateAndMirror(0., 1.), 1.);
    EXPECT_EQ(boundary.updateAndMirror(0., 1000.), 1000.);
    // value = 0, jump < 0 -> always mirror -> will be tested below in seperate test
    // value < 0, jump > 0 -> will never occur, as min = 0
    // value < 0, jump < 0 -> will never occur, as min = 0
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(1., 1.), 2.);
    EXPECT_EQ(boundary.updateAndMirror(1000000., 1000.), 1001000.);
    double value = std::nextafter(std::numeric_limits<double>::max(), 0.); // next-smaller value below numeric max
    EXPECT_EQ(boundary.updateAndMirror(value, std::numeric_limits<double>::max() - value), std::numeric_limits<double>::max()); // jump exactly on max -> ok because max is included
    // value > 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(1., -1.), 0.); // ok because min is included
    EXPECT_EQ(boundary.updateAndMirror(1000000., -1000.), 999000.);
}

// don't mirror, min and max are excluded
TEST_F(TBoundaryMinIs0DoubleTest, dontMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> will never occur as min excluded
    // value = 0, jump < 0 -> will never occur as min excluded
    // value < 0, jump > 0 -> will never occur, as min = 0
    // value < 0, jump < 0 -> will never occur, as min = 0
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(1., 1.), 2.);
    EXPECT_EQ(boundary.updateAndMirror(1000000., 1000.), 1001000.);
    double value = std::nextafter(std::numeric_limits<double>::max(), 0.); // next-smaller value below numeric max
    EXPECT_EQ(boundary.updateAndMirror(value, std::numeric_limits<double>::max() - value), value); // jump exactly on max -> jump back (max is excluded)
    // value > 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(1., -1.),  0+cutoffFloat);
    EXPECT_EQ(boundary.updateAndMirror(1000000., -1000.), 999000.);
}

// do mirror, min is included
TEST_F(TBoundaryMinIs0DoubleTest, doMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
    EXPECT_EQ(boundary.updateAndMirror(0., 1000000000.), 1000000000.);
    // value = 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(0., -3.), 3.);
    EXPECT_EQ(boundary.updateAndMirror(0., -100000.), 100000.);
    // value < 0, jump > 0 -> will never occur, as min = 0
    // value < 0, jump < 0 -> will never occur, as min = 0
    // value > 0, jump > 0
    double value = std::nextafter(std::numeric_limits<double>::max(), 0.); // next-smaller value below numeric max
    EXPECT_EQ(boundary.updateAndMirror(value, 3.*(std::numeric_limits<double>::max() - value)), std::numeric_limits<double>::max() - 2.*(std::numeric_limits<double>::max() - value));
    // value > 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(1., -10.),  9.);
    EXPECT_EQ(boundary.updateAndMirror(1000000., -3000000.), 2000000.);
}

TEST_F(TBoundaryMinIs0DoubleTest, doMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> will never occur as min excluded
    // value = 0, jump < 0 -> will never occur as min excluded
    // value < 0, jump > 0 -> will never occur, as min = 0
    // value < 0, jump < 0 -> will never occur, as min = 0
    // value > 0, jump > 0 -> same as doMirrorIncluded
    double value = std::nextafter(std::numeric_limits<double>::max(), 0.); // next-smaller value below numeric max
    EXPECT_EQ(boundary.updateAndMirror(value, 3.*(std::numeric_limits<double>::max() - value)), std::numeric_limits<double>::max() - 2.*(std::numeric_limits<double>::max() - value));
    // value > 0, jump < 0 -> same as doMirrorIncluded
    EXPECT_EQ(boundary.updateAndMirror(1., -10.),  9.);
    EXPECT_EQ(boundary.updateAndMirror(1000000., -3000000.), 2000000.);
}

//--------------------------------------------
// Test: min is 0, max is default (numeric limit)
struct TBoundaryMinIsPositiveDoubleTest : Test {
    std::string min = "1.";
    std::string max = toString(std::numeric_limits<double>::max());
    bool minIncluded = true;
    bool maxIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;
    TBoundary<double> boundary;
};

TEST_F(TBoundaryMinIsPositiveDoubleTest, init){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_THAT(boundary.minIncluded(), true);
    EXPECT_THAT(boundary.maxIncluded(), true);
    EXPECT_EQ(boundary.min(), 1.);
    EXPECT_EQ(boundary.max(), std::numeric_limits<double>::max());
    EXPECT_EQ(boundary.range(), std::numeric_limits<double>::max());
}

TEST_F(TBoundaryMinIsPositiveDoubleTest, checkForValidJumpSizeMirroring){
    // jump size is restricted to range/2
    EXPECT_NO_FATAL_FAILURE(boundary.updateAndMirror(1., std::numeric_limits<double>::max()/2.));
    EXPECT_NO_FATAL_FAILURE(boundary.updateAndMirror(1., -std::numeric_limits<double>::max()/2.));

    // too large
    EXPECT_DEATH(boundary.updateAndMirror(1., std::numeric_limits<double>::max()*0.51), "");
    EXPECT_DEATH(boundary.updateAndMirror(1., -std::numeric_limits<double>::max()*0.51), "");
}

// don't mirror, min and max are included
TEST_F(TBoundaryMinIsPositiveDoubleTest, dontMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> will never occur, as min = 1
    // value = 0, jump < 0 -> will never occur, as min = 1
    // value < 0, jump > 0 -> will never occur, as min = 1
    // value < 0, jump < 0 -> will never occur, as min = 1
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(2., 1.), 3.);
    EXPECT_EQ(boundary.updateAndMirror(1000000., 1000.), 1001000.);
    double value = std::nextafter(std::numeric_limits<double>::max(), 0.); // next-smaller value below numeric max
    EXPECT_EQ(boundary.updateAndMirror(value, std::numeric_limits<double>::max() - value), std::numeric_limits<double>::max()); // jump exactly on max -> ok because max is included
    // value > 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(2., -1.), 1.); // ok because min is included
    EXPECT_EQ(boundary.updateAndMirror(1000000., -1000.), 999000.);
}

// don't mirror, min and max are excluded
TEST_F(TBoundaryMinIsPositiveDoubleTest, dontMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> will never occur, as min = 1
    // value = 0, jump < 0 -> will never occur, as min = 1
    // value < 0, jump > 0 -> will never occur, as min = 1
    // value < 0, jump < 0 -> will never occur, as min = 1
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(2., 1.), 3.);
    EXPECT_EQ(boundary.updateAndMirror(1000000., 1000.), 1001000.);
    double value = std::nextafter(std::numeric_limits<double>::max(), 0.); // next-smaller value below numeric max
    EXPECT_EQ(boundary.updateAndMirror(value, std::numeric_limits<double>::max() - value), value); // jump exactly on max -> jump back (max is excluded)
    // value > 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(2., -1.),  1.+cutoffFloat);
    EXPECT_EQ(boundary.updateAndMirror(1000000., -1000.), 999000.);
}

// do mirror, min is included
TEST_F(TBoundaryMinIsPositiveDoubleTest, doMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> will never occur, as min = 1
    // value = 0, jump < 0 -> will never occur, as min = 1
    // value < 0, jump > 0 -> will never occur, as min = 1
    // value < 0, jump < 0 -> will never occur, as min = 1
    // value > 0, jump > 0
    double value = std::nextafter(std::numeric_limits<double>::max(), 0.); // next-smaller value below numeric max
    EXPECT_EQ(boundary.updateAndMirror(value, 3.*(std::numeric_limits<double>::max() - value)), std::numeric_limits<double>::max() - 2.*(std::numeric_limits<double>::max() - value));
    // value > 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(2., -10.),  10.);
    EXPECT_EQ(boundary.updateAndMirror(1000000., -3000000.), 2000002.);
}

TEST_F(TBoundaryMinIsPositiveDoubleTest, doMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> will never occur, as min = 1
    // value = 0, jump < 0 -> will never occur, as min = 1
    // value < 0, jump > 0 -> will never occur, as min = 1
    // value < 0, jump < 0 -> will never occur, as min = 1
    // value > 0, jump > 0 -> same as doMirrorIncluded
    double value = std::nextafter(std::numeric_limits<double>::max(), 0.); // next-smaller value below numeric max
    EXPECT_EQ(boundary.updateAndMirror(value, 3.*(std::numeric_limits<double>::max() - value)), std::numeric_limits<double>::max() - 2.*(std::numeric_limits<double>::max() - value));
    // value > 0, jump < 0 -> same as doMirrorIncluded
    EXPECT_EQ(boundary.updateAndMirror(2., -10.),  10.);
    EXPECT_EQ(boundary.updateAndMirror(1000000., -3000000.), 2000002.);
}


//--------------------------------------------
// Test: max is positive, min is default (numeric limit)
struct TBoundaryMaxIsPositiveDoubleTest : Test {
    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = "1.";
    bool minIncluded = true;
    bool maxIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;
    TBoundary<double> boundary;
};

TEST_F(TBoundaryMaxIsPositiveDoubleTest, init){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_THAT(boundary.minIncluded(), true);
    EXPECT_THAT(boundary.maxIncluded(), true);
    EXPECT_EQ(boundary.min(), std::numeric_limits<double>::lowest());
    EXPECT_EQ(boundary.max(), 1.);
    EXPECT_EQ(boundary.range(), std::numeric_limits<double>::max());
}

TEST_F(TBoundaryMaxIsPositiveDoubleTest, checkRangeMaxIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_NO_THROW(boundary.checkRangeAndThrow(0.5, "boundary"));
    // value 1. is exactly max, but max is included -> ok
    EXPECT_NO_THROW(boundary.checkRangeAndThrow(1., "boundary"));
    // value 1.01 is above max
    EXPECT_THROW({try { boundary.checkRangeAndThrow(1.01, "boundary");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(TBoundaryMaxIsPositiveDoubleTest, checkRangeMaxExcluded){
    maxIncluded = false;
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_NO_THROW(boundary.checkRangeAndThrow(0.5, "boundary"));
    // value 1 is exactly max, but max is not included -> throw
    EXPECT_THROW({try { boundary.checkRangeAndThrow(1., "boundary");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    // value 1.01 is above max
    EXPECT_THROW({try { boundary.checkRangeAndThrow(1.01, "boundary");} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

// don't mirror, min and max are included
TEST_F(TBoundaryMaxIsPositiveDoubleTest, dontMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(0., 0.5), 0.5);
    EXPECT_EQ(boundary.updateAndMirror(0., 1.), 1.); // ok because max is included
    // value = 0, jump < 0 (cannot jump directly on minimum because jump size is restricted to range/2)
    EXPECT_EQ(boundary.updateAndMirror(0., -0.5), -0.5);
    EXPECT_EQ(boundary.updateAndMirror(0., -10000.), -10000.);
    // value < 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 0.25), -0.25);
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 1.5), 1.); // ok because max is included
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, -0.2), -0.7);
    double value = std::nextafter(std::numeric_limits<double>::lowest(), 0.); // next-smaller value above numeric min
    EXPECT_EQ(boundary.updateAndMirror(value, std::numeric_limits<double>::lowest()-value), std::numeric_limits<double>::lowest()); // jump exactly on min -> ok because min is included
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(0.5, 0.25), 0.75);
    EXPECT_EQ(boundary.updateAndMirror(0.5, 0.5), 1.); // ok because max is included
    // value > 0, jump < 0 (cannot jump directly on minimum because jump size is restricted to range/2)
    EXPECT_EQ(boundary.updateAndMirror(0.5, -2.), -1.5);
    EXPECT_EQ(boundary.updateAndMirror(0.5, -1000.), -999.5);
}

// don't mirror, min and max are excluded
TEST_F(TBoundaryMaxIsPositiveDoubleTest, dontMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(0., 0.5), 0.5);
    EXPECT_EQ(boundary.updateAndMirror(0., 1.), 1.-cutoffFloat);
    // value = 0, jump < 0 (cannot jump directly on minimum because jump size is restricted to range/2)
    EXPECT_EQ(boundary.updateAndMirror(0., -0.5), -0.5);
    EXPECT_EQ(boundary.updateAndMirror(0., -10000.), -10000.);
    // value < 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 0.25), -0.25);
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 1.5), 1.-cutoffFloat);
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, -0.2), -0.7);
    double value = std::nextafter(std::numeric_limits<double>::lowest(), 0.); // next-smaller value above numeric min
    EXPECT_EQ(boundary.updateAndMirror(value, std::numeric_limits<double>::lowest()-value), value); // jump exactly on min -> jump back (min is excluded)
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(0.5, 0.25), 0.75);
    EXPECT_EQ(boundary.updateAndMirror(0.5, 0.5), 1.-cutoffFloat);
    // value > 0, jump < 0 (cannot jump directly on minimum because jump size is restricted to range/2)
    EXPECT_EQ(boundary.updateAndMirror(0.5, -2.), -1.5);
    EXPECT_EQ(boundary.updateAndMirror(0.5, -1000.), -999.5);
}

// do mirror, min is included
TEST_F(TBoundaryMaxIsPositiveDoubleTest, doMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(0., 1.5), 0.5);
    EXPECT_EQ(boundary.updateAndMirror(0., 1000.), -998.);
    // value = 0, jump < 0 -> not possible to cross minimum in this case, as jump size is restricted to range/2
    EXPECT_EQ(boundary.updateAndMirror(0., -10000000.), -10000000.);
    // value < 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 2.), 0.5);
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 1000.), -997.5);
    // value < 0, jump < 0
    double value = std::nextafter(std::numeric_limits<double>::lowest(), 0.); // next-smaller value above numeric min
    EXPECT_EQ(boundary.updateAndMirror(value, -3.*(value-std::numeric_limits<double>::lowest())), std::numeric_limits<double>::lowest() + 2.*(value - std::numeric_limits<double>::lowest())); // jump exactly on min -> ok because min is included
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(0.5, 1.5), 0.);
    EXPECT_EQ(boundary.updateAndMirror(0.5, 2.5), -1.);
    // value > 0, jump < 0 -> not possible to cross minimum in this case, as jump size is restricted to range/2
    EXPECT_EQ(boundary.updateAndMirror(0.5, -100000.), -99999.5);
}

TEST_F(TBoundaryMaxIsPositiveDoubleTest, doMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    // exactly the same as doMirrorIncluded, as this should not make a difference
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(0., 1.5), 0.5);
    EXPECT_EQ(boundary.updateAndMirror(0., 1000.), -998.);
    // value = 0, jump < 0 -> not possible to cross minimum in this case, as jump size is restricted to range/2
    EXPECT_EQ(boundary.updateAndMirror(0., -10000000.), -10000000.);
    // value < 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 2.), 0.5);
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 1000.), -997.5);
    // value < 0, jump < 0
    double value = std::nextafter(std::numeric_limits<double>::lowest(), 0.); // next-smaller value above numeric min
    EXPECT_EQ(boundary.updateAndMirror(value, -3.*(value-std::numeric_limits<double>::lowest())), std::numeric_limits<double>::lowest() + 2.*(value - std::numeric_limits<double>::lowest())); // jump exactly on min -> ok because min is included
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(0.5, 1.5), 0.);
    EXPECT_EQ(boundary.updateAndMirror(0.5, 2.5), -1.);
    // value > 0, jump < 0 -> not possible to cross minimum in this case, as jump size is restricted to range/2
    EXPECT_EQ(boundary.updateAndMirror(0.5, -100000.), -99999.5);
}

//--------------------------------------------
// Test: max is 0, min is default (numeric limit)
struct TBoundaryMaxIs0DoubleTest : Test {
    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = "0.";
    bool minIncluded = true;
    bool maxIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;
    TBoundary<double> boundary;
};

TEST_F(TBoundaryMaxIs0DoubleTest, init){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_THAT(boundary.minIncluded(), true);
    EXPECT_THAT(boundary.maxIncluded(), true);
    EXPECT_EQ(boundary.min(), std::numeric_limits<double>::lowest());
    EXPECT_EQ(boundary.max(), 0.);
    EXPECT_EQ(boundary.range(), std::numeric_limits<double>::max());
}

// don't mirror, min and max are included
TEST_F(TBoundaryMaxIs0DoubleTest, dontMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> will never occur, as max = 0
    // value = 0, jump < 0 (cannot jump directly on minimum because jump size is restricted to range/2)
    EXPECT_EQ(boundary.updateAndMirror(0., -1.), -1.);
    EXPECT_EQ(boundary.updateAndMirror(0., -1000.), -1000.);
    // value < 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 0.25), -0.25);
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 0.5), 0.); // ok because max is included
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, -0.2), -0.7);
    double value = std::nextafter(std::numeric_limits<double>::lowest(), 0.); // next-smaller value above numeric min
    EXPECT_EQ(boundary.updateAndMirror(value, std::numeric_limits<double>::lowest()-value), std::numeric_limits<double>::lowest()); // jump exactly on min -> ok because min is included
    // value > 0, jump > 0 -> will never occur, as max = 0
    // value > 0, jump < 0 -> will never occur, as max = 0
}

// don't mirror, min and max are excluded
TEST_F(TBoundaryMaxIs0DoubleTest, dontMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> will never occur, as max = 0
    // value = 0, jump < 0 -> will never occur, as max = 0 and max is excluded
    // value < 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 0.25), -0.25);
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 0.5), 0.-cutoffFloat);
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, -0.2), -0.7);
    double value = std::nextafter(std::numeric_limits<double>::lowest(), 0.); // next-smaller value above numeric min
    EXPECT_EQ(boundary.updateAndMirror(value, std::numeric_limits<double>::lowest()-value), value); // jump exactly on min -> jump back (min is excluded)
    // value > 0, jump > 0 -> will never occur, as max = 0
    // value > 0, jump < 0 -> will never occur, as max = 0
}

// do mirror, min is included
TEST_F(TBoundaryMaxIs0DoubleTest, doMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> will never occur, as max = 0
    // value = 0, jump < 0 -> not possible to cross minimum in this case, as jump size is restricted to range/2
    EXPECT_EQ(boundary.updateAndMirror(0., -10000000.), -10000000.);
    // value < 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 2.), -1.5);
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 1000.), -999.5);
    // value < 0, jump < 0
    double value = std::nextafter(std::numeric_limits<double>::lowest(), 0.); // next-smaller value above numeric min
    EXPECT_EQ(boundary.updateAndMirror(value, -3.*(value-std::numeric_limits<double>::lowest())), std::numeric_limits<double>::lowest() + 2.*(value - std::numeric_limits<double>::lowest())); // jump exactly on min -> ok because min is included
    // value > 0, jump > 0 -> will never occur, as max = 0
    // value > 0, jump < 0 -> will never occur, as max = 0
}

TEST_F(TBoundaryMaxIs0DoubleTest, doMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    // exactly the same as doMirrorIncluded, as this should not make a difference
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> will never occur, as max = 0
    // value = 0, jump < 0 -> not possible to cross minimum in this case, as jump size is restricted to range/2
    EXPECT_EQ(boundary.updateAndMirror(0., -10000000.), -10000000.);
    // value < 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 2.), -1.5);
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 1000.), -999.5);
    // value < 0, jump < 0
    double value = std::nextafter(std::numeric_limits<double>::lowest(), 0.); // next-smaller value above numeric min
    EXPECT_EQ(boundary.updateAndMirror(value, -3.*(value-std::numeric_limits<double>::lowest())), std::numeric_limits<double>::lowest() + 2.*(value - std::numeric_limits<double>::lowest())); // jump exactly on min -> ok because min is included
    // value > 0, jump > 0 -> will never occur, as max = 0
    // value > 0, jump < 0 -> will never occur, as max = 0
}

//--------------------------------------------
// Test: max is negative, min is default (numeric limit)
struct TBoundaryMaxIsNegativeDoubleTest : Test {
    std::string min = toString(std::numeric_limits<double>::lowest());
    std::string max = "-1.";
    bool minIncluded = true;
    bool maxIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;
    TBoundary<double> boundary;
};

TEST_F(TBoundaryMaxIsNegativeDoubleTest, init){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_THAT(boundary.minIncluded(), true);
    EXPECT_THAT(boundary.maxIncluded(), true);
    EXPECT_EQ(boundary.min(), std::numeric_limits<double>::lowest());
    EXPECT_EQ(boundary.max(), -1.);
    EXPECT_EQ(boundary.range(), std::numeric_limits<double>::max());
}

// don't mirror, min and max are included
TEST_F(TBoundaryMaxIsNegativeDoubleTest, dontMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> will never occur, as max = -1
    // value = 0, jump < 0 -> will never occur, as max = -1
    // value < 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(-2., 0.25), -1.75);
    EXPECT_EQ(boundary.updateAndMirror(-2., 1.), -1.); // ok because max is included
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-1.5, -0.2), -1.7);
    double value = std::nextafter(std::numeric_limits<double>::lowest(), 0.); // next-smaller value above numeric min
    EXPECT_EQ(boundary.updateAndMirror(value, std::numeric_limits<double>::lowest()-value), std::numeric_limits<double>::lowest()); // jump exactly on min -> ok because min is included
    // value > 0, jump > 0 -> will never occur, as max = -1
    // value > 0, jump < 0 -> will never occur, as max = -1
}

// don't mirror, min and max are excluded
TEST_F(TBoundaryMaxIsNegativeDoubleTest, dontMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> will never occur, as max = -1
    // value = 0, jump < 0 -> will never occur, as max = -1
    // value < 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(-2., 0.25), -1.75);
    EXPECT_EQ(boundary.updateAndMirror(-2, 1.), -1.-cutoffFloat);
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-1.5, -0.2), -1.7);
    double value = std::nextafter(std::numeric_limits<double>::lowest(), 0.); // next-smaller value above numeric min
    EXPECT_EQ(boundary.updateAndMirror(value, std::numeric_limits<double>::lowest()-value), value); // jump exactly on min -> jump back (min is excluded)
    // value > 0, jump > 0 -> will never occur, as max = -1
    // value > 0, jump < 0 -> will never occur, as max = -1
}

// do mirror, min and max are included
TEST_F(TBoundaryMaxIsNegativeDoubleTest, doMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> will never occur, as max = -1
    // value = 0, jump < 0 -> will never occur, as max = -1
    // value < 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(-2., 3.), -3.);
    EXPECT_EQ(boundary.updateAndMirror(-1.5, 1000.), -1000.5);
    // value < 0, jump < 0
    double value = std::nextafter(std::numeric_limits<double>::lowest(), 0.); // next-smaller value above numeric min
    EXPECT_EQ(boundary.updateAndMirror(value, -3.*(value-std::numeric_limits<double>::lowest())), std::numeric_limits<double>::lowest() + 2.*(value - std::numeric_limits<double>::lowest())); // jump exactly on min -> ok because min is included
    // value > 0, jump > 0 -> will never occur, as max = -1
    // value > 0, jump < 0 -> will never occur, as max = -1
}

TEST_F(TBoundaryMaxIsNegativeDoubleTest, doMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    // exactly the same as doMirrorIncluded, as this should not make a difference
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> will never occur, as max = -1
    // value = 0, jump < 0 -> will never occur, as max = -1
    // value < 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(-2., 3.), -3.);
    EXPECT_EQ(boundary.updateAndMirror(-1.5, 1000.), -1000.5);
    // value < 0, jump < 0
    double value = std::nextafter(std::numeric_limits<double>::lowest(), 0.); // next-smaller value above numeric min
    EXPECT_EQ(boundary.updateAndMirror(value, -3.*(value-std::numeric_limits<double>::lowest())), std::numeric_limits<double>::lowest() + 2.*(value - std::numeric_limits<double>::lowest())); // jump exactly on min -> ok because min is included
    // value > 0, jump > 0 -> will never occur, as max = -1
    // value > 0, jump < 0 -> will never occur, as max = -1
}

//-------------------------------------------
// Test: min is 0, max is positive
struct TBoundaryMin0MaxPositiveTest : Test {
    std::string min = "0.";
    std::string max = "1.";
    bool minIncluded = true;
    bool maxIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;
    TBoundary<double> boundary;
};

TEST_F(TBoundaryMin0MaxPositiveTest, init){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_THAT(boundary.minIncluded(), true);
    EXPECT_THAT(boundary.maxIncluded(), true);
    EXPECT_FLOAT_EQ(boundary.min(), 0.);
    EXPECT_FLOAT_EQ(boundary.max(), 1.);
    EXPECT_FLOAT_EQ(boundary.range(), 1.);
}

// don't mirror, min and max are included
TEST_F(TBoundaryMin0MaxPositiveTest, dontMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(0., 0.5), 0.5); // ok because min is included
    EXPECT_EQ(boundary.updateAndMirror(0., 0.25), 0.25); // ok because min is included
    // value = 0, jump < 0 -> can only happen by mirroring -> tested below
    // value < 0, jump > 0 -> never occurs, as min = 0
    // value < 0, jump < 0 -> never occurs, as min = 0
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(0.5, 0.25), 0.75);
    EXPECT_EQ(boundary.updateAndMirror(0.5, 0.5), 1.); // ok because max is included
    // value > 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(0.5, -0.3), 0.2);
    EXPECT_EQ(boundary.updateAndMirror(0.5, -0.5), 0.); // ok because min is included
}

// don't mirror, min and max are excluded
TEST_F(TBoundaryMin0MaxPositiveTest, dontMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> never occurs, as min = 0 and min is excluded
    // value = 0, jump < 0 -> never occurs, as min = 0 and min is excluded
    // value < 0, jump > 0 -> never occurs, as min = 0
    // value < 0, jump < 0 -> never occurs, as min = 0
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(0.5, 0.25), 0.75);
    EXPECT_EQ(boundary.updateAndMirror(0.5, 0.5), 1.-cutoffFloat);
    // value > 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(0.5, -0.3), 0.2);
    EXPECT_EQ(boundary.updateAndMirror(0.5, -0.5), 0.+cutoffFloat);
}

// do mirror, min is included
TEST_F(TBoundaryMin0MaxPositiveTest, doMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> never reach max in order to mirror, because jump size is restricted to range/2
    // value = 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(0., -0.25), 0.25); // ok because min is included
    // value < 0, jump > 0 -> never occurs, as min = 0
    // value < 0, jump < 0 -> never occurs, as min = 0
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(0.8, 0.5), 0.7);
    EXPECT_FLOAT_EQ(boundary.updateAndMirror(0.9, 0.2), 0.9);
    // value > 0, jump < 0
    EXPECT_FLOAT_EQ(boundary.updateAndMirror(0.2, -0.3), 0.1);
    EXPECT_EQ(boundary.updateAndMirror(0.1, -0.5), 0.4);
}

TEST_F(TBoundaryMin0MaxPositiveTest, doMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    // exactly the same as doMirrorIncluded, as this should not make a difference
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> never reach max in order to mirror, because jump size is restricted to range/2
    // value = 0, jump < 0 -> never occurs, as min = 0 and min is excluded
    // value < 0, jump > 0 -> never occurs, as min = 0
    // value < 0, jump < 0 -> never occurs, as min = 0
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(0.8, 0.5), 0.7);
    EXPECT_FLOAT_EQ(boundary.updateAndMirror(0.9, 0.2), 0.9);
    // value > 0, jump < 0
    EXPECT_FLOAT_EQ(boundary.updateAndMirror(0.2, -0.3), 0.1);
    EXPECT_EQ(boundary.updateAndMirror(0.1, -0.5), 0.4);
}

//-------------------------------------------
// Test: min is negative, max is 0
struct TBoundaryMinNegativeMax0Test : Test {
    std::string min = "-1.";
    std::string max = "0.";
    bool minIncluded = true;
    bool maxIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;
    TBoundary<double> boundary;
};

TEST_F(TBoundaryMinNegativeMax0Test, init){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_THAT(boundary.minIncluded(), true);
    EXPECT_THAT(boundary.maxIncluded(), true);
    EXPECT_FLOAT_EQ(boundary.min(), -1.);
    EXPECT_FLOAT_EQ(boundary.max(), 0.);
    EXPECT_FLOAT_EQ(boundary.range(), 1.);
}

// don't mirror, min and max are included
TEST_F(TBoundaryMinNegativeMax0Test, dontMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> can only happen by mirroring -> tested below
    // value = 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(0., -0.5), -0.5); // ok because max is included
    EXPECT_EQ(boundary.updateAndMirror(0., -0.25), -0.25); // ok because max is included
    // value < 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 0.25), -0.25);
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 0.5), 0.); // ok because max is included
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, -0.25), -0.75);
    EXPECT_FLOAT_EQ(boundary.updateAndMirror(-0.5, -0.5), -1.); // ok because min is included
    // value > 0, jump > 0 -> never occurs, as max = 0
    // value > 0, jump < 0 -> never occurs, as max = 0
}

// don't mirror, min and max are excluded
TEST_F(TBoundaryMinNegativeMax0Test, dontMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> never occurs, as max = 0 and max is excluded
    // value = 0, jump < 0 -> never occurs, as max = 0 and max is excluded
    // value < 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 0.25), -0.25);
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 0.5), 0.-cutoffFloat);
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, -0.25), -0.75);
    EXPECT_EQ(boundary.updateAndMirror(-0.5, -0.5), -1.+cutoffFloat);
    // value > 0, jump > 0 -> never occurs, as max = 0
    // value > 0, jump < 0 -> never occurs, as max = 0
}

// do mirror, min is included
TEST_F(TBoundaryMinNegativeMax0Test, doMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> can only happen by mirroring -> tested below
    EXPECT_EQ(boundary.updateAndMirror(0., 0.5), -0.5); // ok because max is included
    EXPECT_EQ(boundary.updateAndMirror(0., 0.25), -0.25); // ok because max is included
    // value = 0, jump < 0 -> never reach min in order to mirror, because jump size is restricted to range/2
    // value < 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(-0.25, 0.5), -0.25);
    EXPECT_EQ(boundary.updateAndMirror(-0.1, 0.5), -0.4);
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-0.75, -0.5), -0.75);
    EXPECT_FLOAT_EQ(boundary.updateAndMirror(-0.8, -0.3), -0.9);
    // value > 0, jump > 0 -> never occurs, as max = 0
    // value > 0, jump < 0 -> never occurs, as max = 0
}

TEST_F(TBoundaryMinNegativeMax0Test, doMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    // exactly the same as doMirrorIncluded, as this should not make a difference
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> never occurs, as max = 0 and max is excluded
    // value = 0, jump < 0 -> never occurs, as max = 0 and min is excluded
    // value < 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(-0.25, 0.5), -0.25);
    EXPECT_EQ(boundary.updateAndMirror(-0.1, 0.5), -0.4);
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-0.75, -0.5), -0.75);
    EXPECT_FLOAT_EQ(boundary.updateAndMirror(-0.8, -0.3), -0.9);
    // value > 0, jump > 0 -> never occurs, as max = 0
    // value > 0, jump < 0 -> never occurs, as max = 0
}

//-------------------------------------------
// Test: min is negative, max is negative
struct TBoundaryMinNegativeMaxNegativeTest : Test {
    std::string min = "-10.";
    std::string max = "-1.";
    bool minIncluded = true;
    bool maxIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;
    TBoundary<double> boundary;
};

TEST_F(TBoundaryMinNegativeMaxNegativeTest, init){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_THAT(boundary.minIncluded(), true);
    EXPECT_THAT(boundary.maxIncluded(), true);
    EXPECT_FLOAT_EQ(boundary.min(), -10.);
    EXPECT_FLOAT_EQ(boundary.max(), -1.);
    EXPECT_FLOAT_EQ(boundary.range(), 9.);
}

// don't mirror, min and max are included
TEST_F(TBoundaryMinNegativeMaxNegativeTest, dontMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> never occurs, as max = -1
    // value = 0, jump < 0 -> never occurs, as max = -1
    // value < 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(-5., 3.25), -1.75);
    EXPECT_EQ(boundary.updateAndMirror(-3., 2.), -1.); // ok because max is included
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-5., -0.25), -5.25);
    EXPECT_FLOAT_EQ(boundary.updateAndMirror(-9.5, -0.5), -10.); // ok because min is included
    // value > 0, jump > 0 -> never occurs, as max = -1
    // value > 0, jump < 0 -> never occurs, as max = -1
}

// don't mirror, min and max are excluded
TEST_F(TBoundaryMinNegativeMaxNegativeTest, dontMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> never occurs, as max = -1
    // value = 0, jump < 0 -> never occurs, as max = -1
    // value < 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(-5., 3.25), -1.75);
    EXPECT_EQ(boundary.updateAndMirror(-3., 2.), -1.-cutoffFloat);
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-5., -0.25), -5.25);
    EXPECT_FLOAT_EQ(boundary.updateAndMirror(-9.5, -0.5), -10.+cutoffFloat);
    // value > 0, jump > 0 -> never occurs, as max = -1
    // value > 0, jump < 0 -> never occurs, as max = -1
}

// do mirror, min is included
TEST_F(TBoundaryMinNegativeMaxNegativeTest, doMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> never occurs, as max = -1
    // value = 0, jump < 0 -> never occurs, as max = -1
    // value < 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(-2., 2.25), -2.25);
    EXPECT_EQ(boundary.updateAndMirror(-3., 3.), -2.);
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-8., -3.), -9.);
    EXPECT_FLOAT_EQ(boundary.updateAndMirror(-9.5, -2.5), -8.);
    // value > 0, jump > 0 -> never occurs, as max = -1
    // value > 0, jump < 0 -> never occurs, as max = -1
}

TEST_F(TBoundaryMinNegativeMaxNegativeTest, doMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    // exactly the same as doMirrorIncluded, as this should not make a difference
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> never occurs, as max = -1
    // value = 0, jump < 0 -> never occurs, as max = -1
    // value < 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(-2., 2.25), -2.25);
    EXPECT_EQ(boundary.updateAndMirror(-3., 3.), -2.);
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-8., -3.), -9.);
    EXPECT_FLOAT_EQ(boundary.updateAndMirror(-9.5, -2.5), -8.);
    // value > 0, jump > 0 -> never occurs, as max = -1
    // value > 0, jump < 0 -> never occurs, as max = -1
}

//-------------------------------------------
// Test: min is negative, max is positive
struct TBoundaryMinNegativeMaxPositiveTest : Test {
    std::string min = "-1.";
    std::string max = "1.";
    bool minIncluded = true;
    bool maxIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;
    TBoundary<double> boundary;
};

TEST_F(TBoundaryMinNegativeMaxPositiveTest, init){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_THAT(boundary.minIncluded(), true);
    EXPECT_THAT(boundary.maxIncluded(), true);
    EXPECT_FLOAT_EQ(boundary.min(), -1.);
    EXPECT_FLOAT_EQ(boundary.max(), 1.);
    EXPECT_FLOAT_EQ(boundary.range(), 2.);
}

// don't mirror, min and max are included
TEST_F(TBoundaryMinNegativeMaxPositiveTest, dontMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(0., 0.25), 0.25);
    EXPECT_EQ(boundary.updateAndMirror(0., 1.), 1.); // ok because max is included
    // value = 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(0., -0.25), -0.25);
    EXPECT_EQ(boundary.updateAndMirror(0., -1.), -1.); // ok because min is included
    // value < 0, jump > 0 (never reach max because jump size is restricted to range/2)
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 0.75), 0.25);
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 0.25), -0.25);
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, -0.25), -0.75);
    EXPECT_FLOAT_EQ(boundary.updateAndMirror(-0.5, -0.5), -1.); // ok because min is included
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(0.5, 0.25), 0.75);
    EXPECT_FLOAT_EQ(boundary.updateAndMirror(0.5, 0.5), 1.); // ok because max is included
    // value > 0, jump < 0 (never reach min because jump size is restricted to range/2)
    EXPECT_EQ(boundary.updateAndMirror(0.5, -0.25), 0.25);
    EXPECT_FLOAT_EQ(boundary.updateAndMirror(0.5, -0.5), 0.);
}

// don't mirror, min and max are excluded
TEST_F(TBoundaryMinNegativeMaxPositiveTest, dontMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(0., 0.25), 0.25);
    EXPECT_EQ(boundary.updateAndMirror(0., 1.), 1.-cutoffFloat);
    // value = 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(0., -0.25), -0.25);
    EXPECT_EQ(boundary.updateAndMirror(0., -1.), -1.+cutoffFloat);
    // value < 0, jump > 0 (never reach max because jump size is restricted to range/2)
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 0.75), 0.25);
    EXPECT_EQ(boundary.updateAndMirror(-0.5, 0.25), -0.25);
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, -0.25), -0.75);
    EXPECT_FLOAT_EQ(boundary.updateAndMirror(-0.5, -0.5), -1.+cutoffFloat);
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(0.5, 0.25), 0.75);
    EXPECT_FLOAT_EQ(boundary.updateAndMirror(0.5, 0.5), 1.-cutoffFloat);
    // value > 0, jump < 0 (never reach min because jump size is restricted to range/2)
    EXPECT_EQ(boundary.updateAndMirror(0.5, -0.25), 0.25);
    EXPECT_FLOAT_EQ(boundary.updateAndMirror(0.5, -0.5), 0.);
}

// do mirror, min is included
TEST_F(TBoundaryMinNegativeMaxPositiveTest, doMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> can't mirror because jump size is restricted to range/2
    // value = 0, jump < 0 -> can't mirror because jump size is restricted to range/2
    // value < 0, jump > 0 -> can't mirror because jump size is restricted to range/2
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, -0.75), -0.75);
    EXPECT_FLOAT_EQ(boundary.updateAndMirror(-0.8, -0.3), -0.9);
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(0.5, 0.75), 0.75);
    EXPECT_FLOAT_EQ(boundary.updateAndMirror(0.9, 0.5), 0.6);
    // value > 0, jump < 0 -> can't mirror because jump size is restricted to range/2
}

TEST_F(TBoundaryMinNegativeMaxPositiveTest, doMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    // exactly the same as doMirrorIncluded, as this should not make a difference
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> can't mirror because jump size is restricted to range/2
    // value = 0, jump < 0 -> can't mirror because jump size is restricted to range/2
    // value < 0, jump > 0 -> can't mirror because jump size is restricted to range/2
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-0.5, -0.75), -0.75);
    EXPECT_FLOAT_EQ(boundary.updateAndMirror(-0.8, -0.3), -0.9);
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(0.5, 0.75), 0.75);
    EXPECT_FLOAT_EQ(boundary.updateAndMirror(0.9, 0.5), 0.6);
    // value > 0, jump < 0 -> can't mirror because jump size is restricted to range/2
}

//-------------------------------------------
// Test: min is negative, max is negative
struct TBoundaryMinPositiveMaxPositiveTest : Test {
    std::string min = "1.";
    std::string max = "10.";
    bool minIncluded = true;
    bool maxIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;
    TBoundary<double> boundary;
};

TEST_F(TBoundaryMinPositiveMaxPositiveTest, init){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_THAT(boundary.minIncluded(), true);
    EXPECT_THAT(boundary.maxIncluded(), true);
    EXPECT_FLOAT_EQ(boundary.min(), 1.);
    EXPECT_FLOAT_EQ(boundary.max(), 10.);
    EXPECT_FLOAT_EQ(boundary.range(), 9.);
}

// don't mirror, min and max are included
TEST_F(TBoundaryMinPositiveMaxPositiveTest, dontMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> never occurs, as min = 1
    // value = 0, jump < 0 -> never occurs, as max = 1
    // value < 0, jump > 0 -> never occurs, as min = 1
    // value < 0, jump < 0 -> never occurs, as min = 1
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(5., 3.25), 8.25);
    EXPECT_EQ(boundary.updateAndMirror(8., 2.), 10.); // ok because max is included
    // value > 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(5., -3.25), 1.75);
    EXPECT_EQ(boundary.updateAndMirror(2., -1.), 1.); // ok because min is included
}

// don't mirror, min and max are excluded
TEST_F(TBoundaryMinPositiveMaxPositiveTest, dontMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> never occurs, as min = 1
    // value = 0, jump < 0 -> never occurs, as max = 1
    // value < 0, jump > 0 -> never occurs, as min = 1
    // value < 0, jump < 0 -> never occurs, as min = 1
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(5., 3.25), 8.25);
    EXPECT_EQ(boundary.updateAndMirror(8., 2.), 10.-cutoffFloat);
    // value > 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(5., -3.25), 1.75);
    EXPECT_EQ(boundary.updateAndMirror(2., -1.), 1.+cutoffFloat);
}

// do mirror, min is included
TEST_F(TBoundaryMinPositiveMaxPositiveTest, doMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> never occurs, as min = 1
    // value = 0, jump < 0 -> never occurs, as max = 1
    // value < 0, jump > 0 -> never occurs, as min = 1
    // value < 0, jump < 0 -> never occurs, as min = 1
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(9., 3.), 8.);
    EXPECT_EQ(boundary.updateAndMirror(7., 4.), 9.);
    // value > 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(2., -3.5), 3.5);
    EXPECT_EQ(boundary.updateAndMirror(3., -4.), 3.);
}

TEST_F(TBoundaryMinPositiveMaxPositiveTest, doMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    // exactly the same as doMirrorIncluded, as this should not make a difference
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> never occurs, as min = 1
    // value = 0, jump < 0 -> never occurs, as max = 1
    // value < 0, jump > 0 -> never occurs, as min = 1
    // value < 0, jump < 0 -> never occurs, as min = 1
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(9., 3.), 8.);
    EXPECT_EQ(boundary.updateAndMirror(7., 4.), 9.);
    // value > 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(2., -3.5), 3.5);
    EXPECT_EQ(boundary.updateAndMirror(3., -4.), 3.);
}

// won't test float, as the logic should be exactly the same as for doubles
// will test int, but only few cases (because same function is used as for doubles, only if we jump on min/max, the template differs)

//-------------------------------------------
// TBoundary int template specialization
//-------------------------------------------

//--------------------------------------------
// Test: min and max are default (numeric limit)
struct TBoundaryDefaultIntTest : Test {
    std::string min = toString(std::numeric_limits<int>::lowest());
    std::string max = toString(std::numeric_limits<int>::max());
    bool minIncluded = true;
    bool maxIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;
    TBoundary<int> boundary;
};

TEST_F(TBoundaryDefaultIntTest, init){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_THAT(boundary.minIncluded(), true);
    EXPECT_THAT(boundary.maxIncluded(), true);
    EXPECT_EQ(boundary.min(), std::numeric_limits<int>::lowest());
    EXPECT_EQ(boundary.max(), std::numeric_limits<int>::max());
    EXPECT_EQ(boundary.range(), std::numeric_limits<int>::max());
}

TEST_F(TBoundaryDefaultIntTest, checkForValidJumpSizeMirroring){
    // jump size is restricted to range/2
    EXPECT_NO_FATAL_FAILURE(boundary.updateAndMirror(0., std::numeric_limits<int>::max()/2.));
    EXPECT_NO_FATAL_FAILURE(boundary.updateAndMirror(0., -std::numeric_limits<int>::max()/2.));

    // too large
    EXPECT_DEATH(boundary.updateAndMirror(0., std::numeric_limits<int>::max()*0.51), "");
    EXPECT_DEATH(boundary.updateAndMirror(0., -std::numeric_limits<int>::max()*0.51), "");
}

// don't mirror, min and max are included
TEST_F(TBoundaryDefaultIntTest, dontMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 (cannot jump directly on maximum because jump size is restricted to range/2)
    EXPECT_EQ(boundary.updateAndMirror(0, 1), 1);
    EXPECT_EQ(boundary.updateAndMirror(0, 1000), 1000);
    // value = 0, jump < 0 (cannot jump directly on minimum because jump size is restricted to range/2)
    EXPECT_EQ(boundary.updateAndMirror(0, -5), -5);
    EXPECT_EQ(boundary.updateAndMirror(0, -1000), -1000);
    // value < 0, jump > 0 (cannot jump directly on maximum because jump size is restricted to range/2)
    EXPECT_EQ(boundary.updateAndMirror(-5, 3), -2);
    EXPECT_EQ(boundary.updateAndMirror(-5, 1000), 995);
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-5, -2), -7);
    int value = std::numeric_limits<int>::lowest() + 1; // next-smaller value above numeric min
    EXPECT_EQ(boundary.updateAndMirror(value, -1), std::numeric_limits<int>::lowest()); // jump exactly on min -> ok because min is included
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(1, 1), 2);
    EXPECT_EQ(boundary.updateAndMirror(1000000, 1000), 1001000);
    value = std::numeric_limits<int>::max() - 1; // next-smaller value below numeric max
    EXPECT_EQ(boundary.updateAndMirror(value, 1), std::numeric_limits<int>::max()); // jump exactly on max -> ok because max is included
    // value > 0, jump < 0 (cannot jump directly on minimum because jump size is restricted to range/2)
    EXPECT_EQ(boundary.updateAndMirror(1, -2), -1);
    EXPECT_EQ(boundary.updateAndMirror(1000000, -1000), 999000.);
}

// don't mirror, min and max are excluded
TEST_F(TBoundaryDefaultIntTest, dontMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 (cannot jump directly on maximum because jump size is restricted to range/2)
    // value = 0, jump < 0 (cannot jump directly on minimum because jump size is restricted to range/2)
    // value < 0, jump > 0 (cannot jump directly on maximum because jump size is restricted to range/2)
    // value < 0, jump < 0
    int value = std::numeric_limits<int>::lowest() + 1; // next-smaller value above numeric min
    EXPECT_EQ(boundary.updateAndMirror(value, -1), value); // jump exactly on min -> jump back (min is excluded)
    // value > 0, jump > 0
    value = std::numeric_limits<int>::max() - 1; // next-smaller value below numeric max
    EXPECT_EQ(boundary.updateAndMirror(value, 1), value); // jump exactly on max -> jump back (max is excluded)
    // value > 0, jump < 0 (cannot jump directly on minimum because jump size is restricted to range/2)
}

// do mirror, min is included
TEST_F(TBoundaryDefaultIntTest, doMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
    // value = 0, jump < 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
    // value < 0, jump > 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
    // value < 0, jump < 0
    int value = std::numeric_limits<int>::lowest() + 1; // next-smaller value above numeric min
    EXPECT_EQ(boundary.updateAndMirror(value, -3), std::numeric_limits<int>::lowest() + 2);
    // value > 0, jump > 0
    value = std::numeric_limits<int>::max() - 1; // next-smaller value below numeric max
    EXPECT_EQ(boundary.updateAndMirror(value, 3), std::numeric_limits<int>::max() - 2);
    // value > 0, jump < 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
}

TEST_F(TBoundaryDefaultIntTest, doMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    // exactly the same as doMirrorIncluded, as this should not make a difference
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
    // value = 0, jump < 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
    // value < 0, jump > 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
    // value < 0, jump < 0
    int value = std::numeric_limits<int>::lowest() + 1; // next-smaller value above numeric min
    EXPECT_EQ(boundary.updateAndMirror(value, -3), std::numeric_limits<int>::lowest() + 2);
    // value > 0, jump > 0
    value = std::numeric_limits<int>::max() - 1; // next-smaller value below numeric max
    EXPECT_EQ(boundary.updateAndMirror(value, 3), std::numeric_limits<int>::max() - 2);
    // value > 0, jump < 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
}

//-------------------------------------------
// Test: min is negative, max is positive
struct TBoundaryMinNegativeMaxPositiveIntTest : Test {
    std::string min = "-10";
    std::string max = "10";
    bool minIncluded = true;
    bool maxIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;
    TBoundary<int> boundary;
};

TEST_F(TBoundaryMinNegativeMaxPositiveIntTest, init){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_THAT(boundary.minIncluded(), true);
    EXPECT_THAT(boundary.maxIncluded(), true);
    EXPECT_FLOAT_EQ(boundary.min(), -10);
    EXPECT_FLOAT_EQ(boundary.max(), 10);
    EXPECT_FLOAT_EQ(boundary.range(), 20);
}

// don't mirror, min and max are included
TEST_F(TBoundaryMinNegativeMaxPositiveIntTest, dontMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(0, 5), 5);
    EXPECT_EQ(boundary.updateAndMirror(0, 10), 10); // ok because max is included
    // value = 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(0., -5), -5);
    EXPECT_EQ(boundary.updateAndMirror(0., -10), -10); // ok because min is included
    // value < 0, jump > 0 (never reach max because jump size is restricted to range/2)
    EXPECT_EQ(boundary.updateAndMirror(-5, 2), -3);
    EXPECT_EQ(boundary.updateAndMirror(-5, 10), 5);
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-5, -2), -7);
    EXPECT_EQ(boundary.updateAndMirror(-5, -5), -10); // ok because min is included
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(5, 2), 7);
    EXPECT_EQ(boundary.updateAndMirror(5, 5), 10); // ok because max is included
    // value > 0, jump < 0 (never reach min because jump size is restricted to range/2)
    EXPECT_EQ(boundary.updateAndMirror(5, -5), 0);
    EXPECT_EQ(boundary.updateAndMirror(5, -10), -5);
}

// don't mirror, min and max are excluded
TEST_F(TBoundaryMinNegativeMaxPositiveIntTest, dontMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(0, 10), 9);
    // value = 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(0., -10), -9);
    // value < 0, jump > 0 (never reach max because jump size is restricted to range/2)
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-5, -5), -9);
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(5, 5), 9);
}

// do mirror, min is included
TEST_F(TBoundaryMinNegativeMaxPositiveIntTest, doMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> can't mirror because jump size is restricted to range/2
    // value = 0, jump < 0 -> can't mirror because jump size is restricted to range/2
    // value < 0, jump > 0 -> can't mirror because jump size is restricted to range/2
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-5, -7), -8);
    EXPECT_EQ(boundary.updateAndMirror(-8, -10), -2);
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(5, 7), 8);
    EXPECT_EQ(boundary.updateAndMirror(9, 10), 1);
    // value > 0, jump < 0 -> can't mirror because jump size is restricted to range/2
}

TEST_F(TBoundaryMinNegativeMaxPositiveIntTest, doMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    // exactly the same as doMirrorIncluded, as this should not make a difference
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> can't mirror because jump size is restricted to range/2
    // value = 0, jump < 0 -> can't mirror because jump size is restricted to range/2
    // value < 0, jump > 0 -> can't mirror because jump size is restricted to range/2
    // value < 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(-5, -7), -8);
    EXPECT_EQ(boundary.updateAndMirror(-8, -10), -2);
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(5, 7), 8);
    EXPECT_EQ(boundary.updateAndMirror(9, 10), 1);
    // value > 0, jump < 0 -> can't mirror because jump size is restricted to range/2
}

//-------------------------------------------
// TBoundary bool template specialization
//-------------------------------------------

TEST(TBoundaryBoolTest, init){
    // this is valid (specify default)
    TBoundary<bool> boundary;
    boundary.init(true, true, "0", "1", true, true);
    EXPECT_THAT(boundary.minIncluded(), true);
    EXPECT_THAT(boundary.maxIncluded(), true);
    EXPECT_EQ(boundary.min(), 0);
    EXPECT_EQ(boundary.max(), 1);
    EXPECT_EQ(boundary.range(), 1);

    // this is valid (specify not default)
    TBoundary<bool> boundary2;
    boundary2.init(false, false, "0", "1", true, true);
    EXPECT_THAT(boundary2.minIncluded(), true);
    EXPECT_THAT(boundary2.maxIncluded(), true);
    EXPECT_EQ(boundary2.min(), 0);
    EXPECT_EQ(boundary2.max(), 1);
    EXPECT_EQ(boundary2.range(), 1);
}

//-------------------------------------------
// TBoundary uint8_t template specialization
//-------------------------------------------
//--------------------------------------------
// Test: min and max are default (numeric limit)
struct TBoundaryDefaultUint8Test : Test {
    std::string min = toString(std::numeric_limits<uint8_t>::lowest());
    std::string max = toString(std::numeric_limits<uint8_t>::max());
    bool minIncluded = true;
    bool maxIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;
    TBoundary<uint8_t> boundary;
};

TEST_F(TBoundaryDefaultUint8Test, init){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_THAT(boundary.minIncluded(), true);
    EXPECT_THAT(boundary.maxIncluded(), true);
    EXPECT_EQ(boundary.min(), std::numeric_limits<uint8_t>::lowest());
    EXPECT_EQ(boundary.max(), std::numeric_limits<uint8_t>::max());
    EXPECT_EQ(boundary.range(), std::numeric_limits<uint8_t>::max());
}

TEST_F(TBoundaryDefaultUint8Test, checkForValidJumpSizeMirroring){
    // jump size is restricted to range/2
    EXPECT_NO_FATAL_FAILURE(boundary.updateAndMirror(0., std::numeric_limits<uint8_t>::max()/2.));
    EXPECT_NO_FATAL_FAILURE(boundary.updateAndMirror(0., -std::numeric_limits<uint8_t>::max()/2.));

    // too large
    EXPECT_DEATH(boundary.updateAndMirror(0., std::numeric_limits<uint8_t>::max()*0.51), "");
    EXPECT_DEATH(boundary.updateAndMirror(0., -std::numeric_limits<uint8_t>::max()*0.51), "");
}

// don't mirror, min and max are included
TEST_F(TBoundaryDefaultUint8Test, dontMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 (cannot jump directly on maximum because jump size is restricted to range/2)
    EXPECT_EQ(boundary.updateAndMirror(0, 1), 1);
    EXPECT_EQ(boundary.updateAndMirror(0, 127), 127);
    // value = 0, jump < 0 -> always mirrors, see below in separate test
    // value < 0, jump > 0 -> never the case for uints
    // value < 0, jump < 0 -> never the case for uints
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(1, 100), 101);
    uint8_t value = std::numeric_limits<uint8_t>::max() - 1; // next-smaller value below numeric max
    EXPECT_EQ(boundary.updateAndMirror(value, 1), std::numeric_limits<uint8_t>::max()); // jump exactly on max -> ok because max is included
    // value > 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(255, -2), 253);
    EXPECT_EQ(boundary.updateAndMirror(10, -10), 0); // ok because mis included
}

// don't mirror, min and max are excluded
TEST_F(TBoundaryDefaultUint8Test, dontMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 (cannot jump directly on maximum because jump size is restricted to range/2)
    // value = 0, jump < 0 -> always mirrors, see below in saperate test
    // value < 0, jump > 0 -> never the case for uints
    // value < 0, jump < 0 -> never the case for uints
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(1, 100), 101);
    uint8_t value = std::numeric_limits<uint8_t>::max() - 1; // next-smaller value below numeric max
    EXPECT_EQ(boundary.updateAndMirror(value, 1), value); // jump exactly on max -> jump back
    // value > 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(254, -2), 252);
    EXPECT_EQ(boundary.updateAndMirror(10, -10), 1); // jump exactly on min -> jump back
}

// do mirror, min is included
TEST_F(TBoundaryDefaultUint8Test, doMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
    // value = 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(0, -2), 2);
    EXPECT_EQ(boundary.updateAndMirror(0, -127), 127);
    // value < 0, jump > 0 -> never the case for uints
    // value < 0, jump < 0 -> never the case for uints
    // value > 0, jump > 0
    uint8_t value = std::numeric_limits<uint8_t>::max() - 1; // next-smaller value below numeric max
    EXPECT_EQ(boundary.updateAndMirror(value, 3), std::numeric_limits<uint8_t>::max() - 2);
    // value > 0, jump < 0
    value = std::numeric_limits<uint8_t>::lowest() + 1; // next-smaller value above numeric min
    EXPECT_EQ(boundary.updateAndMirror(value, -3), std::numeric_limits<uint8_t>::lowest() + 2);
}

TEST_F(TBoundaryDefaultUint8Test, doMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    // exactly the same as doMirrorIncluded, as this should not make a difference
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> not possible to cross maximum in this case, as jump size is restricted to range/2
    // value = 0, jump < 0 -> never occurs if min is excluded
    // value < 0, jump > 0 -> never the case for uints
    // value < 0, jump < 0 -> never the case for uints
    // value > 0, jump > 0
    uint8_t value = std::numeric_limits<uint8_t>::max() - 1; // next-smaller value below numeric max
    EXPECT_EQ(boundary.updateAndMirror(value, 3), std::numeric_limits<uint8_t>::max() - 2);
    // value > 0, jump < 0
    value = std::numeric_limits<uint8_t>::lowest() + 1; // next-smaller value above numeric min
    EXPECT_EQ(boundary.updateAndMirror(value, -3), std::numeric_limits<uint8_t>::lowest() + 2);
}

// Test: min and max are default (numeric limit)
struct TBoundaryNonDefaultUint8Test : Test {
    std::string min = "1";
    std::string max = "10";
    bool minIncluded = true;
    bool maxIncluded = true;
    bool hasDefaultMin = false;
    bool hasDefaultMax = false;
    TBoundary<uint8_t> boundary;
};

TEST_F(TBoundaryNonDefaultUint8Test, init){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    EXPECT_THAT(boundary.minIncluded(), true);
    EXPECT_THAT(boundary.maxIncluded(), true);
    EXPECT_EQ(boundary.min(), 1);
    EXPECT_EQ(boundary.max(), 10);
    EXPECT_EQ(boundary.range(), 9);
}

// don't mirror, min and max are included
TEST_F(TBoundaryNonDefaultUint8Test, dontMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> never the case, as min = 1
    // value = 0, jump < 0 -> never the case, as min = 1
    // value < 0, jump > 0 -> never the case for uints
    // value < 0, jump < 0 -> never the case for uints
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(5, 3), 8);
    EXPECT_EQ(boundary.updateAndMirror(6, 4), 10); // jump exactly on max -> ok because max is included
    // value > 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(6, -2), 4);
    EXPECT_EQ(boundary.updateAndMirror(3, -2), 1); // ok because mis included
}

// don't mirror, min and max are excluded
TEST_F(TBoundaryNonDefaultUint8Test, dontMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> never the case, as min = 1
    // value = 0, jump < 0 -> never the case, as min = 1
    // value < 0, jump > 0 -> never the case for uints
    // value < 0, jump < 0 -> never the case for uints
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(5, 3), 8);
    EXPECT_EQ(boundary.updateAndMirror(6, 4), 9); // jump exactly on max -> jump back
    // value > 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(6, -2), 4);
    EXPECT_EQ(boundary.updateAndMirror(3, -2), 2); // jump exactly on min -> jump back
}

// do mirror, min is included
TEST_F(TBoundaryNonDefaultUint8Test, doMirrorIncluded){
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> never the case, as min = 1
    // value = 0, jump < 0 -> never the case, as min = 1
    // value < 0, jump > 0 -> never the case for uints
    // value < 0, jump < 0 -> never the case for uints
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(9, 4), 7);
    EXPECT_EQ(boundary.updateAndMirror(10, 2), 8); // ok because max is included
    // value > 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(1, -4), 5);  // ok because min is included
    EXPECT_EQ(boundary.updateAndMirror(2, -3), 3);
}

TEST_F(TBoundaryNonDefaultUint8Test, doMirrorExcluded){
    minIncluded = false;
    maxIncluded = false;
    // exactly the same as doMirrorIncluded, as this should not make a difference
    boundary.init(hasDefaultMin, hasDefaultMax, min, max, minIncluded, maxIncluded);
    // value = 0, jump > 0 -> never the case, as min = 1
    // value = 0, jump < 0 -> never the case, as min = 1
    // value < 0, jump > 0 -> never the case for uints
    // value < 0, jump < 0 -> never the case for uints
    // value > 0, jump > 0
    EXPECT_EQ(boundary.updateAndMirror(9, 4), 7);
    // value > 0, jump < 0
    EXPECT_EQ(boundary.updateAndMirror(2, -3), 3);
}

// won't test uint16, 32, 64 etc., as logic should be exactly the same as for uint8_t
