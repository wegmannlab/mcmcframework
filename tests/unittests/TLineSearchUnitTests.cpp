//
// Created by madleina on 24.03.21.
//

#include "../../core/TLineSearch.h"
#include "../TestCase.h"
using namespace testing;

class TPolynomial {
    // use polynomial functions
public:
    TPolynomial(){};

    double polynomialWithMinimum(const double & x) {
        return x*x - x - 2;
    }

    double polynomialWithMaximum(const double & x){
        return -x*x + x - 2;
    }

    double polynomialWithZero(const double & x){
        return x*x*x;
    }
};

class TLineSearchTest : public Test {
public:

    TPolynomial polynomial;
    TLineSearch lineSearch;
    TLog logfile;
    TRandomGenerator randomGenerator;

    // solutions
    double min = 0.5;
    double max = 0.5;
    double root = 0.;
};

TEST_F(TLineSearchTest, findMinimum){
    auto ptr = &TPolynomial::polynomialWithMinimum;
    //lineSearch.report(&logfile);

    uint16_t numRep = 1000;
    for (uint16_t i = 0; i < numRep; i++){
        // get random starting value and initial step size
        double start = randomGenerator.getRand(-10., 10.);
        double initialStep = randomGenerator.getRand(-10., 10.);
        double result = lineSearch.findMin(polynomial, ptr, start, initialStep, 10e-10, 100000);
        EXPECT_FLOAT_EQ(result, min);
    }
}

TEST_F(TLineSearchTest, findMaximum){
    auto ptr = &TPolynomial::polynomialWithMaximum;
    //lineSearch.report(&logfile);

    uint16_t numRep = 1000;
    for (uint16_t i = 0; i < numRep; i++){
        // get random starting value and initial step size
        double start = randomGenerator.getRand(-10., 10.);
        double initialStep = randomGenerator.getRand(-10., 10.);
        double result = lineSearch.findMax(polynomial, ptr, start, initialStep, 10e-10, 100000);
        EXPECT_FLOAT_EQ(result, max);
    }
}

TEST_F(TLineSearchTest, findZero){
    auto ptr = &TPolynomial::polynomialWithZero;
    //lineSearch.report(&logfile);

    uint16_t numRep = 1000;
    for (uint16_t i = 0; i < numRep; i++){
        // get random starting value and initial step size
        double start = randomGenerator.getRand(-10., 10.);
        double initialStep = randomGenerator.getRand(-10., 10.);
        double result = lineSearch.findZero(polynomial, ptr, start, initialStep, 10e-10, 100000);

        EXPECT_NEAR(result, root, 10e-8);
    }
}