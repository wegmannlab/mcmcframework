//
// Created by caduffm on 7/16/20.
//

#include "../TestCase.h"
#include "../../core/TMCMCParameter.h"
using namespace testing;

//-------------------------------------------
class MockTMCMCParameter : public TMCMCParameter<double> {
public:
    MockTMCMCParameter(std::unique_ptr<TPrior<double>> Prior, std::shared_ptr<TParameterDefinition> & def, TRandomGenerator* RandomGenerator)
            : TMCMCParameter(std::move(Prior), def, RandomGenerator) {};
    MOCK_METHOD(void, _propose, (const size_t & i), (override));
    MOCK_METHOD(double, getAcceptanceRate, (), (override));
};

class MCMCParameterTest : public Test {
protected:
    std::shared_ptr<TParameterDefinition> def;
    std::unique_ptr<TPrior<double>> prior;
    MockRandomGenerator randomGenerator;
    TLog logfile;

    void SetUp() override {
        def = std::make_shared<TParameterDefinition>("def");
    }
    void TearDown() override {}
};

class BridgeTMCMCParameter : public TMCMCParameter<double> {
public:
    BridgeTMCMCParameter(std::unique_ptr<TPrior<double>> Prior, const std::shared_ptr<TParameterDefinition> & def, TRandomGenerator* RandomGenerator)
            : TMCMCParameter(std::move(Prior), def, RandomGenerator) {};

    void _adjustJumpSize(size_t i) {
        return TMCMCParameter::_adjustJumpSize(i);
    }
    double _checkDefaultInitVal(double initVal, bool hasDefaultInitVal){
        return TMCMCParameter::_checkDefaultInitVal(initVal, hasDefaultInitVal);
    }
    void _readInitVals(const std::string& initVal, bool hasDefaultInitVal) {
        TMCMCParameter::_readInitVals(initVal, hasDefaultInitVal);
    }
    void _readInitJumpSizesVals(bool oneJumpSizeForAll, const std::string &initJumpSize){
        TMCMCParameter::_readInitJumpSizesVals(oneJumpSizeForAll, initJumpSize);
    }
    void _reject(const size_t &i) {
        TMCMCParameter::_reject(i);
    }
    double _jumpSize() const  {
        return TMCMCParameter::_jumpSize();
    }
    double _restrictScaleToNumericalLimit(double scale, double jump){
        return TMCMCParameter::_restrictScaleToNumericalLimit(scale, jump);
    }
};

TEST_F(MCMCParameterTest, constructor){
    BridgeTMCMCParameter param(std::move(prior), def, &randomGenerator);

    EXPECT_EQ(param.name(), "def");
    EXPECT_EQ(param.isUpdated(), true);
}

TEST_F(MCMCParameterTest, constructorIsNotUpdated){
    def->update(false);
    BridgeTMCMCParameter param(std::move(prior), def, &randomGenerator);

    EXPECT_EQ(param.name(), "def");
    EXPECT_EQ(param.isUpdated(), false);
}

TEST_F(MCMCParameterTest, initalizeInitialValueWeirdStrings) {
    BridgeTMCMCParameter param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    def->setInitVal("1.");
    EXPECT_NO_THROW(param._readInitVals(def->initVal(), def->hasDefaultInitVal()));
    def->setInitVal("-1.");
    EXPECT_NO_THROW(param._readInitVals(def->initVal(), def->hasDefaultInitVal()));
    def->setInitVal("j");
    EXPECT_THROW(try{param._readInitVals(def->initVal(), def->hasDefaultInitVal());} catch (...) {throw std::runtime_error("blah");}, std::runtime_error);
}

TEST_F(MCMCParameterTest, checkDefaultInitValNoBoundary) {
    BridgeTMCMCParameter param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    double initVal = 0.;
    bool hasDefaultInitVal = true;
    EXPECT_EQ(param._checkDefaultInitVal(initVal, hasDefaultInitVal), initVal);
}

TEST_F(MCMCParameterTest, checkDefaultInitValNoDefaultInitVal) {
    def->setMin("2.", true);
    BridgeTMCMCParameter param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    double initVal = 1.;
    bool hasDefaultInitVal = false;
    EXPECT_EQ(param._checkDefaultInitVal(initVal, hasDefaultInitVal), initVal);
}

TEST_F(MCMCParameterTest, checkDefaultInitValMinAndMax) {
    def->setMin("2.", true);
    def->setMax("4.", true);
    def->setInitVal("2.1"); // just to prevent error in constructor
    BridgeTMCMCParameter param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    double initVal = 0.;
    bool hasDefaultInitVal = true;
    EXPECT_EQ(param._checkDefaultInitVal(initVal, hasDefaultInitVal), 4.);
}

TEST_F(MCMCParameterTest, initalizeJumpSizeProposal) {
    BridgeTMCMCParameter param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    def->setInitJumpSizeProposal("1.");
    EXPECT_NO_THROW(param._readInitJumpSizesVals(def->oneJumpSizeForAll(), def->initJumpSizeProposal()));
    EXPECT_EQ(param._jumpSize(), 1.);

    def->setInitJumpSizeProposal("               1.  ");
    EXPECT_NO_THROW(param._readInitJumpSizesVals(def->oneJumpSizeForAll(), def->initJumpSizeProposal()));
    EXPECT_EQ(param._jumpSize(), 1.);

    def->setInitJumpSizeProposal("-1.");
    EXPECT_THROW({try {param._readInitJumpSizesVals(def->oneJumpSizeForAll(), def->initJumpSizeProposal());} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
    def->setInitJumpSizeProposal("j");
    EXPECT_THROW({try {param._readInitJumpSizesVals(def->oneJumpSizeForAll(), def->initJumpSizeProposal());} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error);
}

TEST_F(MCMCParameterTest, initalizeJumpSizeProposalWithBoundaries) {
    def->setMin("0.", true);
    def->setMax("1.", true);
    def->setInitJumpSizeProposal("1.");

    BridgeTMCMCParameter param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();
    EXPECT_EQ(param._jumpSize(), 0.5);
}

TEST_F(MCMCParameterTest, proposeIsUpdated){
    MockTMCMCParameter param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();
    // check if _propose is called from update() if _isUpdated is true
    EXPECT_CALL(param, _propose(0))
    .Times(1);
    EXPECT_TRUE(param.update());
}

TEST_F(MCMCParameterTest, proposeIsNotUpdated){
    def->update(false);
    MockTMCMCParameter param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    EXPECT_CALL(param, _propose(0))
    .Times(0);
    EXPECT_FALSE(param.update());
}

TEST_F(MCMCParameterTest, proposeNormalNoBoundary){
    TMCMCParameter<double> param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    // mock random number (normal prop kernel)
    EXPECT_CALL(randomGenerator, getNormalRandom)
    .Times(1)
    .WillOnce(Return(2.));
    param.update();
    EXPECT_EQ(param.oldValue<double>(), 0.);
    EXPECT_EQ(param.value<double>(), 2.);
}

TEST_F(MCMCParameterTest, proposeNormalMinAndMaxBoundary){
    def->setMin("-1.", true);
    def->setMax("1.", true);
    def->setInitVal("0.8");
    TMCMCParameter<double> param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    // mock random number (normal prop kernel)
    EXPECT_CALL(randomGenerator, getNormalRandom)
    .Times(1)
    .WillOnce(Return(0.5));
    param.update();
    // should mirror
    EXPECT_EQ(param.oldValue<double>(), 0.8);
    EXPECT_EQ(param.value<double>(), 0.7);
}

TEST_F(MCMCParameterTest, proposeNormalMinBoundary){
    def->setMin("-1.", true);
    TMCMCParameter<double> param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    // mock random number (normal prop kernel)
    EXPECT_CALL(randomGenerator, getNormalRandom)
    .Times(1)
    .WillOnce(Return(-1.5));
    param.update();
    // should mirror
    EXPECT_EQ(param.oldValue<double>(), 0.);
    EXPECT_EQ(param.value<double>(), -0.5);
}

TEST_F(MCMCParameterTest, proposeNormalMaxBoundary){
    def->setMax("1.", true);
    TMCMCParameter<double> param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    // mock random number (normal prop kernel)
    EXPECT_CALL(randomGenerator, getNormalRandom)
    .Times(1)
    .WillOnce(Return(1.5));
    param.update();
    // should mirror
    EXPECT_EQ(param.oldValue<double>(), 0.);
    EXPECT_EQ(param.value<double>(), 0.5);
}

TEST_F(MCMCParameterTest, proposeUniformNoBoundary){
    def->setPropKernel("uniform");
    TMCMCParameter<double> param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    // mock random number (uniform prop kernel)
    EXPECT_CALL(randomGenerator, getRand)
    .Times(1)
    .WillOnce(Return(2.));
    param.update();
    EXPECT_EQ(param.oldValue<double>(), 0.);
    EXPECT_FLOAT_EQ(param.value<double>(), 0.15);
}

TEST_F(MCMCParameterTest, proposeUniformMinAndMaxBoundary){
    def->setPropKernel("uniform");
    def->setMin("-1.", true);
    def->setMax("1.", true);
    def->setInitJumpSizeProposal("20.");
    TMCMCParameter<double> param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    // mock random number (uniform prop kernel)
    EXPECT_CALL(randomGenerator, getRand)
    .Times(1)
    .WillOnce(Return(1.));
    param.update();
    // should mirror
    EXPECT_EQ(param.oldValue<double>(), 0.);
    EXPECT_EQ(param.value<double>(), 0.5);
}

TEST_F(MCMCParameterTest, proposeUniformMinBoundary){
    def->setPropKernel("uniform");
    def->setMin("-1.", true);
    def->setInitJumpSizeProposal("20.");
    TMCMCParameter<double> param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    // mock random number (uniform prop kernel)
    EXPECT_CALL(randomGenerator, getRand)
    .Times(1)
    .WillOnce(Return(0.1));
    param.update();
    // should mirror
    EXPECT_EQ(param.oldValue<double>(), 0.);
    EXPECT_EQ(param.value<double>(), 6.);
}

TEST_F(MCMCParameterTest, proposeUniformMaxBoundary){
    def->setPropKernel("uniform");
    def->setMax("1.", true);
    def->setInitJumpSizeProposal("20.");
    TMCMCParameter<double> param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    // mock random number (uniform prop kernel)
    EXPECT_CALL(randomGenerator, getRand)
    .Times(1)
    .WillOnce(Return(0.9));
    param.update();
    // should mirror
    EXPECT_EQ(param.oldValue<double>(), 0.);
    EXPECT_EQ(param.value<double>(), -6.);
}

TEST_F(MCMCParameterTest, proposeBool){
    std::unique_ptr<TPrior<bool>> prior1;
    TMCMCParameter<bool> param(std::move(prior1), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    param.update();
    EXPECT_EQ(param.oldValue<bool>(), false);
    EXPECT_EQ(param.value<bool>(), true);
}

TEST_F(MCMCParameterTest, getLogPriorRatio){
    prior = std::make_unique<TPriorNormal<double>>();
    prior->initialize("0,1");
    def->setInitVal("10.");

    std::shared_ptr<TMCMCParameter<double>> param;
    param = std::make_shared<TMCMCParameter<double>>(std::move(prior), def, &randomGenerator);
    param->initializeStorageSingleElementBasedOnPrior();

    param->set(1.);
    EXPECT_FLOAT_EQ(param->getLogPriorRatio(), 49.5);
    EXPECT_FLOAT_EQ(param->getLogPriorRatio(), param->getLogPriorDensity() - param->getLogPriorDensityOld());
}

TEST_F(MCMCParameterTest, reject){
    def->setInitVal("10.");
    BridgeTMCMCParameter param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    // mock random number (normal prop kernel)
    EXPECT_CALL(randomGenerator, getNormalRandom)
    .Times(1)
    .WillOnce(Return(2.));
    param.update();
    EXPECT_EQ(param.oldValue<double>(), 10.);
    EXPECT_EQ(param.value<double>(), 12.);

    param._reject(0);
    EXPECT_EQ(param.oldValue<double>(), 10.);
    EXPECT_FLOAT_EQ(param.value<double>(), 10.);
}

TEST_F(MCMCParameterTest, acceptOrRejectDoReject){
    def->setInitVal("10.");
    def->setMeanVarFile("something");
    TMCMCParameter<double> param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    // mock random number (normal prop kernel)
    EXPECT_CALL(randomGenerator, getNormalRandom)
    .Times(1)
    .WillOnce(Return(2.));

    param.update();
    EXPECT_EQ(param.oldValue<double>(), 10.);
    EXPECT_EQ(param.value<double>(), 12.);

    // mock random number (for accepting/rejecting)
    EXPECT_CALL(randomGenerator, getRand)
    .Times(1)
    .WillOnce(Return(0.5));

    double logH = log(0.1);
    EXPECT_FALSE(param.acceptOrReject(logH));
    EXPECT_EQ(param.oldValue<double>(), 10.);
    EXPECT_FLOAT_EQ(param.value<double>(), 10.);
}

TEST_F(MCMCParameterTest, acceptOrRejectDoAccept){
    def->setInitVal("10.");
    def->setMeanVarFile("something");
    TMCMCParameter<double> param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    // mock random number (normal prop kernel)
    EXPECT_CALL(randomGenerator, getNormalRandom)
    .Times(1)
    .WillOnce(Return(2.));

    param.update();
    EXPECT_EQ(param.oldValue<double>(), 10.);
    EXPECT_EQ(param.value<double>(), 12.);

    // mock random number (for accepting/rejecting)
    EXPECT_CALL(randomGenerator, getRand)
    .Times(1)
    .WillOnce(Return(0.5));

    double logH = log(0.8);
    EXPECT_TRUE(param.acceptOrReject(logH));
    EXPECT_EQ(param.oldValue<double>(), 10.);
    EXPECT_FLOAT_EQ(param.value<double>(), 12.);
}

TEST_F(MCMCParameterTest, initBoth){
    def->setInitVal("20.");
    TMCMCParameter<double> param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    EXPECT_EQ(param.oldValue<double>(), 20.);
    EXPECT_EQ(param.value<double>(), 20.);
}

TEST_F(MCMCParameterTest, initBothAcrossBoundary){
    def->setInitVal("20.");
    def->setMax("10.", true);
    TMCMCParameter<double> param(std::move(prior), def, &randomGenerator);

    EXPECT_THROW({try {param.initializeStorageSingleElementBasedOnPrior();} catch (...) {throw std::runtime_error("blah");}}, std::runtime_error); // too many cols
}

TEST_F(MCMCParameterTest, set){
    def->setInitVal("20.");
    TMCMCParameter<double> param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    param.set(10.);
    EXPECT_EQ(param.oldValue<double>(), 20.);
    EXPECT_EQ(param.value<double>(), 10.);
}

TEST_F(MCMCParameterTest, setBoolTrue){
    std::unique_ptr<TPrior<bool>> prior1;
    TMCMCParameter<bool> param(std::move(prior1), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    EXPECT_EQ(param.oldValue<bool>(), false);
    EXPECT_EQ(param.value<bool>(), false);

    param.set(1.);
    EXPECT_EQ(param.oldValue<bool>(), false);
    EXPECT_EQ(param.value<bool>(), true);

    param.set(18.1213);
    EXPECT_EQ(param.oldValue<bool>(), true);
    EXPECT_EQ(param.value<bool>(), true);

    param.set(0.123); // not zero -> bool will be "true"
    EXPECT_EQ(param.oldValue<bool>(), true);
    EXPECT_EQ(param.value<bool>(), true);
}

TEST_F(MCMCParameterTest, setBoolFalse){
    std::unique_ptr<TPrior<bool>> prior1;
    TMCMCParameter<bool> param(std::move(prior1), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    EXPECT_EQ(param.oldValue<bool>(), false);
    EXPECT_EQ(param.value<bool>(), false);

    param.set(0.);
    EXPECT_EQ(param.oldValue<bool>(), false);
    EXPECT_EQ(param.value<bool>(), false);
}

TEST_F(MCMCParameterTest, setAcrossBoundary){
    def->setInitVal("20.");
    def->setMax("50.", true);
    TMCMCParameter<double> param(std::move(prior), def, &randomGenerator);
}

TEST_F(MCMCParameterTest, reset){
    def->setInitVal("20.");
    TMCMCParameter<double> param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    param.set(10.);
    EXPECT_EQ(param.oldValue<double>(), 20.);
    EXPECT_EQ(param.value<double>(), 10.);
    param.reset();
    EXPECT_EQ(param.oldValue<double>(), 20.);
    EXPECT_EQ(param.value<double>(), 20.);
}

TEST_F(MCMCParameterTest, acceptanceRate){
    BridgeTMCMCParameter param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    // nothing happened yet
    EXPECT_EQ(param.getAcceptanceRate(), 1.);

    // mock random number (normal prop kernel)
    EXPECT_CALL(randomGenerator, getNormalRandom)
    .Times(AtLeast(1))
    .WillRepeatedly(Return(2.));
    // mock random number (for accepting/rejecting)
    EXPECT_CALL(randomGenerator, getRand)
    .Times(AtLeast(1))
    .WillRepeatedly(Return(0.5));

    // accept
    param.update();
    double logH = std::log(0.8);
    EXPECT_TRUE(param.acceptOrReject(logH));
    EXPECT_EQ(param.getAcceptanceRate(), 1.);

    // reject
    param.update();
    logH = std::log(0.1);
    EXPECT_FALSE(param.acceptOrReject(logH));
    EXPECT_FLOAT_EQ(param.getAcceptanceRate(), 2./3.);
}

class BridgeUint8tTMCMCParameter : public TMCMCParameter<uint8_t> {
public:
    BridgeUint8tTMCMCParameter(std::unique_ptr<TPrior<uint8_t>> Prior, const std::shared_ptr<TParameterDefinition> & def, TRandomGenerator* RandomGenerator)
            : TMCMCParameter(std::move(Prior), def, RandomGenerator) {};
    double _restrictScaleToNumericalLimit(double scale, uint8_t jump){
        return TMCMCParameter::_restrictScaleToNumericalLimit(scale, jump);
    }
};

TEST_F(MCMCParameterTest, _restrictScaleToNumericalLimit){
    std::unique_ptr<TPrior<uint8_t>> prior;
    BridgeUint8tTMCMCParameter param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    // range = 255 -> range/2 = 127.5 -> as uint8_t = 127

    // scale sufficiently small
    double scale = 12.7;
    uint8_t jump = 10;
    EXPECT_EQ(param._restrictScaleToNumericalLimit(scale, jump), 12.7); // 12.7*10 = 127 < 127.5

    // scale exactly max
    scale = 12.75;
    EXPECT_EQ(param._restrictScaleToNumericalLimit(scale, jump), 12.75); // 12.75*10 = 127.5 = 127.5

    // scale too big
    scale = 12.8;
    EXPECT_EQ(param._restrictScaleToNumericalLimit(scale, jump), 12.75); // 12.8*10 = 128 > 127.5 --> reset to 12.75
}

TEST_F(MCMCParameterTest, adjustJumpSize){
    def->setInitJumpSizeProposal("0.1");
    BridgeTMCMCParameter param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    EXPECT_FLOAT_EQ(param._jumpSize(), 0.1);

    // mock random number (normal prop kernel)
    EXPECT_CALL(randomGenerator, getNormalRandom)
    .Times(1)
    .WillOnce(Return(2.));
    // mock random number (for accepting/rejecting)
    EXPECT_CALL(randomGenerator, getRand)
    .Times(1)
    .WillOnce(Return(0.5));

    // accept
    param.update();
    double logH = std::log(0.8);
    EXPECT_TRUE(param.acceptOrReject(logH));
    EXPECT_EQ(param.getAcceptanceRate(), 1.);

    param._adjustJumpSize(0);
    EXPECT_FLOAT_EQ(param._jumpSize(), 0.3);
}

TEST_F(MCMCParameterTest, valueDouble){
    BridgeTMCMCParameter param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    param.set(1.3562);
    param.set(28.3);
    EXPECT_EQ(param.value<double>(), 28.3);
    EXPECT_EQ(param.oldValue<double>(), 1.3562);
    EXPECT_EQ(param.value<double>(0), 28.3);
    EXPECT_EQ(param.oldValue<double>(0), 1.3562);
}

TEST_F(MCMCParameterTest, valueFloat){
    std::unique_ptr<TPrior<float>> prior;
    TMCMCParameter<float> param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    param.set(1.3562);
    param.set(28.3);
    EXPECT_FLOAT_EQ(param.value<float>(), 28.3);
    EXPECT_FLOAT_EQ(param.oldValue<float>(), 1.3562);
    EXPECT_FLOAT_EQ(param.value<float>(0), 28.3);
    EXPECT_FLOAT_EQ(param.oldValue<float>(0), 1.3562);
}

TEST_F(MCMCParameterTest, valueInt){
    std::unique_ptr<TPrior<int>> prior;
    TMCMCParameter<int> param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    param.set(1.3562);
    param.set(28.3);
    EXPECT_EQ(param.value<int>(), 28.);
    EXPECT_EQ(param.oldValue<int>(), 1.);
    EXPECT_EQ(param.value<int>(0), 28.);
    EXPECT_EQ(param.oldValue<int>(0), 1.);
}

TEST_F(MCMCParameterTest, valueBool){
    std::unique_ptr<TPrior<bool>> prior;
    TMCMCParameter<bool> param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    param.set(false);
    param.set(true);
    EXPECT_EQ(param.value<bool>(), true);
    EXPECT_EQ(param.oldValue<bool>(), false);
    EXPECT_EQ(param.value<bool>(0), true);
    EXPECT_EQ(param.oldValue<bool>(0), false);
}

TEST_F(MCMCParameterTest, valueUint8t){
    std::unique_ptr<TPrior<uint8_t>> prior;
    TMCMCParameter<uint8_t> param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    param.set(0);
    param.set(255);
    EXPECT_EQ(param.value<uint8_t>(), 255);
    EXPECT_EQ(param.oldValue<uint8_t>(), 0);
    EXPECT_EQ(param.value<uint8_t>(0), 255);
    EXPECT_EQ(param.oldValue<uint8_t>(0), 0);
}

TEST_F(MCMCParameterTest, valueUint16t){
    std::unique_ptr<TPrior<uint16_t>> prior;
    TMCMCParameter<uint16_t> param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    param.set(0);
    param.set(65535);
    EXPECT_EQ(param.value<uint16_t>(), 65535);
    EXPECT_EQ(param.oldValue<uint16_t>(), 0);
    EXPECT_EQ(param.value<uint16_t>(0), 65535);
    EXPECT_EQ(param.oldValue<uint16_t>(0), 0);
}

TEST_F(MCMCParameterTest, valueUint32t){
    std::unique_ptr<TPrior<uint32_t>> prior;
    TMCMCParameter<uint32_t> param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    param.set(0);
    uint32_t max = 4294967295;
    param.set(max);
    EXPECT_EQ(param.value<uint32_t>(), 4294967295);
    EXPECT_EQ(param.oldValue<uint32_t>(), 0);
    EXPECT_EQ(param.value<uint32_t>(0), 4294967295);
    EXPECT_EQ(param.oldValue<uint32_t>(0), 0);
}

TEST_F(MCMCParameterTest, valueUint64t){
    std::unique_ptr<TPrior<uint64_t>> prior;
    TMCMCParameter<uint64_t> param(std::move(prior), def, &randomGenerator);
    param.initializeStorageSingleElementBasedOnPrior();

    param.set(0);
    uint64_t max = 18446744073709551615U;
    param.set(max);
    EXPECT_EQ(param.value<uint64_t>(), 18446744073709551615U);
    EXPECT_EQ(param.oldValue<uint64_t>(), 0);
    EXPECT_EQ(param.value<uint64_t>(0), 18446744073709551615U);
    EXPECT_EQ(param.oldValue<uint64_t>(0), 0);
}